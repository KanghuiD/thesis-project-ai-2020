# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 13:21:26 2020

@author: Debby Leijen-Lam
"""
# %% ----------- Import libraries -----------

import json 
import os
from os.path import dirname, abspath

from operator import itemgetter
import numpy


import pandas as pd
import numpy as np
import re
import collections

from itertools import cycle, combinations

import math
import statistics as stat

import random 


# %% ----------- Topic distance Calculation -----------

# From pandas dataframe group for each docID, each column will be associated with topic group
# Fill in the values for each topic group. 

# Create a dictionary from every sent_top pair in list
def create_dict(tuple_list):
    return dict((x, y) for x, y in tuple_list)

# 
def convert_dict_to_vec(dic, topic_size):
    vec_list = []
    for i in range(0, topic_size):
        if i not in dic:
            vec_list.append(0)
        else:
            vec_list.append(dic[i])
    return vec_list


# Convert the 
def convert_tuples_to_vec(tuple_list):
    topic_size = max(tuple_list)[0][0]
    dic = []
    for tuples in tuple_list:
        dict_entry = create_dict(tuples)
        vec = convert_dict_to_vec(dict_entry, topic_size+1) 
        dic.append(vec)
        
    return dic


def compare_topic_dist_euclidean(vec1, vec2):
    npa = np.asarray(vec1)
    npb = np.asarray(vec2)
    return numpy.linalg.norm(npa-npb)


def match_topics_document(topic_dist, sent_dist):
    sent_top_dist_list = []
    for sent_item in sent_dist:
        docID = sent_item[1]
        index = int(re.sub("[^0-9]", "", sent_item[1]))
        index -= 1
        sent_top_dist_list.append((topic_dist[index], sent_item))
    return sent_top_dist_list


def get_topic_from_sentimented_topic(sent_top_pair):
    return sent_top_pair[0]


# Get all the topics from a set of sentimented topics = opinion
def get_all_topics_from_opinion(opinion):
    return [get_topic_from_sentimented_topic(pair) for pair in opinion]


#Check if the topic exists in the set of sentimented topics (Opinion)
def check_topic_in_pair(topic, opinion):
    for sent_top in opinion:
        if topic in sent_top:
            return True
        else:
            return False
    
# Check for each opinion if the topic is present in one of its sentimented topics
# Add the document ID into the list for later use.        
def create_cluster(topic, list_of_docs_opinion):
    cluster = []
    for doc_opinion in list_of_docs_opinion:
        doc_id = doc_opinion[1]
        if(check_topic_in_pair(topic, doc_opinion[0])):
            cluster.append(doc_id)
    return cluster


def find_doc_sent_top(doc_id, list_of_docs_opinion):
    results = []
    for doc_opinion in list_of_docs_opinion:
        if doc_id == doc_opinion[1]:
            results.append(doc_opinion)
    return results



# %% -------------------------

# Get the words from the topics
def parse_topic_words(topic):
    # takes the string returned by model.show_topics()
    # split on strings to get topics and the probabilities
    topic = topic.split('+')
    # list to store topic bows
    topic_bow = []
    for word in topic:
        # split probability and word
        prob, word = word.split('*')
        # get rid of spaces and quote marks
        word = word.replace(" ","").replace('"', '')
        topic_bow.append(word)
        
    return topic_bow


# Get the word and percentage tuples
def parse_topic_words_percentages(topic):
    # takes the string returned by model.show_topics()
    # split on strings to get topics and the probabilities
    topic = topic.split('+')
    # list to store topic bows
    topic_bow = []
    for word in topic:
        # split probability and word
        prob, word = word.split('*')
        # get rid of spaces and quote marks
        word = word.replace(" ","").replace('"', '')
        topic_bow.append((word, prob))
        
    return topic_bow


# Get all the words from the topic dictionary
def get_all_topic_words_dict(topic_dict):
    word_list = []
    keys = topic_dict.keys()
    for key in keys:
        words = parse_topic_words(topic_dict[key])
        word_list.extend(words)
    return list(set(word_list))


# Create a dictionary that determines the position of the word to the vector
def convert_words_to_vector_pos(words_list):
    vec_pos = list(range(len(words_list)))
    dictionary = dict(zip(words_list, vec_pos))
    return dictionary

# Convert a sentimented topic into a n-vector and sentiment
def convert_senttop_point(sent_top, topic_dict):
    words_list = get_all_topic_words_dict(topic_dict)
    vec_pos_dict = convert_words_to_vector_pos(words_list)
    vector = list(np.zeros(len(vec_pos_dict) + 1))
    word_percent_tuples = parse_topic_words_percentages(topic_dict[sent_top[0]])
    
    for word_percent in word_percent_tuples:
        pos = vec_pos_dict[word_percent[0]]
        vector[pos] = float(word_percent[1])
    vector[-1] = sent_top[1]
    return vector
    

# Retrieve all the sentimented topics from a list of sentimented topics
# in which a single document has multiple sent_tops
def get_unique_sentimented_topics(sent_tops_list):
    flat_list = [item for sublist in sent_tops_list for item in sublist]
    return list(set(flat_list))


# Retrieve all sentimented topics from a list of annotated tuples of (sentimented_topic, docID)
def get_unique_sentimented_topics_singles(annotated_sent_tops_list):
    flat_list = [item[0] for item in annotated_sent_tops_list if None not in item[0]]
    return list(set(flat_list))


def get_all_topic_words(topics):
    all_topic_words = [parse_topic_words(x[1]) for x in topics]
    flat_word_list = [item for sublist in all_topic_words for item in sublist]
    return list(set(flat_word_list))


def parse_topic_string_words(topic):
    # takes the string returned by model.show_topics()
    # split on strings to get topics and the probabilities
    topic = topic.split('+')
    # list to store topic bows
    topic_bow = []
    for word in topic:
        # split probability and word
        prob, word = word.split('*')
        # get rid of spaces and quote marks
        word = word.replace(" ","").replace('"', '')
        topic_bow.append((word, float(prob)))
        
    return topic_bow


# Python code to convert into dictionary 
def Convert(tup, di): 
    di = dict(tup) 
    return di 

# Calculates the distance between 2 topics based on the words and percentages
# Top1 and top2 are in the form of 
def adjust_jaccard_topic_distance(top1, top2):
    dictionary = {}
    dictionary2 = {}
    id_list = set([x[0] for x in top1])
    id_list2 = [x[0] for x in top2]
    top_value = 0
    
    intersection = list(id_list.intersection(id_list2))
    dicA = Convert(top1, dictionary)
    dicB = Convert(top2, dictionary2)
    
    if dicA == dicB:
        return 0
    elif intersection == []:
        return 1
    else:
        for common in intersection:
            top_value += dicA[common] * dicB[common]
            dicA.pop(common)
            dicB.pop(common)
        max_probs = max(sum(dicA.values()), sum(dicB.values()))
        return 1 - (top_value/max_probs)
    

# Calculate the topic distance between 2 topics    
def tops_distance(top1, top2, top_dic):
    top1 = parse_topic_string_words(top_dic[top1])
    top2 = parse_topic_string_words(top_dic[top2])
    return adjust_jaccard_topic_distance(top1, top2)


# Calculate the topic distance for each topic with every other topic
def topic_distance_dictionary(topics_dic):
    dist = collections.defaultdict(dict)
    keysA = list(topics_dic.keys())
    keysB = list(topics_dic.keys())
    for keya in keysA:
        for keyb in keysB:
            dist[keya][keyb] = tops_distance(keya, keyb, topics_dic)
    return dist


# The sentimented topic distance ranges between the values [0,1] 
# It calculates the adjusted jaccard distance between topics and 
# normalize the sentiment scores ranging [-1,1] into a distance between [0,1]    
def sentimented_tops_distance(sent_top1, sent_top2, top_dic):
    top1 = parse_topic_string_words(top_dic[sent_top1[0]])
    top2 = parse_topic_string_words(top_dic[sent_top2[0]])
    jaccard_distance = adjust_jaccard_topic_distance(top1, top2)
    sentiment_distance = abs(((sent_top1[1] + 1)/ 2) - ((sent_top2[1] + 1)/ 2))
    return (0.5*jaccard_distance) + (0.5*sentiment_distance)


# Parse topic string and probability by the model's id2word
def parse_topic_string_id2word(model, topic):
    # takes the string returned by model.show_topics()
    # split on strings to get topics and the probabilities
    topic = topic.split('+')
    # list to store topic bows
    topic_bow = []
    for word in topic:
        # split probability and word
        prob, word = word.split('*')
        # get rid of spaces and quote marks
        word = word.replace(" ","").replace('"', '')
        # convert to word_type
        word = model.id2word.doc2bow([word])[0][0]
        topic_bow.append((word, float(prob)))
    return topic_bow
    

# Calculate the sent_top distance for each element gi to gj and 
# save it in a dictionary/ hash map
def calculate_set_distance_dict(list_sent_tops, top_dict):
    dist = collections.defaultdict(dict)
    for i in range(0, (len(list_sent_tops))):
        for j in range(0, len(list_sent_tops)):
            dist[list_sent_tops[i]][list_sent_tops[j]] = sentimented_tops_distance(list_sent_tops[i], list_sent_tops[j], top_dict)
    return dist


################################################################################
#### Scoring function, when distance is between 0-0.25 get lower score
#### Distance between 0.26-0.5 is high score, > 0.5 is lowering score values
# Possible to make the scoring function dynamic based on query point thresholds?

# Assigns a score based on sentiment distance
def score_sentiment_distance(sentimentA, sentimentB):
    # Normalize both sentiment scores and calculate the distance
    distance = abs(((sentimentA + 1)/ 2) - ((sentimentB + 1)/ 2))
    
    # If the sentiment distance is less than 0.25, we return a score based on a sinusoid function over the distance, else 1 - distance.
    return (math.sin(distance*2*math.pi)) if distance < 0.25 else 1 - distance


# Assigns a score based on topic distance
def score_topic_distance(topicA, topicB, topic_dictionary):
    # Normalize both sentiment scores and calculate the distance
    distance = tops_distance(topicA, topicB, topic_dictionary)
    return math.cos(distance*(math.pi/2))


# Corporate both score functions into a single function
def score_sentimented_topic_distance(sent_topA, sent_topB, topic_dictionary):
    if sent_topA == sent_topB:
        return 0
    else:
        sentiment_score = score_sentiment_distance(sent_topA[1], sent_topB[1])
        topic_score = score_topic_distance(sent_topA[0], sent_topB[0], topic_dictionary)
        return sentiment_score + topic_score



# Calculate score based on a dynamic sentimented topic distance threshold for the query
def score_sentimented_topic_distance(sent_topA, sent_topB, topic_dictionary):
    if sent_topA == sent_topB:
        return 0
    else:
        sentiment_score = score_sentiment_distance(sent_topA[1], sent_topB[1])
        topic_score = score_topic_distance(sent_topA[0], sent_topB[0], topic_dictionary)
        return sentiment_score + topic_score



#%% Preprocesses the output of the sentiment-analyser file and convert them into JSON formatted files

# Get the topics of a ldamodel
def get_topics_ldamodel(ldamodel, num_tops=20, num_words=30):
    topics = ldamodel.show_topics(num_tops, num_words)
    return topics

# Convert a tuple into a dictionary
def Convert(tup, di): 
    di = dict(tup) 
    return di 

# Create a dictionary of words and percentages from all the topics of a ldamodel 
def create_topics_dictionary(ldamodel):
    top_dict = {}
    topics = get_topics_ldamodel(ldamodel, ldamodel.num_topics, num_words=30)
    return Convert(topics, top_dict)

# Function to create docIDs based on a list of sentimented topic tuples
def create_docIDs(list_sentimented_topic_tuples):
    names = []
    for i in range(len(list_sentimented_topic_tuples)):
        names.append("Doc" + str(i))
    return names

# Combine the docID with the sentimented topic
def create_named_tuples(list_sentimented_topics, names):
    return list(zip(list_sentimented_topics, names))

# Create annotated sentimented topics, link the docID with sentimented topic
# Takes the output of the function link_topic_sentiment as input
def create_annotated_sentimented_topics(list_sentimented_topics):
    names =  create_docIDs(list_sentimented_topics)
    return create_named_tuples(list_sentimented_topics, names)

# consisting of (topic, sentiment score) and dictionary of topics.
def create_dict_tuples_sentimented_topics(sentimented_topic, top_dic):
    dic = {}
    dic["topic_id"] = sentimented_topic[0]
    dic["topic_words"] = top_dic[sentimented_topic[0]]
    dic["sentiment"] = sentimented_topic[1]
    return dic

# Create a dictionary from lists of annotated sentiment sentences
# Takes the output of function create_annotated_sentimented_topics as input
def create_dict_data_lists(annotated_sentimented_topics, top_dic):
    new_dic = {}
    for tup in annotated_sentimented_topics:
        key = tup[1]
        if tup[0][0] != []:
            new_dic[key] = create_dict_tuples_sentimented_topics(tup[0], top_dic)
    return new_dic
        

################################################################################
######### Dataframe functions #####################################

# Specific function to extract the sent_tops from dataframe
# Extract the sentiment scores for each associated topics and create a vector for it 
# consisting of ([(topic, sentiment score),...], docID), sentimented topics and docID.
# Creates for each document multitude of sentimented topics
def get_sentimented_topic(data_df):
    sent_top_list = []
    temp_sent_top = []
    temp_doc_num = data_df.iloc[0].name[0]
    for i in range(len(data_df) - 1):
        doc_num = data_df.iloc[i].name[0]
        topic_num = data_df.iloc[i].name[1]
        sentiment_score = data_df.iloc[i]["Sentiment Score"]
        if temp_doc_num == doc_num:
            temp_sent_top.append((topic_num, sentiment_score))
            temp_doc_num = doc_num
        else:
            sent_top_list.append((temp_sent_top, temp_doc_num))
            temp_sent_top = [(topic_num, sentiment_score)]
            temp_doc_num = doc_num
        
    return sent_top_list

        

#Create a dictionary for each sentimented topic by saving the topic_id, topic_words and topic_sentiment
def create_dict_sentimented_topic(sentimented_topic, top_dic):
    dic = {}
    dic["topic_id"] = sentimented_topic[0].item()
    dic["topic_words"] = top_dic[sentimented_topic[0]]
    dic["sentiment"] = sentimented_topic[1].item()
    return dic

# From a list of documents of [sentimented topics, docID], create a dictionary
def create_dict_doc(doc, top_dic):
    new_dic = {}
    sent_top_list = []
    for item in doc[0]:
        entry = create_dict_sentimented_topic(item, top_dic)
        sent_top_list.append(entry)
    new_dic[doc[1]] = sent_top_list
    return new_dic


def create_dict_list_docs(docs, ldamodel):
    dic = {}
    top_dic = {}
    topics = get_topics_ldamodel(ldamodel, ldamodel.num_topics, num_words=50)
    top_dic = Convert(topics, top_dic)
    for doc in docs:
        entry = create_dict_doc(doc, top_dic)
        dic.update(entry)
    return dic
        
# create a JSON file based on a dictionary
def create_json_file(dic_data, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "data")  #..\..\software scripts\word-docs 
    with open(os.path.join(save_path, name + '.json'), 'w', encoding='utf-8') as f:
        json.dump(dic_data, f, ensure_ascii=False, indent=4)
        
#Read a JSON file
def read_json_file(file_name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "data")  #..\..\software scripts\word-docs 
    with open(os.path.join(read_path, file_name)) as f:
        data = json.load(f)
    return data

#If each document contains at most 1 sentimented topic
def get_sent_tops_json(dic):
    sent_tops_list = []
    keys = [*dic]
    for key in keys:
        sent_tops_list.append(((dic[key]["topic_id"], dic[key]["sentiment"]), key))
    return sent_tops_list
        

#If each document contains at most 1 sentimented topic, get all sentimented topics with docID
def get_annotated_sent_tops_json(dic):
    sent_tops_list = []
    keys = [*dic]
    for key in keys:
        sent_tops_list.append(((dic[key]["topic_id"], dic[key]["sentiment"]), key))
    return sent_tops_list


#If each document contains at most 1 sentimented topic
def get_all_topics_json(dic):
    tops_list = []
    keys = [*dic]
    di = {}
    for key in keys:
        tops_list.append((dic[key]["topic_id"], dic[key]["topic_words"]))
    return Convert(tops_list, di)


# Get the sentimented topics fromt he JSON file
def get_sentimented_topics_json(dict_elem):
    sent_tops_list = []
    for item in dict_elem:
        sent_tops_list.append((item["topic_id"], item["sentiment"]))
    return sent_tops_list


#If each document contains at more than 1 sentimented topic
def get_topics_json(dict_elem):
    tops_list = []
    di = {}
    for item in dict_elem:
        tops_list.append((item["topic_id"], item["topic_words"]))
    return Convert(tops_list, di)

#If each document contains at more than 1 sentimented topic
def get_all_multiple_topics_json(dic):
    keys = [*dic]
    topic_dic = {}
    for key in keys:
        topic_dic.update(get_topics_json(dic[key]))
    return topic_dic


# %% ----------- Example query q -----------

# Create a hash map with the sentimented topic and related point position
# Convert sentimented topics into points
def create_sent_top_vector_dict(sent_top_list, topics):
    unique_vecs = [convert_senttop_point(u,  topics) for u in sent_top_list]
    np_vecs = [np.array(u) for u in unique_vecs]
    return dict(list(zip(sent_top_list, np_vecs)))


# Get related points based on the euclidean distance 
def get_candidates_query_point(query, sent_tops, topics, k, min_bound=None, max_bound=None):
    
    points_dict = create_sent_top_vector_dict(sent_tops, topics)
    
    query_vec = convert_senttop_point(query, topics)
    keys = [*points_dict]
    distance_dict = {}
    
    for key in keys:
        distance_dict[key] = np.linalg.norm(points_dict[key]-query_vec)
    
    mean = stat.mean(distance_dict.values())
    std = stat.stdev(distance_dict.values())
    
    candidates = {k: v for k, v in distance_dict.items() if v > (mean-2*std) and v < (mean-std)}
    
    
    if min_bound == None and max_bound == None:
        outer_bound = mean-std
        while len(candidates) < k:
            candidates = {k: v for k, v in distance_dict.items() if v > (mean-2*std) and v < outer_bound}
            outer_bound += std
    else:
        while len(candidates) < k:
            candidates = {k: v for k, v in distance_dict.items() if v > min_bound and v < max_bound}
            outer_bound += std
        
    print("The number of candidates for preparing is:", len([*candidates]))    
    
    return create_sent_top_vector_dict([*candidates], topics)
 

# %% ----------- Function calculation IPV; Beforehand, some sentimented topics are already filtered ----------
# -------------- by the boundaries regarding the query point. --------------------------------------


# Pre-processing part to create a dictionary of IPFs
# Function aska for a dictionary consisting of (sent_top and point)
def calculateIPFs(points_dict):
    # Create a default dictionary
    IPFs = collections.defaultdict(dict)
    
    # Get the keys of the argument
    points = [*points_dict]
    for i in points:
        for j in points:
            # Check if point i and j are the same
            if(np.array_equal(points_dict[i], points_dict[j])):
                continue
            else:
                # Calculate the distance between the points
                distance = np.linalg.norm(points_dict[j]-points_dict[i])
                # Calculate the normalized IPV
                normIPV = (points_dict[j] - points_dict[i])/(distance**3)
                # Assign the normalized IPV to the dictionary 
                IPFs[i][j] = normIPV
    return IPFs



def calculateCumulForceCombi(KP, IPFS):
    keys = [*IPFS]
    length_vec = len(IPFS[keys[0]][keys[1]])
    
    combis = combinations(KP, 2)
    cumulF = 0

    # Inititalize an empty vector to tally total IPFs
    total_ipf = np.zeros(length_vec)
    for combi in combis:
        if(combi[0] == combi[1]):
            continue
        else:
            total_ipf = total_ipf + IPFS[combi[0]][combi[1]]
    
        pCumulF = 0
        pCumulF = pCumulF + np.sqrt(np.sum((total_ipf**2)))
        cumulF = cumulF + pCumulF
    
    return cumulF


def calculateCumulForce(KP, IPFS):
    keys = [*IPFS]
    length_vec = len(IPFS[keys[0]][keys[1]])
    
    cumulF = 0

    # Inititalize an empty vector to tally total IPFs
    total_ipf = np.zeros(length_vec)
    for pointA in KP:
        for pointB in KP:
            if(pointA == pointB):
                continue
            else:
                total_ipf = total_ipf + IPFS[pointA][pointB]
        
            pCumulF = 0
            pCumulF = pCumulF + np.sqrt(np.sum((total_ipf**2)))
        
        cumulF = cumulF + pCumulF
    return cumulF
        
                    

# Calculate the CumulForce for every possible combination of k points
def cumulForceSelection(points_dict, k, IPFS):
    # Get the keys of the points_dictionary
    point_ids = [*points_dict]
    # Set up a global variable measure to compare sets of k points
    globalKF = math.inf
    
    #Generate all the possible combinations
    sets_of_k_points = list(combinations(point_ids, k))
    print("There are", len(sets_of_k_points), " combinations.")
    
    # Check for every combination of the points
    for combi in sets_of_k_points:
        KF = calculateCumulForce(combi, IPFS)
        
        # Check if the force is smaller than globalKF
        if KF < globalKF:
            globalKF = KF
            best_point_set = combi
            
    return best_point_set
        

# %% ----------- Running the code Selection -----------

path = dirname(abspath(__file__)) #..\..\software scripts\scripts
parent_dir = dirname(path) #..\..\software scripts
destination_path = os.path.join(parent_dir, "data")  #..\..\software scripts\preprocessed
files = os.listdir(destination_path)

k = input("Type an integer of k to return points: ")

for file in files:
    k = k
    data = read_json_file(file)
    topics = get_all_topics_json(data)
    sent_tops = get_annotated_sent_tops_json(data)
    unique = get_unique_sentimented_topics_singles(sent_tops)
    print("There are", len(unique), "unique points.")
    q = sent_tops[8][0] #Sentimented topic of document 8 
    q2 = sent_tops[4762][0] #Sentimented topic of document 4762
    q3 = sent_tops[61][0] #Sentimented topic of document 61 
    candidates = get_candidates_query_point(q, unique, topics, k)   
    ipfs = calculateIPFs(candidates)
    best = cumulForceSelection(candidates, k, ipfs)
    
    with open(file + "_best_combination.txt", 'w', encoding="utf-8") as output:
        output.write("Query is:", )
        output.write(str(q))
    
        output.write("Topic words:")
        output.write("\n")
        output.write(str(parse_topic_words(topics[q[0]])))
        output.write("\n")
        output.write("Size of the combinations is:")
        output.write("\n")
        output.write(str(k))
        output.write("\n")
        output.write("Best combination is:")
        output.write(str(best))
        output.write("\n")
        
        for pair in best:
            output.write("Topic words for topic " + str(pair[0]))
            output.write(str(parse_topic_words(topics[pair[0]])))
            output.write("\n")
        
        output.close()
        

# %% ----------- Dynamic boundary based on points distance-----------

# get first points of query by mean-2*std and mean-std, otherwise shift mean-std to mean

# candidate points it will be mean to neab+std and mean+2std 

# Based on the point position of the query and a list of sentimented topic points.
# Calculate the distance and find a fitting boundary when a point is close 
def find_close_threshold(query, list_sent_tops, top_dict):
    distance_list = [sentimented_tops_distance(query, sent_top, top_dict) for sent_top in list_sent_tops]
    mean = stat.mean(distance_list)
    std = stat.stdev(distance_list)
    return (mean-2*std), (mean-std), mean


def find_far_threshold(query, list_sent_tops, top_dict):
    distance_list = [sentimented_tops_distance(query, sent_top, top_dict) for sent_top in list_sent_tops]
    mean = stat.mean(distance_list)
    std = stat.stdev(distance_list)
    return mean, (mean+std), (mean+2*std)


# For the points in the results list, point is far enough if it is greater than initial point
# mean + sd. 

# stepplan:
# Calculate the distance for every sent_top in list of sent_tops with Query
# Calculate the mean, sd of distance values of Q. Really close is mean - 3sd or mean-2sd.
#
# Use score function that uses the dynamic thresholds to determine the scores of interestingness to Q. 
    # Distance value closer to mean-sd range, then assign higher score. 
# Order list of score en sentimented topic in descending order
# Calculate distance of top item sent_top with every other sent_top
# Find the distance value which is considered to be far away, thus value of mean + sd or greater
# Add next sent_top item of list, if distance is greater than threshold for all items in Result, then add to results
# Calculate the distance threshold for newly added item.
# Repeat untill list has length of K
# If length Results is nog equal to K, then reduce threshold of result items from mean+sd to mean.

# Chances are, it will choose the points that are at the outer circle points for sure
# Not sure if another set of point might be better as part of the solution in which case, we can use 
# the ratio between sum distance to q/ sum distance across other points
# Just run once to find something. 

def selection_max_distance_naive(query, list_sentimented_topics, k, topic_dictionary):
    results = []
    candidates = [] 
    
    # Calculate the distance between every sentimented topic and query 
    distance_list_to_q = [sentimented_tops_distance(query, sent_top, topic_dictionary) for sent_top in list_sentimented_topics]
    df = pd.DataFrame(list(zip(list_sentimented_topics, distance_list_to_q)), columns = ["sentimented_topic", "distance"])

    std = df.std()[0]
    mean = df.mean()[0]
    
    # Get the candidates sentimented topic that falls between 2 values
    rel_df = df[df['distance'].between((mean-2*std), (mean-std))]

    # If we find nothing, then increase the boundary
    if rel_df.empty:
        rel_df = df[df['distance'].between((mean-2*std), (mean))]
    

    # Get the candidate sentimented topics
    candidates = list(rel_df["sentimented_topic"])

    # Pick a random sentimented topic point
    results.append(random.choice(candidates))
    
    # Create a starting list of sentimented topic and total distance to sent_tops in results
    zip_list = list(zip(candidates, cycle([0]))) 
    cand_df = pd.DataFrame(zip_list,  columns = ["sentimented_topic", "total_distance"])
    
    # repeat untill results has k sent_tops
    while len(results) < k:
        # Calculate the distance of every other point and the recently added point
        total_dist = [sentimented_tops_distance(results[-1], sent_top, topic_dictionary) for sent_top in candidates]
        cand_df_distance = list(cand_df["total_distance"])
        
        # Add the distance value to the total distance value
        new_total_dist = [x + y for x, y in zip(total_dist, cand_df_distance)]
        new_df = pd.DataFrame({'total_distance': new_total_dist})
        cand_df.update(new_df)
        
        # Get the sentimented topic that has the max distance from the sent_tops in results list 
        best_cand_index = cand_df["total_distance"].argmax()
        results.append(cand_df["sentimented_topic"].iloc[best_cand_index])
        
        #Remove the item from the candidates list
        candidates.remove(cand_df["sentimented_topic"].iloc[best_cand_index])
        cand_df.drop(cand_df.index[best_cand_index])
    
    return results

# %% ----------- Find multiple possible combinations -----------



# Chances are, it will choose the points that are at the outer circle points for sure
# Not sure if another set of point might be better as part of the solution in which case, we can use 
# the ratio between sum distance to q/ sum distance across other points
# returns multiple groups

def selection_max_distance_naive_combi(query, list_sentimented_topics, topic_dictionary, k=5):
    results_list = []
    result = []
    candidates = [] 
    
    # Phase 1: Find the potential candidates within the range of the query
    # Calculate the distance between every sentimented topic and query 
    distance_list_to_q = [sentimented_tops_distance(query, sent_top, topic_dictionary) for sent_top in list_sentimented_topics]
    df = pd.DataFrame(list(zip(list_sentimented_topics, distance_list_to_q)), columns = ["sentimented_topic", "distance"])

    std = df.std()[0]
    mean = df.mean()[0]
    
    # Get the candidates sentimented topic that falls between 2 values
    rel_df = df[df['distance'].between((mean-2*std), (mean-std))]

    # If we find nothing, then increase the boundary
    if rel_df.empty:
        rel_df = df[df['distance'].between((mean-2*std), (mean))]
    

    # Get the candidate sentimented topics
    candidates = list(rel_df["sentimented_topic"])
    
    # Create a dictionary of distance between pairs of sent_tops
    candidates_dist_dict = calculate_set_distance_dict(candidates, topic_dictionary)

    # Phase 2: Find potential combinations of sentimented topics.
    
    while len(candidates) != 0:
                
        # Pick a random sentimented topic point
        random_point = random.choice(candidates)
        result.append(random_point)
        candidates.remove(random_point)
    
        # Create a starting list of sentimented topic and total distance to sent_tops in results
        zip_list = list(zip(candidates, cycle([0]))) 
        cand_df = pd.DataFrame(zip_list,  columns = ["sentimented_topic", "total_distance"])

        # repeat untill results has k sent_tops
        while len(result) < k:
                
            # Calculate the distance of every other point and the recently added point
            total_dist = [sentimented_tops_distance(result[-1], sent_top, topic_dictionary) for sent_top in candidates]
            cand_df_distance = list(cand_df["total_distance"])
            
            # Add the distance value to the total distance value
            new_total_dist = [x + y for x, y in zip(total_dist, cand_df_distance)]
            new_df = pd.DataFrame({'total_distance': new_total_dist})
            cand_df.update(new_df)
            
            # Get the sentimented topic that has the max distance from the sent_tops in results list 
            best_cand_index = cand_df["total_distance"].argmax()
            result.append(cand_df["sentimented_topic"].iloc[best_cand_index])
            
            #Remove the item from the candidates list
            candidates.remove(cand_df["sentimented_topic"].iloc[best_cand_index])
            cand_df = cand_df.drop(cand_df.index[best_cand_index])
            
            # If there are less candidates than given number k
            if len(candidates) < k:
                break
        
        if len(result) == 5:
            results_list.append(result)
        
        result = []
        
    # Phase 3 Check the distance ratio
    combi_score_list = []
    temp_dist_k = 0
    
    # Check for each combination of sent_tops
    for combi in results_list:
        dist_to_q = sum([sentimented_tops_distance(q, sent_top, topic_dictionary) for sent_top in combi])
         # Get the total distance score within all items in candidates
        for item in combi:
            # Total distance within candidates, should be high
            temp_dist_k += sum([candidates_dist_dict[item][item2] for item2 in combi])
        
        # Calculate the ratio between total dist of query and total distance between results points
        ratio = dist_to_q/temp_dist_k
        print("total dist. to q, k, ratio:", dist_to_q, temp_dist_k, ratio)
        # Add the tuple of combi and distance ratio
        combi_score_list.append((combi, ratio))
        
    return combi_score_list

    

# %% ----------- 2nd idea to select points with force equation -----------      

# Chances are, it will choose the points that are at the outer circle points for sure
# Not sure if another set of point might be better as part of the solution in which case, we can use 
# the ratio between sum distance to q/ sum distance across other points    

# Calculate the force from sentimented topics by distance
def get_force_score(sent_topA, sent_topB, topic_dictionary):
    return 1/ (sentimented_tops_distance(sent_topA, sent_topB, topic_dictionary)**2)

# get all the possible pair combination from a list of points
def get_pairs_points(list_points):
    return [pair for pair in combinations(list_points, 2)]


def get_most_furthest_points(candidates_dist_dict):
    max_pair = []
    keys = [*candidates_dist_dict]
    for key in keys:
        max_value = max(candidates_dist_dict[key].values())
        max_key = max(candidates_dist_dict[key], key=candidates_dist_dict[key].get)
        max_pair.append(((key,max_key), max_value))
    return [*max(max_pair, key=itemgetter(1))[0]]



# Get the sentimented topic that is closest to the initial sentimented topic point
def get_closest_point_by_distance(key, candidates_dist_dict):
    # Remove the dict entry of the candidates distance dictionary
    candidates_dist_dict[key].pop(key, None)
    # Sort the dict on distance
    max_key = sorted(candidates_dist_dict[key].items(), key = itemgetter(1))    
    return max_key[0][0] #Return the sentimented topic closest to the key


# Get random start point, find the k-1 points that are far away
def selection_max_distance_V2(query, list_sentimented_topics, k,  topic_dictionary):
    candidates = [] 
    temp_combi = []
    combi = []
    
    print("Start phase 1...")
    # Phase 1: Determine candidates
    # Calculate the distance between every sentimented topic and query 
    distance_list_to_q = [sentimented_tops_distance(query, sent_top, topic_dictionary) for sent_top in list_sentimented_topics]
    df = pd.DataFrame(list(zip(list_sentimented_topics, distance_list_to_q)), columns = ["sentimented_topic", "distance"])

    std = df.std()[0]
    mean = df.mean()[0]
    
    # Get the candidates sentimented topic that falls between 2 values
    rel_df = df[df['distance'].between((mean-2*std), (mean-std))]

    # If we find nothing, then increase the boundary
    if rel_df.empty:
        rel_df = df[df['distance'].between((mean-2*std), (mean))]
    
    print("Start phase 2...")
    # Phase 2: Get the combinations of points; for now it picks points by picking the max distance between points
    # Get the candidate sentimented topics
    candidates = list(rel_df["sentimented_topic"])
        
    # Repeat here with candidates untill no more
    # Find the next point that is closest to the first random chosen point
    
    start_point = random.choice(candidates)
    
    while len(candidates) != 0:
                
        # Pick a random sentimented topic point
        temp_combi.append(start_point)
        candidates.remove(start_point)
    
        # Create a starting list of sentimented topic and total distance to sent_tops in results
        zip_list = list(zip(candidates, cycle([0]))) 
        cand_df = pd.DataFrame(zip_list,  columns = ["sentimented_topic", "total_distance"])

        # repeat untill results has k sent_tops
        while len(temp_combi) < k:
                
            # Calculate the distance of every other point and the recently added point
            total_dist = [sentimented_tops_distance(temp_combi[-1], sent_top, topic_dictionary) for sent_top in candidates]
            cand_df_distance = list(cand_df["total_distance"])
            
            # Add the distance value to the total distance value
            new_total_dist = [x + y for x, y in zip(total_dist, cand_df_distance)]
            new_df = pd.DataFrame({'total_distance': new_total_dist})
            cand_df.update(new_df)
            
            # Get the sentimented topic that has the max distance from the sent_tops in results list 
            best_cand_index = cand_df["total_distance"].argmax()
            temp_combi.append(cand_df["sentimented_topic"].iloc[best_cand_index])
            
            #Remove the item from the candidates list
            candidates.remove(cand_df["sentimented_topic"].iloc[best_cand_index])
            cand_df = cand_df.drop(cand_df.index[best_cand_index])
            
            # If there are less candidates than given number k
            if len(candidates) < k:
                break
        
        # If the candidates are precisely of size k, then add it to list 
        if len(temp_combi) == k:
            combi.append(temp_combi)
        
        #Reset the temp_combi list
        temp_combi = []
        
        # Pick a new start point which is closest to the first start point
        distance_list_to_start = [(sentimented_tops_distance(start_point, sent_top, topic_dictionary), sent_top) for sent_top in candidates]
        
        #If there are no candidates
        if len(candidates) == 0:
            break
        
        start_point = min(distance_list_to_start)[1]
    
    print("Start phase 3...")
    # Phase 3: Calculate the force and pick the set with best force
    force_score_list = []
    
    # Calculate for each combination the force score
    for comb in combi:
        pairs = get_pairs_points(comb)
        force = sum([get_force_score(pair[0], pair[1], topic_dictionary) for pair in pairs])/k
        force_score_list.append((comb, force))
    
    best_set = min(force_score_list, key=itemgetter(1))
    
    return best_set


# %% ----------- Selection procedure with results points with boundary ring -----------            

# Chances are, it will choose the points that are at the outer circle points for sure
# Not sure if another set of point might be better as part of the solution in which case, we can use 
# ensure if no point is within the circle radius of the 3 points

def selection_max_distance_constraint(query, list_sentimented_topics, k, topic_dictionary):
    results = []
    candidates = [] 
    
    # Calculate the distance between every sentimented topic and query 
    distance_list_to_q = [sentimented_tops_distance(query, sent_top, topic_dictionary) for sent_top in list_sentimented_topics]
    df = pd.DataFrame(list(zip(list_sentimented_topics, distance_list_to_q)), columns = ["sentimented_topic", "distance"])

    std = df.std()[0]
    mean = df.mean()[0]
    
    # Get the candidates sentimented topic that falls between 2 values
    rel_df = df[df['distance'].between((mean-2*std), (mean-std))]

    # If we find nothing, then increase the boundary
    if rel_df.empty:
        rel_df = df[df['distance'].between((mean-2*std), (mean))]
    

    # Get the candidate sentimented topics
    candidates = list(rel_df["sentimented_topic"])

    # Pick a random sentimented topic point
    results.append(random.choice(candidates))
    
    # Create a starting list of sentimented topic and total distance to sent_tops in results
    zip_list = list(zip(candidates, cycle([0]))) 
    cand_df = pd.DataFrame(zip_list,  columns = ["sentimented_topic", "total_distance"])
    
    # repeat untill results has k sent_tops
    while len(results) < k:
        # Calculate the distance of every other point and the recently added point
        total_dist = [sentimented_tops_distance(results[-1], sent_top, topic_dictionary) for sent_top in candidates]
        cand_df_distance = list(cand_df["total_distance"])
        
        # Add the distance value to the total distance value
        new_total_dist = [x + y for x, y in zip(total_dist, cand_df_distance)]
        new_df = pd.DataFrame({'total_distance': new_total_dist})
        cand_df.update(new_df)
        
        # Get the sentimented topic that has the max distance from the sent_tops in results list 
        best_cand_index = cand_df["total_distance"].argmax()
        results.append(cand_df["sentimented_topic"].iloc[best_cand_index])
        
        #Remove the item from the candidates list
        candidates.remove(cand_df["sentimented_topic"].iloc[best_cand_index])
        cand_df.drop(cand_df.index[best_cand_index])
    
    # Phase Check boundaries for each sent_top in the results
    # Get the threshold for each sent_top in the list.
    # Check if each sent_top pair distance has at least this much distance
    thresholds = [find_far_threshold(res, list_sentimented_topics, topic_dictionary) for res in results]
    mean_thresh = [threshold[0] for threshold in thresholds]
    thresh_dict = dict((x,y) for x,y in zip(results, mean_thresh))
    false_check = []
    
    
    
    pairs = get_pairs_points(results)
        
    for pair in pairs:
        check = sentimented_tops_distance(pair[0], pair[1], topic_dictionary)
        if check < thresh_dict[pair[0]]:
            false_check.append(pair)
            
    # Everything satsfies? Then it's okay
    # Otherwise, remove the points that does not satisfy the thresholds
    # Got through candidate list again, and see if it satisfies thresholds, then add to result list
    # Repeat untill end or thresholds satisfied
    
    return false_check
    
    

# %% ----------- Selection idea number 1 -----------

# Using the score functions.
# Results_list <- []
# k <- integers of k elements
# D <- Set of sentimented topics
# q <- query sentimented topic
# Order list D by score function between q and each element in D in scores list
# Pop top element of ordered list D into results_list
# Pop the next element of D, 
    # if next element is similar to elements in results_lists then skip this. 
    # Check similarity by comparing sentiment score and topic
# Stop when results list has size k 
# return results list

def count_positives(list_tuples):
    return len([num for num in list_tuples if num[0][1] > 0]) 

def check_positive(tup):
    return tup[1] > 0

def selection_by_score(query, list_sentimented_topics, k, topic_dictionary):
    results = []
    scores = []
    
    # For each sentimented topic sent_top, calculate the score between sent_top and the query
    for sent_top in list_sentimented_topics:
        score = score_sentimented_topic_distance(query, sent_top, topic_dictionary)
        scores.append((sent_top, score))
        
    # Sort the list in decscending order by score
    sorted_scores = sorted(scores, key=itemgetter(1), reverse=True)
    results.append(sorted_scores.pop(0))
    
    # Specify the distance threshold
    size = len(results)
    dist_threshold = 0.5
    
    # Repeat while the size of results list is smaller than int k
    while size < k:
        
        for i in range(len(sorted_scores) - 1):
            
            # For loop makes use of list.pop(), making the len of list shorter
            # It might be possible that the func traverses the end of the list 
            # thus break the for loop if this happens
            if i == len(sorted_scores):
                break   
            
            temp_item = sorted_scores[i]
            similar = False
            # Compare the potential sent_top with the sent_top(s) in the result list
            for j in results:
                # If the sentimented distance value is smaller than the threshold, then these sent_tops are similar
                if sentimented_tops_distance(temp_item[0], j[0], topic_dictionary) < dist_threshold:
                    similar = True        
            
            # Create a variable biased to keep track the results list
            biased = False
            
            # sentiment_ratio keeps tabs on the ratio of positive or negative sentimented topics
            sentiment_ratio = count_positives(results)/len(results)
            
            # If there's too many negative sentimented topics and temp_item is also negative
            if (sentiment_ratio < 0.5) and (check_positive(temp_item[0]) == False):
                biased = True

            # If there's too many positive sentimented topics and temp_item is also positive  
            if (sentiment_ratio > 0.5) and (check_positive(temp_item[0]) == True):
                biased = True

            # Add the sent_top to the results lists if it's not similar to any elem in results
            if(similar != True and biased != True):
                results.append(temp_item)
                sorted_scores.pop(i)
            
            # Return results list if the list has a size k
            size = len(results)
            if size == k:
                return [tup[0] for tup in results]
        
        # Lower the threshold if we traversed sorted_scores and results list does not have the size int k
        dist_threshold = dist_threshold*0.95            
        
        

# %% ----------- Selection idea number 2 -----------


def selection_V2(query, list_sentimented_topics, k, topic_dictionary):
    results = []
    candidates = [] 
    scores = []
    topic_dist_dict = {}
    
    max_topic_distance_threshold = 0.5
    
    while len(candidates) < k:
        keys = [*topic_dictionary]
        
        # Calculate the topic distance for every topic and query topic
        for key in keys:
            topic_dist_dict[key] = tops_distance(query[0], key, topic_dictionary)
        
        # Retrieve the topics whose topic distance value is less than the distance threshold 
        candidate_topics = {k: v for k, v in topic_dist_dict.items() if v < max_topic_distance_threshold}
    
        # Consider the sent_top as candidate if it contains one of the topics in the candidate topics list
        for elem in list_sentimented_topics:
            if(elem[0] in candidate_topics):
                candidates.append(elem)
        
        sumDist = 0
        for c in candidate_topics:
            for i in range(1, len(candidate_topics)-1):
                sumDist += sentimented_tops_distance(c, candidate_topics[i], topic_dictionary)
        
        avgDist = sumDist/len(candidate_topics)
        if (avgDist < 0.5):
            max_topic_distance_threshold = max_topic_distance_threshold*1.05
        
        
        # Increase the topic distance threshold if we don't have at least k candidates
        if(len(candidates) < k):
            max_topic_distance_threshold = max_topic_distance_threshold*1.05
    
    # Calculate the score for each candidate sent_top with the query
    for candidate in candidates:
        score = score_sentimented_topic_distance(query, candidate, topic_dictionary)
        scores.append((candidate, score))
    
    # Sort the list in decscending order by score
    sorted_scores = sorted(scores, key=itemgetter(1), reverse=True)
    
    # Set the threshold values
    min_score_threshold = 0.8
    min_sentiment_distance_threshold = 0.5
    
    # Repeat the process if the results list does not have the size integer k
    while len(results) < k:
        for i in range(len(sorted_scores)-1):
            
            # For loop makes use of list.pop(), making the len of list shorter
            # It might be possible that the func traverses the end of the list 
            # thus break the for loop if this happens
            if i == len(sorted_scores):
                break
            
            temp_item = sorted_scores[i]
            not_add = False
            
            # The sent_top should at least have a minimal score to be relevant to 
            # the query to be considered candidate
            if(temp_item[1] >= min_score_threshold):
                for j in results:
                    # If the sentiment score difference value is less than the sentiment dist threshold, 
                    # then the sent_tops are similar in sentiment 
                    if (abs((temp_item[0][1] - j[0][1])) < min_sentiment_distance_threshold):
                        not_add = True
            else:
                not_add = True
            
            # Add the sent_top to the results lists if it's not similar to any elem in results
            if(not_add != True):
                results.append(temp_item)
                sorted_scores.pop(i)
            
            # Return results list if the list has a size k
            if len(results) == k:
                return [tup[0] for tup in results]
        
        # Lower the thresholds if results does not have k elements
        min_score_threshold = min_score_threshold*0.95
        min_sentiment_distance_threshold = min_sentiment_distance_threshold *0.95
                

# %% ----------- Selection idea number 2 -----------

# Calculate the sum of the distance within a set of sentimented topics
def calculate_set_distance(list_sent_tops, top_dict):
    sum_dist = 0
    for i in range(0, (len(list_sent_tops) - 1)):
        for j in range(1, len(list_sent_tops)):
            sum_dist += sentimented_tops_distance(list_sent_tops[i], list_sent_tops[j], top_dict)
    return sum_dist


def sentiment_cases(sent_score1, sent_score2):
    sent_diff = abs(sent_score1 - sent_score2)
    
    
def uninteresting_score(sent_top1, sent_top2, top_dict):
    top1 = parse_topic_string_words(top_dict[sent_top1[0]])
    top2 = parse_topic_string_words(top_dict[sent_top2[0]])
    jaccard_distance = adjust_jaccard_topic_distance(top1, top2)
    

# Calculate the sent_top distance for each element gi to gj and 
# save it in a dictionary/ hash map
def calculate_set_distance_dict(list_sent_tops, top_dict):
    dist = collections.defaultdict(dict)
    for i in range(0, (len(list_sent_tops))):
        for j in range(0, len(list_sent_tops)):
            dist[list_sent_tops[i]][list_sent_tops[j]] = sentimented_tops_distance(list_sent_tops[i], list_sent_tops[j], top_dict)
    return dist


# Calculate the distance of a list of sentimented topics towards query q
# and put it in a dictionary
def calculate_dist_dict_to_q(query, list_sent_tops, top_dict):
    q_dist_dict = {}
    for i in list_sent_tops:
        q_dist_dict[i] = sentimented_tops_distance(q, i, top_dict)
    return q_dist_dict


# Naive approach to the sentimented topic selection, takes all sentimented
# topics into consideration. Reduces or check for every sentimented topic 
# to find the best subset of sentimented topics based on sent_top distance alone.
# Takes a long time to execute

def selection_naive(query, list_sentimented_topics, k, topic_dict):
    temp_dist_q = 9999
    temp_dist_k = 0
    dist_q = 0
    results = []  
    ratio = 99999
    
    # Get K number of sentimented topics
    candidates = list_sentimented_topics[:k]
    
    #Create a dictionary of distance value between query and every element in sent_tops
    q_dist_dict = calculate_dist_dict_to_q(query, list_sentimented_topics, topic_dict)
    
    #Create a dictionary of distance value between every element in sent_tops
    k_dist_dict = calculate_set_distance_dict(list_sentimented_topics, topic_dict)
    
    del list_sentimented_topics[:k]
    # Traverse the sentimented topics list
    for i in list_sentimented_topics:
        
        # Get the total uninteresting score for each candidate to Q
        cand_dict = {key: q_dist_dict[key] for key in candidates}
        temp_dist_q = sum(cand_dict.values()) # Change to UninterestingFunc
        
        # Get the total distance score within all items in candidates
        for item in candidates:
            # Total distance within candidates, should be high
            temp_dist_k += sum([k_dist_dict[item][item2] for item2 in candidates])
        
        # Calculate the ratio
        temp_ratio = temp_dist_q/temp_dist_k
        
        # If the temp_ratio is lower than ratio, then we have found a better subset
        if temp_ratio < ratio:
            ratio = temp_ratio
            results = candidates
        
        # If temp_dist_q is lower, this means that we've caught a sent_top
        # who has a higher value to Q, which is bad, thus delete this key
        if temp_dist_q < dist_q:
            key = min(cand_dict.keys(), key=(lambda k: cand_dict[k]))
        else:
            # If not, this means that the total distance within C has lowered
            # delete the key with the lowest total distance across all items in C
            key = min(cand_dict.keys(), key=(lambda k: cand_dict[k]))
        
        # Remove the value in candidates
        candidates.remove(key)
        
        # Add the new element into candidates
        candidates.append(i)
        
        # Reset the temp_dist_k
        temp_dist_k = 0

    return results
    


