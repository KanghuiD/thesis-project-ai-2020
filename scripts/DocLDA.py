# -*- coding: utf-8 -*-
"""
Created on Thu May 14 16:23:54 2020

@author: Debby Leijen-Lam
"""


# %% ----------- Import libraries -----------

import os
from os.path import dirname, abspath
import time

import matplotlib.pyplot as plt
from matplotlib import gridspec

from operator import itemgetter

import gensim, spacy
from gensim.models import Phrases
from gensim import corpora, models, similarities 
from nltk.corpus import stopwords

import pandas as pd
import numpy as np
import re

import en_core_web_sm
nlp = en_core_web_sm.load()
from spacy.lang.en.stop_words import STOP_WORDS
from gensim.parsing.preprocessing import STOPWORDS

from gensim.models import CoherenceModel# Compute Coherence Score


#text processing
import re
import string
import nltk

from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem.porter import PorterStemmer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer

# %% ----------- Load and create a list of stopwords -----------

# Load NLTK stopwords
stop_words = stopwords.words('english')

# Add some extra words in it if required
stop_words.extend(['from', 'subject','pron'])

# Load Spacy stopwords
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS

# Create one big stopwords list
stopwords_list = STOPWORDS.union(set(stop_words), spacy_stopwords)

# %% ----------- Read a file with more stop words and extend the stop words list -----------


def load_stopwords_list(name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "word-docs")  #..\..\software scripts\word-docs
    file = open(os.path.join(read_path, name), mode='r', encoding='utf-8')

    # read all lines at once
    extra_stopwords = file.readlines()

    # close the file
    file.close()
    return extra_stopwords



# Clean the list of stopwords from \n
def clean_stopwords(list):
    clean_list = []
    for word in list:
         clean_list.append(word.rstrip("\n"))
    return clean_list
         


def save_stopwords_list(word_list, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "word-docs")  #..\..\software scripts\word-docs 
    with open(os.path.join(save_path, name), 'w', encoding="utf-8") as output:
        for word in word_list:
            output.write(str(word) + "\n")


# %% ----------- Read file in----------

def read_file(doc):
    file = open(doc , mode='r', encoding='utf-8')
    text = file.read()
    file.close()
    return text


def read_preprocessed(num = None):
    num = num
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "preprocessed")  #..\..\software scripts\preprocessed
    os.chdir(read_path) 
    files = os.listdir()
    text_list = []
    if num == None:
        print("Progress is starting...")
        count = 0
        for file in files:
            file = open(file, mode='r', encoding='utf-8')
            text = file.readlines()
            file.close()
            text = "".join(text[:-1])
            text_list.append(text)
            count += 1
            if(count % 1000 == 0):
                print("Progress at file: " + str(count))
        print("Process is done!")
        return list(set(text_list))
    else:
        for i in range(0, num):
            text_list.append(read_file(files[i]))
        return list(set(text_list))


    
# %% ----------- Clean text function ----------

# Split whole text into sentences
def split_sentences(text):
    sentences = nltk.sent_tokenize(text)
    sentences.pop(-1)
    return sentences


# Retrieve the WordNet POS tag
def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}

    return tag_dict.get(tag, wordnet.NOUN)


# Transform word tokens to its base form which still makes semantically sense
def lemmatize_nltk(tokens):
    word_lemmas = []
    lemma = WordNetLemmatizer()
    for word in tokens:
        pos_tag = get_wordnet_pos(word)
        word_lemmas.append(lemma.lemmatize(word, pos_tag))
    return word_lemmas


# Remove punctuation, uppercases and tokenize
def tokenize_text(text):
     """
     Function to clean text-remove punctuations, lowercase text etc.    
     """
     tokenizer = RegexpTokenizer(r'\w+')
     text = text.lower() # lower case text
     text = text.strip('.')
     text = ' '.join([word for word in text.split() if word not in stopwords and len(word) > 2])
     tokens = tokenizer.tokenize(text)
     clean_tokens = lemmatize_nltk(tokens)
     return clean_tokens
    

# Remove digits from list
def remove_digits(item_list):
    return [item for item in item_list if not item.isdigit()]

# Remove single letters from list
def remove_single_letter(item_list):
    return [item for item in item_list if len(item) > 2]

# Remove single letters from list
def remove_stopwords(item_list):
    return [item for item in item_list if item not in stopwords]

# List cleaner fucntion
def list_cleaner(item_list):
    removed_digits = remove_digits(item_list)
    stop_free = remove_stopwords(removed_digits)
    clean_list = remove_single_letter(stop_free)
    return clean_list

# %% ----------- SPACY version lemmatize the words ----------

# Transform word tokens to its base form which still makes semantically sense
def lemmatize_spacy(text):
    lemmas = []
    nlp = spacy.load("en_core_web_sm") 
    doc = nlp(text)
    for token in doc:
        if token.text not in stopwords:
            lemmas.append(token.lemma_)
    return lemmas
    

# %% ----------- Pre-processing documents for Gensim dictionary corpus ----------

# Splitting sentences, tokenization, removal of stopwords for a text document.
def clean_doc(text):
    doc_sentences = split_sentences(text) #list of sentences
    sent_clean = [tokenize_text(sentence) for sentence in doc_sentences] #list of lists of tokens for each sentence
    clean_tokens = [list_cleaner(sent) for sent in sent_clean]
    flat_token_list = [item for sub_list in clean_tokens for item in sub_list]
    return flat_token_list

# Split and tokenize text documents
def clean_doc_progress(documents):
    count = 0
    doc_clean_list = []
    tic = time.perf_counter()

    for doc in documents:
        doc_clean_list.append(clean_doc(doc))
        if(count % 500 == 0):
            print("Progress at file: " + str(count))
            toc = time.perf_counter()   
            print(f"Progress of cleaning documents at this point took {toc - tic:0.2f} seconds")    
        count += 1
        
    toc = time.perf_counter()   
    print(f"Progress of cleaning documents into tokens took {toc - tic:0.2f} seconds")    
    return doc_clean_list

def remove_empty_values(token_list):
    res = [ele for ele in token_list if ele != ''] 
    return res

def clean_token_list(token_lists):
    clean_lists = [remove_empty_values(token_list) for token_list in token_lists] 
    return clean_lists


# %% ----------- Compute and add bigrams ----------

# Phrases is Gensim is a statistical analysis model. 
# Potential pairings are given a 'score', and those that score over a configurable 'threshold' are combined.
# Default threshold is 10, but check for value 5
# Default min_count value is 5, but check for 10, 15 and 20

# Create a list of tokenized doucments with unigrams and bigrams
def create_bigrams(doc_terms, num=5, score=10):
    # Add bigrams to docs (only ones that appear num times or more).
    bigram = Phrases(doc_terms, min_count=num, threshold=score)
    for idx in range(len(doc_terms)):
        for token in bigram[doc_terms[idx]]:
            if '_' in token:
                # Token is a bigram, add to document.
                doc_terms[idx].append(token)
    return doc_terms

# Create trigrams from tokenized words and sets
def create_trigrams(doc_terms, bigrams, num=5, score=10):
    # Add bigrams and trigrams to docs (only ones that appear num times or more).
    trigram = Phrases(bigrams[doc_terms], min_count=num, threshold=score)
    for idx in range(len(doc_terms)):
        for token in trigram[doc_terms[idx]]:
            if '_' in token:
                # Token is a bigram, add to document.
                doc_terms[idx].append(token)
    return doc_terms


def create_bigrams_loose(doc_terms, num=5, score=10):
    # Add bigrams and trigrams to docs (only ones that appear num times or more).
    bi_list = []
    bigram = Phrases(doc_terms, min_count=num, threshold=score)
    for doc_term in doc_terms:                
            bigram_sentence = ', '.join(bigram[doc_term])
            for token in bigram_sentence.split(","):
                if "_" in token:
                    bi_list.append(token)
    return bi_list


# %% ----------- Clean the documents ----------

def save_clean_tokens(token_list, name):
    my_df = pd.DataFrame(token_list)
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "dictionaries")  #..\..\software scripts\preprocessed
    my_df.to_csv(os.path.join(save_path, name), index=False, header=False)
    

# %% ----------- Read the clean documents ----------

from csv import reader


def read_clean_tokens(name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "dictionaries")  #..\..\software scripts\preprocessed
    
    # read csv file as a list of lists
    
    with open(os.path.join(read_path, name), 'r', encoding='utf-8') as read_obj:
        # pass the file object to reader() to get the reader object
        csv_reader = reader(read_obj)
        # Pass reader object to list() to get a list of lists
        list_tokens = list(csv_reader)
        
        # User list comprehension to create a list of lists from Dataframe rows
    
    return clean_token_list(list_tokens)



# %% ----------- Create Gensim dictionary corpus ----------

def create_dictionary(tokens, minimum=10, maximum=0.5):
    # Creating the term dictionary of our corpus, where every unique term is assigned an index. 
    dictionary = corpora.Dictionary(tokens)
    # Filter terms which occurs in less than min articles & more than max 50%) of the articles
    dictionary.filter_extremes(no_below=minimum, no_above=maximum)
    return dictionary

def create_doc_term_matrix(dictionary, token_list):
    # Converting list of documents (corpus) into Document Term Matrix using dictionary prepared above.
    doc_term_matrix = [dictionary.doc2bow(sent) for sent in token_list]
    return doc_term_matrix
    


# %% ----------- Create LDA model ----------

def create_LDA_model(dictionary, doc_term_matrix, num=20, 
                     a="auto", b = "auto", random_num=100, 
                     pass_num=1, iters=100):
    
    # Creating the object for LDA model using gensim library
    Lda = gensim.models.ldamodel.LdaModel
    
    ldamodel = Lda(doc_term_matrix, id2word=dictionary, alpha = a, eta= b, num_topics=num, 
                   random_state=random_num , passes=pass_num, iterations=iters, chunksize = 2000)
    return ldamodel


# %% ----------- Check the results of LDA models ----------

def check_topics(ldamodel, num=10, num_words=20):
    topics = ldamodel.print_topics(num_topics=num, num_words=num_words)
    print(topics)
    return topics

def check_top_topics(ldamodel, doc_term_matrix, num =10):
    top_topics = ldamodel.top_topics(doc_term_matrix, topn=num)
    print(top_topics)
    return top_topics


# %% ----------- Classification function for Gensim LDA model ----------

# Get the topic ID of a tuple topic ID and probability
def get_topic_by_groupID(tuple_list):
    return max(tuple_list,key=itemgetter(1))[0]

# Get the probability score of a tuple of topic ID and probability
def get_topic_by_probability(tuple_list):
    return max(tuple_list,key=itemgetter(1))[1]

# The function takes an Gensim LDA model, dictionary and plain text document to determine
# which topic group is assigned to the plain text document (one long string)
def classify_topic_gensim(model, dictionary, doc):
    score_list = model.get_document_topics(dictionary.doc2bow(doc.split()))
    return [get_topic_by_groupID(score_list), get_topic_by_probability(score_list)]


# The function takes an Gensim LDA model, dictionary and plain text document to show
# the topic group probability distribution of the plain document (one long string). 
def show_topic_prob_distribution(model, dictionary, doc, printing = False):
    score_list = model.get_document_topics(dictionary.doc2bow(doc.split()))
    if printing == True:
        print(score_list)
    return score_list


# Retrieve the topic with the highest assigned probability from a list of tuples
# consisting of a (topic, probability) of a single document
# Input is the output of the function show_topic_prob_distribution
def get_most_probable_topic(tuple_list):
    if tuple_list != []:
        return max(tuple_list,key=itemgetter(1))[0]
    else:
        return []

#
def get_most_probable_topic_by_threshold(tuple_list, threshold):
    if tuple_list != []:
        best_topic = max(tuple_list,key=itemgetter(1))
        if best_topic[1] > threshold:
            return best_topic[0]
        else:
            return []
    else:
        return []

# Retrieve the topic terms for specific topic group by ID number
def get_topic_terms_of_group(ldamodel, dictionary, group_id, printing = False):
    topic_terms = []
    topic_group = ldamodel.get_topic_terms(group_id)
    for lda_tuple in topic_group:
        dic_num = lda_tuple[0]
        term = dictionary[dic_num]
        topic_terms.append(term)
    if printing == True:       
        print("Topic group ID:" + str(group_id))
        print(topic_terms)
    return topic_terms


# %% ----------- Classification function for Gensim LDA model ----------

def save_pd_topics(df, name, directory):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, directory)  #..\..\software scripts\preprocessed
    os.chdir(save_path)
    df.to_pickle(name + ".pkl")

def get_topic_terms_per_group(ldamodel, dictionary):
    topic_group = []
    for i in range(0, ldamodel.num_topics):
        topic_words = get_topic_terms_of_group(ldamodel, dictionary, i)
        topic_group.append([i, topic_words])
    colnames = ["Topic ID", "Topic features"]
    df = pd.DataFrame(topic_group, columns = colnames)
    return df 

        
# %% ----------- Check the topic distribution and terms for each document ----------

def create_names(docs):
    index_names = []
    for i in range(0,len(docs)):
        index_names.append("doc" + str(i))
    return index_names

def create_lda_datarow(doc, ldamodel, dictionary):
    #doc = " ".join(doc)
    topic_dist = show_topic_prob_distribution(ldamodel, dictionary, doc)
    #classified_topic = classify_topic_gensim(ldamodel, dictionary, doc)
    highest_prob_topic = get_topic_by_probability(topic_dist)
    topic_ID = get_topic_by_groupID(topic_dist)
    topic_terms = get_topic_terms_of_group(ldamodel, dictionary, topic_ID)
    return [topic_dist, topic_ID, highest_prob_topic, topic_terms]

def create_lda_subj_datarow(subj_sents_list, ldamodel, dictionary):
    topic_group = []
    for subj_sent in subj_sents_list:
        classified_topic = classify_topic_gensim(ldamodel, dictionary, subj_sent)
        highest_prob_topic = classified_topic[1]
        topic_terms = get_topic_terms_of_group(ldamodel, dictionary, classified_topic[0])
        topic_group.append([classified_topic[0], highest_prob_topic, topic_terms])
    return topic_group
        

def create_lda_df(docs, ldamodel, dictionary):
    index_names = create_names(docs)
    data = [create_lda_datarow(doc, ldamodel, dictionary) for doc in docs]
    colnames = ["Topic distribution", "Classified group", "Highest prob. topic", "Topic terms"]
    df = pd.DataFrame(data, index=index_names, columns = colnames)
    return df

                                                        

# %% ----------- Visualise of LDA with py LDA vis----------

import pyLDAvis
import pyLDAvis.gensim 
pyLDAvis.enable_notebook()


def create_vis(ldamodel, doc_term_matrix, dictionary):
    vis = pyLDAvis.gensim.prepare(ldamodel, doc_term_matrix, dictionary)
    return vis

#Save LDA models into correct folder
def save_model(model, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "LDA")  #..\..\software scripts\preprocessed
    model.save(os.path.join(save_path, name + '.model'))


#Save LDA models into correct folder
def save_ldamodel(model, vis, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "LDA")  #..\..\software scripts\preprocessed
    pyLDAvis.save_html(vis, os.path.join(save_path, name + '.html'))
    model.save(os.path.join(save_path, name + '.model'))


# later on, load trained model from file
def load_LDA(name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "LDA")  #..\..\software scripts\preprocessed
    os.chdir(read_path)
    model =  models.LdaModel.load(name + ".model")
    return model


# %% ----------- Coherence score ----------

# Average topic coherence is the sum of topic coherences of all topics, divided by the number of topics.
def get_average_topic_coherence(ldamodel, corpus):
    top_topics = ldamodel.top_topics(corpus)
    num_topics = ldamodel.num_topics
    avg_topic_coherence = sum([t[1] for t in top_topics]) / num_topics
    return avg_topic_coherence


def parse_topic_words(topic):
    # takes the string returned by model.show_topics()
    # split on strings to get topics and the probabilities
    topic = topic.split('+')
    # list to store topic bows
    topic_bow = []
    for word in topic:
        # split probability and word
        prob, word = word.split('*')
        # get rid of spaces and quote marks
        word = word.replace(" ","").replace('"', '')
        topic_bow.append(word)
        
    return topic_bow


def words_per_topics(list_topic_tuples):
    dic = {}
    for item in list_topic_tuples:
        key = item[0]
        dic[key] = parse_topic_words(item[1])
    return dic


# %% ----------- function to compare different models and to select the best topic size----------

# Hyperparameter tuning
# Check passes = [1, 5, 10] - Also called Epochs
# Check iterations = [50, 100, 200, 400]
# Chunk_size = [500, 1000, 2000]

# Text should be tokenized text

def compute_coherence_values(dictionary, corpus, texts, limit, start=2, step=3, pass_num = 1, 
                             iters = 50, chunk = 2000):
    """
    Compute u_mass coherence for various number of topics

    Parameters:
    ----------
    dictionary : Gensim dictionary
    corpus : Gensim corpus
    texts : List of input texts
    limit : Max num of topics

    Returns:
    -------
    model_list : List of LDA topic models
    coherence_values : Coherence values corresponding to the LDA model with respective number of topics
    """
    Lda = gensim.models.ldamodel.LdaModel
    coherence_values = []
    model_list = []
    perplexity_list = []
    
    for num_topics in range(start, limit, step):
        print("Process started for model with num_topics:", num_topics)
        model=Lda(corpus=corpus, id2word=dictionary, num_topics=num_topics, alpha='auto', eta='auto', 
                  passes = pass_num, iterations = iters, chunksize = chunk, random_state=100)
        model_list.append(model)
        coherencemodel = CoherenceModel(model=model, texts=texts, dictionary=dictionary, coherence='u_mass')
        coherence_values.append(coherencemodel.get_coherence())
        perplexity_list.append(model.log_perplexity(corpus))

    return model_list, coherence_values, perplexity_list


# %% ----------- Get a wordcoud of the topics of LDA model ----------

from wordcloud import WordCloud


def create_wordcloud(ldamodel):
    for t in range(ldamodel.num_topics):
        plt.figure(figsize=(20,10))
        plt.imshow(WordCloud(width=800, height=400, background_color="white").fit_words(dict(ldamodel.show_topic(t, 30))))
        plt.axis("off")
        plt.title("Topic #" + str(t))
        plt.show()
  
    
def display_wordcloud(lda_model):
    fig = plt.figure(figsize=(20,10))
    j = np.ceil(lda_model.num_topics/4)
    for t in range(lda_model.num_topics):
        i=t+1
        plt.subplot(j, 4, i).set_title("Topic #" + str(t))
        plt.plot()
        plt.imshow(WordCloud(width=600, height=175, background_color="white").fit_words(dict(lda_model.show_topic(t, 30))))
        plt.axis("off")
    plt.tight_layout()
    plt.show()



#%%

import pickle

def save_models_pickle(models, cvs, perplexity, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "LDA_models")  #..\..\software scripts\preprocessed
    os.chdir(save_path)
    
    with open(name + ".models", "wb") as f:
        pickle.dump(models, f)
        
    with open(name + ".cvs", "wb") as g:
        pickle.dump(cvs, g)
    
    with open(name + ".perps", "wb") as h:
        pickle.dump(perplexity, h)
        
def load_models_pickle(name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "LDA_models")  #..\..\software scripts\preprocessed
    os.chdir(save_path)
    
    with open(name + ".models", "rb") as f:
        models = pickle.load(f)
        
    with open(name + ".cvs", "rb") as g:
        cvs = pickle.load(g)
        
    with open(name + ".perps", "rb") as h:
        perps = pickle.load(h)
        
    return models, cvs, perps






