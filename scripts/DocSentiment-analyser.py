# -*- coding: utf-8 -*-
"""
Created on Mon May 11 14:29:51 2020

@author: Debby Leijen-Lam



Make use of models and dictionaries that contain sentiment values for sentiment words. 

"""

# %% ----------- Import libraries -----------
import nltk   
from nltk import tokenize
from nltk.tokenize import sent_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer 
from nltk.stem.snowball import SnowballStemmer
from nltk import FreqDist, pos_tag
from nltk.sentiment.vader import SentimentIntensityAnalyzer

import spacy
import textacy
import en_core_web_sm
spacy.load('en')
nlp = en_core_web_sm.load()

from spacy import displacy
from IPython.core.display import display, HTML

from textblob import TextBlob
import os
from os.path import dirname, abspath
import collections, functools, operator 

from itertools import cycle
import pandas as pd
import pickle

import importlib
#importlib.import_module("DocLDA")
import json
import os
from os.path import dirname, abspath


# %% ----------- TextBlob sentiment test ----------

def get_subjective_sents(text, subj_value=0.4):
    blob = TextBlob(text)
    subject_sents = []
    for sentence in blob.sentences:
        if sentence.sentiment.subjectivity > subj_value:
            subject_sents.append(sentence.string)
    return subject_sents


def get_sentiment_polarity_score(sentence_list):
    sentiment_list = []
    for sentence in sentence_list:
        blob_sent = TextBlob(sentence)
        sentiment_list.append(blob_sent.sentiment.polarity)
    return sentiment_list
    

def get_avgscore_subjectivity(blob):
    total_score = 0
    try:
        for sentence in blob.sentences:
            total_score += sentence.sentiment.subjectivity
            return total_score/len(blob.sentences)
    except ZeroDivisionError:
        return None

def get_avgscore_polarity(blob):
    total_score = 0
    try:
        for sentence in blob.sentences:
            total_score += sentence.sentiment.polarity
            return round(total_score/len(blob.sentences), 3)
    except ZeroDivisionError:
        return None

# Get the average sentiment score for the whole text
def get_avg_sentiment(text):
    blob = TextBlob(text)
    return get_avgscore_polarity(blob)

# Get the average polarity and subjectivity score of the text
def get_avg_sentiment_values(text):
    blob = TextBlob(text)
    avg_polarity = get_avgscore_polarity(blob)
    avg_subj = get_avgscore_subjectivity(blob)
    return (avg_polarity, avg_subj)
    

            
# %% ----------- Get the subjective sentences for each doc ----------

def get_average_sentiment(docs_list):
    avg_sentiment_docs = []
    for doc in docs_list:
        if doc != []:
            avg_sentiment_docs.append(sum(doc)/len(doc))
        else:
            avg_sentiment_docs.append(0)
    return avg_sentiment_docs


# %% ----------- General item saver ----------

def write_to_pickle(items, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "sentiment-topic-results")  #..\..\software scripts\preprocessed
    os.chdir(save_path)
    with open(name, 'wb') as fp:
        pickle.dump(items, fp)

def read_pickle(name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "sentiment-topic-results")  #..\..\software scripts\preprocessed
    os.chdir(save_path)
    with open (name, 'rb') as fp:
        itemlist = pickle.load(fp)
    return itemlist


def flatten_subj_sentences(sentences_list):
    return ''.join(sentences_list)
    

# %% ----------- Combine the document and sentiment score ----------

# Combine the sentiment scores of an article to the document with topic. 
# Takes the output of the function get_most_probable_topic. 
def link_topic_sentiment(list_doc_topics, list_sentiment_scores):
    return tuple(zip(list_doc_topics, list_sentiment_scores))


# %% ----------- Create Pandas DF  ----------

def create_topic_dist(subj_docs, ldamodel, dictionary):
    doc_ids = create_doc_id(subj_docs)
    subs_sents = [item for sublist in subj_docs for item in sublist]
    top_distributions_subj_sents = [DocLDA.show_topic_prob_distribution(ldamodel, dictionary, doc) for doc in subs_sents]
    df = pd.DataFrame(doc_ids, columns = ["DocID"])
    df["Sentence"] = subs_sents
    df["Topic distribution"] = top_distributions_subj_sents
    df.to_excel("Topic_distribution_per_subjective_sentence.xlsx")
    return df
    
def create_doc_id(subj_sents):
    doc_id = []
    for i in range(0, len(subj_sents)):
        for sent in subj_sents[i]:
            doc_id.append("doc" + str(i))
    return doc_id


def create_subj_df(subj_docs, ldamodel, dictionary):
    doc_ids = create_doc_id(subj_docs)
    sentiment_polarity_docs = [get_sentiment_polarity_score(sent_list) for sent_list in subj_docs]
    sentiment_score = [item for sublist in sentiment_polarity_docs for item in sublist]
    single_sents = [item for sublist in subj_docs for item in sublist]
    data = [create_lda_subj_datarow(doc, ldamodel, dictionary) for doc in subj_docs]
    data = [item for sublist in data for item in sublist]
    colnames = ["Topic group", "Probability", "Topic terms"]
    df = pd.DataFrame(data, columns = colnames)
    df["DocId"] = doc_ids
    df["Sentence"] = single_sents
    df["Sentiment Score"] = sentiment_score
    return df


def save_pd_sent(df, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "sentiment-topic-results")  #..\..\software scripts\preprocessed
    os.chdir(save_path)
    df.to_pickle(name + ".pkl")
    print("Pickle convertion complete!")
    
    
def convert_excel(df, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "sentiment-topic-results")  #..\..\software scripts\preprocessed
    os.chdir(save_path)
    df.to_excel(name)
    print("Excel convertion complete!")


# %% ----------- Save pandas dataframe functions ----------

def save_df_to_excel(df, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "sentiment-topic-results")  
    os.chdir(save_path)
    df.to_excel(name + ".xlsx")
    
def read_df_excel(name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "sentiment-topic-results")  #..\..\software scripts\preprocessed
    df = pd.read_excel(os.path.join(read_path, name), index_col=0)
    return df
    

# %% ----------- Calculating the with DF  ----------

# Get the average sentiment score for each topic groep
def get_mean_sent_per_topic(df, name):
    grouped_doc = df.groupby(['DocId', 'Topic group'])
    x = grouped_doc.mean()
    save_df_to_excel(x, name)
    return x

# Normalize the probability for each topic within each individual Document
def normalize_df(df, name):
    grouped_doc = df.groupby(['DocId', 'Topic group'])
    x = grouped_doc.mean()
    sum_probs = x.groupby(["DocId"])["Probability"].sum()
    normalized_prob = x["Probability"]/sum_probs
    x["Probability"] = normalized_prob
    x = x.round(4)
    save_df_to_excel(x, name)
    return x


#%% Preprocesses the output of the sentiment-analyser file and convert them into JSON formatted files

# Get the topics of a ldamodel
def get_topics_ldamodel(ldamodel, num_tops=20, num_words=30):
    topics = ldamodel.show_topics(num_tops, num_words)
    return topics

# Convert a tuple into a dictionary
def Convert(tup, di): 
    di = dict(tup) 
    return di 

# Create a dictionary of words and percentages from all the topics of a ldamodel 
def create_topics_dictionary(ldamodel):
    top_dict = {}
    topics = get_topics_ldamodel(ldamodel, ldamodel.num_topics, num_words=30)
    return Convert(topics, top_dict)

# Function to create docIDs based on a list of sentimented topic tuples
def create_docIDs(list_sentimented_topic_tuples):
    names = []
    for i in range(len(list_sentimented_topic_tuples)):
        names.append("Doc" + str(i))
    return names

# Combine the docID with the sentimented topic
def create_named_tuples(list_sentimented_topics, names):
    return list(zip(list_sentimented_topics, names))

# Create annotated sentimented topics, link the docID with sentimented topic
# Takes the output of the function link_topic_sentiment as input
def create_annotated_sentimented_topics(list_sentimented_topics):
    names =  create_docIDs(list_sentimented_topics)
    return create_named_tuples(list_sentimented_topics, names)

# consisting of (topic, sentiment score) and dictionary of topics.
def create_dict_tuples_sentimented_topics(sentimented_topic, top_dic):
    dic = {}
    dic["topic_id"] = sentimented_topic[0]
    dic["topic_words"] = top_dic[sentimented_topic[0]]
    dic["sentiment"] = sentimented_topic[1]
    return dic

# Create a dictionary from lists of annotated sentiment sentences
# Takes the output of function create_annotated_sentimented_topics as input
def create_dict_data_lists(annotated_sentimented_topics, top_dic):
    new_dic = {}
    for tup in annotated_sentimented_topics:
        key = tup[1]
        if tup[0][0] != []:
            new_dic[key] = create_dict_tuples_sentimented_topics(tup[0], top_dic)
    return new_dic
        

################################################################################
######### Dataframe functions #####################################

# Specific function to extract the sent_tops from dataframe
# Extract the sentiment scores for each associated topics and create a vector for it 
# consisting of ([(topic, sentiment score),...], docID), sentimented topics and docID.
# Creates for each document multitude of sentimented topics
def get_sentimented_topic(data_df):
    sent_top_list = []
    temp_sent_top = []
    temp_doc_num = data_df.iloc[0].name[0]
    for i in range(len(data_df) - 1):
        doc_num = data_df.iloc[i].name[0]
        topic_num = data_df.iloc[i].name[1]
        sentiment_score = data_df.iloc[i]["Sentiment Score"]
        if temp_doc_num == doc_num:
            temp_sent_top.append((topic_num, sentiment_score))
            temp_doc_num = doc_num
        else:
            sent_top_list.append((temp_sent_top, temp_doc_num))
            temp_sent_top = [(topic_num, sentiment_score)]
            temp_doc_num = doc_num
        
    return sent_top_list

        

#Create a dictionary for each sentimented topic by saving the topic_id, topic_words and topic_sentiment
def create_dict_sentimented_topic(sentimented_topic, top_dic):
    dic = {}
    dic["topic_id"] = sentimented_topic[0].item()
    dic["topic_words"] = top_dic[sentimented_topic[0]]
    dic["sentiment"] = sentimented_topic[1].item()
    return dic

# From a list of documents of [sentimented topics, docID], create a dictionary
def create_dict_doc(doc, top_dic):
    new_dic = {}
    sent_top_list = []
    for item in doc[0]:
        entry = create_dict_sentimented_topic(item, top_dic)
        sent_top_list.append(entry)
    new_dic[doc[1]] = sent_top_list
    return new_dic


def create_dict_list_docs(docs, ldamodel):
    dic = {}
    top_dic = {}
    topics = get_topics_ldamodel(ldamodel, ldamodel.num_topics, num_words=50)
    top_dic = Convert(topics, top_dic)
    for doc in docs:
        entry = create_dict_doc(doc, top_dic)
        dic.update(entry)
    return dic
        
# create a JSON file based on a dictionary
def create_json_file(dic_data, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "data")  #..\..\software scripts\word-docs 
    with open(os.path.join(save_path, name + '.json'), 'w', encoding='utf-8') as f:
        json.dump(dic_data, f, ensure_ascii=False, indent=4)
        
#Read a JSON file
def read_json_file(file_name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "data")  #..\..\software scripts\word-docs 
    with open(os.path.join(read_path, file_name)) as f:
        data = json.load(f)
    return data

#If each document contains at most 1 sentimented topic
def get_sent_tops_json(dic):
    sent_tops_list = []
    keys = [*dic]
    for key in keys:
        sent_tops_list.append(((dic[key]["topic_id"], dic[key]["sentiment"]), key))
    return sent_tops_list
        

#If each document contains at most 1 sentimented topic, get all sentimented topics with docID
def get_annotated_sent_tops_json(dic):
    sent_tops_list = []
    keys = [*dic]
    for key in keys:
        sent_tops_list.append(((dic[key]["topic_id"], dic[key]["sentiment"]), key))
    return sent_tops_list


#If each document contains at most 1 sentimented topic
def get_all_topics_json(dic):
    tops_list = []
    keys = [*dic]
    di = {}
    for key in keys:
        tops_list.append((dic[key]["topic_id"], dic[key]["topic_words"]))
    return Convert(tops_list, di)


# Get the sentimented topics fromt he JSON file
def get_sentimented_topics_json(dict_elem):
    sent_tops_list = []
    for item in dict_elem:
        sent_tops_list.append((item["topic_id"], item["sentiment"]))
    return sent_tops_list


#If each document contains at more than 1 sentimented topic
def get_topics_json(dict_elem):
    tops_list = []
    di = {}
    for item in dict_elem:
        tops_list.append((item["topic_id"], item["topic_words"]))
    return Convert(tops_list, di)

#If each document contains at more than 1 sentimented topic
def get_all_multiple_topics_json(dic):
    keys = [*dic]
    topic_dic = {}
    for key in keys:
        topic_dic.update(get_topics_json(dic[key]))
    return topic_dic

    
