# -*- coding: utf-8 -*-
"""
Created on Thu May 21 14:58:22 2020

@author: Debby Leijen-Lam
"""

import matplotlib.pyplot as plt 
import numpy as np

# %% ----------- Execute preprocessing part ----------

print("Start formatting the text documents...")
automatic_preprocess()

# %% ----------- Execute preprocessing part ----------

print("Start deleting unnecessary files...")
#Remove documents equal or smaller than 1Kb and greater dan 100kB and titles containing live blogs or timelines
delete_preprocessed_files_bytes(1024, "less")
delete_preprocessed_files_bytes(102400, "greater")
delete_preprocessed_timeline_liveblogs()
delete_preprocessed_language()

print("Deletion is complete!")


# %% ----------- Execute preprocessing part ----------

tic = time.perf_counter()

docs = read_preprocessed()

print("Start preprocessing the text documents into tokens...")
id_and_docs = list(zip(*docs))
idnames = [*id_and_docs[0]]
text = [*id_and_docs[1]]

doc_clean_set = clean_doc_progress(text) 
doc_clean_set = clean_token_list(doc_clean_set)
doc_clean_set_bigrams = create_bigrams(doc_clean_set)
print("Tokenization complete!")

name = input("Type in the name of the tokenized files: ")
save_clean_tokens(doc_clean_set_bigrams, name + ".csv")
save_clean_tokens(idnames, name + "idnames_bigrams.csv")

print("Preprocessing complete!")
toc = time.perf_counter()  
print("Progress of performing pre-processing took", round((toc - tic), 2), "seconds") 

# %% ----------- Time performance ----------

docs = read_preprocessed()


# %% ----------- different document sizes ----------

big_docs = [tex for tex in text if len(tex.split()) > 1200]
middle_docs = [tex for tex in text if len(tex.split()) > 500 and len(tex.split()) < 1200]
small_docs = [tex for tex in text if len(tex.split()) < 500]


# %% ----------- different document sizes ----------

def duplicate(testList, n):
    new_list = [] 
    for i in range(n):
        new_list.extend(testList)
    return new_list

bigs = duplicate(big_docs, 6)[:7500]
middles = duplicate(middle_docs, 2)[:7500]
smalls = duplicate(small_docs, 8)[:7500]


# %% ----------- Time performance ----------

# Split and tokenize text documents
def clean_doc_progress_time(documents):
    count = 0
    doc_clean_list = []
    test_time = []
    count_docs = []
    tic = time.perf_counter()

    for doc in documents:
        doc_clean_list.append(clean_doc(doc))
        if(count % 500 == 0):
            print("Progress at file: " + str(count))
            toc = time.perf_counter()   
            print(f"Progress of cleaning documents at this point took {toc - tic:0.2f} seconds")
            test_time.append(round((toc-tic),2))
            count_docs.append(count)
        count += 1
        
    toc = time.perf_counter()   
    print(f"Progress of cleaning documents into tokens took {toc - tic:0.2f} seconds")    
    test_time.append(round((toc-tic),2))
    count_docs.append(count)
    return doc_clean_list, test_time, count_docs

tic = time.perf_counter()

doc_clean_set, time_stamps, count = clean_doc_progress_time(text) 
doc_clean_set = clean_token_list(doc_clean_set)
doc_clean_set_bigrams = create_bigrams(doc_clean_set)
print("Tokenization complete!")

toc = time.perf_counter()  
print("Progress of performing pre-processing took", round((toc - tic), 2), "seconds") 


# %% ----------- Time performance ----------

#all mixed well, (7500 docs)-3529.06- (7552)- 3659.06 - 3691.1 - tokenization + bigrams
x = list(proc["col1"])
y = list(proc["col2"])

#small documents - 799.49, 861.32 - tokenization + bigrams
x2 = list(proc["col1"])
y2 = list(proc["col4"])

#medium documents 1979.18 - 2110.19s - tokenization + bigrams
x3 = list(proc["col1"])
y3 = list(proc["col6"])

#big documents 8617.40s - 9194.87s - tokenization + bigrams
x4 = list(proc["col1"])
y4 = list(proc["col8"])


plt.plot(x, y, label = "Mixed sizes") 
plt.plot(x2, y2, label = "Small documents") 
plt.plot(x3, y3, label = "Middle documents") 
plt.plot(x4, y4, label = "Big documents") 

plt.xticks(np.arange(0, 8000, 500))
plt.yticks(np.arange(0, 9500, 500))
  
# naming the x axis 
plt.xlabel('Amount of documents') 
# naming the y axis 
plt.ylabel('Time in s') 
# giving a title to my graph 
plt.title('Performance of the pre-processing of different sized documents') 
  
# show a legend on the plot 
plt.legend() 
  
# function to show the plot 
plt.show() 

# %%--- Execute the code LDA

name = input("Insert the name of the tokens file: ")

doc_names_bigrams = read_clean_tokens(name + 'idnames_bigrams.csv')
doc_names_bigrams = [item for sublist in doc_names_bigrams for item in sublist]
doc_clean_bigrams = read_clean_tokens(name + '.csv')
doc_clean_bigrams = clean_token_list(doc_clean_bigrams)

print("Loading tokens complete!")


print("Creating dicitonary and Document-Term-Matrix...")
dictionary = create_dictionary(doc_clean_bigrams)
doc_term_matrix = create_doc_term_matrix(dictionary, doc_clean_bigrams)

num_tops = input("Insert the number of topics to find: ")
num_tops = int(num_tops)

ldamodel = create_LDA_model(dictionary, doc_term_matrix, num=num_tops, pass_num= 3, iters=100)

model_name = input("Insert the name of the LDA model: ")
save_model(ldamodel, model_name)
save_item(dictionary, model_name +'_dictionary.p')
save_item(doc_term_matrix, model_name + "_dtm.p")
print("LDA model saved!")

tups_probs = [show_topic_prob_distribution_tokens(ldamodel, dictionary, doc) for doc in doc_clean_bigrams]

condition = input("Do you want to get (1) the most probable topic or (2) get topics by percentage? (Insert 1 or 2): ")
condition = int(condition)

if (condition == 1):
    tops = [get_most_probable_topic(tup) for tup in tups_probs]
    assigned_tops = list(zip(doc_names_bigrams, tops))
else:
    perc = input("Enter percentage between 0.1 and 0.9: ")
    perc = float(perc)
    tops = [get_most_probable_topic_by_threshold(tup, perc) for tup in tups_probs]
    assigned_tops = list(zip(doc_names_bigrams, tops))
    
    
print("Assigning topics to documents complete!")

save_name = input("Insert the name to save the file of topic assigned documents: ")
save_assigned_topics(assigned_tops, save_name)
     
print("Save complete!")

doc_names_bigrams = read_clean_tokens("tokens7000" + 'idnames_bigrams.csv')
doc_names_bigrams = [item for sublist in doc_names_bigrams for item in sublist]
doc_clean_bigrams = read_clean_tokens("tokens7000" + '.csv')
doc_clean_bigrams = clean_token_list(doc_clean_bigrams)


#%% ###################### Code execution of the SentTopGenerator file ###############

sent_condition = input("Find multiple sentimented topics per document? (Enter yes): ")

documents = read_preprocessed()
documents = sorted(documents)
id_and_docs = list(zip(*documents))
idnames = [*id_and_docs[0]]
docs = [*id_and_docs[1]]

if sent_condition == "yes":
    ldamodel_name = input("Enter the name of LDA model you wish to use: ")
    subj_docs = [get_subjective_sents(doc, 0.5) for doc in docs]
    ldamodel = load_LDA(ldamodel_name)
    dictionary = load_item(ldamodel_name +'_dictionary.p')
    doc_term_matrix = load_item(ldamodel_name + "_dtm.p")
    sentiment_polarity_docs = [get_sentiment_polarity_score(sent_list) for sent_list in subj_docs]

    sentence_tops = []
    for subj_list in subj_docs:
        tups_probs = [show_topic_prob_distribution(ldamodel, dictionary, doc) for doc in subj_list]
        topics_per_docs = [get_most_probable_topic(tuple_list) for tuple_list in tups_probs]
        sentence_tops.append(topics_per_docs)
    
    sent_tops_docs = []
    for i in range(len(sentence_tops)):
        sent_tops_docs.append(list(zip(sentence_tops[i], sentiment_polarity_docs[i])))
    
    test = []
    for row in sent_tops_docs:
        avg_sent_top_docs = []
        count_dic = Counter(elem[0] for elem in row)
        total_sent_tops = sum_sent_tops(row)        
        for item in total_sent_tops:
            avg_sent_top_docs.append((item[0], (item[1]/count_dic[item[0]])))
        
        test.append(avg_sent_top_docs)
    
    assigned_sent_tops_docs = list(zip(test, idnames))
    
    print("Start converting sentimented topics into JSON files...")
    dic_data = create_dict_list_docs(assigned_sent_tops_docs, ldamodel)
    
    name_json = input("Enter name of JSON file: ")
    create_json_file(dic_data, name_json + "_multiple_senttops")
    
    print("JSON convertion complete!")
    
else:
    #Checking time | Check the performance by the amount of documents to assign the sentimented topics
    doc_name = input("Enter the name of the file with topic assigned documents: ")  
    
    for i in range(0,100):
        while True:
            try:
                st = read_assigned_topics(doc_name)
                st = sorted(st)
                break 
            except Exception:
                print("Try again with different name")
                doc_name = input("Enter the name of the file with topic assigned documents: ")
                continue
    
    st_loose = list(zip(*st))
    idnames = [*st_loose[0]]
    topics = [*st_loose[1]]
    
    subj_docs = [get_subjective_sents(doc, 0.5) for doc in docs]
    avg_textblob = [get_avg_sentiment_values(text) for text in docs]
    sentiment_polarity_docs = [get_sentiment_polarity_score(sent_list) for sent_list in subj_docs]
    avg_sentiment_docs = get_average_sentiment(sentiment_polarity_docs)
    
    sent_tops = link_topic_sentiment(topics, avg_sentiment_docs)

    save_condition = input("Save sentimented topics in a pickle file? (Enter yes): ")

    if save_condition == "yes":
        name_pickle = input("Enter the name of the pickle file: ")
        id_sent_tops = link_topic_sentiment(idnames, sent_tops)
        write_to_pickle(id_sent_tops, name_pickle)
    
    print("Start converting sentimented topics into JSON files...")
    
    ldamodel_name = input("Enter the name of LDA model used for the topic assignment: ")
    
    for i in range(0,100):
        while True:
            try:
                ldamodel = load_LDA(ldamodel_name)
                break 
            except Exception:
                print("Try again with different name")
                ldamodel_name = input("Enter the name of LDA model used for the topic assignment: ")
                continue

    
    topics = create_topics_dictionary(ldamodel)
    id_sent_tops = create_named_tuples(sent_tops, idnames)
    dic_data = create_dict_data_lists(id_sent_tops, topics)
    
    name_json = input("Enter name of JSON file: ")
    create_json_file(dic_data, name_json)
    
    print("JSON convertion complete!")
    
    # %% ----------- Running the code Selection -----------


path = dirname(abspath(__file__)) 
parent_dir = dirname(path) 
destination_path = os.path.join(parent_dir, "sent-tops")  
files = os.listdir(destination_path)

k = input("Type an integer of k to return points (min 2): ")
name = input("Type the name of the directory for results: ")

question = input("Do you want to use docId or sentimented topic? (doc or st): ")

if (question == "doc"):
    num = input("Type the docID to find points (number): ")
    num = int(num)
else:
    senttop = input("Fill in a sent_top (e.g. (1, 0.2)): ")


for dat in files:
    k = int(k)
    data = read_json_file(dat)
    topics = get_all_topics_json(data)
    sent_tops = get_annotated_sent_tops_json(data)
    sent_tops = sorted(sent_tops, key=lambda x: x[1])
    unique = get_unique_sentimented_topics_singles(sent_tops)
    print("There are", len(unique), "unique points.")
    
    if (question == "doc"):
        q = sent_tops[num][0]
    else: 
        q = eval(senttop)
        
    tic = time.perf_counter()
    print("Start of finding candidates...")
    cond = input("Do you want to input min and max bound of points? (enter yes) ")
    #cond = "no"
    
    if cond == "yes":
        min_d = input("Input the minimum point distance (between 0.1 and 0.9) ")
        max_d = input("Input the maximum point distance (between 0.1 and 0.9) ")
        min_d = eval(min_d)
        max_d = eval(max_d)
        candidates = get_candidates_query_point(q, unique, topics, k, min_d, max_d) 
    else:
        candidates = get_candidates_query_point(q, unique, topics, k)   
        
    ipfs = calculateIPFs(candidates)
    print("Commencing finding best set of",  k, "points ...")
    best = cumulForceSelection(candidates, k, ipfs)
    toc = time.perf_counter()  
    print("Progress of performing selection took", round((toc - tic), 2), "seconds") 
    
    time_s = toc - tic
    directory = dirname(abspath(__file__))
    parent_dir = dirname(directory)
    
    file_path = os.path.join(parent_dir, "results")
    
    if not os.path.isdir(file_path):
        os.mkdir(file_path)
    
    file_path = os.path.join(file_path, name)
    
    
    if not os.path.isdir(file_path):
        os.mkdir(file_path)
    
    with open(os.path.join(file_path, dat + "_best_combination_doc.txt"), 'w', encoding="utf-8") as output:
        output.write("Query is:", )
        output.write(str(q))
        output.write("\n")
        output.write("Topic words:")
        output.write("\n")
        output.write(str(parse_topic_words(topics[q[0]])))
        output.write("\n")
        output.write("Size of the combinations is:")
        output.write("\n")
        output.write(str(k))
        output.write("\n")
        output.write("Best combination is:")
        output.write(str(best))
        output.write("\n")
        
        
        for pair in best:
            output.write("Topic words for topic " + str(pair[0]))
            output.write(str(parse_topic_words(topics[pair[0]])))
            output.write("\n")
        
        output.close()
    
    name = input("Enter name for the result file: ")    
    
    read2_path = os.path.join(parent_dir, "result")
    
    if not os.path.isdir(read2_path):
        os.mkdir(read2_path)
    
    with open(os.path.join(read2_path, str(k) +  dat + "_best_combination.txt"), 'w', encoding="utf-8") as output:
        output.write(str(best))
        output.close()
    
    with open(os.path.join(file_path, dat + "_stats.txt"), 'w', encoding="utf-8") as output:
        output.write("Document name is:", )
        output.write(str(sent_tops[num][1]))
        output.write("\n")
        output.write("Query is:", )
        output.write(str(q))
        output.write("\n")
        output.write("Time running script in seconds: ")
        output.write(str(time_s))
        output.write("\n")
        output.write("Combination size of k: ")
        output.write(str(k))
        output.write("\n")
        output.write("Amount of total points: ")
        output.write(str(len(unique)))
        output.write("\n")
        output.write("Amount of candidates: ")
        output.write(str(len(candidates)))
        
# %% ----------- Execute code for docFinder-----------
        
path = dirname(abspath(__file__)) #..\..\software scripts\scripts
parent_dir = dirname(path) #..\..\software scripts
read_path = os.path.join(parent_dir, "sent-tops")  #..\..\software scripts\preprocessed
files = os.listdir(read_path)

read2_path = os.path.join(parent_dir, "result")  #..\..\software scripts\preprocessed
    
if not os.path.isdir(destination_path):
    os.mkdir(destination_path)
    
data_files = os.listdir(read2_path)

dir_name = input("Please give a name to the directory: ") 

sim_text = []

for i in range(len(data_files)):
    
    data = read_json_file(files[0])
    topics = get_all_topics_json(data)
    sent_tops = get_annotated_sent_tops_json(data)
    
    print("Starting finding documents...")
    res = read_result_file(data_files[i], read2_path)
    res = list(eval(res[0]))
    t = find_document_by_sentimented_topics(res, sent_tops)
    print("Found documents: ", t)
    dig = re.findall(r'\d+', data_files[i])
    
    dest_path = dir_name + str(dig)
    
    get_text_senttops(t, dest_path)
    
    
    text_path = os.path.join(parent_dir, "clean-docs")
    for name in t:
        with open(os.path.join(text_path, name[1]), encoding="utf8") as f:
            text = f.read()
            f.close()
            sim_text.append(text)
    print("Found this amount of text: ", len(sim_text))

