# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 13:19:16 2020

@author: Debby Lam

The main purpose of this script is to analyse documents by various techniques in order to understand the documents
and underlying structures which may be useful for later steps.


"""

# %% ----------- Import libraries -----------

import pathlib

import nltk   
from nltk import tokenize
from nltk.tokenize import sent_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer 
from nltk.stem.snowball import SnowballStemmer
from nltk import FreqDist, pos_tag
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import string

from langdetect import detect
import textacy

import spacy
#import en_core_web_sm
#nlp = en_core_web_sm.load()
#spacy.load('en_core_web_sm')

import os
from os.path import dirname, abspath

import pandas as pd
from pandas import *
import re

from IPython.display import display, HTML

import matplotlib.pyplot as plt
import numpy as np

from collections import Counter

import seaborn as sns


# %% ----------- NLTK functions and work ----------

def text_to_lines(text):
    return nltk.sent_tokenize(text)


# %% ----------- Reader function ----------

def read_file(doc):
    file = open(doc , mode='r', encoding='utf-8')
    text = file.read()
    file.close()
    return text



def read_preprocessed(num = None):
    num = num
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "clean-docs")  #..\..\software scripts\preprocessed
    os.chdir(read_path) 
    files = os.listdir()
    text_list = []
    if num == None:
        print("Progress is starting...")
        count = 0
        for file in files:
            file = open(file, mode='r', encoding='utf-8')
            text = file.readlines()
            file.close()
            text = "".join(text)
            text_list.append(text)
            count += 1
            if(count % 1000 == 0):
                print("Progress at file: " + str(count))
        print("Process is done!")
        return list(set(text_list))
    else:
        for i in range(0, num):
            text_list.append(read_file(files[i]))
        return list(set(text_list))

#7752 docs with weblinks
#7387 unique docs without weblinks
    
#docs = read_preprocessed() 
docs2 = read_preprocessed() 

def duplicates(lst, item):
    return [i for i, x in enumerate(lst) if x == item]

#diff = []
#for doc in docs2:
#    diff.append(duplicates(docs2, doc))


#diff = [x for x in diff if len(x) > 1]  

# %% ----------- Text analysis ----------

# Check # of words
# Check # of sentences
# Length longest sentence
# length shortest sentence
# Check avg length of sentences
# Check language
# Check unique words
# Title


# Overall:
# Check average words, sentences, weblinks

def merge_text(sent_list):
    text = " ".join(sent_list)
    text = text.strip("\n")
    return text

def common_words(sent_list, num=None):
    text = merge_text(sent_list)
    clean_text = text.translate(str.maketrans('', '', string.punctuation))
    tokens = word_tokenize(clean_text.lower())
    return FreqDist(tokens).most_common(num)

def get_title(sent_list):
    return sent_list[0]

def get_url(sent_list):
    return sent_list[-1]

def count_words(sent_list):
    text = merge_text(sent_list)
    clean_text = re.sub(r'[^\w\s]','', text)
    return len(word_tokenize(clean_text.lower()))

def longest_sent_find(sent_list):
    long_sent = max(sent_list, key=len)
    return len(word_tokenize(long_sent))

def shortest_sent_find(sent_list):
    short_sent = min(sent_list, key=len)
    return len(word_tokenize(short_sent))

def count_sentences(sent_list):
    return len(sent_list)

def detect_language(sent_list):
    text = "".join(sent_list)
    return detect(text)

def count_uniques(sent_list):
    text = merge_text(sent_list)
    clean_text = text.translate(str.maketrans('', '', string.punctuation))
    word_set = set(word_tokenize(clean_text.lower()))
    return len(word_set)

def avg_word_sentence(sent_list):
    return count_words(sent_list) / count_sentences(sent_list)


# %% ----------- Create pandas df for docs ----------

import re
s = "string. With. Punctuation?"
s = re.sub(r'[^\w\s]','',s)

def create_names(docs):
    index_names = []
    for i in range(0,len(docs)):
        index_names.append("doc" + str(i))
    return index_names

    
def create_datarow(doc):    
    title = get_title(doc)
    word_count = count_words(doc)
    sent_count = count_sentences(doc)
    unique_count = count_uniques(doc) 
    avg_word = avg_word_sentence(doc)
    lan = detect_language(doc)
    longest = longest_sent_find(doc)
    shortest = shortest_sent_find(doc)
    url = get_url(doc)
    text = merge_text(doc)
    return [title, word_count, sent_count, unique_count, avg_word, lan, longest, shortest, url, text]
    

def create_df(docs):
    index_names = create_names(docs)
    data = [create_datarow(text_to_lines(doc)) for doc in docs]
    colnames = ["Title", "# of words", "# of sents", "# of unique words", "Average word/sent",
                "Language", "longest sent len", "shortest sent len", "Web URL", "text"]
    df = pd.DataFrame(data, index=index_names, columns = colnames)
    return df

    
def save_pd(df, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "word-docs")  #..\..\software scripts\preprocessed
    os.chdir(save_path)
    df.to_pickle(name + ".pkl")


def read_pd(name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "word-docs")  #..\..\software scripts\preprocessed
    df =  pd.read_pickle(os.path.join(read_path, name + ".pkl"))
    return df


# %% ----------- Read the pandas dataframe ----------
clean_docs = []

for doc in docs2:
    lines = doc.splitlines()
    lines = [line for line in lines if len(line) != 0]
    clean_docs.append("".join(lines))


#%% ---

df = create_df(clean_docs)

# %% ----------- Save the pandas dataframe ----------

save_pd(df, "preprocessed_analysisEng7387")

# %% ----------- Read the pandas dataframe ----------

#df = read_pd("preprocessed_analysisEng7000")
colnames = ["Title", "Language", "text", "Web URL"]
df = df.drop(columns = colnames)
df = df.round(2)

# %% ----------- Read the pandas dataframe ----------

#df = read_pd("preprocessed_analysisEng7000")
#df.to_html('filename7000.html')
df_summary = df.describe()
df_summary = df_summary.round().astype(int)
print(df_summary.to_latex())


# %% ----------- Get the web urls and count ----------

import re
from urllib.parse import urlparse
from collections import Counter
from itertools import dropwhile
import numpy as np


def get_filtered_weburls(df_column):
    url_list = []
    for web_url in df_column:
        parsed_url = urlparse(web_url.strip())
        print(parsed_url)
        url_list.append(parsed_url.netloc)
    return url_list

def count_occurrences(item_list):
    return Counter(item_list)

# Get dictionary of web urls with occurrences
def clean_dic(dic, num, name):
    others = 0
    for key, count in dropwhile(lambda key_count: key_count[1] >= num, dic.most_common()):
        others += count
        del dic[key]
    dic[name] = others
    return dic


# %% ----------- Get word counts for documents ----------

doc_count = df["# of words"]
#doc_count = count_occurrences(doc_count)


# %% ----------- Get the web urls and count ----------


#web_urls = df["Web URL"]
#o = urlparse(web_urls[1].strip())

#o = get_filtered_weburls(web_urls)
#occ = count_occurrences(o)

links = []
for doc in docs2:
    lines = doc.splitlines()
    lin = lines[-1]
    links.append(" ".join(lin.split()))

o = get_filtered_weburls(links)
occ = count_occurrences(o)
    

# %% ----------- Get the web urls and count ----------


clean_dict = clean_dic(occ, 50, "other")
clean_dict.most_common()
url_df = pd.DataFrame(clean_dict.items(), columns=['URL name', 'Count'])
url_df["Percentage"] = ((url_df["Count"]/len(df))*100).round(2)
url_df.to_html('url.html')
print(url_df.to_latex(index=False, escape=False))

# %% ----------- Create plot ----------


fig, ax = plt.subplots(figsize=(12,7))
plt.ylabel('Count of occurrence')
plt.title('Count of URL link occurrences of repository', fontsize = 15)
plt.bar(doc_count.keys(), doc_count.values())
plt.xticks(rotation=75)
plt.yticks(np.arange(0, 5001, step=500))
plt.tight_layout()
plt.show()

# %% ----------- Create plot ----------


hist,bin_edges = np.histogram(doc_count)
plt.plot(hist)
plt.show()

# %% ----------- Create plot ----------

fig, ax = plt.subplots(figsize=(15,6))

ax.set_title("Distribution of number of words", fontsize=16)
ax.set_xlabel("Number of words")
sns.distplot(doc_count, bins=500, ax=ax, kde=False)
plt.xlim(-10, 5000)
ax.set_xticks(range(0,8000, 500))


# %% ----------- Create plot ----------

hist,bin_edges = np.histogram(doc_count)

plt.xlim(min(bin_edges), max(bin_edges))
plt.ylim(0,10)
plt.grid(axis='y', alpha=0.75)
plt.xlabel('Value',fontsize=15)
plt.ylabel('Frequency',fontsize=15)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.ylabel('Frequency',fontsize=15)
plt.title('Normal Distribution Histogram',fontsize=15)
plt.show()


# %% ----------- Get common words ----------

def read_preprocessed(num = None):
    num = num
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "preprocessed")  #..\..\software scripts\preprocessed
    os.chdir(read_path) 
    files = os.listdir()
    text_list = []
    if num == None:
        print("Progress is starting...")
        count = 0
        for file in files:
            file = open(file, mode='r', encoding='utf-8')
            text = file.readlines()
            file.close()
            text_list.append(text)
            count += 1
            if(count % 1000 == 0):
                print("Progress at file: " + str(count))
        print("Process is done!")
        return list(set(text_list))
    else:
        for i in range(0, num):
            text_list.append(read_file(files[i]))
        return list(set(text_list))
        
#docs = read_preprocessed()   

def collapse_list(l):
    flat_list = [item for sublist in l for item in sublist]
    return flat_list

def remove_punctuation(docs):
    docs = [doc.lower() for doc in docs]
    docs = [doc.translate(str.maketrans('', '', string.punctuation)) for doc in docs]
    return [doc.replace("'","") for doc in docs]



# %% ----------- Get common words with stopword removal ----------
 
stop_words = set(stopwords.words('english'))


path = dirname(abspath(__file__)) #..\..\software scripts\scripts
parent_dir = dirname(path) #..\..\software scripts
read_path = os.path.join(parent_dir, "word-docs")  #..\..\software scripts\word-docs
os.chdir(read_path) 
file = open(os.path.join(read_path, "stopwords.txt"), mode='r', encoding='utf-8')
removal_words = file.readlines()

# close the file
file.close()

removal_words = clean_wordlist(removal_words)
complete = list(set(stop_words) | set(removal_words))


# %% ----------- Get common words with stopword removal ----------

#col_list = collapse_list(docs)
col_list = remove_punctuation(docs)
words = [word_tokenize(sent) for sent in col_list]
words = collapse_list(words)


# %% ----------- Get common words without stopword removal ----------

count = Counter(words)
most_common = count.most_common(30)

# %% ----------- Get common words without stopword removal ----------

df_common = pd.DataFrame(most_common, columns=["Word", "Count"])

# %% ----------- Get bigrams ----------

nltk_tokens = [nltk.word_tokenize(word_data) for word_data in docs]  	

bigrams = [(list(nltk.bigrams(nltk_token))) for nltk_token in nltk_tokens]
bigrams = collapse_list(bigrams)

# %% ----------- Count the bigrams ----------

bigrams_count = Counter(bigrams)
print(bigrams_count.most_common(30))


# %% ----------- Get common words with stopword removal ----------


filtered_list = [w for w in words if not w in complete] 
filtered_count = Counter(filtered_list)
most_common_filtered = filtered_count.most_common(30)


# %% ----------- Get common words without stopword removal ----------

df_common_filtered = pd.DataFrame(most_common_filtered, columns=["Word", "Count"])



