# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 13:19:16 2020

@author: Debby Lam

The main purpose of this script is to pre-process documents by various techniques in order to retrieve 
important sentences and representations of text.


"""

# %% ----------- Import libraries -----------

import pathlib

import nltk   
from nltk import tokenize
from nltk.tokenize import sent_tokenize
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer 
from nltk.stem.snowball import SnowballStemmer
from nltk import FreqDist, pos_tag

import spacy
import en_core_web_sm
spacy.load('en')
nlp = en_core_web_sm.load()


from langdetect import detect

import pandas as pd
import os
from os.path import dirname, abspath
import string


import os
from os.path import dirname, abspath
import time


# %% ----------- Plain text cleaner Spacy version-----------

spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS


# %% ----------- Download NLTK corpus -----------

# Download NLTK stopwords and vader lexicon
nltk.download('stopwords')
nltk.download('vader_lexicon')


# %% ----------- NLTK functions and work ----------

def text_to_lines(string):
    return nltk.sent_tokenize(string)

# %% -----------  Read more sentence removals ----------

def load_removal_list():
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "word-docs")  #..\..\software scripts\word-docs
    os.chdir(read_path) 
    file = open(os.path.join(read_path, "removal_words.txt"), mode='r', encoding='utf-8')

    # read all lines at once
    removal_words = file.readlines()

    # close the file
    file.close()
    return removal_words


# %% ----------  Defining cleaning functions for stopwords list ----------- 


list_of_removals = ["share this with", "email", "facebook messenger", "facebook", "messenger", "subscribe",
                    "twitter", "pinterest", "whatsapp", "linkedin", "copy this link", "instagram",
                    "external link", "will open in a new window", "terms and conditions", "privacy policy",
                    "cookie notice", "copyright © 2020", "copyright at", "copyright ©", "copyright @", "read:", 
                    "more:", "plus:", "by using this site you agree to the", "subscriber", "privacy notice", 
                    "advertisement", "supported by", "our email", "click here", "visit our", 
                    "visit our dedicated covid-19 page", "login to your account", "recover your password",
                    "A password will be e-mailed to you", "your email address will not be published",
                    "prev post", "next post", "we invite you to join", "you need a subscription to",
                    "sign up for", "don't show this again", "cookie settings", "source:", "unsubscribe",
                    "join now!", "©2020", "©", "all rights reserved", "click close", "link copied",
                    "© 2020", "here are some tips", "illustration:", "copyright 2020", "copyright", 
                    "last modified", "name (required)", "cookies", "sponsered by:", "terms of use",
                    "jump to content", "photo by:", "photo by", "terms of service"]


# Clean the list of words from \n and lowercase
def clean_wordlist(list):
    clean_list = []
    for word in list:
         clean_list.append(word.rstrip("\n").lower())
    return clean_list


def complete_list(list1, list2):
    set1 = set(list1)
    set2 = set(list2)
    return list(set(set1.union(set2)))


# %% ----------  Defining cleaning functions ----------- 


# Removes duplicate sentences and save them in a set
def unique(sequence):
    seen = set()
    return [x for x in sequence if not (x in seen or seen.add(x))]


# Remove sentences that contains certain words in a list of tag words,
# but keep the sentences that are important in the list
# Remove the last item of the list because it contains a link
def remove_sentences(sentences):
    new_list = []
    for sentence in sentences:
        clear_sent = sentence.lower()
        clear_sent = clear_sent.replace('\n','')
        clean_sent = clear_sent.translate(str.maketrans('', '', '.'))
        if len(sentence) > 2:
            if (not any(word in clean_sent for word in list_of_removals)):
                new_list.append(sentence)
    return new_list


# %% ---------- Saver functions for cleaned text-----------   

def file_size(fname):
        statinfo = os.stat(fname)
        return statinfo.st_size


def get_files_by_size(size, condition):
    files = []
    check_files = os.listdir()
    # r=root, d=directories, f = files
    if(condition == "less"):
        for file in check_files:
            if file_size(file) <= size:
                files.append(file)
        return files
    else:
        for file in check_files:
            if file_size(file) > size:
                files.append(file)
        return files
        

# Delete a file in a directory by byte_size
def delete_files(directory_path, byte_size, condition):
    os.chdir(directory_path)
    files = get_files_by_size(byte_size, condition)
    for file in files:
        os.remove(file)

def get_title(file):
    doc = open(file, mode='r', encoding='utf-8', errors='ignore')
    lines = doc.readlines()
    doc.close()
    return lines[0] 
    

def get_text(file):
    doc = open(file, mode='r', encoding='utf-8', errors='ignore')
    lines = doc.readlines()
    doc.close()
    return lines    


def clean_text(lines):
    weblink = lines[-1]
    #title = lines[0]
    processed_lines = remove_sentences(lines)
    processed_lines.append(weblink)
    #processed_lines.insert(0, title)
    unique_lines = list(unique(processed_lines))
    clean_doc = ' '.join(unique_lines)
    return clean_doc

def save_clean_text(processed_text, name, path):
    if not os.path.exists(path):
        os.makedirs(path)
    with open(os.path.join(path, name + ".txt"), 'w+', encoding="utf-8") as output:
        output.write(processed_text)
        output.close()


# %% ---------- Saver functions for cleaned text-----------   

def automatic_preprocess():
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    os.chdir(parent_dir)
    read_path = os.path.join(parent_dir, "text-docs")  #..\..\software scripts\text-docs
    save_path = os.path.join(parent_dir, "preprocessed")  #..\..\software scripts\preprocessed
    os.chdir(read_path) 
    
    # Get the directories where the text docs are
    directories = os.listdir()
    docID = list(string.ascii_uppercase)
    num = 0
    alpha = 0
    tic = time.perf_counter()
    #For each directory
    for directory in directories:
        os.chdir(directory)
        files = os.listdir()
        alpha += 1
        # Get the text from each file
        for file in files:
            if os.path.isfile(file):
                text = get_text(file)
                clean_doc = clean_text(text)
                save_clean_text(clean_doc, "processed" + str(num) + docID[alpha], save_path)
                num += 1
                if(num % 500 == 0):
                    print("Progress at file: " + str(num))
                    toc = time.perf_counter()   
                    print(f"Progress of cleaning documents at this point took {toc - tic:0.2f} seconds") 
        os.chdir(read_path)
    print("Process is done!")



# %% ---------- Remover functions -----------   
    
    
def delete_preprocessed_files_bytes(byte_size, condition):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    destination_path = os.path.join(parent_dir, "preprocessed")  #..\..\software scripts\preprocessed
    delete_files(destination_path, byte_size, condition) 

def delete_preprocessed_timeline_liveblogs():
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    destination_path = os.path.join(parent_dir, "preprocessed")  #..\..\software scripts\preprocessed
    os.chdir(destination_path)
    check_files = os.listdir()
    words = ["liveblog", "timeline", "live updates", "update", "updates"]
    for check_file in check_files:
        title = get_title(check_file)
        clean_title = title.lower().translate(str.maketrans('', '', string.punctuation))
        if any(word in words for word in clean_title):
            os.remove(check_file)
        
def delete_preprocessed_language():
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    destination_path = os.path.join(parent_dir, "preprocessed")  #..\..\software scripts\preprocessed
    os.chdir(destination_path)
    check_files = os.listdir()
    for check_file in check_files:
        text_lines = get_text(check_file)
        full_text = " ".join(text_lines)
        if detect(full_text) != "en":
            os.remove(check_file)



# %% ---------- Tokenization of documents -----------   


import os
from os.path import dirname, abspath
import time

import matplotlib.pyplot as plt
from matplotlib import gridspec

from operator import itemgetter

import gensim, spacy
from gensim.models import Phrases
from gensim import corpora, models, similarities 
from nltk.corpus import stopwords

import pandas as pd
import numpy as np
import re

import en_core_web_sm
nlp = en_core_web_sm.load()
from spacy.lang.en.stop_words import STOP_WORDS
from gensim.parsing.preprocessing import STOPWORDS

from gensim.models import CoherenceModel# Compute Coherence Score


#text processing
import re
import string
import nltk

from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem.porter import PorterStemmer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer

# %% ----------- Load and create a list of stopwords -----------

# Load NLTK stopwords
stop_words = stopwords.words('english')

# Add some extra words in it if required
stop_words.extend(['from', 'subject','pron'])

# Load Spacy stopwords
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS

# Create one big stopwords list
stopwords_list = STOPWORDS.union(set(stop_words), spacy_stopwords)

# %% ----------- Read a file with more stop words and extend the stop words list -----------


def load_stopwords_list(name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "word-docs")  #..\..\software scripts\word-docs
    file = open(os.path.join(read_path, name), mode='r', encoding='utf-8')

    # read all lines at once
    extra_stopwords = file.readlines()

    # close the file
    file.close()
    return extra_stopwords

# Clean the list of stopwords from \n
def clean_stopwords(list):
    clean_list = []
    for word in list:
         clean_list.append(word.rstrip("\n"))
    return clean_list
         
stopwords = list(clean_stopwords(stopwords))


def save_stopwords_list(word_list, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "word-docs")  #..\..\software scripts\word-docs 
    with open(os.path.join(save_path, name), 'w', encoding="utf-8") as output:
        for word in word_list:
            output.write(str(word) + "\n")
    

# %% ----------- Read file in----------

def read_file(doc):
    file = open(doc , mode='r', encoding='utf-8')
    text = file.read()
    file.close()
    return text


def read_preprocessed(num = None):
    num = num
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "preprocessed")  #..\..\software scripts\preprocessed
    os.chdir(read_path) 
    files = os.listdir()
    text_list = []
    if num == None:
        print("Progress is starting...")
        count = 0
        for file in files:
            file = open(file, mode='r', encoding='utf-8')
            text = file.readlines()
            file.close()
            text = "".join(text[:-1])
            text_list.append(text)
            count += 1
            if(count % 1000 == 0):
                print("Progress at file: " + str(count))
        print("Process is done!")
        return list(set(text_list))
    else:
        for i in range(0, num):
            text_list.append(read_file(files[i]))
        return list(set(text_list))

    
# %% ----------- Clean text function ----------

# Split whole text into sentences
def split_sentences(text):
    sentences = nltk.sent_tokenize(text)
    sentences.pop(-1)
    return sentences


# Retrieve the WordNet POS tag
def get_wordnet_pos(word):
    """Map POS tag to first character lemmatize() accepts"""
    tag = nltk.pos_tag([word])[0][1][0].upper()
    tag_dict = {"J": wordnet.ADJ,
                "N": wordnet.NOUN,
                "V": wordnet.VERB,
                "R": wordnet.ADV}

    return tag_dict.get(tag, wordnet.NOUN)


# Transform word tokens to its base form which still makes semantically sense
def lemmatize_nltk(tokens):
    word_lemmas = []
    lemma = WordNetLemmatizer()
    for word in tokens:
        pos_tag = get_wordnet_pos(word)
        word_lemmas.append(lemma.lemmatize(word, pos_tag))
    return word_lemmas


# Remove punctuation, uppercases and tokenize
def tokenize_text(text):
     """
     Function to clean text-remove punctuations, lowercase text etc.    
     """
     tokenizer = RegexpTokenizer(r'\w+')
     text = text.lower() # lower case text
     text = text.strip('.')
     text = ' '.join([word for word in text.split() if word not in stopwords and len(word) > 2])
     tokens = tokenizer.tokenize(text)
     clean_tokens = lemmatize_nltk(tokens)
     return clean_tokens
    

# Remove digits from list
def remove_digits(item_list):
    return [item for item in item_list if not item.isdigit()]

# Remove single letters from list
def remove_single_letter(item_list):
    return [item for item in item_list if len(item) > 2]

# Remove single letters from list
def remove_stopwords(item_list):
    return [item for item in item_list if item not in stopwords]

# List cleaner fucntion
def list_cleaner(item_list):
    removed_digits = remove_digits(item_list)
    stop_free = remove_stopwords(removed_digits)
    clean_list = remove_single_letter(stop_free)
    return clean_list

# %% ----------- SPACY version lemmatize the words ----------

# Transform word tokens to its base form which still makes semantically sense
def lemmatize_spacy(text):
    lemmas = []
    nlp = spacy.load("en_core_web_sm") 
    doc = nlp(text)
    for token in doc:
        if token.text not in stopwords:
            lemmas.append(token.lemma_)
    return lemmas
    

# %% ----------- Pre-processing documents for Gensim dictionary corpus ----------

# Splitting sentences, tokenization, removal of stopwords for a text document.
def clean_doc(text):
    doc_sentences = split_sentences(text) #list of sentences
    sent_clean = [tokenize_text(sentence) for sentence in doc_sentences] #list of lists of tokens for each sentence
    clean_tokens = [list_cleaner(sent) for sent in sent_clean]
    flat_token_list = [item for sub_list in clean_tokens for item in sub_list]
    return flat_token_list

# Split and tokenize text documents
def clean_doc_progress(documents):
    count = 0
    doc_clean_list = []
    tic = time.perf_counter()

    for doc in documents:
        doc_clean_list.append(clean_doc(doc))
        if(count % 500 == 0):
            print("Progress at file: " + str(count))
            toc = time.perf_counter()   
            print(f"Progress of cleaning documents at this point took {toc - tic:0.2f} seconds")    
        count += 1
        
    toc = time.perf_counter()   
    print(f"Progress of cleaning documents into tokens took {toc - tic:0.2f} seconds")    
    return doc_clean_list

def remove_empty_values(token_list):
    res = [ele for ele in token_list if ele != ''] 
    return res

def clean_token_list(token_lists):
    clean_lists = [remove_empty_values(token_list) for token_list in token_lists] 
    return clean_lists


# %% ----------- Compute and add bigrams ----------

# Phrases is Gensim is a statistical analysis model. 
# Potential pairings are given a 'score', and those that score over a configurable 'threshold' are combined.
# Default threshold is 10, but check for value 5
# Default min_count value is 5, but check for 10, 15 and 20

# Create a list of tokenized doucments with unigrams and bigrams
def create_bigrams(doc_terms, num=5, score=10):
    # Add bigrams to docs (only ones that appear num times or more).
    bigram = Phrases(doc_terms, min_count=num, threshold=score)
    for idx in range(len(doc_terms)):
        for token in bigram[doc_terms[idx]]:
            if '_' in token:
                # Token is a bigram, add to document.
                doc_terms[idx].append(token)
    return doc_terms

# Create trigrams from tokenized words and sets
def create_trigrams(doc_terms, bigrams, num=5, score=10):
    # Add bigrams and trigrams to docs (only ones that appear num times or more).
    trigram = Phrases(bigrams[doc_terms], min_count=num, threshold=score)
    for idx in range(len(doc_terms)):
        for token in trigram[doc_terms[idx]]:
            if '_' in token:
                # Token is a bigram, add to document.
                doc_terms[idx].append(token)
    return doc_terms


def create_bigrams_loose(doc_terms, num=5, score=10):
    # Add bigrams and trigrams to docs (only ones that appear num times or more).
    bi_list = []
    bigram = Phrases(doc_terms, min_count=num, threshold=score)
    for doc_term in doc_terms:                
            bigram_sentence = ', '.join(bigram[doc_term])
            for token in bigram_sentence.split(","):
                if "_" in token:
                    bi_list.append(token)
    return bi_list



# %% ----------- Clean the documents ----------

def save_clean_tokens(token_list, name):
    my_df = pd.DataFrame(token_list)
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "dictionaries")  #..\..\software scripts\preprocessed
    my_df.to_csv(os.path.join(save_path, name), index=False, header=False)
    

