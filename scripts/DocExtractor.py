# -*- coding: utf-8 -*-
"""
Created on Tue Mar 17 13:19:16 2020

@author: Debby Lam
"""

# %% ----------- Import libraries -----------
import urllib.request 
import pandas as pd

from googlesearch import search
import requests
from bs4 import BeautifulSoup

#from duckduckpy import query
from fake_useragent import UserAgent
import en_core_web_sm

import os, shutil
from os.path import dirname, abspath

# %% ----------- Defining google search and weblinks saver functions-----------

# Creating a function to query search results on Google
# @query = topic you wanrt to find, @amount_per_page = # of docs from each google page, 
# @total_amount = total amount of weblinks to retrieve
  
def google_search(query_search, amount_per_page, total_amount, start=0, domain = None):
    list_of_webdocs = []
    if domain != None:
        query_search = f"site:{domain} {query_search}"
    print("Process is starting...")
    for j in search(query_search, lang="en", tbs="qdr:d", start=start, num=amount_per_page, 
                    stop=total_amount, pause=20):
        list_of_webdocs.append(str(j)) 
    print("Process is done")
    return(list_of_webdocs)


# Function to search with a list of multiple search queries
# Remove duplicate weblinks    

def google_search_multiple(list, amount_per_page, total_amount):
    total_of_links = []
    num = 1
    
    for query in list:
        print("Processing with query number: " +str(num) )
        total_of_links.extend(google_search(query, amount_per_page, total_amount))
        print("(Temporarily) number of total links: " + str(len(total_of_links)))
        num +=1
        
    return(set(total_of_links))


def list_converter(set):
    return list(set)


# %% ----------- saver function-----------

# Write the list of weblinks to a single .txt file

def save_list(list, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "weblinks")  #..\..\software scripts\weblinks
    with open(os.path.join(save_path, name + ".txt"), 'w', encoding="utf-8") as output:
        for link in list:
            output.write(str(link) + '\n')

# %% ----------- Query creator; Create multiple queries to fire in query function ----------
            
def remove_verbs_spacy(string):
    nlp = en_core_web_sm.load()
    tokens = nlp(string)
    words = []
    
    for token in tokens:
        if token.pos_ not in ["VERB", "AUX"]:
            words.append(token.text)
    return ' '.join(words)
        

def add_quotation(string):
    return f'"{string}"'

def extract_nouns(string):
    nlp = en_core_web_sm.load()
    tokens = nlp(string)
    words = []
    
    for token in tokens:
        if token.pos_ in ["NOUN", "ADJ", "PROPN"]:
            words.append(token.text)
    return ' '.join(words)

def query_creator(string):
    list_of_queries = []
    nouns = extract_nouns(string)
    #rem_verbs = remove_verbs_spacy(string)
    list_of_queries.extend((string, nouns))
    return list_of_queries


# %% ----------- Examples  -----------

# finding article about coronavirus
queries = query_creator("Coronavirus opinion piece")           
test = google_search_multiple(queries, 15, 300)
test_list = list_converter(test)
save_list(test_list, "webs")  


# %% ----------- Executing the code -----------

queries = query_creator("Coronavirus opinion")           
test = google_search_multiple(queries, 15, 300)
test_list = list_converter(test)
save_list(test_list, "webs2")  


# %% ----------- Get all the .txt files from weblinks -----------

def get_url_files():
    
    path = dirname(abspath(__file__)) 
    parent_dir = dirname(path)
    read_path = os.path.join(parent_dir, "weblinks")
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(read_path):
        for file in f:
            if '.txt' in file:
                files.append(file)
    return files


# %% ----------- Weblinks cleaner function-----------

#Clean the list of URLs from video, forum posts media, dutch websites and podcasts 

def clean_url(file):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "weblinks")  #..\..\software scripts\weblinks
    file = open(os.path.join(read_path, str(file)), mode='r', encoding='utf-8')
    lines = file.readlines()    
    removal_link_words = ["youtube", "klm", "reddit", "soundcloud", ".org", ".nl", "podcast", "spotify", ".be"]
    
    clean_url_list = []
    for link in lines:
        if not any(n in link for n in removal_link_words):
            clean_url_list.append(link)
            
    file.close()     
    return clean_url_list


def save_list_clean(list, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "clean-urls-links")  #..\..\software scripts\clean-url-links
    with open(os.path.join(save_path, name), 'w', encoding="utf-8") as output:
        for link in list:
            output.write(str(link))

# Save a list of weblinks that were unable to extract text from            
def save_list_fail(list, name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    save_path = os.path.join(parent_dir, "fail-links")  #..\..\software scripts\fail-links
    with open(os.path.join(save_path, name), 'w', encoding="utf-8") as output:
        for link in list:
            output.write(str(link))
            

# %% ----------- Filter the weblinks from certain links or types of weblinks----------

def convert_to_clean_links(filenames):
    for file in filenames:
        name = file
        clean_file = clean_url(file)
        save_list_clean(clean_file, name)


# %% ----------- Defining functions to extract content and text from webpages and save them local to documents-----------


def extract_par_text(weblink):
    '''
    Creates a tuple of a list of text and the HTML type of the text

    :param str weblink: The URL where text will be extracted from
    '''
    page = requests.get(weblink)
    soup = BeautifulSoup(page.text, 'html.parser')
    pars = soup.find_all(["p"])
    
    text_list = []
    text_type_list = []
    
    for par in pars:
        text_list.append(par.get_text())
        text_type_list.append(par.name)
        
    return text_list, text_type_list

    
def create_df_text_type(tuple):
    data = {"Text":tuple[0], "HTML Type":tuple[1]}
    return pd.DataFrame(data)

def find_title(weblink):
    page = requests.get(weblink)
    soup = BeautifulSoup(page.text, 'html.parser')
    title = soup.find("title")
    if title is None:
        return "(Empty)"
    else:
        return title.string


def write_to_txt_file(dataframe, title, name, link):
    with open(str(name) + ".txt", 'w', encoding="utf-8") as output:
        output.write(str(title) + '\n')
        for row in dataframe["Text"].values.tolist():
            output.write(str(row) + '\n')
        output.write(str(link))


# Retrieve from a list of weblinks, the paragraphs and text         
def convert_links_to_docs(weblist, name, count):         
    count = count
    cur_path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(cur_path) #..\..\software scripts
    path = os.path.join(parent_dir, "text-docs")
    save_path =  os.path.join(path, str(name))
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    os.chdir(save_path)    
    for link in weblist:
         #print("Process starting for link: " + str(link))
         pars = extract_par_text(link)
         df = create_df_text_type(pars)
         title = find_title(link)
         write_to_txt_file(df, title, "doc"+str(count), link) 
         count+=1

# %% ----------- Defining functions to extract content and text from webpages and save them local to documents URLLIB VER. -----------


def write_to_txt_file_urllib(dataframe, title, name, link, path):
    os.chdir(path)
    with open(str(name) + ".txt", 'w', encoding="utf-8") as output:
        output.write(str(title) + '\n')
        for row in dataframe["Text"].values.tolist():
            output.write(str(row) + '\n')
        output.write(str(link))


def find_title_urllib(weblink):
    url = weblink
    req = urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0","X-Requested-With": "XMLHttpRequest"})
    page = urllib.request.urlopen(req).read()
    soup = BeautifulSoup(page, 'html.parser')
    title = soup.find("title")
    if title is None:
        return "(Empty)"
    else:
        return title.string    
        

def extract_par_text_urllib(weblink):
    '''
    Creates a tuple of a list of text and the HTML type of the text

    :param str weblink: The URL where text will be extracted from
    '''
    url = weblink
    req = urllib.request.Request(url, headers={"User-Agent": "Mozilla/5.0" ,"X-Requested-With": "XMLHttpRequest"})
    page = urllib.request.urlopen(req).read()
    soup = BeautifulSoup(page, 'html.parser')
    pars = soup.find_all(["p"])
    
    text_list = []
    text_type_list = []
    
    for par in pars:
        text_list.append(par.get_text())
        text_type_list.append(par.name)
        
    return text_list, text_type_list



# Retrieve from a list of weblinks, the paragraphs and text         
def convert_links_to_docs_urllib(weblist, name, count):         
    count = count
    path = os.getcwd() + "_" + str(name)
    if not os.path.exists(path):
        os.makedirs(path)
    for link in weblist:
         #print("Process starting for link: " + str(link))
         pars = extract_par_text_urllib(link)
         df = create_df_text_type(pars)
         title = find_title_urllib(link)
         write_to_txt_file_urllib(df, title, "doc"+str(count), link, path) 
         count+=1

# %% ----------Execute code of extracting text-----------
            
def read_clean_webfiles():
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "clean-urls-links")  #..\..\software scripts\weblinks
    return [f for f in os.listdir(read_path)]


# %% ----------Execute code of extracting text-----------

def remove_dupes(x):
  return list(dict.fromkeys(x))


def big_weblist(filenames):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "clean-urls-links")  #..\..\software scripts\weblinks
    
    big_list = []    
    for filename in filenames:
        file = open(os.path.join(read_path, filename), mode='r', encoding='utf-8')
        weblines = file.readlines() 
        file.close()
        big_list.extend(weblines)
    return remove_dupes(big_list)
    

# %% ---------- Find failed weblinks docs and move them to another directory ----------- 

def file_size(fname):
        statinfo = os.stat(fname)
        return statinfo.st_size


def get_files_by_size(size):
    files = []
    check_files = os.listdir()
    # r=root, d=directories, f = files
    for file in check_files:
        if file_size(file) <= size:
            files.append(file)
    return files


def move_files(file_list):
    path = os.getcwd()
    destination = path + "\\small_files"
    
    if not os.path.exists(destination):
        os.makedirs(destination)        
    
    for file in file_list:
        shutil.move(file, destination)

def get_weblinks_from_docs(files):
    
    links_list = []
    
    for f in files:
        file = open(f, mode='r', encoding='utf-8')
        lines = file.readlines()
        file.close()
        links_list.append(lines[-1])
    
    return links_list


# %% ---------- Rename fail links documents ----------- 

def remove_files(files):
    for file in files:
        os.remove(file)

def rename_files(files, num, new_name):
    for name in files:
        os.rename(name, new_name + str(num)+'.txt')
        num += 1

        
# %% ---------- Delete documents which are smaller or equal to 1kB----------- 

# Delete a file in a directory by byte_size
def delete_files(directory_path, byte_size):
    os.chdir(directory_path)
    files = get_files_by_size(byte_size)
    for file in files:
        os.remove(file)

# Automatic function to delete all files in directories by byte size     
def automatic_delete_files(byte_size):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "text-docs")  #..\..\software scripts\weblinks
    os.chdir(read_path)
    directories = os.listdir()
    for directory in directories:
        delete_files(directory, byte_size)
        os.chdir(path)
    

