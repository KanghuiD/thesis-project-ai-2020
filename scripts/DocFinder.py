# -*- coding: utf-8 -*-
"""
Created on Tue Oct 20 16:57:42 2020

@author: Debby Leijen-Lam
"""

import json 
import os
from os.path import dirname, abspath

from operator import itemgetter
import numpy
import time
import string

import pandas as pd
import numpy as np
import re
import collections

from itertools import cycle, combinations

import math
import statistics as stat

import random 

# %% ----------- Import libraries -----------

# Get the words from the topics
def parse_topic_words(topic):
    # takes the string returned by model.show_topics()
    # split on strings to get topics and the probabilities
    topic = topic.split('+')
    # list to store topic bows
    topic_bow = []
    for word in topic:
        # split probability and word
        prob, word = word.split('*')
        # get rid of spaces and quote marks
        word = word.replace(" ","").replace('"', '')
        topic_bow.append(word)
        
    return topic_bow


# Convert a tuple into a dictionary
def Convert(tup, di): 
    di = dict(tup) 
    return di 

#Read a JSON file
def read_json_file(file_name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "sent-tops")  #..\..\software scripts\word-docs 
    with open(os.path.join(read_path, file_name)) as f:
        data = json.load(f)
    return data

#If each document contains at most 1 sentimented topic
def get_sent_tops_json(dic):
    sent_tops_list = []
    keys = [*dic]
    for key in keys:
        sent_tops_list.append(((dic[key]["topic_id"], dic[key]["sentiment"]), key))
    return sent_tops_list
        

#If each document contains at most 1 sentimented topic, get all sentimented topics with docID
def get_annotated_sent_tops_json(dic):
    sent_tops_list = []
    keys = [*dic]
    for key in keys:
        sent_tops_list.append(((dic[key]["topic_id"], dic[key]["sentiment"]), key))
    return sent_tops_list


#If each document contains at most 1 sentimented topic in JSON file
def get_all_topics_json(dic):
    tops_list = []
    keys = [*dic]
    di = {}
    for key in keys:
        tops_list.append((dic[key]["topic_id"], dic[key]["topic_words"]))
    return Convert(tops_list, di)


# Get the sentimented topics fromt he JSON file
def get_sentimented_topics_json(dict_elem):
    sent_tops_list = []
    for item in dict_elem:
        sent_tops_list.append((item["topic_id"], item["sentiment"]))
    return sent_tops_list


#If each document contains more than 1 sentimented topic
def get_topics_json(dict_elem):
    tops_list = []
    di = {}
    for item in dict_elem:
        tops_list.append((item["topic_id"], item["topic_words"]))
    return Convert(tops_list, di)

#If each document contains more than 1 sentimented topic
def get_all_multiple_topics_json(dic):
    keys = [*dic]
    topic_dic = {}
    for key in keys:
        topic_dic.update(get_topics_json(dic[key]))
    return topic_dic


# %% ----------- Get the text document in directory-----------


# Read results file
def read_result_file(file_name, dir_name):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, dir_name)  #..\..\software scripts\word-docs 
    with open(os.path.join(read_path, file_name)) as f:
        text = f.readlines()
        f.close()
    return text



# Find the associated documents by the list of sentimented topics returned by the selection algorithm
def find_document_by_sentimented_topics(list_sentimented_topic, annotated_sentimented_topics):
    docIds = []
        
    #Check for each item in the list of sentimented-topics for which document it corresponds
    for i in list_sentimented_topic:
        names = [x for x in annotated_sentimented_topics if i in x]
        docIds.extend(names)
        
    return docIds



# Fetch the actual text file corresponding with the docID
def get_text_document_by_ID(docID):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "clean-docs")  #..\..\software scripts\preprocessed
    
    # Get the number ID of the document
    digitID = re.search(r'\d+', docID).group()
    check_files = os.listdir(read_path)
    
    # Check for each file in the preprocessed directory
    for file in check_files:
        checkID = re.search(r'\d+', file).group()
        # Check if the query digit ID is the same as the checking file
        if digitID == checkID:
            return file
    # If no file has been found, then this file does not exist
    return "None has been found."



# Fetch the actual text file corresponding with the docID from single sent-top
def get_text_senttop(senttops_doc, dst_path):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "clean-docs")  #..\..\software scripts\preprocessed
    out_path = os.path.join(parent_dir, "result-docs")  #..\..\software scripts\preprocessed
    
    if not os.path.isdir(out_path):
        os.mkdir(out_path)
        
    dest_path = os.path.join(out_path, dst_path)
    print("Trying to write files to", dest_path)
    
    if not os.path.isdir(dest_path):
        os.mkdir(dest_path)
    
    file_name = senttops_doc[1]
    print(file_name)
        
        
    with open(os.path.join(read_path, file_name), encoding="utf8") as f:
        text = f.readlines()
        text = " ".join(text)
        f.close()
            
    print("Writing files to", dest_path)
    
    with open(os.path.join(dest_path, "result_" + senttops_doc[1]), 'w', encoding="utf-8") as output:
        output.write("Topic: " + str(senttops_doc[0][0]))
        output.write("\n")
        output.write("Topic words: ")
        output.write("\n")
        output.write(str(parse_topic_words(topics[senttops_doc[0][0]])))
        output.write("\n")
        output.write("Sentiment Score: " + str(senttops_doc[0][1]))
        output.write("\n")
        output.write(text)


# Fetch the actual text file corresponding with the docID
def get_text_senttops(senttops_docs, dst_path):
    path = dirname(abspath(__file__)) #..\..\software scripts\scripts
    parent_dir = dirname(path) #..\..\software scripts
    read_path = os.path.join(parent_dir, "clean-docs")  #..\..\software scripts\preprocessed
    out_path = os.path.join(parent_dir, "result-docs")  #..\..\software scripts\preprocessed
    
    if not os.path.isdir(out_path):
        os.mkdir(out_path)
        
    dest_path = os.path.join(out_path, dst_path)
    print("Trying to write files to", dest_path)
    
    if not os.path.isdir(dest_path):
        os.mkdir(dest_path)
    
    for senttops_doc in senttops_docs:
        file_name = senttops_doc[1]
        print(file_name)
        
        
        with open(os.path.join(read_path, file_name), encoding="utf8") as f:
            text = f.readlines()
            text = " ".join(text)
            f.close()
            
        print("Writing files to", dest_path)
            
        with open(os.path.join(dest_path, "result_" + senttops_doc[1]), 'w', encoding="utf-8") as output:
            output.write("Topic: " + str(senttops_doc[0][0]))
            output.write("\n")
            output.write("Topic words: ")
            output.write("\n")
            output.write(str(parse_topic_words(topics[senttops_doc[0][0]])))
            output.write("\n")
            output.write("Sentiment Score: " + str(senttops_doc[0][1]))
            output.write("\n")
            output.write(text)



        



