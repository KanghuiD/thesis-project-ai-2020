Coronavirus (COVID-19) - West Sussex County Council
                     
                         Find out how to get in touch online or by phone.
  Are you vulnerable and in need of urgent support because of COVID-19 or worried about someone who is? Access our Community hub page.
 Our Public Health team is working closely with Public Health England (PHE), which is leading the national COVID-19 response.
 The Government has enforced measures, telling people to stay at home.
 See the GOV.UK website for the latest information on the outbreak:
 Marriages and Civil Partnerships
 The Government advice means all weddings, civil partnership formations and appointments to give notice are postponed until, at least, 11 May 2020.
 If you have a ceremony or notice appointment in that period, someone in our team will be in contact to discuss your options.
 New bookings from January 2021 are being taken. However, we will not be able to take any 2020 bookings until we return to our offices and can take notices of intent.
 New Citizens – Citizenship Ceremony    
 If you have a letter from the Home Office advising you to attend a Citizenship Ceremony, you have a six-month window to attend from the date on your invitation letter. This has been extended from the standard three months.
 All West Sussex Libraries are now closed until further notice. However, our teams are working hard to ensure you can still access the service online. 
 View our current offer to read books, magazines and newspapers digitally, listen to audiobooks and learn from home. We are also working on filming some of our regular library activities, such as Toddler Rhyme Time, for you to use.
 Registration offices are currently closed in Midhurst, Burgess Hill, East Grinstead, Littlehampton and Storrington.
 Registering a death:
 All registration appointments will be conducted by telephone with immediate effect. Please book your appointment as normal, but do not attend the Registration Office.  
 Whilst we will do our best to contact you during the allotted half hour, this may not be possible, so please bear with us and you will get a call that day. 
 If the doctor has already handed you the Medical Cause of Death Certificate, please drop it in to one of the following registration offices, as soon as possible: Crawley, Chichester, Haywards Heath or Worthing. This must be done before the time of your appointment. 
 Registering a birth:
 Following Government guidance, we are currently unable to offer birth registration appointments. Further updates will be posted on our website.
 Parents can now make a claim for child benefit or universal credit, prior to the birth being registered. Until we are able to reinstate birth registration, we understand that you will not be able to register within the normal 42 days. 
 The West Sussex Record Office is now closed for foreseeable future.
 We have robust plans in place to ensure we can continue to deliver our emergency response.
 However, we are reducing some of our non-critical work in the community to help prevent the spread of the virus.
 Be assured: if you dial 999 and need the fire service, we will be there.
 Fire safety advice for businesses from West Sussex Fire & Rescue Service
 All Household Waste Recycling Sites (HWRS) across West Sussex are now closed until further notice.
 Please look for information and updates by your local district or borough council, as they operate these and will be able to advise of any disruption or changes.
 We have stopped delivering our normal offer from Children and Family Centres and Youth Centres, until further notice. This includes stopping all groups and events for children, young people and families.
 This service is currently running as usual and our staff are working hard to prioritise our vulnerable residents.
 For the time being, our monthly information sessions have been suspended.
 The government has put in place a range of measures to help local authorities deliver adult social care whilst managing the pressures caused by the current COVID-19 situation.
 These are changes to some local authority duties around assessment of people’s needs and their eligibility for council-funded care and support. These measures are known as ‘easements’ and local authorities were able to put these in place from 31 March 2020.
 They are temporary and will only be used if they become absolutely necessary. Please find out more with our useful guide, here together with an Easy Read version:
 However, we’re working on delivering a service for families and carers who fall into the keyworker category, and those who’s critical needs can’t be met any other way. 
 These have been suspended for the foreseeable future.
 Our deliveries are currently operating as normal and we're accepting new clients. Call 01903 718893 to find out more.
 We're working with Carers Support West Sussex, as well as the voluntary and community sector on increasing support to family and friend carers.
 During this time of national crisis there is some work that needs to continue to ensure we keep the county running both now, and as we come out of this period.
 That will include some construction particularly on key transport routes and in schools. Where this work continues, we are following Government advice and guidelines closely.
 The situation is under constant review and we are ensuring that any contractors working on our behalf are also complying with Government advice and following social distancing guidelines.
 We are working with the transport sector to understand what impact coronavirus (COVID-19) is having and how we may need to respond. Residents wishing to use public transport for essential travel should check with each operator before setting out on their journey.
 If you are unable to reapply for a blue badge, or your renewal application is delayed, Penalty Charge Notices will not be issued to residents using their badges with an expiry date of 1 January 2020 onwards.
 This relaxation of enforcement against expired blue badges should continue until 30 September 2020. We are unable to confirm if this applies to private car parks or areas outside of West Sussex.
 Social care and NHS workers can park free-of-charge in West Sussex County Council on-street spaces during the outbreak.
 Please display supporting evidence in your windscreen, such as a photocopy of your work pass (with sensitive information removed), or a letter of evidence from your employer.
 Please check with the bus operator for the latest details. 
 Pensioners and vulnerable people with West Sussex County Council bus passes can now use them earlier on weekdays. This is so they can take advantage of some supermarkets’ first hour of trading being dedicated to older and vulnerable shoppers.
 From now, and for the duration of the outbreak, residents can use their passes prior to 9.30am on weekdays. They could already use them at any time on weekends and public holidays. All bus pass holders are urged to please follow Government guidance on social distancing when travelling on public transport.
 There is no legislation that allows West Sussex County Council to close Public Rights of Ways, in response to COVID-19. People who choose to use local Public Rights of Ways for their daily exercise should follow the Countryside Code and adhere to social distancing guidelines.
 If you have Public Right of Way crossing your land, DEFRA has issued advice (below) on providing alternative routes on your land.
 We have suspended routine inspections and maintenance work and are only taking action on Public Rights of Way where reported issues require work to ensure public safety.
 The Visitor Centre and toilets at Buchan Park are closed. The car park remains open during the week, but this is being monitored and if numbers of visitors are deemed too high to enable social distancing, it will be closed.
 The car park is closed at weekends and on bank holidays. Local people can access the site via the Public Right of Way network and should adhere to social distancing guidelines.
 All interviews will be conducted via phone or video. If you have any questions, please contact our Recruitment Team.
 The latest guidance for social or community care and residential settings is available on the Government website:
 If you visit or work in a social, community care or a residential setting you should also follow Public Health England's hygiene and prevention advice.
 There are numerous resources and top tips available to support you with your wellbeing during the outbreak.
 Take a look at the advice on our website:
 The Government has also released guidance for the public, on the mental health and wellbeing aspects of coronavirus: 
 The West Sussex Wellbeing website also provides a range of information, particularly useful for people in later life who are looking to maintain good physical and mental health during the outbreak: 
 During the pandemic, it is important you remain aware of coronavirus related scams.
 The most common coronavirus related scams are as follows:
 For additional information on how to stay safe online, see below:
 The Government has asked parents to keep their children at home until further notice. Schools will only remain open only for those children whose parent(s) are classed as key workers, or children who are vulnerable.
 Parents whose children are continuing to attend school via public bus should visit the bus operators’ websites to check the times and routes available. Please note:
 Our suppliers have been asked to operate a full service from now until further notice. Due to possible staff shortages, some flexibility may be needed.
 All parents who do not accompany their children are advised to have a contingency plan agreed with their child in case transport is late or does not arrive.
 General guidance on health protection is available on the Government website.
 Public Health England’s School Zone also offers fun e-Bug resources aimed at primary school children, which share information on how to stop germs spreading.
 The Government is providing up to date COVID-19 guidance for employees, employers, and businesses.
 Government has also launched a package of measures to support businesses and the self-employed through the impact of COVID-19.
 If you're a business, employer or self-employed, visit the Business West Sussex website to access support and information.
 We have introduced measures to support our suppliers and the local economy during this time:
 There are a variety of COVID-19 resources available for you to download and print, including advice on staying at home, hand washing and information for children:
 Easy-read resources, specifically related to coronavirus, are also available:
 Translated guidance
 The UK Government has translated the stay at home guidance into a range of languages.
 Newsletter
 Sign up to our newsletter and receive regular updates on COVID-19. Here you can also choose to be notified of general news in West Sussex: 
 We've joined forces with local community groups to help and support vulnerable people during the coronavirus outbreak.
 See our community hub to:
 https://www.westsussex.gov.uk/campaigns/coronavirus-covid-19/
