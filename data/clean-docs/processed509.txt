Coronavirus: Pence orders review of medical aid procedures overseas
 Thanks for contacting us. We've received your submission.
 News
 By Mark Moore
 Submit 
 		April 1, 2020 | 10:24am			
 Vice President Mike Pence ordered a review of aid procedures to other countries, after a phone call revealed a Trump administration official requested medical gear from a foreign country the US was supplying with the critically needed equipment, according to a report.
 Pence, who’s leading the White House coronavirus task force’s efforts, placed a moratorium on overseas shipments of USAID’s stockpiles of protective equipment and wants the supplies sent to the US instead, Politico reported late Tuesday.
 The task force will also examine all of USAID’s deliveries to countries asking for personal protective equipment that US health experts say is direly needed in American hospitals to treat patients infected with the coronavirus.
 Pence also ordered his staff to ensure the review process wasn’t delaying coronavirus aid to countries helping the US.
 The incident that prompted the actions happened last week when a Trump administration official called counterparts in Thailand requesting protective gear for doctors and nurses. The Thai official responded by saying a second USAID shipment of the same supplies was on its way to Bangkok.
 Trump administration staffers immediately put the shipment on hold.
 They feared that such snafus could either confuse allies or even offend them and outrage Americans who are hearing daily reports of shortages of protective equipment at home amid a rising death toll.
 The report, citing an official close to USAID, said the review is more likely a hold as the task force examines the procedures.
 “They’re really trying to walk a fine line between making sure Americans get everything they need and then starting to provide assistance elsewhere, and the vice president’s oversight is slowing down the decision-making process,” the person told Politico.
 Read Next
 				CBS admits to using footage from Italy in NYC coronavirus ...			
 Share Selection
 Stay informed with our Coronavirus Daily Update newsletter.
 Read Latest Updates
 This story has been shared 138,103 times.
 138,103
 This story has been shared 96,997 times.
 96,997
 This story has been shared 82,067 times.
 82,067
 This story has been shared 76,261 times.
 76,261
 Your Ad Choices
 Sitemap
 California Privacy Rights
 Do Not Sell My Personal Information
 			Would you like to receive desktop browser notifications about breaking news and other major stories?		
 				Not Now			
 				Yes Please			
 https://nypost.com/2020/04/01/coronavirus-pence-orders-review-of-medical-aid-procedures-overseas/