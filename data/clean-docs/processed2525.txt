Italy, France record lower coronavirus deaths: Live updates |  News | Al Jazeera
 France records 357 deaths, lowest daily increase in a week; Italy reports 525 deaths, the lowest in more than two weeks.
 France and Italy have recorded their lowest death toll from the coronavirus in one and two weeks, respectively.
 Meanwhile, British Prime Minister Boris Johnson was taken to a hospital for tests after showing persistent symptoms of coronavirus 10 days after testing positive for the virus.
 After President Donald Trump warned the United States is entering what could be its "toughest" week yet, New York reported 594 new deaths and 8,327 coronavirus cases in the past 24 hours.
 Globally, the death toll surpassed 68,000, according to data compiled by Johns Hopkins University, and the number of infections rose above 1.26 million.
 Here are the latest updates:
 // <![CDATA[
 // ]]]] >
 British Prime Minister Boris Johnson was admitted to hospital for tests after showing persistent symptoms of coronavirus 10 days after testing positive for the virus.
 "On the advice of his doctor, the Prime Minister has tonight been admitted to hospital for tests," Downing Street said in a statement.
 "This is a precautionary step, as the Prime Minister continues to have persistent symptoms of coronavirus 10 days after testing positive for the virus."
 Read more here.
 In a rare broadcast to the nation, Queen Elizabeth thanked healthcare workers on the front line and urged a united effort to overcome the crisis, vowing "we will succeed".
 In what was only the fifth televised address of her 68-year reign, the 93-year-old monarch drew on her experience in World War II and called upon Britons to demonstrate they were as strong as past generations.
 "Together we are tackling this disease, and I want to reassure you that if we remain united and resolute, then we will overcome it," she said in the address from her Windsor Castle home.
 Officials in Greece have placed a second migrant camp near Athens under lockdown after an Afghan resident tested positive for the coronavirus, the migration ministry said.
 Ethiopia reported the first two deaths of patients infected with coronavirus, as officials ramped up testing.
 The first victim was a 60-year-old Ethiopian woman who had spent six days in intensive care, a health ministry statement said.
 The second victim was a 56-year-old Ethiopian man diagnosed with COVID-19 last Thursday.
 France recorded 357 coronavirus deaths in hospitals in 24 hours, the lowest daily increase in a week, bringing the total death toll to 8,078.
 The tally included 5,889 patients who died in hospitals and 2,189 people in old age homes and other medical facilities, a government statement said.
 There are now 28,891 people infected with coronavirus in hospitals, an increase of 748 from the day before.
 Qatar reported 279 new cases of coronavirus and one more death in the past 24 hours, raising the total to 1,604 cases and four deaths.
 Turkey recorded 73 new deaths, raising the total number of deaths to 574, according to Turkey's Health Minister Fahrettin Koca.
 Confirmed cases rose by 3,135, bringing the total to 27,069 in the last 24 hours.
 Koca added that 20,065 tests were conducted in the past day.
 Italy recorded its lowest daily death toll from the coronavirus in more than two weeks, while the number of critical care patients declined for the second day.
 The 525 fatalities reported by the civil protection service was the lowest number since the 427 deaths recorded on March 19.
 In nearly every White House news briefing on the COVID-19 pandemic during the first week of March, healthy young people were reassured that if they were infected, they would likely have mild symptoms.
 Some briefings included additional information on how "individuals with underlying health conditions can protect themselves using common-sense precautions".
 Yet, it is healthy, young people who have enabled the transition of this novel coronavirus from an outbreak to a pandemic.
 Read about it here.
 New York, the hardest-hit US state, reported 594 new deaths in 24 hours, bringing the total death toll to 4,159.
 A total of 8,327 new coronavirus infections were recorded in the US in the past day, raising the total number of cases to 122,031, according to Governor Andrew Cuomo.
 Coronavirus deaths in Canada jumped by over 20 percent in a day to 258, according to official data by the public health agency.
 By 15:05 GMT, the total number of cases rose by almost 12 percent to 14,426.
 On Saturday, there were 12,924 cases and 214 deaths.
 Mahmoud Jibril, the former head of the rebel government that overthrew Libya's Muammar Gaddafi in 2011, has died of the coronavirus infection, his party said.
 Jibril, 68, died in Cairo where he had been admitted to a hospital for two weeks, said Khaled al-Mrimi, secretary of the National Forces Alliance, founded by Jibril in 2012.
 Egypt expects economic growth to slow to 4.5 percent in the third quarter and to one percent in the last three months of the 2019-20 fiscal year to June due to the coronavirus, Planning Minister Hala al-Saeed said.
 The government had been targeting annual growth of 5.6 percent, but is now looking at 4.2 percent, she said.
 Is COVID-19 a sexist disease? It appears that it may well be, as more men than women are being reported as victims of the virus.
 The severe acute respiratory syndrome (SARS) and the Middle East respiratory syndrome (MERS) outbreaks were also found to affect men more significantly.
 Read more about it in our latest Doctor's Note by Dr Sara Kayat here. 
 The United Kingdom's death toll from the coronavirus rose by 621 to 4,934 on April 4, the health ministry said.
 A total of 195,524 people have been tested of which 47,806 tested positive, the ministry said.
 ____________________________________________________________________
 This is Mersiha Gadzo in Doha taking over the live updates from my colleague Usaid Siddiqui.
 Albania reported 28 new cases of the new coronavirus and said a failure to respect social distancing had led to the highest numbers of infections over the last three days.
 The COVID-19 disease caused by the coronavirus has killed 20, infected 361 and caused three to need help breathing, the Public Health Institute said. It added that 104 had recovered.
 The United Arab Emirates' cabinet approved strengthening the country's "strategic stockpile" in response to the coronavirus outbreak, the vice president and prime minister tweeted after a cabinet meeting, without specifying which goods.
 Sheikh Mohammed bin Rashid Al Maktoum, who is also the ruler of regional tourism and business hub Dubai, said authorities had also directed factories to support the needs of the country's health sector.
 The novel coronavirus has infected another 658 people in Russia, bringing the total to 5,389 over the past 24 hours, the country's authorities said.
 The death toll rose to 45 as the infection claimed two more lives since Saturday, an emergency team said in a statement. More than half of the patients are under the age of 45.
 A 63-year-old man was shot dead in the Philippines after threatening village officials and police with a scythe at a coronavirus checkpoint, police said on Saturday. 
 The man is believed to have been drunk when he threatened village officials and police manning the checkpoint in the town of Nasipit in the southern province of Agusan del Norte on Thursday, a police report said. 
 South Sudan reported its first coronavirus case, one of the last African nations to confirm the presence of COVID-19 within its borders.
 "South Sudan confirms one case of coronavirus," Riek Machar, the country's first vice president, told a press conference in the capital, Juba.
 Machar identified the patient as a 29-year-old woman who arrived in South Sudan from the Netherlands via Ethiopia on February 28. Her nationality was not given.
 Palm Sunday was privately marked in Jerusalem after the traditional mass procession was called off amid coronavirus fears.
 The mass was streamed online from the Latin Patriarchate Co-Cathedral in Jerusalem.
 Inside the church, the ceremony was led by Bishop Giacinto-Boulos Marcuzzo, with the number in attendance limited to 10.
 In Israel, more than 8,000 people have contracted the coronavirus and 46 have died.
 More than 300 pilots in the Czech Republic have joined forces to form a group of volunteers using their private planes to distribute medical equipment across the country.
 The "Pilots to the People" project is meant to help state authorities fighting the epidemic of the coronavirus “to deliver supplies to any place in the country as soon as possible".
 The service is offered free of charge and the pilots pay for the fuel. They are using a network of some 200 airports across the country.
 Spain saw its third consecutive daily decline in the number of people dying from COVID-19 as the country recorded another 674 deaths, official figures showed.
 The health ministry said the number, the lowest in 10 days, brought total deaths to 12,418 since the pandemic hit Spain. The number of infections rose 4.8 percent to 130,759.
 The coronavirus death toll in Iran has reached 3,603, health ministry spokesman Kianush Jahanpur said in a statement broadcast live on state TV.
 Over the past 24 hours, 150 people died of COVID-19, he said.
 Iran is the Middle Eastern country worst hit by the epidemic, having recorded a total of 58,226 infections so far, Jahanpur said.
 Jahanpour said 22,011 people have so far recovered, while 4,057 remain in a critical condition.
 Malaysia has reported four new deaths and 179 new confirmed coronavirus cases, taking its total tally to 3,662 infections.
 The United Kingdom will have to further restrict rules allowing outdoor exercise if people flout lockdown rules designed to combat the coronavirus, Health Secretary Matt Hancock said.
 "I don't want to have to take away exercise as a reason to leave home ... if too many people are not following the rules," he told the BBC's Andrew Marr program.
 "At the moment, the vast majority of people are. But we should not break the rules because that would mean that the virus spreads more and we then might have to take further action," he added.
 The Central Bank of the United Arab Emirates said it has doubled to $70bn a stimulus package to support the Gulf state's economy amid the coronavirus pandemic.
 "The aggregate value of all capital and liquidity measures adopted by the CBUAE since 14 March 2020 has reached 256 billion dirhams [$70bn]," the central bank said in a statement.
 Last month, the oil-rich UAE announced stimulus measures worth $35bn that included aid to the banking system, facilities for loans and injecting funds into stock markets.
 More than 130 people were newly infected with the novel coronavirus in Tokyo, Japan's NHK public broadcaster reported, citing officials from the metropolitan government.
 It was the highest daily jump in confirmed cases so far, bringing the number of positive cases in the capital to more than 1,000, NHK said.
 Tokyo's metropolitan government has strongly urged people to stay home as the city of 13 million has seen an uptick in the number of cases in recent days.
 In a virtual Asteroid Day event held recently, Romanian cosmonaut Dumitru Prunariu illustrated to his fellow spacefarers the challenge millions of people across the globe now face: the psychological risks of long-duration isolation, in a confined space, away from friends, colleagues, and even family, in a dangerous environment. 
 Ecuador's vice president Otto Sonnenholzner apologized after dozens of bodies were left on the streets of Guayaquil as coronavirus ravages the port city whose residents had published videos on social media showing abandoned bodies in the streets.
 Authorities collected at least 150 bodies from streets and homes earlier this week but did not confirm how many of the dead were victims of the outbreak.
 "We have seen images that should never have happened, and as your public servant, I apologize," said Sonnenholzer, who is heading the country's virus response, in a statement broadcast by local media on Saturday.
 Priests delivered blessings from the back of trucks and motorised tricycles in the Philippines, adapting the deeply Catholic nation's traditions to the battle against coronavirus.
 People lined up in front of their homes in a district of Manila, which is entering its fourth week of a lockdown that has brought the frenetic metropolis nearly to a halt.
 The priests made signs of the cross as they rolled past waving residents marking Palm Sunday, the start of the week that culminates with Easter.
 More than 1.2 million cases of coronavirus have been confirmed worldwide, according to Johns Hopkins University.
 Some 64,774 people have died globally, while over 246,000 people have recovered.
 Greece has quarantined a second migrant facility this week after a 53-year-old man tested positive for coronavirus, the migration ministry said.
 The Afghan man, who was found to be infected, lives with his family at the Malakasa camp along with hundreds of other migrants and asylum seekers. He has been transferred to a hospital in Athens. Tests on his contacts will continue as the public health agency tries to trace the route of the virus.
 On Thursday, authorities quarantined the Ritsona camp in central Greece after 20 asylum seekers tested positive for coronavirus. It was the first such facility in Greece to be hit since the outbreak of the disease.
 Hello, this is Usaid Siddiqui in Doha taking over from my colleague Zaheena Rasheed in Male, Maldives.
 New Zealand's Prime Minister Jacinda Ardern says tough social distancing measures are working as the number of infections in the Pacific nation rose above 1,000.
 Only one person has died in New Zealand, despite the country recording 1,039 coronavirus cases.
 "Our case rate and death toll is well below other comparable countries," says Ardern.
 "Yes, we had the benefit of time because our distance and because our early border and mass gathering measures also made a difference there."
 There is a growing concern in Indonesia over the safety of front-line workers amid reports that some medical staff are having to make up for a lack of personal protective equipment with plastic raincoats and bin bags.
 The Southeast Asian country has one of the highest COVID-19 mortality rates in the world, at approximately 9 percent, slightly lower than Italy's. The victims include more than a dozen healthcare staff.
 Gotabaya Rajapaksa, Sri Lanka's president, says he has released 2,961 inmates in the past two weeks as part of an effort to reduce congestion in prisons during the COVID-19 pandemic.
 These steps were taken as a result of appeals made by inmates during my visit to the prison in February & will help to reduce congestion in view of #covide19 pandemic
 The New York Times and Politico are reporting that the US Centers for Disease Control (CDC) has begun conducting tests to find out how many Americans have been infected with SARS-Cov-2, the virus that causes COVID-19.
 The test detects antibodies the immune system makes to fight the virus, and such testing can help determine how widespread the disease has been and whether there have been significant numbers of people who were infected but did not become ill, according to the NYT.
 Christos Christou, president of Doctors Without Borders (Medecins Sans Frontieres, or MSF), is calling on European governments to stop hoarding medical supplies, including personal protective gear, and to allow their export to vulnerable countries in Southeast Asia, the Middle East and Africa.
 "We need to scale up the manufacturing capacity of all the essential medicines and all medical material. And while we expand the supply, we have to guard against the nationalistic responses we've seen so far that hoard the medical supplies and limit the exports," he tells Al Jazeera.
 Guillaume Faury, chief executive of Airbus, says the company brought four million masks from China to Europe as part of its efforts to support governments in the fight against COVID-19.
 The delivery is part of Airbus's so-called airbridge mission between China and Europe.
 Andrew Cuomo, governor of New York, says Chinese billionaires Jack Ma and Joseph Tsai have donated 1,000 ventilators to help in the state's fight against the coronavirus.
 "It's going to make a significant difference for us," he says, adding that the US state of Oregon is also volunteering to send 140 more breathing machines.
 Guatemala's President Alejandro Giammattei is banning internal travel as well as spending time at the beach before and during Easter to stem the spread of the coronavirus.
 In a national broadcast on Saturday, Giammattei said travel will only be allowed for work reasons and said sales of alcohol to the public would also be prohibited.
 With no clear indication as to when they can return to business-as-usual, some bars and restaurants in New York City are boarding up their windows and doors, reports Al Jazeera's Radmilla Suleymanova.
 Laundromats, coffee shops, and even doctor's offices - open last week as essential businesses - have since closed or cut back hours.
 The sidewalks are lightly dotted with people waiting for their take-out orders - a New York habit even the ugliest of pandemics cannot seem to break. A growing number are clad in pale blue surgical masks and cloth coverings.
 Those delivering lunches and dinners on bicycles, unloading packages off trucks and hauling boxes into groceries rule the streets. Once relegated to the shadows, they are now centre stage and this city's lifeline.
 China's National Health Commission says it recorded 30 new coronavirus cases in the mainland on Saturday.
 The figure includes 25 cases imported from abroad and is a rise from the 19 cases reported a day earlier.
 Trump warned that the US is facing its "toughest" week in the battle against the new coronavirus amid a rise in the number of infections and deaths.
 "There will be a lot of death, unfortunately," he said.
 He added that the US military will soon deploy soldiers and medical personnel to hard-hit states, including about 1,000 to New York state.
 "We are going to be adding a tremendous amount of military to help supplement the states. Thousands of soldiers, thousands of medical workers, professional nurses, doctors."
 Hello, this is Zaheena Rasheed in Male, Maldives, with Al Jazeera's continuing coverage of the coronavirus pandemic.
 You can find all the updates from yesterday, April 4 here.
             Al Jazeera and news agencies
 	 
 https://www.aljazeera.com/news/2020/04/trump-warns-lot-death-covid-19-battle-live-updates-200404232003006.html
