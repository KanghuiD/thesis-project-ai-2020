Hmmm, we can't seem to find the page you're looking for
 logged-in-corporate-menuYou are currently accessing Investment Week via your Enterprise account.
 If you already have an account please use the link below to sign in.
 If you have any problems with your access or would like to request an individual access account please contact our customer service team.
 Phone: +44 (0) 1858 438800
 Investment Week are delighted to be hosting a virtual quiz in aid of helping heroic frontline NHS staff tackling the coronavirus crisis, taking place at 4pm on 22nd April and hosted by a special industry guest.
 To take part you will be asked to choose a donation fee with all proceeds being donated to CASCAID’s NHS fundraising campaign (minimum donation is £15).
 There will be a mixture of questions so make sure you get the family involved to cover all bases, and like any good pub quiz there will be prizes on offer for first, second and third (prizes to be announced soon).
 Investment Week is delighted to launch a virtual version of the successful Investment Week Select event!
 In these challenging times Fund Buyers need access to Portfolio Managers more than ever, yet physical meetings are clearly impossible.
 In response to this, we are delighted to announce the launch of Investment Week Virtual Select – bringing buyers and sellers together from the safety of their own homes.
 Investment Week Select had a very successful launch in February 2020 where fund buyers enjoyed the ability to choose their own agenda from a selection of Portfolio Managers.
 Sign up
 In this exclusive magazine exploring the evolution of quality and income ETF strategies, King reveals that each ETF follows an investment strategy developed by the group's in-house research team that leverages fundamental active insights to inform the factor definitions and applies portfolio construction principles to mitigate the unintended biases.
 David Cumming, Aviva Investors' chief investment officer for equities, last year witnessed turbulent times for UK equities but he remains positive about the market in which he has a personal as well as a professional stake.
 The page you have requested may be outdated or is not available on our website!
 You can try going back to the previous page, use the search option above or start again from the homepage
 https://www.investmentweek.co.uk/analysis/4012777/coronavirus-blog-without-our-private-sector-there-will-be-no-economy-to-speak-of-boris-johnson
