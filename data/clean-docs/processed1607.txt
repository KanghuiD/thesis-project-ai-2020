COVID-19 (Coronavirus) Information
 UW-Whitewater cares deeply about the health and safety of our community.
 Senior leadership is meeting regularly regarding COVID-19 and has been doing so since mid-January. Campus leaders continue to follow the Centers for Disease Control and Prevention, as well as county and state public health protocols for dealing with the virus. UW-Whitewater has extensive emergency management plans, which address these situations.
 COVID-19 is not specific to an ethnicity or race—disease does not discriminate. Please remember our campus values as you interact with others. Racist behaviors, discrimination or stereotyping are not tolerated at UW–Whitewater. If you experience harassment or discrimination, students, faculty and staff are encouraged to file a hate bias report.
 The university is not closed, but operations have been changed to prioritize the safety of our campus community and the continuation of course delivery.
 To lessen the risk to our community as much as possible, UW-Whitewater has suspended spring semester face-to-face instruction, moving entirely to alternative delivery through the end of the semester. Students will receive additional information as it is available. ICIT is offering a guide for working and learning remotely.
 As part of UW-Whitewater’s continued effort to address the COVID-19 pandemic, all campus buildings will be locked starting at 5 p.m. Friday, March 20. This step will allow facilities staff to disinfect/sanitize buildings, while keeping them safe and secure. Buildings will be accessible to those who have a key card that allows access into the buildings.
 Public spaces such as the University Center, Williams Center, Andersen Library and the Community Engagement Center are closed to the public in keeping with recommendations to limit mass gatherings.
 There is no change in the timeline for registering. Reach out to your academic advisor for questions or concerns.
 A decision on spring commencement will be made at a later date, but given the gravity of this situation and the orders from Gov. Evers and the Centers for Disease Control and Prevention to limit gatherings to no more than 10 people, we may also be looking at an alternative delivery of commencement as well.
 Remember, there is no ceremony requirement to receive your degree. If you are approved for graduation in May and complete your degree requirements, your graduation date will still be May 2020, even if we do not have traditional commencement ceremonies.
 Again, no final decision has been made on spring commencement and we will announce the decision at a later date.
 Placement tests will not need to be completed. SOAR dates, which begin May 28, are still in place. Transfer SOAR dates, April 10 and May 8, will be offered in an alternative format.
 Please contact your instructor or department chair. They will have information specific to the needs based on your college. As long as students successfully complete courses, this change in delivery will not impact the path to graduation.
 Contact ICIT or the help desk at 262-472-4357 or helpdesk@uww.edu.
 A change in grading policy is being discussed and a decision will be made soon.
 Whitewater campus Textbook RentalThe rental return deadline for the spring semester has been extended to June 2. For your safety, we recommend shipping your books to Textbook Rental via media mail.
 Our address is 724 W. Starin Rd. Whitewater, WI 53190.
 If you don’t want to ship, we will be open from 8 a.m. to 4:15 p.m. with a designated drop off area to safely return your books.
 Rock County campus Textbook RentalThe rental return deadline for the spring semester has been extended to June 2. For your safety, we recommend shipping your books to Rock County Textbook Rental via media mail.
 Our address is 2909 Kellogg Ave, Janesville, WI 53546
 For any in-store pick up or drop off questions e-mail bookstore@uww.edu
 We ask all students to leave the residence halls and return home. We recognize that some students may be unable to return to their permanent residence for various reasons and will need to stay in their residence halls. For instance, some international students will not be able to return home at this time, and some may need to stay at UW-Whitewater for other reasons. 
 University of Wisconsin System President Ray Cross announced that all UW System institutions will refund the prorated charges for housing and dining for the remainder of the spring semester for students who have vacated their respective universities. Prorated reimbursements will exclude the period of the originally scheduled spring break and shall be issued by the close of the spring academic semester. 
 Williams Center is closed. If you need to collect your belongings, please call the Williams Center at 262-472-1145. Reimbursements are being discussed at the UW System level and information will be communicated as it is available. 
 Andersen Library is closed. Staff is still available to help, so please call 262-472-5511 and they will work with you to fulfill your needs. Due dates for books have been extended, so contact the library for more information on how to return your books.
 Please contact your supervisor for further direction.
 If you are sick, stay home.
 If you develop symptoms — such as a fever, cough or shortness of breath — within 14 days of your return from personal or official travel to a  country with a COVID-19 outbreak  OR have been in direct (face-to-face) contact with someone who has confirmed COVID-19, please take the steps listed below.
 See information above about preventing the spread.
 People with preexisting health conditions are at higher risk to develop complications from a COVID-19 infection. Your health is the top priority, so public health officials may recommend that you stay home if there are growing numbers of those infected with the virus.  
 Please see guidelines for Cloth Face Covering (PDF).
 Counseling sessions have been moved to phone calls. If you need information on how to set up a counseling session, please call 262-472-1300.
 Please know that there are free meals and food resources available within the community and at our Warhawk Pantry in Drumlin Hall. Review the list of free food and meal options in Whitewater.
 All university-sponsored travel is cancelled through April 17, with limited exceptions from deans, vice chancellors and the athletic director.
 We strongly advise you to reconsider non-essential personal travel, including travel over Spring Break. Documented cases are growing rapidly both domestically and internationally. You may face a higher risk of infection, significant delays returning, and/or the requirement to self-isolate or self-quarantine upon your return, all of which could significantly impact your professional and personal obligations at great individual expense.
 All campus community members should be aware that if you travel, you may be required to self-quarantine for 14 days depending on where you’re traveling to and from, even if you do not exhibit symptoms.
 UW-Whitewater Campus events from now through April 31 have been cancelled or postponed due to COVID-19. The health and safety of our students, faculty, staff and visitors are our first priority. Listed below are the important event updates. Any questions or concerns reach out to:
 Nicole Caine  Event Planner, Office of the Chancellor cainen@uww.edu
 Emergency/Urgent Care ResourcesMy UHCSMy UHCS InstructionsAppointmentsInternships and InvolvmentFAQ's
         
  How are we doing?
  Submit a Question
 See all our accreditatons »
 https://www.uww.edu/uhcs/covid19