'Enhance or change': Coronavirus lockdown review looms | UK News | Sky News
 By  Jon Craig, chief political correspondent and Alan McGuinness, political reporter  
 Thursday 9 April 2020 16:46, UK
 Senior ministers will decide whether to "enhance or change" UK lockdown rules at an emergency summit next week, one of them has told Sky News. 
 Culture Secretary Oliver Dowden said Britons had to "stick to the path that we’ve chosen" and stay at home over the Easter weekend until the review.
 He confirmed a crunch meeting this afternoon will "determine the process" for how the COVID-19 lockdown extension decision will be taken while Prime Minister Boris Johnson is still in intensive care.
 "Instead keep in touch via video call or over the phone."
 Wales has already declared the measures there will last longer than the three week period initially ordered by the UK government, which is due to expire on Monday.
 Now Scotland looks set to follow suit, with First Minister Nicola Sturgeon saying she agrees with the move.
 “I don’t think there is any possibility, any likelihood of these lockdown measures being lifted immediately or even imminently,” she told Kay Burley@Breakfast.
 “We don’t yet really have enough data from what has happened so far to know for sure the impact they’re having.
 “None of us want these measures to be in place for a single minute longer than they have to be but it’s really important that we stick with it.”
 Jason Leitch, the Scottish government’s national clinical director, suggested the same, telling All Out Politics “for sure” the lockdown would continue for another three weeks.
 “I can't give you a date, but I can tell you it's a long haul and it won't stop suddenly. Just as we had a curve on the way in, we'll have a curve on the way out."
 :: Listen to the Daily podcast on Apple Podcasts, Google Podcasts, Spotify, Spreaker
 Hopes of an end to the shutdown of pubs, restaurants, shops and other businesses were all but dashed on Wednesday by Chancellor Rishi Sunak.
 Labour leader Sir Keir Starmer has called on the government to set out what its strategy for exiting the lockdown is.
 "I'm not calling for precise timings, but the strategy. This is incredibly difficult on people and we need to know that plans are in place, and what they are," he said.
 Government medical and scientific advisers are still unsure when there will be a peak in the number of people falling ill with the coronavirus, and in the number of fatalities.
 With the Easter weekend looming - and soaring temperatures forecast for Good Friday and Saturday - experts are concerned that failing to maintain instructions to stay at home could prove catastrophic.
 It was confirmed on Wednesday that another 938 COVID-19 patients have died in UK hospitals, taking the total to 7,097.
 Mr Johnson has now spent a fourth night in hospital, the latest update from Downing Street issued just before 7pm on Wednesday saying: "The prime minister continues to make steady progress. He remains in intensive care."
 A few hours earlier, at the latest Downing Street coronavirus news conference, Mr Sunak said: "The latest from the hospital is the prime minister remains in intensive care where his condition is improving.
 "I can also tell you that he has been sitting up in bed and engaging positively with the clinical team."
 At the news conference, the government's deputy chief scientific adviser Angela Maclean hinted that schools could reopen before the lockdown restrictions are eased.
 Asked about schools reopening before the summer, she said: "There is very interesting work into all sorts of different things that we might do in the next stage."
 When the prime minister announced the lockdown in his televised address on 23 March, he told the nation: "I can assure you that we will keep these restrictions under constant review. We will look again in three weeks, and relax them if the evidence shows we are able to."
 This three-week period ends on Easter Monday, and the Coronavirus Act rushed through parliament last month states that the government has until next Thursday to review the lockdown.
 Although there will be no formal announcement of a decision after the COBRA meeting, it is expected that Mr Raab will warn the public to prepare for an extension of the lockdown at the Downing Street news conference this afternoon.
 Presenting evidence at the COBRA meeting on the effectiveness of the lockdown so far will be England's chief medical officer Professor Chris Whitty, and chief scientific adviser Sir Patrick Vallance.
 Senior police chiefs will also report on lockdown enforcement and make recommendations on whether any easing of restrictions is possible or - more likely - tougher curbs on movement are needed.
 https://news.sky.com/story/coronavirus-no-end-in-sight-senior-ministers-expected-to-extend-uks-lockdown-11970798
