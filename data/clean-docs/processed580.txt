	Coronavirus Updates
 All notifications about the coronavirus, UND’s response to it, or any impact on university classes or operations, will be communicated here.
 The FAFSA will help UND prepare the best financial aid package available for students.
 Watch the April 21 faculty and staff meeting. 
 New cases reported on the UND campus are posted once a day, before noon, Mon - Fri.
 Support our students during this time of crisis.
 During this unprecedented time of uncertainty, sudden changes, social distancing, and stress, UND is especially concerned for our students and employees who are experiencing or have experienced sexual assault, domestic…
 Following is information from the North Dakota Joint Information Center. BISMARCK, N.D. – The North Dakota Department of Health’s Coronavirus website has been updated to improve navigation and provide new resources….
 During these unprecedented times, your health and safety are our main concerns. UND expects all students to practice our physical distancing protocol to protect themselves and others from COVID-19. UND values…
 General: UND.inforequest@UND.edu
 Housing: housing@UND.edu
 Visit the UND calendar to find out if the event was canceled, postposed or moved online.
 Open Meeting Requirements (PDF)
 North Dakota Department of Health
 866.207.2880
 7 a.m. – 7 p.m. Monday – Friday
 Altru Health Systems
 701.780.6358 (24/7)
 Crisis Text Line
 Text HOME to 741741
 http://blogs.und.edu/coronavirus/
