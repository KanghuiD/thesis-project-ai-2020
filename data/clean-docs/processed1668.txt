Inside the Beltway: Move over Russiagate: The news media now longs for Lysolgate - Washington Times
 Sign In
 Coronavirus hype biggest political hoax in history
 Dictator Trump, enemy of the press, hurt by openness to press
 How previous generations would respond to COVID-19 pandemic
 You remember Russiagate and Spygate, of course. Well, welcome to Lysolgate, the newest media creation meant to erode President Trump’s favorability ratings among voters and lessen his chances of reelection. The press is still obsessing over Mr. Trump’s recent stray remarks about the use of disinfectants during the coronavirus pandemic.
 TOP STORIES
 Barr orders legal action against governors whose COVID-19 actions infringe on civil rights
 Illinois governor exceeded authority with stay-at-home order, judge rules
 Firm tests UV light treatment that Trump was mocked for mentioning
 Scott McKay, a columnist for The American Spectator, points out that Mr. Trump should be credited for thinking outside the box during the crisis — and to do so is not a wrongful act.
 “What’s most obnoxious about all of this is while Trump is being treated like a snake-oil salesman or a moron for discussing things like this — the way the media treated him for expressing optimism about hydroxychloroquine, while doctors all over the country were busy prescribing it to largely good effect, was nothing short of disgusting — it seems like he’s the only one who actually cares about finding effective treatments for the virus,” writes Mr. McKay.
 “These geniuses in the mainstream media and their pals in the Democrat party sure are big on telling everybody to stay under self-imposed house arrest, but perhaps your author is the only one noticing they trash every single suggestion for how to treat this thing. Maybe none of these methods work. Or maybe the media and the Left actually like the shutdown. And what would it say about them if that was the case?” asks Mr. McKay.
 REALITY CHECK
 The New York Times recently suggested President Trump lingers in his personal quarters until noon. That claim has been refuted by the White House.
 “President Trump’s schedule is so packed amid the coronavirus crisis that he sometimes skips lunch, his aides told The Post — refuting a report that the commander-in-chief spends his days obsessing over TV coverage and eating fries,” points out the New York Post.
 “White House staffers said the president works around the clock and can make five dozen work-related calls a day during the pandemic,” said the Post, which also examined White House call records to support that claim.
 “We watch him work and work and work, and we get frustrated by the inaccurate coverage, which does not reflect the hard work and leadership we witness every day,” an official told the Post.
 TAKE THEM OUT TO THE BALLGAME
 A round of applause, please, for Fox News prime-time host Sean Hannity, who has purchased 500 Yankees tickets for New York City health-care workers. Mr. Hannity also recruited Yankees president Randy Levine and owner Hal Steinbrenner to donate another 500 free tickets.
 He’s not done yet, and plans to recruit other pals to chip in as well
 “I now challenge my friends Lara Trump and Eric Trump,” Mr. Hannity tweeted Monday.
 GRAHAM’S REMINDER
 Evangelist Franklin Graham has a message for former Democratic National Committee chairwoman Donna Brazile, a current contributor to Fox News who penned an op-ed for the network advising that “saving lives is a priority” during the coronavirus crisis.
 “Already this year, the lives of more than 270,000 babies have been taken by abortion in this country, and according to Right to Life of Michigan, ‘Abortion has taken more Black American lives than every other cause of death combined since 1973.’ Let’s make protecting all lives a priority,” the pastor advised.
 TWO OUT OF 53
 So what’s in a headline? Agenda, if it appears in one particular newspaper.
 “In the 100 days since the coronavirus epidemic began to receive significant news coverage, The Washington Post has used its front page to undermine the administration’s response to the unprecedented crisis, with dozens of headlines flagrantly editorializing against President Trump and nearly everyone on his team,” writes Rich Noyes, research director for the Media Research Center, a conservative press watchdog.
 He reviewed the headlines of all 136 Washington Post front-page news stories about the federal response to the coronavirus from Jan. 17 through April 25.
 And just a few examples of those headlines:
 “A confused effort to show calm control” (March 1); “Trump’s error-filled speech rattled rather than reassured” (March 13); “White House’s chaotic response evokes Trump’s early days” (March 15); “70 days of denial, delays and dysfunction” (April 5); and “Hostility to criticism, hunger for praise are central to his response” (April 9).
 POLL DU JOUR
 • 60% of Americans would approve people voting in the presidential election by mail-in vote; 46% of Republicans and 73% of Democrats agree.
 • 48% overall would prefer allowing only mail-in voting; 37% of Republicans and 60% of Democrats agree.
 • 46% overall would prefer allowing in-person voting if coronavirus is still a threat; 58% of Republicans and 38% of Democrats agree.
 • 44% overall would prefer allowing votes to be submitting entirely online through a website; 33% of Republicans and 50% of Democrats agree.
                         
                           Click
                             here for reprint permission.
  Click to Read More
  Click to Hide 
 https://www.washingtontimes.com/news/2020/apr/27/inside-the-beltway-the-news-media-longs-for-lysolg/
