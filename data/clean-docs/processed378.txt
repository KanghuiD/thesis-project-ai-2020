Opinion in EL PAÍS in English
 This browser is no longer supported. To visit EL PAÍS with the best experience, update it to the latest version or download one of the following supported browsers:
 El Roto cartoon, October 7, 2020
 El Roto cartoon, October 6, 2020
 El Roto cartoon, October 5, 2020
 Madrid's regional premier, Isabel Díaz Ayuso, is alone in her insistence on a harmful legal challenge with no scientific basis
 El Roto cartoon, October 2, 2020
 El Roto cartoon, October 1, 2020
 El Roto cartoon, September 30, 2020
 Spanish politics is as destructive as the coronavirus. There will one day be a vaccine against the latter, but there seems to be no remedy for the former. If we don't do something, these people will be the ruin of us all
 El Roto cartoon, September 29, 2020
 Given the worldwide scarcity of women as judicial authorities, the US Supreme Court Justice will be missed even more
 El Roto cartoon, September 28, 2020
 Despots are seeking a mythological enemy on which to blame the coronavirus. But if we have learned anything about the pandemic, it’s that collective action is the only thing that can bring about change
 El Roto cartoon, September 25, 2020
 El Roto cartoon, September 24, 2020
 El Roto cartoon, September 23, 2020
 El Roto cartoon, September 22, 2020
 El Roto cartoon, September 21, 2020
 El Roto cartoon, September 18, 2020
 El Roto cartoon, September 17, 2020
 El Roto cartoon, September 16, 2020
 El Roto cartoon, September 15, 2020
 El Roto cartoon, September 14, 2020
 Defining the crisis as an induced economic coma, and not a recession, has allowed for the adoption of a different and sounder strategy
 El Roto cartoon, September 11, 2020
 El Roto cartoon, September 10, 2020
 El Roto cartoon, September 9, 2020
 What unites us? Shared values, culture or history? Are we any more than the sum of our parts? Old ideals are under threat: we must once again decide who we are
 https://english.elpais.com/news/opinion/