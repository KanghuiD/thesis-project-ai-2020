Coronavirus: Countries rethink advice on masks, facial coverings - Business Insider
 Countries, states, and cities around the world are starting to rethink their guidance that had said wearing masks and facial coverings is not necessary for most people during the coronavirus for most people.
 Health authorities in Europe and the US, as well as the WHO, have said that wearing a mask is not necessary unless you already have symptoms, or work in a job that involves caring for the infected.
 The US Centers for Disease Control (CDC) states that "You do not need to wear a facemask unless you are caring for someone who is sick (and they are not able to wear a facemask)."
           N95 respiration masks at a laboratory of 3M, in Minnesota, US, on March 4, 2020.
         
             Reuters/Nicholas Pfosi
           
 In a tweet in late February, the US Surgeon General wrote: "STOP BUYING MASKS! They are NOT effective in preventing general public from catching #Coronavirus."
 Now the WHO and CDC are reassessing advice about masks and facial coverings, alongside officials in some US states and some European governments. The Surgeon General also said this week he is looking again at his position.
 A panel of advisors to the WHO is now assessing whether the virus can spread further than previously thought through the air. The chair of that panel told the BBC that the findings could change their advice.
 "The WHO is opening up its discussion again looking at the new evidence to see whether or not there should be a change in the way it's recommending masks should be used," Professor David Heymann said.
 The CDC too may start urging Americans to cover their faces in public, according to The Washington Post. Two federal officials told the Post that the CDC is debating a policy change, which would tell people to use clothing and other materials to keep proper masks free for medical staff.
 Hanno Kautz, a spokesman for Germany's health ministry, said the country also might consider requiring people to wear non-medical masks when going to supermarkets.
           The empty Brandenburg Gate is pictured during the coronavirus disease (COVID-19) outbreak in Berlin, Germany, March 25, 2020.
             Fabrizio Bensch/Reuters
 It would be following neighbouring Austria, which announced on Monday a new rule requiring people to wear masks when they go outside.
 Sebastian Kurz, the chancellor of Austria, noted that masks are "alien to our culture," but said the country needed to try and find new ways of fighting the virus.
 So far, advice in countries like Germany has been in line with that issued in some other European states. For example, Ireland's health service says that "using masks is unlikely to be of any benefit if you are not sick," and there is similar advice from UK health authorities.
 But three European countries — Slovakia, the Czech Republic, and Bosnia-Herzegovina — have already made the wearing of face masks in public a requirement, according to The Financial Times.
           Health care professionals test for COVID-19 at the ProHEALTH testing site in Jericho, New York on March 24, 2020.
             Steve Pfost/Newsday RM via Getty Images
 In the US, Los Angeles Mayor Eric Garcetti said he could not wait any longer for new advice from the CDC and told everyone to start wearing masks.
 He said people should stay home where possible, and use n0n-medical face coverings like clothing when going outside. California's governor has not made the same plea.
 And Israel's prime minister said on Wednesday that everyone should wear masks when they go outside, and should use a "a scarf or any other facial covering" if they cannot find one, Reuters reported.
 As Business Insider's Hilary Brueck reported, scientific studies into the effectiveness of face masks have had inconclusive results, with some finding that they can contribute the slowing a virus spread but others suggesting that they are not worth wearing.
 But they may help to stop the spread if everyone wears them, as it can stop even people who have no symptoms and don't know they're infected from spreading it to others.
 But countries around the world are already facing severe shortages of masks, leaving healthcare workers who are treating patients without. Some are wearing plastic sheeting as an alternative, or breaking health guidelines by reusing the same mask.
           A sign tells customers that all N95 protective masks are sold out at Marin Ace Hardware in San Rafael, California, on March 2, 2020.
             Justin Sullivan/Getty Images
 In the US, for example, the $7 billion federal stockpile of equipment, including masks, that had been maintained to deal with a public health emergency has almost run out of protective equipment.
 And the CDC was forced to update its guidance to say that healthcare workers could use a bandana or a scarf to cover their faces if they did not have a mask.
 In light of these issues, many countries had encouraged people not to buy masks as it would decrease the number available to medical staff and those working with infected people.
 The CDC currently says on its website that "Facemasks may be in short supply and they should be saved for caregivers." 
 Health authorities have also warned that masks and non-medical facial coverings could actually help to spread the virus if people don't wear them properly.
 Get the latest coronavirus business & economic impact analysis from Business Insider Intelligence on how COVID-19 is affecting industries.
 https://www.businessinsider.com/coronavirus-countries-rethink-advice-masks-facial-coverings-2020-4
