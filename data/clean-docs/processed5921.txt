Coronavirus in Colorado: Latest COVID-19 updates from March 31, 2020
 Would you like to receive local news notifications on your desktop?
 Menu
 On Monday, Colorado Gov. Jared Polis delivered some hopeful news: the spread of the virus appears to be approaching a doubling rate of every five days, compared to recent doubling rates of every two days. 
 He said this is likely because of the social distancing guidelines, bans of large groups and school closings. The impact of limiting groups to 10 people or less will be reflected in data in the coming days, but is not yet known. 
 As of Monday at 4 p.m., the Colorado Department of Public Health and Environment said more than 15,300 people have been tested across 47 counties. Of those, 414 people have been hospitalized and 51 people have died. 
 Below, we're updating this blog with the latest information regarding COVID-19 in Colorado.
 8:32 p.m. | CPW extends expiration date for state park passes, off-highway vehicle registration
 Colorado Parks and Wildlife (CPW) is extending the expiration date for the following products, effective immediately:
 -- Off-highway vehicle (OHV) registrations and permits scheduled to expire March 31, 2020 are now valid through April 30, 2020. -- State Park annual passes, annual family passes, Aspen Leaf annual passes and Columbine annual passes are valid for 30-days after the initial expiration date. For example, passes that expire in April 2020 are now valid through May 31, 2020. This 30-day extension is limited to passes with an original printed expiration date of March 2020 or April 2020. 
 These extended expiration dates are intended to mitigate the spread of COVID-19 by reducing the need for public travel and in-person interactions at CPW offices, officials say. 
 6:04 p.m. | Denver City Council passes $13.5 million in emergency funds
 Denver City Council has passed three bills to help residents get through the COVID-19 pandemic: 
 5:05 p.m. | Denver installs washing stations to prevent further spread of COVID-19
 The city of Denver has installed hand-washing stations as well as porta-potties throughout the city to make sure people have the opportunity to wash their hands and further prevent the spread of COVID-19 in the city. 
 5:04 p.m. | DIA to close north TSA checkpoint due to COVID-19 impact
 Denver International Airport will close its north TSA checkpoint "due to lower traffic volumes." The south checkpoint, located near the Westin hotel, will remain open 24 hours a day aand the A Bridge Checkpoint will operate as normal fro 4:30 a.m. to 6 p.m. each day, officials said. 
 A TSA screening officer who last worked at Denver International Airport on March 21 at the oversize baggage screening check point on Level 6 between 4 a.m. and 12:30 p.m. tested positive for COVID-19, TSA officials said Monday.
 5 p.m. | Larimer County deaths
 Larimer County Department of Health and Environment said five people have died of complications associated with COVID-19 in the county.  
 4:57 p.m. | Grand County prohibits short-term lodging due to COVID-19
 Grand County Public Health says it is prohibiting all short-term lodging operations and reservations until April 30, 2020 in order to mitigate the spread of COVID-19 in the county. Short-term lodging is defined as campgrounds, reserved camping sites, hotels, motels, bed and breakfast establishments, lodges, retreats and "short-term rentals" of 30 days or less. 
 4:55 p.m. | Governor will wait for more data to decide if stay-at-home order will be extended statewide
 While Governor Jared Polis said during a news conference Monday that "it is looking more and more like we will need an extension" of the state's stay-at-home order, Conor Cahill, a spokesperson for his office, said the governor wants to get a "few more days of data before we make that decision."
 The statement comes after Denver Mayor Michael Hancock said he will likely extend the city and county's stay-at-home order until April 30. 
 4:25 p.m. | Polis requests extension of work permits for DACA recipients
 Gov. Polis has urged the Acting Secretary of the Department of Homeland Security to automatically extend work authorizations for all Deferred Action for Childhood Arrivals (DACA) recipients whose grants expire in 2020.
 “Coloradans face unprecedented challenges to their economic and social lives, and this extension would provide some needed stability to our businesses and residents who benefit from opportunities that DACA provides,” the Governor wrote to Acting Secretary Wolf.
 The governor's office says Colorado is home for nearly 15,000 Dreamers. 
 4:22 p.m. | Gardner requests coronavirus testing for Colorado tribes
 Sen. Cory Gardner, R-CO, is requesting the Federal Emergency Management Agency (FEMA) provide additional COVID-19 testing kits to the Ute Mountain Ute Tribe and Southern Ute Indian Tribes in Colorado to ensure they have the testing capabilities necessary to resond to the COVID-19 pandemic. 
 “There have already been two confirmed positive cases on the Southern Ute Indian reservation, and the Indian Health Service expects an increase in positive cases amongst all tribes in the coming weeks. The federal government needs to fulfill its trust responsibility to tribal governments and ensure they have access to the resources they need,” wrote Senator Gardner. “It is crucial that FEMA provides a sufficient amount of testing kits both to the state of Colorado and to our tribal communities in order to ensure that they are able to respond to the grave threat that COVID-19 poses.”
 4:05 p.m. | Number of coronavirus cases in Colorado grows to nearly 3,000, CDPHE explains why it hasn't released number of recovered cases
 The number of positive cases of the novel coronavirus in Colorado has increased to 2,966, a jump of 339 from Monday's numbers, according to the latest data from the CDPHE.
 So far, 509 people have been hospitalized and 16,849 people have been tested for the new virus. The state has spread to 50 of Colorado's 64 counties and there have been 16 outbreaks at residential and non-hospital healthcare facilities. A total of 69 deaths have been reported by the state as of 4 p.m. Tuesday afternoon.
 State and health officials say there thousands of more COVID-19 cases that not been diagnosed.
 As of Tuesday afternoon, there was no data available from the state on the number of people who tested positive for the virus and recovered, but a COVID-19 incident manager with CDPHE confirmed they are "working with local public health agencies and partners in the health care environment to identify the best way to start identifying recoveries."
 4:02 p.m. | Denver has issued 5 citations since stay-at-home order went into effect
 The city of Denver has issued five citations to non-essential businesses that were still operating despite the city's stay-at-home order since the public health order went into effect on March 24. Data from the city shows the first citation was issued two days after the city went under a stay-at-home order. The highest number of citations issued by the city is two, which were issued on Sunday, March 29. 
 Officials have made contact with 3,052 businesses to inform them of the stay-at-home order and have issued 601 warnings since. 
 4:01 p.m. | Denver Sheriff's Department reports 2 positive cases of COVID-19 
 The Denver Sheriff Department says it currently has one deputy and one civilian security specialist who tested positive for COVID-19. On Monday, an inmate at the Denver Detention Center tested positive for COVID-19. 
 3:57 p.m. | Dental charity to donate pallets of PPE to Denver Health 
 The Colorado Mission of Mercy Dental Clinic (COMOM) will be unloading a semi-truck of PPE to donate to Denver Health and other hospitals in need of PPE supplies on Wednesday. The Colorado Dental Association estimates that they'll be donating 6-8 pallets of PPE.
 3:45 p.m. | Denver likely to extend stay-at-home order until April 30
 The city of Denver is considering extending its stay-at-home order, which currently is in place through April 11, until April 30, a city and county spokesperson said Tuesday afternoon. The spokesperson said the city was coordinating with the state to extend the order, but she said she could not put a timeline on when that change might happen.
 Read the full story here. 
 3:04 p.m. | Rep. Diana DeGette to host telephone town hall about COVID-19
 Rep. Diana DeGette, D-CO, will host a telephone town hall at 6:30 MT tonight to discuss what's being done to protect constituents from the novel coronavirus as well as answer any questions they may have. She'll also discuss the economic relief package that was signed into law Friday. 
 3 p.m. | "We will have no options": The coronavirus challenge facing rural Colorado
 More people were tested for the coronavirus in rural southwestern Colorado on Tuesday, but the overall lack of tests available has left officials unsure how much COVID-19 has spread in the community, where hospital capacity is limited to just dozens of beds.
 "We're fighting a global pandemic in the dark," said Liane Jollon, the executive director of San Juan Basin Public Health, which serves about 70,000 residents in Archuleta and La Plata counties, including Pagosa Springs and Durango.
 The fear from San Juan Basin officials centers on what might happen if the San Juan Basin sees a surge in patients in coming weeks. Jollon said it appears the San Juan Basin's expected peak for cases — the "curve" formed as cases increase — could happen after the peak of cases in more populous areas of the state and in other mountain towns.
 If that happened, healthcare resources across the state would already be tied up, leaving San Juan Basin providers to shoulder the burden on their own. Two hospitals serve the two counties: Mercy Regional Medical Center in Durango, with 82 licensed beds, and Pagosa Springs Medical Center, with 11 beds.
 12:30 p.m. | Walmart, Sam's Club to start testing employees for COVID-19
 In a joint letter, the CEO of Walmart and CEO of Sam's Club said they have decided that all associates will have their temperatures taken before reporting to work. The companies are currently sending infrared thermometers to all locations, which may take up to three weeks. 
 12:15 p.m. | Updates from CDPHE, Emergency Management briefing
 The bi-weekly briefing featuring Office of Emergency Management Director Mike Willis and CDPHE COVID-19 Incident Commander Scott Bookman contained a few new bits of information. Highlights below:
 --The CDPHE is still evaluating the efficacy of different types of testing for various phases of the state’s response. Blood testing, as has been done in San Miguel County, will have a role at some point, Bookman said.
 --The state is not releasing the names or locations of the four new residential, non-hospital health care facility outbreaks. But Willis said there are two in Larimer, two in Weld, two in El Paso, two in Denver, two in Arapahoe and one in Chafee, Routt and Jefferson counties.
 --Bookman said that the 414 hospitalizations was the total number confirmed since the outbreak started and does not include people who are no longer hospitalized.
 --The state does not currently have numbers of the number of people who have recovered from the virus but says it is working with county health departments to find a way to streamline receiving that data. Bookman said there were some complexities around tracking hospitalized vs. non-hospitalized recoveries. Since many people are currently not being tested, that would add to the complexity. Boulder County released the number of cases and recoveries Tuesday morning. 
 --Bookman said the state was continuing to work with health care providers to get daily reports on the number of ventilators, ICU beds and staff available. He said “most hospitals have been very willing” to provide that information and that the state was continuing to streamline those efforts. He said if providers don’t comply once that process is finalized, the state would have the ability to issue an executive order or public health order to receive said information.
 --Willis said that the forecast models the state is using for its COVID-19 response is coming from a team of professionals at the University of Colorado and the CDPHE. When asked if those models could be released publicly, he did not answer.
 --Bookman said the state continues to see challenges around specimen collection and testing and reiterate that tests were being prioritized for people who are hospitalized or who are health care workers or first responders because of a shortage of testing equipment and PPE.
 --Willis reiterated the state is not considering closing its borders. Gov. Polis said previously the state does not have the authority to do so because of federal rules involving interstate commerce.
 11:18 a.m. | No DPD officers test positive
 According to the Denver Police Department, no officers have tested positive to the coronavirus as of now. 
 10:05 a.m. | Boulder County releases COVID-19 recovery numbers
 Of the 100 people who have tested positive for the novel coronavirus in Boulder County, 41 have recovered, the county announced Tuesday morning. This is one of the few times a Colorado county has reported the number of people to have recovered from the virus. Read more here. 
 10:02 a.m. | Boulder County resident dies of complications from COVID-19 
 Boulder County Public Health says a Boulder County resident in their 70s who tested positive for COVID-19 has died. The person had been hospitalized for two weeks. The public health department said the virus was transmitted within the community. This is the second person to die of complications from COVID-19 in the county. 
 9:45 a.m. | Drive-through testing in Pagosa Springs
 A drive-through coronavirus testing site will be held at the Pagosa Springs Medical Center on Tuesday, from noon to 4 p.m. The testing will be for Archuleta County residents who meet certain criteria: Healthcare or long-term care workers, first responders, older adults and people with underlying health conditions. One person in Archuleta County has tested positive, though officials believe the number of cases is higher due to a shortage of testing, according to San Juan Basin Public Health.
 9:05 a.m. | 18th District Attorney's Office cancels jury calls 
 All jury calls through May 15 have been canceled, according to the 18th District Attorney's Office. Aside from public safety matters, all appearances and hearings set through 5 p.m. on April 17 are vacated and continued. Cases set between Monday, April 20 and Friday, May 1 that are not included in the public safety matters may be heard via telephone or video conferencing. Read more details here. 
 5:45 a.m. | Reminder regarding street sweeping
 Denver's street sweeping program begins Wednesday, but because so many people are inside under the stay-at-home order to help reduce the spread of COVID-19, the city will not enforce parking restrictions related to street sweeping for 30 days. Anybody who is able to move their cars is asked to do so. 
 5:30 a.m. | Send in your Census to help reduce COVID-19 spread
 Read the live blog from March 30 here. 
 https://www.thedenverchannel.com/news/coronavirus/coronavirus-in-colorado-latest-covid-19-updates-from-march-31-2020
