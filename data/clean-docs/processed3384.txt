| COVID-19 and the border
 NATIONAL CORONAVIRUS HOTLINE
 1800 020 080
 Updates to Australia's immigration and border arrangements during the COVID-19 (Coronavirus) outbreak.
 Information in your language
 My visa is
 Latest information for all visas
 06 April 2020
 The Government will hold online citizenship ceremonies via secure video link, to enable people to continue to become Australian citizens during the coronavirus crisis.
 Frequently asked questions for temporary visa holders and temporary visa measures supporting the agriculture sector.
 04 April 2020
 The Federal Liberal and Nationals Government is making temporary changes to visa arrangements to help farmers access the workforce they need to secure Australia’s food and produce supply during COVID-19.
 The Government is making a number of changes to temporary visa holder arrangements during the coronavirus crisis in order to protect the health and livelihoods of Australians, support critical industries, and assist with the rapid recovery post the virus.
 27 March 2020
 To reduce community transmission of COVID-19 from Australians returning from overseas, new quarantine measures will be coming into effect.
 18 March 2020
 Aged care providers will temporarily be able to offer more hours to international students to ensure the care of senior and vulnerable Australians, as part of the campaign to combat the impact of coronavirus.
 https://www.homeaffairs.gov.au/news-media/current-alerts/novel-coronavirus?fbclid=IwAR0O9buBJ3aVl03OW-040VXHMmQZm3q-NDYJ71pt1gBDoLsLwfVmFjN9Cls
