Coronavirus: Online tool to target fake testing kits - BBC News
 An online tool has been updated to pick up fake testing kits, illicit homemade sanitiser and "miracle" coronavirus cures on sale on eBay.
 The free checker is designed to warn online shoppers of potential high-risk purchases before they buy.
 It has already been embedded on the websites of Trading Standards Scotland and national consumer advice service consumeradvice.scot. 
 It aims to stop illicit sellers from "taking advantage" of consumers.
 The checker, created by Edinburgh-based Vistalworks, allows users to paste in an eBay listing URL in order to get an indication of whether a product is legitimate or not.
 Vistalworks said its consumer protection software was developed in conjunction with Police Scotland, HMRC and Trading Standards Scotland.
 It has also developed a Chrome browser plugin which allows online shoppers to hover over any eBay UK listing to see if it is a potential high-risk purchase.
 Vistalworks said information from its own analysts and Trading Standards Scotland was being used to create "coronavirus-specific" updates to its software.
 Vistalworks chief executive Vicky Brock said: "With consumers' anxiety about the seriousness of coronavirus growing, and mainstream retailers' supply chains taking unprecedented stress, we know there will be a significant rise in the number of illicit sellers trying to take advantage of worried consumers. 
 "We've already seen this in the online marketplace listings we monitor.
 "To tackle these emerging threats, we've updated our eBay checker to pick up fake testing kits, illicit homemade sanitiser, and a whole array of 'miracle' cures, including specific items which our analysts and Trading Standards Scotland have become aware of in the last weeks."
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/uk-scotland-scotland-business-51959570
