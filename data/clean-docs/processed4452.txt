Coronavirus Deals Another Blow to an Industry Already Reeling
 World
 An Olive Oil Times survey reveals growing despair and uncertainty for an industry that seemed under attack even before the pandemic arrived.
 As the death toll from the novel coro­n­avirus mounts around the world, stark mea­sures nec­es­sary to quell the dis­ease have turned a health and human­i­tar­ian cri­sis into an eco­nomic dis­as­ter.
 Gov­ern­ments are shut­ting down non-essen­tial work­places, schools, restau­rants and events — any­where peo­ple gather. While such restric­tions have helped slow the spread of the dis­ease, they put a hard stop to the liveli­hood and income of broad swaths of work­ers.
 Olive farm­ers and olive oil pro­duc­ers have been hit hard in nearly every region. In Spain, Italy, Greece, Por­tu­gal and Tunisia, France, Turkey, Croa­tia, Lebanon, Syria — places where olive oil pro­duc­tion is a way of life — there is grow­ing despair and uncer­tainty for an indus­try that seemed under attack even before the pan­demic arrived.
 Olive oil pro­duc­ers are peo­ple hard­ened by droughts, freezes, fires, rav­ages and a job that never really seems free of calami­ties. Over­com­ing chal­lenges are rou­tine, and every year brings a new set of con­di­tions to con­front. If they man­age every­thing just right and pro­duce a per­fect prod­uct, it will fetch a price far lower than its true value and almost always less than pro­duc­ers need.
 The coro­n­avirus has even these fight­ers rat­tled.
 Respon­dents to an Olive Oil Times sur­vey over the week­end reflected the dev­as­tat­ing impact the cri­sis is hav­ing on all aspects of life on the farm and through­out the sup­ply chains on which they depend.
 “We don’t know what will hap­pen after the cri­sis,” said a small Greek pro­ducer who exports mainly to Ger­many. ​“For now we are wait­ing to see how far our last money lasts.”
 Farms at a stand­still
 Many grow­ers say they can’t get help tend­ing to the urgent needs of the farm.
 “We had trou­ble gath­er­ing work­ers for olive farm­ing, even for out­door jobs. Peo­ple stay at home despite hav­ing the need for money,” a pro­ducer in Turkey, Mustafa Safa Soy­dan, said.
 “I am prun­ing my olive trees alone in this period because my helpers can­not reach me,” said Debra Carol Kyllingstad Had­dock, the owner of Casale Prato delle Coc­cinelle Olive Estate in Umbria, Italy.
 “Like oth­ers in North­ern Cal­i­for­nia, we are on manda­tory shel­ter in place,” said Geoff Peters at Showa Farm. ​“This has caused us to get to many deferred tasks and to make prepa­ra­tions for spring such as inspect­ing drip irri­ga­tion and order­ing mate­ri­als for McPhail traps.”
 Some farms, how­ever, are so far man­ag­ing to con­tinue with oper­a­tions, reflect­ing the patch­work nature of the global response mea­sures in effect. ​“Restric­tions are not apply­ing to olive oil pro­duc­ers or farm­ers in Spain,” reported the pro­pri­etor of Casa del Aguila in Jaén. ​“In our estates, we are work­ing nor­mally,”
 Shut­tered points of sale
 While dozens of respon­dents described the shut­down of their fields, mills and pack­ing processes, there were also the effects of manda­tory restric­tions fur­ther down the chain.
 “All the mar­kets and events where I sell are closed, impact­ing 90 per­cent of my income,” said a U.S. dis­trib­u­tor. ​“We have a ship­ment on a boat from Italy. It is sup­posed to clear eas­ily but now it is in limbo slow­ing my sup­ply chain,” said another.
 “Our busi­ness is shut down and all employ­ees laid off. It’s hor­ri­ble,” a third Amer­i­can dis­trib­u­tor told us.
 “I do fairs and mar­kets. All of these have been shut down so now I am only online. I’ve offered free deliv­ery to those in my area, but it isn’t enough to sus­tain me — rent, mort­gage, sup­plies — it’s tough,” said a Cal­i­for­nia retailer.
 “We have 70 per­cent lower sales of our prod­ucts. We can­celed all fairs, tast­ings, and edu­ca­tions,” Mar­i­jan Mar­janovic, the CEO for Stan­cija St. Anto­nio in Croa­tia, said.
 In Canada, Myrna Bur­lock, who oper­ates Liq­uid Gold Olive Oils and Vine­gars, said, ​“All four of our tast­ing bars, mean­ing our liveli­hood, have been forced to close. At ages 63 and 68, where does one go from here?”
 Another Cana­dian com­pany, Sarafino, has been allowed to stay open, ​“as we are part of the gro­cery sup­ply chain,” they said.
 “In Turkey, the tourism sea­son was about to start with hotels mak­ing their deals for olive oil buy­ing,” a farmer in Turkey told us. ​“Now every­thing stopped.”
 Some see a call to action, oth­ers hun­ker down
 Just as the cri­sis has had vary­ing impacts on the busi­nesses of grow­ers and pro­duc­ers — gen­er­ally rang­ing from severe to out­right par­a­lyz­ing — so are the ways they plan to respond to the new real­ity.
 Many of those we polled said they noticed an increase in online sales — whether seen on their own web­sites or in news reports — and there was broad deter­mi­na­tion to shore up dig­i­tal pres­ences, home deliv­ery pro­grams and cus­tomer out­reach ini­tia­tives.
 “We have to cre­ate urgently an online store and we have to use this time to do a lot of com­mer­cial fol­low-ups,” said a pro­ducer in Por­tu­gal, Pilar Abreu e Lima, ​“and, at the same time, do all the farm­ing work by our­selves.”
 In Greece, Maria Anag­nos­topou­los, said she plans to ​“trans­fer my sales dig­i­tally, both bulk and pack­aged on my own chan­nels and reach out to other e‑commerce part­ners.”
 There are lay­offs, missed rent pay­ments and dwin­dling sav­ings. Stocks sit idle in ware­houses and shops, machines pow­ered down in closed mills.
 Signs of sol­i­dar­ity, and some res­ig­na­tion
 Farm­ers in Spain fired up their trac­tors last week to help dis­in­fect city streets.
 Kathryn Keeler, at Ran­cho Azul y Oro Olive Farm in Cal­i­for­nia, said she saw a need to help peo­ple sud­denly con­fined to their homes cook healthy meals by post­ing more recipes for their online com­mu­nity.
 “Just look at Insta­gram,” said a Cal­i­for­nia dis­trib­u­tor of Por­tuguese oils, Bare Foods. ​“Every­one is cook­ing!”
 There were calls for gov­ern­ment action now and once the health cri­sis sub­sides and the eco­nomic hard­ships come into sharper focus. And among the responses to our sur­vey, there were expres­sions of hope­less­ness in the face of what might some see as the last straw.
 “I am depressed,” a farmer and pro­ducer in Turkey said sim­ply when asked how he was doing.
 And there are deep con­cerns for the long-term effects the coro­n­avirus cri­sis will have across the sec­tor. More than one-third of the respon­dents said they were ​“extremely con­cerned” about the last­ing dam­age to their busi­nesses.
 Nowhere so far has the sit­u­a­tion been as dire than in Italy, where warn­ings ring out to the rest of the world.
 “I see politi­cians in Eng­land, France, Ger­many, USA and Brazil down­play­ing the seri­ous­ness of COVID-19 while we’re try­ing to bury our 5,000 dead,” Debra Kyllingstad Had­dock wrote from Umbria. “#stay­ath­ome!”
 More articles on:  COVID-19, natural disasters
 Jan. 27, 2020
 Australian Producers Making Do Despite Bush Fires, Record Drought 
 In spite of a record drought and devastating bush fires, some large Australian producers are expecting close to an average production and high-quality oils in 2020.
 Apr. 6, 2020
 Argentina Olive Harvest Begins Amid COVID-19 Lockdown 
 As the coronavirus spreads through Argentina, olive growers and oil producers are facing complications both during and after the harvest. Small producers are likely to be impacted the most.
 Feb. 27, 2020
 Coronavirus Epidemic Hits Italy's Agriculture Sector 
 The rising number of coronavirus infections across northern Italy has impacted every corner of the country's agriculture sector, from harvesting and production to tourism and exports.
 Apr. 2, 2020
 Transporters Warn of Effects of COVID-19 Measures on Food Supply 
 European logistics firms fear the quarantine measures and bureaucratic hurdles, while consumer demand keeps growing.
 Olive Oil World
 Health
 Business
 Production
 https://www.oliveoiltimes.com/world/coronavirus-deals-another-blow-to-an-industry-already-reeling/80727
