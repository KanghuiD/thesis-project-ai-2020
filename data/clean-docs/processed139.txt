Coronavirus in Georgia: Deaths from COVID-19 continue to rise
 The Atlanta Journal-Constitution is committed to providing our readers with the most comprehensive coverage of the deadly coronavirus. 
 This blog will be updated throughout Thursday, April 2, with news and details of COVID-19 in Georgia.
 » Complete coverage: Coronavirus
 8:30 p.m.: An 11-year-old DeKalb County boy died Thursday from the coronavirus, according to the Georgia Department of Public Health. Alexis Stevens reports he is the youngest from Georgia to die from the virus.
 8 p.m.: Georgia’s 10.6 million residents will soon be under a ntatewide shelter-in-place order to try to slow the spread of the coronavirus. Greg Bluestein tells you what that means.
 7:30 p.m.: 
 Georgia recorded 13 more coronavirus deaths since noon, bringing the state’s death toll to 176, Chelsea Prince and Zachary Hansen report.
 The DPH also confirmed 96 more cases of COVID-19, which brings the state’s total to 5,444. Of those, 1,129 patients are hospitalized, which is about 20.7% of all cases.
 » Coronavirus in Georgia: stats in real time
 4:45 p.m.: In the fight against COVID-19, some experts are hoping the heat and humidity that draw complaints from locals may prove beneficial. Nedra Rhone talked to experts about whether the rise in temperature will help fight the virus.
 Noon: 
 Georgia’s coronavirus cases surpassed 5,000, Chelsea Prince reports. 
 The latest data from the Georgia Department of Public Health shows 5,348 confirmed cases, up about 13% from 4,748 cases Wednesday night. Nine more Georgians have died as a result of COVID-19, the disease caused by the novel coronavirus, bringing the state’s total to 163.
 » Your money: Resources during the coronavirus pandemic
 9 a.m.: ﻿Local doctors, paint stores, ham provisioners, optometrists, food deliverers, gas stations and others are scrambling for increasingly innovative ways to make their interactions with the public as touchless and distant as possible to avoid spreading the new coronavirus. Matt Kempner tells you what they’re doing.
 6 a.m.: Georgia Power’s parent company is warning investors that the coronavirus pandemic could disrupt its nuclear power expansion at Plant Vogtle, a project that already is far behind schedule. Matt Kempner explains what that means.
 OVERNIGHT
 New Birth Missionary Baptist Church and Texas-based RoweDocs plans to provide drive-through coronavirus testing for 1,000 people this weekend on the church’s Stonecrest campus.
 ﻿» Why you shouldn’t worry that a cat tested positive for COVID-19
 WEDNESDAY’S TOP STORIES
 » Where to find free internet in Georgia during the coronavirus
 ﻿» Photos: Metro Atlanta adjusts to coronavirus shifts in daily life
 » ﻿Studies show this is the best time to shop for groceries amid social distancing
 DAILY CORONAVIRUS UPDATES
 ﻿Wednesday, April 1
 ﻿Tuesday, March 31
 Monday, March 30
 Friday, March 27
 Thursday, March 26
 Wednesday, March 25
 Tuesday, March 24
 Monday, March 23
 Friday, March 20
 Thursday, March 19
 Wednesday, March 18
 Tuesday, March 17
 Monday, March 16
 Sunday, March 15
 Saturday, March 14
 Friday, March 13
 Thursday, March 12
 		        See offers.
 Your subscription to the Atlanta Journal-Constitution funds in-depth reporting and investigations that keep you informed. Thank you for supporting real journalism.
 The Atlanta Journal-Constitution.
 			Visitor Agreement and
 			Ad Choices. Learn about
 			careers at Cox Enterprises.
 		
 https://www.ajc.com/news/latest-atlanta-coronavirus-news-georgia-cases-near-deaths-154/DHY0wt76GpuQh110eDa8IP/
