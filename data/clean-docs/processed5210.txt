Can your Garmin detect coronavirus? - Garmin Blog
 Unlock exciting app adventures, and follow your favorite characters with these fitness trackers for kids
 Stylish hybrid smartwatch with a discreet touchscreen display
 Save on products that help you stay active
 Premium multisport GPS watches in 3 sizes offer heart rate, Pulse Ox, routable maps, pace guidance, music and more
 All-terrain motorcycle navigator with ultrabright 5.5” display
 The ultimate outdoor watches with specialized marine features, including comprehensive boat connectivity
 Protect your most precious cargo
 Find sales, promotions, discounts and rebates on your favorite Garmin products
 Can your
 Garmin detect coronavirus?
 Garmin
 smartwatches were not designed or intended to monitor or diagnose diseases, but
 scientists from top universities and institutes around the world are actively
 researching whether wearable devices like ours can help identify early
 indications of coronavirus.  
 One premise
 is actually quite simple. (1) An elevated heart rate is an early sign of a
 common cold, the flu or coronavirus. (2) Garmin smartwatches have built-in
 heart rate monitoring at the wrist. Put the two together and society could have
 an extremely helpful tool for early indications of the virus on a mass scale.
 To be sure,
 the ways in which wearable technology can help solve the current and future crises
 extend far beyond heart-rate monitoring. That’s why Garmin has teamed up with
 scientists around the world to seek the answers about the potential for Garmin
 smartwatches to be a useful tool in society’s need to better identify, track
 and monitor coronavirus.  
 In this
 article, we’ll cover a few of the major studies in the U.S. in which Garmin is participating
 and also provide details on how Garmin smartwatch users can join the cause and
 be a part of the solution. Here we go:
 Can
 wearable data predict illness?
 Duke University recently
 launched a study called Covidentify, aimed at slowing the spread of COVID-19.
 Using data obtained from mobile and wearable devices like Garmin smartwatches,
 Duke hopes to learn how to track the spread of COVID-19, understand when someone
 may be susceptible to contracting it, and who might be at higher risk of worse
 outcomes if they get infected. Garmin smartwatch users are encouraged to link
 their watches to the study to help researchers learn how their heart rate and
 movement are affected by COVID-19. People healthy and staying at home are also
 encouraged to join. For more information, visit www.covidentify.org. 
 Is your
 smartwatch already a detection device?
 When your
 heart beats faster than usual, it can mean that you’re coming down with a cold,
 flu, coronavirus, or other viral infection. The Scripps Research DETECT study
 seeks to determine if tracking of changes in heart rate, activity and sleep, at
 the individual level, can provide an early indication of a viral illness like
 COVID-19. Garmin users can enroll in the study via the MyDataHelps app which
 allows them to provide informed consent, sync their device, and enter
 self-reported data. By collecting data from thousands of individuals,
 scientists hope to identify possible influenza-like illness in those
 individuals and complement traditional outbreak response measures. For more
 information, visit www.detectstudy.org. 
 aggregated wearable data help families and doctors? 
 PhysioQ has
 just opened up the waitlist for NEO, a free, at-home COVID-19 monitoring
 platform built for families to keep track of their loved ones. Using top
 wearables trusted by researchers, including Garmin smartwatches and activity
 trackers, families can gain peace of mind by monitoring from oxygen saturation
 levels, beat-to-beat heart rate, and more from afar.
 In an effort
 to accelerate COVID-19 research, the anonymized data donated by families will
 be aggregated to create one of the world’s largest open COVID-19 databases.
 Many medical researchers and scientists have already signed the pledge to work
 on this initiative, including Dr. Andrew Ahn, an internist and researcher treating
 patients on the front lines, and Dr. Chung-Kang Peng, Director of the Center
 for Dynamical Biomarkers and
 Associate Professor of Medicine at Harvard Medical School.
 “Being able
 to monitor SpO2 and beat-to-beat intervals as well as activity and sleep
 through Garmin’s consumer-friendly devices is a real gamechanger,” said Dr.
 Chung-Kang Peng. “We believe the insights gained from this data can change our clinical approach
 and significantly expedite our disease discovery process,” added Dr.
 Ahn. For more information on the PhysioQ NEO project, visit www.physioq.org. 
 We’re in
 this together.  
 In these
 ever-changing times, one thing’s for certain: our society won’t stop seeking
 answers about this health crisis and what can be done to avoid another one in
 the future. Garmin is proud to work alongside these entities and more around
 the world in their quest for answers that will get scientists one step closer
 to early detection and hopefully one day, a cure. 
 https://www.garmin.com/en-US/blog/general/can-your-garmin-detect-coronavirus/
