Opinion | Coronavirus Advice: Wear a Mask. Here’s How. - The New York Times
 letters
 Two psychiatrists urge the public to overcome any hesitation, and a surgeon gives advice on how to wear a mask. Other readers offer a D.I.Y. tip and a historical note.
 To the Editor:
 Prospects for beating this pandemic were boosted Thursday when our leaders finally embraced face masks for the public.
 Wearing a face mask in public can be a jarring experience. “Am I overreacting?” and “Am I needlessly terrifying others?” are thoughts inevitably made worse by negative reactions from our neighbors.
 As psychiatrists, we commonly see how psychological factors and stigma can prevent us from making healthy, even lifesaving decisions. Nowhere does this seem more evident than in our current reluctance to embrace universal mask wearing.
 The early recommendations not to wear masks were rooted in the scarcity of medical masks around the world. But do-it-yourself masks, especially washable ones, will have no impact on medical supplies and may slow the spread of this virus, which helps us all.
 In “It’s Time to Make Your Own Face Mask” (column, nytimes.com, March 31), Farhad Manjoo clearly shows that face masks function not only as personal protective equipment but also as what we might call “others’ protective equipment” by reducing airborne viral spread from sneezes and coughs. One of the most self-protective and altruistic things we can do now is to wear a face mask while in public because asymptomatic people with Covid-19 can still transmit the illness.
 Eileen KavanaghDirk WinterNew YorkThe writers are psychiatrists at Columbia University Medical Center.
 You may have heard that surgical masks don’t work for preventing viral infections and may increase the risk of infection. Why is this? It is because people not accustomed to wearing masks can’t keep their hands off them. They constantly fiddle with them, adjust them, pull them down to talk and so on. Every time they do this, they are contaminating their mask with their hands, and if their hands have virus or bacteria on them, then they are risking infection.
 As a surgeon, I am used to standing around for hours without touching my mask or my face, but it is very difficult for someone who is not used to this. So, if you decide to wear a mask for protection against viral or bacterial infection, here is what you need to do:
 (1) Start with a clean mask. If you are reusing a mask, make sure that you keep it clean when you are not wearing it.
 (2) Wash your hands well with soap and water, for a minimum of 20 seconds but preferably more.
 (3) Place your mask and get it adjusted perfectly. It may help to do this in front of a mirror.
 (4) Now wash your hands again. You are doing this to protect everyone else since you have contaminated your hands by touching your face.
 (5) Do not ever touch the mask or any part of your face again unless you repeat steps 2 to 4.
 That’s it. It may be hard to get in the habit, but if surgeons can do it, you can, too!
 Mitchell A. FremlingWestminster, Colo.
 Nearly every woman in America has in her wardrobe the materials for manufacturing homemade, durable face masks. Take a spare bra, cut it vertically down the center, and one has in one’s hands the essential materials for creating two working face masks that can serve general purposes for you and your family.
 Polly LongsworthAmherst, Mass.
 I trust that historians will note that there was a day (and not as an April Fool’s joke) when the “newspaper of record” in the richest country in the history of the world devoted a full page to “How to Sew a Face Mask” — while the president patted himself on the back about what a great job he’s done.
 What might those historians think?
 Larry DuBoisSalt Lake City
 https://www.nytimes.com/2020/04/02/opinion/letters/coronavirus-masks.html
