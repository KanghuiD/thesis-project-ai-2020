 India coronavirus summary: cases, deaths, news - 21 April - AS.com
 Other Sports
 AS English
 According to the latest figures published by Johns Hopkins University, 2,531,804 cases have been detected worldwide, with 174,336 deaths and 665,458 people now recovered.
 In India there have been 18,985 cases with 603 deaths. 3,273 people have recovered from the virus.
 Some unexpected technical issues prevented us from adding to our feed in the latter stages of the day. Very soon we will be back up and running bringing you all the latest news.
 Thank you for you patience...
 The Indian Council of Medical Research has advised all states to stop using the rapid tests as they are giving back incorrect results.
 Oil’s epic price collapse is giving Indian bonds the upper hand over Indonesian debt in a market where dollars are in short supply and developing nations compete for foreign money, according to JPMorgan Private Bank.
 Since India was placed under lockdown on 24 March, movement throughout the country has been restricted to essential journeys only but some state governments have started issuing Emergency curfew e-passes which are now available online. All of those who are involved in the movement of essential goods and supplies across any of the country’s 28 states or union territories will need to apply for a an e-pass or movement pass. Here is all you need to need about who is eligible, where and how to apply for an e-Pass.
 India continues to count new cases with 6 reserve police officers testing positive today.
 The central government will be conducting a telephonic survey on the coronavirus pandemic calling up citizens on their mobile phones across the country. This will be carried out by NIC and calls will be made from the number 1921, according to the Indian Express.
 The Delhi-Noida border has been closed due to the spread of coronavirus. Only those who have government passes and workers are allowed to travel between the two cities in India. Heavy vehicles engaged in the transportation of goods will also enjoy the exemption.
 The number of confirmed cases of coronavirus worldwide has surpassed 2.5 million. The grim landmark was reached following warnings from the WHO president, Dr Tedros Adhanom Ghebreyesus, that "the worst is yet ahead of us".
 "This virus is dangerous. It exploits cracks between us when we have differences," he said.  "Trust us. The worst is yet ahead of us," 
 “Let’s prevent this tragedy. It’s a virus that many people still don’t understand. Easing restrictions is not the end of the epidemic in any country".
 Jamlo Madkam worked in chili fields in Perur village, in the southern state of Telangana...
 A local farmer named as Rambhavan Shukla is reported to have taken his own life on the outskirts of Jaari village in desperation after spending several days trying to find labourers to help harvest his wheat crop, News 18  report. The incident is being investigated by the police.
 Under lockdown restrictions, cars may only be used for journeys made in a medical emergency or to buy essential commodities with a maximum of one passenger in the back seat of the car.
 About 500 people entered self-isolation at India's presidential palace in New Delhi on Tuesday after a worker's relative tested positive for the coronavirus in the latest alarm close to public figures as the pandemic spreads through densely populated South Asia.
 The alarm was raised at President Ram Nath Kovind's Rashtrapati Bhavan residence in the centre of Delhi after the daughter-in-law of a sanitation worker living in employee quarters there tested positive for Covid-19, officials said. Neither Kovind, 74, nor his aides were required to self-isolate as they would not have come into contact with lower-level workers on the premises, officials said. But the families of palace workers living in 114 apartments on the grounds were ordered to stay inside, while the seven members of the sanitation worker's family were moved to a quarantine facility. No one on the premises is known to be infected so far.
 '"It may be clarified that to date no employee of the President's Secretariat has tested positive for Covid-19 and the Secretariat along with the Local Administration are taking all the preventive measures required under the government guidelines," Kovind's office said in a statement.
 An employee of India's parliament has also tested positive, but did not come to work, an official said, as the administrative wing of the legislature reopened on Monday as part of a staggered exit from the lockdown.
 While the total number of confirmed infections is still increasing, Indian health officials said the speed of transmission was slowing thanks to the lockdown, which at the moment is due to be lifted on 3 May. India's 1.3 billion people have been ordered to remain indoors for 40 days under a nationwide lockdown to slow the spread of the virus so that the country's hospitals are not overwhelmed by a flood of infections.
 Striking images taken five months apart comparing pollution levels in New Delhi. The top image was taken on 8 November 2019 and the image below, on 8 April 2020. According to data from the Delhi Pollution Control Committee, levels of nitrogen dioxide, sulphur dioxide, ozone and Pm10 particles have all seen significant drops during April while India is under a 21-day lockdown because of the Coronavirus epidemic.
 Officials confirmed that a woman died at 10:45 hours local time this morning at the 1500-bed Rajendra Institute of Medical Science (RIMS). The victim, who is reported to have been a resident of the Hindpiri district, had tested positive for coronavirus and was being treated at RIMS - as was her husband, a 60-year-old, who died just over a week ago.
 The United Nations issued a statement this morning to express their sadness at the news of Mr Pyae Sone Win Maung, a personnel driver of the World Health Organization, has died after being wounded in a security incident in Minbya Township in Rakhine State on Monday evening, 20 April.
 The WHO worker was driving a marked UN vehicle from Sittwe to Yangon transporting Covid-19 surveillance samples in support of the Ministry of Health and Sports. The United Nations is seeking further information on the circumstances of the incident.
 Spain's Ministry of Health confirmed this morning that 430 people have died during the past 24 hours due to Coronavirus-related issues. The number is slightly up on Monday's count of 399, which was expected but continues in the general downward trend. In total, Spain have registered 204,178 positive cases of Covid-19 infection with 21,282 fatalities. Madrid has recorded the most deaths with 7,460 followed by Cataluña with 4,152.
 Rather than it being a new policy, the transport and environment minister Elisabeth Borne was explaining the situation that already exists.
 British monarch Queen Elizabeth II turned 94 years old today but this year, her birthday will be a subdued affair. The Queen usually spends her actual birthday privately, but the occasion is marked publicly by gun salutes in central London at midday: a 41 gun salute in Hyde Park, a 21 gun salute in Windsor Great Park and a 62 gun salute at the Tower of London - but on this occasion, that will not happen today.
 France has stopped all incoming flights from outside the Schengen Area, French Transport and Environment Minister Elisabeth Borne said on Tuesday. "There are no more international flights outside the Schengen zone", Borne told French RTL Radio.
 The Schengen Area is a visa-free zone comprising 26 European states who agreed to abolish internal borders, for the free and unrestricted movement of people under the Schengen Agreement in 1985. 
 There are reports that three members of the police in Delhi's Nabi Karim - one of the capital's 84 containment zones - have tested positive for Covid-19.
 According to the latest figures published by Johns Hopkins University, 2,478,153 cases have been detected worldwide, with 170,368 deaths and 651,503 people now recovered.
 In India there have been 18,601 cases with 592 deaths. 3,273 people have recovered from the virus.
 All the information you need to understand the coronavirus and ways to stay safe during the Covid-19 pandemic:
 Covid-19
 Coronavirus: the complete guide to the Covid-19 pandemic
 80% of coronavirus sufferers have shown mild symptoms or none at all, India’s health ministry said on Monday - a statistic that emphasises “the need to follow lockdown rules and maintain social distancing”, notes the Times of India.
 The anti-malarial drug hydroxychloroquine is to be tested on residents in Mumbai - one of the areas of India worst hit by the coronavirus - NDTV has reported.
 Hydroxychloroquine has been touted by US president Donald Trump as a possible “game changer” in the battle against Covid-19.
 You'll find more on hydroxychloroquine, and its potential to combat the coronavirus, here.
 Hello and welcome to our live blog on the coronavirus pandemic, with a special focus on the latest developments in India. We'll do our very best to bring you the major news and numbers as they emerge throughout the day.
 0 
 Comentarios        
 Para poder comentar debes estar 
    registrado                
 y haber iniciado sesión. 
 ¿Olvidaste la contraseña?
 Ediciones internacionales
 Apps
 Síguenos
 https://en.as.com/en/2020/04/21/other_sports/1587427427_979169.html
