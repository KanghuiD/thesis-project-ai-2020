Coronavirus deaths in US rise to six: Live updates |  News | Al Jazeera
 Virus continues to spread around world with Saudi Arabia, Jordan, and Indonesia among countries reporting first cases.
 Six people have died in the United States from the coronavirus with researchers warning the virus could have been circulating in the community for weeks, as more countries around the world reported their first cases of the disease on Monday.
 Saudi Arabia, Jordan, and Tunisia reported their first confirmed coronavirus cases, while the death toll in Iran rose to 66 - the highest outside China.
 The Czech Republic, Scotland, and the Dominican Republic also confirmed their first cases. Indonesia, the world's largest Muslim majority nation, also announced its first patients.
 More than 89,000 people across 58 countries have now been diagnosed with the infection, while the death toll globally has exceeded 3,000.
 This is Hamza Mohamed taking over from Virginia Pietromarchi. Here are the latest updates: 
 Visitors from countries where coronavirus has spread are banned from entering Oman, the country's foreign ministry said, without specifying countries.
 This is a precautionary procedure and will be applied on all ports including land, sea and air, the ministry added.
 Six people have died from coronavirus in the US state of Washington, according to health officials, in an outbreak centred around a nursing home in Seattle.
 The total number of infections in the state was now 18, according to officials.
 The virus may have been circulating for weeks undetected in the state, researchers say.
 More on the story here.
 Awkward moment in Germany as interior minister rebuffed Chancellor Angela Merkel's attempt to shake hands with him on Monday as the number of coronavirus cases in the country rose to 150. When Merkel reached out to greet Horst Seehofer at a meeting on migration in Berlin, he smiled and kept both his hands to himself.
 They both laughed and Merkel then threw her hand up in the air before taking a seat.
 Senegal became the second country in sub-Saharan Africa to confirm a coronavirus case.
 Health Minister Diouf Sarr said a French man who lives in Senegal visited France in mid-February, contracting the virus before returning to the West African country.
 The patient, according to the minister, is now in quarantine in the capital, Dakar.
 The death toll from an outbreak of coronavirus in Italy jumped by 18 over the past 24 hours to 52, the Civil Protection Agency said.
 The accumulative number of cases in the country, which has been hardest hit by the virus in Europe, totalled 2036, up from 1,694 on Sunday.
 The head of the agency said of those originally infected, 149 people had recovered.
 The contagion came to light 10 days ago and is focused mainly on a handful of hotspots in the north of Italy, with isolated cases reported in many other regions.
 Saudi Arabia's health ministry announced its first coronavirus case.
 The victim travelled from Iran to the Gulf kingdom through Bahrain, the state-run Saudi Press Agency reported.
 A test confirmed the man was infected with the virus.
 Samples were taken from those in contact with the person, and results were expected soon, the health ministry said.
 Four new cases of coronavirus have been confirmed in Qatar, state news agency (QNA) reported on Monday, taking the total to seven.
 Two Qatari citizens and two domestic workers who accompanied them to Iran were diagnosed with the virus, QNA quoted the health ministry as saying.
 They were among a group of citizens who were evacuated by the government on a private plane from Iran on February 27, the ministry added.
 The victims did not have any contact with the community since their arrival. The three earlier coronavirus patients were in stable condition and recovering, it said.
 In North Africa, Tunisia has confirmed its first case of coronavirus.
 Abdelatif el-Maki, the health minister, said the patient is a Tunisian man, 40, who returned from Italy last week. 
 Tunisia is the second country in North Africa to announce a confirmed case after Algeria.
 Jordan announced its first case of coronavirus, the latest country in the Middle East to be hit by the outbreak.
 The kingdom's health minister said the infected person arrived back in Jordan from Italy two weeks ago.
 "A medical specialised team is checking now the family of the infected person, and sterilising their house, and also checking whether they are infected or not, and putting those not infected in house quarantine for at least 14 days, and transferring the infected cases to the hospital," Saad Jaber said.
 In Africa's most populous country, Nigeria, 58 people who came into contact with an Italian infected with the virus have been quarantined, health officials said.
 On Friday, Abuja confirmed the first case of the virus in sub-Saharan Africa after the patient was diagnosed in the economic hub, Lagos - a city of 20 million people.   The Italian had arrived from Milan on a business trip a week ago and travelled to a company in neighbouring Ogun state before being diagnosed.
 Nigeria is viewed by many as highly vulnerable to viral spread given its weak health system and high population density.
 There are four more confirmed cases of coronavirus in the United Kingdom, health officials say, taking the total to 40.
 The newly infected people had recently travelled to Italy, according to Chris Whitty, the chief medical officer of England.
 On Monday, Prime Minister Boris Johnson said the country needs to be prepared for the spread of the disease.
 The coronavirus outbreak will have a "substantial" impact on the global economy, according to the head of the World Trade Organization (WTO).
 "The effects on the global economy are ... likely to be substantial and will start to show up in the trade data in the weeks to come," Roberto Azevedo said on Monday.
 Also on Monday, the European Central Bank (ECB) called off a joint event with the European Commission that was to take place on Tuesday.
 "Several cancellations by conference participants" forced the ECB to cancel the event as the region is hit by the coronavirus outbreak.
 The death toll from coronavirus in France has jumped to four following the death of two people in the country's north, Le Parisien newspaper reported, citing the mayor of Compiegne.
 More than 400 Indian fishermen stranded on the Iranian island of Kish are calling for the Indian government to airlift them home.
 The fishermen, who sent out an SOS video, are reportedly running out of food, fuel supplies and fear for their health as they do not have protective masks in coronavirus-hit Iran, reported Indian TV channel NDTV.
 Subrahmanyam Jaishankar, India's minister of external affairs, tweeted that he is currently collaborating with the Iranian authorities for their return. 
 Working on the issue of Indians in Iran anxious to return due to #COVID19. Have seen many tweets in this regard. We are collaborating with the Iranian authorities to set up a screening process for return of Indians. (1/2)
 The risk level of coronavirus in the European Union has increased to "high", EU Commission’s President Ursula von der Leyen told a news conference in Brussels.
 "The ECDC [European Centre for Disease Prevention and Control] has announced today that the risk level has risen from moderate to high for people in the European Union. In other words, the virus continues to spread," von der Leyen told reporters.
 Europe registered 2,199 cases and 38 deaths, most of them in Italy.
 Follow the conference live here.
 As China marked its lowest number of infected cases since January 22, authorities closed the first of 16 hurriedly built makeshift hospitals after it discharged the last recovered patients, state broadcaster CCTV reported.
 Despite a sharp decline in new cases in Hubei province and the provincial capital Wuhan, controlling the outbreak remains at a crucial stage and strict prevention must be maintained, said Premier Li Keqiang during a meeting.
 Chinese cities with high population mobility such as the capital, Beijing, must control channels and prevent the spread of the epidemic, the government said in a statement on its website.
 Coronavirus presents the world economy with its greatest danger since the global financial crisis in 2008, according to the Organisation for Economic Cooperation and Development (OECD).
 A sharp slowdown in global growth is expected in the first half of 2020 as supply chains and commodities are hit, tourism drops and lower confidence grows.
 The global economy is set to grow only 2.4 percent this year, the lowest since 2009 and down from a forecast of 2.9 percent in November, according to the Paris-based policy forum.
 However, if the virus spreads throughout Asia, Europe and North America, global growth could drop as low as 1.5 percent this year.
 Sixty-six people have died and 1,501 have been infected by the coronavirus in Iran, Deputy Health Minister Alireza Raisi announced on state television.
 "The definite latest numbers we have are 523 new infections and 12 new deaths so the total number of those infected is 1,501 until now and the number of deaths is 66," he said.
 As voters in Israel head to the polls for the third time in less than a year, 16 dedicated voting booths have been set up across the country for about 5,500 people who have been placed in home quarantine after travelling to areas considered outbreak hotspots.
 Paramedics dressed in head-to-toe protective gear are standing next to the polling stations to assist possibly infected voters.
 "The situation is under control," said Moshe Bar Siman-Tov, director-general of Israel's Health Ministry, urging citizens to not hesitate to go out to vote.  
 Read more about the Israeli election here.
 Andorra, Iceland and Portugal reported their first cases.
 In all three countries - which registered one, three and two cases, respectively - the people who tested positive had recently travelled to Italy, which is the epicentre of the virus outbreak in Europe. 
 State radio announced that Mohammad Mirmohammadi, 71, a member of the Expediency Council, has died after contracting the coronavirus.
 Other prominent members infected in the country include Vice President Masoumeh Ebtekar and Iraj Harirchi, head of the country's task force on COVID-19.
 A health ministry official in Kuwait announced 10 new cases of coronavirus, bringing the total number to 56.
 The French government warned the effect of the coronavirus will be larger than previous estimates and promised to provide the necessary support to companies.
 Speaking on France2 television, Finance Minister Bruno Le Maire said the impact from the virus will be "much more significant" than the 0.1 percent reduction in growth that his ministry had forecast two weeks ago.
 The country's travel and tourism sectors have already been hit as companies scaled back travel and cancelled conventions.
 Officials in Australia said a woman and a male doctor have contracted coronavirus, becoming the country's first cases of community transmissions in the country.
 New South Wales state's Minister for Health Brad Hazzard said a 31-year old doctor has tested positive for coronavirus, though it is not clear whom he contracted the virus from.
 State officials said a 41-year-old woman was tested after her brother returned to Australia from Iran, a country that has emerged as a coronavirus hotspot in the Middle East.
 Dutch news agency ANP said Nike's European headquarters in the Netherlands would be closed on Monday and Tuesday following the infection of an employee with coronavirus.
 ANP said the patient was staying home in isolation for 14 days while the office in Hilversum would be disinfected.
 "The place is on lockdown," a security guard at the location told Reuters news agency, which said roughly 2,000 employees from 80 countries work at the facility.
 The leader of a church linked to about half of South Korea's more than 4,000 coronavirus cases apologised for the spread of the virus
 "I would like to offer my sincere apology to the people on behalf of the members," Shincheonji head Lee Man-hee told reporters in Gapyeong in a breaking voice.
 A report from Caixin on the way China counts coronavirus infections has analysed data from the province of Heilongjiang in the northeast.
 Since the beginning of last month, the National Health Commission has required that local authorities include "asymptomatic infected individuals" in the coronavirus data but it seems not all are doing so.
 Caixin says there were 104 asymptomatic cases in Heilongjiang on February 25 but they were not included in its 408 "confirmed cases".
 Before arriving at 104, 15 asyms were reclassified as “confirmed,” usually done as ppl start to show symts. Suggests portion of coronavirus asyms are presymptomatic. Studies suggest asyms are infectious, national measures treat them as such. #Covid19 https://t.co/mCpvskAGbB
 Indonesia's President Joko Widodo says two Indonesians have tested positive for the coronavirus and are in hospital.
 There has been speculation about the absence of cases in Indonesia since a Chinese tourist who returned from the holiday island of Bali was confirmed to have the infection.
 Australia's chief medical officer has said it is no longer possible to completely prevent people with the coronavirus from entering the country.
 "It is no longer possible to absolutely prevent new cases coming in," Brendan Murphy, Australia's chief medical officer, told reporters in Canberra.
 "We have got concerns about Japan and South Korea. They are working hard to control their outbreaks but we are still concerned that people in those countries and other high-risk countries may present with an infection."
 Australia has confirmed 29 cases of coronavirus.
 The government in Kazakhstan has announced it will bar Iranian nationals from the country from March 5 as part of a range of measures to tackle the coronavirus.
 It is also reducing the number of flights to and from Azerbaijan, which borders Iran, according to Reuters, and will no longer issue work permits to people from countries hit by the virus.
 Officials in the capital, Seoul, are accusing the Christian church at the epicentre of South Korea's growing coronavirus outbreak of murder.
 A case has been filed with prosecutors, claiming the leaders of Shincheonji - the church where the first cases were reported - are liable for the outbreak because they did not cooperate with efforts to stop the disease. 
 Park Won-soon, mayor of Seoul, said if Lee and other leaders of the church had cooperated, effective preventive measures could have saved those who later died of the virus.
 The state of Washington on the west coast of the US has reported its second death from coronavirus.
 Ian Morse, who is reporting from the area for Al Jazeera, says the person who died was a resident in the same nursing home where two cases of the virus were previously reported.
 The man was in his 70s, according to Reuters, citing local health officials.
 The Korea Centers for Disease Control and Prevention has released the first of its twice-daily updates on the coronavirus outbreak in South Korea - the largest outside China.
 The KCDC says four people died overnight and 476 new cases of the virus were confirmed.
 In total, 22 people have now died from COVID-19 in South Korea and 4,212 people are infected.
 China has announced its latest data on the coronavirus outbreak, reporting 42 new deaths.
 That takes the death toll in China to 2,912, and worldwide to more than 3,000.
 The National Health Commission says the latest deaths were all in the worst-hit province of Hubei and most were in its sealed-off capital of Wuhan.
 Officials said there were also 202 new infections, the lowest number since the end of January.
 Andrew Cuomo, governor of New York, said the US state had confirmed its first case of coronavirus in someone who had recently returned from Iran.
 Cuomo said there was "no reason for undue anxiety" and the risk remained low.
 Hello and welcome to Al Jazeera's live updates on the new coronavirus as it continues to spread from its epicentre in central China to countries around the world.
 I'm Kate Mayberry in Kuala Lumpur, taking over from my colleague Mersiha Gadzo.
 Click here to read updates from Sunday, March 1.
 Inside Story
 Is the spread of coronavirus out of control?
             Al Jazeera and news agencies
 	 
 https://www.aljazeera.com/news/2020/03/algeria-egypt-confirm-coronavirus-cases-live-updates-200301232150803.html
