Stay Informed - Coronavirus | Middleburg, VA
 Summary
 The novel coronavirus is a rapidly-evolving international virus outbreak that causes a respiratory disease known as COVID-19. The most up-to-date information can be found from the Centers for Disease Control and Prevention (CDC) and the Virginia Department of Health (VDH). Loudoun County supports this effort through the Loudoun County Health Department, Office of Emergency Management, and the Office of the County Administrator. The County’s dedicated page with all their latest information can be found at www.loudoun.gov/coronavirus.
 
 --------------------------------------
 Meal Voucher Program
 The Town Council approved a "Restaurant Support Program" in March that includes support for Middleburg Residents. Each household in Middleburg was mailed a packet of "meal vouchers" for the 13 participating restaurants in Town. We encourage you to support your local restaurants - read more at this link!
 We encourage citizens and visitors to support our local businesses in creative ways. Some retailers have changed hours of operation - and some retailers are offering no-contact pick-up options. Continue to support local businesses - call ahead and order for pickup, or shop your favorite in-Town store online (where available). Check out this link for a listing of all shopping options!
 Support for Water/Sewer Customers
 The Town Council authorized two key actions for all water/sewer customers (residential and commercial):
 Support to Business
 Business should stay in touch with the Town’s dedicated website located here about resources the Town is providing in regards to COVID-19. Town staff are eager to assist with any questions you may have.
 Town of Middleburg Operational Changes (revised as of March 23, 2020)
 To this end, the Town is making the following changes:
 Town of Middleburg Preparations
 Concerned members of the public can contact the Town Office at 540-687-5152 during regular business hours (M-F 8:30am-4:30pm) or the Town Administrator after hours.
 Loudoun County has a hotline for more information or concerned residents. That number is 703-737-8300 or at health@loudoun.gov.
 Town issues Declaration of Local Emergency (March 16, 2020)
 Prevention is Key
 In accordance with CDC and Commonwealth of Virginia directives, please maintain physical/social distancing, and limit your public activities. In nearly all cases, gatherings of more the 10 persons is restricted and should be avoided.
  Currently, the CDC recommends everyday preventive actions to help prevent the spread of respiratory viruses, including:
  Wash your hands often with soap and water for at least 20 seconds. Use an alcohol-based hand sanitizer that contains at least 60% alcohol if soap and water are not available.
  These are tried and true methods of preventing all types of infections, including COVID-19.
 Information for Employers and Places of Business
 The CDC has provided specific information for Employers and Places of Business. To prevent stigma and discrimination in the workplace and for business patrons, use only the guidance described below to determine risk of COVID-19. Do not make determinations of risk based on race or country of origin, and be sure to maintain confidentiality of people with confirmed COVID-19. There is much more to learn about the transmissibility, severity, and other features of COVID-19 and investigations are ongoing. Updates are available on CDC’s web page at www.cdc.gov/coronavirus/covid19.
 Take the following measures to keep your place of employment safe from COVID-19:
 Links and Further Information
 One of the most important steps that each of us can take right now is to stay informed. The Centers for Disease Control and Prevention (CDC) is the best source of information about COVID-19, including details on how the virus spreads, symptoms of infection, prevention and treatment, the global outbreak, FAQs, and much more. You are encouraged to take time to learn about COVID-19 by visiting the CDC’s website.
 Keep abreast of the latest information by visiting the following websites:
 CDC: www.cdc.gov/coronavirus/2019-ncov
 VDH: www.vdh.virginia.gov/surveillance-and-investigation/novel-coronavirus/
 Loudoun County: www.loudoun.gov/coronavirus  
 https://www.middleburgva.gov/323/Stay-Informed---Coronavirus