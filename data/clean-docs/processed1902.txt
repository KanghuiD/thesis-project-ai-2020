Coronavirus live updates: Global cases top 2 million, Trump casts doubt on China's numbers
 
 The number of confirmed coronavirus cases around the world topped 2 million Wednesday, according to data from Johns Hopkins University, with more than 137,000 confirmed deaths.
 Meanwhile, President Donald Trump questioned the number of coronavirus cases and deaths reported by China, saying the U.S. "reports the facts."
 In Europe, Germany became the latest nation to commit to cautiously reopening some businesses despite keeping a wider lockdown in place.
 Here's what to know about the coronavirus, plus a timeline of the most critical moments:
 Download the NBC News app for latest updates on the coronavirus outbreak.
 Henry Austin
 Almost 14,000 patients have died in British hospitals after testing positive for coronavirus as of 5 p.m. local time (12 p.m. ET) on Wednesday, the U.K.'s Department of Health said Thursday. 
 This was up by 861 from 12,868 the day, bringing the total to 13,729. 
 The Department of Health said that, as of 9 a.m. on Thursday, 327,608 people had been tested, of which 103,093 tested positive. Overall, 417,649 tests have concluded. 
 Rebecca Shabad
 Ivanka Trump and her family traveled to the Trump National Golf Club in New Jersey last week to celebrate Passover despite federal guidelines that advise against nonessential travel and a stay-at-home order is in effect in Washington, D.C., according to The New York Times.
 The president's eldest daughter went with her husband, Trump adviser Jared Kushner, and their three young children, to the Trump golf club in Bedminster, two people with knowledge of their travel plans told the Times, which reported on them Wednesday night. The family lives in Washington's Kalorama neighborhood in the northwest part of the city.
 Read the full story here.
 Minyvonne Burke
 New York City Mayor Bill de Blasio called on President Donald Trump for more federal stimulus money. 
 "The federal government was very quick to bail out the banks a decade ago, no questions asked. The federal government was very quick to bail out the auto industry. How about bailing out the nation's largest city? How about bailing out the epicenter of this crisis, where people have been suffering," he said at a news conference Thursday.
 According to the mayor, there are currently negotiations for another stimulus package with a focus on small business and paycheck protection. De Blasio said on the table is $100 billion for hospitals and health care workers, $150 billion for states and localities based on need, and $250 billion for small businesses. 
 De Blasio slammed Senate Majority Leader Mitch McConnell for being a "roadblock" to the city's getting the funding it needs. 
 "He has to hear our plea. He has to understand what it means for human beings that he's not allowing the kind of aid to flow that we need," the mayor said, adding, "It's clearly time for President Trump to speak up."
 "Saturday Night Live" comedian Michael Che announced that he's going to pay the rent for people living in a New York City public housing building where his grandmother, who died of coronavirus, lived.
 Che, 36, said he would cover the rent for all 160 units in the building.
 Ben Kesslen 
 An anonymous tip led to the discovery of 17 bodies crowded into a four-person morgue at one of New Jersey’s largest nursing homes.
 Police found the bodies this week at the Andover Subacute and Rehabilitation Center I and II in Sussex County, in northwest New Jersey, Rep. Josh Gottheimer confirmed to NBC New York. Sixty-eight people linked to Andover, NJ nursing home have died in recent weeks, including two staff members.
 “They were just overwhelmed by the amount of people who were expiring,” Eric C. Danielson, the town’s chief of police, told The New York Times.
 Erin Einhorn 
 In Detroit, the outbreak has stolen an estimated $348 million from the city's budget for the next 16 months — nearly a quarter of the money the city had been counting on.
 "We expected a downturn, and we prepared for it," Detroit Mayor Mike Duggan said this week as he announced layoffs for all of the city's 200 part-time or seasonal employees, as well as steep pay cuts or reduced hours for more than 2,200 full-time staffers. "We didn't expect it to be this sudden or this dire."
 Read the full story here. 
 Adela Suliman
 There have been 18 extra-judicial killings in Nigeria by security forces while the country has been under coronavirus lockdown, according to the country's National Human Rights Commission, more than the number of people who have died from the virus so far.
 The commission said the killings were a "display of impunity and reckless disregard for human life in law enforcement by security personnel." Most of the deaths occurred in the northern Kaduna state.
 The report, published late Wednesday, found 105 human rights complaints since the lockdown began on March 30, including excessive use of force, abuse of power and corruption by security forces — with Lagos State recording the highest number of incidents.
 Joe Biden said Thursday that the decision between protecting people’s health from coronavirus and reopening the U.S. economy is a “false choice” because you can't do one without the other.
 In an interview on MSNBC’s “Morning Joe,” co-host Willie Geist asked the former vice president what he would say to people who might favor a move by President Donald Trump to reopen the economy soon if it means they can get back to work and put food on the table.
 “I say we should not send you back to work till it’s safe to send you back to work. This is a false choice,” said Biden, who was speaking from his home in Delaware, sitting next to his wife, Jill Biden.
 “The way you revive the economy is you defeat the disease,” he said.
 Brandy Zadrozny and Ben Collins
 Nancy Ing and Reuters
 Amazon closed six of its warehouses in France on Thursday in one of the biggest fallouts yet from a growing stand-off with its workers over safety measures during the coronavirus pandemic.
 This comes as a French court ruled Tuesday that Amazon had to carry out a more thorough assessment of the risk of coronavirus contagion at its warehouses and should restrict its deliveries in the meantime, or face a fine. It ordered the company to limit deliveries to essential goods, such as food and medical supplies.
 The director-general of Amazon France, Frederic Duval, rejected the court's order on Thursday, saying the company had spent "colossal investments" to ensure the safety of their employees in warehouses. The firm will appeal the decision, he said in a radio interview.
 In a statement on Thursday, the union said it would continue to work for the recognition of the health and security of workers facing COVID-19.
 https://www.nbcnews.com/health/health-news/live-blog/2020-04-13-coronavirus-news-n1182376