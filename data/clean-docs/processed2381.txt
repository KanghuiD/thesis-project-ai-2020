Coronavirus: The latest developments from Spain and around the world | MARCA in English
 Editions:
 En/more-sports
 23.00. That's all for this Sunday's live blog on the coronavirus. We'll be back on Monday with another live blog and further updates on all the major stories surrounding the crisis.
 22:39. UK prime minister Boris Johnson has had to be admitted to hospital this Sunday due to the coronavirus. It was revealed that he tested positive for the virus on March 27, but now he is being admitted to hospital. According to a spokesperson: "On the advice of his doctor, the prime minister has tonight been admitted to hospital for tests. This is a precautionary step, as the prime minister continues to have persistent symptoms of coronavirus 10 days after testing positive for the virus."
 22:27. The doctor of Ligue 1 side Reims has committed suicide. According to Le Parisien, he had tested positive for coronavirus.
 22:12. The ERTEs requested by Barcelona, Espanyol and Alaves have been given the green light by the government. 
 20:51. New York is preparing for even tougher times and the city's mayor Andrew Cuomo has shared a motivational video this Sunday. 
 20:39. The World Tourism Organisation has painted a bleak outlook. As a result of the coronavirus, they're expecting seven years of lost growth. 
 19:57. In Paraguay, the coronavirus is hitting young people harder than in other countries. Of the 104 cases in the country, 17 are people between the ages of 20 and 24.
 19:48. The Madrid government have created a working group to deal with the situation and crisis that is expected after the current situation calms down. 
 19:04. Legendary American football player Tom Dempsey has died from the coronavirus. 
 18:41. The police have warned that 1.5 million accounts have been created on social media with the aim of manipulating information relating to the crisis. 
 17:57. The first case has been detected in Sudan. 
 17:24. The president of the Canary Islands' government has called for a plan to be developed to deal with the impact on tourism on the islands. 
 17:13. Masks are now obligatory in Lombardy for those going outside. 
 17:05. UEFA have clarified their stance on the issue of resuming football, with the governing body denying reports they had set a date by which the 2019/20 season must end. 
 16:30. The United Kingdom has registered 621 deaths in the last 24 hours from the coronavirus, bringin the total death toll to 4,394 and confirmed cases to 47,806.
 16:25. The Kremlin has warned the Russian people of the need to prepare for the impact of a global economic crisis as a result of the coronavirus pandemic.
 15:30. Serbian international footballer Aleksandar Prijovic has been sentenced to three months of house arrest after hosting a gathering of 20 people, breaching a national regulation. Read more here.
 14.37. A nursing home in Ourense in Galicia has registered 41 new cases of coronavirus. 
 14:30. Spain's death toll from COVID-19 has reached 12,418 deaths, with 674 more deceased in the last 24 hours.
 13:55. The Catalan government has ruled out the possibility of prolonging the school year into the summer. 
 13:39. Portugal has suffered 295 deaths from COVID-19 in the last 24 hours. 
 12:32. China has donated 100,000 face masks, 60,000 gloves and 5,000 protective glasses to Madrid City Council.
 11:33. Spain has reported 674 new fatalities in the last 24 hours, representing a decrease in deaths compared to Saturday, which saw 809 peris. 
 Spain has suffered a total of 12,418 deaths and 130,759 infected, of which 58,744 have required hospitalisation and 38,080 have been cured.
 11:06. Tokyo has seen a daily record of 118 new cases, the biggest spike in infections since the start of the crisis. 
 9:29. Brazil has exceeded 400 deaths and 10,000 positive cases of coronavirus. 
 9:00. The Knicks, the Brooklyn Nets and the NBA have donated one million surgical masks to the city of New York.
 8:45. Mexico has registered 19 deaths and 202 new infections in the last 24 hours.
 8:30. The number of deaths in Argentina from COVID-19 amounts to 43.
 8:10. South Korea registers 81 new cases of coronavirus in the last 24 hours.
 8:00. China registers a rise in new cases of coronavirus, with 30 more people affected, 11 more than the previous day.
 2020
  Unidad Editorial Información Deportiva, S.L.U. Todos los derechos reservados.
 Follow us
 https://www.marca.com/en/more-sports/2020/04/05/5e899405268e3eb1688b45c3.html
