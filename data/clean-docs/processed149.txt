Surfing: Coronavirus lockdown advice leaves room for confusion - BBC News
 In one surf-loving nation, Australia, some beaches are closed but others remain packed. So is it ok to surf during the Covid-19 crisis? Gary Nunn reports from Sydney.
 Mark Stockdale surfs every day of his life. But not today.
 "Ironically, it was too crowded," he tells the BBC. 
 He lives near surfing hotspot Bells Beach, Victoria, in Australia's south. Rather than people staying home, the opposite seems to have happened. 
 "It was even more packed than usual," the 48-year-old says. "The local surfing population is quite large, but people were also travelling in from Melbourne [110km or 70 miles away] because they're off work. The message isn't getting through."
 But that message is also "ambiguous", he says. On one hand, surfing is not in conflict with Australia's social distancing rules. Exercise is one of the four essential reasons to leave your house and, if done with a maximum of two people, 1.5m apart, it seems to fall within the rules. 
 That is - till the beach gets rammed.
 To navigate this, Mr Stockdale is taking extra precautions: "I've been waiting till conditions aren't very good. Or I go with my son to one of the lesser quality surf spots out the way of the crowds." 
 Katie Stoddart, 35, lives further up Australia in Kingscliff, New South Wales, and has been surfing for 16 years. 
 She has created her own rules: she only surfs alone, for 45 minutes maximum, only locally, avoids busy spots and gets up early to find a clear ocean area. 
 "If I can't find a quiet wave to myself (which happens often), I don't paddle out and surf that day - I just have a swim instead," she says. "I definitely don't get the best waves or the longest surf, but at least I get some exercise. It's better than nothing."
 One question that seems to be confusing people is the likelihood of airborne ocean-spray risks of viral transmission.
 Kim Prather, a scientist at the University of California San Diego, told the San Diego Union-Tribune: "Surfers are saying that they're safe if they stay six feet away from other people, but that's only true if the air isn't moving... Most of the time, there's wind or a breeze at the coast. Tiny drops of virus can float in the air and get blown around."
 Hannah Sassi, from the University of Sydney, adds: "She's merely stating it's a theoretical possibility - which, technically, it is." 
 "It's tricky though," Dr Sassi, an environmental virologist, tells the BBC. "Could virus particles be taken up by ocean breeze and travel? It's probably not that likely, and it could happen on the shore just as easily (if it at all)."
 She says there's no consensus the virus can be transmitted through airborne contact: "Research and medical data are pouring in very quickly. We're learning new things about Covid-19 every day, and the situation is constantly evolving."
 Close contact transmission, whether on land or at sea, is the biggest risk, she says: "Much of what we're cautioning against [such as not covering coughs and sneezes] is to prevent large droplets carrying the virus from expelling and settling on surfaces." 
 There's a positive for surfers and swimmers though, she says: "We haven't isolated the virus in ocean waters yet!"
 Some of Australia's most popular and famous surf beaches - including Sydney's Bondi and Manly - have been closed and fences erected to deter surfers and swimmers.
 It hasn't always worked. The Sydney Morning Herald has reported incidents of fence jumping, including one man who attacked a photographer, shouting repeatedly: "This is my beach."  
 Ethan Smith from Surfing NSW says the confusion is happening because, although surfing falls within the rules, some beaches are closed, causing surfers local to those beaches to seek out other waves.
 "Surfers who'd usually go to closed beaches like Bondi are going to other beaches and crowding them. You can see that from the sheer volume of people driving up coast lines looking for somewhere else," he says.
 "But you're supposed to stay at your local beach or stay home. They keep saying - it's recreation, not congregation."
 With many surf events cancelled, some surfers have put their board away altogether to be on the safe side, he adds.
 Seeing the crowded waves left Mr Stockdale feeling "really disappointed".
 Although he notes some in the surf community are doing their bit, others are letting the side down. "Surfers are renowned for being quite selfish," he says. "It's no surprise it'll become an issue. I'm trying not to contribute by making sacrifices - I just wish others would."
 Dr Sassi has some tips for surfers during the Covid-19 outbreak: "It's advice that I know surfers won't want to hear, but: sit it out [if it's really busy] or go at less busy times. Being on the water doesn't automatically mean you're not at risk of being exposed.
 "And definitely wash your hands/face as soon as you can after your surf!"
 The health secretary stresses the danger of the "invisible killer" and welcomes the public's effort to stay home.
 A history of chocolate in 10 quick facts
 https://www.bbc.co.uk/news/world-australia-52225031
