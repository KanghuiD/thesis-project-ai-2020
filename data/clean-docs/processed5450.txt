
 	
     
         Alexandria Health Department Urges Self-Quarantine for Certain Patrons after Potential COVID-19 Exposure at Murphy’s Irish Pub | City of Alexandria, VA
 301 King Street                           
                   Alexandria, VA 22314
 311 or 703.746.4311 
 For immediate police, fire or emergency medical assistance, call or text 911. 
 Need urgent help? These help hotline numbers are available 24/7:
 Are you ready? These links contain resources and information to help residents, businesses and visitors to prepare for all types of emergencies, and to stay safe.
 Learn more about pets and animals, schools and libraries, parks and farmers' markets, community gardens, and more. 
 There are many opportunities to get involved to help better the City of Alexandria. Opportunities include serving on a Board, Commission or Committee, providing input on a new City project or speaking at a City Council meeting.
 Information on how to get to and through the City of Alexandria, including walking, biking, bus, rail, air, ridesharing, and more.
  Historic Alexandria is a treasure trove of early structures, artifacts, and records that creates a unique way of life for its citizens and provides enjoyment for thousands of people who visit this special community every year. The City continues to add resources to its collection of local and nationally designated historic districts.
  Find out whether a permit is required, the type of permit needed, fees involved, and what requirements are necessary for the activity you want to engage in Alexandria.
 Parking Information for the City of Alexandria, Virginia.
 Alexandria is committed to ensuring our residents thrive through physical, mental, and social health.
  Agencies and programs that help maintain our safety and overall quality of life. These links contain information about the City of Alexandria's law enforcement agencies and public safety organizations, courts and judicial system.
  The City collects car and real estate taxes, maintains tax relief programs and assesses property values. Taxes can be paid in a variety of ways including online, by phone and by mail.
 Did you know that ACPS is one of the most diverse school systems in the country? Our students come from more than 80 different countries, speak more than 60 languages, and represent a myriad of ethnic and cultural groups. The Alexandria Library is an educational, user-oriented service institution providing free public access to recorded knowledge and ideas.
 The City provides public assistance as a safety net for individuals and families, including help with homelessness prevention, food, rent, utilities, medical coverage and prescriptions, job training and placement assistance, and much more.
 The City of Alexandria does not operate any public utilities. The following companies are the primary providers of their respective servers: 
 Whether you live here or are just visiting, Extraordinary Alexandria is a great place to shop, dine, take in the arts, get outdoors, and just have fun.
   If you are looking for small-town charm and big-city amenities, Alexandria is the place to find them. Alexandria has a fascinating history, and many of its historic buildings are still preserved today. The City's many historic homes, churches, businesses, and museums allow residents and visitors alike to experience the past that makes it the charming town it is today.
 Through engaging the community, encouraging participation, and facilitating access to arts and culture, the City builds a vibrant community for its residents, workers, and visitors.
     Find events and activities, shops and restaurants, concerts and performances, arts and culture, historic attractions, parks and libraries, farmers' markets, and more.
 Alexandria is a desirable location to live, work and play. The City owns many of the premier historic sites in Alexandria and it is charged with the conservation, interpretation and promotion of these links to the past.
 Alexandria is an active community that offers more than 900 acres of parks and dedicated public space, and a wide variety of neighborhood and recreation centers, pools, dog parks, farmers' markets, waterfront activities and more.
 Information about visiting Alexandria, including shopping, dining, attractions, accommodations, events listings and more. Plan your visit with an itinerary builder, interactive maps, hotel booking, online restaurant reservations and much more.
 Ideally located just across the Potomac River from Washington, D.C., Alexandria is nationally recognized as one of the best places to live and do business on the east coast.   Learn how we can help you start or relocate your business in Alexandria, and access resources for existing businesses and commuters.
 Find out whether a permit is required, the type of permit needed, fees involved, and what requirements are necessary for the activity you want to engage in Alexandria.
     The City collects car and real estate taxes, maintains tax relief programs and assesses property values. Taxes can be paid in a variety of ways including online, by phone and by mail.
 Connect with professional and knowledgeable staff for City service and information requests from every City department.
 What you need to know about animals, pets and wildlife in Alexandria.
 "Green" initiatives, air quality, noise, water conservation, mosquito and rodent control.
 Geographic Information Systems (GIS) centrally manages, shares and analyzes information about locations through specialized mapping technology. This information increases transparency, improves many City technology applications and provides critical data to decision makers and the public.
 Agencies and programs that help maintain our safety and overall quality of life. These links contain information about the City of Alexandria's law enforcement agencies and public safety organizations, courts and judicial system.
 The City collects car and real estate taxes, maintains tax relief programs and assesses property values. Taxes can be paid in a variety of ways including online, by phone and by mail.
 Learn more about Alexandria's accountable, effective, and well-managed government.
 The Alexandria City Council is composed of a Mayor and six Council members who are elected at-large for three-year terms. The Mayor, who is chosen on a separate ballot, presides over meetings of the Council and serves as the ceremonial head of government.
 The City of Alexandria has a strong commitment to citizen participation as evidenced by the number of citizen boards and commissions established by City Council. These bodies compose a formal system through which citizens can advise City Council on all major issues affecting the City.
 Information about the City of Alexandria's law enforcement agencies and public safety organizations, courts and judicial system.
 View a list of City departments, offices and other agencies, and learn about their role in the organization. 
 Alexandria voters elect a Mayor and City Council and three local officers, as well as state and federal representatives.
 Find upcoming and archived meeting dockets, agendas, minutes and videos for various City Boards, Commissions and Committees; including City Council, Planning Commission, Boards of Architectural Review, Board of Zoning Appeals, Transportation Commission, Traffic & Parking Board, and more.
 The City government acts with integrity in an open process, and provides timely access to clear, trustworthy information, presented and employed by all parties from the beginning to the end of the process, including the reasoning that leads to and supports the policy conclusion.
 Much of our work involves creating and improving infrastructure and planning for the future.  Learn more about major projects and plans here, and how you can get involved!
 Information about arts, culture and historic projects in the City.
 Information about commercial and residential development projects in the City.
 Information about environmental and "green" projects in the City.
 Information about infrastructure projects in the City.
 Information about neighborhood projects in the City.
 Information about parks and recreation projects in the City.
 Information about City strategic plans, master plans, studies and guidance policies.
 Information about public facilities projects in the City.
 Information about transportation projects and plans in and around the City.
 There's always something to do in Alexandria!  Browse community events, government meetings and activities, and important deadlines.                                                                                                      
                 
 View All Events & Public Meetings »  
 Use these shortcuts to perform some of the most common tasks on our site.
 ­­For Immediate Release: March 25, 2020
 On the evening of March 25, 2020, the Alexandria Health Department (AHD) was notified that a non-resident of Alexandria later confirmed to have the COVID-19 coronavirus spent time at Murphy’s Irish Pub (713 King St.). AHD’s investigation has determined that patrons at Murphy’s Irish Pub on the following dates and times may have been exposed to the virus that causes COVID-19:
 March 10, between 6 p.m. and 2 a.m.
 March 14, between 11 a.m. and 6 p.m.
 March 15, between 10 a.m. and 6 p.m.
 The Alexandria Health Department urges anyone who visited Murphy’s Irish Pub on the dates and times above to self-quarantine at home and call the Alexandria COVID-19 Information Line at 703.746.4988, weekdays from 9 a.m. to 6 p.m., for further guidance.
 Until those patrons visiting Murphy’s at the above time and dates receive specific guidance from AHD, they should stay at home, avoid visitors, not share items like towels and utensils, stay at least six feet away from other people in the household, and wash hands frequently.
 If You Have Symptoms of COVID-19
 The most common symptoms of COVID-19 are coughing, fever of over 100.4 F, and shortness of breath. Use the CDC's Coronavirus Self-Checker to review your symptoms. If you are concerned you may have COVID-19, call your primary care physician to ask whether you should come for an exam or test before visiting in person. Most people who get COVID-19 recover on their own at home and do not need testing or treatment. Anyone with symptoms of respiratory illness should isolate themselves; avoid contact with other people; wash their hands frequently; and disinfect surfaces regularly. If you have chest pain or shortness of breath, call or text 911.
 Protect Yourself and Others, Especially Vulnerable Community Members
 Wash Your Hands. Rub hands together with soap and water for at least 20 seconds.
 Use Hand Sanitizer. If you can’t wash your hands, use hand sanitizer that contains at least 60% alcohol as you would wash your hands, rubbing them together for 20 seconds. 
 Don’t Touch Your Face. Avoid touching your eyes, nose and mouth with unwashed hands.
 Stay Home. Do not go out except for essential trips like food purchases or medical care. It is especially critical to stay home if you are feeling sick. If you are well, avoid close contact with people who are sick.
 Use Your Elbow. Cough and sneeze into your elbow, not your hand. Alternatively, cough or sneeze into a tissue, then throw the tissue in the trash, and wash your hands.
 Disinfect Surfaces. Clean and disinfect frequently touched objects and surfaces.
 Manage Stress. The CDC recommends taking breaks from exposure to the news; take deep breaths or meditate; try to eat healthy; get sleep or rest; make time to do activities you enjoy; and connect with others to share your feelings. 
 To receive text message alerts from the City related to COVID-19, text ALEXCOVID19 to 888777. For more information about the COVID-19 coronavirus and how you can help protect yourself and those around you, visit alexandriava.gov/Coronavirus. For questions about COVID-19, call the Alexandria COVID-19 Information Line at 703.746.4988, weekdays from 9 a.m. to 6 p.m. Virginia residents can also call the Virginia Department of Health public information line, 877-ASK-VDH3, for questions about the novel coronavirus situation. If you have chest pain or shortness of breath, call or text 911.
 For inquiries from the news media only, contact Craig Fifer, Director of Communications and Public Information, at craig.fifer@alexandriava.gov or 703.746.3965.
 # # #
 This news release is available at alexandriava.gov/114406.
  
 https://www.alexandriava.gov/news_display.aspx?id=114406