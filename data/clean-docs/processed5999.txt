Coronavirus (COVID-19) and supply chains - IDH - the sustainable trade initiative
 We are committed to support our public and private partners, so that the consequences to those most vulnerable – the farmer, worker households and SMEs upstream in the supply chains – will be minimized. COVID-19 has demonstrated that we need to work together to win. We are all part of the solution. Businesses and governments must put partnerships at the heart of their solutions. Now is the time to show solidarity with the most vulnerable production systems that serve our world. We believe businesses that stand by their partners in this crisis will come out of this stronger in the long run. 
 Joost Oorthuizen, CEO of IDH 
 Join IDH, FMO, Proparco and Mirova on Thursday 23 April 2020 from 10:30 – 12:00 CEST to hear from companies and land-based operators in Africa about the measures they are taking on operational health and safety, business continuity planning and more, to support their workers, communities and business partners, and help halt the spread of COVID19.
  Read more here
 22 Apr 2020
 To support textile workers and factories, IDH is facilitating industry players to shift operations in…
 Originally posted on the Undercurrent News. The African aquaculture sector has become an even…
 21 Apr 2020
 Ten organisations working with 2,000 brands join forces to protect workers…
 20 Apr 2020
 Shrimp farmers are a hardy bunch. In recent years, the industry has been hit by a number of viral diseases, such as the White Spot Disease and Early Mortality Syndrome. The Asian shrimp industry alone reported to have lost at least USD $20 billion in the last decade. This series of shocks has led the industry to bounce back better: with farmers increasingly cutting out use of antibiotics, reducing environmental impact of shrimp production and finding technological solutions to boost their resilience. Last year, Indonesian start-up JALA together with IDH launched a data-driven system to monitor and prevent shrimp diseases. But can these experiences bolster the industry’s immunity to the global impacts of the Coronavirus pandemic?
 16 Apr 2020
 Multiple initiatives in India subsidize hospital transport, share information or help deliver essential items to…
 The FAO is playing a role in assessing and responding to the potential impacts of COVID-19 on people’s life and livelihoods, global food trade, markets, food supply chains and livestock.
 The WTO provides up-to-the minute trade-related information including relevant notifications by WTO members, the impact the virus has had on exports and imports and how WTO activities have been affected by the pandemic.
 WEF publishes information on the current situation with the COVID-19 coronavirus and its effects on global health, the economy, and more.
 Want to keep in touch?
                 Your browser is too old to optimally experience this website.
                 Upgrade your browser to improve your experience.
             
 https://www.idhsustainabletrade.com/covid-19-and-supply-chains/
