Financial Review - Business, Finance and Investment News | afr.com
 China's envoy in Australia says the Morrison government risks economic repercussions over 'politically motivated' calls for an inquiry into the virus outbreak.
 The Australian sharemarket is poised to rise sharply at the open; Congress has passed another $US484 billion stimulus bill; BoJ to double bond purchasing program.
 A near-$1 billion deal by Macquarie to sell down an office tower from its Martin Place development has fallen over, the highest-profile casualty yet in the commercial property market from the economic impact of the coronavirus pandemic.
 In Australia, the number of confirmed coronavirus cases rose by 14, or 0.2 per cent, on Sunday to 6709. There have been 83 deaths. Follow our live coronavirus blog here.
 The competition regulator will be on high alert for any attempt by Qantas to squash a reborn Virgin in the early stages.
 Atlantic Pacific Australian Equity Fund's Nicolas Bryon expected a sell-off for over a year and was primed to profit from the COVID-19 volatility.
 Paul Singer has joined the likes of Ray Dalio and Howard Marks in worrying central banks are killing the system they've prospered in. 
 How the coronavirus is changing markets, business and politics.
 Coronavirus: Need to know. Our daily reporting, in your inbox.
 Every country puts its own interests first, but the important question is how broadly or narrowly those interests are defined.
 Pandemics are bad for economies, but economic growth rebounds - and it often rebounds faster in places that take more forceful action to stop disease.
 The coronavirus is spreading in all Australian states. The total number of confirmed cases in Australia has exceeded 6600. Here's the latest on the numbers.
 A sharp fall in new apartment completions and weak approvals could be made worse by new rules that would make developers pay for more infrastructure.
 How do you juggle full-time work with trying to care for, educate and entertain the kids? Here's a survival guide from the experts.
 The US giant behind Latina pasta, Old El Paso tacos and Betty Crocker cake mixes had an extraordinary 28 per cent jump in sales in March and early April.
 The former Domain partnership has retooled and has a number of well-known investors backing it.
 Interest rates paid on highly-liquid cash deposits collapse after super funds sink billions of dollars into the banking system ahead of early access redemptions.
 CSL's extraordinary growth has won admirers, but in the increasingly important sphere of ESG investing, it's faced questions.
 News and current affairs and reality TV shows are driving increasing viewing during the coronavirus pandemic, according to data released by Fetch.
 The trust set up to protect Velocity Frequent Flyer members lent up to $200 million to Virgin Australia, positioning it as a creditor to the failed airline.
 Australia's aviation industry is a brutal business but experts say there is room for two airlines in this country.
 The CEO has stayed relatively quiet amid the COVID-19 turmoil, but Berkshire Hathaway's earnings, set to be released in May, will reveal his moves.
 Meanwhile a big week looms for US earnings season and the Federal Reserve is expected to keep interest rates near zero - if not negative.
 The divergence between the flying stock market and the dying economy is so extreme it is leaving many analysts scrambling for explanations.
 Global markets will head into the week optimistic governments will continue to do whatever is necessary to prop up the global economy.
 Oil market chaos has shone a spotlight on the flawed structure of many products sold to retail investors.
 It makes no sense to me to become agitated about health authorities using my data to combat COVID-19. Yet I still seem to be in a minority and that's a very big problem, writes Jennifer Hewett.
 Columnist
 Former prime minister Malcolm Turnbull's new memoir contains some candid revelations about the strength of the US-Australia alliance.
 Contributor
 Australia is a sitting duck if it does not get its new debts under control. That means Labor getting behind serious tax reform.
 The government has hurriedly dumped a large number of costly business rules. Why would we now want to bring them back?
 The purpose of COVIDSafe app is not to track Australians down, but to liberate them up to go back to a more normal way of life.
 Editorial
 The $7.5 billion in writedowns by super funds of their unlisted assets is nowhere near what is required under accounting rules. That needs to be reckoned with before the next infrastructure boom, warns Grant Wilson of Exante Data.
 Prime Minister Scott Morrison has shelved talk of a six-month economic "hibernation" and is nudging the states to allow more businesses to reopen before an official review of restrictions in less than three weeks.
 Multiple changes have been made to the new coronavirus contact tracing app to improve privacy and data integrity.
 Western Australia and Queensland eased some of their social distancing restrictions on Sunday, as new data reveals that Australians are increasingly looking to venture away from home as fresh case growth remains slow.
 The decision by superannuation funds to invest in infrastructure has been a boon for retirement savings and there is good reason for asset values to diverge.
 Sustainably expanding the nation’s secondary industry is an argument that is gaining increasing traction, reports Andrew Clark.
 The idea of American exceptionalism is being severely tested by coronavirus, which shows how much the US has in common with the rest of the world.
 The French government confirmed on Saturday that it had agreed a €7b loan package for the airline, while the Dutch government pledged between €2b and €4b.
 Government sources said the UK Prime Minister was now raring to go, having been given the green light by doctors to resume work.
 The US President and his top aides are working behind the scenes to sideline the WHO on several new fronts.
 The vast majority of Domain employees have opted in favour of a proposed cost-cutting measure as the company battles the coronavirus downturn.
 The Trenerry Property Group is understood to have struck a $14.5 million deal to buy the famed Mornington Peninsula hotel.
 A portfolio of 13 assets offered by HQPlantations delivered a 100 per cent sell out as a number of big rural holdings hit the market.
 Despite a positive outlook for farmland values, risks for the sector are higher due to the impact COVID-19 could have on global food supply chains.
 Company's decision comes as the national mood swings to easing pandemic restrictions.
 Listings and auction numbers are dropping, pushing deals between buyers and sellers who have little choice but to get transactions done.
 How you use exchange traded funds depends on your view of what markets are likely to do and where you see opportunities.
 Investor hopes for dividends from the major banks could rest on a guessing game ahead of the financials' earnings season.
 Research by scientists at Oxford university has found that the uptake required for effective contact tracing is 80 per cent of mobile phone users.
 Making big tech companies pay for news has a familiar ring to the music industry, which has managed to adapt after two decades of digital disruption.
 Big universities are worried by the loss of the Chinese student market but smaller universities have lost the all important India and south Asia markets.
 The NSW union says the official report on the risk to teachers, which informed federal government deicisions, is based on too small a sample size.
 Gym owners say they have enough hygiene and distancing protocols in place to be among the first businesses to reopen, but epidemiologists say that's a stretch.
 The acclaimed singer-songwriter wasn't due to be on the road for most of 2020 anyway, but she's angry on behalf of those who were.
 The kids are all right - it's their parents and grandparents who are hitting it hard when it comes to drugs and alcohol, and they've been doing so since before lockdown.
 An English art gallery has gone one better than virtual tours, offering an AI guide to take you remotely through its halls.
 Climate change has created a whole new industry on the Russian Steppes.
 The Daily Habit of Successful People
 https://www.afr.com/
