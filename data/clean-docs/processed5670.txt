Boy, 13, becomes youngest person to die from coronavirus in UK | Metro News
 135.6m shares
 A 13-year-old schoolboy from south London has reportedly become the UK’s youngest person to die after testing positive for Covid-19.
 Ismail Mohamed Abdulwahab, from Brixton, died at Kings College Hospital yesterday morning after being put into an induced coma. Hospital chiefs confirmed he had tested positive for Covid-19.
 His devastated family, who also lost their father to cancer, said the young teenager had no underlying health conditions and died alone due to coronavirus isolation rules.
 The Mohamed family said: ‘Ismail started showing symptoms and had difficulties breathing and was admitted to Kings College Hospital. He was put on a ventilator and then put into an induced coma but sadly died yesterday morning. 
 ‘To our knowledge he had no underlying health conditions. We are beyond devastated.’
 A GoFundMe page has been set up by the director of Madinah College, where Ismail’s sister works, to cover the funeral costs. 
 The page, organised by fundraiser Mark Stephenson, reads: ‘It is with great sadness to announce that the younger brother of one of our teachers at Madinah College has sadly passed away this morning (Monday 30th March 2020) due to being infected with Covid 19.
 ‘Ismael was only 13 years old without any pre-existing health conditions and sadly he died without any family members close by due to the highly infectious nature of Covid 19.
 ‘We at Madinah College would like to appeal to our brothers and sisters to donate generously to help raise £4000 for the funeral costs and to support the family, who sadly also lost their father to cancer.
 To view this video please enable JavaScript, and consider upgrading to a web
 						browser that
 						supports HTML5
 							video
 ‘May Allah grant the family patience through this difficult time and make it a means of drawing closer to Him.’
 Earlier, it emerged a 19-year-old had become the UK’s youngest victim. He is said to have had no pre-existing health conditions.
 The news comes after the deputy chief medical officer for England, Jenny Harries, said the news of young people with no health conditions dying after catching the virus, should serve as a ‘really sad’ reminder that everyone must stick to social distancing rules.
 Addressing the daily Downing Street press conference on Tuesday, Dr Harries said the deaths will have come as a ‘huge shock’ to the heartbroken families.
 She said: ‘Although what we know about this disease is that, in general, younger people are not having significant severe illness, it is the case, very sadly… that young people can still be affected.’
 Dr Harries said younger people ‘tend not to think of death’ and so it is ‘quite easy perhaps to not think of yourself as part of the risk, or part of the affected group’.
 She added: ‘They are really sad reminders that it doesn’t matter what age you are, you should be staying at home and observing all the social distancing measures we have highlighted.’
 It comes as the UK death toll for coronavirus rocketed by nearly 400 – the biggest daily jump – on Tuesday, bringing the total number to 1,789. A total of 367 patients in England died, and Scotland recorded 13 more deaths. 
 A further seven people were confirmed dead in Wales, while Northern Ireland recorded six more deaths today. Officials said 28 of the victims did not have underlying health conditions.
 For more stories like this, check our news page.
 Not convinced? Find out more »
 https://metro.co.uk/2020/03/31/boy-13-becomes-youngest-person-die-coronavirus-uk-12486740/
