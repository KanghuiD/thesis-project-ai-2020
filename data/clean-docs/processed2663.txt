Coronavirus in Scotland - gov.scot
 Protect yourself and others:
 The most common symptoms of COVID-19 are a new continuous cough and/or a fever/high temperature (37.8C or greater).
 A new continuous cough is where you:
 Health advice can be found on: NHS Inform.
 A free helpline can give advice if you do not have symptoms, but are looking for general advice: 0800 028 2816.
 The latest information and advice for professionals and organisations is on the Health Protection Scotland website.
 Overseas visitors to Scotland, regardless of their residency status, are exempt from NHS charges for both the diagnosis and treatment of coronavirus (COVID-19). 
 A total of 46,089 people in Scotland have been tested. Of these:
 Find out about the process for daily reporting of COVID-19 deaths. 
 Management information reported by NHS Boards shows:
 View the full daily data for Scotland, which includes NHS Board data on the above, and other management information such as number of 111 calls, care homes with outbreaks, and NHS absences. 
 The latest numbers will publish at 2pm each day. 
 The latest weekly report on COVID-19 related deaths (including those where COVID-19 is suspected) is available from National Records of Scotland (NRS).
 This report provides report provides a breakdown of deaths by age, sex, setting (hospital, care home or home) and area (NHS Board area and Local Authority). It is updated every Wednesday.
 The Department of Health publishes UK figures on behalf of all UK Chief Medical Officers. Test results from Scotland are included in the overall UK figure.
 Information on how to help your community is on the Ready Scotland website.
 If you run a business you can get advice by calling the helpline: 0300 303 0660.  It is open Monday to Friday, 8.30am to 5.30pm. Select option 1 to speak to the COVID-19 team. 
 The Find Business Support website gives the latest information and advice. 
 Advice about benefits and employment rights is on Citizens Advice Scotland.
 Advice for parents is on the ParentClub website.
 You must stay at home, do not travel unless for food, health and essential work.
 Guidance for British people travelling abroad: Travel advice: coronavirus (COVID-19).
 Travel advice and country specific information can be found at: FitForTravel.
 Guidance for visa holders and applicants in Scotland, and British nationals overseas who need to apply for a passport: Visa and passport advice (COVID-19)
 Our approach is guided by the Chief Medical Officer, and we continue to monitor the situation closely and to work with the WHO and international community.
 We are working with the Welsh Government, Northern Ireland Executive, and the UK Government to respond to the ongoing outbreak.
 Strategy/plan
 23 Apr 2020
 Your feedback will help us improve this site
 Thanks for your feedback
 https://www.gov.scot/coronavirus-covid-19/
