Live Blog: 9th person dies from coronavirus in JA as cases reach 488 | Loop News
 Jamaica has recorded its ninth COVID-19 death as cases on the island reached 488.
 Ages range from two to 87 and the vast majority of cases are female. Fifty-six people have so far recovered.
 Meanwhile, in a bid to contain the COVID-19 spread, Prime Minister Andrew Holness has continued new stricter measures including an islandwide daily curfew of 6 am to 6 pm until May 13.
 Related ArticleRelated article :WATCH: Stranded Jamaicans check in at NMIA
 Schools will remained closed until September and our borders will remain closed until May 31.
 No public gatherings shall exceed 10 persons. This is down from groups of 20 that was previously allowed.
 Global cases of the coronavirus pandemic have surpassed 3.6 million, and deaths worldwide have topped 253,000.
 National lockdowns, school closures, travel restrictions and health alerts have been issued globally as the coronavirus, which originated in Wuhan, China has infiltrated almost every region in the world.
 Governments in the region have enhanced their COVID-19 preparations with all eyes focused on safeguarding citizens from the threat of the virus being imported.  
 Follow our live blog for all updates related to COVID-19 in the Caribbean.  
 (The blog takes several seconds to launch)
 Get the latest local and international news straight to your mobile phone for free:
   
 Related article :WATCH: Stranded Jamaicans check in at NMIA
 View the discussion thread.
 http://www.loopjamaica.com/content/live-blog-covid-19-caribbean-3
