Unions demand coronavirus protections for workers | Las Vegas Review-Journal
 Las Vegas labor unions are calling for stronger protections and personal protective equipment for all front-line workers in Nevada.
 The Service Employees International Union has joined with the Culinary union to demand that large corporations provide stronger protections and personal protective equipment and training for front-line resort workers in the state.
 Leaders from both unions advocated for greater assistance in a 30-minute video press conference Tuesday.
 “We are home to some of the largest, wealthiest and powerful corporations in the country,” said SEIU Local 1107 Executive Director Grace Vergara-Mactal. “We are joining together to demand that these corporations show true leadership by looking past their bottom lines and profits and do what is right for health care workers, casino workers, grocery store workers, delivery drivers and more.”
 Culinary Local 226 and Bartenders Local 165 represent 60,000 workers in Las Vegas and Reno, including most of the Las Vegas Strip and downtown Las Vegas hotel-casinos. Union properties have been devastated by closures resulting from the COVID-19 pandemic that have idled hundreds of thousands of workers since mid-March.
 Some companies have agreed to pay and provide health benefits for their workers for the duration of the closures, but others have laid off or furloughed workers at various times since then, casting them to unemployment lines and state government assistance that has been slow to materialize.
 SEIU Local 1107 represents 19,000 workers statewide. In addition to public-sector workers, including many employed by the Las Vegas Convention and Visitors Authority, the union represents health care workers.
 Vergara-Mactal, Culinary Secretary-Treasurer Geoconda Argüello-Kline and SEIU President Brenda Marzan said large Las Vegas corporations have a moral obligation to help struggling workers because they make “billions of dollars” through their casinos.
 “I believe that corporations should be doing more on this issue, to do the right thing,” said Vergara-Mactal. “We are enterprises. We’re talking about families, children and workers that have worked so hard to make sure that their company makes money.
 “And Geo (Argüello-Kline) is right. They have billions and billions of dollars that their employees have made for them,” she said. “It is a moral duty and a moral decision for these corporations, multibillion-dollar corporations, that they care for the same people that helped them make that money, the same people that helped make our community work better.”
 Only two Las Vegas resort companies earned over $1 billion in profits in 2019, according to earnings reports.
 “It’s terrible what’s happening right now because these front-line workers are dealing every day and risking their own lives with a lot of fear,” Argüello-Kline said.
 “I know the pain that families feel because my daughter has a nursing certificate and know every day she goes to work she’s risking her life,” she said. “She’s very proud of helping people.”
 Argüello-Kline said 12 Culinary members have died from COVID-19 since the pandemic began. 
 The U.S. government reported Friday that 20.5 million people lost work in April, the worst set of jobs numbers since record-keeping began in 1948.
 As Nevada prepares to roll out antibody testing, health officials say there’s no substantial evidence that coronavirus was present at the January technology conference.
 Phase 1 of the state’s so-called reopening begins at 12:01 a.m. Saturday, following Gov. Steve Sisolak’s Thursday declaration.
 NFR meets NFL in December when the Indianapolis Colts visit the Las Vegas Raiders. But the league dodged many potential city conflicts when drafting the 2020 schedule.
 Taxicab demand in Las Vegas has decreased steeply since the resort corridor was shut down by the coronavirus.
 Operators of The Strat reopened a slot route operation in Montana on Monday and so far, company executives believe the pent-up demand there could follow in Nevada.
 Speaker Nancy Pelosi said Thursday she is prepared to unveil a stimulus bill written by Democrats that would increase federal assistance to states and cities, expand nutritional benefits and Medicaid funding, and provide monthly payments to individuals.
 It’s not just casinos getting hit hard by the COVID-19 pandemic; it’s also the companies manufacturing the games that fill them.
 The move by the 112-year-old storied luxury department store chain follows the bankruptcy filing by J.Crew on Monday.
 Three Nevada Amazon warehouses have now reported positive cases of the coronavirus.
 Powered by WordPress.com VIP
 https://www.reviewjournal.com/business/las-vegas-unions-calls-for-worker-protection-against-coronavirus-2011260/
