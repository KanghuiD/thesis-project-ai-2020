Global confirmed Covid-19 cases pass 2.5m but Italy sees first significant fall in infections – as it happened | World news | The Guardian
 This blog is now closed.
 Helen Sullivan (now and earlier); 
 Kevin Rawlinson, 
 Damien Gayle and 
 Frances Perraudin
 Wed 22 Apr 2020 00.54 BST
 First published on Tue 21 Apr 2020 00.53 BST
 12.52am BST
 00:52
 We’ve launched a new live blog at the link below where we’ll be bringing you the latest developments in the coronavirus pandemic: 
 Updated
 at 12.54am BST
 12.36am BST
 00:36
 Maanvi Singh
 In today’s White House press briefing Trump said the US has “tested more than any country in the world, even put together.”
 This is false.
 Overall, the US had administered more than 4.5m coronavirus tests, according to the Covid Tracking Project. From a very slow start, the US, with a population of 329m, had ramped up to a testing rate of one in every 100 people — similar to South Korea. Germany has done even better, testing every 1 in 63 people.
 The UK, however, is behind, having tested only 1 in 230 people.
 In America, despite the recent increase in testing, backlogs are reported in labs across the country, and many people with symptoms — including health workers — are still struggling to access tests
 12.35am BST
 00:35
 Sarah Boseley
 UK guidance for people aged 70 and over to self-isolate is leaving people aged 60 to 69 at increased risk from coronavirus, say scientists.
 Prof Azeem Majeed and colleagues at Imperial College London (ICL) noted that other countries had different policies and the World Health Organization said the highest risk was in over-60s.
 According to a paper published by the Centre for Evidence-Based Medicine at Oxford University, the death rate among people in their 70s is 8%, and the rate among those in their 60s is 3.6%, which the ICL scientists said was “still substantial”.
 They recommend that the 7.3 million people in their 60s in the UK should be more careful about physical distancing and personal hygiene.
 12.05am BST
 00:05
 Today’s White House press briefing has ended. We’ll have a summary for you shortly. 
 11.57pm BST
 23:57
 “The American people want to ensure that we have jobs for American people,” says Trump, not jobs for people who come in, “in many cases illegally.” 
 “We’ll see you all tomorrow and we’ll have a couple of interesting things for you tonight,” says Trump as he leaves the podium. 
 11.55pm BST
 23:55
 You can stream the White House press briefing live here: 
 11.54pm BST
 23:54
 Any other Billy Joel fans out there? 
 🎶is such a lonely word🎶 https://t.co/K8SYjIt7sd
 11.50pm BST
 23:50
 The World Health Organization chief has just tweeted this: 
 Honesty.
 11.49pm BST
 23:49
 Trump: “We have one of the most successful, if you can call it, mortality rates... One person is too many, but we’ve tested more than any other country in the world, put together.” (This is not true). 
 Trump is now talking about China and the economy. He says part of the reason he was elected is that he is touch on China. He says the economy was doing really well, then out of nowhere came the “Invisible Enemy” (I’m using caps here because he does in his tweets about it), “and we’ll be talking a lot about where it came from.” 
 at 11.51pm BST
 11.40pm BST
 23:40
 Real whopper from Trump a minute ago – he said the US has done more testing than “all other countries – combined”:
 #Trump - “We’re doing a great job on testing.” Also “not everyone wants to do such significant testing.” Who? Everyone involved in scientific and medical side of fighting #coronavirus wants more testing Also: “We’re doing more than everybody else combined. *Not true.
 11.34pm BST
 23:34
 Trump says he will probably sign the executive order halting immigration on Wednesday (in the US). 
 After the 60 days he could “roll it for 30 days or roll it for much longer than 60 days,” he says. 
 11.29pm BST
 23:29
 Trump says there will be more information on the immigration ban soon. 
 “It’s a strong order. It involves a big circle, as you know.” The “big circle” in question appears to be the world. 
 11.25pm BST
 23:25
 Trump says he does not know Kim Jong-un’s condition. “I can only say this: I wish him well."
 11.22pm BST
 23:22
 You can watch the White House press briefing live below: 
 11.20pm BST
 23:20
 Dr Birx says the US has one of the lowest mortality rates worldwide – meaning deaths per number of confirmed infections. 
 It’s worth remembering however that the US has the highest number of deaths – 44,228 according to Johns Hopkins University data – which is just under 20,000 more people who have died than in Italy, which has lost 24,648 people to the virus. 
 Trump also mentioned the mortality rate. From my colleague Maanvi Singh: 
 “Our mortality rate remains roughly half” that of other countries, Trump said, adding that the rate was “one of the lowest in any other country in the world.”
 The president is half-right — the US does indeed appear to have a lower case fatality rate than several countries, including Italy, Belgium and the UK, according to the Johns Hopkins tracker.
 The case fatality rate refers to the number of deaths divided by the number of confirmed cases. Johns Hopkins has calculated that figure for about 130 countries. but the US’s rate is far from the “lowest” of any country.
 The US rate is 5.4% — more double the rate of Japan and South Korea (2.2%), and several times the rate of Singapore, with a rate. of .1%.
 https://www.theguardian.com/world/live/2020/apr/21/coronavirus-live-news-donald-trump-downplays-oil-price-crash-as-short-term-latest-updates
