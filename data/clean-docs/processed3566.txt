Seneca County Chairman urges visitors to end "sheltering" from COVID-19 in his County - 870 AM 97.7FM News Talk WHCU870 AM 97.7FM News Talk WHCU
 
 Listen Live
 3 weeks ago
                                 in COVID 19, Local
  Photo: Saga Communications
 Waterloo, NY (WHCU) – Seneca County Chairman of the Board of Supervisors Robert Hayssen is calling on residents of more populated areas of the state and country to avoid sheltering in Seneca County.   The Governor of New York State has issued a State of Emergency and the Seneca County has done the same.   Seneca County hosts people from around the Globe that want to visit the great area known as the Finger Lakes Region.
 “We need to work together at this time to slow the spread of the COVID-19 virus down,” said Hayssen.  “As Chairman of the Board of Supervisors for Seneca County I am encouraging that vacation rentals temporarily postpone renting until we get past this pandemic.”
 Seneca County has a limited number of healthcare resources and is not equipped for a broad spread of the virus.
 Seneca County joins Essex and other counties throughout the state who find themselves in a similar predicament and has called on the New York State of Association of Counties to play a greater role in getting support from New York State to enact stricter regulations related to vacation rentals and second homes. News articles in these communities can be viewed at:
             https://www.pressrepublican.com/news/coronavirus/essex-county-to-non-residents-stay-away/article_6fe9963f-5e4c-542b-a307-d78e17a7439a.html
 https://www.wsj.com/articles/upstate-counties-urge-new-york-city-residents-to-stay-in-primary-homes-11585092090
 Deborah Birx, the State Department doctor who is advising Vice President Mike Pence, said at a White House news conference on Tuesday. “Everybody who was in New York should be self-quarantining for the next 14 days to ensure that the virus doesn’t spread to others.”
 “I know this will be a hardship for those who must cancel reservations and lose important income.  Some individuals depend on this income for maintaining the property and various other cost associated with short term rentals.  We do not need people to possibly be quarantining themselves or not knowing they are infected until it is too late,” said Seneca County Manager Mitch Rowe.
 At this time it is more important to protect all the individuals that can be associated with vacation rentals.   Hopefully this industry will be helped or aided by the State or Federal Government minimize loss of revenue.
 15 mins ago
                     in Trending, World
 The exchange comes as part of an agreement brokered last year at a summit of the leaders of Ukraine, Russia, Germany and France.
 20 mins ago
                     in Coronavirus Updates, National, Trending
 The new guidelines, expected to be announced Thursday, are aimed at clearing the way for an easing of restrictions in areas with low transmission of the coronavirus...
 22 mins ago
                     in Local
 ITHACA, N.Y. (WHCU) – Bikewalk Tompkins says a bicycle can be a valuable tool in helping the community pedal forward…
 28 mins ago
                     in Coronavirus Updates, Trending, World
 China, where the coronavirus pandemic started in December, is cautiously trying to get back to business, but it's not easy...
 45 mins ago
                     in National, Trending
 A recent federal relief package makes it easier for people financially harmed by the coronavirus outbreak...
 870 AM 97.7FM News Talk WHCUCopyright 2020 Saga Communications, Inc.
 EEO Report |
 					FCC Public File  | For assistance accessing public files, contact
 					pfhelp@cyradiogroup.com
 					Contests & Rules | Some images copyright AP, Clipart.com, Reuters
 Non-Discriminatory Advertising Letter | Advisory Public Notice - Non-Discriminatory Ad Contracts
 				
 https://whcuradio.com/news/025520-seneca-county-chairman-urges-visitors-to-end-sheltering-from-covid-19-in-his-county/