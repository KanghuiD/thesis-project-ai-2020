Long Island Business News – The premier source of Long Island news and data on business, economic trends and the region’s robust entrepreneurial sector.
 Contributions from the Rauch Foundation, National Grid and M&T Bank.
 Opinion: I’ve been to the Oklahoma City National Stockyards and let me declare with certainty: We are never going to run out of meat.
 The three highest-priced home sales in Great Neck last month ranged from $1.825 million to $1.925 million.
 The SBA is encouraging small businesses to continue applying through approved banks, credit unions and non-bank lenders.
 The move comes in anticipation of resumed airline travel, so airport officials can ensure the health and wellness of those spending time at the airport.
 U.S. meat supplies are dwindling due to coronavirus-related production shutdowns. As a result, some stores like Costco and restaurants like Wendy’s are limiting sales.
 The great majority of people being newly hospitalized with the coronavirus in New York are either retired or unemployed and were avoiding public transit, according to a new state survey, the first such look at people still getting seriously ill despite six weeks of severe social distancing.
 29 percent of funds are spoken for after first three days of the program, according to numbers released by the SBA last night.
 The look and feel of print, on your screen.
 Current edition
 Access the latest special publication
 Have you ordered food for curbside pickup?
 View Results
 January 24, 2019 
 January 18, 2019 
 January 17, 2019 
 https://libn.com/
