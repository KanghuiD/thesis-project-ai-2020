How it happened: Coronavirus live updates and news, Monday, April 6, 2020 | The Canberra Times | Canberra, ACT  
 The ACT is approaching 100 confirmed cases of coronavirus, and authorities are still trying to work out how two of those people caught it.
 Across the world, there are more than 1.26 million cases and 68,412 deaths.
 There are 5687 cases in Australia. Of those, 2315 people have recovered and 34 have died. Ninety-one people are in intensive care, including three in the ACT.
 The ACT will begin testing a random sample of people who would not otherwise qualify. 
 Over the past week, there has been an average of 246 new cases reported each day. Of the newly reported cases, the majority have been from New South Wales, followed by Victoria and Queensland.
 The blog sometimes takes a moment to appear, but will show up here.
 Sign up to receive our Breaking News Alerts and Editor's Daily Headlines featuring the best local news and stories.
 https://www.canberratimes.com.au/story/6712284/coronavirus-live-updates-and-news-monday-april-6-2020/
