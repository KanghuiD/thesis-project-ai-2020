Coronavirus: how we act will stay in memory, says Jim Saker | Opinion
 I am writing this article the day after Boris Johnson announced the first stage of what have become the most stringent restrictions on British life since World War II as a response to the threat of the Coronavirus.  
 Several of our motor groups had already announced they were closing down their operations, then all showrooms were told to close.
 The obvious concern is for the welfare of their staff and this has to be the priority.
 Despite Government promises of long-term support, many of the companies in our sector face an uncertain future.
 Some industries, such as the general transport sector, will have to be supported, airlines are facing a massive problem and one suspects that cruising on large ships will not appear that attractive going forward. 
 Part of the problem for our industry is the need for consumer confidence; the idea that customers coming off the back of a prolonged shut down will rush out into the new car market is not realistic. 
 The dystopian view is that a number of the automotive retail chains will fail, leading to a radical pruning of the weaker or poorly structured organisations.
 The upside of a prolonged period of inactivity is the potential for a boom in aftersales with people having delayed servicing while the outbreak continues. 
 The industry will bounce back, but it is important that in the meantime dealers remain visible to their customer base and are seen to support their local community.  
 A number of groups have offered vehicles to help distribute food to people who are isolated while others are supporting homeless charities.
 People have long memories and remember those who do good in tough times. 
 His approach based on his Christian faith sees his return on sales outstrip that of his staff turnover.
 When he was away from the business after suffering a brain haemorrhage the organisation continued to flourish and kept faith with his faith.
 The company gives £230k a year to charitable projects.
 The underpinning concept is that the workforce spend one-hour-in-10 working for the benefit of someone less fortunate than themselves.
 At a time like this when some of our employees (and their organisations) are facing an uncertain and potentially threatening future the need for generosity from government, the banks, shareholders, employers and employees is important. 
 The selfish panic buying and exploitation of the situation by some companies will be long remembered but so will those who help and support others who are struggling.
 I hope everyone comes through the crisis both physically and commercially in good health. 
 In this issue
 Coronavirus
 AM survey reveals huge concerns while confusion reigns over Government compensation / p6 - 7
 Could coronavirus speed up digital transformation of automotive retail? / p8 - 9
 Face to face: Drayton Motors
 A strong emphasis on people and a collective willingness to roll up their sleeves propels profits / p22
 Brand values: DS Automobiles
 A unique form of avant-garde branding is helping to set the young company apart from its rivals / p34
 Used cars
 Are used approved schemes hurting dealer profitability? / p53
 READ NOW
 27/03/2020
 30/03/2020
 17/04/2020
 Automotive Management Live: Where franchised and independent dealers will find everything they need to know about operating a modern showroom and service and repair facility fit for the digital age.
 When: November 12 2020
 Where: Birmingham NEC
 ​Find out what features are in this month's, and future, issues
 Media House, Lynch Wood, Peterborough, PE2 6EA - Registered number 01176085
 www.am-online.com is the leading website for the UK motor trade sector with a 56% market share (site visits, December 2019), according to Hitwise.
 https://www.am-online.com/opinion/2020/04/18/coronavirus-how-we-act-will-stay-in-memory-says-jim-saker
