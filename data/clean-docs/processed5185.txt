Topic | Coronavirus pandemic | Australian Financial Review
 Women have borne the brunt of the recession, losing jobs at far higher rates than men. Easing COVID restrictions in regional Victoria has seen that state record slight rises in payroll jobs and wages. Follow updates here.
 Treasurer Josh Frydenberg's budget took the heat off the states, where Daniel Andrews is holding his ground and the NSW-Qld border battle may be back to square one. Here's your Wednesday briefing.
 More than 99 per cent of businesses will get access to a new investment allowance, which Josh Frydenberg calls 'a game-changer' for economic recovery.
 On top of Labor's support, Anthony Albanese has promised $500 million for the first instalment of a larger social housing policy he will take to the next election.
 Leading Australian businesswomen have slammed the Coalition's budget for failing to support women during a 'pink recession', saying its lack of funding for childcare and focus on stimulus for sectors dominated by men leaves female workers behind.
 Universities have been given breathing space in which they can rebuild international enrolments.
 The budget is chock-full of optimistic assumptions. But history says the harder Australia falls, the harder it rebounds.
 Treasury has based budget forecasts on the hope that a coronavirus vaccine will be widely available late next year. Medical experts suggest that timetable may be too optimistic.
 The Malaysian budget airline said it has proposed restructuring $US15.3 billion of debt and reducing its share capital by 90 per cent to continue as a going concern.
 'I've had more than enough' of COVID-19, the British Prime Minister says as he vows to build a 'New Jerusalem' rather than just restore normality.
 NSW Premier Gladys Berejiklian says the state's first unknown community cases for almost two weeks show that Queensland's criteria for reopening the border are unrealistic.
 A straw poll suggests many intend to save the gains from income tax cuts in the federal budget, flying in the face of the government's hopes the relief will boost consumption.
 Income tax cuts, a boost to the home buyers deposit scheme, new wage subsidies and a super fund comparison tool are among the highlights for Millennials.
 Offices aren't going away, but they are going to face more competition than they used to and employers will need to think harder about the purposes they serve.
 The prolific Irish fashion designer and creative powerhouse behind the reinvention of Loewe is the ski wear brand's latest recruit. Just don't call him a star.
 Despite the fast-tracking of a bill containing business and income tax cuts in the budget, the backdated income tax cuts may not flow until December.
 The virus has dictated its design, from more collaboration spaces and distraction-free zones to contactless building entry and banning some in-person meetings.
 Its designer says the $US100 commemorative currency 'includes more than a hint of superhero qualities in history's most fascinating president', Donald Trump.
 The shoe maker says the trainers were inspired by 'the pandemic-related rise of remote working' that has led many businesses to rely on the messaging software.
 In a high-stakes political move, Donald Trump accused Democrat House Leader Nancy Pelosi of acting in bad faith and rejected her demands for a $3.4 trillion bailout.
 The superannuation act has recently been amended to require trustees to promote the financial interests of members.
 Researchers specialising in the delicate art of ranking and analysing super funds say the ambitious government program could go badly wrong.
 Josh Frydenberg is betting business and households will take advantage of tax cuts and concessions to invest and spend more. But he's keeping his fiscal options open even after such a big-spending budget.
 The Daily Habit of Successful People
 https://www.afr.com/topic/coronavirus-pandemic-1ndb
