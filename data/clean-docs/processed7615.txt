Opinion: Let’s create a Health Force to mobilize against the coronavirus – The Colorado Sun
 						Special to The Colorado Sun					
 Colorado has a tradition of showing its best during times of crisis. Our response to the COVID-19 pandemic has been no exception.
 For the past few weeks, I’ve had the honor to work alongside leaders from across the state as part of the effort to accelerate Colorado’s response to the virus. At every turn, I’ve seen Colorado step up, as it always does, with a surge of innovation, grit and collaboration.
 Our colleges, universities and bioscience companies have accelerated research and development of diagnostics, vaccines and therapies and expedited critical supplies to the front lines.  
 Over just 48 hours, our universities and federal labs set up a test network for new critical supplies of personal protective equipment coming into the state. The maker community has delivered thousands of critical supplies across the state. As always, Colorado commits, collaborates and creates to get the job done.
 However, much of this coordinated and accelerated response would not be possible without the dedication of Colorado’s public servants. 
 The latest from the coronavirus outbreak in Colorado:
 >> FULL COVERAGE
 In his typical fashion, Sen. Michael Bennet has worked tirelessly and behind the scenes to connect, assist, communicate and innovate for Colorado during this crisis.  
 When initial concerns about ventilator shortages for the state arose, Bennet asked major manufacturers and defense contractors with substantial Colorado presence to assist. Many have.  
 Lockheed Martin’s national efforts are led by their Colorado operations, where it is increasing hiring, scaling up innovation and giving its employees vouchers to spend at local restaurants.  
 Woodward, a $2.6 billion manufacturing firm based in Fort Collins, has worked closely with Colorado State University to develop and produce low-cost ventilators.  
 Sen. Bennet is also proposing the bold ideas we need to overcome this crisis. Last week, Bennet and Sen. Kirsten Gillibrand, D-N.Y., proposed creating a Health Force, a historic initiative to employ hundreds of thousands of young people and unemployed Americans to support our public health response to the pandemic. 
 The Health Force would create a federally supported and locally managed program to train and deploy public and community health frontline workers to conduct testing, contact tracing and eventual vaccine administration. 
 These positions would complement America’s highly trained and skilled medical professionals already fighting on the front lines. It would also complement the many burgeoning digital health and exposure alerting efforts underway — many of which have close Colorado connections.  
 There is no better and no more urgent opportunity for the country right now than a national service employment effort focused on our health and recovery. 
 UNDERWRITTEN BY TOBACCO-FREE KIDS ACTION FUND
 OUR UNDERWRITERS SUPPORT JOURNALISM.   BECOME ONE.
 As an educator, I know how many students are worried about internships, co-ops and jobs. I cannot imagine a more inspiring service opportunity for Americans right now, as everyone searches for an opportunity to work and to help the mission. 
 We hear this every day from each one of our great students, who believe their future is all about solving the world’s Grand Challenges.
 Let’s rally to provide that path for Coloradans and the country right now. 
 And as we do so — remember how much we owe to all the wonderful, selfless public servants in Colorado who are giving their all to help lead us through.
 J.B. Holston is the Dean of the Daniel Felix Ritchie School of Engineering and Computer Science at the University of Denver. He has recently been volunteering as a senior adviser to the State’s Innovation Response Team for COVID19.
 				Support independent, Colorado-owned journalism by joining The Colorado Sun.			
 Denver attorney Steve Zansberg imagines what different leadership in the White House briefing room might look like.
 Colorado’s system of 52 school-based health centers has grown into a crucial element of overall population health, delivering more than 100,000 visits last school year.
 After a lifetime of watching big moments in sport history together, the coronavirus has changed that bonding experience for us for the foreseeable future
 https://coloradosun.com/2020/04/26/colorado-coronavirus-bennet-public-health-opinion/
