Coronavirus: Tiger at Bronx Zoo tests positive for Covid-19 - BBC News
 A four-year-old female Malayan tiger at the Bronx Zoo has tested positive for the coronavirus.
 The tiger, named Nadia, is believed to be the first known case of an animal infected with Covid-19 in the US.
 Nadia, along with six other big cats, is thought to have been infected by an asymptomatic zoo keeper.
 The cats started showing symptoms, including a dry cough, late last month after exposure to the employee, who has not been identified. 
 "This is the first time that any of us know of anywhere in the world that a person infected the animal and the animal got sick," Paul Calle, the chief veterinarian at the zoo, told Reuters news agency on Sunday. 
 There have been isolated instances of pets testing positive for the coronavirus elsewhere in the world, but experts have stressed there is no evidence they can become sick or spread the disease. 
 Mr Calle said he intends to share the findings with other zoos and institutions researching the transmission of Covid-19.
 "We tested the cat [Nadia] out of an abundance of caution and will ensure any knowledge we gain about Covid-19 will contribute to the world's continuing understanding of this novel coronavirus," the zoo said in a statement.
 Nadia, her sister Azul, as well as two Amur tigers and three African lions who showed symptoms, are all expected to make a full recovery, the zoo said.
 The big cats did have some decrease in appetite but "are otherwise doing well under veterinary care and are bright, alert, and interactive with their keepers", it said.
 The zoo said it is not known how the virus will develop in animals like tigers and lions since various species can react differently to new infections, but all the animals will be closely monitored. 
  We couldn’t pick so today we’re enjoying #tongueouttuesday and #tigertuesday together. We hope you won’t mind 😌🐅 . . #bronxzoo #tiger #tigermountain
 A post shared by  Bronx Zoo (@bronxzoo) on Sep 10, 2019 at 8:29am PDT
 None of the zoo's other big cats are showing any signs of illness.  All the tigers showing symptoms were housed in the zoo's Tiger Mountain area. It is unclear if the others will be tested.
 All four zoos run by the Wildlife Conservation Society in New York City, including the Bronx Zoo, have been closed to the public since 16 March.  New measures will now be put in place to protect the animals and their caretakers at all the facilities. 
 This coronavirus was first detected in humans in the Chinese city of Wuhan late last year. 
 The coronavirus (called Sars-CoV-2, which causes the disease Covid-19) is thought to have originated in wildlife and been passed to humans via a live animal market in Wuhan.
 The pandemic has been driven by human-to-human transmission, but the infection of Nadia raises new questions about human-to-animal transmission.
 There have been less than a handful of isolated reports of companion animals testing positive for coronavirus, including two dogs in Hong Kong. 
 There is "no evidence that any person has been infected with Covid-19 in the US by animals, including by pet dogs or cats," the zoo's statement noted.
 That is also the view of the World Organisation for Animal Health and  the World Health Organization (WHO), which says there is no evidence that pet dogs or cats can pass on the coronavirus.
 The World Organisation for Animal Health says studies are under way to understand the issue more.  and urges anyone who has become sick to limit contact with pets.
 Dr Sarah Caddy, Veterinarian and Clinical Research Fellow at the University of Cambridge, is among experts to respond to the reports.
 "It is surprising that the tiger has become infected with what must have been a fairly low dose of virus - we can assume the tiger did not have continual close contact with the asymptomatic zoo keeper," she said about the transmission. 
 "It is also interesting that the tiger showed clinical signs consistent with Covid-19 in humans. Although scientific proof is lacking, the chance this is just a coincidence is low."
 Conservation experts have warned that the virus could pose a threat to some wildlife like the great apes - and have said measures are needed to reduce the risk of wild gorillas, chimps and orangutans.
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-us-canada-52177586
