Russian And Chinese Misinformation Makes Useful Idiots Go Wild - OpEd - Eurasia Review
 A Journal of Analysis and News
 By Baiba Zile*
 Despite both China and Russia continuing to deny any accusations of spreading false and defamatory information concerning COVID-19, the reality is quite different, and numerous foreign media outlets continue informing the international community about the disinformation campaigns launched in social media by Moscow and Beijing. The European External Actions Service believes that the aim of this disinformation is to undermine the EU and discredit its partners.
 The publication notes that since the beginning of January there have been over 150 cases of disinformation linked with the global health crisis. The spread narratives mainly concern discrediting the EU: the EU is about to collapse; there is disunity between EU members; the EU and NATO secretly plot to attack Russia and seize control over the country.
 According to disinformation analysis, over the course of the last couple of weeks China and Russia have deliberately attempted to undermine trust within the EU. Chinese and Russian troll farms, media outlets and propaganda mouthpieces continue actively spreading narratives that the 27 EU member states are unable and unwilling to fight the pandemic, stressing their negligence in eliminating the consequences of COVID-19 within the EU, Western Balkans, North America and elsewhere around the world. The propagandists at RT and Sputnik were particularly active to inform that the Kremlin is much better prepared to fight the COVID-19 crisis than Russia’s overly liberal and naïve Western counterparts. According to the Kremlin’s media outlets, the governments of some EU members have turned to Vladimir Putin for help and have praised the support provided by Moscow and Beijing.
 In third place was an article that argued that COVID-19 is Project ID2020 launched by an alliance of state and private actors, including UN agencies and the civic society. The article explains that it is an electronic ID program that employs general vaccination as a platform for digital identity. The main idea of the Kremlin’s propaganda writers is that people will get chips inserted in them when receiving the vaccine for the new coronavirus. This nonsense saw 2,733 shares, and there is no doubt that the devoted followers of MemoryWater’s washed-up luminary Jānis Pļaviņš are also among those spreading the Kremlin’s and Chinese propaganda.
 Five more articles on COVID-19 were shared on social media at least 1,000 times, for example:
 Although these numbers may not seem significant in a global context, we can only guess how many times people have shared these fever dreams. They could point to the number of people supporting the expressed ideas, i.e. it is the number of the Kremlin’s useful idiots who deliberately or due to the lack of common sense have shared the particular propaganda piece. However, the actual number of people who saw the article is much higher.
 There is no doubt the Chinese and the Kremlin’s disinformation is solely aimed at misleading the public, enticing minorities in Western nations to turn against democracy and discredit their governments and institutions, particularly health and law enforcement institutions and their employees. This creates fertile soil for the activities of individual pro-Kremlin officials and useful idiots that aim to further the geopolitical interests of Putin and Chinese communists, which, as we know, is to discredit the EU and its partners.
 In the video, von der Leyen can be heard saying “disinformation can cost lives”.
 *Baiba Zile is a freelance researcher and works for several think thanks in Latvia.
 SOURCES:
 Statement by Ursula von der Leyen, President of the European Commission, on disinformation during the coronavirus crisis (English version) https://audiovisual.ec.europa.eu/en/video/I-187719
 Russia’s top coronavirus ‘fake news’ stories https://euobserver.com/coronavirus/147905
 COVID-19: China and Russia Disinformation and Shenanigans
 Website 
   
 (function() {
     var cx = 'partner-pub-7107515782730935:6933006405';
     var gcse = document.createElement('script');
     gcse.type = 'text/javascript';
     gcse.async = true;
     gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
     var s = document.getElementsByTagName('script')[0];
     s.parentNode.insertBefore(gcse, s);
   })(); 
 The momentous process of raising the Shrine of ‘Abdu’l-Baha continues to advance, while work practices adhere to all public health measures required
  Did you enjoy this article? Then please consider donating today to ensure that Eurasia Review can continue to be able to provide similar content.
 Delivered by FeedBurner
 This website uses Google Analytics and Ezoic to collect anonymous information such as the number of visitors to the site, and the most popular pages.
 More information about our Privacy and Cookie Policy.
 https://www.eurasiareview.com/24042020-russian-and-chinese-misinformation-makes-useful-idiots-go-wild-oped/
