[OPINION] The coronavirus pandemic bares older Filipinos' vulnerability 
 Welcome to Rappler, a social news network where stories inspire community engagement and digitally fuelled actions for social change. Rappler comes from the root words "rap" (to discuss) + "ripple" (to make waves).
 Read more
 'If testing remains limited, many COVID-19-related deaths might be masked as common pneumonia, the second cause of death among older Filipinos'    
 “Kung makuha ko ang virus, at walang pang-pagamot ang pamilya ko, eh 'di patay ako,” shared an 81-year-old hypertensive woman I recently met at the market. 
 Despite being identified as the most vulnerable, and being told to not leave her home, she still sold vegetables to pay for her basic needs. “Kaysa naman nasa bahay lang, mamamatay ako sa gutom,” she reasoned.   
 As of March 30, 40% of the 1,546 confirmed COVID-19 cases in the Philippines are older persons, while 3 in 4 reported deaths are also from the same age group. 
 In 2015, there were 7.5 million older persons aged 60 and above in the Philippines, accounting for 7.5% of the total population. Of this number, 55% of this are older women. According to official poverty estimates, 16% of older Filipinos are poor, and a much larger proportion of older Filipinos live in a situation of economic insecurity just above the poverty line.  
 Official labor statistics suggest that almost half of the older population are still working in spite of health issues, mostly through low-income and informal work (such as street vending and tricycle driving).  They are not covered by the pension system since they did not have sufficient disposable income to save for a pension when they were younger. ˇThey also do not meet the strict eligibility criteria for the current social pension for the poorest. 
 With the lockdown, poor older Filipinos are potentially struggling to feed themselves and their family, even resorting to being buried in debt. This also imposes an extra challenge to older women who are mostly informal workers, with more limited pension entitlements and lower pension benefits than men.
 Based on the 2019 Longitudinal Study on Ageing and Health in the Philippines (LSAHP), 60% of older persons surveyed live with at least one child, and a significant proportion of them are dependent on their family for financial support. More mothers than fathers (70% vs 55%) report receiving monetary support from their co-resident children. However, many of their children are also working as daily wage earners who are most likely affected by the lockdown. In these circumstances, adult children will find it difficult to make both ends meet, much less provide for the medicines and other basic needs of their aging parents.  
 Other groups of older persons such as those who are homeless are in a more dreadful position in dealing with this outbreak. At particular risk of social isolation are older couples and older women living alone without any relatives in the community, and skip generation households (these are older persons living with their grandchildren while the parents are away). With reports claiming that domestic violence and neglect increase during quarantine periods, a vast number of at-risk older persons – particularly the disabled and care-dependent – will be imperiled. 
 For this year, about a third of the older population or 3.7 million indigents are targeted under the government’s social pension program, a P500 monthly subsidy to augment the daily needs of the poor. However, due to the weak administrative system even before this crisis, the program fails to distribute the cash assistance in a swift and compassionate manner, therefore failing to protect beneficiaries from falling further into poverty. In fact, around a quarter of the targeted beneficiaries have not received the social pension since last year.  
 Poor older persons also continue to shoulder the burden of health inequality. A better health status and health-seeking behavior are observed among the well-educated and more affluent older persons. Despite being universally covered by Philhealth and its guarantee to cover expenses related to COVID-19, reported out-of-pocket expenses remain high, which can prevent the poor from seeking medical care. In fact, 1 in 5 older persons have unmet healthcare needs because of financial constraints. Furthermore, if testing remains limited, many COVID-19-related deaths might be masked as common pneumonia, the second cause of death among older Filipinos. 
 The COVID-19 outbreak has not caused these underlying conditions, it simply exposes the vulnerable condition of many older women and men, and the fragility of the Philippines' social protection system.
 Older persons need more support than ever in this crisis situation. The government should consider these key action points in the short-term:
 Universal or near-universal basic income 
 Universal or near-universal basic income support should be guaranteed to keep older persons at home and prevent them from falling into poverty. This includes the release of a 6-month social pension and unconditional cash transfer for indigent senior citizens; advanced release of  Social Security System (SSS) pension; and ensuring their inclusion in the proposed emergency subsidy. Here are available guidelines on how local government units can disburse cash to reduce infection risks at pay points. Other in-kind support such as food and medicines should be sufficient during quarantine periods.
 Ensure access to health
 Information should raise awareness on their rights and on prevention of the disease. Communication should be appropriate for older persons with low literacy levels and physical and sensory disabilities. Mobilize young and healthy people for community-based programs for at-risk elderly. Philhealth should hasten the release of guidelines on the full coverage of COVID-19 treatments in virtue of the Republic Act 11469 of Bayanihan Heal as One Act of 2020. Ultimately, older person representatives or organizations should be consulted in the triage protocols to guarantee equal treatment of all.  
 After the current health crisis, the government should strongly consider the following reforms:
  – Rappler.com
 Aura Sevilla works primarily towards the realization of active and dignified ageing in the Philippines and in the region. Currently, she works as Advocacy Development Manager at the Tsao Foundation's International Longevity Center-Singapore, for the project "Promoting Financial Security of Older Women in Southeast Asia" or the Pro-Older Women Project. 
 These stories made other people 
 Password:
 Thank You.
 View your profile page
 here OR
 Fields with * are required.
 Password*:
 First Name:
 Last Name:
 Birthday:
 Gender:
  Fields with * are required. 
 Thank You.You have successfully updated your account.
 https://www.rappler.com/move-ph/ispeak/257248-opinion-coronavirus-pandemic-bares-older-filipinos-vulnerability
