Blog Recap: Coronavirus updates from around Wisconsin
 Updates on coronavirus and how it's affecting life in Wisconsin from reporters from the Milwaukee Journal Sentinel and the USA Today Network-Wisconsin.
 Updated Live Blog: Friday's coverage of coronavirus around Wisconsin
 Blog Recap: Wednesday's coronavirus updates
 Daily Digest: What you need to know about coronavirus in Wisconsin
 More Coverage: Coronavirus in the U.S. and around the world 
 The second person to die in Fond du Lac County from complications of the coronavirus was an IT contractor helping to install a computer system at St. Agnes Hospital, where he later died.
 His wife, who also tested positive after he fell ill, told medical investigators she believes they became infected at a birthday party in Washington, D.C.
 That's according to a Fond du Lac County medical examiner's report released Thursday to The Reporter in response to a public records request.
 Cesar S. Capule, 49, of the Washington, D.C., suburban community of Cheverly, Maryland, died March 29 after he was removed from life support, according to the document.
 Read more here.
 — Mark Treinen
 When soldiers were training at Camp Randall in 1861 to fight in the Civil War, the founding director of the Wisconsin Historical Society gave them journals to write down their thoughts.
 A century later when Freedom Riders, including many University of Wisconsin students, headed to Southern states to work in the civil rights movement, a group of UW grad students collected thousands of pages of contemporary documents that might have been lost.
 So it's no surprise that the Wisconsin Historical Society is now asking Wisconsinites to keep journals during the coronavirus pandemic to document history as it's happening.
 The society on Thursday launched its COVID-19 Journal Project seeking folks in all 72 counties to write down their experiences and thoughts either on actual paper, digitally or in video blogs.
 "By collecting these journals now it gives a voice to people today for people in the future who can look back at the lessons learned," said Wisconsin Historical Society Director and CEO Christian Øverland.
 The project will also be a gift to future historians who won't have to rely on the things most historians use to glean information — newspapers, records, indexes — but can use the actual voices of people living through the global pandemic.
 "This is the part of the history that disappears. Capturing it now is important," Øverland said in a phone interview Thursday afternoon.
 The Wisconsin Historical Society is asking participants to keep a 30-, 60- or 90-day journal in whatever format works best. Journalers should provide a summary of their day and over time document how COVID-19 affects their lives, their families and their perspective of the world.
 Øverland is hoping hundreds, if not thousands of Wisconsinites — even Cheeseheads living in other states during the epidemic — will participate because a large collection of journals will help historians in the future better understand what it's like living through this global pandemic.
 Besides, plenty of folks have a lot of time on their hands now.
 - Meg Jones
 Seven deaths and 99 cases of coronavirus have been reported in Ozaukee and Washington counties, according to an online dashboard launched April 2 by the Washington Ozaukee Public Health Department.
 The online dashboard identifies the number of coronavirus cases reported in each ZIP code. The map shows 23 cases in Mequon; 18 in Grafton; 21 in West Bend; and nine in Germantown.
 The dashboard, which can be found at www.washozwi.gov, also identifies eight facilities with coronavirus outbreaks.
 — Jeff Rumage
 A 60-year-old white woman and a 75-year-old black woman became Milwaukee County's latest victims of the coronavirus Thursday afternoon, according to an online dashboard maintained by the Milwaukee County Medical Examiner's Office.
 And previously unreported, the March 28 death of a 58-year-old black man is now linked to the virus.
 The cases bring Milwaukee County's total virus-related deaths to 22 and the statewide total to 41 as of Thursday evening.
 Milwaukee County reported 918 confirmed cases Thursday afternoon, with 722 in the city of Milwaukee.
 — Sophie Carson 
 Milwaukee-area officials are asking that the Army Corps of Engineers build an "alternative care facility" on the Wisconsin State Fair Park grounds in case it's needed for coronavirus patients.
 "This is an attempt to prepare in the event that we need this," Milwaukee Mayor Tom Barrett said during a call with media Thursday afternoon.
 Local officials are asking the state to ask the Army Corps to outfit an existing building on the State Fair Park grounds.  
 Barrett said local officials believe the state will move forward quickly on the request.
 This week, state and local officials announced two "isolation" facilities were opening in the county — a state-run facility at the Super 8 on South Howell Avenue by Milwaukee Mitchell International Airport and a locally run facility at Clare Hall on the grounds of the Saint Francis de Sales Seminary. 
 — Alison Dirr
 The Department of Workforce Development said that the Department of Administration has approved its request for 25 additional positions to help process the unprecedented number of claims for unemployment insurance caused by the COVID-19 pandemic.
 Last week, over 110,000 initial claims were received by the department. More than 98% of the people applying were able to file their claim online without needing to contact a claims specialist.
 Read about how to apply for unemployment benefits here.
 — Guy Boulton
 MADISON - A federal judge Thursday kept next week's presidential primary on track but allowed more time to count absentee ballots after excoriating Wisconsin officials for not doing more to protect voters during the coronavirus pandemic. 
 The ruling by U.S. District Judge William Conley will allow absentee ballots to be counted if they arrive by April 13 — six days after election day. He also gave people until Friday to request absentee ballots and loosened another voting rule as people turn to absentee voting in record numbers.
 But Conley did not go as far as Democrats and voter mobilization groups wanted and declined to postpone Tuesday's election. On the ballot is the presidential primary and elections for state Supreme Court and local offices around the state. 
 — Patrick Marley
 WAUKESHA - Inside an emergency room at ProHealth Waukesha Memorial Hospital, Alexis Garuz knew only one thing for certain: She could hardly breathe.
 It's the reason she had been rushed to the emergency room on Monday, presuming that medical staff would confirm what she strongly suspected — that she had COVID-19, by now a familiar designation of the coronavirus disease that has infected more than 1,500 people in Wisconsin alone.
 Instead, she was sent home untested.
 It's not that medical personnel didn't believe she had been infected. Medical screenings, first by phone and then through ER tests at the ProHealth Care hospital in Waukesha, left little doubt.
 What the physician assistant told her next left her more numb than the feelings caused by her affliction: "But I can’t test you even though I want to."
 Read the rest of the story here.
 — Jim Riccioli
 The coronavirus has been connected to a second death in Washington County on Wednesday after confirming its first death days earlier on March 30, according to data from the Washington Ozaukee Public Health Department.
 The victim was an 85-year-old woman who was living in a long-term care facility and was hospitalized with underlying health issues, according to the Health Department.
 Between Washington and Ozaukee counties the death toll is at seven.
 Nineteen Milwaukee County residents had died from the virus as of Thursday afternoon.
 — Ricardo Torres
 Confirmed COVID-19 infections in the state of Wisconsin increased by 180 on Thursday to 1,730.
 Thirty-eight Wisconsin residents had died by early Thursday afternoon, according to reports from state and county health departments and medical examiners.
 Twenty-seven percent of patients, or 461, had been hospitalized. More than 20,000 Wisconsinites have tested negative for the coronavirus.
 State officials have said that despite Gov. Tony Evers' measures to reduce the spread through social distancing, they expect confirmed cases and deaths to continue to increase for a while — possibly weeks — because of the lag between the time of infections and onset of symptoms and testing.
 Milwaukee Ald. Milele Coggs is calling for a meeting among city leaders to talk about the impact coronavirus is having on people of color in Milwaukee.
  “At this point in the pandemic, it is very clear that communities of color are being impacted at a greater rate in Milwaukee,” Coggs said. “The intent of this file is to bring together partners all working on this issue and have a conversation about what is being done, and what can be done, to assist communities of color. Together we can work to curb the impact of this virus and make sure those who need us most are getting the support they need.”
 Coggs noted that people of color, especially in African American neighborhoods in the city's central city and north side, have seen a larger concentration of coronavirus cases. Most of those who have died in Milwaukee County are African Americans.
 “There are a variety of factors that are attributing to communities of color seeing a higher density of positive cases,” Coggs said.. “We need to be aware that there are many items that impact public health and work to address all of them.”
 — Mary Spicuzza
 A 92-year-old African American Milwaukee man became the 19th person in Milwaukee County to die from the coronavirus Thursday.
 The man was pronounced dead Thursday morning. It is the 12th fatal case of COVID-19 among black residents of Milwaukee County.
 — Elliot Hughes
 Milwaukee finds itself in scramble mode to accommodate the logistics of a new date for the Democratic National Convention, pushed back to the week of Aug. 17. Read the full story.
 — Bill Glauber
 More than 70,800 new unemployment claims have been filed in Wisconsin so far this week, according to the state's Department of Workforce Development. 
 After a Monday high at more than 24,600 new claims, 18,480 were made Tuesday and another 15,552 were filed Wednesday.
 More than 255,800 new unemployment claims have been filed in Wisconsin since March 15, according to preliminary daily totals from the state's Department of Workforce Development. 
 New unemployment claims topped 110,700 in the week ending March 28, according to data released by the Department of Labor Thursday. 
 — Sarah Hauer
 Roundy’s Supermarkets, Inc., says it has expanded its options for no-contact grocery shopping.
 Milwaukee-based Roundy’s operates 106 Pick ‘n Save and Metro Market stores in Wisconsin.
 “At Pick ‘n Save and Metro Market, we have instituted several safety protocols in our stores during the ongoing COVID-19 pandemic,” said James Hyland, vice president of communications and public affairs for Roundy’s, in a statement.  
 The company has also instituted a number of contact-free checkout options for customers, Hyland said.
 Among those options are:
 •Online ordering, then pickup. “Pickup is a contact-free checkout service as payment information is provided at the time of the order and customers never have to leave their vehicle as our store associates will load their groceries directly into their vehicle,” Hyland said.
 •Online Ordering for Delivery: “When ordering for delivery online, you have the option to check a box indicating you wish the delivery service to leave your groceries at your doorstep ensuring a contact free transaction,” Hyland said.
 •In-Store Shopping Using Kroger Pay: Download the Pick ‘n Save or Metro Market App from the App Store (iOS) or Google Play (Android), go to the menu and select Kroger Pay. Once you’ve set up your Kroger Pay wallet, you can use in-store for completely contact free checkout.
 Roundy’s and the Pick ‘n Save and Metro Market banners are divisions of Cincinnati-based Kroger.
 — Joe Taschler
 Either Wisconsin Center, downtown Milwaukee's convention center, or State Fair Park could each accommodate up to 1,200 beds for coronavirus patients who have been discharged from Milwaukee-area hospitals but are not yet able to go home.
 That's according to Marty Brooks, president and chief executive officer at the Wisconsin Center District, the state-created agency which operates the convention facility.
 He said the alternative care site would not be a hospital, but would provide post-hospital care for people recovering from the virus. It would be operated by the Army Corps of Engineers.
 The corps is recommending operate the site at either site, Brooks said, with a decision likely within the next 12 to 24 hours.
 — Tom Daykin
 Seven of Wisconsin’s largest farm groups have asked the U.S. Department of Agriculture to use its extensive purchasing power to buy large amounts of dry milk, butter and cheese that normally would be going to restaurants and the food-service industry.
 Wisconsin Cheese Makers Association, Edge Dairy Farmer Cooperative, Dairy Business Association, Cooperative Network, Wisconsin Farm Bureau Federation, Professional Dairy Producers of Wisconsin, and Wisconsin Farmers Union say the current circumstances, far beyond their control, are beginning to result in farms having no place to accept their milk.
 In fact, this week some large Wisconsin dairy operations have begun dumping milk because there is no buyer for it.
 “With 80 percent of Americans under order to shelter in their homes, hundreds of thousands of restaurants, schools and other food service outlets have closed or significantly reduced offerings, which means cheese and butter manufacturers have lost their largest market. While retail sales have increased in past weeks, they are now leveling, and orders are slowing. Dairy manufacturers and processors also have seen their export markets decimated,” the Wisconsin farm groups said in their letter to U.S. Agriculture Secretary Sonny Perdue.
 “Commodity dairy prices have plummeted and will result in milk prices lower than many farms can handle to sustain long-term viability,” the letter said.
 The federal Coronavirus Aid, Relief and Economic Security Act directed $9.5 billion to a dedicated disaster relief fund for agriculture, $25 billion for the Supplemental Nutrition Assistance Program, $15 billion to the Commodity Credit Corp. Program and $450 million to support food banks.
 The Wisconsin farm groups are asking the USDA to use that money to provide direct relief to dairy farmers and to purchase dairy commodities in bulk and in formats that normally would be used by restaurants and food-service businesses.
 The groups are also asking the USDA to look for ways to “make farmers whole” for milk that’s produced but has to be dumped, or for which they’ve received drastically reduced payments.
 — Rick Barrett
 A 300,000 square foot automotive parts factory near Madison is being retooled to manufacture hospital beds in response to the coronavirus pandemic.
 Kruger Family Industries, of Portage, says two of its companies are partnering to produce more than 3,000 beds a day that can be assembled on-site without tools in just a few minutes.
 “Our engineers have completed the design of an emergency and disaster relief bed that can be used by health care facilities across the United States and around the world. Right now, our team is working around the clock to ensure our facility is ready to begin manufacturing as soon as possible,” company President David Kruger said in a written statement.
 There is a severe and immediate need to address the upcoming hospital bed shortage related to COVID-19, according to the company that says two of its divisions, Penda and TriEnda, will produce the beds.
 Milwaukee County reported another 44 cases of COVID-19 Thursday morning, bringing the area’s overall total to 877.
 Thirty-six of those new cases were in the city of Milwaukee, totaling 692 cases.
 The countywide death toll moved to 18 after eight people died from the virus Wednesday, including a 73-year-old African American man who died shortly before 4:30 p.m. and an 80-year-old African American woman died shortly after 6:45 p.m.
 West Allis (31), Wauwatosa (25) and Oak Creek (21) have the most confirmed cases of COVID-19 among Milwaukee County suburbs.
 — Elliot Hughes and Mary Spicuzza
 The Wisconsin National Guard's top leader dispelled rumors almost two weeks ago that the military will be used to enforce coronavirus quarantines or the state will implement martial law.
 But the rumors persist and Guard leaders on Wednesday again tried to knock down false notions that the National Guard will force people to stay in their homes to follow Gov. Tony Evers' "safer at home" order.
 Among the rumors were from people reporting seeing large trainloads of military vehicles in Wisconsin to enforce quarantines.
 "These rumors are patently false," Wisconsin National Guard spokesman Capt. Joe Trovato said in a statement.
 With large manufacturers in the state like Oshkosh Corp., which makes military vehicles, it's not unusual to see humvees and Army trucks on rail cars or traveling on highways to train at military installations in Wisconsin.
 National Guard leaders have not discussed enforcing statewide quarantines or shutting down highways.
 "Unfortunately, there are nefarious actors — in some cases, from other countries — operating in the shadows and intentionally circulating false information and misinformation via social media," Trovato said.
 "We understand that these rumors are part of a concerted disinformation campaign aimed at creating hysteria and panic in our nation's communities."
 On March 20 Mark Belling, a Milwaukee-area conservative talk radio show host, falsely told listeners that Gov. Tony Evers was issuing a shelter in place order and that the "Wisconsin National Guard has been deployed and is basing at State Fair Park to enforce the order."
 After Belling's remarks, Maj. Gen. Paul Knapp spoke to the Milwaukee Journal Sentinel on the same day to criticize the falsehoods. Belling later corrected his statement.
 "There are people out there trying to instill a sense of hysteria across the United States because it's to their advantage," Knapp said on March 20. "It's really important for us to make perfectly clear to the people in Wisconsin that the National Guard has in no way shape or form been tasked or asked to enforce any type of shelter-in-place (order)."
 Wisconsin National Guard troops have been used a variety of ways in the coronavirus outbreak response:
 Half a dozen medics spent three days working with staff at a Grafton nursing home where numerous cases of COVID-19, and one death, were reported.
 Guardsmen will serve as poll workers at election sites around the state on Tuesday for the presidential primary and spring election because of an extreme shortage of workers. The number of soldiers and airmen who will be used for this task is not yet determined. Guardsmen will serve as poll workers in their home counties, in accordance with state law.
 A small liaison team of guardsmen is working with the Wisconsin Elections Commission to offer advice, project management expertise and logistics support.
 Guard members are distributing sanitary supplies to election sites including hand sanitizer, wipes and spray bottles.
 Soldiers and airmen are helping with warehouse operations to receive and repackage personal protection equipment such as gloves and masks from the federal stockpile to send to health care and first responder sites around the state.
 30 troops drove Wisconsin residents who were quarantined on a cruise ship in California from Volk Field to their homes.
 — Meg Jones
 https://www.jsonline.com/story/news/2020/04/02/coronavirus-wisconsin-latest-updates-cases-cancellations/5110491002/
