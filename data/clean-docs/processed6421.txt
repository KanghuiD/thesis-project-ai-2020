| Haringey Council
    Coronavirus (COVID-19) – latest news and updates: everyone must stay at home to help stop the spread of coronavirus. Haringey Together – see how we are responding and how you can help.
 Sign in
 We are sorry but the page you were looking for could not be found.
 This might be because:
 You may be able to find what you are looking for by:
 Visit My Haringey to get information about your property and local area including council tax bands, parking zones, planning applications and much more.
 For non-urgent queries you can use our online contact form.
 Help us make sure we’re achieving our aims and tell us what you think.
 Tell us what you're interested in and receive updates direct to your inbox.
 Get the latest important announcements as they happen.
 Be part of the Haringey community online as well as off.
 View images of happenings and events from our vibrant borough.
 See and hear about Haringey issues and news on our very own YouTube channel.
 https://www.haringey.gov.uk/news-and-events/haringey-coronavirus-covid-19-updates
