Spotlight: World leaders, experts warn against discrimination caused by coronavirus epidemic - Xinhua | English.news.cn
 BEIJING, Feb. 6 (Xinhua) -- World leaders and experts have called on the international community to discard discrimination against certain groups of people due to misinformation about the novel coronavirus epidemic.
 Ever since the outbreak of the novel coronavirus that started first in China's central city of Wuhan, there have been unpleasant cases of discrimination and violence against the Chinese people being reported. Some biased media outlets even tagged the novel coronavirus as "China virus."
 Having noticed the situation, United Nations (UN) Secretary-General Antonio Guterres said: "it's easy to move into ... perspectives in which there tends to be discrimination; there tends to be violation of human rights; there tends to be stigma on innocent people just because of their ethnicity."
 The chief of the world's largest international organization also appealed for "a strong feeling of international solidarity, a strong feeling of support to China in these difficult circumstances but also to other countries that might be impacted," and "a strong concern to avoid the stigmatization of people that are innocent and that might be victims of that situation."
 GLOBAL APPEAL FOR NON-DISCRIMINATION
 Spanish Prime Minister Pedro Sanchez has met leaders of the Chinese community in Spain to offer them support in the ongoing coronavirus crisis.
 Sanchez said he lamented that fears of the virus have led to some Chinese people being victims of xenophobic incidents and caused difficulties to the Chinese community.
 He made a call for understanding and made a positive valuation of the efforts that China is making to control the spread of the virus.
 Discrimination against the Chinese because of the novel coronavirus outbreak will not be tolerated, said a U.S. local official in Houston, Texas.
 Harris County Judge Lina Hidalgo made the remarks at an Asian Chamber of Commerce event hosted at the Chinese Community Center.
 "Asian American community members, especially Chinese community members feel that they are being discriminated against. That is the ugly side of diversity. It's not to be tolerated in this community," she said.
 "Diversity is our strength," Hidalgo said. "We thrive because of the people that live here. We should not be discriminating ever."
 As the hashtag went viral, a young woman who was mocked on a bus in Paris and declined to give her name, said "the worst virus is systemic racism ... We know very well that a virus has no nationality!"
 "I was the victim of a racist act by a group of young girls. I could hear them laughing and making fun of me. One said: How do we call Coronavirus patients? Chinese people, right?" she said.
 "A person of Chinese origin is not a person infected with the coronavirus. We are not a virus," the Association of Young Chinese in France said.
 STEMMING MISINFORMATION
 Misunderstandings and fake news about the novel coronavirus are the major reasons that have led to the possible discrimination and violence.
 "The situation is absolutely under control, so no one in Italy should think of taking advantage of this situation for displays of discrimination or even violence, which we absolutely cannot accept," said Conte.
 Educating the public globally is key, including the sharing of practical, preventive measures through schools, teachers, children and parents, said Yasmine Sherif, director of Education Cannot Wait, a global fund hosted by the UN Children's Fund.
 "It is important that we all work together to share accurate information about the coronavirus to help contain the outbreak," said Sherif.
 The World Health Organization (WHO) has commissioned a rapid response team from an Austrian university to counter conspiracy theories and misinformation in fighting the novel coronavirus, Austria Press Agency has reported.
 http://www.xinhuanet.com/english/2020-02/06/c_138761294.htm
