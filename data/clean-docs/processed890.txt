	Coronavirus (COVID-19) - The University of Nottingham
 The University is closely monitoring the ongoing coronavirus situation. You should use these webpages as your main source of information about our response.
 Online teaching for all students – guidance and support
 Summer exams and assessments moved online – marks will not be negatively affected
 Staff must work from home wherever possible – advice on homeworking and wellbeing
 Staff who are unable to work may be considered for furloughing, under the government's job retention scheme
 NHS (national health service): symptoms of the virus and what to do
 UK government: how to protect yourself / foreign travel advice
 University of Nottingham: student information / staff information / look after your mental health / staff wellbeing / sports support at home
 The University is following advice from UK government and the NHS – as well as from the Chinese health authorities for our Ningbo campus. Our priority is the health and safety of our community in the UK, China, Malaysia and wider world.
 People are what makes this University great, now more than ever. Our staff and students are supporting the local, national and global effort to tackle the pandemic.
 This includes:
 Our response
 Join the effort: find out about secondments and volunteering for staff and students.
   
  Connect with the University of Nottingham through social media and our blogs. 
          
  Campus maps | More contact information | Jobs 
 Browser does not support script.
 https://www.nottingham.ac.uk/coronavirus/
