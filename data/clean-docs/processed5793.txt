U.S. now leads world in deaths, passes 20,000 mark 
 This live coverage has ended. Continue reading Apr. 12 Coronavirus news.
 The U.S. has now passed the 20,000 mark in the number of coronavirus deaths and leads the world in this grim tally, surpassing Italy for the first time.
 The virus has killed 20,029 people in the United States, just above the number in Italy, according to NBC News' figures.
 Worldwide, the death toll is more than 107,000, and the number of confirmed cases has surpassed 1.7 million, according to Johns Hopkins University.
 Dr. Anthony Fauci, the top infectious disease expert in the U.S., has warned that it is too early to relax coronavirus restrictions.
 "Now is not time to back off," Fauci said Friday,
 Meanwhile, current and former U.S. officials have told NBC News that American spy agencies collected raw intelligence hinting at a public health crisis in Wuhan, China, in November, but the information was not understood as the first warning signs of an impending global pandemic.
 Download the NBC News app for latest updates on the coronavirus outbreak.
 The Associated Press
 President Donald Trump’s Virginia vineyard could be eligible for a federal bailout under the $2.2 trillion coronavirus stimulus he signed into law last month, despite provisions in the bill that Democrats said were intended to prevent him and his family from personally benefiting.
 Deep in the fine print of the law passed by Congress to try to arrest an economic free fall is language that would the make the vineyard eligible for aid extended to growers and producers of “specialty crops,” among them grapes used to make wine.
 Alicia Victoria Lozano
 Walt Disney Co. plans to furlough 43,000 workers at its Disney World resort in Orlando, Florida, as coronavirus forces theme parks around the country to close indefinitely.
 Employees will keep their current benefits for up to one year and will be eligible to apply for unemployment immediately, according to an agreement reached with the Service Trades Council, the coalition of unions representing the Disney World workers.
 About 200 essential employees will continue to work during the closure, and they will be offered positions based on seniority. All employees will be able to return to their jobs once businesses can reopen.
 Earlier this month, Disney announced plans to furlough non-union workers starting April 19.
 David K. Li
 At least 300 more New Yorkers have died from complications brought on COVID-19, the city's health department reported Saturday.
 The death toll reached at least 5,742 by 5 p.m., up from 5,429 a day earlier, according to the Department of Health and Mental Hygiene.
 The 313-fatality increase capped a heartbreaking week in New York City, where the death toll spiked by more than 500 on four separate nightly reports.
 A Florida emergency room physician temporarily lost custody of her daughter over concerns she poses a health risk to the 4-year-old child. 
 Dr. Theresa Greene tested negative for coronavirus, but her ex-husband worried her job could endanger their daughter. The little girl splits her time equally between both parents, NBC Miami reported.
 Greene is entitled to "equivalent make up timesharing" for every day of custody lost as a result of the temporary custody suspension, according to court documents. She is also entitled to daily phone calls or video chats with her daughter.
 In his court order, Miami-Dade County Circuit Judge Bernard Shapiro said his ruling was intended to "protect the best interests" of the child and is solely based on concerns over the coronavirus pandemic.
 Greene told NBC Miami the ruling was devastating and also shocking because the judge did not consult with medical experts.
 "I feel like the family court system now is stressing me almost more than the virus," Greene said.
 A hair dresser trying to make her rent works at home after the salon she rents space from had to close its doors as a non-essential business.
 See more photos from photojournalist Todd Bigelow's portrait of a neighborhood lockdown. 
 Pete Williams
 Planned Parenthood on Saturday asked the U.S. Supreme Court to step into the Texas legal battle over whether abortions should be accessible during the coronavirus pandemic.
 A federal judge in Texas has twice ruled that the order restricts the constitutional right to abortion access, and the 5th Circuit Court of Appeals has twice ruled that he got it wrong. In going to the Supreme Court, Planned Parenthood said Abbott's order means virtually all women in the state with unplanned pregnancies have no access to abortion, even in pill form. 
 "Some will engage in risky, out-of-state travel," the group said, "this increasing contagion risks in the midst of a pandemic." The court will likely ask Texas for a response before acting on the request.
 At least 550 crew members aboard the USS Theodore Roosevelt have tested positive for COVID-19 more than a week after its captain, Brett Crozier, was relieved of duty for sounding the alarm about an outbreak on the ship. 
 The Navy said 92 percent of crew members have been tested for COVID-19. More than 3,600 tested negative. The ship had 416 cases two days ago. 
 A crew member who had contracted coronavirus was found unresponsive Thursday in the room where they were quarantined. That person is now hospitalized in an intensive care unit. 
 Dan Good
 The number of deaths from COVID-19 in the United States passed the 20,000 mark on Saturday, with over a half million confirmed cases of the coronavirus.
 As of late afternoon ET, the disease had killed 20,029 people in the country, according to NBC News' tally.
 More than half of the deaths were concentrated in three states: New York, with 8,627; New Jersey had 2,183;  and Michigan, with 1,392..
 Earlier on Saturday, the death toll in the U.S. became the highest in the world, surpassing that of Italy.
 Nicole Acevedo 
 Ricardo Román woke up on Wednesday morning asking "God to give me the strength necessary to be able to see my father for the last time." That afternoon he attended his father's funeral.
 Ramón Román, 52, died Sunday of complications from COVID-19 at a hospital in Brooklyn. For 10 years, he worked as an auxiliary police officer for the New York City Police Department.
 The coronavirus outbreak is hitting Hispanics in the city harder than any other racial or ethnic group. Latinos account for 34 percent of all coronavirus deaths in New York City, while making up 29 percent of the city's population, according to officials. The preliminary death rate for Hispanics in the city is about 22 people per 100,000 compared to 10 per 100,000 for white residents.
 Read the full story here.
 While many people around the country are applauding health care workers during the pandemic, some employees of a hospital in Westchester County, New York didn't feel the love Friday morning.
 Staff at New York-Presbyterian Hudson Valley Hospital in Cortlandt completed an overnight shift Friday morning to find their car tires had been slashed, authorities said.
 A 29-year-old man has since been arrested for cutting the tires of 22 vehicles in the hospital's parking lot.
 https://www.nbcnews.com/health/health-news/live-blog/coronavirus-live-updates-fauci-warns-it-too-early-ease-restrictions-n1181761/ncrd1181976
