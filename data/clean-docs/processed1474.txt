Arts & Letters Daily - ideas, criticism, debate
 Anne Case and Angus Deaton first recognized “deaths of despair.” As economic hardships swell, their work may be more relevant than ever... more »
 When his "theory of the firm" went bust, the economist Michael Jensen needed a scapegoat. He found one in basic human irrationality... more »
 Previous epidemics might have prepared us for Covid-19 — if only their histories were better remembered and their victims duly honored. We could have been more like Venice, a city defined by disease... more »
 So what does Kierkegaard have to tell our age? A lot. First, we should stop thinking of ourselves as occupying an age at all... more »
 Rebecca Solnit’s writing hits broad feminist notes, avoiding anything difficult or controversial. No one disagrees with her — which is a problem... more »
 Being friends with Philip Roth. He possessed the terrible gift of intimacy, causing people to tell him things they told no one else... more »
 Antonin Dvorak scavenged sounds from birds, swishing grass, stamping cows, shouts, and cries. In Iowa the music poured out of him... more »
 Thoreau showed how the telegraph could alter our sense of time and space. Howard Axelrod shows us what that alteration now looks like... more »
 "Cocaine for the masses." The growth of a coffee culture has been trailed, and sometimes advanced, by a coffee literature... more »
 The notion that the brain is like a computer has led neuroscience down a false path. It’s drowning in data and short on theoretical insight... more »
 Why do women read more than men? The answer may have more to do with masculinity than with femininity... more »
 Withdrawing from the public leads some artists, like Thomas Pynchon, to fame — and others, like Lee Bontecou, to obscurity... more »
 Death is scary. Arbitrary death is terrifying. The pandemic has Joseph Epstein considering the distinction... more »
 "Reading Mozart’s correspondence is like being tugged by an enthusiastic, garrulous friend right into the green room of 18th-century European culture"... more »
 Oxford University Press is a leading publisher. Why has it allowed the pseudo-scientific distortion of Shakespeare's canon?... more »
 What’s the best scholarly book of the past decade? Amia Srinivasan, Merve Emre, David Halperin, and others make their picks... more »
 In the years after his death, Andy Warhol’s art was seen skeptically. Now the cultural fog has cleared, and we can see him for what he was: an American Picasso... more »
 The Plague is Camus’s tale of illness and quarantine, yes, but its moral is one of inoculation through shared consciousness... more »
 Who would steal ancient papyrus from Oxford’s collection? An associate professor in papyrology has been detained by the police... more »
 Richard Hofstadter's view of populism is more influential than ever. But as with many intellectual legacies, much has been lost in translation... more »
 Sartre’s Stalinism. His relationship with the grislier aspects of the Soviet regime aren’t as black and white as his critics make out... more »
 Feminist manifestoes may seem worlds apart, yet they illuminate one another, all the way from Sojourner Truth to the SCUM Manifesto to “Occupy Menstruation”... more »
 As Frank Kermode held, the apocalypse is a fiction that lends us a “sense of an ending.” But what happens when cataclysms actually approach?... more »
 In the pantheon of Western critics of Communism, George Orwell and Raymond Aron have privileged perches. Rebecca West deserves a spot alongside them... more »
 Academe has enshrined Foucault and Fanon, but it keeps their most provocative and radical ideas at arm's length... more »
 How do campus leaders rectify racism? They ask black students to identify and fix problems, turning them into colleges' greatest unpaid consultants... more »
 The Naipaul conundrum. What makes him uniquely insightful is intimately connected to what makes him so problematic... more »
 We are so used to sharing art that we neglect a fundamental truth: Aesthetic experience had depths that cannot be communicated... more »
 If P.T. Barnum embodied the best of America, he also embodied the worst: casual racism, callousness toward women, indifference toward animals... more »
 “Liberal” is a slippery term that can mean almost anything. It’s also a crucial political weapon, writes Michael Walzer... more »
 Covidiot, Miss Rona, Geisterspiel, Hamsteren, Quatorzaine, Untore: Do you speak corona? A guide to pandemic parlance... more »
 Trivializing Alexander Calder was all too easy, as if he were a naïf who stumbled onto success. That undersells his ambition and his intellectual rigor... more »
 A global catastrophe has forced a huge, literate, internet-savvy population indoors. This is a test for humanists. Why are so many failing?... more »
 Amazon’s self-publishing arm was set up to democratize publishing but instead has gone toxic. No idea is too inflammatory, no author off-limits... more »
 Lord Byron once compared his family to "whole woods of withered pines." He wasn't exaggerating. Excess, incest, and misery flowed in those bloodstreams... more »
 A bad book gets a second chance. Vivian Gornick’s history of American communism was praised by none and vilified by all. Now, decades later, it’s caught on... more »
 Why do some intellectuals’ reputations endure, while others end up nearly forgotten? Consider the case of G.E. Moore... more »
 Simone de Beauvoir was often cast — to her chagrin — as a feminist heroine. Do the many biographies revealing her dark side change that?... more »
 What pulses through the literature of pandemics is the body itself. "The world both narrows and broadens into the body’s suffering”... more »
 Can social distancing lead to divine inspiration? Consider Saint Simeon Stylites, who spent 36 years atop a ruined column in the Syrian desert... more »
 Frail, regicidal, strangely set on conquering the Caribbean, Oliver Cromwell was just the leader England needed... more »
 Was a Ukrainian town overrun by wolves after the arrival of Soviets troops? Paul Auster investigates, with poetic assistance Reply Forward... more »
 We read Walden as an ode to nature or a paean to self-righteousness. But it’s really about grief and social distance... more »
 Einstein fled the Nazis in 1933, going to Norfolk, England. There he lived in a thatched hut, protected by locals with shotguns... more »
 Depressed and impoverished scholars 100 years ago asked a question that remains with us: Is intellectual work still possible as a vocation... more »
 Fear of geopolitical decline and of cultural decadence often go hand-in-hand, a point echoed by conservative choirs of the 1750s and our own time... more »
 Political earnestness can be tedious in an essayist. But in Olivia Laing, it's leavened by empathy and an omnivorous curiosity... more »
 Peter Schjeldahl predicts what we'll experience when can once again wander museums: Everything in them will be other than what we remember... more »
 Covid-19 is a disease of the social as well as physical body. Our challenge is to invent, rather than nostalgically restore, a society in which we are in touch without touching... more »
 Bearded like a deep-sea fisherman, baggy-eyed, high on drugs or alcohol, the novelist Robert Stone cut a curious figure at catered cocktail parties... more »
 An artist's gifts are meant for giving. That's always been a dilemma for gifted people. In the digital age, it's a crisis... more »
 Evgeny Morozov made his name as a scourge of megalomaniacal technologists. Now he’s readying a new way to spread information online... more »
 “Cosmopolitanism” is often meant as a dig at the Davos set of footloose capitalists. But the term contains rich philosophical potential, says Martha Nussbaum... more »
 The art world has survived outlaw days and obscene money, keeping its culture intact. But can it survive the coronavirus?... more »
 When magazines were ambitious. In the 1890s, they offered designs for homes, college degrees, even a model town. Occasionally those ventures worked... more »
 Darwin wrote more than a dozen books. Some of them are easily ignored. Some are fun and charming. Some grind through important stuff. One of them flows briskly and changed the world... more »
 What to read in quarantine? Colette, Hilary Mantel, and classic, 700 page academic tomes. It’s a good time for slow reading... more »
 As the bubonic plague ravaged Florence, Boccaccio observed the putrid crisis at ground level. What did social distancing look like in 1347?... more »
 Wordsworth's credentials as a sage and prophet of nature were unimpeachable. Then he suffered "the longest, dullest decline in literary history"... more »
 The coronavirus marks the end of an era defined by liberalism and rising living standards. What will shape the new world order?... more »
 The Russian battle against death began in the second half of the 19th century. It continues to this day... more »
 Whoever has the most galleries when he dies, wins. Such is the consensus among art collectors like the particularly ruthless Larry Gagosian... more »
 The case for Don DeLillo. By every metric of literary greatness, he is in the highest percentile. Give him the Nobel Prize. This year... more »
 The genius of Heinrich von Kleist — writer, newspaperman, tragic figure — was in using plain, reportorial language toward grotesque ends... more »
 Influence is difficult to measure, but however you calculate it, Encounter had it. How a magazine shaped the intellectual climate... more »
 A sensational Parisian scandal in 1386 escalated into France’s last trial by combat. Was it all just a case of mistaken identity?... more »
 Zach Baron is a profiler, involving a strange and ephemeral genre of magazine journalism. He spends time with someone and writes about the experience. Now he can't leave the house... more »
 Orwell and the left. Would this foremost cant-detector find a whiff of it on the contemporary left? He would cast a baleful eye on identity politics and be dubious about diversity... more »
 From the Sator Square to anti-riddles, word games have a long and delightful history. Crosswords weren’t invented until 1913... more »
 “In all the creative occupations, there’s no stability unless you’re a superstar of some sort.” Barbara Ehrenreich reckons with success... more »
 There’s a lot we can numb ourselves to in order to survive, says Meghan O'Rourke. But can we ignore our inability to come together to mourn the dead?... more »
 In the 1790s, muslin gowns, hoops earrings, and madras were the height of European fashion. They were all shamelessly appropriated from the West Indies... more »
 Crossword editors are strange arbiters of cultural relevance. But it really matters what reaches the grid — and what's kept out... more »
 Inigo Philbrick is the art world’s mini-Madoff. A young and charming dealer who dabbled in drugs, prostitution, and endless scams... more »
 Judith Shklar, critic of liberal triumphalism, was no system builder. But her thought resonates in our disenchanted, skeptical world... more »
 As Furtwängler and Shostakovich showed, classical music has the ability to bind a nation. Why hasn’t that happened in America?... more »
 War, corruption, oppression, and misogyny were facts of life in Renaissance Italy. Are the period’s great artistic achievements complicit in those brutalities?... more »
 Why do American cemeteries look the way they do? The answer lies in a 19th-century passion for rural, rustic design... more »
 From The Decameron to Station Eleven, there has always been a literature of pandemic because there have always been pandemics... more »
 When Dora Maar, Picasso’s lover, suffered a jealousy-fueled breakdown, he blamed Surrealism and “that whining, weepy phony, Jean-Jacques Rousseau”... more »
 A medical mystery to unravel: Jordan Peterson was in a drug-induced coma in Russia. When he awoke, he was unable to speak or write... more »
 In 1967, William Styron, a white novelist, fictively entered the mind of a black man. What would happen if The Confessions of Nat Turner appeared today?... more »
 Stylish novelists like Ottessa Moshfegh and Ling Ma give us curiously passive, apathetic characters. What’s behind this tedious tendency?... more »
 William Blake's flint cottage, Rudyard Kipling's stone manor: What role do houses play in the lives of creative people?... more »
 If you elide crucial historical context, it’s easy to make religious notions of the afterlife look like freakish curiosities. A recent book does just that... more »
 No modernism without lesbians. More than a sexual preference, lesbianism created the conditions for an artistic revolution. Or so claims Diana Souhami... more »
 In his letters, Robert Frost is never tedious, always fun, appallingly crafty, and unusually trusting in people he barely knew... more »
 Freud was a social philosopher, using a scientific guise to lend his ideas more authority. Or so argues a new book... more »
 During the 16 months Einstein spent in Bohemia, he did nothing much. But the banality of his experience there is itself worth consideration... more »
 Could it be that loneliness was invented around 1800? The separation of self and society has deeper roots, as Hamlet or Othello might testify... more »
 “Advertising was like art, and more and more art was like advertising. … Ideally, the only difference would be the logo.” So it went at the Warhol Factory... more »
 George Gershwin attacked a “crude, vulgar, and unadorned” version of jazz. But were his experiments in the genre just cultural appropriation?... more »
 Lucian Freud’s art was an obsessive quest to exert maniacal control over contour, color, and light. He was a King Lear of the brush... more »
 Tarred as elitist and conservative for its New Critical roots, formalism is making a comeback. Will it stick this time?... more »
 Before the 1600s, the workplace drug most commonly consumed in the West was beer. Then capitalism brought a craze for something new: coffee... more »
 Shrinking news coverage, online consumer reviews, the withering of literature in public esteem: an audit of book reviewing in uncertain times... more »
 Explainer journalism never lets actual events get in the way of big ideas. Exhibit A: Ezra Klein’s new book... more »
 Writing about music is a difficult thing, saying in words that which words cannot say. Philip Kennicott gets it exactly right... more »
 How was America transformed from a “rabidly anti-theatrical” society to one that, by 1833, viewed Shakespeare as its great national author?... more »
 The battle between Noah Webster and Joseph E. Worcester still matters because it was over the words Americans use to understand themselves... more »
 Previous epidemics might have prepared us for Covid-19 — if only their histories were better remembered and their victims duly honored. We could have been more like Venice, a city defined by disease... more » 
 Being friends with Philip Roth. He possessed the terrible gift of intimacy, causing people to tell him things they told no one else... more » 
 "You can live alone without being lonely, and you can be lonely without living alone," says Jill Lepore, "but the two are closely tied together, which makes lockdowns, sheltering in place, that much harder to bear"... more »
 Two years ago, Joseph Epstein set out to learn Latin. As his dream of mastery has given way to reality, what remains is his love for the language... more »
 Auden was filthy, strewing old food, cigarettes, and martini glasses everywhere. Such squalor, he protested, was necessary for a man of letters... more »
 A pandemic is a time for rethinking everything, for changing one’s mind, for putting down books by fashionable theorists... more »
 The literature of contagion is vile, says Jill Lepore. "Every story of epidemic is a story of illiteracy, language made powerless, man made brute... more »
 The critical theorist as coronavirus denier. Giorgio Agamben uses outdated jargon to make once-fashionable arguments that now seem absurd. Who's listening?... more »
 In praise of marginalia. "Like having a child or planting a garden, annotating a book is an expression of hope for the future"... more »
 Any critic who wants to write something lasting must do so in part from aggression. Few have been more successful than T.S. Eliot... more »
 Society, Durkheim held, was a social fact irreducible to psychology or economics. In a time of pandemic, we’re all Durkheimians... more »
 When Alex Perez praises Philip Roth and Henry Miller, people react as if to an atrocity. And yet he can’t get enough of these problematic white dudes... more »
 What does a writer do when his words stop working? Wait for the muse to recharge or dwell in the struggle to overcome your own silence... more »
 “Be of good cheer,” advises Geoff Dyer, reflecting on the pandemic. But inwardly he’s clutching his head like Munch’s screamer... more »
 In literature, a disease is not just a disease. It is a return of the repressed, a manifestation of an eternal crisis... more »
 Much of the poetry we love most is wistful and regret-filled. Why? The all-too human hankering for things to be other than they are. Michael Dirda explains... more »
 Through Umberto Eco’s eyes, the library functions not to enlighten, but to disorient — to overwhelm your self-confidence... more »
 In 1948, Camus discovered Simone Weil’s manuscripts. They revolutionized not only his sense of suffering but his sense of self... more »
 George Steiner, an unrepentant elitist, was a moral force for high art. Among his virtues was a hunger to be more serious... more »
 Long gone are the days of Dickens and Kafka — the novel has been brought low. Are its days numbered?... more »
 George Scialabba, who has spent a lifetime suffering from depression and reading about others', has some advice... more »
 The University of Chicago is renowned for its commitment to open debate and rigorous inquiry. Is that still true of its English department?... more »
 In praise of passive protagonists. The insipidity of characters like Candide and Bartleby belies their triumphant potential... more »
 Faulkner arrived in Hollywood in 1932 and made a lasting first impression: He was drunk, disheveled, and bleeding from the head... more »
 It's been said that most writers are lucky to have one original book in them. A lot of literary careers consist of rewriting it again and again... more »
 All is not well in the humanities. Why are its adherents so eager to convince themselves otherwise? Charlie Tyson unpacks “reassurance lit”... more »
 The real Joan Didion. Her work isn’t about her family or about death, but about the allure of alienation from all that... more »
 The idea of selfhood, taken for granted in secular societies, depends on a theistic worldview. John Gray explains... more »
 In 2004, Richard Rorty went to Tehran. Thousands came to hear how philosophy could build democracy. They got something different... more »
 Writers like V.S. Naipaul felt shame and self-loathing for Caribbean literature. Then came Kamau Braithwaite, who threw off the burden... more »
 The stock market is a fickle beast. Is there such a thing as a philosophically responsible investment?... more »
 The ALD Archives
 New material is added to Arts & Letters Daily six days a week.
 Our motto, "Veritas odit moras," is found at line 850 of Seneca's version of 
 	  	Oedipus. It means "Truth hates delay."
 Founding Editor (1998-2010): Denis DuttonEditor: Evan GoldsteinManaging Editor: Tran Huu DungAssistant Editor: David WescottCopy Editor: Mitch Gerber
 Arts & Letters Daily is brought to you byThe Chronicle of Higher Education
 https://www.aldaily.com/
