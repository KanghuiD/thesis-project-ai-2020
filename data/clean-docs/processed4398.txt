Coronavirus in UK live blog as it happened: lockdown measures likely to be extended according to cabinet minister | Peterborough Telegraph
 Peterborough to celebrate key workers with clap for carers tonight
 Five-day dog festival in Peterborough cancelled
 We will be providing live updates until 6pm this evening.
 #HealthHeroes
 A message from the Editor
 Thank you for reading this story on our website. While I have your attention, I also have an important request to make of you.
 In order for us to continue to provide high quality and trusted local news on this website, I am asking you to also please purchase a copy of our newspaper.
 Our journalists are highly trained and our content is independently regulated by IPSO to some of the most rigorous standards in the world. But being your eyes and ears comes at a price. So we need your support more than ever to buy our newspapers during this crisis.
 With the coronavirus lockdown having a major impact on many of our local valued advertisers - and consequently the advertising that we receive - we are more reliant than ever on you helping us to provide you with news and information by buying a copy of our newspaper.
 Thank you, and stay safe.
 Last updated: Thursday, 09 April, 2020, 17:49
 So concludes today's daily press briefing and today's live blog.
 Tune in at 8am tomorrow for the latest on the coronavirus pandemic
 Professor Whitty said he expects to see an increase in the number of tests going out.
 He said: "We're ramping up every day on the testing but I think it's important to differentiate between tests available and ready to run and the whole system running in terms of in the lab, and actually having the whole system running so you're very confident that someone that needs a test can order the right test and will get the right result back.
 "And the whole process is going up every single day this month, I expect to see an increase in the number of tests that go out and the number of results that come back."
 Asked whether those working in the NHS deserved a pay rise, Mr Raab said: "There will be a moment when we look at how we formally recognise all of those on the frontline who have done so much to pull us through this very difficult period for our country."
 Professor Chris Whitty says that the supply of reagents is now less of an issue
 Sir Patrick, responding to a question about death projections, said: "In general I'd expect the deaths to continue to keep going up for about two weeks after the intensive care picture improves and so we're not there yet in terms of knowing exactly when that will be, but that's the sort of time frame I'd expect."
 Dominic Raab says he has not been in touch with the Prime Minister since he started deputising.
 Professor Chris Whitty says the doubling time of patients in ICU has been slowing over the last two weeks thanks to social distancing. 
 Doubling itme is now six  days and is extending as the pandemic goes on. 
 The trio are taking questions now. 
 Questioned on policing, Raab says the police are doing a great job. 
 He warns public to think about NHS workers before being tempted to ignore rules.
 Sir Patrick Vallance says we're beginning to see the first signs of the virus "levelling off".
 He says that hospital admissions aren't accelerating. 
 The number of daily deaths won't change for a few weeks, according to the chief scientific adviser.
 Dominic Raab is speaking now. 
 He starts by saying thank you to the public for adhering to social distancing rules before thanking NHS workers and key workers. 
 Raab says "we're not done yet" and that we must continue to follow social distancing rules over Easter weekend. 
 He says its too early to say if lockdown meausres are having the desired impact. 
 He says we don't be able to say more on this until the end of next week. 
 He says the measures will stay in place until it's clear that we've moved beyond the peak.
 Dominic Raab is due to hold the daily press briefing at 5pm.
 He will be joined by Sir Patrick Vallance and Professor Chris Whitty
 A police chief has said his force is only "a few days away" from introducing measures such as road blocks and searching shopping trolleys, as people continue to flout coronavirus regulations.
 Northamptonshire Police Chief Constable Nick Adderley said a "three-week grace period" is over in the county and the force will now be issuing fines and arresting people breaking the rules.
 He said further measures will also be implemented should people continue to flout the regulations, including "marshalling" supermarkets and checking the items in baskets and trolleys.
 The O2 Arena will be used as an NHS training facility for those going on to work at NHS Nightingale at London's ExCel centre, O2 has said.
 It will initially operate from April 12 - June 29 to prepare staff, with no patients treated on site.
 "Mobile connectivity is more important than ever before and we're continuing to work hard to keep the country connected," said Mark Evans, chief executive of O2.
 "We've seen the highest volume of voice traffic ever carried on our network and we know how important those conversations are.
 "From providing additional capacity for the NHS, to working with our partners to utilise venues such as The O2, we're committed to giving customers and key workers the network they need to keep in contact with those closest to them."
 O2 said it will not charge the NHS a hire fee for using the site.
 According to Johns Hopkins University the number of confirmed cases of coronavirus around the world has surpassed 1.5m.
 That's a rise of half-a-million in just six days.
 765 more deaths have been recorded in England in the last 24 hours.
 Figures for the UK as a whole are expected later today
 https://www.peterboroughtoday.co.uk/health/coronavirus/coronavirus-uk-live-blog-lockdown-measures-will-continue-number-weeks-says-welsh-health-minister-2533868
