Opinion: First responders deserve line of duty death benefits due to coronavirus crisis - centraljersey.com
  
 Under the normal connotation of this status, these officers have died due to being shot, stabbed, run over by vehicles, or suffering death-related injuries in struggles with suspects. Yet there is now another cause for deaths of law enforcement first responders that should, and must, be considered as a line-of-duty related death: that cause is the death of a law enforcement first responder due to contracting the novel coronavirus.
 
 Law enforcement and correctional officers do not have the privilege of being able to “stay at home” or telecommute for their job. Ours is, unfortunately, a true “feely-touchy, in your face” type of occupation. We must actually be in direct contact and confrontation with people in order to do our job. Yes, it is one that we have gladly espoused for one reason or another, always recognizing that it is a dangerous occupation.
 We urge all law enforcement and corrections agencies, whether they be municipal, county, state, or federal, to recognize the men and women who serve and are now dying from this disease as the true heroes that they are, and bestow upon them full line of duty death benefits. We as well demand that they make every effort to insure that these officers have access to the resources and equipment necessary to protect them from incurring this death-dealing disease.
 Charles P. Wilson
 National chairman
 National Association of Black Law Enforcement Officers, Inc.
 You can find our Archives from 2000 – 2015 at archive.centraljersey.com
 https://centraljersey.com/2020/04/06/opinion-first-responders-deserve-line-of-duty-death-benefits-due-to-coronavirus-crisis/