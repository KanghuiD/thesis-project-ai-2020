Panel: Texas Can Ban Medical Abortions to Fight Coronavirus – NBC 5 Dallas-Fort Worth
 Texas can now ban medication abortions as part of the state's effort to fight the spread of the new coronavirus, a federal appeals court panel ruled Monday.
 The 5th Circuit Court of Appeals in New Orleans last week temporarily blocked the state's prohibition on medication abortions while it gave more consideration to the issue.
 The same panel, in a 2-1 decision, said Monday the state was within its rights to ban that -- and other abortion procedures -- as it sought to slow the use of masks, gowns and other protective medical gear.
 Full coverage of the COVID-19 outbreak and how it impacts you
 The majority opinion by judges Jennifer Elrod and Kyle Duncan said a lower court erred by treating a medication abortion as "an absolute right."
 "But the constitutional right to abortion does not include the right to the abortion method of the woman's (or the physician's) choice," the majority opinion said.
 Clinics have argued that medication abortions do not require personal protective equipment such as masks, gloves and gowns that might be needed for coronavirus patients. 
 Monday's opinion said that it has not been proven that, during the pandemic, such protective equipment would not be used during an examination of a woman seeking a medication abortion.
 The Center for Reproductive Rights said the restriction was the state's latest attempt to restrict abortions and had nothing to do with the 2019 novel coronavirus.
 “The appellate court is creating chaos and uncertainty for women seeking abortions in Texas. Patients who had appointments scheduled will now be thrown into a state of panic yet again," the Center for Reproductive Rights said in a statement Monday. "It’s clear this abortion ban has nothing to do with the pandemic. Texas has been trying to restrict abortion for decades and this is part of that larger strategy. We will continue to fight for the rights of the women of Texas.”
 Texas Attorney General Ken Paxon said he was pleased with the ruling in light of the medical crisis.
 “Without exception, Texans must continue to work together to stop the spread of COVID-19,” said Paxton. “Gov. Abbott’s order ensures that hospital beds, supplies and personal protective equipment remain available for the medical professionals on the front lines of this battle.” 
 https://www.nbcdfw.com/news/coronavirus/panel-texas-can-ban-medical-abortions-to-fight-coronavirus/2354875/
