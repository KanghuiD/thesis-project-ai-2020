Opinion: Alberta’s government is using coronavirus as a shield for laying off education workers - The Globe and Mail
 The subject who is truly loyal to the Chief Magistrate will neither advise nor submit to arbitrary measures.
 Getting audio file...
 Audio for this article is not available at this time.
 This translation has been automatically generated and has not been verified for accuracy. Full Disclaimer
 A empty hallway is pictured at Eric Hamber Secondary school in Vancouver, B.C. in this file photo from March 23, 2020.
 JONATHAN HAYWARD/The Canadian Press
 Blake Shaffer and Trevor Tombe are economics professors at the University of Calgary.
 While the world struggles to contain the worst pandemic in more than a century, many countries are putting their economies into medically induced comas. To contain the spread of COVID-19, people must remain physically apart. It’s difficult, lonely, and financially costly. In response, governments are using their considerable fiscal capacity to support families and businesses through the crisis.
 In effect, they are building a fiscal bridge to the other side of this crisis. Income transfers to individuals stuck at home, wage subsidies to businesses struggling to stay afloat, and a wide variety of other supports are all on the table. We are witnessing the most significant mobilization of fiscal resources by Canadian governments since the Second World War.
 And all of it on borrowed money.
 In Alberta, rising debt levels were already a concern for the new UCP government. It was elected on a platform to lower government spending and balance the books. While the recent crisis threw those plans out the window, it has also provided the government a new pretext to continue its cost-cutting efforts.
 On March 28, Alberta chose to lay off roughly 25,000 education workers, including those providing critical support to many families now struggling to provide home schooling with the kindergarten-to-Grade 12 system shut down.
 The stated rationale for the decision was to “protect the safety, security and economic interests of Albertans,” with resources needing to be diverted to fight the COVID-19 pandemic. The implied premise of the government’s actions was that this was a zero-sum choice: it could spend on education or health care, but not both.
 What was being suggested was the government faced a liquidity crisis – the inability to borrow more, requiring it to ration available funds to highest-value uses. If true, this would be a serious turn of events for the wealthiest province in Canada, albeit one beset by a severe economic downturn.
 The trouble is, it’s not true.
 Alberta’s budget is not subject to external borrowing constraints that forced this decision. Nothing illustrates the falsehood of a liquidity crisis better than the fact that, only two days later, the government injected $1.5-billion into TC Energy’s Keystone XL pipeline project, followed by an additional $6-billion loan guarantee next year.
 Whatever the merits of this specific decision – and, to be clear, we accept there may be arguments in its favour if TC Energy was teetering – this aid, at the very least, demonstrates Alberta’s strong balance sheet and incredible fiscal capacity.
 Capital markets are indeed strained as investors seek shelter from the crisis, yet Alberta retains the ability to issue debt. It even issued a 100-year bond as recently as last week. While provincial credit spreads versus Government of Canada debt have widened of late, Alberta can still borrow at historically low rates – barely over 2 per cent for 10 years. Adjusting for inflation, the real interest rate the provincial government faces is therefore almost zero. Further, the Bank of Canada has already begun purchasing billions per week of Government of Canada bonds, and there are indications it may step into provincial debt markets should provincial credit spreads widen further.
 While the provincial government is not as constrained as it says it is, Alberta does have real challenges before it that should force serious debate and discussion among all Albertans and their leaders. But the government is incorrectly framing its decision to cut education spending as a response to externally imposed financing constraints, rather than a choice.
 The fact is, cutting K-12 spending by $128-million when, a week before, the Education Minister assured school boards they would continue to receive their full 2019/20 allotments was a choice: one around how much debt to take on, not a question of whether it could.
 This is not to say borrowing and spending in every area, on every project, or under any circumstances makes sense. Alberta’s debt is growing rapidly – this year’s deficit is likely to top $15-billion, given current oil price and spending levels – and at some point that debt will become a concern. But for now, it remains manageable. Spending decisions need to be evaluated based on their individual merits, and this can and should involve reasonable debate.
 But the government is misleading Albertans, telling them that they have no choice but to divert or cut spending – that the province has lost the ability to raise cash. That should not be happening. There are no external financing constraints that forced the government to make this decision. It was a choice: One that it must live with, and that Albertans should rightly judge.
 Keep your Opinions sharp and informed. Get the Opinion newsletter. Sign up today.
 Your subscription helps The Globe and Mail provide readers with critical news at a critical time. Thank you for your continued support. We also hope you will share important coronavirus news articles with your friends and family. In the interest of public health and safety, all our coronavirus news articles are free for anyone to access.
 We aim to create a safe and valuable space for discussion and debate. That means:
 Read our community guidelines here
 Discussion loading ...
 351 King Street East, Suite 1600, Toronto, ON Canada, M5A 0N1
 Phillip Crawley, Publisher
 https://www.theglobeandmail.com/opinion/article-albertas-government-is-using-coronavirus-as-a-shield-for-laying-off/
