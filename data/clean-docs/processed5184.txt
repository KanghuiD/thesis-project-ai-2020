Coronavirus (COVID-19) Testing for Undocumented Immigrants in NJ : Immigration Law Blog
 Blogs > Immigration Law Blog
 Posted on April 13th, 2020 | Authors
 						: 
 Officials in New Jersey, like the rest of the country, are urging people with symptoms of COVID-19 to get tested. 500,000 unauthorized immigrants in New Jersey are at a crossroads, as many do not have health insurance. However, many immigrant advocates say there are options for undocumented immigrants who need COVID-19 testing.
 The Federal Emergency Management Agency conducts COVID-19 testing for state residents free of cost, immaterial of their immigration status. However, to get tested, the patients must show a state or municipal identification. The proof of identification varies from county to county. While some counties have said that a utility bill will suffice, other counties have said that a valid passport and a utility bill are required. There are other counties that will only administer the test when the patient has a medical referral, which is a hurdle for most undocumented immigrants, as many do not have medical insurance.
 Torres of Movimiento Cosecha, a nonprofit that campaigns for immigrants, said, “Immigrants not only face extra challenges getting tested but also fear that if they do need hospital care, they won’t be able to afford it.” There have been other immigrants’ rights groups that have received calls from desperate immigrants who show symptoms of COVID-19 but do not know how and where to get tested. Torres also said that “there’s a lack of information and lack of resources, and [immigrants] do not know where they can go to get help.”
 New Jersey has been testing prioritized patients only when they display symptoms of COVID-19, have traveled to and from highly-affected areas, or have been in close contact with someone who was tested positive for COVID-19. According to the New Jersey Department of Health, as of April 12, 2020, there are 61,850 positive cases and 2,350 deaths.
 A complete list of the New Jersey health centers that are testing for COIVD-19 can be found here. Immigrants can also call the New Jersey State Department of Health public hotline number at 1 (800) 222-1222, which offers multiple language options.
 The good news is that if undocumented immigrants need emergency treatment at a hospital, they cannot be turned away. The state has many charity-care programs that will provide medical treatment for immigrants and for others who cannot afford medical treatment.
 The information contained in this post may not reflect the most current developments, as the subject matter is extremely fluid and constantly changing. Please continue to monitor this site for ongoing developments. Readers are also cautioned against taking any action based on information contained herein without first seeking advice from professional legal counsel.
 Posted in: Hot Topics, Undocumented |
           Tags: Coronavirus (COVID-19), health care, New Jersey 
 Receive timely legal information delivered to your inbox.
 Bridgewater, NJ (908) 722-0700 | New York, NY (212) 808-0700 | Allentown, PA (610) 391-1800
 (function(){var s = document.getElementsByTagName("script")[0];
 var b = document.createElement("script");
 b.type = "text/javascript";b.async = true;
 b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
 s.parentNode.insertBefore(b, s);})();
   !function(f,b,e,v,n,t,s)
   {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
   n.callMethod.apply(n,arguments):n.queue.push(arguments)};
   if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
   n.queue=[];t=b.createElement(e);t.async=!0;
   t.src=v;s=b.getElementsByTagName(e)[0];
   s.parentNode.insertBefore(t,s)}(window, document,'script',
   fbq('init', '595568424315541');
   fbq('track', 'PageView');
 https://norrismclaughlin.com/ib/2020/04/13/coronavirus-covid-19-testing-available-for-undocumented-immigrants-in-new-jersey/