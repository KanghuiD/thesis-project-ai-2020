Trump vs. Biden on coronavirus: The timeline is utterly damning - The Washington Post
 We’re now learning that President Trump’s efforts to promote an anti-malarial drug for use against the coronavirus is causing internal administration tensions. Anthony Fauci, the administration’s own leading expert, privately challenged this optimism about hydroxychloroquine, and fittingly, Trump prevented Fauci from publicly reiterating this skepticism at Sunday’s briefing.
 Only hours earlier, Joe Biden presented a striking contrast. On ABC’s “This Week,” Biden called on Trump to exercise the Defense Production Act to marshal the private sector to speed lifesaving equipment to hospitals, urged stricter social distancing and reiterated the need to “follow the science” and “listen to the experts.”
 It’s now clear that this stark contrast could define the 2020 campaign. Democrats are unveiling new ads highlighting Trump’s serial failures to take the coronavirus seriously. Meanwhile, the Trump campaign is urging surrogates to claim, laughably, that Trump is “leading the nation” in the “war against coronavirus,” and to cast the former vice president as “the opposition in that war.”
 Full coverage of the coronavirus pandemic
 So let’s talk about who said what about coronavirus, and when they said it.
 I’ve compiled a timeline that compares public statements by Biden and Trump throughout the early days of this crisis, when extraordinary levels of failure by Trump and his administration squandered crucial lost time whose consequences are only beginning to be felt.
 What’s notable is that this contrast — Trump defying science and experts on one side, and Biden calling for a response shaped around science and expertise on the other — has been omnipresent throughout:
 Trump, Jan. 22: The president tells CNBC that “we have it totally under control” and “it’s going to be just fine.”
 Top Biden adviser, Jan. 22: Ron Klain, a Biden adviser who managed the 2014 Ebola response, co-writes a piece excoriating Trump for “brashly” dismissing coronavirus as “under control,” while calling for “expertise” to “guide critical decisions” and noting “reasons for great concern.”
 Trump, Jan. 24: Trump praises and gives thanks to China for its efforts to contain the coronavirus, and adds: “It will all work out well.”
 Biden, Jan. 27: Biden publishes an op-ed in USA Today hitting Trump for “shortsighted policies" that "have left us unprepared for a dangerous epidemic,” and warning that the coronavirus “will get worse before it gets better.”
 Trump, Jan. 30: Trump says at a rally in Michigan: “We think we have it very well under control.”
 Biden, Jan. 31: Biden tells reporters in Iowa that “science” must “lead the way,” adding: “We have, right now, a crisis with the coronavirus.”
 Biden, Feb. 1: Biden tweets: “We are in the midst of a crisis with the coronavirus. We need to lead the way with science.”
 Trump, Feb. 2: Trump goes on Sean Hannity’s show and claims: “We pretty much shut it down, coming in from China.” Trump extols our “tremendous relationship” with China, and adds: “We did shut it down, yes.”
 Trump, Feb. 10: Trump claims that “a lot of people” think the coronavirus “goes away in April with the heat,” adding that we only have “11 cases,” and that “we’re in great shape.”
 Biden, Feb. 11: Biden goes on “Morning Joe” and excoriates Trump for claiming the coronavirus will disappear in the warm weather, crossing himself while doing so, and adding: “You couldn’t make it up.”
 Trump, Feb. 26: Trump claims the media is conspiring with Democrats to hype the coronavirus to rattle the markets. Trump also says the coronavirus is “going very substantially down, not up.”
 Trump, Feb. 27: Trump hails his administration’s handling of the coronavirus, and while he does reveal a hint of uncertainty, he says: “It’s going to disappear. One day — it’s like a miracle — it will disappear.”
 Trump, Feb. 28: Trump shouts at a rally in South Carolina that Democrats’ criticism of his response (which proved entirely accurate) is “their new hoax.”
 Biden, Feb. 28: Biden goes on CNN and says Trump has yet to “gain control” of the coronavirus, while calling on Trump to stop downplaying it and urging him instead to “let the experts take this over” and “let the experts speak.”
 Trump, March 9: Trump dismissively compares the coronavirus with flu, claiming flu kills tens of thousands annually and that “life & the economy go on.”
 Trump, March 10: Trump again hails the “great job” he’s doing on the coronavirus, and declares: “It will go away. Just stay calm. It will go away.”
 Biden, March 12: Biden gives a speech stressing the importance of presidential truth-telling amid crises, noting that Trump’s ongoing falsifications risk leaving Americans without reliable guidance, compounding “public fears.”
 Trump, March 13: Asked about the administration’s epic failure to ramp up testing, Trump declares: “I don’t take responsibility at all.”
 Biden, March 15: Biden responds to that Trump quote by reiterating his call for widespread free testing, and by declaring: “It is the job of the president to take responsibility — and his response is unacceptable.”
 Trump, March 17: After all that, Trump preposterously proclaims: “I felt it was a pandemic long before it was called a pandemic.”
 It’s worth stressing that all this public dismissiveness by Trump reflects a mind-set that has had horrifying and extensive real-world consequences.
 As The Post’s weekend exposé revealed, it took 70 days from the time Trump initially learned of the coronavirus to the point at which he treated it as a serious threat to untold numbers of American lives. Trump’s instinct to minimize and lie about the coronavirus saturated his government’s response, to catastrophic effect:
 Obviously one cannot know how successfully a President Biden would have managed this crisis. But it has become inescapably obvious that under a different president, things probably would look rather different right now.
 One shudders to think how many lives that might have saved, and how many will now be lost.
 The Opinions section is looking for stories of how the coronavirus has affected people of all walks of life. Write to us.
 Jonathan Capehart: Susan Rice on Trump’s coronavirus response: ‘He has cost tens of thousands of American lives’
 Max Boot: The worst president. Ever.
 James Downie: Democrats must investigate Trump’s coronavirus response
 Sophie A. Greenberg: As a doctor, I am willing to do whatever it takes to defeat this virus. Why isn’t Trump?
 Greg Sargent: Trump’s effort to bury the truth about his coronavirus fiasco is failing
 Greg Sargent: A brutal new ad uses Trump’s own words against him
 Live updates: The latest in the U.S. and abroad
 More news today: How false hope spread about hydroxychloroquine as treatment for covid-19
 How to help: Your community | Seniors | Restaurants | Keep at-risk people in mind
 Share your story: Has someone close to you died from covid-19?
 Follow the latest on the outbreak with our newsletter every weekday. All stories in the newsletter are free to access.
 https://www.washingtonpost.com/opinions/2020/04/06/trump-versus-biden-coronavirus-timeline-is-utterly-damning/
