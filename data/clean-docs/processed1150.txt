BLOG: Duggan updates COVID-19 response, announce new safety measures
 Would you like to receive local news notifications on your desktop?
 Menu
 DETROIT (WXYZ) — Mayor Mike Duggan has updated Detroit's response to the COVID-19 outbreak.
 Duggan is also expected to announced new health safety measures that will be put into place on DDOT buses.
 3:16 p.m.
 Duggan announced that the city's nutrition centers will not be operating this Friday because of the holiday. They will be giving out extra meals on their final day of operation for the week to get kids through the weekend.
 3:14 p.m.
 Duggan says they will begin using the 15 minute Avid Labs coronavirus testing on nursing homes. They will be testing both staff at the home at the testing site and then medical students will be going to the nursing homes to test the residents. 
 The testing on the residents will be processed after the live testing at the testing center is done for the day.
 3:12 p.m.
 Officials will begin placing boxes of surgical masks on DDOT buses, along with signs asking people who do not already have on to take one. The will be put into place tonight. 
 3:10 p.m.
 Duggan says the death rate for African Americans in 23X the death rate of Caucasians from COVID-19. He says the virus is widening the racial health gap in the country.
 3:09 p.m.
 "Social distancing in the city of Detroit is starting to work." However Duggan says we're going to have some very bad days still and we'r going to lose a lot of our neighbors.
 3:08 p.m.
 Duggan says we are at a very dangerous time because we have to keep doing what we've been doing and not going back to business as usual.
 3:05 p.m.
 Duggan shows statistics show that the rate of double of deaths in Detroit has slowed since the social distancing and stay-at-home orders were put into place. The rate of doubling is down to 5 to 6 days, from doubling every 2 days.
 3:04 p.m.
 Duggan is sharing some raw data to show the pattern of COVID-19. He begins with the daily total of Detroiters have died of the virus.
 He says the "rate of doubling" is the most important stat.
 3:01 p.m.
 Duggan says test testing is letting the Detroit Police Department get back to normal operations.
 3:00 p.m.
 Duggan says they have 6,000 test scheduled for the testing center at the State Fairgrounds, calling it the "smoothest running drive-thru center in America.
 Additional Coronavirus information and resources:
 Read our daily Coronavirus Live Blog for the latest updates and news on coronavirus.
 View a global coronavirus tracker with data from Johns Hopkins University.
 Find out how you can help metro Detroit restaurants struggling during the pandemic.
 See all of our Helping Each Other stories.
 See complete coverage on our Coronavirus Continuing Coverage page.
 https://www.wxyz.com/news/coronavirus/live-blog-duggan-to-update-covid-19-response-announce-new-safety-measures
