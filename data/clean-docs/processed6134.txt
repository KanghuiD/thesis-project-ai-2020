'Coronavirus is the biggest low in my career' - BBC News
 The BBC's weekly The Boss series profiles different business leaders from around the world. This week we speak to UK-based Serbian fashion designer Roksanda Ilincic.
 I should be speaking to Roksanda at her east London studio, surrounded by the unusual, colourful clothes that made her name. 
 But the coronavirus pandemic means we've had to relegate our interview to a chat over the phone, where her passion for design still shines through, alongside her distress at the current situation.
 "It's a very sad and challenging time," she says. "We are questioning everything - how we and our families are going to survive, and our businesses as well. 
 ''Sadly, this time is the biggest low in my career, and I just hope together we are going to find some answers."
 So, how does one lead a team of 35 while designing from home? With difficulty, Roksanda says.
 "It's not easy, because in design lots of things are done in 3D, so you have to assess things on a model, and see how they look in each direction - it's very tactile."
 The UK has been the Serbian designer's home for more than two decades, after she enrolled on a masters degree in fashion at the prestigious Central Saint Martins college in London. Born and raised in Serbia, she knew just one person in London when she started in her early 20s.
 Despite her "zero" fashion contacts, Roksanda's self-titled designs have become the go-to look for some of the world's most famous women. 
 Michelle Obama and Kate Middleton, the Duchess of Cambridge, are long-term supporters of her brand. While Meghan Markle wore one of her designs on her 2018 tour of Australia with Prince Harry.  It promptly sold out, prompting a host of High Street lookalike versions.
 ''When you see how many different women - the Duchess of Cambridge, but also other incredible women - wear me, I am very proud I can appeal to women who are diverse, not just in personality, but in body shape and style,'' the designer says.
 Accordingly, Roksanda has collaborated on a plus size collection, and says she is more inspired by her mum and her daughter than by her famous fans.
 When discussions turn to getting the UK moving again, post coronavirus, the fashion industry may not be top of the list.
 Yet the sector is worth £32bn to the British economy and employs almost 900,000 people. That's a bigger workforce than the car industry, as Roksanda points out.
 And fashion isn't the bubble that some would have us believe, she says. Look at Christian Dior, whose feminine, opulent designs at the end of World War Two were a direct reaction to the drab, utilitarian clothing worn during the conflict.
 "Clothes are worn to protect us, like a uniform. But fashion is something very different. It is a mirror of society and what's going on," says Roksanda.
 "In hard times, some people want to retreat and go to the basics, while others want to dream and escape. Fashion tackles and answers both of those needs."
 Roksanda's work has always been a reflection of her wider preoccupations, all the way back to her Saint Martins studies, where "terrible homesickness" for Serbia inspired her designs.
 ''The colours that I'm so well known by came out of nostalgia for my country. Back home we have wonderful strong suns, and colours bursting from nature, and I wanted to brings those elements in to my work, to remind me what I'm missing,'' she explains.
 Despite this pining - and the fashion world's frosty reputation - Roksanda says she was welcomed in London, a city of "endless possibility".
 After graduating from Saint Martins, she was given financial support from the British Fashion Council's "NewGen" scheme. Sponsored by fashion store chain Topshop, it backs new designers.
 Then in 2005, Roksanda made her London Fashion Week debut, showcasing 11 dresses. Her reputation and sales grew from there.
 More The Boss features:
 Today, like her Saint Martin's peer Stella McCartney, Roksanda has become synonymous with British fashion industry, and she now helps to campaign for more British exports.
 Her status was cemented earlier this year when she hosted her 2020 London Fashion Week show in the grand surroundings of the UK government's Foreign and Commonwealth Office, looked on by stars such as actors Cate Blanchett and Vanessa Redgrave.
 Roksanda's brand has "a real signature look, which helps it to stand out in a crowded market,'' says Chana Baram, senior retail analyst at market research group Mintel.
 Regarding the wider luxury fashion sector, Ms Baram says it had been performing far better than the mainstream clothing, footwear and accessories market over the past few years. However, she warns that the large-scale shut down of Italy and China will be a huge blow to the high end fashion industry this year.
 "Luxury industries, and all fashion sectors, are going to take a big hit in 2020," says Ms Baram. "For smaller brands it might be really tough going, but they will hopefully be able to pull through and see success in 2021."
 Back at Roksanda's studio, she says that if you want to be successful in business, or whatever else you want to do well in, you have to dedicate your life to it.
 "There is this myth, when you look at somebody successful, that you think it happened quick, or it was effortless," she says.
 "But with every win comes many losses, and you have to sacrifice things to gain something. It's important to have passion, belief and patience for something you really want to do, and not to expect things to happen overnight."
 Germany is among several European countries easing the lockdown as rates of infection slow.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/business-52189121
