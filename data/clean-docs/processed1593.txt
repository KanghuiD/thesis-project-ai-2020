coronavirus — Blog — Jill on Money
 
 coronavirus
 More than 5 million people filed for unemployment in the last week as coronavirus lockdowns continue through the country. Although the number is lower than the previous two weeks, the figure rounds out a grim four-week span in which over 20 million Americans filed jobless claims. I joined CBS This Morning to talk about what that figure means for the U.S.'s road to economic recovery.
         Tagged: Jill Schlesinger, Jill on Money, corona, coronavirus, economy, recession, jobless claims, unemployment benefits, unemployed
 Please leave us a rating or review in Apple Podcasts.
 "Jill on Money" theme music is by Joel Goodman, www.joelgoodman.com.
         Tagged: Jill Schlesinger, Jill on Money, corona, coronavirus, bankruptcy, retire, retirement, retirement planning
 A refinance can be a total pain in the butt, that said, it can also be totally worth the required time and effort. But before you do anything, run the numbers! 
         Tagged: Jill Schlesinger, Jill on Money, refi, refinance, mortgage, corona, coronavirus, retire, retirement, retirement planning
 There are rules of thumb in the investing world, pandemic or no pandemic, and that includes where to park money for short-term needs. 
         Tagged: Jill Schlesinger, Jill on Money, corona, coronavirus, retire, retirement, retirement planning
 Crazy times in the market makes us all re-evaluate things, or at least it should, and that includes whether or not it's the right time to enlist the help of a professional. That's the conversation with Rachel in Colorado. 
 Just over half of all American workers are employed by small businesses (companies with 500 or fewer employees), and according to the U.S. Chamber of Commerce, nearly a quarter of those small businesses could close permanently within two months without a financial lifeline. I chatted with some small business owners who are trying to maintain their (and their employees') livelihoods in a time of tremendous uncertainty.
         Tagged: Jill Schlesinger, Jill on Money, corona, coronavirus, recession, economy, small business, small business owners
 Here we go again, more questions from people who jumped out of the market and now want to know when it makes sense to jump back in. When I find that crystal ball, I'll let you know :)
         Tagged: Jill Schlesinger, Jill on Money, corona, coronavirus, market timing, timing the market, retire, retirement, can I retire?
 While most individuals need the stimulus check coming their way, some are fortunate and do not. If that's you, what should you do with it? 
         Tagged: Jill Schlesinger, Jill on Money, corona, coronavirus, stimulus check, retire, retirement
 The deepest recession “of our lifetimes” could be on the horizon, according to the World Trade Organization; coronavirus has delivered “unprecedented shocks to economies and labor markets.”
         Tagged: Jill Schlesinger, Jill on Money, recession, corona, coronavirus, economy, jobless claims, unemployment benefits, unemployed
 Over 15 million Americans applied for unemployment benefits over the last three weeks, a devastating reflection of the coronavirus pandemic's impact on the economy. Many people, even those who do not know if they will still be employed, are looking for information to help navigate the uncertainty. I joined CBS This Morning to respond to some of viewers' critical personal finance questions.
         Tagged: Jill Schlesinger, Jill on Money, corona, coronavirus, unemployment, jobless claims, recession, unemployment benefits
 All opinions expressed by Jill Schlesinger on this website and on the “Jill On Money” radio show are solely Jill Schlesinger’s opinions and do not reflect the opinions of CBS News, its parent and affiliated companies or radio station affiliates. You should not treat any opinion expressed on this website or on the radio show as a specific inducement to make a particular investment or follow a particular strategy, but only as an expression of an opinion. Jill Schlesinger, CBS News, its parent and affiliated companies and radio station affiliates make no warranty as to the completeness or accuracy of any opinion expressed on this website or on the radio show, and any opinion expressed on this website or on the radio show should not be relied upon as complete or accurate. Before acting on any information on this website or on the radio show, you should consider whether it is suitable for your particular circumstances and seek advice from your own financial or investment adviser
 https://www.jillonmoney.com/blog/tag/coronavirus