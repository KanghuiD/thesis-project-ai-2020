The good things the coronavirus situation brings - Opinion - Israel News | Haaretz.com 
 With Passover Eve upon us, the keyboard refuses to accept anymore doomsday scenarios or desolate visions of what we can expect in the coming months in the wake of the economic and social crisis caused by the coronavirus pandemic.
                                                     
 Yes, the decision-making is a mess and there is a lot of anxiety both health-wise and financially, with over a million unemployed, and hundreds of thousands of self-employed who don’t know how they’ll survive the crisis. The government that could be emerging may also make many of us unhappy. But to paraphrase Prime Minister Benjamin Netanyahu: Leaving aside the sick, the dead, the unemployed and the bankrupt – Our situation is excellent. This is an exaggeration, of course, but there are still some encouraging things about the dynamic caused by the coronavirus crisis.
 1. The pictures of IDF soldiers distributing food to the needy and to people shut inside their homes in Bnei Brak and other ultra-Orthodox neighborhoods are heartwarming and plant the hope that this will help topple the walls built up over many years in Haredi society’s attitude toward the army, by a hardline rabbinical establishment and maintained by cynical identity politics. This is not a victory for the IDF or science over unenlightened rabbis, it’s a victory of goodwill and solidarity shown by Israeli society toward a community in need. It’s doubtful that this will spur throngs of Haredi young men to enlist in the near future, but it will certainly make it clear to the extremists in that society who viewed the army as the enemy – just who was at their side in a time of trouble.
 3. Before the coronavirus crisis erupted, only 4 percent of Israeli employees worked from home, despite the many options for working remotely offered by the internet and related technology. A survey by the Central Bureau of Statistics found that in late March, when the closure restrictions were in force, the rate of people working from home was up to 21 percent.
 It’s too soon to gauge the productivity of working from home, but it’s already possible to see how much such a solution could contribute to reducing congestion on the highways, air pollution and time wasted in traffic jams. If this arrangement becomes more common once the crisis abates and we see a higher rate of people working from home, it could be a good antidote to the transportation hell we’ve witnessed in Gush Dan in recent years.
 Please try again later.
 4. We need to get used to the idea that major crises seem to occur about once every decade. In the previous decade it was the global financial crisis that led to the rise of populist politicians and a professional bureaucracy called the Deep State.
 The current crisis underscores the crucial role of professionals and scientists in problem solving. The flippant attitude initially adopted by British Prime Minister Boris Johnson and U.S. President Donald Trump, who scoffed at the danger of coronavirus, has been dealt a serious blow, and this may help recalibrate a balance between elected officials and public servants and point up the importance of having social safety nets.
 5. After the disappointment and breakup of the potential alternative to Likud and Netanyahu, it’s a good thing Israel will soon have a new government. Israel cannot afford to hold another election.
 Sami Peretz
  Haaretz Contributor 
 Want to enjoy 'Zen' reading - with no ads and just the article?
 Sign in
                 to join the conversation.
             
                                     accordance with site policy.
                                 
 								
 Please try again later
 Haaretz.com, the online edition of Haaretz Newspaper in Israel, and analysis from Israel and the Middle East. Haaretz.com provides extensive and in-depth coverage of Israel, the Jewish World and the Middle East, including defense, diplomacy, the Arab-Israeli conflict, the peace process, Israeli politics, Jerusalem affairs, international relations, Iran, Iraq, Syria, Lebanon, the Palestinian Authority, the West Bank and the Gaza Strip, the Israeli business world and Jewish life in Israel and the Diaspora.
 https://www.haaretz.com/opinion/.premium-the-good-things-the-coronavirus-situation-brings-1.8749305
