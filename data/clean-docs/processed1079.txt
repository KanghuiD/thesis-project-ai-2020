Coronavirus: Older people being 'airbrushed' out of virus figures - BBC News
 Many older people are being "airbrushed" out of coronavirus figures in the UK, charities have warned.
 The official death toll has been criticised for only covering people who die in hospital - but not those in care homes or in their own houses.
 Work and Pensions Secretary Therese Coffey told the BBC the daily figure was based on hospital deaths because "it's accurate and quick".
 Meanwhile, scientists will begin a review of the UK lockdown later.
 The evaluation will be passed to the government - but ministers have said it was unlikely restrictions would change.
 The latest figures from the Office for National Statistics, which include every community death linked to Covid-19 in England and Wales, showed a total of 406 such deaths registered up to 3 April had occurred outside of hospitals. 
 That would have added an extra 11% to the official UK figures, based solely on deaths in hospitals, that were being reported at that time.
 Of those extra deaths, 217 took place in care homes, 33 in hospices, 136 in private homes, three in other communal establishments and 17 elsewhere.
 Northern Ireland's chief medical officer has said details about the number of coronavirus-related deaths in care homes remain unclear, but it was reported last week that there were cases of Covid-19 in 20 care homes across the nation.
 Industry leaders from Age UK, Marie Curie, Care England, Independent Age and the Alzheimer's Society have written to Health Secretary Matt Hancock demanding a care package to support social care through the pandemic.
 They have also called for a daily update on deaths in the care system.
 It comes after the government confirmed there had been coronavirus outbreaks at more than 2,000 care homes in England - although they did not specify the number of deaths that had occurred.
 The figures prompted the charity Age UK to claim coronavirus is "running wild" in care homes for elderly people.
 "The current figures are airbrushing older people out like they don't matter," Caroline Abrahams, the charity's director, said.
 Meanwhile, Britain's largest care home operator, HC-One, said coronavirus was present in two-thirds - 232 - of the group's care homes.
 Its director, Sir David Behan, told BBC Radio 4's Today programme that coronavirus deaths represented about one-third of all deaths at HC-One's care homes over the last three weeks. HC-One has 329 care homes throughout England, Scotland and Wales.
 MHA, a charity which operates 131 homes, said there had been 210 coronavirus-related deaths.
 About 410,000 people live in care homes in the UK, living in 11,300 care homes for older people supplied by 5,500 different providers.
 Addressing why deaths in care homes are not being included in the government's data, Ms Coffey told BBC Radio 4's Today programme that the figures published weekly by the ONS is a "fair" way of establishing the "unfortunate picture" of where deaths are occurring.
 England's care home regulator, the Care Quality Commission, has said it will begin recording deaths in adult social care from this week - asking care providers to give daily updates on the number of confirmed and suspected cases.
 Labour's shadow social care minister Liz Kendall said daily figures were essential to dealing with the "emerging crisis" in care homes and called for the government to offer social care "whatever resources it needs".
 Conservative peer and former work and pensions minister Baroness Altmann told Today that "one or two" people in care homes had said to her they felt as though older people are being treated "like lambs to the slaughter".
 "They [care homes] are left without protective equipment, they are left without testing," she said. 
 She added that "the mark of a civilised society" was "how it treats it most vulnerable and oldest citizens".
 It comes after Ms Abrahams said care homes were "underprepared" for the outbreak, adding that the lack of personal protective equipment (PPE) and testing was leading to the spread of coronavirus across the care home sector. 
 However, Ms Coffey told the Today programme that the care sector was not being left behind, adding that PPE was being delivered "to over 26,000 care settings across the country including care homes, home care providers and also hospices".
 On Monday, the UK's chief medical adviser Prof Chris Whitty told the daily Downing Street coronavirus briefing that 92 homes in the UK reported outbreaks in one day.
 The Department of Health and Social Care later confirmed 2,099 care homes in England have so far had cases of the virus.
 Care England has estimated there have been nearly 1,000 deaths from coronavirus in care homes, leaving social care as "the neglected front line".
 The Labour Party has called on the government to publish daily figures of deaths in care homes to highlight the "true scale" of the spread of the virus, which causes the Covid-19 disease.
 The issue has regularly been raised by journalists at the daily Downing Street briefing and the government response has been that the number announced each day is based on hospital figures as this can be quickly gathered and analysed - whereas deaths in the wider community take much longer to be collated after death certificates are issued by doctors.
 The government says it is following the international standard by quoting the hospital figures each day - and that the fuller ONS figures can lag many days behind.
 The latest care homes to confirm residents have died with symptoms of the virus include a home in Drumchapel, Glasgow, a specialist dementia home in Selston, Nottinghamshire, and a home in County Durham where 13 residents have died. 
 The Department of Health's official death number of deaths of people in hospital with coronavirus rose to 11,329 on Monday - up by 717 in a day.
 The BBC's science editor David Shukman said the Scientific Advisory Group for Emergencies (Sage) meeting later in the day will evaluate various ways coronavirus is unfolding in the UK.
 It will look at hospital admissions, the approach to testing, data on intensive care capacity and deaths, the effectiveness of lockdown tactics, and whether or not the public should be advised to wear face masks outdoors.
 Meanwhile, the government has defended itself after reports it missed three chances to bulk-buy PPE for healthcare workers treating virus patients.
 Health workers in 25 EU countries are set to receive deliveries of kit worth £1.3bn in the coming days, according to the Guardian.
 The paper reports the UK missed three opportunities to join the scheme and has not taken part in talks on future purchases.
 The Department of Health said it would "consider participating in future EU joint procurement schemes on the basis of public health requirements at the time".
 "We will continue to work with European countries and others in order to make sure that we can increase the capacity within the NHS," they said.
 In other developments:
 Or use the form below
 If you are happy to be contacted by a BBC journalist please leave a telephone number that we can
                         you provide it and location, unless you state otherwise. Your contact details will never be published.
                         When sending us pictures, video or eyewitness accounts at no time should you endanger yourself or others,
 The World Health Organization "failed in its basic duty" over coronavirus, Donald Trump says.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/uk-52275823
