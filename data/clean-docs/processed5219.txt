 Coronavirus updates: data, infections, deaths USA, India and UK on 1 April - AS.com
 Other Sports
 AS English
 CORONAVIRUS
 Coronavirus: total USA cases and deaths, real-time map, 1 April
 Coronavirus
 Coronavirus: total UK cases and deaths, real-time map, 1 April
 Coronavirus: total India cases and deaths, real-time map, 1 April
 Coronavirus: total Nigeria cases and deaths, real-time map, 1 April
 We’re taking a short break now from our live coverage of the coronavirus pandemic, but the morning team will be here soon with all the latest news. In the meantime, here’s our front page for Thursday: A star is born on Real Madrid’s Achraf, currently on loan at Dortmund, but a target for Juventus, Chelsea, PSG. Here’s hoping we can get back to transfer speculation dominating the headlines soon… stay safe wherever you are. 
 The US has reached 5,000 deaths during the coronavirus pandemic. This news comes after Donald Trump back-pedalled on his view of the threat of the virus. He initially said it was no worse than the flu and that it would be gone very quickly but he now accepts that 250,000 Americans might die from the virus. 
 Dolly Parton is donating a million dollars to coronavirus research, with the money going to the Vanderbilt University Medical Center in Nashville, Tennessee. 
 Donald Trump said on Wednesday the United States will soon have more ventilators than it needs and there will be enough eventually to send some to other countries. Many state governors complain they do not have enough of ventilators at present to meet the urgent need.
 Trump has been asked if China underreported their death numbers. "Their numbers seem to be a little bit on the light side," Trump says. "And I'm being nice when I say that."
 "We must not let the drug cartels exploit the pandemic to threaten American lives," Trump says. He says they have been so focused on coronavirus that it is time to re-focus on the drug issue but says they "hope" and think it will have an effect on the coronavirus.
 This attack on drugs cartels will also help coronavirus crisis as it will stop people "coming in".
 Donald Trump's press conference kicks off and announces that they are going after cartels and those trying to take advantage of the coronavirus by bringing drugs into the country.
  "Football needs to have a good, long, hard look at itself and see whether or not morally this is really right..." UK MPs have laid into Premier League clubs for seeking government aid to pay players' wages during the Covid-19 crisis. 
 Italy's daily death toll from coronavirus on Wednesday was the lowest for six days, authorities said, but the overall number of new infections grew and the government extended a national lockdown until at least the middle of April.
 The Civil Protection Agency said 727 people had died over the last 24 hours, down from 837 the day before, bringing total fatalities from the world's deadliest outbreak of the viral pandemic to 13,155.
 Brazilian governors are ignoring Jair Bolsonaro's advice and calls not to shut the country down amid the coronavirus pandemic.
 Wilson Witzel, Rio de Janeiro state’s rightwing governor, has also refused to back away from strict social isolation measures. “So far I’ve been asking, now I am giving an order: don’t leave your home,” Witzel told his state’s 17 million residents on Monday as he extended Rio’s shut down for another fortnight. Witzel, a one-time Bolsonaro ally, went on to suggest the president’s behaviour could land him a trial at the International Criminal Court in the Hague. - The Guardian.
 Covid-19
 Coronavirus USA: what does lockdown mean and what is an essential business
 A 13-year-old boy has become the youngest known person to die of coronavirus in the UK according to Britain's office of National Statistics.
 India keeps rising to the occasion... as well as working on manufacturing ventilators for hospitals for a cost of $650, Indian Rail is using it's rolling stock as makeshift isolation units.  
 Why toilet paper? Is this the burning question you've been wondering about?
 Coronavirus: why is toilet paper most bought in supermarkets?
 The total number of Covid-19 cases in the USA has now passed 200,000, with 191,774 cases currently active. 
 UK daily briefing summary
 - number of deaths confirmed in hospitals increased by 563 to 2,352
 - the world famous Edinburgh festival and Wimbledon cancelled for this year
 - now capacity for nearly 3,000 tests which is to increase further. Drive-through test centres to be set up. Intention to get from thousands to hundreds of thousands of test done within the coming weeks.
 A clear frustration on the back of the briefing was an inability, once again, for direct questions about failings and challenges to be given clear answers.
 Wimbledon cancelled
 The Wimbledon tennis championships have been cancelled for the first time since World War Two as the coronavirus pandemic wiped another top sports event off the calendar.
 Economic report (Reuters)
 The dollar advanced on Wednesday, with markets staring at what looked likely to be one of the worst economic contractions in decades as the world confronts the coronavirus pandemic. The greenback rose against the euro, sterling and most other major currencies as selling in global shares highlighted growing risks from the pandemic that has shown little sign of easing.
 The dollar's status as the world's reserve currency makes it a natural safe haven, as evidence builds of a massive global economic downturn. 'We saw broad dollar-buying flows into month-end, but today's flows are more haven-like,' said Erik Bregar, head of FX strategy at Exchange Bank of Canada in Toronto.
 The game changer, he added, was U.S. President Donald Trump's dire press briefing late Tuesday, where he warned Americans of 'painful' two weeks ahead in fighting the coronavirus even with strict social distancing measures. White House coronavirus coordinator Deborah Birx displayed charts demonstrating data and modelling that showed an enormous jump in deaths to a range of 100,000 to 240,000 people from the virus in the coming months. 'It feels like the U.S. is not doing enough to curb the spread,' Bregar said.
 The arrival of Spring means the start of seasonal allergies - a runny nose, coughs and streaming eyes... but there are many differences with the Covid-19 virus.
 Coronavirus: What are differences between Covid-19 symptoms and common allergies?
 The earlier new from BAT comes as companies and institutions from countries around the globe are racing to get an effective vaccine approved. Including in Wuhan, where this is all believed to have begun...
 Coronavirus: vaccine human trials on Covid-19 begin in China
 British American Tobacco working on vaccine
 Tobacco producer releases statement in surprise move:
 - Potential vaccine in development for Covid-19 using new, fast-growing tobacco plant technology – pre-clinical testing under way
 - Tobacco plants offer the potential for faster and safer vaccine development compared to conventional methods
 - Potential to manufacture 1-3 million doses of vaccine per week
 Full press release
 LaLiga, Premier League, Serie A, Bundesliga, Ligue 1, Champions League
 Remove international fixtures, delay next season...
 International
 UEFA still aiming to complete top leagues and Champions League
 Bored at home during the lockdown? Here are a few ideas to keep you occupied...
 There are going to be a lot of questions asked across every country about how this crisis is being handled. Alyssa here has a look at some of the latest numbers in Spain.
 Coronavirus: how long does it survive on objects and surfaces?
 Remote Putin
 Russian president Vladimir Putin is leading the country from a remote location, said the Kremlin on Wednesday. Last week Putin met with a doctor who has since tested positive for conronavirus.
 Still not 100% official as the All England Lawn Tennis and Croquet Club meet...
 Natives of the Chinese city relay their personal experiences as daily life in the capital of Hubei Province is starting to slowly get back to a semblance of normality.
 The Spanish Ministry of Health have published the figures relating to the Covid-19 pandemic which reflect the past 24 hours. The overall Covid-19 positive cases topped the 100,000 threshold. The full summary from the 1 April briefing is available here.
 Spain has now seen more than 100,000 positive confirmed Covid-19 cases since the outbreak began and the Wednesday fatality figure once again exceeds 800.
 The Italian Minister for Health, Roberto Speranza confirmed this morning that the lockdown in the nation will continue through to Easter Monday (13 April). Italy has seen over 105,000 positive cases and more than 12,000 fatalities since the outbreak of Covid-19.
 This was the promise from French President Emmanuel Macron, adding that "France will invest €4billion in strategic health products including masks and respirators, with the aim of making the country fully and completely self-sufficient by the end of this year."
 This is the view of Jaume Roures (head of rights holders Mediapro) speaking on Spanish radio last night.
 Britain is aiming to increase the number of tests for coronavirus to 25,000 a day by the middle of the month from its present capacity of 12,750 a day, housing minister Robert Jenrick told Sky News on Wednesday.
 Britain is beginning to test medical staff in additional to patients in hospital, but critics have said it is not expanding testing fast enough or wide enough.
 The European football governing body have called all 55 members to a video-conference later this morning where they will endeavour to work on a road map for domestic seasons to be completed. Earlier this week, President Ceferin stated that the organisation was working on an A, B & C plan.
 China: latest
 On Wednesday morning the Chinese Ministry for Health confirmed that one local positive case had been confirmed (in the province of Guangdong) with a further 35 positive cases being diagnoses among international travellers arriving in China from other destinations.
 Vaccine human trials on Covid-19 begin in China
 The pandemic begin in Wuhan, Hubei Province, and there are now hopes that the solution to the global pandemic may also emerge from the same place.
 J-League positive
 Another Covid-19 football related story from Japan with the news that a member of the Vissel Kobe club has tested positive. Andres Iniesta is one of Kobe's leading players.
 The world of football is in mourning with the overnight news that 68-year-old former Marseille president Pape Diouf died in Senegal after contracting the Covid-19 virus. After former Real Madrid president Lorenzo Sanz, Diouf is another high profile member of the global football family to succumb to the Covid-19 virus. 
 Cases-Deaths (1 April)
 USA: total confirmed cases 188,592 / total Covid-19 deaths: 4,055
 Italy: total confirmed cases 105,792 / total Covid-19 deaths:12,428
 Spain: total confirmed cases 95,923 / total Covid-19 deaths: 8,464
 Germany: total confirmed cases 71,808 / total Covid-19 deaths: 775
 France: total confirmed cases 52,128 / total Covid-19 deaths: 3,523
 *source (Worldometer 08:45 CEST)
 Italy to end lockdown in mid May?
 Reports suggest that the Italian government is planning a gradual reopening of the country in mid-May with the focus on preventing fresh coronavirus outbreaks.
 Italy has been the worst-hit country in the world as a result of the Covid-19 outbreak with more than 12,000 deaths from the virus and although there have been some encouraging signs in recent days - 30 March saw the lowest number of new cases in 13 days and the highest number of people recovering from coronavirus since the beginning of the outbreak. 
 After closing its borders last week, the Caribbean island has now decided to issue a temporary ban international flights to Havana and Santiago de Cuba airports. The country will also request that all foreign boats and ships currently in Cuban waters to leave. At present the country has sees 186 cases of individuals testing positive with the Covid-19 virus.
 The White House maintain that potentially 240,000 lives could be lost in the US with President Donald Trump stating on Monday that the nation should expect a “very, very painful two weeks.” US deaths currently top the 4,000 mark.
 Good morning and welcome to our live coronavirus blog. Over the course of the day, we will be bring you all the latest news relating to the Covid-19 virus from around the world on 1 April. At the time of writing, the virus has claimed over 42,000 lives with over 860,000 positive cases currently confirmed around the world.
 0 
 Comentarios        
 Para poder comentar debes estar 
    registrado                
 y haber iniciado sesión. 
 ¿Olvidaste la contraseña?
 Ediciones internacionales
 Apps
 Síguenos
 https://en.as.com/en/2020/04/01/other_sports/1585720755_042746.html
