All coronavirus articles | Chemistry World
 Sitewide message
 Book your free demo and find out what else Mya 4 from Radleys can do
 2020-04-24T15:26:00Z
 The unexplained appearance and dramatic spread of a new form of HIV drug ritonavir hurt patients and cost its makers almost $250 million.
 2020-04-24T13:44:00Z
 Everything changed for a young chemist in Shanghai when Covid-19 hit while he was away visiting family to celebrate the Chinese New Year
 2020-04-23T13:30:00Z
 Accidents involving cleaning products have been on the rise this year, most likely due to the Covid-19 pandemic 
 2020-04-22T14:00:00Z
 A young computational chemist from India, working to finish his PhD in Italy, is getting media attention for his social action during the pandemic
 2020-04-21T14:06:00Z
 Goal is to minimise disruption to students and researchers without putting anyone at unnecessary risk
 2020-04-21T14:00:00Z
 Polish chemist has been able to keep his research at labs in South Korea and Poland remarkably stable during this volatile period
 2020-04-20T13:28:00Z
 Staying on track means learning from past outbreaks
 2020-04-17T12:34:00Z
 How a drug that prevents HIV from donning its protective ‘coat’ now makes up one arm of the World Health Organisation’s Covid-19 trial
 2020-04-17T12:21:00Z
 University of Toronto quantum computing guru says the computational chemists are ‘not in a normal state’, in Canada or anywhere
 2020-04-17T10:18:00Z
 What we know about the biocides that attack the virus, and how you should use them
 2020-04-17T08:28:00Z
 Mapping out drug discovery routes with artificial intelligence
 2020-04-16T13:30:00Z
 Research that might have taken years is being turned around in months as journals fast-track Covid-19 manuscripts
 2020-04-15T14:01:00Z
 The Spanish scientific superstar is at home, but busier than ever trying to get researchers access to Europe’s supercomputers to fight Covid-19
 2020-04-15T13:30:00Z
 Universities and research labs are being brought in to help with testing but 100,000 tests per day by the end of April still looks overly ambitious 
 2020-04-14T14:00:00Z
 As Japan declares an emergency state, a Romanian team leader at Riken uneasily shuts down his lab
 2020-04-14T09:29:00Z
 How chemists around the world are coping with life and work during the Covid-19 pandemic
 2020-04-09T14:31:00Z
 Mauro Ferrari walks away following ‘unanimous rejection’ of coronavirus response plan
 2020-04-09T13:36:00Z
 A former space-travelling chemist finds she has to rejig her work here on Earth as a science communicator
 2020-04-09T10:12:00Z
 CAS collection hosts 50,000 compounds with potential to treat coronavirus infections
 2020-04-09T08:52:00Z
 The Wellcome Trust’s Covid-Zero initiative calls on businesses to bridge the funding shortfall
   document.write(new Date().getFullYear());
 Registered charity number: 207890
 Site powered by Webvision Cloud
 https://www.chemistryworld.com/coronavirus/100140.tag
