U.S. deaths top 16,000, Boris Johnson out of intensive care
 
 This live coverage has ended. Continue reading Apr. 11 Coronavirus news.
 The coronavirus death toll across the U.S. continues to climb and passed 18,500 by Friday evening, according to an NBC News tally. The number of confirmed coronavirus cases in New York state had reached 170,512.
 Globally, the number of cases passed 1.6 million with more than 102,000 deaths, according to Johns Hopkins University, as countries deliberate over further lockdown measures or worry about second wave outbreaks. Millions of people around the world are preparing for religious celebrations and a holiday weekend.
 Current and former U.S. officials, meanwhile, tell NBC News that American spy agencies collected raw intelligence hinting at a public health crisis in Wuhan, China, in November, but the information was not understood as the first warning signs of an impending global pandemic.
 Download the NBC News app for latest updates on the coronavirus outbreak.
 Dennis Romero
 Burning Man, the end-of-summer music and arts festival in the Nevada desert, has been canceled for 2020 as a result of COVID-19, organizers said Friday.
 The weeklong event on federal land in the Silver State's Black Rock desert is one of several high-profile music gatherings, including Miami's Ultra Festival; SXSW in Austin, Texas; Coachella in Indio, California; and Las Vegas' Electric Daisy Carnival, to cancel or postpone in reaction to the continued spread of coronavirus.
 Marian Goodell, Burning Man project CEO, announced that a virtual version of the festival, which started in the 1980s in San Francisco, will take place in the stead of the colorful antics on the festival's famous Black Rock "playa."
 She offered refunds for the nearly $500 tickets "to those that need" them, but otherwise asked so-called burners to convert their purchases into tax-deductible donations as the nonprofit organization would likely face layoffs.
 Alicia Victoria Lozano
 At least 70 people, including two staff members, at a San Francisco homeless shelter tested positive for coronavirus, Mayor London Breed announced Friday.
 The MSC South shelter is the largest in the city and serves 340 people. Capacity has been reduced to 100 residents, Breed tweeted. People who have not tested positive for the virus will be moved to hotel rooms while those with COVID-19 will remain in quarantine at MSC South.
 "We've been preparing for situations like this and we're responding," Breed said in a separate tweet. "But it is critical that people continue following the Stay Home Order."
 From the beginning of this pandemic, one of our biggest concerns has been spread in congregate living situations.We've seen this happen in countless other places throughout the country. It is now happening at MSC South, one of our shelters here in San Francisco.
 Last week, Gov. Gavin Newsom announced statewide plans to house some of California's 151,000 people experiencing homelessness in hotels and motels through Project Roomkey. The goal is to secure 15,000 rooms for high-risk individuals living on the street.
 Dartunorro Clark
 The White House will not hold a coronavirus briefing on Saturday, according to President Donald Trump's official schedule released Friday. 
 Over the last few weeks, Trump has held, with the exception of one day, daily briefings at the White House with Vice President Mike Pence, who leads the administration's coronavirus task. The briefings often devolve into jousting with the press over the administration response to the pandemic. 
 Los Angeles County is extending its stay-at-home order until May 15 at the earliest with the possibility of extending it into the summer, officials announced Friday. The order had been set to expire on April 19.
 Public health officials said social distancing has helped flatten the coronavirus curve but more is needed to protect the county's 12 million residents. More than 8,400 residents have contracted COVID-19, according to the County of Los Angeles Public Health Department.
 Public health officials warned Friday that lifting the stay-at-home order now could result in nearly 96 percent of residents being infected.
 The Associated Press
 INDIANAPOLIS — Twenty-four residents of a central Indiana nursing home hit hard by COVID-19 have died, the state’s health commissioner said.
 Sixteen of the residents at the Bethany Pointe Health Campus in Anderson had tested positive for the disease caused by the coronavirus pandemic and the other eight had compatible symptoms, Commissioner Dr. Kristina Box said during a state news briefing on the pandemic.
 Next week, @USTreasury and #IRS will launch the Get My Payment web app where filers can enter bank account information & check the status of their payment to get their money fast! https://t.co/VCBpGD8KXv
 Erika Edwards
 Early research shows an experimental treatment for the coronavirus may help very sick patients improve their breathing, though experts caution more studies are needed before the drug, remdesivir, can be recommended.
 The research, published Friday in The New England Journal of Medicine, looked at 53 coronavirus patients who had been given remdesivir through what’s called "compassionate use."
 In a majority of the patients — 68 percent — doctors were able to reduce the amount of oxygen support needed. What's more, 17 of 30 patients who'd been on ventilators were able to come off of those machines. That's important because COVID-19 patients who need to be put on ventilators appear to be more likely to suffer long-term health consequences, and may have worse outcomes.
 Read the full story.
 Corky Siemaszko
 The Florida pastor who wound up in handcuffs after he defied a local stay-at-home coronavirus order by holding a church service for hundreds of worshippers will be celebrating Easter online with his flock Sunday.
 “Join us ONLINE ONLY at 9:30 AM on Sunday, April 12th, for our Resurrection Sunday service as we celebrate the death, burial, and resurrection of our Lord & Savior, Jesus Christ,” the announcement read.
 https://www.nbcnews.com/health/health-news/live-blog/2020-04-09-coronavirus-news-n1179786