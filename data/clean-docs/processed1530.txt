Coronavirus updates in Central Texas: Travis County coronavirus cases reach 900, three more deaths reported
 KVUE is keeping you updated with the latest coronavirus, also known as COVID-19, news in the Austin area.
 Scroll down for the top headlines and latest updates in KVUE's April 14 live blog.
 Top Headlines:
 Updates:
 10 p.m. – The Texas Tribune reports that twice as many Texas families filed for food stamps in March 2020 than in March 2019. 
 6:15 p.m. – Confirmed coronavirus cases in Travis County have reached 900. Three more deaths were reported, bringing total to 14.
 5:45 p.m. – Caldwell County reported its seventh case of COVID-19 in the county on April 14. Batsrop County's number rose to 34 confirmed cases. 
 RELATED: LIST: Confirmed Central Texas coronavirus cases by county
 5 p.m. – As of April 14, Hays County is reporting 59 active cases of COVID-19.
 While the total number of lab-confirmed cases is now 103, 43 patients have recovered. The county has had one COVID-19 death, a woman in her 80s who had been living with a relative in Buda.
 All cases reported so far have been adults, except for three cases, one reported in the 0-9 age range and two in the 10-19 age range. As of April 14, the Hays County Local Health Department has received 604 negative test results.
 Hays County officials announced on March 31 it had launched an online dashboard online which keeps track of the county's COVID-19 numbers. The online tool, along with other important information about the response to the COVID-19 crisis, is available here: www.sanmarcostx.gov/covid19info. County residents may visit https://hayscountytx.com/covid-19-information-for-hays-county-residents/.
 2:27 p.m. – At a Travis County Commissioners Court meeting, the commissioners voted to approve a hiring freeze for Travis County employees in order to ease the economic setback caused by the coronavirus pandemic.
 2:05 p.m. – Sen. John Cornyn announces that several Central Texas colleges will be receiving $84,147,616 in federal grants due to the coronavirus. Funding comes from the Higher Education Relief Fund authorized by the CARES Act. At least 50% of each grant must go toward students with emergency financial aid grants to help cover expenses related to campus operations. The act allows each college discretion on how to award this assistance to students.
 1:50 p.m. – U.S. Rep. Lloyd Doggett (D-Texas) announces that $58,735,130 secured in the recently passed CARES Act is on its way to assist the Austin-Bergstrom International Airport.
 “While it is no panacea for hard times our local airports are facing, this funding will help prevent Austin-Bergstrom from any freefall and maintain planned payroll as well as safety and construction work,” said Rep. Doggett. “This investment allows local airports flexibility to deploy resources as needed. But so much more needs to be done; until we resolve our health care crisis, we cannot resolve our worsening economic crisis.”
 This relief will help the airport with payroll support, construction and other safety measures.
 1:18 p.m. – Two more bus operators with Capital Metro have reported that they tested positive to the agency, according to CapMetro. One of them last worked on April 2 and the other operator last worked April 9. CapMetro said, "we have implemented several measures that ensure social distancing on our vehicles, including a yellow chain that serves as a physical barrier between operators and customers, free fares and rear-door only boarding."
 After four bus operators and one mechanic with CapMetro previously reported positive tests, this brings the total to seven.
 12:55 p.m. – Austin Mayor Steve Adler said in a press conference that the Travis County community must follow and enforce the new stay-at-home order.
 You can watch the full press conference on KVUE's YouTube channel:
 12:09 p.m. – Williamson County announced five new positive COVID-19 cases on Tuesday, bringing the total to 124. So far, 72 people have recovered in Williamson County. 
 10:30 a.m. – After the "stay home" order was extended for Austin and Travis County to May 8, Austin Mayor Steve Adler, Travis County Judge Eckhardt and Dr. Mark Escott, interim Austin-Travis County health authority are set to hold a press conference about the order at noon April 14.
 WATCH: Gov. Abbott announces $50M in small business loans in Texas
 MORE CORONAVIRUS COVERAGE: 
 https://www.msn.com/en-us/money/realestate/coronavirus-updates-in-central-texas-what-to-know-tuesday/ar-BB12Bru1
