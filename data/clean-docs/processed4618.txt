Chipotle Digital Sales Jump 80.8 Pct | PYMNTS.com
 Amid rocketing digital sales and a growing loyalty program, Chipotle Mexican Grill reported better-than-expected earnings on Tuesday (April 21).
 Digital sales at the quick-service restaurant (QSR) chain jumped 80.8 percent and accounted for 26.3 percent of sales for the first quarter of 2020.
 CEO Brian Niccol said on an earnings call with analysts, “As people started to implement social distancing, we moved swiftly by driving further investments towards digital and delivery design to reduce friction while increasing convenient access.”
 The company also announced a national delivery partnership with Uber Eats during the quarter, which Niccol said is helping to drive new diners in a higher frequency.
 While Niccol indicated that delivery is still the quickest growing part of Chipotle’s digital platform, he said the company is also pleased with its order-ahead business that has seen average daily sales increase by two times from pre-coronavirus levels.
 Niccol also noted that daily signups for the restaurant chain’s rewards program increased almost four times over the past month, which he said “is another sign that our digital platform is gaining traction.”
 The executive indicated that 65 percent of newly enrolled rewards members are new to the chain’s brand, which is up from 51 percent before the coronavirus. He also pointed out that 61 percent of previously store-only rewards members are new to digital compared to 8 percent before the coronavirus.
 Niccol indicated that the company is starting to harness its increasing install base with personalized offers to “better engage and incent behaviors.”
 He noted that the company is seeing small transaction increases throughout all frequency bands and that it foresees the lever turning into “a bigger driver in the future as we gain greater customer insights while continuing to expand our digital ecosystem.”
 The Road Ahead
 Niccol said on the call that no one can forecast the scope or length of the crisis. However, he noted that the company is “focused on winning today while we cultivate a bright future for our employees, guests, communities and shareholders.”
 But Niccol noted that the company could weather the current downturn by remaining calm and working to harness its formidable brand, balance sheet, and business model. He said the chain is continuing to “judiciously invest” in pivotal aspects of its business so it can come out stronger on the other side.
 Chipotle Mexican Grill reported revenue of $1.4 billion and adjusted diluted earnings per share excluding charges of $3.08 compared to analyst’s estimates of $1.4 billion and $2.90 per share for Q1 2020, according to CNBC.
 Shares in the company were up approximately 5.5 percent just before 7 p.m. Eastern Daylight Saving Time.
 ——————————
 LATEST PYMNTS REPORT: B2B API TRACKER
 Glitches such as those arising from faulty application programming interfaces (APIs) can lead up to 88 percent of app users to abandon them, making it imperative for banks to constantly monitor their APIs’ performance. In the May 2020 B2B API Tracker, PYMNTS spoke with Skandinaviska Enskilda Banken AB executives Robert Pehrson and Paula da Silva about how harnessing internal data and third-party data from ratings institutes and government agencies can maintain API performance and offer a seamless banking experience.
 Get our hottest stories delivered to your inbox.
 https://www.pymnts.com/earnings/2020/chipotle-digital-sales-jump-80-percent-amid-delivery-loyalty-expansion/
