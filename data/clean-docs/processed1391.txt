The coronavirus is amplifying the bias already embedded in our social fabric - The Washington Post
 There’s an old saying that has circulated in black communities for as long as I can remember. “When white America catches a cold, black folks catch pneumonia.” So, what’s the analogy now that all of America is in danger of catching covid-19?
 The answer that’s emerging is devastating. Covid-19 is not an equal opportunity disease. Right now, black America is uniquely in the bull’s eye of this pandemic.
 Blacks comprise 32 percent of Chicago’s population but nearly 70 percent of covid-19 deaths.
 Blacks comprise 26 percent of Milwaukee’s population but account for 73 percent of covid-19 deaths.
 Blacks account for 40 percent of covid-19 deaths in Michigan even though they represent just 14 percent of the state’s population.
 In Louisiana blacks make up 32 percent of the state’s population but 70 percent of those who have died because of the virus.
 The disparities should not have been surprising. It should have been clear that a crisis of this magnitude would stretch and snap the social safety nets that were already in bad shape. It should have been obvious that communities with higher concentrations of diabetes, heart disease, high blood pressure and asthma would be devastated by covid-19.
 Full coverage of the coronavirus pandemic
 And it should have been obvious that the usual disparities in our health-care system — where blacks and all people of color have poorer health outcomes, lower access to insurance and higher mortality rates — would create a series of localized catastrophes within a national crisis.
 The concentration of these numbers is so overpowering that death will not be random and rare. There will be families and churches with multiple losses. Trauma on a massive scale.
 I say this not to wag a finger — we don’t have time for that. I say it to ask what is being done now to inform and protect the communities at the center of this pandemic.
 Collecting, analyzing and disclosing racial data on testing, treatment and deaths is just a start. The Centers for Disease Control and Prevention moved relatively quickly to provide guidance for long-term care facilities and nursing homes because of early and frequent reports of infection in those places. It should follow the same course for other potential hot spots where people are still venturing into the workplace daily: warehouses, transportation hubs and high-density, low-income communities.
 We are asking a lot of the people who are making our isolation possible. You might be confined to your home, but who do you think is picking the produce you still enjoy at dinner and unpacking the latest shipment at your local pharmacy? Who is working in all those nursing homes? Who is driving the buses and garbage trucks? Think about the passengers who still have to get across town to those jobs as mass transit scales back (to what are unironically known in Washington, D.C., as “lifeline” routes.)
 If America cannot provide masks and goggles and protective equipment for front-line doctors and nurses, what do you think is happening for the thousands of back-end staff who keep hospitals running? The custodians and cooks? The health aides and lab techs? The security guards and maintenance staffs?
 The novel coronavirus is exposing and amplifying the bias that was already embedded in our social fabric. We must do more to protect people who get up and get out of the house every day right now to keep at least some of the cogs running our hobbled economy.
 When asked about the disproportionate impact on African Americans during Tuesday’s White House briefing, Anthony S. Fauci, director of the National Institute of Allergy and Infectious Diseases, said there is “nothing we can do about it right now,” except to try to give black covid-19 patients the best possible care.
 With all due respect to the good doctor, there are some things that can be done now.
 You can’t extinguish a fire unless you know where it is burning. An effort should be made to close the gaps in testing in virus-ravaged communities so people can protect themselves, their families and their co-workers. Since access to health care is limited in black communities, that might require creating mobile rapid response teams that can move as needed to localized hot zones.
 The Opinions section is looking for stories of how the coronavirus has affected people of all walks of life. Write to us.
 We need a coordinated effort to make sure that people who live in food deserts have access to what they need to keep themselves healthy and keep their homes safe. In areas where the virus is concentrated, food banks and meal distribution plans could also dispense basic cleaning products, which are already in short supply in grocery stores.
 And there should be targeted awareness campaigns to swat down any kind of misinformation that leads people to minimize the threat, mistakenly thinking that young people are immune or the virus mainly claims older white victims.
 There are those who will say this should not be about race. They are wrong. It already is.
 Read a letter in response to this piece: A pandemic doesn’t recognize borders. It’s a problem for everyone.
 Michele L. Norris: The writer of ‘Contagion’ imagined all of this — except the inept government response
 Michele L. Norris: Our children are watching us closely now
 The Post’s View: Keeping our hospitals afloat means supporting all hospital workers
 Daniel S. Harawa and Ben Miller: D.C. must protect its inmates from the coronavirus
 Spencer Overton: The CDC must end its silence on the racial impact of covid-19
 Live updates: The latest in the U.S. and abroad
 More news today: How false hope spread about hydroxychloroquine as treatment for covid-19
 How to help: Your community | Seniors | Restaurants | Keep at-risk people in mind
 Share your story: Has someone close to you died from covid-19?
 Follow the latest on the outbreak with our newsletter every weekday. All stories in the newsletter are free to access.
 https://www.washingtonpost.com/opinions/2020/04/08/this-is-not-an-equal-opportunity-virus/
