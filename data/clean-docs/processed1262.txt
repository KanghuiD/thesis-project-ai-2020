Rattled government re-launch blog in response to Sunday Times expose
 The Department of Health has re-launched its blog as the government rolls out an unprecedented response to a Sunday Times expose. 
 Boris Johnson faced calls to resign yesterday after a damning report revealed that the Prime Minister had skipped five Cobra meetings on the coronavirus outbreak. 
 He was also singled out for contributing to a “lost” five weeks from January 24 which allowed Britain to “sleepwalk into disaster”.
 Michael Gove hit out at the reports yesterday, dubbing criticism of Mr Johnson as “grotesque”.
 The long-time ally of Mr Johnson went Sky News’s Sophy Ridge to defend the PM, later telling BBC One’s The Andrew Marr Show that “most Cobra meetings don’t have the Prime Minister attending them”. 
 But Shadow health secretary Jonathan Ashworth later argued Mr Gove’s line that one or two aspects of The Sunday Times story were “off beam” is “possibly the weakest rebuttal of a detailed expose in British political history”.
 The Department of Health posted on its blog for the first time since 2018 yesterday, directly responding to the article in an unprecedented move. 
 A Government spokesman said: “This article contains a series of falsehoods and errors and actively misrepresents the enormous amount of work which was going on in government at the earliest stages of the Coronavirus outbreak.”
 It concluded that “the Prime Minister has been at the helm of the response to this, providing leadership during this hugely challenging period for the whole nation.”
 Related: Government support for businesses needs to become ‘faster and simpler’ – BCC
 Jack is a business and economics journalist and the founder of The London Economic (TLE).He has contributed articles to The Sunday Telegraph, BBC News and writes for The Big Issue on a weekly basis.Jack read History at the University of Wales, Bangor and has a Masters in Journalism from the University of Newcastle-upon-Tyne.
 Website 
 TheLondonEconomic.com – Open, accessible and accountable news, sport, culture and lifestyle.
  Read more
 Editorial enquiries, please contact: jack@thelondoneconomic.com
 Commercial enquiries, please contact: advertise@thelondoneconomic.com
 We do not charge or put articles behind a paywall. If you can, please show your appreciation for our free content by donating whatever you think is fair to help keep TLE growing and support real, independent, investigative journalism.
 https://www.thelondoneconomic.com/politics/rattled-government-re-launch-blog-in-response-to-sunday-times-expose/20/04/
