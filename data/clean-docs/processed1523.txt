New York Post Editorial Board: Coronavirus -- Congress needs to send the MTA another $4 billion in aid | Fox News
       This material may not be published, broadcast, rewritten,
       All market data delayed 20 minutes.
     
 Dr. Mehmet Oz weighs in on aggressive antibody testing and new data suggesting New York is past the coronavirus high point
 Get all the latest news on coronavirus and more delivered daily to your inbox. Sign up here.
 Congress needs to heed MTA Chairman Pat Foye’s plea for another $4 billion in federal aid. The coronavirus crisis has crushed the mass-transit agency’s finances, with roughly $8 billion in toll, tax and fare revenues likely lost this year.
 Since the pandemic hit, subway and bus ridership is down more than 90 percent, while bridge tolls have dropped 62 percent.
       Subway riders, wearing personal protective equipment due to COVID-19 concerns, step off a train, Tuesday, April 7, 2020, in New York.
       (AP Photo/John Minchillo)
 The MTA is crucial to the functioning of the nation’s most important city — and so will be vital to the nation’s post-coronavirus economic recovery.
       
 Without a major influx of cash, the MTA will have to hike fares and cut service, as well as delay much of its $51.5 billion five-year modernization plan. It’s already facing new costs in the hundreds of millions of dollars for regular disinfection of its rolling stock.
 https://www.foxnews.com/opinion/coronavirus-congress-mta-billion-aid
