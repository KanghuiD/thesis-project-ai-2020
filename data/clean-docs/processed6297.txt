Coronavirus: New Zealand's response to pandemic far better than most countries | Stuff.co.nz
 OPINION: If the only economics you ever learned was from watching Ben Stein's lecture in the classic film Ferris Bueller's Day Off, you'll still have learned something awfully important for the economic recovery from the pandemic.
  And it's a lesson that New Zealand and Singapore have taken to heart - as evidenced by the trade agreement Trade Minister David Parker struck last week. The government deserves kudos for it.
  Economic projections are rather grim, with forecasters looking not to the 2009 Great Financial Crisis as precedent but rather all the way back to the Great Depression.
  And the Great Depression provided one very big lesson about what not to do.
  In Ferris Bueller's Day Off, actor Ben Stein was asked to provide an incredibly boring lecture on economics.
  Off the cuff, he delivered one. He told the rather inattentive class that the Republican-controlled House of Representatives in 1930 raised tariffs in an attempt to ("anyone?... anyone?") alleviate the effects of the Great Depression, and that it instead sank America deeper into crisis.
  A lot of things contributed to the Great Depression lasting longer than necessary, including contractionary monetary policy and panicky responses by Congress that kept changing the rules of the game for firms trying to recover.
  But protectionist responses by the US and others also certainly did not help.
  Any one country trying them will quickly find that tariffs meant to protect domestic steel producers, for example, ruin domestic industries that use steel. And when everyone turns protectionist, the complex international supply networks that deliver us everything from cars to phones seize up.
  Earlier this month, the American government began seizing shipments of medical equipment destined for foreign countries. It held up the export of masks ordered from 3M by the public health system in Ontario, Canada.
  Germany called America's seizure of masks destined for Berlin police officers "an act of modern piracy."
  Meanwhile, individual American states trying to import critical medical equipment from abroad have had to do so in secret, to avoid equipment being seized by their federal government.
  It is utter madness – and especially when we remember that masks produced in the US use paper pulp specially produced by Harmac Pacific, in Nanaimo, Canada.
  America and Canada are both better off when Canada can export pulp to America and America can send masks back.
  If Canada banned the export of pulp, to try to build up domestic manufacturing of masks, and America banned the exports of masks, neither would have any masks until 3M figured out a different supply of pulp and until Canadian mask suppliers started to come on-stream.
  And both would wind up with fewer masks over the longer term.
  New Zealand has laudably been standing against this madness.
  In late March, New Zealand and Singapore announced a commitment to maintaining open supply chains and removing trade restrictions on essential goods, especially medical supplies. Australia, Brunei, Canada, Chile, and Myanmar also signed up.
  Last week, David Parker announced a joint declaration with Singapore of what that commitment would mean for both countries, including a list of over 120 products that both countries consider essential for tariff removal.
  The Singaporean press welcomed the move, with the Straits Times reporting on how the agreement allows Singapore to diversify its sources of food supplies. That especially matters for Singapore, as Malaysia is not the most reliable of neighbours.
  Importantly, the agreements are based on a principle of open plurilateralism. Any other country can join simply by signing on.
  These kinds of agreements are vital. They not only help to ensure that New Zealand will have access to the supplies necessary in fighting the current pandemic, but also provide valuable international leadership during a time of rising protectionism.
  While New Zealand's response to the pandemic has been far from perfect, it has been far better than that of most other countries.
  More typically ignored in international comparisons and left off of international maps, New Zealand's squashing of the Covid curve has seen us highlighted in the international press.
  Our trade response to the pandemic deserves similar attention.
  As New Zealand increasingly and deservedly draws the international spotlight, Prime Minister Jacinda Ardern could do well in asking other countries to sign on to the agreement her government has come to with Singapore.
  Trump's response to the pandemic has looked far too much like the 1930 Republican Congress's response to the Great Depression: an insular knee-jerk protectionism that only makes things worse both for Americans, and for everyone else. It is a model of what not to do.
  Ardern's government has been providing a vision of a better way. One that does not beggar its neighbours, and itself in the process.
  It is a vision that should prove compelling beyond New Zealand and Singapore. Ardern should appeal for others to join us.
  Eric Crampton is Chief Economist with the New Zealand Initiative
 Stuff
 https://www.stuff.co.nz/business/opinion-analysis/121117834/coronavirus-new-zealands-response-to-pandemic-far-better-than-most-countries
