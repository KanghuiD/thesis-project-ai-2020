U.S. Coronavirus Blog: Fauci's New Projection; Shortages Remain | Across America, US Patch
 This story on coronavirus developments is updated throughout the day with national news and developments from around our network of local Patches. Scroll down for links helpful to day-to-day living and stories on recent developments.
 We're All In This Together: Help Patch Help Our Communities
 Americans have received a welcome message of hope from Dr. Anthony Fauci, the government's top infectious disease expert, about the success of the social distancing measures across the country.
 Fauci said in an interview with Fox News he thinks the number of U.S. deaths from the new coronavirus will be less than the original projection of 100,000 to 200,000. 
 "Although one of the original models projected 100,000 to 200,000 deaths, as we're getting more data and seeing the positive effect of mitigation, those numbers are to be downgraded," Fauci said. "I don't know exactly what the numbers are going to be, but right now it looks like it's going to be less than the original projection."
 Fauci's message coincides with signs of hope coming out of New York, which has been the hardest hit state in the country by the new coronavirus pandemic and may give an indication of the pattern other hard-hit states such as Massachusetts will follow.
 For the fourth day in a row, the state saw a decline in new hospitalizations, ICU admissions and intubations, according to Gov. Andrew Cuomo, indicating the state's social distancing measures have been effective.
 The numbers could be a good omen for states including Massachusetts, where health officials Wednesday rolled out a new point system that hospitals may use to help doctors and nurses determine whom they should save amid scarce medical resources. 
 The Crisis Standards of Care recommends hospitals prioritize treatment "to result in the best possible health outcomes for the population as a whole." Essentially, the point system ranks patients based on who is most likely to benefit from intensive care, giving priority to health care workers, pregnant women and those most likely to survive a COVID-19 diagnosis.
 Massachusetts on Tuesday saw the highest single-day number of deaths yet, 96. On Wednesday, the state announced there were 77 new deaths and 1,588 new cases for a total of 433 deaths and 16,790 cases. 
 While most of the conversation about hospital shortages has revolved around a lack of ventilators and protective equipment for health care workers, yet another new obstacle has emerged in the fight against coronavirus.
 Hospitals, particularly those in hard-hit areas such as New York, are running low on the drugs needed to sedate patients on ventilators to keep them alive. 
 Aside from the glimmer of hope in New York, the state reported on Wednesday its highest daily death toll, with 779 lives lost to coronavirus in a single day. A flattening in the number of cases and hospital admissions is often followed by an increase in deaths, health officials said.
 "What we have done and what we are doing is working and is making a difference," Cuomo told New Yorkers on Wednesday. "But the bad news is actually just terrible."
 NY Breaks Coronavirus Death Record Set The Day Before: Cuomo
 Coronavirus In NYC: Latest Happenings And Guidance
 Now, officials on Wednesday are watching closely to see if the pattern holds, gets better or was a cruel dose of false hope.
 New York City also received its own bit of hopeful news Tuesday, when Mayor Bill de Blasio said the number of new cases in the city is slowing, giving more time to stock desperately needed ventilators. 
 As of Wednesday, 80,204 people tested positive for the virus in New York City and 4,260 died, according to city officials. The updated figures represent a jump of over 5,000 new confirmed cases and 716 new deaths since Tuesday. 
 The signs of possible hope in the coronavirus hub of the United States are backdropped by the somber reality that almost 425,000 people in the country have been confirmed with the virus and almost 14,530 have died as of early Wednesday evening.
 With more information emerging every day, it's become clear that young Americans are also not immune to suffering deadly complications from COVID-19. The Washington Post published a report Wednesday outlining hundreds of people in the United States under the age of 50 who have died from the virus. 
 The United States on Tuesday reported more than 1,800 coronavirus-related fatalities, a new one-day high, with some states still to release their totals.
 The World Health Organization responded Wednesday to Trump's criticism of the organization on Tuesday. Tedros Adhanom Ghebreyesus, the WHO's director-general, said countries should unify or risk worsening the pandemic. 
 Meanwhile, Trump accused a member of his own administration Tuesday of creating a "fake dossier" detailing the shortages U.S. hospitals are experiencing. And his secretary of the Navy resigned for telling sailors that the captain of their Navy ship — who was removed from his command for raising alarms about cases of the new coronavirus — was either "naive" or "stupid." 
 Additionally, Trump removed a Pentagon inspector general who was to make sure the $2 trillion stimulus money was being spent how it should be.
 Votes were still being counted Wednesday for Wisconsin's presidential primary, which took place Tuesday despite the ongoing coronavirus pandemic. Results won't be available until Monday.
 Voters struggled with long lines and 90-minute waits to cast their ballots while also facing the challenge of maintaining at least 6 feet of distance from each other. In Milwaukee, a city of 600,000 people, only five polling places were open to the public, contributing to the long waits and unavoidable crowds. 
 In the state with nearly 2,500 coronavirus cases, Wisconsin Gov. Tony Evers had sought to delay in-person voting, but to no avail — the conservative-leaning court state Supreme Court ruled that Evers did not have the authority to postpone the election.
 Cops and firefighters shared a moment of solidarity with staff at Newark Beth Israel, University Hospital, St. Michael's and East Orange General.
 With the coronavirus pandemic spreading, a Georgia Republican running for Congress is giving away a semiautomatic rifle to one "lucky" supporter who can use it to shoot "looting hordes from Atlanta."
 Since Gov. Larry Hogan issued a stay-at-home executive order March 30, police officers in Maryland have conducted 14,905 compliance checks, arrested 14 people and issued 665 warnings to people who are violating the order.
 A nurse raised more than $12,000 to buy and distribute protective gear for her colleagues. Her hospital suspended her.
 The Port Washington Public Library is using technology to help first responders and hospital workers during the outbreak of the coronavirus.
 Dozens of "souped-up" cars and motorcycles have been doing laps every day since the stay-at-home order emptied the highway, residents say.
 Is hydroxychloroquine, long used to treat malaria and lupus, a "silver bullet" for the new coronavirus? Should Americans wear masks?
 In New York, Steve Scerri is working long days and nights to keep up with the endless march of death. Cremations and burials are up 100 percent, he said.
 Lady Gaga, Elton John, Paul McCartney, Lizzo, Billie Eilish and Priyanka Chopra Jonas are among the One World: Together at Home" special.
 The ventilators are to be delivered to the Strategic National Stockpile by the end of August.
 The University of Washington School of Medicine is looking to pay 25,000 people across the nation to test an outbreak prediction app.
 President Donald Trump says he will consider "putting a hold" on U.S. funding to the World Health Organization, saying "they missed the call" on the coronavirus pandemic.
 This article will be updated through the day; please check back in for updates.
 Read more local news from Across America
 Swipe for more from Patch »
 https://patch.com/us/across-america/u-s-coronavirus-blog-more-hope-ny-trump-threatens-who