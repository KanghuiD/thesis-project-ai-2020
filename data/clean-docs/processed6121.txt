Scientists find 78% of people don't show symptoms of coronavirus — here's what that could mean
                             make our site easier for you to use.
                             We do also share that information with third parties for
                             advertising & analytics.
                         
             by The Conversation
             — 
                         in Syndication
 The COVID-19 pandemic continues to spread, with 1.4 million cases and almost 75,000 deaths reported worldwide as of April 7. To slow down the spread and reduce mortality, governments across the world have put in place social distancing measures. When such measures are lifted, the “flattened epidemic curve” is expected to start rising again in the absence of a vaccine.
 As most testing takes place inside hospitals in the UK and many other countries, the confirmed cases so far largely capture people who show symptoms. But to accurately predict the consequences of lifting the restrictions, we need to understand how many people with COVID-19 don’t show symptoms and to what extent they are contagious.
 Attend Europe’s leading (and most FUN) tech event TNW2020
 A recent study, published in the British Medical Journal, suggested that 78% of people with COVID-19 have no symptoms.
 The findings are in line with research from an Italian village at the epicenter of the outbreak showing that 50%-75% were asymptomatic, but represented “a formidable source” of contagion. A recent Icelandic study also showed that around 50% of those who tested positive to COVID-19 in a large-scale testing exercise were asymptomatic.
 Meanwhile, a WHO report found that “80% of infections are mild or asymptomatic, 15% are severe infections and 5% are critical infections”. Though we don’t know what proportion of that 80% were purely asymptomatic, or exactly how the cases were counted, it again points to a large majority of cases who are not going into hospital and being tested.
 The new BMJ study is seemingly different to the findings of studies from earlier in the pandemic, which suggested that the completely asymptomatic proportion of COVID-19 is small: 17.9% on the Diamond Princess Cruise Ship and 33.3% in Japanese people who were evacuated from Wuhan.
 The new paper is based on collated data that Chinese authorities began publishing daily from April 1 on the number of new coronavirus cases in the country that are asymptomatic. It reports that “a total of 130 of 166 new infections (78%) identified in the 24 hours to the afternoon of Wednesday April 1 were asymptomatic”. They say that the 36 symptomatic cases “involved arrivals from overseas”, quoting China’s National Health Commission.
 The new BMJ data is hugely important as the majority of new information and findings released daily worldwide is from the potentially small proportion of people who have shown symptoms, sought hospital help, undertook a test and tested positive. This is different to previous epidemics such as SARS, where most of the infections were symptomatic and could be traced.
 Ultimately, widespread antibody testing, which is still not imminent, will be able to tell us how many people have already had COVID-19. This will give a better approximation of the total number of infections. This will be important in making decisions on lifting social distancing measures.
 For example, if antibody testing suggests that a large proportion of the population has had COVID-19 already, there is a smaller chance of asymptomatic and undiagnosed cases spreading the infection once restrictions are lifted. But if only a very small proportion of the population has had the infection, then lifting of social distancing measures may have to be delayed until vaccination strategies are ready to be implemented.
 Mathematical modeling allows us to develop a framework in which to mimic reality using formulaic expressions and parameters based on what we know about the virus spread. Models can be refined to replicate known aspects – for example the number of reported infections and deaths due to COVID-19. Such models can then be used to make a prediction about the future.
 Ideally, a mathematical model for infectious disease spread should be based on parameters including the population of susceptible people, those exposed to the virus, those infected by the virus and those recovered from the virus. The group infected by the virus can further be split into asymptomatic and symptomatic population groups that can be modeled separately. But currently, there are large uncertainties around these numbers.
 The new information will be crucial in addressing some of these uncertainties, and developing more robust and reliable modeling frameworks. This is because, although modeling has strong predictive power, it is only as good as the data it uses.
 The data currently being used is from people who have tested positive to COVID-19 infections. And if asymptomatic infections are a large proportion of COVID-19 infections, as the recent estimates seem to suggest, then a number of model parameters potentially need to be refined and reconsidered. We don’t know how many people current models assume to be asymptomatic, but it could be different to the newly suggested 78%.
 Increasing this number would considerably reduce the case fatality rate – the proportion of deaths per number of infections. That’s because, while the number of deaths related to COVID-19 are clearly countable, this new evidence suggests that there are a lot more infections than we thought, with a large proportion asymptomatic.
 3D medical animation still shot showing the structure of a coronavirus. wikipedia, CC BY-SA
 There is also very little information available to estimate the model parameter describing the time it takes for an infection to progress from asymptomatic to symptomatic. One study from Singapore suggested that progression occurs within one to three days. Confirming this will notably change the model predictions.
 So while the new study suggests a large proportion of people may have already had COVID-19, we can’t say this for sure. Ultimately, we need a large blanket antibody testing strategy to confirm it.
 Only then can we discuss whether the UK has reached “herd immunity” – whereby enough people have been infected to become immune to the virus – for this pandemic, and think about relaxing social distancing measures. Hopefully such a test will be available very soon.
 This article is republished from The Conversation by Jasmina Panovska-Griffiths, Senior Research Fellow and Lecturer in Mathematical Modeling, UCL under a Creative Commons license. Read the original article.
 Read next:
         The 5 fundamental steps to getting your startup ready for VCs    
 Stay tuned with our weekly recap of what’s hot & cool by our CEO Boris.
     
                 Sit back and let the hottest tech news come to you by the magic of electronic mail.
             
                 Prefer to get the news as it happens? Follow us on social media.
 1.76M followers
 1M likes
                 Got two minutes to spare? We'd love to know a bit more about our readers.
 Start!
                 All data collected in the survey is anonymous.
 https://thenextweb.com/syndication/2020/04/13/scientists-find-78-of-people-dont-show-symptoms-of-coronavirus-heres-what-that-could-mean/
