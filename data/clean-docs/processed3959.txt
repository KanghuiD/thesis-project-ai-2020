Coronavirus: Teachers in Singapore stop using Zoom after 'lewd' incidents - BBC News
 Singapore has suspended the use of video-conferencing tool Zoom by its teachers, after a "very serious incident" during a home-based lesson.
 Singapore closed its schools on Wednesday in response to a rising number of coronavirus cases.
 But one mother told local media that, during her daughter's geography lesson, obscene images appeared on screen, before two men asked girls to "flash".
 Zoom told the BBC the company was "deeply upset" about the incidents.
 Zoom recently changed its default settings for home-based learning, and issued a guide for teachers to secure their "virtual classrooms".
 Parents told local media the incidents happened in a geography class for first-year secondary school pupils.
 "Home-based learning is supposed to be a safe space," one parent told the Straits Times. "I know it's difficult to manage but as a parent I feel very concerned."
 It's not known how the hackers gained access. Zoom meetings have nine-digit IDs and can, in theory, be joined by any user if they are not protected by the organiser.
 "These are very serious incidents," said Aaron Loh of the government's educational technology division.
 "The Ministry of Education is currently investigating both breaches and will lodge a police report if warranted. 
 "As a precautionary measure, our teachers will suspend their use of Zoom until these security issues are ironed out."
 Mr Loh said the government had "spelt out to all our teachers the security measures they must adhere to", including secure log-ins.
 "We have been deeply upset to hear about these types of incidents," a spokesperson said.
 "Zoom strongly condemns such behaviour and we encourage users to report any incidents of this kind directly to Zoom so we can take appropriate action."
 The company said it had "changed default settings", to "enable virtual waiting rooms and ensure only hosts can share their screens by default".
 It has also set up a guide for setting up and securing virtual classrooms.
 Zoom is a video conferencing tool that went live to the public in 2013.
 But since the Covid-19 pandemic has caused lockdown around the world, usage of Zoom has "ballooned overnight", the company recently said.
 Until last year, its highest number of daily users was 10 million. In March this year, it reached more than 200m daily users. 
 But that rapid growth has caused problems, with meetings hijacked around the world.
 A video conference at a US school was recently disrupted by racist remarks, and a local government meeting in Pennsylvania was targeted with pornography
 In response to the so-called "Zoom-bombing", the company said: "The first rule of Zoom Club: don't give up control of your screen. 
 "You do not want random people in your public event taking control of the screen and sharing unwanted content with the group."
 More recently, the company said it would spend 90 days "dedicating the resources needed to better identify, address, and fix issues proactively".
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-asia-52240251
