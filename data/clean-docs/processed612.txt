Can God Use the Coronavirus for Good?
 God can use the coronavirus for good because God is good. Contrary to what some are saying, the coronavirus is not the judgment of God for the sin of the world. Because of God’s great love for us, Jesus bore not only the judgment of God for sin but also our sickness and disease. 
 The coronavirus (or COVID-19) is wreaking havoc all over the world. Is there any way that God can use it for good? What could be “useful” about a pandemic? How can God use something so destructive?
 To be clear, the coronavirus is not useful in and of itself. The coronavirus is bad. Evil even. So then, how can God use it for good?
 God can use the coronavirus for good because God is good.
 Contrary to what some are saying, the coronavirus is not the judgment of God for the sin of the world.
 Because of God’s great love for us, Jesus bore not only the judgment of God for sin but also our sickness and disease.
 Bad things happen. Jesus reminded us of that with his words: “I have told you these things so that in me you may have peace. In the world you have trouble and suffering, but take courage — I have conquered the world” (John 16:33).
 Jesus knew trouble and suffering were already taking place. Sickness and violence were part of humanity’s history. But Jesus knew it was going to get worse and humanity had a choice to make.
 When bad things happen, like the coronavirus, it reveals what’s inside of a person. People start to talk, and Jesus said, “For the mouth speaks what the heart is full of” (Luke 6:45).
 It starts with talking and then moves into behavior. We’ve seen both good and bad due to this pandemic.
 Some things I’ve heard or seen are:
 Negative Responses
 Positive Responses
 It’s obvious what is good and what is not. What we do with what we see is up to each of us.
 We see evil in the world because people make bad choices. We see good when people make good choices. God uses both.
 Joseph’s brothers made a bad choice by selling their brother into slavery. Joseph chose to believe God was with him and leaned into his faith. God took the awful thing his brothers did and not only saved the nation of Israel but also many other nations as well. Joseph spoke about how God used the bad for good. “You intended to harm me, but God intended it for good to accomplish what is now being done, the saving of many lives” (Genesis 50:20)
 Enemy armies made the choice to wipe out Israel when Jehoshaphat was king. Jehoshaphat chose to take the awful news to God. Then he chooses to believe the prophet Jahaziel to put singers in the front when advancing toward their enemies because the prophet said, “the battle is the Lords.” Not only did God save them but they came away with abundant prosperity — “it took three days to collect it” (2 Chronicles 20).
 God can take whatever hard, difficult, awful, or evil thing going on in our lives (like the coronavirus) and make use of it.
 Because God is good, he always works to bring about good in our lives.
 The Bible says, “And we know that in all things God works for the good of those who love him, who have been called according to his purpose” (Romans 8:28).
 The phrase works together is one word in the Greek — synergei. It’s where we get our word synergy. Simply put, it’s the interaction or cooperation of two or more things creating something greater than the sum of their separate parts.
 The list above where people have done good things is not by any means exhaustive. Keep looking for more!
 Good shines bright in the dark. The innovators, generous, and lovers of God are rising up.
 The Almighty Triune God will use the coronavirus to create new things through synergy.
 This is the poem by the priest mentioned above:
 Lockdown by Brother Richard:
 Yes there is fear. Yes there is isolation. Yes there is panic buying. Yes there is sickness. Yes there is even death. But, they say that in Wuhan after so many years of noise, you can hear the birds again. They say that after just a few weeks of quiet, the sky is no longer thick with fumes but blue and grey and clear. 
 They say that in the streets of Assisi, people are singing to each other across the empty squares, keeping their windows open so that those who are alone may hear the sounds of family around them. They say that a hotel in the West of Ireland is offering free meals and delivery to the housebound. Today a young woman I know is busy spreading fliers with her number through the neighbourhood so that the elders may have someone to call on.
 Today Churches, Synagogues, Mosques and Temples are preparing to welcome and shelter the homeless, the sick, the weary. All over the world people are slowing down and reflecting. All over the world people are looking at their neighbours in a new way. All over the world people are waking up to a new reality. To how big we really are. To how little control we really have. To what really matters. To Love.
 So we pray and we remember that. Yes there is fear. But there does not have to be hate. Yes there is isolation. But there does not have to be loneliness. Yes there is panic buying. But there does not have to be meanness. Yes there is sickness. But there does not have to be disease of the soul. Yes there is even death. But there can always be a rebirth of love.
 Wake to the choices you make as to how to live now. Today, breathe. Listen, behind the factory noises of your panic. The birds are singing again. The sky is clearing, spring is coming, and we are always encompassed by Love. Open the windows of your soul. And though you may not be able to touch across the empty square, Sing.Choose love and feed your faith instead of your fear. 
 Danielle Bernock is an international, award-winning author, speaker, and coach who helps people embrace their value and heal their soul through the power of the love of God. She’s written Emerging With Wings, A Bird Named Payn (now available in audio), Love’s Manifesto and Because You Matter. A long time follower of Christ, Danielle lives with her husband in Michigan near her adult children and grandchildren. For more information or to connect with Danielle https://www.daniellebernock.com/  
 https://www.christianity.com/wiki/god/can-god-use-the-coronavirus-for-good.html
