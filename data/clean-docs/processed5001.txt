	Coronavirus resources and information | Congresswoman Debbie Dingell
 Quick Links:
 Michigan Governor Gretchen Whitmer ordered “Stay Home, Stay Safe” effective 12:01 am on March 24, 2020 to stay in place until at least April 30, 2020. Individuals may only leave their home for a limited number of activities. 
 YOU CAN:
 YOU MAY NOT:
 BUSINESSES THAT REMAIN OPEN FOR IN-PERSON WORK MUST TAKE AGGRESSIVE STEPS TO MINIMIZE THE VIRUS’S SPREAD. THEY MUST: 
 Further, to maximize public health: 
  If you think you might be sick
 STAY HOME except to get medical care 
 SEPARATE yourself from others, including family
 CALL YOUR DOCTOR before seeking care
 PRACTICE GOOD HYGIENE by washing hands frequently, covering your mouth with a tissue or your elbow when coughing 
 SEEK MEDICAL CARE if your symptoms worsen, such as difficulty breathing
 COVID-19 Tests
 Call your doctor:  
 If you think you have been exposed to COVID-19 and develop a fever and symptoms, such as cough or difficulty breathing, call your healthcare provider for medical advice. They will decide whether you need to be tested, but keep in mind that there is no treatment for COVID-19 and people who are mildly ill may be able to isolate and care for themselves at home.
 The following symptoms may appear 2-14 days after exposure: 
 Fever 
 Cough
 Shortness of breath
  Unemployment Insurance
 More laid-off and furloughed workers than ever before (including those new to the job market) will be eligible for Unemployment Insurance and will see an additional $600 per week to match the average paycheck for up to 4 months of benefits. These benefits are available immediately.
 The Coronavirus Aid, Relief, and Economic Security Act (CARES Act) provides unemployment for the self-employed and so-called gig economy workers. If you applied for unemployment benefits previously, you DO NOT need to reapply at this time. 
 Residents seeking more information about UI benefits should contact the state unemployment insurance program. 
 To apply for unemployment benefits: visit http://www.Michigan.gov/UIA or over the phone at 866-500-0017
 To file you need:
 Social Security number 
 (or Non-citizens Alien registration and the expiration date of your work authorization card)
 Employment information for the past 18 months: employer name and address, first/last day worked/gross earnings
 Your address, phone number, date of birth
 Driver’s License or State ID
 Michiganders are asked to use the tools and resources available on the homepage of the website to help answer any outstanding questions they may have. Due to the unprecedented demand for unemployment benefits, those using the phone option may sometimes receive a busy signal and those using the website should expect longer load times. Users are asked to be patient and not click more than once to reload a page. Filing online remains the fastest way for Michiganders to apply for unemployment benefits.
 Online Filing Schedule - Michigan.gov/UIA
 Last names beginning with letters A-L are asked to file claims on Mondays, Wednesday, Fridays.
 Last names beginning with letters M-Z are asked to file claims on Sundays, Tuesdays, or Thursdays.
 Saturdays will be available for anyone to accommodate those who could not file during their allotted window.
 Call Center Filing Schedule - 866-500-0017:
 Last names beginning with letters A-L are asked to call on Mondays and Wednesdays between  8:00am – 5:00pm.
 Last names beginning with letters M-Z are asked to call on Tuesdays and Thursdays between  8:00am – 5:00pm.
 Fridays (8:00am – 5:00pm) will be available for anyone to accommodate those who could not file during their allotted window.
 The day or time of day in which a claim is filed will not impact whether a worker receives benefits or their benefit amount. Additionally, claims will be back-dated to reflect the date in which a claimant was laid-off or let go from their job due to COVID-19. The eligibility window to apply has also been increased from 14 to 28 days from the date of their work stoppage.
 For more information visit Michigan.gov/UIA 
 Direct Payments to Individuals & their Families
 Individuals making up to $75,000 ($150,000 for married workers) will receive payments of $1,200 with an additional $500 payment per minor child. The payments decrease ratably and stop altogether for single workers making more than $99,000 ($198,000 for married workers and $218,000 for a family of four.)
 These payments will be issued by the IRS via direct deposit and will be based on 2019 or 2018 tax return or 2019 Social Security statement.
 If you receive veterans disability compensation, a pension, survivor benefits from the Department of Veterans Affairs, or your income level does not require you to file a tax return, then you need to submit information to the IRS to receive an Economic Impact Payment. More information and directions are available by clicking the link here: www.irs.gov/coronavirus/non-filers-enter-payment-info-here
 TIMING OF PAYMENTS:
 The IRS will make about 60 million payments to Americans through direct deposit in mid-April (likely, the week of April 13th). The IRS has direct deposit information for these individuals from their 2018 or 2019 tax returns. This will include Social Security  beneficiaries who filed federal tax returns that included direct deposit information.
 Shortly (hopefully within 10 days) after the first round of payments are made in mid-April, the IRS plans to make a second run of payments. These payments will be made to SS beneficiaries who did not file tax returns in 2018 or 2019 and receive their Social Security benefits via direct deposit. (The estimates are that nearly 99 percent of SS beneficiaries who do not file a return receive their SS benefits through direct deposit.)  
 About 3 weeks after the first round of payments are made (the week of May 4th), the IRS will begin issuing paper checks to individuals.
 The paper checks will be issued at a rate of about 5 million per week, which could take up to 20 weeks to get all the checks out.  
 The checks will be issued in reverse “adjusted gross income” order—starting with people with the lowest income first.
 The IRS is encouraging taxpayers to file their 2019 returns to the maximum extent possible.  As taxpayers file their 2019 returns electronically, the IRS will post updated tax information weekly to its files and then send this information to another agency that will issue weekly payments. 
 The IRS expects to create a portal by the end of April/early May that will allow taxpayers, once they have been authenticated, to: (1) find out the status of their rebate payment and (2) update direct deposit information.  
 For Social Security  beneficiaries who do not file returns, Treasury and the IRS announced that these beneficiaries will not need to file a “simple tax return” to receive their rebate.  Recipients will receive their rebate just as they would their Social Security benefits.  
 For other taxpayers who do not file returns, the IRS expects to release the “simple tax return” referred to in a recent IRS News Release “soon.”  The IRS expects it will contain only a few questions, including name, SSNs, dependents, and deposit information.  There also will be other IRS guidance accompanying this simple tax return.
 Paying Taxes
 The IRS announced that deadline to file your taxes has been extended to July 15. Please share this important update with your family, friends, and colleagues. 
 The IRS recently went live with a website that provides tips for taxpayers and tax pros. You can find that site here.  
 The IRS also recently pushed FAQs regarding this year’s tax filing deadline extension. You can find that site here. 
 Governor Whitmer extended all April 2020 state and city income tax filing deadlines in Michigan to July 2020. State of Michigan and cities with income taxes due on April 15 will now be due on July 15, while cities with income taxes due on April 30 will now be due on July 31. More information on Michigan taxes available at www.Michigan.gov/taxes 
 Social Security
 Residents experiencing issues with Social Security should reach out to Dingell's office at 313-278-2936.
 Food Assistance
 My office keeps an updated list of Food Banks and Kitchens in Michigan's 12th Congressional District  
 Michigan Department of Education Food Service Program Interactive Map
 To find food assistance near you, call the USDA National Hunger Hotline 1-866-3-HUNGRY or 1- 877-8-HAMBRE
 Supplemental Nutrition Assistance Program (SNAP), more commonly known as food stamps, ensures that Americans receive the food they need, especially if they are newly unemployed. Congress has invested in SNAP in the last three bills to help Americans put food on the table during this crisis.
 In order to apply for SNAP, use MI Bridges to apply for assistance, check your eligibility status, and manage your account online. 
 Homeowner & Renters Protections
 Mortgage Forbearance: Homeowners with FHA, USDA, VA, or Section 184 or 184A mortgages (for members of federally-recognized tribes) and those with mortgages backed by Fannie Mae or Freddie Mac have the right to request forbearance on their payments for up to 6 months, with a possible extension for another 6 months without fees, penalties, or extra interest. Homeowners should contact their mortgage servicing company directly.
 Eviction Protections: Renters residing in public or assisted housing, or in a home or apartment whose owner has a federally-backed mortgage, and who are unable to pay their rent, are protected from eviction for 4 months. Property owners are also prohibited from issuing a 30-day notice to a tenant to vacate a property until after the 4-month moratorium ends. This protection covers properties that receive federal subsidies such as public housing, Section 8 assistance, USDA rural housing programs, and federally-issued or guaranteed mortgages. Renters whose landlord is not abiding by the moratorium should contact the relevant federal agency that administers their housing program or their local Legal Aid office.
 Energy Assistance for Low Income Families
 The Coronavirus Aid, Relief, and Economic Security Act (CARES Act) includes $12 million specifically to Michigan to help low income Americans and their families heat and cool their homes. To learn more about the Low Income Home Energy Assistance Program (LIHEAP) in Michigan, visit www.michigan.gov/heatingassistance or call 1-855-275-6424.
 Emergency Paid Family and Medical Leave
 Many workers in America currently have no paid leave and are being forced to choose between their paycheck, their health, and the health of the people around them.
 The emergency paid leave provisions passed by Congress and signed into law by the President are a critical step toward protecting families’ financial security and mitigating the spread of the coronavirus.
 U.S. Department of Labor Fact Sheet for Employees
 U.S. Department of Labor Fact Sheet for Employers
 U.S. Department of Labor Questions and Answers
 Workers at companies with fewer than 500 employees are eligible to receive up to 12 weeks of job-protected leave under the Family and Medical Leave Act (FMLA). This leave is available to employees who are adhering to quarantine requirements or medical advice, obtaining a medical diagnosis, and caring for an individual who is sick or in quarantine or for a child whose school or day care has closed due to coronavirus. During this time, workers will be compensated at two-thirds of their normal compensation (after the first two weeks). Employers will receive a tax credit to offset 100% of the costs associated with providing this paid family leave, up to $200 per day. This program will expire at the end of the year. For more information about paid sick leave and paid family and medical leave, visit the IRS webpage.
 Emergency Paid Sick Leave
 Under this new law, workers at companies with fewer than 500 employees are eligible to receive paid sick leave for issues related to coronavirus, including adhering to quarantine requirements or medical advice, obtaining a medical diagnosis, and caring for an individual who is sick or in quarantine or for a child whose school or day care has closed due to coronavirus. During this time, workers are required to be paid at least their normal compensation if they themselves are sick or two-thirds of their compensation if they are providing care to another individual or child. Employers will receive a tax credit to offset 100% of the costs associated with providing this paid sick leave, up to $511 per day if the employee themselves are sick and up to $200 per day if the employee is caring for someone else.
 Stopping Price Gouging
 Michigan Governor Gretchen Whitmer issued an executive order on March 15 to protect consumers against price gouging during the crisis. You can report potential price-gouging to the Michigan Attorney General online here or by calling 877-765-8388.
 Essential Business and Employees
 Many have reached out to me with questions about what’s considering an essential business, what can stay open, and what are my rights as a critical infrastructure employee. For example, a carwash is a non-essential business, but a pharmacy is essential. The Governor’s office and Attorney General’s Office put together a guidance that all businesses must follow, a resource on the rights for critical infrastructure employees, and FAQs that’s useful. Following this guidance will flatten the curve and help people. 
 SMALL BUSINESSES
 Small Business Forgivable Loans and Grants
 Congress secured $350 billion in forgivable loans and $10 billion in grants to small businesses, tribal business concerns, and certain nonprofits.
 Loans through a new SBA 7(a) Paycheck Protection Program can be forgiven when used for payroll costs (including paid leave, health care, and other costs) to retain workers, and other
 expenses such as rent, mortgage interest, and utilities. Independent contractors, sole-proprietors,
 and other self-employed persons are eligible for these loans.
 Small businesses can also apply for up to $10,000 in grants to retain workers and pay for debt obligations.
 Small businesses, tribal business concerns, 501(c)(3) nonprofits, and 501(c)(19) veterans organizations in your district should contact their lender and the Small Business Administration office directly. 
 Michigan SBA District Office
 313-226-6075 
 https://www.sba.gov/offices/district/mi/detroit
 Michigan@sba.gov
 Michigan Small Business Relief Grant
 The Michigan Small Business Relief Grant is live and provides grants of up to $10,000. Qualifying expenses are include payroll, rent, and other operating expenses. This application link applies to Wayne County. Find the link here.
 Paid Sick and Family Leave Tax Credit
 Employers who provide required leave to their employees and who employ less than 500 employees are eligible for a tax credit to offset the costs of such leave. This tax relief will be provided against quarterly payroll taxes (those paid in connection the Form 941). For additional guidance, please refer to the IRS’s website.
 VETERANS
 Veterans Crisis Line 1-800-273-8255
 ANN ARBOR VA: For Veterans in these affected areas who have questions about routine medical care, please contact 734-845-5290 press 2.  For those needing pharmacy refills, please call 866-316-9350 Ext. 1.  For those needing to speak to a medical provider, please call 734-769-7100.  
 DETROIT VA: Veterans, if you have flu-like symptoms such as fever, cough and shortness of breath, call us at 313-576-1500 before you visit your local medical center or clinic to get answers on medical questions/concerns or prescription inquiries.
 VA FAQ on COVID-19 available here
 STUDENTS & EDUCATORS
 Student Loan Relief for Borrowers
 If you have student loan debt, Congress secured several options outlined below for borrowers that help provide relief through September 30, 2020. During this period, a borrower will be able to:
 For additional guidance on how to apply and learn about next steps as this critical relief becomes available, please refer to the U.S. Department of Education website.
 Student Aid FAQ for COVID-19
 School Meals
 As more schools close due to coronavirus, Congress has provided emergency funding for Child Nutrition Programs to ensure students can still receive their free or reduced-price school meals during this time. 
 Stabilization Funding for Education
 Congress secured $30.75 billion in funding for school districts, states and higher education institutions to ensure state resources and investments are not diverted from life-long learning.For additional guidance on how to apply and learn about next steps as this critical relief becomes available, please refer to the U.S. Department of Education website.
 UTILITY AND HOUSING ASSISTANCE
 Utility Shutoffs Suspended 
 The Program: DTE Energy and Consumers Energy announced on March 16 they will suspend electricity and natural gas shutoffs during the coronavirus pandemic.
 Eligibility Requirements: Shutoffs due to non-payment are suspended for people with low incomes and senior citizens. 
 How to Access: DTE customers impacted by COVID-19 — those with a sudden loss of income or medical condition — as well as vulnerable seniors can call 800-477-4747 to determine eligibility for payment assistance. Consumers customers affected by COVID-19 can call 800-477-5050.
 Program End Date: Both companies said their shutoff suspension will last through April 30, and that timeframe could be adjusted. Customers in either company’s Winter Protection Program already had their end dates extended through May 3, without any additional action required. 
 More Information: Learn more about the companies’ response plans online: DTE Energy, Consumers Energy
 Statewide Water Service Restoration
 The Program: Gov. Whitmer issued an executive order on March 28 that requires public water suppliers in Michigan to immediately identify residences in their service area that do not currently have water service and to restore service to homes where the service has been disconnected due to non-payment or damaged infrastructure by April 12. A $2 million Water Restart Grant Program has been established through the Michigan Department of Environment, Great Lakes, and Energy (EGLE) to provide funding to local communities to help reconnect homes to their water supplies. 
 Eligibility Requirements: Everyone in the state is eligible for water restoration, regardless of past-due water bills. This program does not eliminate the obligation of a resident to pay for water, prevent a public water supply from charging any customer for water service, or reduce the amount a resident may owe to a public water supply. 
 How to Access: Contact your water supplier if you need your water service restored. 
 Program End Date: Public water suppliers must restore water to residences by April 12. The executive order remains in effect as long as Michigan is under a state of emergency related to coronavirus.  
 More Information: Read the governor’s press release on the water restoration order. 
 Keep Americans Connected Initiative (internet and phone service)
 The Program: The Federal Communications Commission (FCC) has asked broadband and telephone companies to take the Keep Americans Connected Pledge, which says they will:
 1.	Not terminate service to any residential or small business customers because of their inability to pay their bills due to the disruptions caused by the coronavirus pandemic;
 2.	Waive any late fees that any residential or small business customers incur because of their economic circumstances related to the coronavirus pandemic; and
 3.	Open their Wi-Fi hotspots to any American who needs them.
 Eligibility Requirements: See if your broadband and telephone service provider is among the more than 390 companies and associations who have committed to the Keep Americans Connected Pledge. 
 How to Access: Contact your service provider if you anticipate trouble paying your bill. 
 Program End Date: There is no set end date; the pledge is expected to last for the duration of the coronavirus pandemic.
 More Information: A crowd-sourced list of free and low-cost internet options provides more information.  
 Statewide Moratorium on Evictions
 The Program: Michigan is prohibiting evictions from residential properties, including mobile homes, during the coronavirus pandemic.
 Eligibility Requirements: The eviction moratorium applies to residential properties, but not businesses. 
 How to Access: Residents do not need to take any action in order to avoid eviction. Tenants are still responsible for rent owed. 
 Program End Date: The eviction moratorium took effect March 20, and it will last until 11:59 p.m. April 17. 
 More Information: Read Gov. Whitmer’s executive order prohibiting evictions
 State’s Tax Foreclosure Deadline Extended
 The Program: Michigan has temporarily postponed the tax foreclosure deadline for all residents. The deadline to pay back taxes and avoid foreclosure is extended from March 31 to May 29 or 30 days after the expiration of the coronavirus-related state of emergency, whichever comes first.
 Eligibility Requirements: All Michigan residents are eligible for the extended tax foreclosure deadline.
 How to Access: Property owners do not need to take any additional steps to utilize the extended deadline. 
 Program End Date: The new deadline to pay delinquent taxes and avoid foreclosure is May 29 or 30 days after the expiration of the coronavirus-related state of emergency, whichever comes first.
 More Information: Contact your local county treasurer for more information on property tax payment options. 
 Mortgage forbearance and moratorium on foreclosures and evictions
 The Program: The federal CARES Act allows borrowers with federally-backed residential mortgages to defer payments for 180 days, with the option to request another 180 days of forbearance. Federally-backed mortgages include mortgages insured by the Federal Housing Administration, purchased by Fannie Mae and Freddie Mac, and insured or guaranteed by other federal departments.
 The forbearance period is in addition to a previously announced 60-day moratorium on mortgage foreclosures for borrowers with federally-backed, single-family home mortgages. The CARES Act also places a 120-day moratorium on evictions for properties with federally-backed mortgages, multi-family mortgages and certain housing programs. 
 Eligibility Requirements: The suspension of foreclosures and evictions automatically applies to all eligible properties. The forbearance period is available to anyone with a federally-backed, residential mortgage. 
 How to Access: Homeowners who anticipate trouble paying their mortgage should not simply stop making payments. Contact your servicer/lender to request a forbearance due to financial hardship related to the COVID-19 emergency. If given mortgage forbearance, take steps to ensure you will be able to repay the amount that was reduced or suspended after the mortgage forbearance period.
 Program End Date: Mortgage foreclosures are suspended through May 17, and evictions are suspended for 120 days. The mortgage payment forbearance period is up to 180 days, with the option to request a 180-day extension. During a forbearance, no fees, penalties, or interest beyond the amounts scheduled or calculated as if the borrower made timely contractual payments will accrue on the borrower’s account.
 Dingell Michigan and Washington Office Operations
 Congresswoman Dingell’s offices in Washington, Dearborn and Ypsilanti are on enhanced telework policies to help reduce the risk of spreading coronavirus, but they are still working full time to serve you. 
 Leave a message to be returned to Dearborn, MI office by phone: (313) 278-2936 or her Washington, DC office by phone at (202) 225-407.
 Because of coronavirus, tours of the U.S. Capitol have been halted and access to some public areas of the Capitol complex is restricted.
 Additional Resources
 Homemade Facemasks
 If you are interested in donating handmade face masks, please contact the Michigan -Community Service Commission at COVID19donations@michigan.gov or call 517-335-4925
 https://debbiedingell.house.gov/news/documentsingle.aspx?DocumentID=2253
