Coronavirus Resources: Protecting Your Business and Your People | Woodruff Sawyer
  Client Access
 Client Access
 Insights
  no-author
 April 5, 2020
 Crisis Management
 The coronavirus, or COVID-19, continues to evolve quickly, impacting nearly every aspect of business from supply chains to investor questions to employee health concerns. Woodruff Sawyer has established this Coronavirus Resource Center to provide guidance and answer your pressing questions, such as how to prepare or respond to the associated business risks, and whether insurance coverage may respond.
 Last Updated April 13, 2020:  Added upcoming webinars
 April 17, 2020 @ 10:00 AM – 11:00 AM PDT
 Join us for a live discussion with experts from Woodruff Sawyer, Gibson Dunn, and Concord as we review the effects of the ongoing coronavirus situation on M&A Reps and Warranties exclusions.
 We’ve included a downloadable and editable Guide to Creating a Business Continuity Plan for Pandemics, which includes templates, as well as a sample pandemic plan. Keep in mind that these plans should tie closely with your current business continuity plan.
 COVID-19 has many employers asking questions about what actions to take now to help protect their employees and their business interests. The following resources help you ask the right questions, and provide you with the answers and solutions according to current developments and projections.
 If you are a Woodruff Sawyer client and have specific questions related to your risks and coverage, reach out to your account team.
 ON-DEMAND WEBINAR
 In this live webinar we hosted on March 12, our experts weighed in on how your property & casualty, cyber liability, and management liability policies may respond to coronavirus claims.
 WATCH NOW >>
 On March 26, Woodruff Sawyer’s Retirement Team, alongside Goldman Sachs’ retirement specialists, provided information and updates on how the Coronavirus is impacting retirement plans and defined contributions. Topics included the potential economic relief that could impact 401k plans, what record keepers are saying, and what both plan sponsors and plan participants should be thinking about today.
 On March 19, we hosted this live webinar where Woodruff Sawyer Employee Benefits experts alongside Snell & Wilmer’s Employment Law Partner, Tiffanny Brosnan, covered topics like remote work, legal concerns, extending PTO and sick leave, and the impact on life insurance coverage, claims, and employee anxiety and overall well-being.
 In this March 24th webinar, the Woodruff Sawyer Retirement Plans Team, along with advisors at Franklin Templeton Investments, provided information about the current market and gave perspective based on patterns seen in previous market cycles and downturns. The advice they give helps you avoid common pitfalls and stay on track towards achieving your financial goals.
 In this April 2nd webinar, the Woodruff Sawyer Benefits Team, along with Withum, uncovered the details and impact of the $2 trillion Coronavirus stimulus bill. Topics of discussion included insurance coverage for furloughed or laid-off employees, the cost implications of the stimulus, and how the market is reacting.
 The CDC and World Health Organization are continually providing best practices and updates on the status of coronavirus. Keep in mind that your local Health Department has the authority to control all epidemiological response, including quarantines or other actions that may affect your business, as well as how to respond if an employee has been exposed or tests positive. 
 Refer to these sources regularly to help you plan and protect your workplace according to the latest recommendations.
 CDC COVID-19
 WHO COVID-19
 WHO Daily Situation Reports
 CDC US Case Count and Map
 CDC World Case Map
 Safety Practices for Potentially Exposed Workers
 CDC: Prevention and Treatment
 WHO: Advice for Public
 Federal OSHA Information
 California OSHA Information
 State-Level Health Departments
 Local Health Departments
 California
 Colorado
 Massachusetts
 Oregon
 Washington
 This page will continually be updated, so please check back regularly.
 All views expressed in this article are the author’s own and do not necessarily represent the position of Woodruff-Sawyer & Co.
 
 TTelephone 844.972.6326
 https://woodruffsawyer.com/crisis-management/coronavirus-business-people-risks/