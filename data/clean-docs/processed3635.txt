The economic consequences of the coronavirus pandemic - The Boston Globe
 Easter never felt more Eastery. The world economy looks dead. Can it be resurrected?
 Just over a century ago, amid the worst influenza pandemic in history, the greatest economist of his generation fell ill. John Maynard Keynes was in Paris, attending the peace conference that would ultimately produce the Treaty of Versailles.
 He remained prostrate for close to a week. Did Keynes have the dreaded Spanish flu? If so, he was lucky to survive it. According to the latest estimate, that pandemic killed 39 million people, 2 percent of the world’s population, dwarfing the battlefield fatalities of World War I.
 It was shortly after his recovery and return to England that Keynes wrote the inflammatory tract that made him famous, “The Economic Consequences of Peace.” In it, he deplored the punitive terms of the Versailles Treaty — which imposed on Germany an unspecified but potentially vast war reparations debt — and prophesied an inflationary economic disaster, followed by a political backlash.
 Who among today’s great economists will write “The Economic Consequences of the Plague?” Large parts of the world’s economy have been brought to an abrupt standstill by the COVID-19 pandemic. To contain the contagion, countless businesses have been told to cease trading; millions of workers have been told to stay at home. To offset this supply shock, and to prevent a catastrophic downward spiral of shrinking demand and debt deflation, the world’s central banks and finance ministries are injecting even more liquidity (that’s money to you and me) than they did in the wake of the 2008-09 financial crisis.
 The effects of these measures can be seen in the remarkable performance of the markets for corporate stocks and bonds. As I write, the S&P 500 is just 18 percent below its peak on Feb. 19. At its low point (March 23), it was down 34 percent. Those who anticipated carnage in the low-grade (“junk”) bond market are stunned. Who could have foreseen that the Federal Reserve would buy even junk?
 Yet I feel a bit like Keynes did in 1919. Of course, I see the need to get money to those workers who will likely be unemployed for as long as it takes scientists and pharmaceutical companies to find and distribute a COVID-19 vaccine. But the Fed’s current policy would appear to be a generalized bailout of investors, even those whose positions were known to be risky.
 And Congress has, in great haste and amid frantic horse-trading, passed legislation that commits taxpayers to write down hundreds of billions of dollars of “loans” to businesses large and small. Moreover, the legislation passed in the past few weeks would appear to make more than half of American workers better off being unemployed than they would have been working.
 Turning to some of today’s leading economists, I became more despondent. For the arch-liberal Paul Krugman, this is "the economic equivalent of a medically induced coma,” but the Keynesian remedy of government borrowing can provide the necessary relief and stimulus. “There may be a slight hangover from this borrowing,” Krugman wrote on April 1, "but it shouldn’t pose any major problems.” (Was the date of Krugman’s blog post significant?)
 By contrast, Ken Rogoff — one of Harvard’s few conservative professors — wrote last week of an “economic catastrophe . . . likely to rival or exceed that of any recession in the last 150 years.” The pandemic, Rogoff argued, was akin to an “alien invasion.”
 Larry Summers, who lies somewhere between these two ideologically, chose a more "grisly” metaphor in an interview on Thursday. “Physical isolation is chemotherapy,” he said, “and the goal is remission. The problem is that chemo is . . . increasingly toxic over time.” He foresees an “accordion-like dynamic” until a vaccine is generally available in 12 to 18 months.
 I am with Rogoff and Summers. This is a disaster, the economic consequences of which cannot be offset by even the biggest monetary and fiscal splurge.
 Our estimates at Greenmantle, based on state-by-state assessments that allow for the share of the population that can work from home, suggest a slight drop in GDP for the first quarter of this year (-1.45 percent quarter-on-quarter), followed by a lockdown-driven collapse (-10.8 percent) in the second quarter. We expect a partial rebound of 6.5 percent in the third quarter, as lockdowns are partially relaxed but social distancing measures remain in place — as they must, until a vaccine is available.
 There has been loose talk from some of the banks about a “V-shaped” recovery, a sharp decline in the economy followed by an equally sharp recovery. That prediction was wrong after 2009 and it will be even more wrong in 2020. The shape we have in mind is something like an inverted square root or a tortoise’s back. Certainly, the speed of recovery will be more like a tortoise’s than a hare’s.
 The key is that in the protracted “post-lockdown, pre-vaccine” period, there will inevitably be a reduction of capacity in all sectors of the economy that depend on some level of social proximity, such as retail, air travel, education, live entertainment, hotels, and restaurants.
 An economy without crowds is not a “new normal.” It may be more like the new anomie, to borrow Émile Durkheim’s term for the sense of disconnectedness. For most people, the word “fun” is almost synonymous with “crowd.” The coming year will be a time of depression in the psychological as well as the economic sense.
 Note that our calculations above omit the effects of lockdowns and social distancing on the demand side, including domestic consumption and investment, and the effects of the pandemic on international trade. Panicked people are going to save as much of those checks as they can. Broke businesses will pocket the government cash and still downsize. And don’t get me started on the enduring hit to trade.
 In short, I can’t honestly wish my readers a “Happy Easter.” In the Bible, Christ’s resurrection happens in just three days. The resurrection of the world economy will take a whole lot longer. I just wish Keynes could rise from his eternal rest to tell us exactly how long.
 Niall Ferguson is the Milbank Family Senior Fellow at the Hoover Institution at Stanford University and managing director of Greenmantle.
 Digital Access
 Home Delivery
 Gift Subscriptions
 Log In
 Manage My Account
 Customer Service
 Help & FAQs
 Globe Newsroom
 Advertise
 View the ePaper
 Order Back Issues
 News in Education
 Search the Archives
 Terms of Purchase
 Work at Boston Globe Media
 https://www.bostonglobe.com/2020/04/13/opinion/economic-consequences-coronavirus-pandemic/