Opinion: Three reasons why Jacinda Ardern's coronavirus response has been a masterclass in crisis leadership | Newshub
 Watch: Prime Minister Jacinda Ardern's 6 April press conference.  Credits: Video - Newshub ; Image - Getty
 By Suze Wilson for The Conversation.
 OPINION: Imagine, if you can, what it's like to make decisions on which the lives of tens of thousands of other people depend. If you get things wrong, or delay deciding, they die.
 Your decisions affect the livelihoods of hundreds of thousands of people, resulting in huge economic disruption, mass layoffs and business closures. Imagine you must act quickly, without having complete certainty your decisions will achieve what you hope.
 Now imagine that turning your decisions into effective action depends on winning the support of millions of people.
 Yes, you do have enforcement capacity at your disposal. But success or failure hinges on getting most people to choose to follow your leadership - even though it demands sudden, unsettling, unprecedented changes to their daily lives.
 This is the harsh reality political leaders around the world have faced in responding to COVID-19.
 As someone who researches and teaches leadership - and has also worked in senior public sector roles under both National and Labour-led governments - I'd argue New Zealand's Prime Minister Jacinda Ardern is giving most Western politicians a masterclass in crisis leadership.
 When it comes to assessing New Zealand's public health response, we should all be listening to epidemiologists like Professor Michael Baker. On Friday, Baker said New Zealand had the "most decisive and strongest lockdown in the world at the moment" - and that New Zealand is "a huge standout as the only Western country that's got an elimination goal" for COVID-19.
 But how can we assess Ardern's leadership in making such difficult decisions? A good place to start is with American professors Jacqueline and Milton Mayfield's research into effective leadership communication.
 The Mayfields' research-based model highlights "direction-giving", "meaning-making" and "empathy" as the three key things leaders must address to motivate followers to give their best.
 Being a public motivator is essential for leaders - but it's often done poorly. The Mayfields' research shows direction-giving is typically over-used, while the other two elements are under-used.
 Ardern's response to COVID-19 uses all three approaches. In directing New Zealanders to "stay home to save lives", she simultaneously offers meaning and purpose to what we are being asked to do.
 In freely acknowledging the challenges we face in staying home - from disrupted family and work lives, to people unable to attend loved ones' funerals - she shows empathy about what is being asked of us.
 The March 23 press conference announcement of New Zealand's lockdown is a clear example of Ardern's skillful approach, comprising a carefully crafted speech, followed by extensive time for media questions.
 In contrast, British Prime Minister Boris Johnson pre-recorded his March 24 lockdown announcement, offering no chance for questions from the media, while framing the situation as an "instruction" from government, coupled with a strong emphasis on enforcement measures.
 Where Ardern blended direction, care and meaning-making, Johnson largely sought "compliance".
 Ardern's approach also strongly reflects what well-known Harvard leadership scholar Professor Ronald Heifetz has long argued is vital - but also rare and difficult to accomplish - when leading people through change.
 Also consistent with Heifetz's teachings, she has regulated distress by developing a transparent framework for decision-making - the government's alert level framework - allowing people to make sense of what is happening and why.
 Importantly, that four-level alert framework was released and explained early, two days before a full lockdown was announced, in contrast with the prevarication and sometimes confusing messages from leaders in countries such as Australia and the UK.
 The work of another leadership scholar, the UK's Professor Keith Grint, also sheds light on Ardern's leadership approach during this crisis.
 Grint also argues that when dealing with "wicked problems" - which are complex, contentious and cannot be easily resolved - leaders must ask difficult questions that disrupt established ways of thinking and acting.
 It's clear this has happened in New Zealand, as shown in the suite of initiatives the government has taken to respond to the pandemic, including its decision to move to a national lockdown relatively fast compared to many - though not all - countries.
 Of course, not everything has been perfect in New Zealand's or Ardern's COVID-19 response. Ongoing, independent scrutiny of the government's response is essential.
 But as my own research has argued, expecting perfection of leaders, especially in such difficult circumstances, is a fool's errand.
 It's never possible. Nor should we allow the "perfect" to become the enemy of the "good" when speed and enormous complexity are such significant features of the decision-making context.
 Whether you're comparing Ardern's performance against other Western leaders, or assessing her efforts using researchers' measures of leadership excellence, as a New Zealander I think there is much to be grateful for in how she is leading us through this crisis.
 https://www.newshub.co.nz/home/politics/2020/04/opinion-three-reasons-why-jacinda-ardern-s-coronavirus-response-has-been-a-masterclass-in-crisis-leadership.html
