Coronavirus in UK live blog as it happened: Prime Minister in hospital as a precaution | Wigan Today
 Coronavirus in UK live blog: latest as Government expected to fall short of its 100,000 tests a day target
 152 Wigan hospital patients have died with Covid-19, NHS reports
 Scroll down and read our coronavirus live blog as it happens.
 Before you do that here are some articles offering practical advice:
 These are the signs and symptoms of the Covid-19 coronavirus strain
 This is how to help your elderly relatives and friends during the coronavirus outbreak
 What's the difference between coronavirus and flu? Here's how they compare
 This is how to safely disinfect your phone
 How is the coronavirus affecting children? Here's what you should know
 Everything you need to know about statutory sick pay amid coronavirus outbreak
 We will be providing live updates until 6pm this evening.
 #HealthHeroes
 A message from the Editor
 Thank you for reading this story on our website. While I have your attention, I also have an important request to make of you.In order for us to continue to provide high quality and trusted local news on this website, I am asking you to also please purchase a copy of our newspaper.Our journalists are highly trained and our content is independently regulated by IPSO to some of the most rigorous standards in the world. But being your eyes and ears comes at a price. So we need your support more than ever to buy our newspapers during this crisis.With the coronavirus lockdown having a major impact on many of our local valued advertisers - and consequently the advertising that we receive - we are more reliant than ever on you helping us to provide you with news and information by buying a copy of our newspaper.Thank you, and stay safe.
 Last updated: Monday, 06 April, 2020, 17:38
 At the top of his Sunday night White House press briefing, US President Donald Trump sent well wishes to Prime Minister Boris Johnson, who has been admitted to hospital after suffering with coronavirus.
 "He's a great friend of mine," Mr Trump said of his British counterpart.
 "I'm sure he is going to be fine, he's a strong man, a strong person."
 Mr Trump admitted he had not spoken to the Prime Minister and was notified he had been taken to hospital by Washington's ambassador to the UK, Woody Johnson.
 The president said it was a "big move going to the hospital".
 "He's a great gentleman, I just hope he's OK," Mr Trump added.
 A Cabinet minister has said Boris Johnson is "still very much in charge of the Government" despite spending a night in hospital with the coronavirus.
 Housing and Communities Secretary Robert Jenrick told BBC Breakfast: "Of course we all wish him well and we hope that as a result of these tests he will be able to come back to Downing Street as soon as possible
 "He's been working extremely hard leading the Government and being constantly updated. That's going to continue. Obviously today he's in hospital having the tests but he will continue to be kept informed as to what's happening and to be in charge of the Government.
 "I hope, I think we all do, that he has a speedy recovery and that he gets back to Number 10 and takes charge in the way he would want to as soon as possible.
 "I'm sure this is very frustrating for him, for somebody like Boris who wants to be hands running the Government from the front, but nonetheless he's still very much in charge of the Government."
 Housing and Communities Secretary Robert Jenrick - speaking on BBC Breakfast - has said lockdown measures could be gradually eased "in the weeks to come".
  that there is currently "excess capacity" in intensive care units "across the country", which must be maintained.
 "If we can do that then we can look in the weeks to come to begin to very carefully... lift some of those measures," he said by a videolink that was facing connection issues.
 "But an exit strategy that's sustainable will also have to be accompanied by much greater testing and tracing than we are able to do today."
 Continuing to speak on BBC Breakfast, Housing and Communities Secretary Robert Jenrick said there are no "imminent plans" to impose greater social distancing restrictions.
 "It would be very unfortunate if we had to do so and make it harder for people, particularly people who live in flats in towns and cities, to get the exercise they deserve," he said.
 "Nobody wants to see that happen."
 Jenrick implored people to stay inside, with the potential for more good weather approaching as well as the Easter weekend.
 "If we do that we will be able to in a sensible measure lift these restrictions sooner and begin to turn the tide on the virus.
 "So please, as we approach more good weather it seems and the Easter weekend, play your part."
 The former head of the civil service - Lord Kerslake - has said it would be "sensible" for Boris Johnson to "step back" if he is not well enough to carry out his role for now.
 He told the Today programme: "If he's not well enough it would be sensible to step back and let others take on the role."
 He added: "I think in the end if he's not well, he will have to reflect on this because the job's tough at the best of times and it's doubly tough now."
 Housing and Communities Secretary Robert Jenrick has not ruled out the Prime Minister spending more nights in hospital but said he expects him to return to Number 10 "shortly".
 Asked if the PM will spend more nights in hospital, Mr Jenrick told BBC Radio 4's Today programme: "He will obviously take the advice of doctors and nurses who are doing those tests and act accordingly."
 But he added: "It's worth underlining that going to hospital was a precautionary step, he's undergoing some routine tests on the advice of his doctors, this isn't an emergency admission, and so I certainly expect that he will be back at Number 10 shortly."
 Former prime minister Tony Blair has said Boris Johnson's hospital situation must be "hellish".
 He told BBC Radio 4's Today programme: "I have every sympathy and solidarity with him. I know it must be a hellish situation to be in."
 Asked whether the Prime Minister should hand over control while ill, Mr Blair said: "I'm not going to second guess them on that.
 "He knows the state of his own condition and he will be judging it carefully himself, I'm sure."
 Last night the Queen addressed the nation saying: "I hope in the years to come everyone will be able to take pride in how they responded to this challenge."
 The speech was just the fourth time she's spoken to the nation in a time of crisis. 
 A third British nurse has died with coronavirus. 
 Liz Glanister, described as a "long-serving nurse" died with the disease on Friday after years of service at Aintree University Hospital. 
 It was also confirmed that midwife Lynsay Coventry of Princess Alexandra Hospital Trust in Harlow died on Thursday.
 The number of new deaths recorded in Spain has fallen for the fourth consecutive day. 
 The number of deaths announced by the government in Spain on Monday was 637, down from  670 on Sunday.
 Foreign Secretary Dominic Raab chaired the daily coronavirus meeting on Monday morning, while the Prime Minister remains in hospital, Downing Street has said.
 It is thought that Raab would temporarily take charge of the country if Boris Johnson was to become incapacitated 
 https://www.wigantoday.net/health/coronavirus-uk-live-blog-latest-daily-press-briefing-dominic-raab-stand-prime-minister-2530111
