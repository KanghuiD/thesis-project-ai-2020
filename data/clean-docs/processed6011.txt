 Coronavirus live updates: data, infections, deaths USA, India, UK...6 and 7 April - AS.com
 Other Sports
 AS English
 CORONAVIRUS
 Coronavirus: total Nigeria cases and deaths, real-time map, 6 April
 Coronavirus
 Coronavirus: total UK cases and deaths, real-time map, 6 April
 Coronavirus: total USA cases and deaths, real-time map, 6 April
 Coronavirus: total India cases and deaths, real-time map, 7 April
 The Monday live Coronavirus feed is now closed, for the latest updates on the global Covid-19 pandemic please follow the Tuesday 7 April daily live coverage.
 We're taking a short break from the live feed right at this moment, but the morning team will be here soon with all the latest developments on the coronavirus situation.
 Meanwhile here's our front page for Tuesday, April 7, which needs no translation - Real Madrid want the French superstar, and are willing to wait until 2021. Mbappé is yet to renew with PSG.
 Latest figures from India: 4,778 confirmed coronavirus cases and 136 deaths. 375 people have recovered. 
 How athletes are doing their bit during the coronavirus pandemic
 The US Surgeon General has given the country a stark warning with coronavirus cases still to peak. 
 The US president Donald Trump said, in reaction to news that UK Prime Minister Boris Johnson had been taken to intensive care suffering from Covid-19, "We are working with London with respect to Boris Johnson. He's been a really good friend. He doesn't give up ... We’ve contacted all of Boris’ doctors ... When you get brought into intensive care, that gets very, very serious."
 Boston Mayor Marty Walsh has called for more consistent messaging from Washington after President Donald Trump had suggested that some measures could be relaxed for Easter. 
 With the coronavirus pandemic leaving clubs in financial difficulty, Serie A clubs have voted in favour of temporarily cutting player wages.
 Several political figures in the UK have sent messages of support to Prime Minister Boris Johnson, who was moved to intensive care on Monday after being hospitalised yesterday in London due to persistent coronavirus symptoms. 
 Johnson's predecessor, Theresa May tweeted: My thoughts and prayers are with @BorisJohnson and his family as he continues to receive treatment in hospital. This horrific virus does not discriminate. Anyone can get it. Anyone can spread it. Please #StayHomeSaveLives."
 The doctors remarks sparked outrage, with accusations they were treating Africans like "human guinea pigs". 
 Didier Drogba and Samuel Eto'o had previously both lashed out at the doctors' remarks, with Drogba saying: "It is totally inconceivable we keep on cautioning this. Africa isn’t a testing lab. I would like to vividly denounce those demeaning, false and most of all deeply racist words". "Helps us save Africa with the current ongoing Covid-19 and flatten the curve,” added the former Chelsea striker.
 News breaking that Boris Johnson has been moved to ICU after he was hospitalised yesterday in London. Johnson had been diagnosed with coronavirus a few days ago. 
 A spokesperson for No.10 Downing Street said: “Since Sunday evening, the Prime Minister has been under the care of doctors at St Thomas’ Hospital, in London, after being admitted with persistent symptoms of coronavirus.
 “Over the course of this afternoon, the condition of the Prime Minister has worsened and, on the advice of his medical team, he has been moved to the Intensive Care Unit at the hospital."
 Liverpool have retracted their decision to furlough several non-playing staff members impacted by the Premier League's suspension due to coronavirus, after a major backlash from fans and former players.
 The number of new infections in Italy increased by 1,941 over the past day, the lowest increase seen in the country since 30 March and a sign the situation may be improving. For the third day in a row the number of intensive care beds in use fell. 
 There were however 636 more deaths today, 111 more than the 525 registered on Sunday. In total Italy has seen 132,547 confirmed cases of Covid-19, with 16,523 deaths and 22,837 people recovered. 
 On Sunday news broke that Nadia, a four-year-old tiger at the Bronx Zoo, became the first animal in the United States to test positive for coronavirus. The news has naturally left many people wondering how susceptible animals, and particularly their beloved household pets, are to coronavirus. 
 Coronavirus: Can pets get infected with COVID-19?
 As German chancellor Angela Merkel considers making face masks mandatory in the country, we look at the advice coming from the CDC on creating your own.
 CDC face masks: how to make, use and other instructions
 With flights cancelled and travel restrictions in place to help limit the spread of the coronavirus, a report in the Financial Times has revealed that the UK government has quietly made use of an EU scheme to help repatriate Britons stranded abroad.
 Full story:
 Covid-19: UK uses EU mechanism to return stranded Britons
 Match of the Day host and fellow former Barcelona player Gary Lineker has offered his condolences to Pep Guardiola, in the wake of the news that the Manchester City manager's mother has died after contracting the coronavirus.
 The 82-year-old mother of the Man City manager has passed away after being infected with coronavirus. The club announced on Monday:
 “The Manchester City family are devastated to report the death today of Pep’s mother Dolors Sala Carrió in Manresa, Barcelona after contracting Corona Virus. She was 82-years-old.
 “Everyone associated with the club sends their most heartfelt sympathy at this most distressing time to Pep, his family and all their friends.”
 Coronavirus: Guardiola's mother dies after contracting Covid-19
 For the first time since World War II, this year will be without the famous golf tournament.
 The largest economy in Africa has turned to the IMF and the World Bank for help as the situation worsens.
 A major hospital in Mumbai has been shut after multiple health workers tested positive for Covid-19
 Coronavirus: total India cases and deaths, real-time map, 6 April
 Sergio Ramos and Gerard Piqué have joined in the campaign to keep people active at home through the lockdown. What are you doing to stay in shape?
 UK prime minister Boris Johnson, who spent last night in hospital due to ongoing coronavirus symptoms, has tweeted this afternoon to say he is “in good spirits” after being admitted for what he described as “routine tests”.
 Johnson is still in charge of the government despite being hospitalised, the BBC has said, although foreign secretary Dominic Raab chaired this morning’s coronavirus crisis meeting in his absence.
 Stanford University has studied the effects of certain disinfectant techniques for the reuse of protective masks with supplies of the equipment limited.
 Coronavirus: are masks washable & reusable? How to disinfect masks
 The January arrest of Harvard professor Charles Lieber over his ties to China led to false rumours that he had been involved in manufacturing the coronavirus.
 Covid-19: Who is Charles Lieber, the man conspiracy theorists say created the coronavirus?
 Alfredo Relaño looks at the tense situation in European football as clubs and federations try and work out the best way to finish this season and organise the next. 
 ALFREDO RELAÑO
 European football's tense wait to decide on finishing 2019-20 season
 A look at the latest impasse between the EPL and PFA from UK correspondent Will Gittins.
 Austria plans to reopen smaller shops from next week in its first step to loosen a lockdown that has slowed the spread of the coronavirus, as long as the public continue to observe the lockdown broadly, Chancellor Sebastian Kurz said on Monday.
 Kurz told a news conference that since Austria had acted earlier than most countries, that gave it the ability to reopen shops sooner as well.
 If all goes well, it will reopen non-essential shops of less than 400 square metres and DIY shops on 14 April, followed by all shops and malls on 1 May, he said.
 (Text: Reuters)
 Queen Elizabeth addressed the UK for only the fifth time in history last night as the country grapples with the spread of Covid-19.
 All the information you need to understand the coronavirus and ways to stay safe during the Covid-19 pandemic:
 Covid-19
 Coronavirus: the complete guide to the Covid-19 pandemic
 For the fourth consecutive day, the death toll drops in Spain, adding to hopes that the worst of the country’s outbreak is over. Today's fatality rate of 637 is the lowest number reported since 24 March.
 The New Zealand Prime Minister, Jacinda Ardern, responding to a letter sent to a reporter by a worried child has confirmed that both the Tooth Fairy and the Easter Bunny are considered essential workers and are not subject to lockdown. However, she also noted that they will both be quite busy at the moment, "so if the Easter Bunny doesn't make it too your household, we have to understand that it's a bit difficult at the moment for the Bunny to perhaps get everywhere." Might be worth showing this to any smaller members of the family before next Sunday, if there's a worry about Easter egg supplies...
 Russian officials said they had diagnosed 954 new cases of the disease on Monday, almost an 18% increase from the previous day’s tally.
 Russia now has a total 6,343 confirmed cases of covid-19 and has recorded 47 deaths from the virus.
 LaLiga side Real Betis lending their weight to the ongoing battle to tackle Covid-19 as their Benito Villamarín Stadium becomes a logistical centre for the reception, creation and delivery of masks for health workers in Seville.
 Once again, Spanish top flight side Leganes open up their Twitch account to the general public as fitness coach Pol Llorente takes the first team through their daily drill. 
 Join in and follow the session here
 Latest Covid-19 situation for the USA
 Cases, deaths and recoveries. #COVID19USA
 Plain, simple but essential Monday motivation from Chelsea FC
 The latest Covid-19 statistics for Nigeria: cases, deaths and recoveries. #COVID19Nigeria
 Germany's confirmed coronavirus infections rose by 3,677 in the past 24 hours to 95,391 on Monday, the fourth straight drop in the daily rate of new cases, according to data from the Robert Koch Institute for infectious diseases.
 The number of new cases was lower than the 5,936 new infections reported on Sunday. The reported death toll rose by 92 to 1,434.
 The Taoiseach (PM) for the Republic of Ireland Leo Varadkar has rejoined the medical register and is set to work a session a week to help out in the country's Coronavirus crisis
 The Japanese government is considering a period of six months for the state of emergency that it is preparing to call in response to the coronavirus pandemic, broadcaster TBS reported on Monday.
 The move would cover Tokyo and three neighbouring prefectures as well as Osaka, TBS said, citing unidentified sources. Within the six-month period, these prefectures would decide the length of time for their individual measures.
 British PM Boris Johnson was still in hospital on Monday suffering persistent coronavirus symptoms 10 days after testing positive for the virus, though Downing Street said he remained in charge of the government.
 Full story
 ABC journalist Pablo Diaz published a series of photos from central Wuhan yesterday showcasing a slow sense of normality resuming to the Chinese city which was at the epicentre of the Covid-19 outbreak. The pictures show busy streets with commercial activity slowly resuming to a normal level.
 The Bundesliga champions have decided to return to training in small groups and under strict medical observation. At present both the German championship and DFB cup are on hold. They join fellow Bundesliga outfit Wolfsburg who made a decision to return to low key activity over a week ago.
 A zoo in New York has had a confirmed Covid-19 case in one of its 'big cats', and they claim that there are other animals showing symptoms.
 Iconic English singer Marianne Faithful is also hospitalised as a consequence of contracting the virus over the weekend with the 73-year-old reported to be in a 'stable' condition in a London hospital.
 It was confirmed late on Sunday night that UK PM Boris Johnson was hospitalised overnight after being admitted for tests. Johnson was diagnosed with the virus ten days ago.
 UK Foreign secretary Dominic Raab is set to oversee the government's next coronavirus meeting, scheduled for later this morning
 Cases-Deaths
 USA: total confirmed cases 336,830 / total Covid-19 deaths: 9,618
 Italy: total confirmed cases 131,646 / total Covid-19 deaths: 12,641
 Spain: total confirmed cases 129,948 / total Covid-19 deaths: 15,887
 Germany: total confirmed cases 100,123 / total Covid-19 deaths: 1,584
 France: total confirmed cases 92,839 / total Covid-19 deaths: 8,078
 *source (Worldometer 0830 CEST)
 Good morning and welcome to our live coronavirus blog. Over the course of the day, we will be bring you all the latest news relating to the Covid-19 virus from around the world on 6 April. At the time of writing, the virus has claimed over 69,000 lives with over 1.2 million positive cases currently confirmed around the world.
 0 
 Comentarios        
 Para poder comentar debes estar 
    registrado                
 y haber iniciado sesión. 
 ¿Olvidaste la contraseña?
 Ediciones internacionales
 Apps
 Síguenos
 https://en.as.com/en/2020/04/06/other_sports/1586152723_229734.html