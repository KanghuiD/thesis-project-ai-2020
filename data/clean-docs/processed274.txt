Americans warned that as many as 240,000 may die 
 As many as 240,000 people in the U.S. could die from COVID-19 — and that’s only with strict social distancing measures in place, one of the government’s top doctors warned Tuesday.
 Dr. Deborah Birx, Vice President Mike Pence’s coronavirus response coordinator, said that between 1.5 and 2.2 million could die without the intervention.
 Already, the death toll in the United States has surpassed the number of people killed on Sept. 11, 2001. According to NBC News’ tally, the disease has killed 3,768 people and infected more than 185,000.
 The numbers continued to rise as Wall Street ended one of its worst quarters in history. The Dow Jones was down by 400 points — a quarterly loss of 22 percent — while the S&P 500 recorded its worst three months since 1938. The Nasdaq, meanwhile, closed down at just under 1 percent.
 Full coverage of the coronavirus outbreak
 Download the NBC News app for latest updates on the coronavirus outbreak.
 This coverage has ended. Continue reading April 1 coronavirus news.
 The Associated Press
 A bleary-eyed Chris Cuomo, saying he wanted to be a cautionary tale for his audience, anchored his CNN show from his basement Tuesday after testing positive for the coronavirus.
 Via remote link, he interviewed Illinois Gov. J.B. Pritzker, an emergency room nurse and CNN medical correspondent Dr. Sanjay Gupta, who expressed worry about one of Cuomo’s symptoms.
 “Brace yourself,” Cuomo told viewers, “not for a hoax. But for the next few weeks of scary and painful realities. This is a fight. It’s going to get worse. We’re going to suffer.”
 Cuomo looked pale, his eyes watery and red-rimmed. He took a few deep breaths to compose himself. He repeated himself. Even Gupta said he didn’t look good, and said he’d call later to talk about a tightness Cuomo was feeling in his chest.
 Read the full story here. 
 MOSCOW — An international media freedom watchdog says the autocratic ex-Soviet nation of Turkmenistan has banned the media from using the word “coronavirus.”
 Reporters Without Borders said Tuesday the word also has been removed from health information brochures distributed in schools, hospitals and workplaces. The gas-rich Central Asian nation that neighbors Iran so far has reported no cases of the new coronavirus. Iran has reported more than 44,000 cases.
 Paris-based Reporters Without Borders said people wearing face masks or talking about the coronavirus are liable to be arrested by plainclothes police. Ranked last in the group’s 2019 World Press Freedom Index, Turkmenistan is one of the world’s most closed countries.
 Turkmenistan President Gurbanguly Berdymukhamedov has ruled the country since 2006 through an all-encompassing personality cult that styles him as Turkmenistan’s “arkadaq,” or protector.
 Phil Helsel
 There may not be a physical Democratic national convention this year because of the coronavirus pandemic, former vice president and current Democratic presidential candidate Joe Biden suggested Tuesday.
 "It's hard to envision that," Biden told MSNBC's Brian Williams when asked whether he could see prominent Democrats from around the county gathering in an arena for the convention, which is scheduled for July.
 Conventions, primaries and elections have been held during times of national crisis in the past, said Biden, who said officials should listen to the scientists when making decisions. 
 "The fact is, it may have to be different," Biden said. "My guess is, there's going to be a great deal more absentee balloting, we used to call it, but paper ballots." He also said that the situation could change by then.
 Biden has had a surge of primary victories, but his rival Sen. Bernie Sanders, I-Vt., is still in the race.  Sanders earlier this week told "Late Night" host Seth Myers that "there is a path" to the nomination, though "admittedly a narrow path."
 Lisa Cavazuti, Cynthia McFadden and Rich Schapiro
 One month ago, Chris Austin was running a little-known mom-and-pop business in Texas that fielded a few dozen orders a week for his helmet-style ventilation devices.
 He had five employees and a handful of volunteers from the family's church who would pitch in at the workshop behind their home in the small town of Waxahachie.
 Then the coronavirus epidemic hit.
 Austin's company, Sea-Long Medical Systems Inc., is getting thousands of orders every day, from America's top hospitals to countries as far flung as the United Arab Emirates. Researchers say the device, which costs less than $200, could help hospitals free up ventilators for only the most critically ill coronavirus patients.
 Shamar Walters
 Across the state this weekend, the faithful gathered in such cities as Fayetteville, LaGrange, and Newnan. Hundreds attended an event in Cartersville, and videos showing hospital staff walking onto the roof to join the worship service have been viewed millions of times.
 The organizers told the hospital about the service beforehand but didn’t expect the staff’s rooftop appearance. “We just thought they might come out the bottom or to the window,” organizer Camden McGill said. “Them on the roof was just kind of an amazing random surprise. That was just God.”
 Georgia has more than 4,000 confirmed coronavirus cases and more than 100 confirmed deaths, according to the state's Department of Public Health.
 One way Americans are coping with the new coronavirus? Booze.
 U.S. sales of alcoholic beverages rose 55 percent in the week ending March 21, according to market research firm Nielsen.
 Spirits like tequila, gin and pre-mixed cocktails led the way, with sales jumping 75 percent compared to the same period last year. Wine sales were up 66 percent, while beer sales rose 42 percent.
 Online sales far outpaced in-store sales. Nielsen said online alcohol sales were up 243 percent.
 Danelle Kosmal, a Nielsen vice president, suspects growth rates peaked that week as people loaded up their pantries before state stay-at-home orders went into effect. Kosmal said data for the week ending March 28 will be a better indicator of ongoing demand.
 President Donald Trump on Tuesday approved disaster declarations for Montana and Ohio related to the coronavirus pandemic, the White House said.
 The president has approved disaster declarations for many states as they deal with the outbreak, including for Rhode Island, Pennsylvania, Kansas, Alabama, Georgia, Oregon and the District of Columbia.
 The declarations allow for federal assistance.
 There have been five deaths in Montana associated with the coronavirus illness COVID-19 and more than 190 cases. In Ohio, there have been 2,199 confirmed cases and 55 deaths, according to the state health department.
 https://www.nbcnews.com/health/health-news/live-blog/live-coronavirus-updates-u-s-death-toll-passes-3-000-n1172706/ncrd1173496
