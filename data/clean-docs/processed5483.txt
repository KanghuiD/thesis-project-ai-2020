										Coronavirus business&nbspupdate
 										Get 30 days complimentary access to our Coronavirus Business Update newsletter
 									
 										30 days complimentary
 Add this topic to your myFT Digest for news straight to your inbox
 From Giovanna Forte, London, UK
 From Christopher Gibbs, Villars, France
 From Muneer Ahmad, First Secretary (Press), Pakistan High Commission, London SW1, UK
 From Sheila Hayman, Director’s Fellow, MIT Media Lab, London, UK
 From Dr Hans Dembowski, Frankfurt am Main, Germany
 From Sir Anthony Brenton, Cambridge, UK
 From Rodney Atkinson, Stocksfield, Northumberland, UK
 From Benoît Barthelmess and Kristof Kleemann
 From Barbara Goodman, London, UK
 From Ian Blackford MP and others
 From Giles Conway-Gordon, Ronan, MT, US
 From Prof Markus C Kerber, Berlin, Germany
 From Roger Gill, Cradley, Herefordshire, UK
 From Richard Calland, Associate Professor, Public Law, University of Cape Town, South Africa
 From Etienne Deffarges, San Francisco, CA, US
 From Peter Doyle, Washington, DC, US
 From Bert Ely, Ely & Company, Alexandria, VA, US
 From James Winpenny, Chipping Norton, Oxon, UK
 From Gary Goodman, Hove, E Sussex, UK
 From Keith Corkan, Richmond upon Thames, Surrey, UK
 From Melissa Cook, Patterson, NY, US
 From Malcolm J Heslop, Stratham, NH, US
 From Jack Winkler, London N1, UK
 From Robert Chandler, London W14, UK
 From David Robertson, West Malvern, Worcs, UK
 International Edition
 https://www.ft.com/letters
