Coronavirus: Warning over daily death figures - BBC News
 Experts are warning against over-interpreting daily figures of people dying with Covid-19, since they often reflect reporting delays. 
 Spikes or dips may in part reflect bottlenecks in the reporting system, rather than real changes in the trend. 
 On Monday, 439 coronavirus deaths were recorded in the UK - down from 621 on Sunday and 708 on Saturday. 
 Many hospitals will not report deaths that happened over the weekend until the middle of the following week. 
 Over the weekend, NHS England released new figures broken down by the actual date of death. 
 And these reveal that between 11 March and 1 April there were about 300 more deaths than previously thought during that period. 
 Prof Jim Naismith, at the University of Oxford, said because of "understandable" delays in reporting by NHS trusts, the daily figures included deaths that may have happened up to two weeks ago.
 Separate figures, published by the Office for National Statistics (ONS) also suggest the number of people dying with coronavirus is higher than the daily totals indicate. 
 The ONS examined registrations and found deaths in the community not included in the daily hospital deaths figures. 
 In the week to 27 March, for the 501 deaths recorded in hospitals the ONS also found 38 deaths linked to coronavirus in the community. 
 Nevertheless, there are some early promising signs the virus's spread is slowing, as new confirmed cases fell from 4,450 to 3,802 between Friday and Monday. 
 The rate of increase in cases has halved in the past week.
 And this should feed through into a slowing in critical-care admissions and eventually deaths. 
 But reporting delays mean once we reach the peak, we may not know about it for several days. 
 Different hospitals will have delays of different lengths of time. 
 And this makes it a challenge to see the real trend at the moment.
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/health-52167016
