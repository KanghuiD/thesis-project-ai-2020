[OPINION] The plight of 'non-bailable' detainees during the coronavirus crisis
 Welcome to Rappler, a social news network where stories inspire community engagement and digitally fuelled actions for social change. Rappler comes from the root words "rap" (to discuss) + "ripple" (to make waves).
 Read more
 'As an academician on issues of criminal justice and as a former accused charged with a non-bailable offense whose trial dragged for 7 long years, I can deeply empathize with the families of persons presumed innocent behind bars'    
 One key segment of the detainee population that is seldom discussed is the plight of the so-called "non-bailable" offenders. At least 50% of the detainees in the BJMP and provincial jails and police detention centers fall under this category. The Constitution and the Rules of Court state that persons charged with capital offenses (punishable by life sentence or reclusion perpetua) may post bail if the evidence of guilt is not strong. To determine whether the guilt is not strong, a petition for bail must be filed by the counsel of the accused. This in turn will require a judge to conduct a "full-blown trial" and entertain the presentation of evidence to determine the merit of the case. 
 Majority of the non-bailable offenders are charged with Section 5/11 of Republic Act 9165, which prohibits the "sale" and "use" of drugs called shabu. Majority of these drug-related offenders are first-time offenders and non-violent. Some may have the capacity to post bail but cannot post bail due to the fact that judges have not ruled on whether the evidence of guilt is strong or not. Given the threat of COVID-19 infections, being charged with a non-bailable offense, without findings of guilt and still presumed innocent, can be a death sentence.
 Using practices in international jurisdictions, I humbly suggest that our courts may adopt the following: 
 1. Introduce summary proceedings in the petition for bail. The Counsel of the accused will submit a petition for bail, which can be done online to meet social distancing guidelines.
 2. The judge will evaluate the case based on "public safety risk." The risk score can be computed on the following set of questions:
 a. Does the accused have a prior criminal history (Yes=1; No=0)
 b. Does the accused have a history of jumping bail (Yes=1; No=0)
 c. Does the accused have a history of breaking probation and parole conditions (Yes=1; No=1)
 d. Is the accused employed/at school prior to arrest (Yes=0; No=1)
 e. Does the accused have a supportive family (Yes=0; No=1)
 f. Is the accused a known troublemaker in the community (Yes=1; No=0) 
 If the accused has a total of "zero points," this suggests that the accused has a low public safety risk. The accused can therefore be released on bail. Additionally, the judge may put conditions for release such as regular reporting to the barangay and being placed under house arrest. When the COVID-19 crisis is over, failure to attend court hearings, commission of new offenses, and violation of release conditions can be used a basis for rearrest.  
 The Defense Counsels can highlight these characteristics of the accused in their petitions. They can attach letters of support from the family and the barangay, and show proof of work or school. The Clerk of Courts can also check with police, jail, or court records to determine criminal history, prior incarceration, and probation/parole background. The judge can utilize the "score" as a guide in the practice of their judicial discretion on whether to grant or deny bail. 
 These can all be done summarily, using online petitions, to expedite the bail procedures. This actuarial scoring will serve in lieu of the "full-blown trial" to determine whether the evidence of guilt is not strong.  
 As an academician on issues of criminal justice and as a former accused charged with a non-bailable offense whose trial dragged for 7 long years, I can deeply empathize with the families of persons presumed innocent behind bars. 
 Justice for one, justice for all. – Rappler.com
 Raymund E. Narag, PhD, is Associate Professor of Criminology and Criminal Justice at Southern Illinois University's School of Justice and Public Safety.
 These stories made other people 
 Password:
 Thank You.
 View your profile page
 here OR
 Fields with * are required.
 Password*:
 First Name:
 Last Name:
 Birthday:
 Gender:
  Fields with * are required. 
 Thank You.You have successfully updated your account.
 https://www.rappler.com/thought-leaders/258672-opinion-plight-non-bailable-detainees-coronavirus-crisis
