Grandmother who went viral after inviting wrong teenager to Thanksgiving loses husband to coronavirus | TheHill
 The husband of Wanda Dench, the grandmother from Arizona who went viral after she accidentally invited the wrong teenager to Thanksgiving and ended up spending the holiday with him, has reportedly died after he contracted the novel coronavirus. 
 “As some of you may have already found out tonight Lonnie did not make it ... he passed away Sunday morning,” Hinton said, “but Wanda told me all the love and support he was receiving put a huge smile on his face so I thank every single one of you guys for that!”
 As some of you may have already found out tonight Lonnie did not make it... he passed away Sunday morning but Wanda told me all the love and support he was receiving put a huge smile on his face so I thank every single one of you guys for that!❤️ https://t.co/tNvals0FMh
 In a statement to AZFamily, Wanda Dench said her husband “had the truest heart of love, like no other.” 
 “He did so many acts of kindness that no one ever heard about,” she continued. “He was my hero. And I’m a better person because of him.”
 Hinton and the Denchs met in November 2016 after the then-high school student had been accidentally invited to their Thanksgiving dinner by Wanda Dench. The grandmother said she had she invited Hinton over text after mistaking his number for her grandson’s number.
 Hinton and Wanda Dench later realized her mistake after they sent photos of each other in text exchanges from the time that had gone viral then.
 “You not my grandma. Can I have still have a plate though?” Hinton wrote then.
 “Of course you can,” Wanda Dench wrote back. “That’s what grandma’s do ... feed every one.”
 Since then, Hinton and the Denchs have celebrated Thanksgiving together, marking their fourth holiday together this past November.
 View the discussion thread.
 The Hill 1625 K Street, NW Suite 900 Washington DC 20006 | 202-628-8500 tel | 202-628-8503 fax
 https://thehill.com/blogs/blog-briefing-room/news/491978-grandmother-who-went-viral-after-inviting-wrong-teenager-to
