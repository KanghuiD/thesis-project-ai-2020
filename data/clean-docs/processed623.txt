Morrison speaks to Bill Gates about future of WHO – as it happened | World news | The Guardian
 PM and philanthropist believed to have also discussed vaccines, while state and federal leaders lift some Covid-19 restrictions on elective surgery as Australia’s infection rate falls. This blog is now closed
 Graham Readfearn, 
 Ben Smee, 
 Michael McGowan and 
 Matilda Boseley
 Tue 21 Apr 2020 11.45 BST
 First published on Mon 20 Apr 2020 22.36 BST
 11.37am BST
 11:37
 That will be that for our live coronavirus coverage for today. 
 Thanks from me, Graham Readfearn, and from my colleagues Ben Smee, Michael McGowan and Matilda Boseley for staying with our live coverage over the past 13 hours. 
 Guardian Australia will be back tomorrow with another live blog.
 Here’s a summary of the day’s main events:
 Just a reminder to treat yourselves and others with care. 
 Updated
 at 11.45am BST
 11.05am BST
 11:05
 He says the RBA’s expectations of high and persistent unemployment was not a sign the economy would “snap back” to normal in the six-month timeframe he said the prime minister had specified.
 When unemployment spikes in the next few months, remember hundreds of thousands of job losses could have been prevented if the treasurer had used his powers to include more workers in the jobkeeper program, which he could do with the stroke of his pen.
 Chalmers said Labor “appreciates the ongoing engagement with the Reserve Bank and the financial sector, as well as their coordinated approach to supporting liquidity, functioning of markets and the economy during this challenging time”.
 He said the RBA’s update should prompt the Morrison government to release a full set of updated economic and budget figures and forecasts in the next month “in lieu of a full budget.”
 at 11.08am BST
 10.32am BST
 10:32
 It’s about that time of the evening when Jimmy Barnes drops a song from his lounge room. 
 Tonight it’s a cover of the 1987 Bruce Springsteen song Tougher Than the Rest.
 at 10.36am BST
 9.59am BST
 09:59
 Staff at Cairns Hospital are all being screened for Covid-19 after three people working in the hospital’s pathology lab tested positive for antibodies of the coronavirus.
 Queensland Health has released a statement. Chief health officer Dr Jeannette Young said: 
 The three Cairns Hospital employees who tested positive for Covid-19 had mild or no symptoms and did not realise they had the disease, which is why they did not get tested. As a precaution, they have been placed in self-quarantine, even though they are not feeling unwell or likely to be infectious.
 She said all staff were now being screened.
 It’s important to understand that there is a low risk of further transmission, but we need to make every effort possible to ensure this disease does not spread further.
 The statement said the tests of the pathology workers were ordered after one pathology worker was diagnosed with Covid-19 on 16 April.
 A Brisbane-based technician who visited the lab on 19 and 20 March and was diagnosed with the disease when he returned home is believed to be the source of infection
 Dr Young appealed for anyone in Cairns who had symptoms including a cough, shortness of breath, sore throat or fever to get tested and remain at home, even if they were essential workers.
 at 10.15am BST
 9.27am BST
 09:27
 Here’s a statement from Anglicare, confirming the death of a 92-year-old woman at its Newmarch House aged care home in western Sydney.
 We are saddened to advise a third resident of Newmarch House, who had tested positive for Covid-19, passed away late this morning. She was 92-years-old and had multiple health issues.
 The resident’s family have been contacted, as have all relevant authorities. The cause of death is still to be formally determined.
 Anglicare Sydney CEO Grant Millard said: “I have spoken personally to the immediate family of the resident to convey our deepest sympathies.”
 This is a very sad time not only for the immediate family but also for other residents and staff.
  Please keep everyone in your thoughts and prayers as we continue to support the families through what is a difficult and challenging time.
 9.23am BST
 09:23
 Here are the numbers of new cases reported by health authorities across states and territories on Tuesday.
 at 10.55am BST
 9.19am BST
 09:19
 Reports are coming in of a third death at Newmarch House, an aged care home in western Sydney where a quarter of residents have previously tested positive for coronavirus.
 Last weekend, Anglicare confirmed a 94-year-old man with Covid-19 had died at the home in Kingswood.
 “A coroner will be the person to confirm the cause of death,” a spokesman told AAP at the time.
 A 93-year-old resident in the home died the previous day, also after testing positive for Covid-19.
 9.09am BST
 09:09
 AAP is reporting that Scott Morrison spoke with the philanthropist Bill Gates earlier today about the future of the World Health Organisation.
 The news agency says the conversation came just days after Gates used a global broadcast organised by the entertainer Lady Gaga to ask for support for the global health body. Here’s more from the report.
 Mr Morrison and Mr Gates are also understood to have discussed vaccines and the Indo-Pacific’s health challenges.
 Mr Gates has been publicly critical of a decision by US president Donald Trump to suspend his country’s funding for the WHO.
 The US is the largest donor to the WHO, providing more than $631 million in 2019 – about 15 per cent of its budget.
 “Halting funding for the World Health Organisation during a world health crisis is as dangerous as it sounds,” Mr Gates tweeted recently.
 “Their work is slowing the spread of Covid-19 and if that work is stopped no other organisation can replace them.”
 Mr Trump has argued the WHO failed to adequately “obtain, vet and share information” in a timely and transparent way, leaving a global trail of death and destruction.
 Mr Morrison recently expressed some sympathy for Mr Trump’s criticisms, pointing to the way Australia pre-emptively declared a pandemic before the WHO.
 Australia has worked closely with the WHO for more than seven decades.
 The Gates Foundation is one of the WHO’s biggest voluntary donors, providing $836 million over the past two years.
 at 9.49am BST
 8.43am BST
 08:43
 Northern Territory chief minister Michael Gunner has announced that anyone caught coughing or spitting at any Territorian doing their job “in order to cause fear about the spread of the coronavirus” will get an on-the-spot fine of $5,495 and could face jail time.
 Gunner said there had been “one incident, the other day, where an idiot threw his spit at some cops and said he had coronavirus”.
 He didn’t have it – but the cops who were just doing their job did not know that.
 And it doesn’t matter anyway – the act itself is revolting. It’s one incident, but it’s one too many.
 So today we are sending a message – and the message is clear: if you pull something like this, you are a grub. You won’t get away with it.
 It’s disgusting and it’s dangerous. It’s unacceptable and it’s un-Territorian. There are zero excuses. You will get zero chances. A massive on-the-spot fine is just the beginning.
 He said the one incident was in contrast to Sydney, where, Gunner said, “there seems to be coughers and spitters on every corner”.
 That hasn’t happened to that extent here. By and large, Territorians are too decent for that.
 All but a few of us are just getting on with it and showing the respect to frontline workers that they deserve.
 But we’re the best in the nation right now because we keep staying ahead of the game.
 Gunner said on Tuesday the NT had seen its 15th day without a new case of Covid-19 being recorded. 
 at 8.50am BST
 8.15am BST
 08:15
 Luke Henriques-Gomes
 The government is facing calls to extend its temporary suspension of welfare mutual obligations beyond next week, as Labor and the Greens warn of the risks of compulsory job-centre meetings during the coronavirus pandemic.
 But the employment minister, Michaelia Cash, has declined to say whether the government still intended to resume the requirements for jobseekers on 27 April.
 The opposition on Tuesday joined calls from the Greens senator Rachel Siewert that welfare recipients should be spared obligations such as in-person appointments with employment service providers while most states remained in lockdown.
 Labor frontbenchers Brendan O’Connor, Linda Burney and Louise Pratt said in a statement: 
 While there should be a resumption of mutual obligation when it is safe to do so, we believe the current arrangements should continue as long as community restrictions are in place.
 Siewert wrote to Cash on Monday asking her to suspend Centrelink obligations for six months. She said: 
 Now is not the time to be penalising people and suspending their payments for choosing not to put their safety at risk by requiring them to attend appointments and face-to-face meetings.
 Those new dole recipients likely add to an existing group of about 700,000 people who were receiving the equivalent payment, formerly known as Newstart, in December.
 The suspension of mutual obligations means welfare recipients do not have their payments cut off for failing to attend meetings with employment service providers or complete a certain number of job applications.
 Before temporarily suspending obligations in the middle of a Centrelink tech meltdown, Cash announced the requirements would be significantly relaxed.
 A spokesman for Cash said on Tuesday:
 As previously stated, mutual obligation arrangements are being regularly reviewed, taking into account a range of factors including labour market conditions.
 at 8.22am BST
 8.05am BST
 08:05
 We saw in central Queensland a few years ago, when Virgin pulled out of a route to a mining town, prices went up 30% after they pulled out. We just went back to Qantas, and I don’t want to see that happen. That is why I’m a strong advocate for the government to get involved, get its hands dirty and find a way forward.
 at 8.17am BST
 https://www.theguardian.com/world/live/2020/apr/21/australia-coronavirus-live-updates-oil-nsw-victoria-qld-schools-latest-news-update
