Desperate for Fall Enrollment, Colleges Are Offering Unprecedented Perks - InsideHook
 As the coronavirus pandemic leaves many students’ plans for fall up in the air (and the recession adds a new layer of economic uncertainty on top of that), colleges across the country are getting desperate to boost their fall semester enrollment numbers. And as a new Washington Post piece points out, recruiters are employing new tactics to try to lure students to their campuses.
 “The gloves have come off,” Angel Pérez, vice president for enrollment and student success at Trinity College in Connecticut, told the publication. “You’re talking about a scenario where colleges need to enroll students at any cost.”
 That includes offering perks like early registration, top choice on dorm rooms, free parking and even scholarship money. Accepted students who pay a deposit to Colorado Christian University by Dec. 1 are guaranteed an additional $1,000-a-year scholarship, while Albion College in Michigan is entering students in a sweepstakes where they can win prizes like free room and board for a semester or $250 for textbooks once they put down their deposits. And as the Post points out, “some of the inducements are a consequence of a Justice Department action that forced college admissions officers to drop key parts of their professional code of ethics, which prohibited many of these kinds of appeals and banned colleges from pursuing each other’s students.”
 “Everybody was sort of, like, ‘Oh my god, what just happened? Fasten your seat belt because it’s going to be an all-out crazy time with people dangling incentives,’” Joan Koven, an educational consultant in Philadelphia, said. “And then we have this [pandemic crisis] bursting open.”
 Of course, all this recruiting is hinged upon the idea that schools will be open for in-person classes this fall — something that isn’t necessarily a given yet, thanks to COVID-19.
 “That’s very much up in the air,” Robert Ruiz, a former admissions director, told the Post. “And if they’re not going to be on the campus this fall, all the things we thought were important to them won’t be important.”
 Read the full story at Washington Post
 News From Around the Web
 Arts & Entertainment
 The iconic magazine has ended its print run after 66 years, marking the beginning of a new, fully digital era of Playboy
 TV
 With the world standing still, the greatest athlete of our lifetime has reclaimed center stage
 Movies
 For his fourth feature, Australia's prodigal son has returned to his visceral, lo-fi roots
 Health & Fitness
 Farmers, doctors and a champion powerlifter weigh in on the merits and perils of an afternoon in the yard
 Booze
 Drink with the world’s best bartenders (or budding wine connoisseur Jon Bon Jovi) from the safety of home
 Gear
 The 12 best pieces of garb and gear that crossed our desks this week
 Here are the best gear deals, from retro vests to sherpa fleece
 Shoes
 Now you can wear the brand's famed denim on your feet, too
 Menswear
 You can now sport the soft AF sweats in shorter styles and cheery colors
 Three shoes for three occasions
 Style
 It might be a weird spring, but it's still spring — let's dress like it
 Audio
 Savings of $20-$50 on already inexpensive yet high-end audio gear
 Get InsideHook in your inbox. It's free. And Awesome.
 Got questions or feedback?Hit us up
 Manage My Subscription
 https://www.insidehook.com/daily_brief/news-opinion/colleges-fall-2020-enrollment-perks
