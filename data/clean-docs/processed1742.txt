Coronavirus reaches new countries as crisis seems to ease in China - The Boston Globe
 NEW YORK (AP) — The coronavirus spread to ever more countries and world capitals Monday — and the US death toll climbed to six — even as new cases in China dropped to their lowest level in over a month.
 A shift in the crisis appeared to be taking shape: Hundreds of patients were released from hospitals at the epicenter of the outbreak in China, while the World Health Organization reported that nine times as many new infections were recorded outside the country as inside it over the past 24 hours.
 Alarming clusters of disease continued to swell in South Korea, Italy and Iran, and the virus turned up for the first time in New York, Moscow and Berlin, as well as Latvia, Malaysia, Morocco, Tunisia, Senegal, Jordan and Portugal. The worldwide death toll topped 3,000 and the number of those infected rose to about 89,000 in 70 countries on every continent but Antarctica.
 Global health officials sought to reassure the public that the virus remains a manageable threat.
 “Containment is feasible and must remain the top priority for all countries,” WHO chief Tedros Adhanom Ghebreyesus said.
 Around the world, the crisis reshaped the daily routines of millions of people. 
 Across Japan, children stayed home after the government announced the closing of schools until April. In Paris, the galleries of the Louvre museum were off limits. With Israel holding a national election, special voting booths were set up for those under quarantine. In Germany, Chancellor Angela Merkel was rebuffed by her interior minister when she extended her hand to greet him.
 At the United Nations, officials said they were postponing a major conference on women that had been expected to bring up to 12,000 people from its 193 member countries to New York next week.
 The Organization for Economic Cooperation and Development warned that the world economy could contract this quarter for the first time since the international financial crisis more than a decade ago.
 “Global economic prospects remain subdued and very uncertain,” the agency said. 
 Nevertheless, the Dow Jones Industrial Average soared nearly 1,300 points as stocks roared back from a seven-day rout on hopes that central banks will take action to shield the global economy from the effects of the outbreak.
 Health officials in Washington state, where a particularly troubling cluster of cases surfaced at a nursing home outside Seattle, said that four more people had died from the coronavirus, bringing the number of deaths in the U.S. to six, all in Washington. New cases were also reported in New Hampshire and New York.
 In Seattle, King County Executive Dow Constantine declared an emergency and said the county is buying a hotel to be used as a hospital for patients who need to be isolated. 
 “We have moved to a new stage in the fight,” he said.
 Over 100 cases have been confirmed in the U.S., more almost certain in the coming weeks. Thousands of test kits were on their way to state and local labs, and new guidelines intended to expand screening were put in place.
 “In this situation, the facts defeat fear. Because the reality is reassuring. It is deep-breath time,’’ New York Gov. Andrew Cuomo said.
 The message was echoed by global health officials, who said they were encouraged that even in some countries that had taken far less aggressive measures than China’s, the virus remains largely in check.
 Because the virus is not transmitted as easily as the flu, “it offers us a glimmer ... that this virus can be suppressed and contained,” said Dr. Mike Ryan, the WHO’s emergencies chief.
 China reported just 202 new cases, its lowest daily count since Jan. 21, and the city at the heart of the crisis, Wuhan, said 2,570 patients were released. At the largest of 16 temporary hospitals that were rapidly built in Wuhan in response to the outbreak, worries over the availability of supplies and protective gear eased, along with the pressure on the medical staff.
 Dr. Zhang Junjian, who leads a temporary hospital in Wuhan with a staff of 1,260, said optimism is high that the facility will no longer be needed in the coming weeks.
 But in other places, problems continued to multiply.
 South Korea, with the worst outbreak outside China, reported 599 new cases, bringing the total to 4,335. The death toll rose to 26.
 In Iran, a confidant of Iran’s supreme leader died from the virus. The Islamic Republic confirmed 1,501 cases and 66 deaths, but many believe the true number is larger. Its reported caseload surged more than 250% in just 24 hours.
 Italy’s caseload rose to 2,036, including 52 deaths. Officials said it could take up to two weeks before they know whether measures including quarantining 11 towns in northern Italy are slowing the spread of the virus.
 In the U.S., meanwhile, four Americans exposed to the virus aboard a Japanese cruise ship were released from quarantine in Nebraska after testing negative.
 One of them, Jeri Seratti-Goldman of Santa Clarita, California, said leaving the hospital was bittersweet, because her husband remained quarantined. Another, Joanne Kirkland of Knoxville, Tennessee, said: “My only question is, will my friends shun me after this?”
 ___
 Contributing to this report were Josh Funk in Omaha, Nebraska; Chris Grygiel in Seattle, Carley Petesch in Dakar, Senegal; Matt Sedensky in Bangkok, Dake Kang in Beijing; Aniruddha Ghosal in New Delhi; Kim Tong-Hyung in Seoul, South Korea; Nasser Karimi in Tehran, Iran; Jon Gambrell in Dubai, United Arab Emirates; Thomas Adamson and Lori Hinnant in Paris; Mari Yamaguchi in Tokyo; Nicole Winfield and Frances D’Emilio in Rome; Colleen Barry in Milan; and Aron Heller in Jerusalem.
 Digital Access
 Home Delivery
 Gift Subscriptions
 Log In
 Manage My Account
 Customer Service
 Help & FAQs
 Globe Newsroom
 Advertise
 View the ePaper
 Order Back Issues
 News in Education
 Search the Archives
 Terms of Purchase
 Work at Boston Globe Media
 https://www.bostonglobe.com/2020/03/02/world/virus-reaches-new-countries-crisis-seems-ease-china/