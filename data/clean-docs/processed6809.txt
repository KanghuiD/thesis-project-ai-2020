Don't forget to take care of your mental health as well during the COVID-19 pandemic
 Would you like to receive local news notifications on your desktop?
 Menu
 With drastic changes to the way Ohioans live day-to-day due to the COVID-19 pandemic—from business closures to unemployment to the isolation social distancing can create—it’s important to focus not only on your physical health, but also your mental health. 
 Those with mental health challenges or addiction challenges are encouraged to keep their appointments or if worried about going in for their appointment to see if telehealth appointments or video appointments are a possibility with your provider. 
 Ohioans who do not already have a provider and may be experiencing mental health concerns or addiction concerns for the first time are also encouraged to reach out for help. Providers are still taking new patients, according to Criss.
 “Mental healthcare is healthcare. The brain is part of the body,” Criss said.
 Resources have been compiled by the state to help those who face mental health and/or addiction challenges so they are able to stay home while still receiving the care they need. 
 National Suicide Prevention Lifeline1-800-273-8255
 The PEER Center Warm Line(614) 358-TALK (8255), 9 a.m. to 3 p.m.
 Disaster Distress Helpline1-800-985-5990 (1-800-846-8517 TTY)
 Ohio Crisis Text LineText the keyword “4HOPE” to 741 741
 Read our daily Coronavirus Live Blog for the latest updates and news on coronavirus.
 View a global coronavirus tracker with data from Johns Hopkins University.
 Here is everything you need to know about testing for coronavirus in Ohio.
 Here's a list of things in Northeast Ohio closed due to coronavirus concerns
 See complete coverage on our Coronavirus Continuing Coverage page.
 Global Coronavirus Tracker: See map hereData from The Center for Systems Science and Engineering at Johns Hopkins University.
 https://www.news5cleveland.com/news/continuing-coverage/coronavirus/dont-forget-to-take-care-of-your-mental-health-as-well-during-the-covid-19-pandemic
