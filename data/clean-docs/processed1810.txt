Coronavirus: Amazon ordered to deliver only essential items in France - BBC News
 Amazon has been ordered to limit its deliveries in France to essential goods only, amid claims it is failing to protect its workers from coronavirus.
 A court in Nanterre, near Paris, ordered the online retail giant to deliver only food, hygiene and medical products in the country from Tuesday.
 This is to allow officials to assess whether Amazon is taking adequate precautions to protect its staff.
 The company has previously said it abides by health and safety guidelines.
 The court said Amazon had “failed to recognise its obligations regarding the security and health of its workers”. The company faces a fine of €1m ($1.1m) per day if it fails to comply.
 Amazon has hired thousands of extra staff worldwide as business booms in countries where shops have closed and lockdowns have been imposed.
 In France, a nationwide ban on public gatherings has been in place since mid-March. President Emmanuel Macron has extended the lockdown measures until 11 May.
 The company’s French arm was taken to court by the trade union group Solidaires Unitaires Démocratiques (Sud), which claimed that more than 100 workers were being forced to work in close proximity to one another.
 Amazon also came under scrutiny from labour inspectors who ordered it to improve the conditions at five of its sites.
 Some unions had called for the complete closure of the company in the country. Failing that, they had asked for stricter restrictions on what kind of deliveries it could carry out.
 Last month, the country’s finance minister Bruno Le Maire said that Amazon was putting “unacceptable” pressure on its workers by refusing to pay them if they didn’t go into work.
 And earlier in March, several hundred Amazon workers held a walk-out in protest against the company. 
 Unions have argued that Amazon delivers very few groceries, while many of its deliveries are non-essential.
 Richard Vives, a worker from the CAT union, told Reuters news agency last month: “We feel really unsafe and I’ve got colleagues who are coming to work feeling fearful.”
 Julien Vincent, a union representative, also told Reuters that he estimated around 30% to 40% of the company’s employees in France had stopped going into work - either out of fear of catching the coronavirus or because they were unable to arrange or afford childcare after schools were closed.
 Amazon has denied claims it is not taking sufficient measures to protect its staff. 
 It says it has brought in stricter cleaning protocols and has ensured that “employees can keep the necessary distance from one another”.
 It has also said it will introduce temperature checks and face masks for staff at all of its US and European warehouses.
 The Commission president says Italy was let down by "too many" at the start of the outbreak.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-europe-52285301
