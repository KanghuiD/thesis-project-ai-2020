   
 While Victorians are urged to stay at home as much as possible, families can leave their homes to take their children to and from an early childhood service. 
 You will also need to lodge a notification with our Quality Assessment and Regulation Division (QARD) if: 
 The Department has established a dedicated coronavirus (COVID-19) advice line for early childhood staff. This advice line is available during business hours.
 If your service is directed to close as a preventative measure, you'll be advised directly and given detailed advice and communication resources to support you through this process.
          Emergency closures
 If your service closes, you're required to report closure to the Regulatory Authority.
 Most Victorian government primary and secondary school students are learning from home in Term 2. 
 The type and frequency of contact with families will depend on each service’s individual circumstances and resources. It is understood that staff occupied with delivering the on-site program will have limited capacity to engage with other families.
 We have provided information about resources for home learning for children aged three to five on our Learning from Home webpage.
 We encourage you to connect with families at home and support them to use these resources, where possible.
 We're offering free webinars for early childhood teachers and educators. These will help increase their knowledge and understanding of best practice teaching and learning approaches for children taking part in learning from home.
 Find out more about the 
          learning from home webinars.
                   talking to their children about coronavirus (COVID-19). 
 For example, from the Royal Children's Hospital:
 OSHC programs may continue to operate and liaise with schools to provide care for their school communities during school holidays, wherever possible.
 OSHC and Vacation Care programs will not be able to operate from schools that have been directed by DHHS to close due to coronavirus (COVID-19). Programs cannot operate from these schools until advised otherwise.
 If OSHC and Vacation Care service providers are advised they cannot operate from a school and would like to operate from another location, they will need to lodge an application for service approval through the National Quality Agenda IT Service portal and contact the Regulatory Authority’s advice line on 1300 307 415. These applications will be considered as a priority.
 OSHC providers can provide education and care on official pupil-free days, where this is consistent with their service approval.  If a variation to a service approval is required, please contact the Regulatory Authority.
 On 2 April 2020, the Commonwealth Government announced the 
          Early Childhood Education and Care Relief Package.
 More information about the Relief Package can be found at:
 The Relief Package may be used in conjunction with the 
          JobKeeper wage subsidy scheme, for eligible providers. 
          SA12 Notification of Change of Information About an Approved Service on the 
          National Quality Agenda IT Service portal or contact 
  All excursions should be postponed or cancelled. This applies to excursions to public venues outside the centre, including bush kindergarten.
 At the direction of the Chief Health Officer, Parks Victoria has closed a number of high-visitation parks across Victoria to help slow the spread of coronavirus (COVID-19). This includes closing until further notice: 
 You may apply for a service waiver (for ongoing issues) or temporary waiver (for issues that can be addressed within 12 months) due to the coronavirus (COVID-19) pandemic.
 Waivers can be sought for staffing qualifications. A detailed list of requirements which may be waived can be found in the 
          Guide to National Quality Framework - Waivers.
 To apply for a waiver, log onto the 
          National Quality Agenda (NQA) IT Service portal. The Department will not charge an application fee for waivers during the coronavirus (COVID-19) pandemic. 
 This includes:
 The role of an early childhood teacher may be filled by an educator actively working towards or holding a diploma level qualification, in extenuating circumstances related to coronavirus (COVID-19).
 In calculating educator to child ratios, ratio requirements are determined by the age and number of children the service is educating and caring for at any one time.
 The educator to child ratio  calculator can help you calculate mixed age ratios.
 Only Australian citizens, residents and immediate family members are now able to travel to Australia. All travellers to Australia are required to self-isolate for 14 days in a hotel.
 This includes children and staff who may be returning from overseas excursions or other travel. Any person who is self-isolating must not visit an early childhood service to pick up children or for any other reason. 
 In addition, there should be full adherence to the NHMRC childcare cleaning guidelines, including:
 Alternative care arrangements should be considered for children who are in the high-risk category for coronavirus (COVID-19). AHPPC recommends parents seek medical advice for these children.
 The AHPPC has also advised that the following visitors and staff should not be permitted entry into an early childhood service:
 The Department will keep you informed as to how best implement these guidelines.
          detailed practice guidance around physical distancing and transmission reduction in ECEC settings (docx - 333.92kb) with particular focus on the following:
 Contact DHHS on 1300 651 160 to discuss what to do next if you think a child, student or staff member:
 DHHS defines ‘close contact’ as someone who has either:
 Unless you have a child, student or staff member in one of the two above categories, you do not need to take further action.
 You should continue to encourage good hygiene practices as this is the most effective way of minimising the spread of coronavirus (COVID-19).
 For reporting requirements:
 Health promotion materials are available to distribute and promote to your staff, children and families, see:
 You can also access the 
          NHMRC childcare cleaning guidelines.
 Victoria's testing criteria has expanded to include testing for a range of workers in public facing roles. This will now include any early childhood education and care staff with 
          symptoms of coronavirus (COVID-19) who have worked with children in the past 14 days.
 Unwell children and staff should stay at home until their symptoms resolve and seek medical assistance as required.
 We know that some larger ECEC providers have also successfully sourced supplies from wholesale providers who ordinarily service the hospitality industry. This information is being shared with peak bodies.
 If a lack of essential supplies is threatening the continued operation of your service in accordance with health guidance, contact the DET advice line on 1800 338 663. 
 If first aid qualification requirements cannot be met, you may apply for a temporary waiver if staff have been unable to complete first aid refresher training due to coronavirus (COVID-19) as outlined above.
 Contact the Quality Assessment and Regulation Division (QARD) advice line on 1300 307 415 for more about applying for waivers or first aid requirements.
 While continuing to educate and care for children, and being a source of support for their families, it is important to look out for one another and stay connected. 
 The following organisations give advice on looking after your mental health at this time:
 The Department will continue to provide all existing streams of kindergarten funding, including: 
          
 Funding for programs that support kindergarten participation, including Access to Early Learning and Best Start, will also continue in full for all sites. The Department recognises that some program activities will need to be paused or adapted given current circumstances.
 The Victorian Government has announced it will provide 
          up to $45 million in extra funding to support sessional kindergarten providers, which have seen numbers decline significantly due to coronavirus (COVID-19). 
 The funding will provide close to $500 per child in a funded kindergarten program for Term 2 (excluding those that already attract a Kindergarten Fee Subsidy or Early Start Kindergarten), based on the annual number of children enrolled through the 
          Kindergarten Information Management (KIM) system. The service will need to remain open (unless directed by DHHS or agreed with DET), however this will apply to enrolled children whether they are attending the service or not.
 The funding will be available for community-based, local government and school providers that are offering government-funded sessional programs. The funding will be provided for government-funded kindergarten programs: Four-Year-Old Kindergarten, and funded Three-Year-Old Kindergarten, where this has been 
 To be eligible for the additional funding, conditions include that providers must:
 Eligibility is not based on the number of children who attend.
 The Commonwealth Government has announced a funding package for the early childhood education and care sector.
 More information about the childcare package can be found on the Commonwealth's Department of Education, Skills and Employment website, including FAQs.
 Information is available on the:
 On Monday 30 March 2020, the Commonwealth Government announced a $139 billion JobKeeper package to support businesses and help keep workers in jobs.
 The wage subsidy is available to eligible businesses with a payment of $1500 per fortnight for each employee for up to six months.
 Visit the 
          Commonwealth’s JobKeeper webpage for more information. 
 Kindergarten Inclusion Support (KIS) additional assistants placed with a kindergarten service should continue to be paid, even if the service they have been allocated to is not currently operating an on-site program, or the child with additional needs is not currently attending. 
 In these cases, the kindergarten service, KIS providers and KIS additional assistants should agree how the additional assistants can be appropriately used until normal program delivery is resumed.
 Regional Advisory Groups will continue to assess new applications for the KIS program, subject to confirmation that the child is currently attending their kindergarten program in person.
 Preschool Field Officers (PSFOs) may still visit a service using appropriate physical distancing. PSFOs can also use video conferencing if remote delivery is agreed. 
 With many children not attending their kindergarten program, PSFOs may also be called on to assist teachers to support learning in the home, in line with the information and resources being made available by the Department.
 Service providers are asked to complete their Annual Confirmation and submit Early Start Kindergarten enrolments through the Kindergarten Information Management (KIM) system as soon as possible. As per normal practice, new Early Start Kindergarten enrolments can be added any time. 
 If a child is enrolled in the kindergarten program but not attending because of parent concerns about coronavirus (COVID-19), they should still be included in the annual confirmation.
 No service will be disadvantaged in accessing their normal review processes.
 Where possible, we'll continue to progress the completion of assessment and rating reports currently in progress.
 If this is not possible, we'll extend all process timelines accordingly.
 To make sure resources are best focussed on responding to and managing coronavirus (COVID-19), we're postponing all assessment and rating visits until further notice.
  Last Update: 23 April 2020
 We respectfully acknowledge the Traditional Owners of country throughout Victoria and pay respect to the ongoing living cultures of First Peoples.                                                                        
 Our website uses a free tool to translate into other languages. This tool is a guide and may not be accurate. For more, see: Information in your language
 https://www.education.vic.gov.au/childhood/Pages/coronavirus-advice-early-childhood.aspx
