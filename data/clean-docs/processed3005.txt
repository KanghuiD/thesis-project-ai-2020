Coronavirus — News | The Colorado Sun
 As another $310 billion in forgivable Small Business Administration loans open up, confusion continues to reign.
 The Denver Metro Emergency Food Network hopes to deliver 250,000 meals by June 1. And they want to extend the network far beyond the coronavirus crisis.
 Late-night phone calls and leveraged international relationships have been necessary to secure equipment the state needs. “There’s incredible competition,” said Sarah Tuneberg, who runs the state’s innovation response team.
 Under new restrictions, child care facilities can have no more than 10 kids per room. That means fewer spots for parents -- and less profit for providers.
 Gov. Jared Polis vowed to make sure Coloradans had plenty of time to digest the specifics, but his safer-at-home directive was revealed about six hours before his statewide stay-at-home order was set to expire
 Colorado’s system of 52 school-based health centers has grown into a crucial element of overall population health, delivering more than 100,000 visits last school year.
 As a heavy spring snow blanketed the state on Thursday, April 16, journalists from news organizations across Colorado set out to chronicle a day in the life of the state’s residents during this extraordinary time
 “There’s about a million fires every day,” Colorado's governor says
 https://coloradosun.com/category/news/coronavirus/
