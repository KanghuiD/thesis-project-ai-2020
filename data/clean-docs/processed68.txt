Coronavirus: Clarks to permanently shut some stores - Retail Gazette
 Clarks is reportedly preparing to permanently close some of its stores and has drafted in bankers to review its finances amid the coronavirus pandemic.
 The footwear retailer has struggled to deliver a turnaround plan and has consequently made the decision not to reopen “a small number” of its 347 UK store estate once the government-mandated lockdown ends, Sky News reported.
 Clarks had appointed investment bank Rothschild to manage the business’ financing options, as well as access new borrowing facilities.
 The retailer has furloughed thousands of its store staff under the government’s Coronavirus Job Retention Scheme, and is currently assessing options for the remainder of its workforce.
 Clarks has been working with the management consultant McKinsey on a new corporate blueprint.
 However, the footwear retailer is now prioritising how best to survive during the pandemic.
 Clarks is reviewing all stores to ensure that they are the right size and located in the right areas.
 As part of the “normal review” the retailer decided not to renew the leases on a small number of stores.
 Last month, Centre for Retail Research estimated that over 20,600 stores are expected to have pulled their shutters down for the final time by the end of the year – a massive leap on the 4547 that closed in 2019.
 As a result, job losses are predicted to reach more than 235,000, up from less than 93,000 last year. That is both in closing stores and in those cutting back on staff.
 Let’s face it. Many retail groups are going to do this. Many branches of certain chains won’t reopen once this is done and dusted. There will be a glut of new retail stores on the market to let and some of those won’t be shops again.
   
  Editorial: 0208 222 0503  Commercial: 07401 241 979
 UK: Four Cavendish Square, Marylebone, London, W1G 0PG
 https://www.retailgazette.co.uk/blog/2020/04/coronavirus-clarks-to-permanently-shut-some-stores/
