Coronavirus: India cannot become a epicenter, World Bank says
 Keep Me Logged In
 The world cannot allow India to become the next coronavirus epicenter, said the World Bank's country director for India, Junaid Ahmad.
 "Governments around the world (are) having to slow down the economy to give a chance for the health sector to recuperate," Ahmad told CNBC's "Street Signs" before India extended its lockdown. "I think that's a very big difference in the way governments have been intervening."
 He explained that as a result of the slowdown, governments — and India, in particular — have taken steps to protect vulnerable members of society and safeguard sectors in their countries through a combination of fiscal and monetary mechanisms. The world is "bracing for probably the worst recession since the post-war era," Ahmad said. 
 The number of reported cases in India has climbed despite efforts from the government to keep people indoors since late March. Data posted on the health ministry's website showed 11,439 confirmed cases of infection as of April 15, 8 a.m. local time; 1,305 patients have been discharged, while 377 people have died.
 India's topdown approach to implementing the initial countrywide lockdown was "critical" in slowing the rapid spread of the coronavirus, according to Ahmad. 
 "Under no circumstances can the world, the region, or India, allow an epicenter to emerge in India. I think that the whole approach of a lockdown nationally and to really push on the health side has been very important," he said.
 India watchers have argued that the country's health-care infrastructure is not equipped to handle the kind of outbreak seen in the United States or in several Western European countries like Spain and Italy. 
 Part of the reason is that India spends comparatively less on public health than other nations such as the U.S. and there are fewer per capita hospital beds and doctors available to treat its massive 1.3 billion population. 
 That could be part of the reason why the government took such drastic actions to lock down the entire country in a bid to stem the virus' spread. 
 To mitigate the negative impact on low-income households and daily wage earners, who are often part of the massive unorganized sector of the economy, India announced a fiscal package worth $22.5 billion. For its part, the World Bank approved a $1 billion project to help India prevent, detect and respond to the Covid-19 pandemic through measures that can reduce local transmission and strengthen the country's public health preparedness. 
 "The World Bank has invested a billion dollars in partnership with Government of India on the health side, to look at community surveillance, to ensure testing, to ensure isolation, to ensure that medicine is available – if needed, importing of ventilators, purchase of ventilators," Ahmad said. "The whole nine yards in, if you will, the health expenditure and health intervention."
 Asked if India has enough fiscal room left to bring in more financial support to the economy, Ahmad said there's some positive news: India's stock of debt is not that high, it is based on the local currency and is not based on short-term borrowing, he explained.
 "The issue is not about how much fiscal space there is, the issue is: where will (the) government intervene and how will it intervene," Ahmad added. 
 Got a confidential news tip? We want to hear from you.
 Data is a real-time snapshot *Data is delayed at least 15 minutes. Global Business and Financial News, Stock Quotes, and Market Data and Analysis.
 Data also provided by 
 https://www.cnbc.com/2020/04/15/coronavirus-india-cannot-become-a-epicenter-world-bank-says.html
