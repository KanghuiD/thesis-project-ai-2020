Opinion | Why Black Americans Are Hit Harder by the Coronavirus - The New York Times
 letters
 How systemic inequality is playing out in the pandemic. Also: Sanctions on Iran; the changing face of grief; domestic abuse in the immigrant community; social distancing on sidewalks.
 To the Editor:
 Re “Black Americans Bear the Brunt as Deaths Climb” (front page, April 8) and “The Racial Time Bomb in the Covid-19 Crisis,” by Charles M. Blow (column, nytimes.com, April 1):
 Though the virus may not be racist, structural inequalities — the sturdy products of racial discrimination — shape health outcomes.
 The devastation handed to us after this pandemic will be no exception. This raises the question of how we engage some of our most systemically vulnerable populations.
 First, we need more states to follow New York, Michigan, Louisiana and the Carolinas and make Covid-19 outcomes by race and ethnicity available. Don’t keep people in the dark, including local advocates who can develop strategic interventions.
 Next, be inclusive of more disenfranchised populations in public health messaging by drumming home more nuanced statements like “if you have had limited access to quality health care before now, be especially vigilant for progression of your symptoms.”
 I learned last week that a neighbor of mine, a man in his early 60s and African-American like me, was ill with Covid-19 and died unexpectedly in his home. What were the barriers to his getting the care he so desperately needed? Was he unaware of some silent condition he had that was socially determined by things like chronic stress, income inequality and discrimination?
 Did he distrust a broken health care system and not go to the hospital at some critical juncture? Would it have made a difference?
 I’m haunted by these questions and the implications of them for so many others.
 Angela CoombsNew YorkThe writer is a psychiatrist at Columbia University and the New York State Psychiatric Institute.
 Re “Iran, With Over 47,000 Cases, Says U.S. Sanctions Are Taking Lives” (news article, April 2):
 The United States has a chance to right its reputation with the rest of the world and come to the aid of the citizens of Iran. The humanitarian thing to do would be to lift the sanctions right now while Iranians are dying of a virus they are unable to combat because we have strangled their economy and their health care systems with our avenging sanctions.
 Secretary of State Mike Pompeo has an opportunity now to make us proud that we are Americans.
 Evelyn WolfsonWayland, Mass.
 Re “We Will Need New Ways to Grieve,” by Beth Waltemath (Sunday Review, April 5):
 Thank you for this touching article. I would like to recommend Dr. Pauline Boss’s book “Ambiguous Loss,” which deals with the complications in grieving, and its resolution, when the usual expected rituals are absent.
 We saw this on 9/11, when the loved one never came home from work that day, and the family had no body to bury. We are now faced with a situation in which we have too many bodies, but we are forbidden to engage in all the usual supportive ritual gatherings of family and friends.
 We cannot be with the mourner in the usual ways. Things feel very strange and out of order.
 Karen GreeneNew YorkThe writer is a clinical psychologist.
 Re “The Wall That Didn’t Stop Covid-19” (editorial, April 1):
 For immigrant survivors of gender-based violence, many of whom are living with the effects of trauma or coping with continuing abuse, this is an especially difficult time. The overwhelming gaps in the immigration, health care and public benefits systems work together to make immigrant survivors more vulnerable to abuse in the current environment.
 Without an income, survivors of violence become all the more dependent on abusive partners to provide shelter, food and access to health care. The power dynamics of abuse are therefore worsened. With an abuser in the home around the clock, the opportunities to organize a departure or meet with helping professionals are all but eliminated.
 That’s on top of halted immigration adjudications and continued operations by Immigration and Customs Enforcement, further worsening the complex situation immigrant survivors face.
 Immigrant survivors deserve more attention and assistance as we continue to mitigate this pandemic.
 Archi PyatiFalls Church, Va.The writer is chief of policy and communications at the Tahirih Justice Center.
 Re “Rules for Using the Sidewalk During the Coronavirus” (Op-Ed, nytimes.com, April 5):
 I couldn’t agree more with Eleanor Barkhorn’s complaints about sidewalk etiquette in this time of six-foot social distancing.
 After many a frustrating walk for fresh air, my husband and I thought of a simple solution: a temporary, citywide rule that mandates one-way traffic on all sidewalks.
 It’s the two-way traffic on narrow walkways that presents the biggest threat to social distancing, so let’s change our collective behavior and do our best to stay on the right side of the road.
 Of course, we’d need to work out some kinks, like what to do when you enter the sidewalk mid-block and need to turn left. Cross in the middle of the street, or walk on the left until you arrive safely at a crosswalk?
 Surely we can make some exceptions. But if everyone follows this basic rule, pedestrian life in New York City will be more anxiety-free for everyone.
 Missy KurzweilBrooklyn
 https://www.nytimes.com/2020/04/08/opinion/letters/coronavirus-race-inequality.html
