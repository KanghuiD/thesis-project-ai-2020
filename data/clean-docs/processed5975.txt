Opinion - Scientific American
 Celebrating 175 Years of Discovery
 A new documentary, The Social Dilemma, warns that the harm wreaked by digital technologies is outweighing the benefits
 For all of its flaws, the U.S. electoral process is relatively transparent, which makes the likelihood that we find ourselves in a constitutional crisis lower than you might think
 It’ll be messy, but we have the tools and the technology to ensure that everyone has an equal opportunity to cast a vote and have it counted
 What if the signature of living things is the lack of a simple signature?
 It’s immoral to saddle today’s young people with an inheritance of environmental catastrophe
 Seismic surveys looking for oil and gas deposits are interfering with cetacean communication, likely stressing the marine mammals
 A little-known provision in the 2020 Democratic Party platform could make a huge difference if it is enacted
 Proponents say yes, but scientific studies on the effectiveness of this chemical derived from cannabis are a mixed bag
 Elections aren’t won on the basis of policies; they’re won on the basis of the stories each side tells about itself and its values
 This pandemic is far from over, and we aren’t doing enough to stop it
 Discover world-changing science. Explore our digital archive back to 1845, including articles by more than 150 Nobel Prize winners.
 Follow us
 Scientific american arabic
 You have  free articles left.
 Support our award-winning coverage of advances in science & technology.
 See Subscription Options
 https://www.scientificamerican.com/section/opinion/
