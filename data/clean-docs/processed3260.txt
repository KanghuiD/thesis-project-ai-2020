West Seattle Blog… | CORONAVIRUS: Tuesday 3/31 roundup
 West Seattle, Washington
 The first full calendar month of the outbreak in King County is concluding. Here’s our nightly roundup:
 NO NEW KING COUNTY/STATE CASE NUMBERS: Neither King County nor the state had new case numbers today. The former explains:
 The Washington State Department of Health (DOH) is experiencing technical difficulties with their COVID-19 data, which is delaying the Public Health data report of new cases and deaths for 3/31/20. Public Health expects to update again on 4/1/20.
 QUARANTINE UPDATE: The county’s daily update did note, “20 people are currently staying in King County isolation and quarantine facilities.” The one in Top Hat, east of White Center, has not opened yet but was projected to be ready in “early April.” We noted a few days ago that the additional modular housing had been moved onto the area where an old office building was demolished. We’ll get an update on its status tomorrow during the weekly phone-conference community meeting with county reps.
 SOME PARKING ENFORCEMENT SUSPENDED, BUT NOT ALL: We had sent the mayor’s office a question last week, to date unanswered, on behalf of a reader who thought ticketing people in a 2-hour zone east of a mostly shuttered business district was a bit harsh, considering everyone is under orders to stay home. This provides something of an answer: The city reiterated today that while it’s changed some parking policies because of the COVID-19 pandemic, it’s still enforcing most parking rules. 
 COMMUNITY CLEANUPS CANCELED: The city says you should still keep watch on the area outside your home, but it’s calling off Adopt-A-Street, Spring Clean, etc., TFN.
 CONFUSED ABOUT THOSE FEDERAL PAYMENTS? Are you getting one? How will you get it? The IRS has a FAQ page.
 GOT A RUMOR TO CHECK? We’re partial to Snopes, but we noticed today that even FEMA has a rumor-checking page.
 LOTS OF SCAMS, TOO: We mentioned again in West Seattle Crime Watch today that scammers continue their evildoing. Here’s yet another page with numerous warnings to heed.
 AND PRICE-GOUGING: State Attorney General Bob Ferguson has sent letters to five online retailers telling them to stop or else, and expects he’ll be sending moe.
 LOTS OF GOOD OUT THERE, TOO: This morning we spotlighted Highland Park Improvement Club‘s new “Through the Windowpane” neighbor-to-neighbor program.
 LAST BUT NOT LEAST: Just got this from Stewart L. – seen from Harbor Avenue SW:
 GOT INFO? westseattleblog@gmail.com or text/voice 206-293-6302 – thank you!
 Any way we can learn who’s price gouging so we know to not support their online business? Pretty heartless to price gouge during this crisis 
 I didn’t see a link in the AG’s news release but this one is in the Times roundup, showing the letters, apparently from the AG’s site.
 https://agportal-s3bucket.s3.amazonaws.com/uploadedfiles/Another/News/Press_Releases/DemandLetters032620.pdf
 While I understand the desire to make a profit on what you sell, but this type of markup is insane. Sanitizer and the like shouldn’t be priced as a collector’s item. 
 From experiance, trying to work with the AG as a consumer is completly fruitless. Im confused about the uninterrupted grocery supply chain. Things like cat litter are slim pickins. Have peeps givin up on TP?
 No yeast in stores. $30-60 for 3 envelopes from 3rd party sellers on Amazon.  Talk about price gouging.  I’m substituting buttermilk. When did people who buy pre peeled eggs and boxed mixes become master bakers?
 1. Try growing a wild sourdough starter.   2.Consider quick breads with chemical leavening.   3.Try whipping egg whites to leaven cakes the old fashioned way.   4.Harvest yeast from fresh bottle conditioned beer or talk to your local brewer about buying a little spent yeast.
 Username: 
 Password: 
 Lost Password?
  Enter the code in the image*: 
 West Seattle Blog 
  3 months ago 
 New owners for a West Seattle landmark! westseattleblog.com/2020/01/biznote-passing-of-the-torch-at-poggie-tavern-new-ownership-after-23-... ... See MoreSee Less
 BIZNOTE: 'Passing of the torch' at Poggie Tavern - new ownership after 23 years
 westseattleblog.com
 Avoid 18th/Cambridge. Big SFD response for shed burning behind a house. Updating on WSB at westseattleblog.com/2020/01/seattle-fire-full-response-in-south-delridge-2/ ... See MoreSee Less
 https://westseattleblog.com/2020/03/coronavirus-tuesday-3-31-roundup/
