	Coronavirus (COVID-19) - HSE.ie
 our health service
 Last updated: 28 April 2020 at 4.15pm
 COVID-19 is a new illness that can affect your lungs and airways. It's caused by a virus called coronavirus.
 It can take up to 14 days for symptoms of coronavirus to appear. They can be similar to the symptoms of cold and flu.
 Common symptoms of coronavirus include:
 For people who get infected with coronavirus:
 Read more about the symptoms and causes of coronavirus
 Everyone needs to stay at home to help slow the spread of coronavirus. You should only leave for essential reasons. This is the best way to protect your friends, families and communities.
 It’s important to practice social distancing and wash your hands properly and often.
 Follow this advice to protect yourself and others from coronavirus
 To help stop the spread of coronavirus everyone has been asked to stay at home. But some people may need to do more than this.
 You may need to either:
 You do these things to stop other people from getting coronavirus.
 Read more about restricted movements and self-isolation
 There are some groups of people who may be at risk of serious illness if they catch coronavirus.  This is similar to other infections such as flu.
 People in these at-risk groups will be prioritised for tests if they have symptoms of coronavirus.
 Read more about who is included in the at-risk groups
 Cocooning means protecting people over 70 years of age, people who are extremely medically vulnerable and people living in residential care homes or long-term care. If you are in one of these groups, you must take extra care to reduce interaction with other people. Do not go outside your home and garden.
 Read more about cocooning
 Phone your GP to be assessed for a coronavirus test if you are in one of these situations:
 You suddenly experience one of these symptoms and there is no other obvious cause:
 You have been in close contact with someone you think or know has coronavirus in the last 14 days and suddenly experience one of these symptoms:
 You should self-isolate if either of these situations apply to you.
 Read more about the GP assessment and testing for coronavirus
 If you test positive for coronavirus we will ask you about other people you've been in close contact with. We will also do this if it is likely that you have coronavirus but have not yet had a test.
 This is 'contact tracing'. The people who do it are 'contact tracers'.
 Read more about contact tracing.
 There is no specific treatment for coronavirus. But many of the symptoms of coronavirus can be treated at home. Take any medication you are already taking as usual, unless you are told not to by a healthcare professional.
 Most people who catch coronavirus will experience mild symptoms. They should make a full recovery without needing to go to hospital.
 If your symptoms get worse and you feel very unwell you may need to go to hospital.
 Read more about how to treat the symptoms of coronavirus
 Read more about medicines and taking your medications during coronavirus
 When you are pregnant, your body naturally changes your immune system. This is to help your pregnancy continue successfully. It means that when you are pregnant you may pick up some infections more easily.
 All pregnant women should get the flu vaccine. This will protect you and your baby from the flu, not from coronavirus.
 Read more about pregnancy and coronavirus
 Around the world, very few cases of coronavirus have been reported in children. Children can sometimes get the virus. But they seem to get a milder infection than adults or older people do.
 Children may also play a big role in spreading the virus. That’s why they should stay away from at-risk groups, such as people over the age of 60. This is because older people, or people in at-risk groups, might have a harder time fighting the virus.
 Encourage your child to wash their hands regularly and properly.
 Read more about how to clean your hands 
 Read more advice for parents and children during the coronavirus outbreak
 You will need to self-isolate for 14 days if you arrive in Ireland from any other country. This includes Irish citizens coming home.
 Self-isolation means staying indoors and completely avoiding contact with other people.
 You will be asked to fill in a form before you arrive in Ireland called the Public Health Passenger Locator Form.
 Download the Public Health Passenger Locator Form (PDF, 704 KB, 4 pages)
 You may be contacted during the 14 days after you arrive in Ireland to check that you are self-isolating.
 You do not need to self-isolate if you:
 Follow this advice to protect yourself and others from coronavirus.
 If you have symptoms of coronavirus
 If you develop symptoms of coronavirus, you will need to self-isolate and phone your GP. Do not go to a GP surgery, pharmacy or hospital. The GP will assess you over the phone. If they think you need to be tested for coronavirus, they will arrange a test.
 Find contact details for your GP
 Avoid all non-essential travel to other countries.
 Read the latest COVID-19 Travel Advisory travel information from the Department of Foreign Affairs.
 Travel to areas with widespread local transmission
 If you travel to an area with widespread local transmission of coronavirus, the risk of catching the virus is high. The World Health Organisation (WHO) publish daily information on rates of local transmission.
 There is no evidence that pets such as cats and dogs can spread coronavirus to humans.
 As a general precaution, it is best to protect yourself by properly cleaning your hands often when in contact with pets.
 Deaf Irish Sign Language users can get information about coronavirus using Irish Remote Interpreting Service (IRIS).
 IRIS is available from 9am to 7pm, Monday to Friday and 10am to 4pm on Saturday.
 This service is for Deaf Irish Sign Language users only.
 Watch more Irish Sign Language videos about coronavirus
 Department of Health - latest updates on COVID-19 (Coronavirus)
 The Health Protection Surveillance Centre – information for health professionals
 Read about hospital service disruptions and visiting restrictions
 HSE.ie
 Got a question?
 We're here to help.
   
 Callsave: 1850 24 1850
 Tweet: @HSELive
 Monday to Friday: 8am - 8pm
 Saturday and Sunday: 9am - 5pm
 Some text in the modal.
 https://www2.hse.ie/conditions/coronavirus/coronavirus.html
