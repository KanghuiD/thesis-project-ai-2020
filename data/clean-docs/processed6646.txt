Coronavirus: Austria and Italy reopen some shops as lockdown eased - BBC News
 Thousands of shops have reopened in Austria, as it seeks to ease restrictions brought in to stem the spread of the coronavirus.
 Garden centres, DIY stores and small shops can open but with strict rules on social distancing. 
 In Italy, where over 20,000 people have died in the pandemic, a limited number of shops and businesses have been allowed to reopen.
 But some of the worst-hit regions have decided to hold off.
 Lombardy and other regions in the north will maintain their measures for longer.
 After five weeks under lockdown, shops selling books, stationery and clothes for babies and young children can reopen their doors but with strict rules on customer numbers and hygiene.
 Meanwhile, Denmark has said it intends to ease its lockdown faster than originally planned.
 The European Commission has urged EU countries to co-ordinate with each other to relax measures gradually.
 An internal document sent by the Commission to EU governments said that even phased measures would "unavoidably lead to a corresponding increase in new cases". 
 Lifting restrictions had to wait for the spread of the virus to be reduced for a significant period, it said, with enough capacity in intensive care units to cope with a second surge in cases and increased testing, along with mass antibody tests. 
 Businesses, shops and schools should lead the way, followed by limited reopening of restaurants, bars and cafes.
 In other developments:
 Spain reported its lowest increase in infections since 20 March on Tuesday. There has been a 1.8% increase from Monday to 3,045. The number of deaths has gone up by 567 to 18,056. 
 President Emmanuel Macron extended France's lockdown for another four weeks until 11 May, as he said current restrictions had slowed the virus but not beaten it.
 The head of Germany's Robert Koch public health institute said that while there had been a slowdown it was too early to talk of a clear trend.
 The World Health Organization (WHO) welcomed the slowing down of infections in some European countries but warned against lifting restrictions too early, so as not to prompt "a deadly resurgence".
 Austria was one of the first European countries to follow neighbouring Italy in imposing strict lockdown measures about a month ago, and the government says it has managed to flatten the curve of new infections. It has so far reported about 14,000 cases and 368 deaths.
 Last week, Chancellor Sebastian Kurz unveiled plans to lift restrictions gradually.
 From Tuesday, shops under 400 sq m (4,300 sq ft) in size are allowed to reopen, along with hardware stores and garden centres. 
 But it is also compulsory for people to wear a mask in supermarkets and pharmacies.
 "Experience in countries that have handled it well has taught us that we have to move gradually," Economy Minister Margarete Schramböck told Austrian TV.
 Larger shops, shopping centres and hairdressers are due to reopen from 1 May, while restaurants and hotels could reopen from mid-May if health conditions allow, Austria's chancellor has said.
 The reopening of bookshops and clothes stores for young children is a glimmer of hope, after Italy saw 20,465 fatalities, second only to the US in the number of deaths officially caused by Covid-19.
 The daily death toll is now falling and the number of patients in intensive care has dropped for 10 days in a row, to 3,260. 
 But Lombardy, where Italy's outbreak began, still saw another 280 deaths declared on Monday, and officials there have decided to allow children's clothing shops to open, but not bookshops.
 Also in the north, some of the worst-affected areas of Emilia-Romagna will stay locked down, while in Veneto officials are talking of a "soft lockdown". Bookshops and clothing stores can open for two days a week, says Veneto governor Luca Zaia, and a ban on exercising more than 200m away from home has been lifted. But residents will still need to wear a mask and gloves if they go out.
 In his address on Monday evening, President Macron said the outbreak in France was "beginning to steady... (and) hope is returning".
 The daily number of deaths rose slightly, taking the country's total to just under 15,000.
 "The eleventh of May will be the start of a new phase," Mr Macron said. "It will be progressive and the rules can be adapted according to our results."
 He said schools would gradually reopen after the new extension but restaurants would stay closed and there could be no festivals until mid-July. He said the most vulnerable people should remain in isolation even after the rules were eased.
 Lothar Wieler, the head of Germany's Robert Koch Institute, said on Tuesday that infections were now levelling off, but at a relatively high level. Germany has seen 2,969 deaths, according to RKI figures, although Johns Hopkins University in the US puts the number at almost 3,200.
 Chancellor Angela Merkel will hold talks on Wednesday with regional leaders on how to exit the country's lockdown.
 "We cannot yet conclusively determine whether the number of cases is actually falling," Mr Wieler said. The RKI is aiming to bring down the infection rate from 1.2 per person now to below one. 
 A nursing home in Rümpel in the northern state of Schleswig-Holstein has gone into quarantine after 53 residents and 19 staff tested positive for coronavirus.
 Meanwhile, the number of people who have died with the virus in the Netherlands increased by 86 to 2,823 on Monday. The number of confirmed cases rose by almost 1,000.
 Cases and deaths also increased in Belgium, which is now among the worst-affected countries in Europe. More than 4,000 people have now died with Covid-19 in the country, officials said.
 The World Health Organization "failed in its basic duty" over coronavirus, Donald Trump says.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-europe-52275959
