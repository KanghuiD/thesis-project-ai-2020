Coronavirus Live Blog: COVID-19 cases rise to 3,214, deaths increase to 137; new emergency directive addresses graduations
 With the number of coronavirus cases in the Silver State on the rise, The Nevada Independent will be keeping you up to date on the latest here, both through regular live blog updates and updates to our infographic tracking cases around Nevada. The most recent updates will be posted at the top.
 Resources:
 
 Note: The default view of the above spreadsheet shows positive cases as reported by the counties and the cumulative county-by-county total, which may differ from the total the state is reporting. Check the above infographic for the latest statewide case total.
 Wednesday state and county update: COVID-19 cases rise to 3,214, deaths increase to 137
 State and county officials are reporting that there are now 3,214 confirmed cases of the novel coronavirus statewide, up 68 from Tuesday evening’s total. The death toll is now at 137.
 Southern Nevada Health District officials announced Wednesday morning that there are 2,559 confirmed COVID-19 cases in Clark County, up 50 from Tuesday. There are now 115 deaths in the county associated with the virus, up nine from Tuesday.
 According to health district officials, 683 individuals with COVID-19 have been hospitalized in Clark County, an increase of 32 from Tuesday. Officials estimate that 1281 people have recovered from the virus.
 Washoe County Health District officials announced 17 new COVID-19 cases, bringing the countywide total to 549. Health district officials also announced one additional death, a man in his 70s, bringing the countywide death total to 14.
 According to the county, 32 people with COVID-19 are currently hospitalized, 41 have been discharged from the hospital, and 103 people have recovered from the virus.
 Health officials in Elko County reported one new case Wednesday, a woman in her 40s who is self-isolating at home. The case brings the county’s total to 12, including eight active cases, three recoveries and one death. 
 Officials also reported one additional case in Carson City and another in Lyon County Wednesday, bringing the number of cases in the four-county region encompassing Carson City-Lyon-Douglas-Storey County to 51, including 39 active cases and 12 recoveries. 
 Both cases, a Carson City man in his 30s and a Lyon County woman in her 60s, are self-isolating at home and remain in stable condition, authorities said. 
 — Last updated 4/15/20 at 8:24 p.m.
 State regulators issue cease and desist for coronavirus testing at Las Vegas urgent care facility
 At issue was the clinic’s use of a serology test — which tests for coronavirus antibodies in the bloodstream — as a means of diagnosing the coronavirus. Such tests only confirm an immune response, not the virus itself, and state health officials said the clinic must immediately notify all patients that their results are invalid for the purpose of diagnosing COVID-19. 
 In a statement, the state’s Division of Public and Behavioral Health said state law prohibits the operation of a medical laboratory without a license, and that the clinic remains under investigation for violating that law following a complaint investigation on Tuesday. 
 Wednesday’s announcement follows a report Tuesday from the Las Vegas Review-Journal that regulators had asked two Las Vegas facilities, Sahara West and Cura Telehealth, to stop conducting coronavirus testing. 
 Andrew Mann, a spokesperson for Sahara West, told The Nevada Independent that the clinic is “completely cooperating” with regulators and working to provide documentation that it remains in compliance with laboratory licensing laws. 
 Mann also said that the clinic believes new state regulations being placed on rapid testing, including the serology testing being conducted at Sahara West, is “onerous” to the point of making it “impossible” to conduct such tests at the urgent care clinic. 
 — Jacob Solis, 4/15/20 at 11:05 a.m.
 New emergency directive addresses high school graduations
 Gov. Steve Sisolak’s latest emergency directive gives the state superintendent power to temporarily waive or suspend laws or regulations that would place an “undue burden” on students’ ability to graduate high school
 The directive — which is the 14th the governor has issued since mid-March — builds on one he signed March 20, waiving state standardized tests and end-of-course exams. Schools statewide have been shut down since March 16 and will remain closed at least through April 30.
 The new directive also gives the State Board of Education the ability to approve amendments to school districts’ work-based learning plans, which will ensure students can earn the credit hours needed for a “College and Career Ready Diploma” despite the coronavirus disruptions.
 The amendments will be considered at a State Board of Education meeting April 30.
 “Due to COVID-19, the graduating class of 2020 will have to shift many of the celebrations and milestones that they have been looking forward to throughout their high school career,” Superintendent Jhone Ebert said in a statement. “One thing they will not have to sacrifice is their chance to demonstrate readiness for college and careers by earning their high school diploma.”
 — Jackie Valley, 4/15/2020 at 9:34 a.m.
 Tuesday state and county update: COVID-19 cases rise to 3,140, while the death toll reaches 130
 Southern Nevada health officials reported five more coronavirus deaths on Tuesday morning, pushing the county total to 106. 
 Statewide, there were 130 reported deaths from coronavirus as of Tuesday afternoon.
 The Southern Nevada Health District also announced the county case total had reached 2,509 — up 65 from the 2,444 reported Monday. Meanwhile, roughly 45 percent of the county’s cases — or 1,141 people — have recovered from COVID-19, health officials said.
 Washoe County announced 32 new confirmed cases of COVID-19 on Tuesday, bringing the county total to 532. Officials also announced a woman in her 50s with underlying conditions died after a COVID-19 diagnosis, bringing the Washoe County death toll to 13. 
 Churchill County authorities reported the county’s first coronavirus case on Monday. 
 On Tuesday evening, health authorities reported three more coronavirus cases in Douglas County, two more in Carson City and one more in Lyon County. That brings the statewide county case count up to 3,140.
 The new patients in Douglas County are a woman in her 50s, a girl under the age of 18, and a man in his 30s. Both Carson City cases are women in their 50s, while the new Lyon County case is a woman in her 40s, health officials said. All are self-isolating at home in stable condition.
 On Tuesday evening, Nye County officials reported four additional positive COVID-19 cases, including three in Tonopah and one in Pahrump. The county is now reporting 19 positive cases, with two recoveries. 
 Additionally, a spokesperson for the Reno-Sparks Indian Colony confirmed the reservation’s first positive coronavirus case to The Nevada Independent on Tuesday morning. According to a social media post from the tribal community, the resident is hospitalized. The Reno Sparks Tribal Health Center offers drive-through patient assessments and testing for the virus.
 Ten of Nevada’s 17 counties have reported coronavirus cases.
 — Last updated 4/14/20 at 6:47 p.m. 
 Nevada receives education funding from CARES Act
 A $200 million boost from the multi-trillion-dollar CARES Act is headed toward Nevada’s schools, Rep. Steven Horsford announced Tuesday.
 Officials said those funds also can be put toward child care and early childhood education, social and emotional support, and the protection of education-related jobs.
 “As Nevada’s students navigate this turbulent time alongside their families, I am working to make sure they have the resources they need to be cared for and continue to learn and grow,” Horsford said in a statement. “I am so encouraged to see the dedication of our state’s educators, and I hope this funding will afford them the resources they need to continue to do so during this pandemic.”
 — Jackie Valley, 4/14/20 at 3:25 p.m.
 Officials unveil plan to use Las Vegas Convention Center as overflow hospital if COVID-19 surges
 Southern Nevada officials announced a plan to convert a large expo hall in the Las Vegas Convention Center into a field hospital if a surge in coronavirus patients overwhelms the capacity of traditional hospitals.
 The proposed alternative care site, designed by the U.S. Army Corps of Engineers and described in a press conference Tuesday afternoon, would hold up to 900 beds in several distinct zones. The convention center’s South Hall would be partitioned with a network of temporary dividers and curtains and could be prepared with four days’ notice of a surge.
 “As of today, we are grateful to report that our hospitalization needs do not require the addition of this alternate care site, but we are ready to put this plan into action if we need it,” Clark County Commission Chairwoman Marilyn Kirkpatrick said in a statement. “As a community, our early actions have made a difference in the caseload we are seeing.”
 The site, built in a hall that encompasses nearly 1 million square feet, would have spaces for coronavirus patients who need medical attention that is lower than intensive care, as well as space for people without COVID-19 who need post-operative care before being discharged from a hospital.
 Officials expressed cautious optimism about whether the facility would be needed. 
 “Two weeks ago, I thought we might need it now and so we were really on the gas trying to get it done,” said Clark County Fire Chief John Steinbeck. “We’re not putting our foot on the brake at all, but we are taking it off the gas just a little bit.”
 University Medical Center CEO Mason VanHouweling said hospitals continue to keep a watchful eye on data such as intensive care unit occupancy and ventilator use to check for any signs of an oncoming surge. But he said that social distancing and moves to cancel outpatient surgeries have “had a huge impact on the acceleration of the cases here in Nevada.”
 Fermin Leguen, acting chief at the Southern Nevada Health District, tempered expectations about a downward trend.
 “At this point, there is not any reliable information in terms of trends of whether the disease is going away or staying the same,” he said.
 — Michelle Rindels, 4/14/20, 3:06 p.m.
 Monday state and county update: COVID-19 cases rise to 3,036; deaths up to 120
 County officials reported Monday a total of 3,036 confirmed COVID-19 cases statewide. By the afternoon, the state reported the death toll was 120, up from 114 earlier in the day.
 Humboldt County health officials on Monday also announced the county’s first coronavirus-related death —a man in his 40s who was a previously reported case.
 The Southern Nevada Health District announced Monday morning 120 new cases of COVID-19 and one additional death in Clark County, bringing total cases countywide to 2,444 and total deaths to 101. According to the health district, 623 people are hospitalized with the virus and 1,082 people are estimated to have recovered from it.
 Washoe County health officials reported an additional 39 cases Monday morning, bringing the countywide total to 500. Of those cases, 417 remain active and 31 individuals are currently hospitalized with the virus. 
 Officials also reported one death, raising the county’s death toll to 12, as well as 10 additional recoveries, bringing the total recovered cases to 71. 
 Hospital occupancy county-wide remains near half, with 53 percent of hospital beds occupied, 51 percent of intensive care rooms occupied and 20 percent of ventilators in-use as of Monday morning. 
 Nye County reported five more people in Pahrump tested positive for COVID-19, bringing the countywide number of cases up to 15. Carson City also reported one additional case.
 — Last updated 4/14/20 at 7:48 a.m.
 Slightly more than one-third of ventilators in Nevada in use
 State officials say 37 percent of the ventilators in Nevada are in use, reflecting a gradual drop in that rate over the last few days since last Tuesday when 47 percent were being used.
 The statistic came in the state’s daily pandemic situation report, which also noted that 68 percent of intensive care unit rooms in Nevada are in use. Last Tuesday, that number was 71 percent.
 On Wednesday, the state noted the arrival of 50 ventilators loaned from California for a three-week period.
 State officials also announced on Monday that 798 people had volunteered to be part of the Battle Born Medical Corps. Sixteen new people applied on Sunday.
 The majority of volunteers — 308 people — are nurses, while 122 are doctors.
 — Michelle Rindels, 4/13/20 at 6:42 p.m.
 First inmate in Nevada tests positive for COVID-19; man had been at Clark County Detention Center, is hospitalized
 In the first reported case of COVID-19 among inmates in Nevada, an inmate at the Clark County Detention Center tested positive for the virus.
 Las Vegas Metropolitan Police Department officials announced on Monday that the 31-year-old man is now being treated at University Medical Center. The agency said the inmate had been in the jail since November and was admitted to the hospital on Sunday after he began displaying symptoms.
 Three inmates in the unit were moved to negative air pressure cells and are being tested for COVID-19. Other inmates in the module are being quarantined and the unit is being re-sanitized.
 All inmates are being provided with masks to wear when they are outside their cells, according to the department.
 At a meeting earlier in the day, police lobbyist Chuck Callaway said the jail has been trying to keep its population down during the pandemic. He noted that the jail has a capacity for 4,000 but is down to 2,900 inmates and has 865 people on house arrest.
 No state prison inmates have been tested for the virus, according to prisons director Charles Daniels, who said prison staff have access to tests but have not determined any inmates needed it. A state dashboard indicates there are six employees within the state prison system to test positive, including staff at High Desert State Prison, Southern Desert Correctional Center, Ely State Prison and Casa Grande Transitional Housing.
 — Michelle Rindels, 4/13/20 at 4:57 p.m.
 Health insurance exchange extends special enrollment window by a month in light of ongoing pandemic
 Nevada’s health insurance exchange is extending the special enrollment period it opened last month in light of the ongoing coronavirus pandemic.
 “COVID-19 does not discriminate. Anyone, regardless of age, income or health can become infected,” Sisolak said. “If you or your family don’t have health insurance, now is the time to get it.”
 Those seeking help enrolling in a plan through the exchange are encouraged to contact the exchange’s call center between Monday through Friday, 9 a.m. to 5 p.m., and Saturday and Sunday, 10 a.m. to 2 p.m., at (800) 547-2927.
 The special enrollment period was made possible by Nevada’s transition last year from a hybrid state-based exchange operating on the federal HealthCare.gov platform to a fully state-based exchange. The Trump administration rejected a special enrollment period for states that rely on HealthCare.gov to enroll their residents last month, while Nevada and 11 other states that operate their own exchanges have opened special enrollment windows.
 — Megan Messerly, 4/13/20 at 1:37 p.m.
 Attorney General, U.S. Attorney for Nevada launch inter-agency task force in bid to curb coronavirus-related fraud
 As the number of online scams and other cybercrimes rise amid the coronavirus pandemic, Attorney General Aaron Ford and Nevada U.S. Attorney Nicholas Trutanich announced the formation of an interagency task force Monday aiming to crack down on virus-related frauds. 
 In all, 15 state, local and federal agencies — from the Las Vegas Metropolitan Police Department to the FBI — will coordinate fraud investigations through the task force. 
 Those agencies will focus on, among other things, scams related to coronavirus diagnosis, treatment and stimulus checks, as well as insurance and Medicaid fraud. 
 Concerns around the possible fraud or theft of stimulus money have become especially acute this week, as the first wave of direct payments from the federal CARES Act begin to circulate.
 — Jacob Solis, 4/13/20 at 10:44 a.m.
 Your browser does not support the audio element.
 https://thenevadaindependent.com/article/coronavirus-live-blog-week-six