Companies concerned about coronavirus cancel conferences, limit travel
 As the viral disease, dubbed COVID-19, continues to spread, some employers are canceling conferences and limiting travel, checking supplies and dusting off their emergency preparedness plans, just as they have for previous outbreaks or for natural disasters, such as hurricanes or earthquakes.
 All workplaces, say corporate benefit and health experts, should have plans that focus on preparation, not fear. Currently, cases of COVID-19 are still rare in the U.S. — far less than seasonal influenza.
 “Scaring the crap out of employees isn’t helpful,” said labor and employment attorney Mark Neuberger in Miami. “Employee communication is critical,” he said. “Stay in touch and up-to-date” with the latest Centers for Disease Control and Prevention information and “don’t panic” employees.
 Coronavirus hits tech companies: Google employee tests positive
 Photos: Deadly coronavirus spreading across China reaches US
 He and other experts recommend employers outline policies about teleworking, travel and sick leave; monitor recommendations from the CDC and local health officials; and stock up on needed office supplies and other products that might be affected by a global manufacturing slowdown.
 The CDC has said the current risk in the U.S. from the virus is low, but it encourages employers to develop plans in case the virus becomes more widespread, potentially resulting in containment efforts that might include closing schools, limiting public transportation or canceling large gatherings.
 Still, while emergency plans and workplace policies are important, employers are warned not to go too far.
 “They can’t do it in a discriminatory fashion,” said Sara Rosenbaum, a health law professor at George Washington University. “The thing that is most worrisome is for people of Asian descent, whether they are singled out. That would be Exhibit A for discrimination.”
 Federal laws, including the Americans with Disabilities Act and other statutes, limit the types of health information employers can seek about their employees ― and they prohibit discrimination based on disability or other factors, including national origin.
 The types of questions matter.
 Employers can’t, for example, ask questions that might indicate a person has an underlying health condition or disability like, “Do you have a compromised immune system?”
 Watch: Coca-Cola says the coronavirus could impact Diet Coke production
 But, during a pandemic, they can ask whether a worker has flu-like symptoms, according to guidance developed by the Equal Employment Opportunity Commission in 2009 following an outbreak of the H1N1 virus. And if so, they can send sick workers home.
 Depending on how serious the outbreak becomes, employers could also measure workers’ temperatures, which under ordinary conditions would be considered a medical exam and thus barred.
 In a pandemic, taking temperatures would be OK if the disease in question is more serious than seasonal flu and “becomes widespread in the community as assessed by state or local health authorities,” the EEOC said.
 Some employers, including Nestlè, are restricting business-related international travel. Others are limiting trips to affected areas. Some, including Volkswagen, are asking workers who return from areas where the virus is endemic to stay away from the office for 14 days. Both of these companies are headquartered near Washington, D.C., but have offices and facilities around the country.
 No matter what, legal experts say, employers should not single out particular employees for travel restrictions, health tests, quarantines or any other policy.
 “If there is some policy implemented with respect to overseas travel, it should apply to all people who engage in travel, not just those of a particular race, nationality or origin,” said Phyllis Pari, an attorney in Connecticut who represents employers.
 And employers should consider continuing regular pay for workers who are required to stay at home for 14 days, rather than making them use sick time or vacation, Neuberger said.
 That way, they won’t have “disgruntled employees forced to stay home who won’t have vacation or time off for rest of the year,” he said.
 Still, some employees may become unhappy as companies weigh other decisions.
 Just this week, an estimated 3,000 sales employees of Workday, a California-based analytics company, learned they won’t be enjoying the Florida sunshine since the company canceled its annual sales meeting set for early March in Orlando.
 While firms say they are reacting out of an abundance of caution, they are also considering how fast the virus has spread ― much of it because of international travel ― after emerging in China late last year. Cases have been reported in more than 40 countries.
 Travel and airline stocks were among the hardest hit this week as the overall stock market declined largely over fears about the impact COVID-19 might have on the global economy.
 Still, many conventions are still moving forward, albeit with a public health message similar to that being pushed out by many employers: Don’t panic, the risk is low, and wash your hands.
 As of Thursday, the giant Healthcare Information and Management Systems Society Global Health Conference & Exhibition is still planned for March in Orlando, where it may draw nearly 45,000 information technology professionals from around the world.
 This year, though, it will have a special medical office on-site dedicated to addressing flu-like symptoms and with the ability to isolate people who do show such symptoms, extra hand sanitizer stations and direct access to Florida Department of Health Experts.
 Organizers have declared the conference “a handshake-free meeting.”
 Kaiser Health News (KHN) is a nonprofit news service covering health issues. It is an editorially independent program of the Kaiser Family Foundation that is not affiliated with Kaiser Permanente.
 https://www.usatoday.com/story/money/2020/02/28/companies-concerned-coronavirus-cancel-conferences-limit-travel/4907004002/