Iranian press review: Clerics volunteer to aid US, Europe's fight against coronavirus | Middle East Eye
 A religious NGO from Iran's holy city of Qom has urged the foreign minister in a letter to facilitate volunteer clerics travelling to the US and European countries to provide care to the elderly.
 Qom is one of the epicentres of the coronavirus outbreak in Iran, with most initial cases reported there.  
  'Pandemics know no borders': US lawmakers urge suspending Iran sanctions
 Read More »
 The NGO, House of Young Clerics, wrote: “Due to insufficient support for the elderly infected by the coronavirus in the US and some European countries, we consider it our human duty to help all creatures of the Kind Creator [God].”
 The letter went viral in Iran a few days after international media reported that Spanish soldiers had found elderly patients abandoned or dead in their beds in care homes. 
 Meanwhile, some Iranian clerics have raised questions about how the incident may have been covered by the western media has it happened in Iran.
 Public parks, national parks and forests were devoid of people on 1 March, Iranian Nature's Day, with reports from across the country detailing a sombre atmosphere on one of the most famous national festivals in Iran.
 What is Nowruz? The Persian New Year explained
 At the end of the two-week Nowruz celebration of the spring and the Persian new year, Iranians spend a day out of their houses in nature to praise what nature gives to humans. The ancient festival, known as Sizdah Be-dar, has roots in Zoroastrian religion and rituals. 
 Photos from large Iranian cities such as Tabriz, Qazvin, Rasht, Isfahan and Mashhad showed public parks and streets empty of people and without the usual heavy traffic seen on this holiday.
 The official IRNA news agency also reported that citizens of Tehran stayed home and celebrated the festival on the rooftops, balconies and courtyards of their houses.
 An interview with a detained Iranian scientist in the United States, published by British newspaper the Guardian, has prompted calls to release detained Iranians amid the coronavirus outbreak.
 Dr Sirous Asgari described the unsanitary conditions in a cramped US Immigration and Customs Enforcement (ICE) detention centre in Alexandria, Louisiana.
 According to Asgari, the detainees in this ICE centre have no access to hand sanitisers or masks, and the facility does not enforce physical distancing guidelines to prevent the spread of Covid-19.
  Iran urges US to release Iranian scientist amid coronavirus pandemic
 The interview was published following US Secretary of State Mike Pompeo's call on Iran and Syria to release their prisoners and help combat the fast-spreading virus.
 Iran has temporarily released 85,000 inmates in a move to contain the virus, however many prominent political prisoners have remained behind bars.
 The Guardian’s interview has also renewed Iranian hardliners' criticism of President Hassan Rouhani’s administration, urging him to respond proportionally to the US maximum pressure campaign against Iran.
 BBC Persian has travelled to a poor Afghan refugee camp in a suburb of Peshawar in Pakistan, to talk to family members of Asadullah Harun Gul, the Afghan prisoner in the US Guantanamo Bay prison who was arrested in 2007 and has remained in custody ever since. 
 BBC Persian reported that Gul’s wife, daughter and mother are living in a dire economic situation since their only breadwinner was arrested. Gul was accused of being affiliated with Hezbe Islami, one of the Mujahedin organisations during the war with the Soviet Union.
 The report said that Gul, who is struggling with intense depression in the US prison, had written 900 letters to NGOs and human rights organisations before lawyers at Reprieve began a campaign for his freedom.
 Clive Stafford Smith, head of Reprieve, told BBC Persian that his client is innocent, and that Americans should apologise to Afghans for keeping more than 20 of its citizens in jail for several years without any trial.
 “Asadullah Harun Gul and 23 others, who have done nothing, are still being kept in this prison,” Smith was quoted as saying. “They are ‘nobody’, they should have been released years ago. They are still kept in the prison just because President Trump ordered not to free anyone [from Guantanamo].”  
 * Iranian press review is a digest of reports that are not independently verified as accurate by Middle East Eye
 View the discussion thread.
 https://www.middleeasteye.net/news/iranian-press-review-clerics-volunteer-aid-elderly-coronavirus-US-EU
