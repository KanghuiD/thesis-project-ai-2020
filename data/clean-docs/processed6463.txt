Indians light lamps to heed Modi's call for coronavirus comradeship - World - The Jakarta Post
 Please Update your browser
 Your browser is out of date, and may not be compatible with our website. A list of the most popular web browsers can be found below.
 Just click on the icons to get to the download page.
 Log in with your social account
  Reuters
 Millions of Indians turned off their lights and lit up balconies and doorsteps with lamps, candles and flashlights on Sunday, in response to Prime Minister Narendra Modi's appeal to "challenge the darkness" spread by the coronavirus crisis.
 Modi, who imposed a three-week long nationwide lockdown on March 25, asked all citizens to turn out their lights for nine minutes at 9 p.m. local time on Sunday, and to display lamps and candles in a show of solidarity.
 Modi's call was met with a huge response, with many people lighting up their balconies. Others lit firecrackers, played musical instruments, and sang patriotic songs. Grid data showed India's national power consumption plunging more than a quarter in a matter of minutes.
 The show of unity came as the total number of coronavirus cases in India increased to 3,577, while the death toll rose to 83.
 Some officials have warned that lockdowns could continue beyond April 14 in parts of India where new cases have been detected.
 With the number of cases continuing to increase daily, India restricted the export of most diagnostic testing kits.
 The government, which in recent weeks already banned the export of certain medicines, along with ventilators, masks and other protective gear needed by both patients and medical staff, issued the latest directive late on Saturday.
 The move came even as US President Donald Trump urged Modi in a phone call to release supplies of anti-malaria drug hydroxychloroquine, which is being tested as a possible treatment for patients with COVID-19, the disease caused by the novel coronavirus.
 "The two leaders agreed to remain in touch on the issue of global supply chains for critical pharmaceuticals and medical supplies and to ensure they continue to function as smoothly as possible during the global health crisis," White House spokesman Judd Deere said on Saturday.
 In a briefing note on the conversation, India said the two leaders "agreed to deploy the full strength of the India–US partnership to resolutely and effectively combat COVID-19."
 Bangladesh stimulus 
 The total number of confirmed COVID-19 cases in South Asia, home to roughly 1.9 billion people, topped 7,000 on Sunday, even as the death toll from the respiratory disease rose to 149 in the region.
 While the figures are relatively low in comparison with the United States, China, Italy and Spain, health experts fear that the spread of the pandemic in South Asia could overwhelm already weak public health systems in the region.
 Bangladesh's Prime Minister Sheikh Hasina on Sunday unveiled a 727.50 billion taka ($8.56 billion) stimulus package to help the economy weather the impact of the coronavirus outbreak.
 "The amount is equivalent to 2.52 percent of gross domestic product," Hasina said in a televised address.
 Reuters reported earlier this month that Bangladesh, the world's second-largest apparel producer after China, was set to lose roughly $6 billion in export revenue this financial year amid order cancellations from some of the world's largest brands and retailers.
 Bangladesh has recorded 88 cases of the disease, with nine deaths.
 Following are government figures on the spread of the coronavirus in South Asia:
 * India has 3,577 cases, including 83 deaths
 * Pakistan has 3,059 cases, including 45 deaths
 * Afghanistan has 349 cases, including 7 deaths
 * Sri Lanka has 175 cases, including 5 deaths
 * Bangladesh has 88 cases, including 9 deaths
 * Maldives has 19 cases and no deaths
 * Nepal has nine cases and no deaths
 * Bhutan has five cases and no deaths 
 https://www.thejakartapost.com/news/2020/04/06/indians-light-lamps-to-heed-modis-call-for-coronavirus-comradeship.html
