Coronavirus in Scotland: Fears raised over fall in cancer case referrals - BBC News
 Scotland's interim chief medical officer has voiced fears that people with cancer symptoms are not coming forward due to the coronavirus crisis.
 Dr Gregor Smith said there had been a 72% reduction in urgent suspected cancer referrals by doctors.
 He said GPs had reported far fewer people than usual coming forward with "symptoms and signs" of cancer.
 He urged anyone with new or persistent symptoms to "seek advice in the way you would have done before Covid-19".
 The number of people seeking help at accident and emergency departments in Scotland's hospitals is also down 54% compared to the weekly average over the last three years.
 However, Dr Smith said this was a "very different pathway" of care, and that the current issue was "a real reduction" in people coming forward to their GP with concerns about their health.
 He said Scotland normally sees an average of 2,700 urgent suspected cancer referrals each week - but that the figure had fallen to 744 last week.
 Dr Smith said: "I don't believe for a second that these diseases or these concerns have simply disappeared, so it leaves me worried that there are people out there who are not seeking help from their GP when they might need it.
 "Anyone who has new or persistent symptoms that are worrying them should seek help and advice. Maybe you've found a new lump or have new or unusual bleeding, or have a persistent change in your bowel pattern.
 "My message is don't ignore it - if it was urgent before Covid-19, it remains urgent now."
 Chief Nursing Officer Fiona McQueen added: "Our NHS is there not just for Covid patients, but for all of the other care and attention that the people of Scotland need."
 Cancer Research UK voiced alarm at the drop in referrals. The charity's head of external affairs in Scotland, Marion O'Neill, said: "It's clear the pandemic has left cancer diagnosis and treatment in a precarious position."
 This Covid-19 epidemic is causing collateral damage beyond the cases in our hospitals. 
 There was a warning today from the interim chief medical officer that the number of urgent referrals from family doctors for cancer cases is down by almost three quarters on the level they would normally expect.
 This echoes what I've heard from medics who say people are afraid to go to their GP or are worried about burdening an already stretched health service. 
 Some of the most vulnerable are already hard for the NHS to reach. They can struggle with using technology and may feel cut off from their GP.
 The message from the CMO was not to hold back in seeking treatment. In cancer, early diagnosis is vital. Delays in going to your doctor with a lump or unusual bleeding could mean a worse outcome. 
 The fear is this epidemic will have unintended health consequences for months and years to come.
 The call came as First Minister Nicola Sturgeon confirmed a further 12 people had died in Scotland after testing positive for the virus, bringing the total under that measure to 915.
 She said 1,809 people were in hospital with confirmed or suspected cases of the virus, 169 of whom are in intensive care.
 Ms Sturgeon expressed her thanks to frontline health and care staff, saying: "You are doing extraordinary work in the most difficult of circumstances and our gratitude is with you each and every day."
 The first minister said she would start to set out "some of the factors that will guide our thinking for the future" later in the week, as the first step towards formulating an "exit strategy" from the lockdown.
 She said the goal was to "continue to suppress the virus while gradually restoring a semblance of normality to everyday life", but warned that some restrictions would likely remain in place until a vaccine has been developed.
 She said: "I want to be clear that the initial version of this work will not set out what measures will be lifted and when. We are not in a position to take those decisions in a properly informed way, and I will not do anything that would risk a resurgence of this virus.
 "That would risk overwhelming the health service and put lives at risk."
 The US leader will sign an executive order to suspend migration, but it's not clear how he will do it.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/uk-scotland-52353657
