Coronavirus has sparked racist attacks on Asians in Australia — including me - ABC News (Australian Broadcasting Corporation)
       Updated 
     
       February 02, 2020 00:40:47
 I was standing in a supermarket aisle and moving my shopping trolley to make room for a middle-aged woman to pass when I overheard it: "Asians … stay home …stop spreading the virus."
 I stopped and looked at the woman. Her face was serious, her eyes stared blankly at the floor in front of her as if she was just thinking aloud.
 I didn't confront her. Her voice lowered when she knew I was watching her, but the muttering continued as she walked away.
 With 10,000 confirmed cases of coronavirus and more than 200 deaths, as of Friday, it's no wonder many people feel anxious about the spread of the disease and the risk of infection.
 Before this experience, racism was something I knew existed but had only experienced via other people's stories.
 Gold Coast surgeon Rhea Liang tweeted on Thursday that one of her patients joked about not shaking her hand because of coronavirus.
 Dr Liang didn't find it funny at all. Instead she felt offended by racism.
 "I have not left Australia. This is not sensible public health precautions," she wrote.
 Several Chinese-Australian friends shared their own experiences when I brought up my supermarket encounter.
 One recounted how a waiter had dumped change at his table before turning and quickly walking away, after my friend paid cash for a meal at a Melbourne restaurant.
 Another, who wore a mask as a precaution at a shopping mall on Friday, had three teenagers tell her: "See you! Go and catch coronavirus".
 None of these three people have been to China recently, nor have had contact with anyone confirmed or suspected of having coronavirus.
 Sydney man SK Zhang, who has lived in Australia for the past 20 years, said since news of the epidemic broke, he has been getting stares on public transport and is increasingly worried about how the online vitriol will affect Chinese-Australian children.
 "It's been a tough week … we are very fearful [of the virus] … but at the same time we are also being targeted with racism and a lot of unwanted attention," he said.
 "The way I would put it is right after 9/11, people looked at every Muslim as if they were terrorists, and that's how people are looking at us."
 He said he wears a face mask for his own protection and could sense tension when he stepped on the bus or train.
 "Nobody has verbally abused me or anything, but I can feel the unwelcome stares coming from these people," he said.
 "People are looking at us and thinking 'he's a virus carrier.'"
 Geelong teacher Andrew Branchflower, who recently returned from China with his family, said he was concerned about the online vitriol. 
 Speaking to the ABC, he said he was troubled by the misconceptions and sentiments about "closing our borders" circulating online, worrying it could lead to a "slippery slope" and restricting freedoms.
 He said it was confronting to think of his children, aged four and one, facing the online prejudice.
 "They're very fortunate to have members of the Asian Australian community calling out this rhetoric."
 On January 29, some in the Chinese community in Australia began an online campaign to protest what they see as "inappropriate" labelling of the coronavirus as a "Chinese" disease by two newspapers, The Herald Sun and The Daily Telegraph.
 The Herald Sun published a headline that referred to the "Chinese Virus Pandamonium", while The Daily Telegraph highlighted "China kids stay home" in their headlines.
 Campaigners fear headlines like these will make Chinese-Australians a target for discrimination.
 And more than 50,000 people agree with them, with the change.org petition receiving huge interest and calls for an apology. 
 "No one called the Ebola virus a Congo Virus! No one called BSE [bovine spongiform encephalopathy, or mad cow disease] as European Virus or French or American Virus! Please, show some respect and humanity!" wrote Anna Ou on the site.
 Meanwhile, examples of insensitive media coverage are not limited to Australia.
 According to The New York Times, French newspaper Le Courrier Picard has apologised after being criticised for its headline "Yellow Alert".
 The Danish daily Jyllands-Posten has also faced controversy after publishing a "coronavirus-themed" cartoon which replaced the stars on Chinese national flag with icons of the virus.
 The newspaper refused to apologise and argued the cartoon was not offensive to China.
 Asian-Canadians also highlighted that they had been subjected to racism and stereotypes — and not for the first time.
 "During SARS, the hyper surveillance and containment in public spaces, transit, their workplaces, schools, etc were terrible to live through."
 Her study highlighted a history of "xenophobic panic" and of associating Asian immigrants with being "dirty and diseased". One survey respondent featured in that paper said the attitudes during SARS saw pre-existing racism resurface. 
 "The racialisation of SARS and the discriminations during SARS only reflected the ongoing and long standing prejudice and discrimination," the respondent said. 
 The study found the way the media represented the SARS crisis generated public hysteria and impacted on Asian communities.
 "The adverse effects from the racial profiling of SARS, 9/11 and other such events have made many of us question the core values of what Canada really stands for," the paper concluded.
 I've been writing about the coronavirus outbreak from South Australia for two weeks now.
 My working day always begins with phone calls and texts to my sources in Wuhan.
 It's stressful to hear about the Wuhan people I know being trapped at home or separated from families by the lockdown, all the while living in fear of becoming infected.
 But it is not only non-Chinese displaying suspicious or racist behaviour towards people of Chinese background. 
 Wuhan residents are being blamed for creating the disease, and discriminated against by people living in other parts of China.
 Even in China, people from Wuhan, no matter whether they are healthy or sick, are often tracked down and reported to police when they post pictures of themselves on social media showing they are outside the lockdown.
 In many parts of Asia, locals have been campaigning to block all Chinese nationals, regardless of their health status, from entry.
 Of course it's understandable that all humans share a fear of death and as a result feel concerned about associating with people from a country where most infections have taken place.
 But as I experienced in the supermarket this week, the coronavirus has really brought out the worst in people.
 And that is really not OK.
 Additional reporting by Matthew Bamford and Erin Handley
 Topics:
 community-and-society,
 	epidemics-and-pandemics,
 	multiculturalism,
 	discrimination,
 	australia,
 	china
     First posted 
       February 01, 2020 06:53:42
 If you have inside knowledge of a topic in the news, contact the ABC.
 ABC teams share the story behind the story and insights into the making of digital, TV and radio content.
 Read about our editorial guiding principles and the standards ABC journalists and content makers follow.
 Learn more
 While the rest of the world is distracted by the coronavirus outbreak, a dangerous new phase has begun in a region already struggling with unrest, corruption and inequality. 
         By Anna Kelsey-Sugg and Bec Zajac for Life Matters
 Alone but for coyotes and rattlesnakes, Claire knew she might die after a serious fall at Joshua Tree National Park. She resolved to change her life if she made it out alive.
         By ABC editorial director Craig McMurtrie
 Some of the language thrown around in the aftermath of the High Court ruling on George Pell seems to ignore the first principles of journalism and the facts, writes ABC editorial director Craig McMurtrie.
         By Phoebe Hosier
 Meet the Tasmanian circus performer who's determined to teach kids and adults how flying through the air and hanging upside down can reframe how we think about bodies, boundaries and consent. 
 AEST = Australian Eastern Standard Time which is 10 hours ahead of GMT (Greenwich Mean Time)
 https://www.abc.net.au/news/2020-02-01/coronavirus-has-sparked-racist-attacks-on-asian-australians/11918962
