Live blog: Coronavirus updates in Hertfordshire, Bedfordshire, Cambridgeshire | Stevenage, Hitchin, Letchworth, Biggleswade News | The Comet
 Advanced search
 PUBLISHED: 12:49 20 April 2020
  Jacob Thorburn
 Coronavirus. Picture: Getty Images/iStockphoto/wildpixel
 Getty Images/iStockphoto
 To send a link to this page you must be logged in.
 This is a rolling article that will be updated as the situation surrounding COVID–19 progresses.
 Our reporters from the Comet, Welwyn Hatfield Times, Herts Advertiser and Royston Crow will be adding to this live blog as and when we get information.
 If you value what this story gives you, please consider supporting the Comet. Click the link in the yellow box below for details.
 This newspaper has been a central part of community life for many years, through good times and bad. Coronavirus is one of the greatest challenges our community has ever faced, but if we all play our part we will defeat it. We're here to serve as your advocate and trusted source of local information. 
 https://www.thecomet.net/news/coronavirus-hertfordshire-cambridgeshire-bedfordshire-news-1-6562798
