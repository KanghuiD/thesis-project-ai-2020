BMC Infectious Diseases | Home page
 As a result of the significant disruption that is being caused by the COVID-19 pandemic we are very aware that many researchers will have difficulty in meeting the timelines associated with our peer review process during normal times.  Please do let us know if you need additional time. Our systems will continue to remind you of the original timelines but we intend to be highly flexible at this time.
 In this Blog, written by Dr Ghazi Kayali, a member of BMC Infectious Diseases Editorial Board, we discuss the rise of the novel 2019 Coronavirus and its significant public health implications.
 BMC Infectious Diseases is pleased to announce the Antimicrobial Resistance special issue. This series includes a diverse range of AMR studies from around the globe, investigating the detection and surveillance of AMR, from the lab bench to hospital bed.
 Unusual manifestation of disseminated herpes simplex virus type 2 infection associated with pharyngotonsilitis, esophagitis, and hemophagocytic lymphohisitocytosis without genital involvement
 Bovine leukemia virus discovered in human blood
 The landscape of vaccines in China: history, classification, supply, and price
 BMC Infectious Diseases is delighted to announce the release of The airborne microbiome - implications for aerosol transmission and infection control. This special issue aims to explore and characterise the airborne microbiome in different environments and discusses possible interventions to control the transmission of airborne pathogens. 
 BMC Infectious Diseases is delighted to announce the release of Human rights as social and structural drivers of sexually transmitted infections. This special issue is a collaboration between BMC Infectious Diseases and BMC International Health and Human Rights and focuses on highlighting novel studies characterizing human rights infringements as structural determinants of sexually transmitted infections. 
 To learn more about this collection, please read our Editorial. 
 Authors: Kayla Schacher, Hannah Spotts, Caroline Correia, Sosina Walelign, Mehret Tesfaye, Kassu Desta, Aster Tsegaye and Bineyam Taye
 Content type: Research article
 25 April 2020
 Authors: David W. McCormick, Jason M. Bacha, Nader K. El-Mallawany, Carrie L. Kovarik, J. S. Slone and Liane R. Campbell
 Content type: Case report
 Authors: Aleksandra A. Zasada, Aldona Wiatrzyk, Urszula Czajka, Klaudia Brodzik, Kamila Formińska, Ewa Mosiej, Marta Prygiel, Katarzyna Krysztopa-Grzybowska and Karol Wdowiak
 Authors: Xiaoxiao Wang, Wei Ruan, Shuisen Zhou, Xinyu Feng, He Yan and Fang Huang
 Authors: Zhuoxin Peng, Andrew Hayen, Martyn D. Kirk, Sallie Pearson, Allen C. Cheng and Bette Liu
                     Most recent articles RSS
                     
                         View all articles
                         
 Authors: Axel Kramer, Ingeborg Schwebke and Günter Kampf
 16 August 2006
 Authors: Raymond Tellier, Yuguo Li, Benjamin J. Cowling and Julian W. Tang
 Content type: Review
 31 January 2019
 Authors: Sereina A Herzog, Christian L Althaus, Janneke CM Heijne, Pippa Oakeshott, Sally Kerry, Phillip Hay and Nicola Low
 11 August 2012
 Authors: Matthew Biggerstaff, Simon Cauchemez, Carrie Reed, Manoj Gambhir and Lyn Finelli
 4 September 2014
 Authors: Niina Ikonen, Carita Savolainen-Kopra, Joanne E. Enstone, Ilpo Kulmala, Pertti Pasanen, Anniina Salmela, Satu Salo, Jonathan S. Nguyen-Van-Tam and Petri Ruutu
 29 August 2018
 Most accessed articles RSS
 BMC Infectious Diseases is an open access, peer-reviewed journal that considers articles on all aspects of the prevention, diagnosis and management of infectious and sexually transmitted diseases in humans, as well as related molecular genetics, pathophysiology, and epidemiology.
 This cross-journal collection brings together in one place articles outlining diseases (and their vectors) that are likely to spread or are already spreading across borders due to the effects of climate change. The impact of policy implementation or interventions designed to contain the spread infectious disease, and studies that could inform future global policy or practical solutions are very much welcome.
 Submissions are now open and the deadline is the 31st of May 2020. A full list of participating journals can be found following the link above. 
 We are recruiting new Associate Editors to join our international editorial board, helping to provide expertise on a wide range of different subject disciplines.
 Prof. Carlo Torti graduated in Medicine from the University of Pavia in 1994.  He obtained his specialty in Infectious Diseases in 2000 from the University of Brescia where he worked as Consultant Physician and Assistant Professor in Infectious Diseases from 2001 to 2012. He was appointed Director of the Infectious and Tropical Diseases Unit at the University “Magna Graecia” (UMG) in Catanzaro in 2012. He is Associate Professor of Infectious Diseases in the same University. His main areas of interests are: general Infectious Diseases, viro-immunological and clinical outcomes of antiretroviral treatment, HIV drug resistance, and co-infection of HIV with hepatitis viruses.
 Dr. Joseph D. Tucker is an Assistant Professor of Medicine at the University of North Carolina at Chapel Hill. He is an infectious diseases physician with a focus on sexually transmitted infections and has a special interest in crowdsourcing as a tool for improving public health.  He has been a BMC Infectious Diseases Section Editor since 2016.
 It is with great sadness that we mourn the loss of Prof Andrea De Luca, who tragically passed away on February 4th 2019 in a car accident.  Prof De Luca, who was based at the University of Siena, Italy, dedicated many years of his life to clinical research on the treatment of HIV and hepatitis, HIV drug resistance, the toxicity of antiretrovirals as well as the impact of hepatitis virus co-infections.
 As a valued member of our editorial board, his death is a tremendous loss to our journal, but more importantly to the field of HIV research, where his efforts and contributions will live on.  
 Research Data Support is an optional Springer Nature service available to researchers who have datasets they want to make easier to cite, share and find. Learn more about this service and the many benefits of making your data publicly available. 
 23 April 2020
 22 April 2020
 08 April 2020
 Citation Impact2.565 - 2-year Impact Factor2.951 - 5-year Impact Factor1.208 - Source Normalized Impact per Paper (SNIP)1.489 - SCImago Journal Rank (SJR) 
 Usage 3,402,716 Downloads3381 Altmetric Mentions
 ISSN: 1471-2334
             By using this website, you agree to our
             Privacy
                 statement and
             
         
             Springer Nature.
 https://bmcinfectdis.biomedcentral.com/
