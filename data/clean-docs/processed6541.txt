Crowdfunder launched to raise money for Preston creatives hit by coronavirus lockdown |  Blog Preston
 A crowdfunder has been launched for Preston creatives who have suffered a loss of earnings due to the coronavirus lockdown.
 Local promoter Garry Cook has created the GoFundMe campaign to get money quickly to the city’s artists, many of whom are struggling in the current crisis.
 Cook is asking the people of Preston to donate to the campaign, which has an initial £1,000 target, with five creatives getting £200 each to help them financially during the lockdown. If the campaign raises more than the £1,000 target, more creatives will get money.
 In return for the £200 funding, the creatives will be asked to produce a new piece of work and present it to the people of Preston at an arts event. The event will be held after the lockdown is lifted.
 Artists of any kind including performers, poets, actors, writers, photographers, painters, sculptors, sound artists and musicians can apply for funding.
 Cook said: “I’ve thought about doing this fundraiser for quite a few weeks. I’ve seen other cities do similar funding campaigns with great success, like Manchester and Liverpool. But smaller cities like Leicester and Hull have also done it, and I think Preston, with all its ambitions as a creative hub, should also stand up and support its creatives.
 “Most creatives are freelance, and that has meant that their entire income streams have been cancelled out due to the lockdown. If we don’t support them and encourage them – even in this small way – we will lose many of them as they will be forced to give up their practice.
 “I would particularly like the area’s big businesses and organisations to step forward and donate money to support our creatives and artists. We are constantly being told that art and creativity is the catalyst for economic change for this city, so I think it is really important that those creatives and artists are given support they need to get through this crisis and enable them to carry on the great work they do in the future.”
 The application process for the £200 support asks artists to demonstrate their connection to Preston and write a 300-word proposal of the new work they would like to create. Successful applicants will have the money paid up front.
 Artists and creatives should visit the Enjoy The Show website for more information on how to apply for the fund.
 To donate, visit the GoFundMe Preston Artists Coronavirus Hardship Fund page.
 				Sign up below to receive Blog Preston's weekly newsletter. It wings its way into inboxes every Sunday rounding up our best content from the last week and a look ahead to what's happening.
 			
 				Find news by location, select an area of your choice to be kept up to date with the latest goings on.
 				Find news by category, select an category of your choice to be kept up to date with the latest goings on.
 BlogPreston was founded by Ed Walker
 Preston based 3manfactory designed & developed the website
 Preston based Clook Internet host the website
 Blog Preston is a Community Interest Company. Company number 08814641.
 https://www.blogpreston.co.uk/2020/04/crowdfunder-launched-to-raise-money-for-preston-creatives-hit-by-coronavirus-lockdown/
