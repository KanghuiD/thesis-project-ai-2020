How can Chancellor Rishi Sunak save the economy from coronavirus crisis? Experts debate raising taxes to save economy – The Sun
 THE coronavirus will no doubt have huge implications on Britain's economy and as Chancellor, Rishi Sunak must ensure he is prepared.
 Here, two experts discuss his best course of action. Is raising taxes the wrong approach or the Government's only option?
 One surprise of this crisis has been how easily Britons have accepted the lockdown . Some people seem to positively enjoy it.
 To some it brings back the spirit of the Blitz a feeling of solidarity. To others fewer cars and planes are an indication of the more green lifestyle we ought to be living.
 It's good that people have managed to cope with these restrictions. But when polls indicate that many people would be prepared to put up with a lengthy extension of the lockdown I begin to worry.
 An economic interruption of this kind carries massive costs and can only be borne for a limited period. Any extension will become extremely difficult for the economy to bear.
 The OBR, the Government's independent forecaster, floated the possibility of a fall in output of the economy of 35 per cent for the period April to June.
 That is beyond anything that has previously been imagined. It wasn’t a definite forecast but if it happened the effects on jobs and living standards would be more than devastating.
 Some describe this economic crisis as the worst in modern times. It may be the worst ever. That doesn’t mean we can't recover. We can and we certainly will.
 This seems unlikely The economy may bounce back but only up to a certain point not necessarily the same point as before. Some firms will have gone out of business, others will have run up debts, some have missed orders that aren’t repeated.
 How long all these problems last depends on the lockdown. The longer the lockdown, the deeper the downturn. So it is vitally urgent that the Government relax the lockdown as soon as the scientists feel that the peak of the epidemic has been flattened and that it is appropriate.
 This isn’t about choosing the economy or lives. A flattened economy costs lives too. Even when we relax the lockdown many will still be nervous and may not want to go out to spend money.
 The continuing need for social distancing will pose problems for hospitality and other businesses and hold back a full recovery. Rishi Sunak, the Chancellor has been bold, promising to do “whatever it takes” to steer the economy through these perilous waters.
 His scheme for furloughing jobs with the Government paying up to £2500 a month of people's wages has helped to prevent firms getting rid of people that they could not afford But it is eye wateringly expensive with costs estimated at up to £60billion.
 Clearly it cannot continue indefinitely. Ironically the generosity of the scheme may have anaesthetised the public to the scale of the crisis we are facing.
 But if the Government lifts some of the restrictions soon and the economy starts to recover , what should the Chancellor do in his Budget in November? Of course we don’t know exactly what the figures will look like and we must wait to see before final judgements.
 Almost certainly he will be confronting a massively reduced economy, groaning under skyscraping debt, higher unemployment and only a gradual recovery.
 Horrific as the borrowing figures will be it would be a mistake to move quickly to reduce the debt before the recovery is firmly established.
 He should let borrowing take the strain in the short term. Any measures to rein in the deficit should be for later. In order to boost jobs he might cut National Insurance or make a temporary cut in VAT designed to bring forward spending.
 Keir Starmer gave a menacing hint of his approach when he said there would have to be “a day of reckoning” and hinted at tax increases on the better off as though they were to blame.
 That would be precisely the wrong approach. As the economy gets back to maybe 90 per cent of where it was before the Chancellor needs to do all he can to support business.
 He has recently given some generous help to new high tech start ups. Well and good, but it will be important to continue to support existing businesses.
 It should never be forgotten that firms that have failed in this crisis have not failed though any fault of their own. This down turn is not a normal recession caused by market forces, a rise in interest rates or overheating.
 No this recession and the bankruptcies have been caused by the Government’s measures rightly introduced to contain the virus. So the Government should not feel embarrassed about continuing some of their schemes to support individual businesses and indeed they have a duty to do so.
 The Chancellor sets the framework but ultimately growth does not come from Governments, it comes from individuals and firms. The way to growth and recovery is to give them the freedom and the incentives and the rest will follow.
 BRITAIN’s four million NHS staff are on the frontline in the battle against coronavirus.
 But while they are helping save lives, who is there to help them?
 The Sun has launched an appeal to raise £1MILLION for NHS workers. The Who Cares Wins Appeal aims to get vital support to staff in their hour of need.
 We have teamed up with NHS Charities Together in their urgent Covid-19 Appeal to ensure the money gets to exactly who needs it.
 The Sun is donating £50,000 and we would like YOU to help us raise a million pounds, to help THEM. No matter how little you can spare, please donate today here: www.thesun.co.uk/whocareswinsappeal.
 Raising taxes in the current climate is not something the Government is likely to want to do.
 But the scale of the economic crisis now gripping the UK means they may end up feeling they have little option.
 So far the Chancellor has done what he had to do to protect businesses and workers from enforced shutdowns. The cost will have to come from somewhere.
 It is estimated the furloughing scheme alone will cost at least £40billion. At the same time, tax receipts have nosedived as earnings have decreased and fewer people are spending in the shops.
 As a result, the Office for Budget Responsibility estimates that public- sector borrowing will increase by an eye-watering £218billion above the earlier forecast.
 The black hole in the country’s finances left by Covid-19 means that at some point, the Chancellor is going to have to face the uncomfortable issue of whether taxes will go up.
 Any rises must be set against the need to ensure nothing chokes the ­economic recovery, which will be vital.
 We must also guard against hitting the most hard-up in society. And ­crucially, we need to ensure we don’t make it unprofitable for dynamic small companies to operate.
 One option for the Chancellor is to target the big multi-nationals and billionaires who use tax havens to avoid paying their fair share.
 Yet we will also need to have big business on our side as we try to recover, so it is a delicate balancing act.
 Our priority right now still has to be protecting the vulnerable and health workers from another Covid-19 outbreak.
 Don't miss the latest news and figures - and essential advice for you and your family.
 To receive The Sun's Coronavirus newsletter in your inbox every tea time, sign up here.
 Get Britain's best-selling newspaper delivered to your smartphone or tablet each day - find out more.
 But it is time too for the Government to think seriously about how to restart the economy as quickly as possible.
 Two days ago came news that Jaguar and Aston Martin will restart production in May. The Chancellor needs to help other companies follow suit.
 The more successful he is, the likelier he will be able to limit the number of extra taxes he has to raise in the months ahead.
 We are urging Sun readers to sign a petition calling for our NHS staff to be awarded the George Cross.
 We are backing a proposal by Lord Ashcroft to honour our health heroes with the gallantry gong given for acts of bravery that did not take place in battle.
 A No10 spokesman said: “The NHS is doing a fantastic job and the nation will want to find a way to say thank you when we have defeated this virus.” SAS hero Andy McNab added: “The award of a George Cross would show an emotional appreciation.”
 We are asking our readers to please sign the petition below.
  Ed Sheeran loses fight to block neighbour's extension next to his £3.7m estate
  Coronavirus tracking app suggests bug was spreading through UK in January
  Paddy McGuinness shares rare sight of son Leo as he relaxes in the sunshine
  Furloughed staff urged to take fruit picking jobs as virus hits migrant workers
  NHS boss warns Brits of 2nd virus peak after April's lowest daily death toll
 https://www.thesun.co.uk/news/11481822/chancellor-save-economy-coronavirus/
