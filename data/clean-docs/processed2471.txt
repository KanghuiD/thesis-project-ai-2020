Guidance for non-clinical settings - Health Protection Surveillance Centre
 See Coronavirus (COVID-19) - information for the public (updated regularly).
 Advice and guidance for healthcare workers (updated regularly).
 Up-to-date advice for how to protect yourself and others from COVID-19 is provided on HSE.ie.
 Resources for the business sector from Government:
 Schools and childcare facilities closed as of 13/3/2020.
 If your child may have been exposed to coronavirus, follow the advice for anyone who may have been in contact with coronavirus.
 Encourage your child to wash their hands regularly and properly.
 Read more advice on how to prevent your child from catching or spreading viral infections.
 Guidance for management of people with suspected COVID-19 infection who present to pharmacies.
 Guidelines for Funeral Directors on managing infection risks when handling deceased individuals with confirmed COVID-19.
 Guidance about preventing the spread of COVID-19 in settings for vulnerable groups  and on cocooning is available in the Vulnerable Groups section of the website. 
 Advice on drinking water supplies or swimming pools being chlorinated in accordance with current recommendations / best practice is sufficient to inactivate COVID-19 virus.
 Guidance on managing Legionella in water systems during the COVID-19 pandemic.
 25-27 Middle Gardiner St, Dublin 1 Ireland. t: +353 1 8765300 f: +353 1 8561299  
 https://www.hpsc.ie/a-z/respiratory/coronavirus/novelcoronavirus/guidance/guidancefornon-clinicalsettings/
