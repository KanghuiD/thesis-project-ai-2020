Headlines - POLITICO
 Top Headlines from POLITICO
 By POLITICO STAFF
 04/22/2020 12:10 AM EDT
 Happy birthday!
 By HANNAH ROBERTS
 04/21/2020 10:56 PM EDT
 As the epidemic ravages Europe, the Kremlin has dialed up the disinformation and propaganda.
 By LILI BAYER, HANS VON DER BURCHARD and BJARKE SMITH-MEYER
 04/21/2020 10:45 PM EDT
 Angela Merkel backs Commission but leaders face plethora of rival plans.
 By BEN WHITE
 04/21/2020 08:55 PM EDT
 Every policy solution has been small in scope for the problem we face.
 By BURGESS EVERETT
 04/21/2020 08:47 PM EDT
 The Senate majority leader expressed concern about rising deficits in an interview and said he wanted the full Senate to return before acting again.
 By RYAN HEATH
 04/21/2020 07:16 PM EDT
 Updated 04/21/2020 08:09 PM EDT
 In Earth Day speech, U.N. Secretary-General Antonio Guterres will challenge President Donald Trump on fossil fuel subsidies.
 By MICHAEL STRATFORD
 04/21/2020 06:16 PM EDT
 Updated 04/21/2020 06:44 PM EDT
 The new guidance prevents undocumented students from accessing the money, although the law includes no explicit restrictions on which students could receive the grants. 
 By IAN KULLGREN
 04/21/2020 06:14 PM EDT
 A spokesperson said the breach affected 7,900 applicants for the Economic Injury Disaster Loans program.
 By SARAH FERRIS, MELANIE ZANONA and HEATHER CAYGLE
 04/21/2020 06:02 PM EDT
 "I think you'll see pretty close to universal Republican opposition," said Rep. Tom Cole.
 By MARIANNE LEVINE
 04/21/2020 05:52 PM EDT
 The Republican senators argued lawmakers should be in town to approve a half-trillion dollar measure.
 By ANITA KUMAR
 04/21/2020 05:44 PM EDT
 Updated 04/21/2020 08:52 PM EDT
 Trump's proclaimed immigration ban will exempt temporary foreign workers, the biggest source of immigration at the moment.
 By DANIEL LIPPMAN
 04/21/2020 05:34 PM EDT
 The Office of Personnel Management is stonewalling their requests for information, lawmakers say.
 By MYAH WARD
 Updated 04/21/2020 08:47 PM EDT
 Republican-led states are looking to reopen their economies, but mayors in some of their biggest cities are not on board.
 By NAHAL TOOSI
 04/21/2020 05:12 PM EDT
 But the possible gesture won't do much good unless it's part of a larger strategy, aid experts say.
 MAGAZINE
 By MICHAEL GRUNWALD
 04/21/2020 05:00 PM EDT
 The “it’s not you” approach might be good politics, but the Covid epidemic is showing it’s also wrong.
 By ZACHARY WARMBRODT
 04/21/2020 04:59 PM EDT
 Lenders are pleading with lawmakers to save the Small Business Administration's 7(a) program.
 By ALEX THOMPSON 
 04/21/2020 04:49 PM EDT
 Updated 04/21/2020 06:19 PM EDT
 Senior leaders are split over whether to hire Hawkfish, a digital firm financed by Mike Bloomberg.
 By CRISTIANO LIMA
 04/21/2020 04:40 PM EDT
 Marc Rotenberg, who co-founded the group in 1994, has been a leading critic of Silicon Valley's efforts to track the spread of Covid-19.
 04/21/2020 04:14 PM EDT
 Updated 04/21/2020 06:28 PM EDT
 The Senate approved Tuesday a $484 billion package to help small businesses and hospitals.
 By BJARKE SMITH-MEYER
 04/21/2020 03:57 PM EDT
 Council and Commission leaders aim to ‘reduce over-dependency on third countries.’
 By JOSH GERSTEIN
 04/21/2020 02:49 PM EDT
 Updated 04/21/2020 10:01 PM EDT
 On Monday, some prisoners were told that officials were no longer considering early releases for inmates who have served less than half their sentence.
 By ANDY BLATCHFORD
 04/21/2020 02:35 PM EDT
 Truck shipments of supplies to Chinese airports have been interrupted by checkpoints and quarantines, the prime minister said. 
 04/21/2020 02:30 PM EDT
 As the events have become more widespread, they have also become better organized and more partisan as national groups get involved.
 By CAITLIN OPRYSKO
 04/21/2020 02:11 PM EDT
 While Northam said he empathized with protesters’ eagerness to reopen parts of the economy, the governor maintained he wanted a safe return to normalcy.
 By KATY O'DONNELL and VICTORIA GUIDA
 04/21/2020 01:40 PM EDT
 The servicers will only have to advance four months of missed payments on single-family loans to Fannie Mae and Freddie Mac.
 By ASSOCIATED PRESS
 04/21/2020 01:23 PM EDT
 With 368 patients, the study is the largest look so far of hydroxychloroquine with or without the antibiotic azithromycin for Covid-19.
 By SARAH OWERMOHLE
 04/21/2020 01:14 PM EDT
 There is not enough data for or against the vast majority of the medicines, including the combination of hydroxychloroquine and the antibiotic azithromycin, the panel said.
 By ANNABELLE DICKSON
 04/21/2020 12:41 PM EDT
 The British prime minister and president also committed to signing a free-trade agreement "as soon as possible."
 By KYLE CHENEY
 04/21/2020 12:24 PM EDT
 Updated 04/21/2020 02:14 PM EDT
 The acting intelligence chief said he felt "compelled to defend these career officers from unsubstantiated indictments."
 By JILLIAN DEUTSCH and SARAH WHEATON
 04/21/2020 11:02 AM EDT
 Scientists are the public face of lockdowns — and politicians aren’t making it easy for them.
 By MARTIN MATISHAK and ANDREW DESIDERIO
 04/21/2020 10:58 AM EDT
 Updated 04/21/2020 05:01 PM EDT
 The report represents a confidence-booster to the country’s intelligence community.
 By QUINT FORGEY
 04/21/2020 10:56 AM EDT
 Calls to fire America’s top infectious disease expert resurfaced over the weekend during a demonstration against coronavirus mitigation measures.
 By BEN LEFEBVRE
 04/21/2020 10:35 AM EDT
 Updated 04/21/2020 05:15 PM EDT
 Oil dropped to record low prices because of the coronavirus pandemic. 
 By LARA SELIGMAN
 04/21/2020 10:16 AM EDT
 The move potentially delays the ship’s departure amid new questions about the spread of Covid-19. 
 By DAVID LIM
 04/21/2020 10:14 AM EDT
 The test could help cut down on the amount of personal protective equipment health providers are using while collecting patient samples for testing.
 By LIZ CRAMPTON
 04/21/2020 10:10 AM EDT
 Updated 04/21/2020 11:21 AM EDT
 Trump tweeted on Monday night that he will sign an executive order temporarily banning immigration to stem coronavirus. 
 By BURGESS EVERETT and HEATHER CAYGLE
 04/21/2020 09:31 AM EDT
 Updated 04/21/2020 06:21 PM EDT
 The legislation delivers funding to small businesses, hospitals and for testing.
 04/21/2020 09:11 AM EDT
 The federal government is looking into intelligence suggesting Kim is in “grave danger” following a surgery.
 04/21/2020 07:24 AM EDT
 Updated 04/21/2020 01:07 PM EDT
 Workers in bowling alleys, gyms, hair salons and tattoo parlors could return to their jobs as early as Friday.
 By CRISTINA GONZALEZ, LAURENS CERULUS and ELINE SCHAART
 04/21/2020 06:54 AM EDT
 Delaying regulations is the new battleground for crisis-hit industries.
 04/21/2020 04:40 AM EDT
 By DAN GOLDBERG and ALICE MIRANDA OLLSTEIN
 04/21/2020 04:30 AM EDT
 Contact tracing is essential to reopening the economy and containing outbreaks, but the workforce doesn’t exist to do it.
 The prime minister counters the president's Covid-19 challenges by applying lessons learned from NAFTA's crash course.
 By LAURA BARRÓN-LÓPEZ
 Updated 04/21/2020 10:36 AM EDT
 Trustworthy communication to minority populations has been sorely lacking, public health experts say.
 THE AGENDA
 By EDWARD B. FOLEY and STEVEN F. HUEFNER 
 If voters don’t get absentee ballots on time, states can offer an easy-access write-in ballot—an option that already exists for Americans overseas.
 By NANCY COOK
 Trump’s advisers recognize his bumpy rollout of coronavirus testing represents a major vulnerability in an election year. 
 By BETSY WOODRUFF SWAN
 The three governments are pushing a host of matching messages, including that the novel coronavirus is an American bioweapon.
 By SCOTT BLAND
 04/20/2020 11:14 PM EDT
 But his fundraising slowed down in the second half of the month.
 04/20/2020 10:48 PM EDT
 Updated 04/20/2020 11:42 PM EDT
 A CNN report sends the South Korean government scurrying.
 By ANITA KUMAR, LIZ CRAMPTON and MATTHEW CHOI
 04/20/2020 10:17 PM EDT
 Updated 04/21/2020 11:59 AM EDT
 Several industry representatives expect the upcoming immigration suspension will exempt one of the largest sources of immigration at the moment — farmworkers.
 https://www.politico.com/story