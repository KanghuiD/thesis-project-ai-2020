613 Confirmed Coronavirus Patients in NYS, 269 in NYC
 In a conference call Saturday evening, Gov. Andrew Cuomo confirmed an additional 187 new coronavirus cases in the state of New York since yesterday, including 115 new cases in the city.
 There are now 613 confirmed new coronavirus cases total in the state, and 269 total in the city.
 The governor spoke about a 65-year-old man in Rockland County who tested positive for the coronavirus after his death, saying he had other health problems that contributed to his death. He is the second confirmed person to die of the coronavirus in the state.
 Cuomo spoke more about a change to the petition process that would allow NY politicians to collect just 30 percent of the required signatures necessary for elections. He also said he encourages voters to take advantage of absentee ballots to avoid large public gatherings.
 Cuomo was critical of the federal coronavirus relief bill agreed upon by the House, saying it is bad for New York.
 Cuomo says the federal coronavirus package gives New York the lowest rate of funding of any state in the country. "We're actually tied for last with the rate of reimbursement," he says, notes NY has one of the highest positive cases in the country.
 The governor also knocked the federal bill for blocking states from changing the local Medicaid reimbursement rate.
 Cuomo: "Who the heck is the federal government to tell me we can't change the Medicaid program at the same time they're giving us the lowest rate of reimbursement?"
 "This bill makes it impossible for us to do our state budget," the governor said. âWe need the representatives to fight for New York just like I do.â
 https://www.ny1.com/nyc/all-boroughs/news/2020/03/15/governor-cuomo-conference-call-saturday-3-14-20