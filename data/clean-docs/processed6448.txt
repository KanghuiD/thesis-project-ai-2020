Global cases surpass 1 million, 6.6 million unemployment claims filed
 The White House is expected to recommend that Americans wear a face covering when they go out.
 On Thursday, the coronavirus death toll in the U.S. topped 5,000 on Thursday, according to NBC News' tally, and nearly 240,000 cases have been confirmed across the country. Globally, more than 1,000,000 people have tested positive and more than 50,000 have died from COVID-19, according to Johns Hopkins University.
 In the United States, government relief payments will begin the week of April 13 — although people who don’t have direct deposit on file with the Internal Revenue Service may have to wait months for checks to arrive, according to a memo obtained by NBC News.
 The economic fallout from the pandemic accelerated with a record 6.6 million jobless claims filed last week.
 Full coverage of the coronavirus outbreak
 Download the NBC News app for latest updates on the coronavirus outbreak.
 This live coverage has ended. Continue reading April 3 Coronavirus news.
 Geoff Bennett
 The White House said Friday that anyone expected to come in "close proximity" to President Donald Trump and Vice President Mike Pence will now receive a test for COVID-19, the disease caused by the coronavirus.
 "As the Physician to the President and White House Operations continue to protect the health and safety of the President and Vice President, starting today anyone who is expected to be in close proximity to either of them will be administered a COVID-19 test to evaluate for pre-symptomatic or asymptomatic carriers status to limit inadvertent transmission," Judd Deere, White House deputy press secretary, said in a statement. 
 Jason Abbruzzese
 Etsy, an online marketplace famous for its homemade goods, is asking its sellers to make masks.
 The company also noted that "skyrocketing demand" was on track to exhaust its current inventory of masks.
 Some areas of the U.S. have asked that people who go out in public wear masks, and the White House is expected to make a recommendation that people in areas of high transmission wear masks.
 Phil Helsel
 U.S. Navy hospital ship USNS Comfort, the 1,000-bed vessel sent to New York City to help alleviate pressure on hospitals, had 19 patients Thursday night, a Navy spokesperson said.
 The ship was one of two dispatched — the other is the USNS Mercy in Los Angeles — and Pentagon officials have said they were to take patients so that hospitals could deal with those suffering from COVID-19.
 In New York City alone, more than 49,700 cases had been confirmed and 1,562 deaths reported as of 5 p.m. Thursday, according to the city's health department. The state overall has more than 92,300 cases and more than 2,300 deaths, according to an NBC News count.
 Gov. Andrew Cuomo told MSNBC's Rachel Maddow on Thursday said that coronavirus cases have overwhelmed hospitals but that that the ship was never supposed to be for COVID-19 patients. 
 Because of the huge demand, Cuomo said he asked President Donald Trump to allow a U.S. Army-run facility at the Javits Center with 2,500 beds to instead be used for COVID-19 patients, and Trump agreed. 
 The commander of the USNS Comfort, Capt. Patrick Amersbach, said Thursday that personnel is following Defense Department orders to accept only non-COVID-19 patients, but if that changes they would adjust. 
 Daniella Silva and Tom Winter
 New York City first responders are handling "tremendously high" call volumes, working multiple double shifts with back-to-back cases and suspected coronavirus patients going into cardiac arrest as the disease continues to sweep the city.
 "Everybody's overworked. ...  People who are working five doubles, five 16-hour tours," in one week, said a New York City Fire Department emergency medical technician who works in the Bronx.
 "You get your two days off, but those days you're just sleeping the whole day because your body's recuperating from so much work," the EMT, who spoke on condition of anonymity, said Thursday.
 Read the full story here. 
 The Associated Press
 WASHINGTON — President Donald Trump is considering intervening to stop the release of some prisoners amid the coronavirus pandemic.
 Correctional facilities in states such as California, Michigan and Pennsylvania have begun releasing certain inmates as the prisons face a shortage of medical supplies.
 Trump said Thursday that “we don’t like it.”
 The president added that “we’re looking to see if I have the right to stop it in some cases.”
 He did not elaborate what measures, or under what legal authority, he would take to stop or reverse the releases.
 RICHMOND, Va. — A Virginia long-term care facility with one of the nation’s worst coronavirus outbreaks announced Thursday that its death toll had reached 16 as new testing confirmed roughly two-thirds of its residents have COVID-19.
 The Canterbury Rehabilitation & Healthcare Center in suburban Richmond tested all its residents earlier this week after the virus began sweeping through the facility in mid-March, a time when limited supplies and strict policies on who could be tested meant such a response was not possible.
 Ninety-two in-house or hospitalized residents have tested positive, the facility said in a statement, up from 41 positive tests earlier in the week. Only 35 tested negative, and 15 tests were still pending, meaning approximately two-thirds of the facility had become infected with the virus.
 Kristen Welker and Geoff Bennett
 The White House is expected to urge Americans who live in areas of high coronavirus transmission to wear cloth face coverings, a senior administration official told NBC News on Thursday night.
 Vice President Mike Pence addressed the potential for a mask advisory, based on U.S. Centers for Disease Control and Prevention guidance, at Thursday's daily news briefing on the pandemic. He said the White House would weigh in on such guidance "in the days ahead."
 White House coronavirus response coordinator Dr. Deborah Birx cautioned that any recommendation on masks must be "additive" and not a substitute for existing social distancing guidelines. 
 BOSTON — The New England Patriots private team plane returned to Boston from China on Thursday carrying most of an order of 1 million masks critical to health care providers fighting to control the spread of the coronavirus.
 Standing in front of the plane on the tarmac at Logan International Airport, Massachusetts Gov. Charlie Baker thanked Patriots owner Robert Kraft, officials in China who partnered with the state, and medical workers who need the masks.
 “This shipment comes at a critical time as we prepare for an anticipated surge in the coming weeks ahead,” Baker said. “What we were able to accomplish with this particular mission will go a long way forward in this fight.”
 Baker secured the N95 masks from Chinese manufacturers but had no way of getting them to the U.S. Baker said Thursday an earlier order for 3 million masks had been confiscated at the Port of New York and this time he wanted a direct humanitarian delivery to the state.
 It took a lot of teamwork to secure the mask shipment on our team plane. Join our efforts to provide more essential personal protective equipment to our healthcare workers who are courageously fighting COVID-19: https://t.co/23xDMPbJjn
 https://www.nbcnews.com/health/health-news/live-blog/2020-04-02-coronavirus-news-n1174651/ncrd1175531
