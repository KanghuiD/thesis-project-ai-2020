Opinion - The Japan Times
 22
 CLOUDY
 A million COVID-19 deaths must drive world to act together
 			The economic impact of this crisis has reached historic proportions; and, while not as bad as the Great Depression, it is far worse than the Great Recession a decade ago.		
 U.N. fails to shine at its diamond jubilee
 			The world is facing a set of unprecedented crises and the world body is distracted by geopolitical rivalries and a growing tendency of governments to go it alone.		
 'Suganomics' and India-Japan economic ties: A seven-point plan
 Suga’s clear global focus can be an advantage for India and Indian talent as it may open more doors for educational and professional exchanges between citizens.
 An appalling debate is only the latest blow to the U.S. image in the world
 Last week’s U.S. presidential debate was engrossing — as is a car crash or a tawdry reality TV show. The rudeness and incoherence left most viewers aghast.
 									Prepare to get rid of hanko seals, Suga tells government								
 									WeWork competitor TKP sees opportunity in Tokyo’s shrinking offices								
 									Yamanashi pushes back against Japan's drive to ditch personal seals								
 									Teetotalers emerge from the shadows in hard-drinking Japan								
 									Virtual tours become new normal for pandemic-era nursery hunting								
 				DEEP DIVE			
 				Episode 66: Kōji — the magical mold of Japan			
 				LAST UPDATED: Oct 1, 2020			
 				Directory of who’s who in the world of business in Japan
 			
 				LAST UPDATED: Jun 8, 2020			
 Formidable challenges ahead as Suga era begins
 			Suga’s career has been marked by hard work. He knows how to maneuver in a bureaucracy. But those efforts, until now, have been in the service of someone else’s agenda.		
 Apparent poisoning of Alexei Navalny reveals Putin's world
 			Moscow must be punished for its actions and Putin stripped of the delusion that he is untouchable.		
 Dangerous tensions at the top of the world — again
 			Nationalist leaders, the general antagonistic public, military confrontations and nuclear-armed neighbors: This is as dangerous a combination as can be imagined.		
 What will be Abe's legacy as the longest-serving prime minister?
 			For someone who has held the top office so long, his accomplishments don't amount to a lot.		
 Work-style reform needed at the government's center
 			Shackled by an outdated system, Japan's national bureaucrats are working dangerously long hours.		
 Bump up the target for increasing renewable energy
 			It's time to shift gears in the effort to restructure the nation's energy landscape.		
 Japan's stalled nuclear fuel cycle policy
 			While the disposal of the spent nuclear fuel is big challenge that must be tackled, it should not stop the government and the power industry from rethinking the costly fuel cycle program.		
 No more 'lost decades' for women's empowerment
 			How many years must we wait until Japan becomes an ordinary country?		
 Support medical institutions and staff combating COVID-19
 			The pandemic's financial toll on hospitals and clinics has been staggering.		
 Saving the most vulnerable in disasters
 			Measures are needed to ensure the protection of the people who are at greatest risk.		
 COVID-19 disobedience goes deeper than we think
 While respect for mask-wearing and personal hygiene is broadly high, according to surveys of European countries, support for quarantine and self-isolation is wavering.
 How to end the pandemic this year
 A properly designed universal COVID-19 testing program could bring the pandemic to an end within just a few months.
 Would a universal basic income make us lazy or creative?
 We are indeed in the midst of a digital transformation that will destroy many old jobs and create many new ones, and we need structures that help people adapt.
 Australia's miners carve themselves an ugly heritage
 Digging iron ore out of the ground and carrying it to port inevitably involves conflicts between miners and the Aboriginal traditional owners.
 The perils of COVID-19 and big brother government in Asia
 Governments’ newfound authority to regulate behavior and collect personal data due to the pandemic risks enabling serious violations of civil liberties and human rights.
 War in the Caucasus will spread to Russia and Turkey
 What is particularly dangerous in this latest flare-up is that Moscow and Ankara are strongly backing different horses.
 India needs to copy China better
 To lure manufacturers away from China, India is going to have to convince them that they’ll be able to operate just as easily and efficiently.
 Sponsored contents planned and edited by JT Media Enterprise Division.
 広告出稿に関するおといあわせはこちらまで
 Read more
 It’s no use crying over spilled oil
 Trump can't make socialism a four-letter word
 Abe leaves a mixed legacy
 Japanese leaders put economy over citizens’ well-being
 Nuclear waste disposal is a matter of environmental concern
 Language matters
 The "frozen conflict” between Armenia and Azerbaijan has turned very hot. What may seem to many Westerners a minor clash in a remote corner of the world actually has significant implications for regional security, energy markets and the ambitions of two problematic strongmen: Vladimir Putin of Ru...
 What if U.S. forces return to Taiwan?
 An ugly but strategically important debate recently erupted between China and the United States over Taiwan.
 The truth is, it is really of no concern as long as the debate is placed in the category of a war of abusive language between hawkish dragon slayers in Washington and equally hawkish an...
 A poem about COVID-19
 Wash your hands! Don’t go out!
 If you must, then mask it!
 Lest you end up
 in an unwanted casket.
 Self isolating and avoiding the street,
 trying not to lose your job
 while making ends meet.
 Still, Bezos, Ma, Zuckerberg and Gates
 are making profits at dizzying rates.
 While Big Pharma
 is w...
 Donald Trump is not a racist
 Regarding the Aug. 14 article “Trump gives credence to false, racist Kamala Harris theory,” the lunatic-left, fake-news media has “reported” for 45-plus years on Trump’s “racism,” but the facts below prove unequivocally that he is the most racially colorblind president ever. Period!
 In 1973, T...
 China is paying a high price for provoking India
 China’s foreign minister, Wang Yi, recently declared that aggression and expansionism have never been in the Chinese nation’s “genes.” It is almost astonishing that he managed to say it with a straight face.
 Aggression and expansionism obviously are not genetic traits, but they appear to be de...
 https://www.japantimes.co.jp/opinion/
