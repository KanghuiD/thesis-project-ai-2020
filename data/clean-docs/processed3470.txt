How to feed your family during a coronavirus outbreak - The Boston Globe
 A trip to a big box store last week confirmed shoppers are frantically preparing for a coronavirus outbreak. There are customer limits on toilet paper and bottled water at Costco, and there are empty shelves in grocery stores where the hand sanitizer used to be. Even the ingredients for homemade hand sanitizer (aloe vera gel and rubbing alcohol) are sold out. It’s a good sign that people are preparing: The Centers for Disease Control and Prevention recommends a 14-day supply of food, cleaning supplies, water, and other personal accessories in the event of school or business closings.
 I made a shopping list that included items I normally buy and would definitely use in the future, whether or not quarantine is necessary. I tried to imagine what two weeks at home would feel like for my family, or another one with kids at home to feed and amuse. With a disruption in normal everyday routines, why not make those two or three weeks, if they occur, into an adventure? I pondered how to use ingredients outside my usual routine, and I invite you to do that, too.
 You may have a different list of preferred ingredients. This one is not meant to be all-inclusive, but should serve as a jumping off point for ideas. The notion of peasant food — simple food with few ingredients, which rural folks have cooked for centuries, was the overarching theme. If you have children at home, don’t forget treats, sweet stuff, and snacks that have the potential for involving participation in the process. (Activities!)
 Make it an adventure, and try not to push the panic button.
 THE PANTRY
 Start by surveying your shelves to see where the gaps are. Then add a couple of new items just for fun. Here’s what to do with some pantry items you already know.
 • Spaghetti: A jar of tomato sauce tossed with spaghetti is going to wear thin quickly. Try tossing spaghetti with toasted breadcrumbs, Parmesan, olive oil, salt, and pepper; it’s easy, tasty, and surprisingly satisfying. (See the recipe below.)
 • Brown or white rice: Sure, you could make fried rice, but what about a filling frittata made with frozen spinach or peas. Saute 1 cup defrosted and drained frozen spinach with 1 clove of chopped garlic in a little butter. Add 1 cup cooked rice, 1 cup grated Parmesan, 6 eggs (for an 8-inch ovenproof skillet) whisked with ¼ cup whole milk, sour cream, or full-fat yogurt. Bake at 350 degrees for 20 to 30 minutes. Don’t forget salt and pepper.
 • Canned or cooked dried chickpeas (garbanzos): Make a curried dish to serve over rice. Sauté a small chopped onion, 1 chopped fresh or canned jalapeno, some chopped fresh or dried ground ginger, and 1 to 2 tablespoons garam masala or curry powder in 2 tablespoons of butter. Add a 15-ounce can of drained chickpeas, a 15-ounce can of diced tomatoes, water, and salt. Stir in lemon juice or a splash of vinegar.
 • Small, green lentils: Soup has to be on the menu, but make it tasty and skip the cans. Sauté more chopped carrots and onions than you think you need in olive oil with ground coriander and cumin. Add water or stock and lentils and simmer until they are tender. Then stir in frozen spinach or other chopped greens, if you have them, and a hefty squeeze of lemon juice. Diced canned tomatoes would be another good addition.
 • Chicken stock and bouillon cubes: You’ll need them for soups and stews. A simple, satisfying bowl of broth with an egg and a sprinkle of Parmesan could be your go-to comfort meal. For one person, bring 2 cups of chicken stock to a boil. Add about ⅓ cup cooked rice or thin noodles, or cook a little pasta in the broth. If you have fresh or reconstituted dried mushrooms, or leftover greens, add a few of them, but don’t overcrowd the soup, it should be brothy. Crack an egg into a cup and slide it into the simmering broth. Cook until the egg is set, ladle into a bowl and sprinkle with Parmesan.
 • Whole, raw almonds: Sprinkle the nuts with olive oil, salt, pepper, and a little chili powder. Roast in a 350-degree oven for 10 minutes. Cocktails, anyone?
 • Peanut butter: Your kids might be happy to live on PB&J sandwiches forever, but treat them to Chocolate Peanut Butter Banana Nice Cream (see recipe below.) You could make something similar with frozen berries and almond butter.
 • Golden raisins: A handful thrown into rice, quinoa, or another cooked grain along with a few chopped nuts gives your rice a fancy makeover. Stir in a little turmeric if you want some color.
 • Tuna in a can or jar: Tuna sandwiches for days? Probably not. Drain some canned cannellini beans and season them generously with olive oil, vinegar, or lemon juice, salt, and pepper. Stir in some parsley if you have it, and add chunks of tuna. Add capers or chopped red onion, too, if you like.
 • Crushed tomatoes: Crushed tomatoes have a little more texture than plain old sauce or puree, which make them ideal for shakshuka, a popular North African and Middle Eastern dish of eggs and tomatoes. In a wide skillet, sauté some chopped onions and garlic in olive oil with spices like cumin, coriander, or harissa for some heat. If you have some peppers from a jar or chickpeas, add them along with a can of crushed tomatoes. Thin with stock or water to make it saucy, and bring to a simmer. One at a time, crack eggs into a cup, and slide each on top of the sauce. Cover the pan and cook for 8 to 10 minutes, or until the eggs are just set.
 • Chocolate chips: Make one giant skillet cookie. Press your favorite chocolate chip cookie dough into a buttered 10-inch ovenproof skillet. Bake at 350 degrees for 35 to 40 minutes, or until the top is golden. Cool for 15 to 20 minutes and cut into wedges.
 • Instant yeast: Instant yeast, also known as rapid rise, has smaller granules than active dry yeast, and does not need to be mixed with water to hydrate before using. When your bread supply runs low, or if you just want to have a little baking fun, make No-Knead Sandwich Loaf (see recipe below.) The dough is good for several days in the fridge, and can be used to make pizza, too. The bread’s moist and chewy texture makes fantastic toast.
 • Panko: The dried breadcrumbs made from bread without crusts that are used in Japanese cooking, are lighter, crunchier and flakier than regular breadcrumbs. Toss them with olive oil and salt, toast them it in a skillet, add chopped almonds, garlic, dried herbs, or Parmesan and sprinkle them on vegetables, pasta, or rice.
 • Tortillas: Avoid the obvious — quesadillas and tacos — and make little pizzas. Tortillas, pita, and naan loaves are also candidates. Brush them with olive oil and bake in a 450-degree oven for 5 minutes. Remove and go to town with toppings: thin slices of potatoes, a smear of tomato sauce, dried herbs, feta, cheddar, mozzarella, thin slices of onion, peppers from a jar, olives, pepperoni, whatever else is on hand. Return to the oven for another 5 minutes, or until the cheese melts and the edges are brown and crispy.
 •Miso, balsamic vinegar, anchovy paste: These are the triumvirate of flavor enhancers. Use them to perk up soups, salad dressings, and roasted root vegetables.
 • Wine: Stock up so you can pour yourself a glass to accompany binge watching after a long day in the confines of your castle.
 THE FREEZER
 Your freezer doesn’t provide much real estate, so stock it wisely. Don’t forget, you can freeze butter, grated cheese, milk, and eggs, too.
 • Frozen peas: Peas add some green to your world when you stir them into pasta or rice. A quick, fresh pea soup is easy to put together. Sauté chopped onions in butter, add a package of frozen peas and stock to cover. Simmer briefly and puree in a blender until smooth. Top each bowl with a spoonful of yogurt or sour cream and a drizzle of diluted harissa paste if you like it spicy.
 • Frozen spinach: You can’t beat frozen spinach to add to omelets or to stir into just about any soup or broth. Or make a spinach pesto to toss with pasta: In a food processor, puree about 1 cup defrosted spinach, ¼ cup toasted walnuts, pine nuts, or almonds, ½ cup grated Parmesan, 1 small clove chopped garlic, ¼ cup olive oil, salt, and pepper. Add enough water to obtain a smooth consistency.
 • Frozen strawberries: A smoothie for breakfast goes down a lot easier than just about anything else for those who aren’t early morning breakfast enthusiasts. In a blender, puree ¼ cup rolled or quick cooking oats, ½ cup almond or other milk, ½ cup yogurt, and 1 cup frozen strawberries. The oats provide enough fiber to keep you going straight through until lunch.
 • Frozen bananas: Never throw out a banana again! Peel end-of-the road bananas and wrap them individually in plastic wrap. They’ll keep in the freezer for at least 2 months. Use them in smoothies, banana bread, or in Chocolate Peanut Butter Nice Cream (see recipe below).
 THE FRIDGE
 Stock up on the things you use on a regular basis. Check your condiments and replenish your favorites or must-haves, such as mustard, jam, ketchup, tahini, mayonnaise, hot sauce, sriracha, horseradish, harissa, sesame oil, vinegar, jalapeno peppers in brine, jars of red peppers, and soy sauce.
 For more information about food storage and safety: Foodsafety.gov/keep-food-safe
 • Eggs: A carton of eggs in the shell will keep at least 3 weeks in the fridge so it’s safe to stock up and have them on hand for baking or omelets. Eggs can also be frozen for up to about a year. For vanilla pudding in the microwave: In a bowl, whisk until smooth ¼ cup sugar, 2 tablespoons cornstarch, a pinch of salt, 2 cups milk, and 2 eggs. Strain into a microwave-safe bowl. Microwave on high for 2 minutes. Remove and whisk. The whisk should leave a trail. If it doesn’t, microwave at 10 second intervals until it does. Whisk in 1 tablespoon butter and 1 teaspoon vanilla. Pour into 3 or 4 bowls and sprinkle with nutmeg. Refrigerate until cold.
 • Parmesan: A chunk of Parmesan will keep from 4 to 6 weeks in the refrigerator. Once it is shredded, it is susceptible to mold. Both chunks and shredded cheese can be frozen for 3 months. Make your own Parmesan crisps to serve with soup or as a snack: Line a baking sheet with parchment. Without crowding them, drop tablespoon-size spoonfuls of grated Parmesan onto the sheet. Flatten them so they are about 3 inches across. Bake in a 350-degree oven for about 5 minutes, or until golden. Cool on the baking sheet.
 • Feta: Buy feta in a block, rather than already crumbled, then crumble it over rice, lentils, or make a spicy appetizer: Cut ½-inch-thick slabs from a block of feta. Place the slices in an oiled baking dish, drizzle with more olive oil, plenty of dried oregano, and black pepper. Add some olives to the dish and top with slices of jalapeno. Bake at 450 degrees for 20 minutes, or until feta turns golden at the edges. Serve with crusty bread.
 • Carrots: If you’re nearing the end of your fresh vegetable supply, a salad of grated carrots will perk up your taste buds. Peel and grate about a pound of carrots on the large holes of a box grater. Make a dressing with 2 tablespoons each of olive oil and lemon juice. Add 1 teaspoon Dijon mustard, a few drops of honey, salt, and pepper and mix it with the carrots. Add chopped parsley, capers, chickpeas, or raisins if you like.
 • Lemons: Lemons perk up just about everything, and they will keep for 3 to 4 weeks if stored in a plastic bag. If they start to look tired, extract the juice and store it in a jar in the fridge to keep for a few more days, or freeze in ice cube trays for 3 months or longer. For a quick lemon and artichoke pasta, cook fettuccine, spaghetti, or linguine. Reserve about ½ cup pasta cooking water, and drain. Return the pasta to the pot, add olive oil, salt, pepper, plenty of lemon juice, enough pasta water to make it saucy, and a can of drained quartered artichoke hearts. Sprinkle with toasted breadcrumbs, Parmesan, and chopped almonds.
 • Yogurt: Make overnight oats (aka muesli): Place a scant ¼ cup rolled or quick oats in an 8-ounce jar. Cover with almond milk, and top with plain yogurt. Mix in a spoonful of raisins or cranberries, and a little honey if you have a sweet tooth. Refrigerate for at least 8 hours, or for up to 5 days. The oats will soften in the yogurt mixture by the next day and you can assemble multiple jars ahead. Sprinkle with chopped nuts or frozen blueberries for a quick breakfast treat.
 • Butter: For a treat, mix 1 stick of softened unsalted butter with 2 tablespoons honey, 1 tablespoon confectioners’ sugar, a little cinnamon or five-spice powder, and a generous pinch of salt. Spread on toast or make waffles for supper and spread them with the butter.
 Sally Vargas can be reached at sally.p.vargas@gmail.com
 Spaghetti with Breadcrumbs
 Serves 4
 This version of the Sicilian-style pasta with breadcrumbs is bare bones. It invites improvisation. Mash anchovies with the oil in a skillet or add anchovy paste or fish sauce. Other additions could include olives, capers, golden raisins, or tuna. While the pasta cooks, toast the breadcrumbs. It adds up to a quick and satisfying meal.
 Salt, to taste
 1 pound spaghetti
 2 garlic cloves, peeled and halved
 ⅓ cup olive oil, or more as needed
 1 cup panko
 Large pinch of red pepper flakes or Aleppo pepper
 ½ cup chopped parsley
 Freshly ground black pepper, to taste
 Lemon wedges (for serving)
 1. Bring a large pot of salted water to a boil. Cook the spaghetti for 10 minutes, or according to package directions. Scoop out ½ cup of the pasta water and drain the pasta. Return it to the pot.
 2. In a skillet over medium heat, cook the garlic cloves in ¼ cup of the oil, turning with tongs, until the garlic is golden. Remove and discard the garlic and add the panko and red pepper to the pan. Cook, stirring constantly, for 2 to 3 minutes, or until the breadcrumbs are golden. Remove from the heat.
 3. Add the remaining olive oil and a little of the pasta water to the spaghetti until it coats the pasta. Stir in half the toasted crumbs, half the parsley, salt, and pepper and toss. Divide among 4 shallow bowls and sprinkle with more black or red pepper and the remaining parsley and breadcrumbs. Serve with lemon wedges. Sally Vargas
 No-Knead Sandwich Bread
 Makes one 9-by-5-inch loaf or four 10-inch pizzas
 A young, aspiring baker could make this loaf, and so can you. Mix the dough in a bowl with a spoon (you can double or triple the recipe if you like), and let it rise in the refrigerator overnight. The refrigerated dough can be used after 8 hours, or for up to 3 days. That means you can bake a loaf every morning and have sandwich bread by lunchtime. But wait, there’s more! The dough is great for pizza, too. Let each family member flatten their piece of dough and add their own toppings. Bake pizzas on a baking sheet at 450 degrees for 10 to 12 minutes, or until browned and crispy.
 4¼ cups all-purpose flour
 1½ teaspoons instant (“rapid rise”) yeast
 2 teaspoons salt
 1⅔ cups water
 1 egg beaten with 2 tablespoons water (optional, for glaze)
 1. In a large bowl, whisk the flour, yeast, and salt until combined. Add the water and stir together until combined. Cover with plastic wrap and refrigerate the dough overnight.
 2. Remove the dough from the refrigerator and let rest for about 2 hours to come to room temperature.
 3. Spray a 9-by-5-inch loaf pan with vegetable oil spray or brush it with oil.
 3. On a generously floured work surface, turn out the dough. Sprinkle it lightly with flour and pat the dough into an 8- by 12-inch rectangle. Position the short side of the rectangle so that it is parallel to the edge of the counter. Fold the dough into thirds as you would fold a business letter: bring the bottom third up and fold the top third down to meet it. Rotate the envelope of dough one quarter turn. Stretch it into a rectangle again and fold it as before. Rotate the envelope so the seam is on top and pinch it firmly together to secure the dough into a log. Flip it over so that the seam is on the bottom. If necessary, roll the dough back and forth, moving from the center outward, until you have a log that is the same length as your bread pan.
 4. Place the dough in the pan with the seam side down. Cover loosely with an oiled piece of plastic wrap and let rise for 1 to 1½ hours, longer if the room is cool. The center of the loaf should dome about one inch above the rim of the pan.
 5. About 20 minutes before the loaf is ready to be baked, position a rack in the lower third of the oven and set the oven to 400 degrees. When the dough has risen, make three shallow, diagonal slashes across the top of the loaf with a serrated knife. Bake the loaf for 10 minutes and decrease the oven temperature to 375 degrees. Bake for an additional 25 to 30 minutes, or until the crust browns. (Total baking time is 35 to 40 minutes.)
 6. Tip the loaf out of the pan and tap the bottom of the loaf. It should make a hollow sound and the bottom of the loaf should have browned. If it still seems squishy, put it back in the oven, directly on the oven rack without the pan, and bake for another 5 minutes or so. If you are still wondering whether or not it’s done, insert an instant read thermometer into the bottom of the loaf — the bread is done when it registers 190°F.
 7. Set the loaf on a rack to cool completely. Do not cut into the bread until it is thoroughly cool; it continues to bake and set as it cools. Once it is completely cool, store it in a plastic bag or wrap it in a clean tea towel. Sally Vargas
 Chocolate Peanut Butter Banana “Nice” Cream
 Serves 2 generously
 Chocolate, peanut butter, bananas. What’s not to like? The bananas add sweetness and a creamy texture. You could wait for this to become firm in the freezer, or just dig in for an instant soft-serve treat.
 3 frozen bananas, thickly sliced
 2 tablespoons peanut butter
 2 tablespoons unsweetened cocoa powder
 3 tablespoons whole milk, almond milk, or milk of choice
 Pinch of salt
 ½ teaspoon vanilla extract
 1. In a food processor, combine the bananas, peanut butter, cocoa powder, milk, salt, and vanilla.
 2. Process until smooth and creamy. Transfer to a freezer container and chill for 2 to 3 hours, or until firm. Sally Vargas
 Digital Access
 Home Delivery
 Gift Subscriptions
 Log In
 Manage My Account
 Customer Service
 Help & FAQs
 Globe Newsroom
 Advertise
 View the ePaper
 Order Back Issues
 News in Education
 Search the Archives
 Terms of Purchase
 Work at Boston Globe Media
 https://www.bostonglobe.com/2020/03/09/lifestyle/hope-best-prepare-worst-how-feed-your-family-during-coronavirus-outbreak/
