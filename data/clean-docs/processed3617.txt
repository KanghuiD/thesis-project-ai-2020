 Anderson: Animal neglect a concern amid coronavirus outbreak - Opinion - Sarasota Herald-Tribune - Sarasota, FL
 Lisa Chittaro, former assistant state attorney who used to prosecute animal neglect cases, is concerned coronavirus will lead to abuse.
 Astro likes bacon, Omar makes snow angels and Mr. Wiggles always hogs the bed.
 Scroll through the internet and you can’t miss people posting about their cute and cuddly doggies. Animals may be more important to us now than at any point in recent history as they connect and comfort us during a time of fear and isolation.
 But as the coronavirus pandemic continues to affect the country on a deeper level each day, the welfare of animals soon could become a concern. There are certain to be problematic offshoots from the virus — aspects no one has given much consideration to yet — and the safety of the most vulnerable is something to be cognizant of.
 It’s like Easter, when there is a run on rabbits, or the demand for Dalmations when the Disney movie came out; people are turning to pets and adoptions to combat the emotional void the virus has dealt. Humans are social creatures, and isolation does not sit well.
 #apolloLink{color:#000;background-color:#F4BE11;text-shadow: none;padding: 8px 15px 10px;font-family: 'Roboto', sans-serif;font-weight: 600;border-radius:10px;}See our complete coverage of the coronavirus outbreak
 But there are economic issues that are certain to come into play. Pets cost money. They need to eat. They need grooming. They need vet care. A record 10 million people filed for unemployment over the last two weeks, and the situation does not promise to improve anytime soon. Many are in need of money already.
 What happens if this gets worse and it comes down to feeding yourself or providing your pet with essential needs?
 And pets such as dogs and cats are one thing. What about all of the people who own horses in the area? Horses cost thousands of dollars a year in upkeep. What if the mortgage is due?
 “Are people going to have to start making a choice?” said Lisa Chittaro.
 Chittaro is a former assistant state attorney who specialized in elder fraud and animal cruelty cases. She is running for state attorney for the 12th Judicial Circuit.
 She has many concerns about animal welfare. She has seen abuse and neglect at levels nearly unimaginable. She handled the case of Alan and Sheree Napier, who were convicted on several animal abuse charges after a 2014 raid on their property revealed more than 300 animals were severely mistreated. It was one of the worst abuse cases in Manatee County history. Not that any of them are good.
 #apolloLink{color:#000;background-color:#F4BE11;text-shadow: none;padding: 8px 15px 10px;font-family: 'Roboto', sans-serif;font-weight: 600;border-radius:10px;}SUPPORT LOCAL NEWS: Consider subscribing today
 “Every image has stayed in my head and has never, ever left,” she said.
 Not everyone intends to neglect their pet, but sometimes economic hardship leads to it. People also conflate hoarding with love, Chittaro said. They feel they are doing the animals favors, and thus they keep bringing more in. Sometimes people grow so lonely they keep adding pets. Soon, they can’t care for them all. They can’t afford to.
 Chittaro remembers a man who had more than 30 cats. Several were dead when she visited the home that he lived in.
 “To him it was love,” she said.
 Being confined to homes can lead to frustration, which can lead to animal abuse. Fuses are short, panic is high. A barking dog can become too much. Or maybe a person who loses their job takes it out on the animal.
 Hopefully, a pet owner will take a deep breath before their frustration grows too deep, and maybe turn on their computer. Cute dogs are everywhere. Like Dempsey. He can fit two tennis balls in his mouth.
 “Things like this bring out the best of us and the worst of us,” Chittaro said.
 Contact columnist Chris Anderson at chris.anderson@heraldtribune.com.
 #apolloLink{color:#000;background-color:#F4BE11;text-shadow: none;padding: 8px 15px 10px;font-family: 'Roboto', sans-serif;font-weight: 600;border-radius:10px;}
 See our complete coverage of the coronavirus outbreak
 SUPPORT LOCAL NEWS: Consider subscribing today
 							Choose the plan that's right for you.
 							Digital access or digital and print delivery.
 						
 Original content available for non-commercial use under a Creative Commons license, except where noted.
 Gannett | USA TODAY NETWORK
 Choose the plan that’s right for you. Digital access or digital and print delivery.
 https://www.heraldtribune.com/opinion/20200406/anderson-animal-neglect-concern-amid-coronavirus-outbreak