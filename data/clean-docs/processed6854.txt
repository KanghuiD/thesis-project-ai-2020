COVID-19: Latest Travel and Immigration Updates [28 April 2020]
 Menu
 Posted on: 2020-04-28
 This is the latest immigration and travel information from Newland Chase. This dedicated page is updated daily at 11am BST and 5.30pm BST and at 7am EST and 1.30pm.
 28 April 2020 | Updated: Belgium, Czech Republic
 * * * * * * * * *
 It is an unprecedented time. As a result of the COVID-19 global pandemic, many countries have implemented restrictions on entry and exit, visa and work permit issuance, closed borders, tightened quarantine rules and taken other measures in an attempt to slow the spread of COVID-19. These restrictions may affect international business travel and assignment plans.
 Some countries have also introduced immigration concessions to facilitate applications; for example, moving to online applications, accepting soft copies of documents, granting automatic extension approval and more. However, these concessions must be reviewed country by country as the requirement to make a notification or other applications is often still in place.
  hbspt.cta._relativeUrls=true;hbspt.cta.load(1806722, '1c377eda-b501-47b7-ac9d-cda41fdc960d', {});  View our comprehensive guide, which gathers together the knowledge and data we have on entry restrictions, immigration restrictions (office closures, visa suspensions, etc.) and immigration concessions, worldwide. Updated twice daily as new information is released. 
 Schedule a custom 30-minute COVID-19 travel and immigration consultation tailored to your foreign national population and receive actionable guidance on your three highest volume countries, general advice for possible changes in circumstances facing your foreign national population, and insights on what your company can do now to ensure you’re in the best position to move forward when countries re-open to travel. 
 Disclaimer: We have aggregated information about some immigration-related restrictions imposed by some countries in response to this situation. The guide above contains information abridged from laws and administrative rulings and should not be construed or relied upon as legal advice.
 To ensure you're receiving the information most crucial to your program, we are providing region- and country-specific webinars featuring APAC, Singapore, Europe, UK, Canada, Mexico (in Spanish), Americas, and Brazil (in Portuguese). Sign up.
                              
 Australian Migration Agents Registration Authority Number 9902581
 https://blog.newlandchase.com/covid-19-latest-travel-and-immigration-disruptions
