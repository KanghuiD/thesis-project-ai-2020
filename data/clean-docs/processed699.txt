Obituaries | Financial Times
 										Coronavirus business&nbspupdate
 										Get 30 days complimentary access to our Coronavirus Business Update newsletter
 									
 										30 days complimentary
 Add this topic to your myFT Digest for news straight to your inbox
 A pioneer who changed lives for lesbians of her generation
 Songwriter whose soul was a quiet symbol of black pride
 An incurable optimist who fought to unite the Greek left
 A gracious man, puzzled by the phenomenal appeal of a village of indomitable Gauls
 A pioneering, outspoken voice for the marginalised
 Master of high miserabilism who starred in ‘The Exorcist’ and ‘The Seventh Seal’
 A revolutionary Nicaraguan mystic with his feet firmly on the ground
 Peruvian diplomat who led global body during pivotal era that culminated with end of cold war
 Celebrity executive who brutally transformed General Electric
 A human ‘computer’ who broke colour barriers and was essential to early space flight
 Former strongman whose 30-year reign ended with 2011 revolution
 No historian did more to let the light in on the workings of European diplomacy
 A perfectionist who trod softly both on stage and off
 He gave a tawdry glamour to the world’s second oldest profession
 For a postwar movie generation Kirk Douglas was the definition of ‘macho’
 Autocratic paternalist who dispensed patronage in the name of the state
 The ‘Tunisian Girl’ who exposed the secrets of an oppressive regime
 Management thinker hooked generations of entrepreneurs on ‘disruptive innovation’
 The versatile Monty Python star who sided with the underdog
 A polarising trailblazer for confessional memoir beloved among young writers
 Deft diplomat who brought unity and prosperity to the third-largest country on the Arabian peninsula
 The ‘pope of Beaujolais’ who lent cult status to a new wine
 A sharp-eyed editor and foreign correspondent who turned a killing phrase
 Military man responsible for Tehran’s engagement overseas killed in US air strike
 Flamboyant editor who transformed the lives of some of the world’s greatest authors
 International Edition
 https://www.ft.com/obituaries
