Influencers among 'key distributors' of coronavirus misinformation | Media | The Guardian
 Study suggests mainstream news outlets struggling to compete with celebrities’ and politicians’ reach
 Jim Waterson Media editor
 Wed 8 Apr 2020 17.08 BST
 Celebrities and politicians with large social media followings are proving to be key distributors of disinformation relating to coronavirus, according to a study that suggests the factcheckers and mainstream news outlets are struggling to compete with the reach of influencers.
 Covid-19 is caused by a member of the coronavirus family that has never been encountered before. Like other coronaviruses, it has come from animals. The World Health Organization (WHO) has declared it a pandemic.
 According to the WHO, the most common symptoms of Covid-19 are fever, tiredness and a dry cough. Some patients may also have a runny nose, sore throat, nasal congestion and aches and pains or diarrhoea. Some people report losing their sense of taste and/or smell. About 80% of people who get Covid-19 experience a mild case – about as serious as a regular cold – and recover without needing any special treatment.
 About one in six people, the WHO says, become seriously ill. The elderly and people with underlying medical problems like high blood pressure, heart problems or diabetes, or chronic respiratory conditions, are at a greater risk of serious illness from Covid-19.
 In the UK, the National health Service (NHS) has identified the specific symptoms to look for as experiencing either:
 As this is viral pneumonia, antibiotics are of no use. The antiviral drugs we have against flu will not work, and there is currently no vaccine. Recovery depends on the strength of the immune system.
 Medical advice varies around the world - with many countries imposing travel bans and lockdowns to try and prevent the spread of the virus. In many place people are being told to stay at home rather than visit a doctor of hospital in person. Check with your local authorities.
 In the UK, NHS advice is that anyone with symptoms should stay at home for at least 7 days. If you live with other people, they should stay at home for at least 14 days, to avoid spreading the infection outside the home.
 Research by Oxford’s Reuters Institute for the study of journalism found that while politicians, celebrities and other prominent public figures were responsible for producing or spreading 20% of false claims about coronavirus, their posts accounted for 69% of total social media engagement.
 The issue has gained extra prominence as Britons began vandalising mobile phone masts in recent days amid wildly sharing baseless claims linking the virus to 5G.
 There is growing concern that online disinformation could be having real world health impacts. Research by Dr Daniel Allington, senior lecturer in social and cultural artificial intelligence at King’s College London, suggested there was a statistically notable link between people who believed false claims about the coronavirus and people who were willing to flout the government’s social distancing guidelines.
 His findings, based on a experimental study conducted in coordination with the Centre for Countering Digital Hate, found that people who said they believed coronavirus was connected to 5G mobile phone masts are less likely to be staying indoors, washing their hands regularly or respecting physical distancing.
 “This small amount of people have a wide reach for the content that they are spreading,” said Scott Brennen, a research fellow at the Reuters Institute. “The most common claims had to do with the policies and actions of public authorities, although we saw plenty of misinformation about the medical side.”
 The service said: “The alarmist information being shared in the message is not correct. We would urge people to disregard the message and not share it further.”
 Additional reporting by Martin Belam
 Celebrities who have drawn criticism for spreading coronavirus disinformation
 The British singer, who is also an opponent of vaccination programmes, has posted a series of tweets criticising engineers installing fibre broadband outside her London home while suggesting the supposed “symptoms” of being near 5G base stations were similar to those of coronavirus.
 https://www.theguardian.com/media/2020/apr/08/influencers-being-key-distributors-of-coronavirus-fake-news
