First Peer-Reviewed Coronavirus Vaccine Trial Shows Promising Results in Mice
 Researchers at the University of Pittsburgh School of Medicine announced today that they have developed a promising new COVID–19 vaccine candidate.
 Early animal trials have shown promise so far, but human trials are still in the planning stages. The researchers already had a big leg up from past epidemics.
 "We had previous experience on SARS-CoV in 2003 and MERS-CoV in 2014," said Andrea Gambotto, co-senior author of the peer-reviewed paper published in the journal EBioMedicine, and associate professor of surgery at the Pittsburgh School of Medicine, in a statement.
 "These two viruses, which are closely related to SARS-CoV–2, teach us that a particular protein, called a spike protein, is important for inducing immunity against the virus," Gambotto explained. "We knew exactly where to fight this new virus."
 The vaccine dubbed "PittCoVacc" (Pittsburgh Coronavirus Vaccine) works in the same basic way as a flu shot: By injecting lab-made pieces of viral protein into the body to help it build an immunity.
 When tested in mice, the researchers found that the number of antibodies capable of neutralizing the deadly SARS-CoV–2 virus surged two weeks after delivery.
 Instead of being delivered through a needle, the new drug is administered through a microneedle array, a Band-Aid like patch made up of 400 tiny microneedles. Once the patch is applied, the microneedles, which are made entirely of sugar and protein dissolve, leaving no trace behind.
 "We developed this to build on the original scratch method used to deliver the smallpox vaccine to the skin, but as a high-tech version that is more efficient and reproducible patient to patient," said co-senior author Louis Falo, professor and chair of dermatology at Pitt's School of Medicine, in the statement. "And it's actually pretty painless – it feels kind of like Velcro."
 According to the researchers, these patches can be easily manufactured in massive "cell factories" at an industrial scale. The vaccine doesn't even need to be refrigerated during storage or transport – a massive complication for other vaccines.
 "For most vaccines, you don't need to address scalability to begin with," Gambotto said. "But when you try to develop a vaccine quickly against a pandemic that's the first requirement."
 Before starting human trials, the researchers are currently applying for drug approval from the US Food and Drug Administration.
 "Testing in patients would typically require at least a year and probably longer," Falo said. "This particular situation is different from anything we've ever seen, so we don't know how long the clinical development process will take."
 This article was originally published by Futurism. Read the original article.
 https://www.sciencealert.com/scientists-in-pittsburgh-say-they-ve-already-developed-a-covid-19-vaccine-candidate
