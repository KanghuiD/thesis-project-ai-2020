St Catherine’s Hospice warn of huge funding shortfall in wake of coronavirus pandemic |  Blog Preston
 A Preston hospice has warned it will face a significant funding shortfall in the wake of the coronavirus pandemic.
 St Catherine’s Hospice has released details of how it is adapting in the face of the disease.
 Chancellor Rishi Sunak announced a £750million funding package for the charity sector on Wednesday (April 8) but St Catherine’s, based in Lostock Hall, said it was unlikely to be enough.
 The hospice, which cares for around 250 people, says it is due to lose an estimated £1.2million in the next six months.
 A statement from the hospice said: “We are working creatively on possible new plans to boost our income later in the year, to help make up some of the shortfall caused by the closure of our shops and café, and the cancellation of our summer fundraising events. However we are also very sensitive to the fact that so many around us are suffering financial hardship.
 “The Government has promised to support the important work of hospices over the next 3 months. We have been working with our national umbrella body, Hospice UK and hope to know details of what this will mean for St Catherine’s in a couple of weeks.
 “We are most grateful for this support remembering that Rishi Sunak, said: “The truth is that we will not be able to match every pound of funding charities would have received this year.” We still expect to have a significant shortfall, despite Government income, that we will have to address.”
 The hospice says it is working to increase its bed numbers and preparing to look after patients with coronavirus who need pallative care if needed.
 St Catherine’s volunteers who are self-isolating are being supported with food deliveries.
 A spokeswoman for the hospice said: “Thank you so much to those of you who have kindly made special donations of different sorts since the emergency began. It makes such a difference and lifts our spirits.
 “We thank paid staff who have kindly agreed to be furloughed so that we can regain furlough income from the Government.
 “We also want to reassure our fabulous volunteers that we miss you enormously and hope that we can see you just as soon as it is safe for restrictions to be lifted.
 “Please take care of yourselves and your loved ones, and thank you for thinking of St Catherine’s.”
 				Sign up below to receive Blog Preston's weekly newsletter. It wings its way into inboxes every Sunday rounding up our best content from the last week and a look ahead to what's happening.
 			
 				Find news by location, select an area of your choice to be kept up to date with the latest goings on.
 				Find news by category, select an category of your choice to be kept up to date with the latest goings on.
 BlogPreston was founded by Ed Walker
 Preston based 3manfactory designed & developed the website
 Preston based Clook Internet host the website
 Blog Preston is a Community Interest Company. Company number 08814641.
 https://www.blogpreston.co.uk/2020/04/st-catherines-hospice-warn-of-huge-funding-shortfall-in-wake-of-coronavirus-pandemic/
