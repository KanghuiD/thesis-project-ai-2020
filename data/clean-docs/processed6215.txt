How to protect children from more than coronavirus | Opinion | Chicago Reader
 
 April 02, 2020
 Columns & Opinion
         
           | Opinion
 We can take action now to prevent child sexual abuse and intervene in concerning situations.
 Kelly Sikkema / Unsplash
 Julia Strehlow is the director of education, prevention, and policy for Chicago Children's Advocacy Center, one of the frontline responders to reports of child sexual abuse in the city. The Center also responds to physical abuse, witness to violence, and other serious maltreatment. ChicagoCAC is the city’s only not-for-profit organization that coordinates the efforts of child protection staff, law enforcement professionals, family advocates, medical experts, and mental health clinicians under one roof.
 Children’s Advocacy Centers across the country continue our vital operations, even as this pandemic of the novel coronavirus impacts every facet of our communities. We are increasingly concerned about children who are isolated in homes with abusive parents, caregivers, siblings, or other family friends and have no advocates or supportive adults to step in and help.
 But abuse is not inevitable. We can take action now to prevent child sexual abuse and intervene in concerning situations. Here are a few prevention tips for reducing the risk for child sexual abuse in a household during this time of shared spaces, increased anxiety due to an unknown future, and reduced options for childcare.
 Communication: The most important thing adults can do to reduce the risk of sexual abuse for children is to maintain open lines of communication and encourage children to come to them at any time with concerns about others’ behavior, feelings of discomfort in the home, or sexual abuse. Adults must believe children. Use this extra time at home to discuss your openness and model assertive communication about boundaries all the time.
 Schedule: Set a daily schedule and stick to it so that everyone knows what to expect and can anticipate what’s coming next. This consistency keeps all adults in the household accountable for their role throughout the day. It provides safety and comfort to kids who are vulnerable to abuse. And it allows anyone who might be at risk of causing harm to be aware that you are paying attention and leaving no room for inappropriate behavior.
 Household safety rules: Set new rules or reinforce existing ones for boundaries and privacy around the house, such as one person in the bathroom at a time, or expectations that everyone is keeping their hands to themselves.
 Online safety rules: Now more than ever, children are spending time online. It’s essential to have regular prevention conversations at an age-appropriate level. 
 Childcare: With schools and many daycares closed, reliable childcare is hard to come by! We recommend caution in allowing children to be cared for by just anyone, even in a pinch.
 As always, it is our responsibility as adults to prevent child sexual abuse and intervene in concerning situations.
 Visit Chicago Children’s Advocacy Center’s website for more information about prevention strategies.  v
 More Opinion »
 We speak Chicago to Chicagoans, but we couldn’t do it without your help. 
 Every dollar you give helps us continue to explore and report on the diverse happenings of our city. 
 Our reporters scour Chicago in search of what’s new, what’s now, and what’s next. 
 Stay connected to our city’s pulse by joining the Reader Revolution.
 Are you in?
 Not ready to commit? Send us what you can!
 Tags: Opinion, Chicago Children's Advocacy Center, Julia Strehlow, child abuse prevention, coronavirus, COVID-19
 https://www.chicagoreader.com/chicago/coronavirus-child-abuse-prevention/Content?oid=78959512