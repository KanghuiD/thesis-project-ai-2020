Peter Rosenberger: Coronavirus -- What caring for the sick can teach us about resentment | Fox News
       This material may not be published, broadcast, rewritten,
       All market data delayed 20 minutes.
     
 MedCV president, CEO Niels Andersen and retired navy captain Dr. Mark Goto join ‘America’s News HQ.’
 Get all the latest news on coronavirus and more delivered daily to your inbox.  Sign up here. 
 Driven by resentment, we can lash out at family, friends, medical personnel, co-workers, employers, clergy, law enforcement, politicians, God …or even at ourselves. Just like wounds to our bodies, injuries (perceived or real) to our hearts and egos can quickly grow septic with resentment.
 America is discovering nationally, thanks to the coronavirus pandemic, what so many family caregivers learned privately during dark days and even darker nights: there’s nothing quite like caring for a chronically ill/impaired/addicted loved one to expose the gunk that lurks in one’s soul.
 As we care for the sick in our country, resentment can creep closer than any mask or pair of gloves we wear. While social distancing protects against a virus, it can sometimes incubate resentment.
 REP. ANDY BIGGS: CORONAVIRUS PANDEMIC SHOULD TEACH US THESE IMPORTANT LESSONS
 In more than 30 years as a caregiver for a woman with severe medical issues (now including the COVID-19 virus), resentment and I have regretfully met on too many occasions.
 Yet, through this journey, I’ve learned principles about caring for the sick in a healthier manner. A goal I’ve set for myself as a caregiver is to one day stand at a grave. While I can’t guarantee outliving my wife and ensuring she and our sons aren’t left to deal with her massive medical challenges without me, I can, however, guarantee a better chance of doing so if I live a healthier life.
 America is discovering nationally what so many family caregivers learned privately during dark days and even darker nights: there’s nothing quite like caring for a chronically ill/impaired/addicted loved one to expose the gunk that lurks in one’s soul.
 Part of living a healthier life is avoiding carrying resentment. I don’t want to stand at a grave with clenched fists while feeling resentful at her, others who didn’t help the way I wanted, myself, or at God.
 The same principle applies when caring for a country. One doesn’t reach across the aisle with a clenched fist. Yet many find themselves daily living with clenched fists—which are only good for fighting and maybe tearing up speeches.
 Holding resentment deceives us with feelings of power, but releasing resentment makes us truly stronger. Sometimes, it starts with the simple act of opening one’s hand. The heart will follow.
 Our country is sick and likely to remain so for some time. Now in my fourth decade as a caregiver, I’ve learned that caring for someone who is sick exposes the best and the worst in each of us.
 One would think a merciless global pandemic would foster unity among leaders to face a common enemy. Yet, although we see the best of America emerging across the nation, the vitriol exposed in politics and elsewhere reveals the cancer of resentment prescribed and ingested by so many.
 The only effective antidote for the deadly virus of resentment is unclenching our fists.
 While maintaining healthy boundaries between the heart and those who either inadvertently or intentionally trample it, one can let go of resentments. In protecting ourselves from the COVID-19 virus, we social-distance without hate.
 Likewise, we can keep healthy boundaries without resentment. Letting go of grudges is not easy, but picture a pianist opening hands to play something expressive and beautiful on the piano. The ensuing music soothes and heals the pianist—as well as listeners.
 The same applies to those caring for a disabled or chronically impaired loved one. One can’t push a wheelchair—or care for a country—with clenched fists.
 Forgiveness and letting go don’t mean it’s unimportant—or the absence of consequences. It simply means we’re willing to take our hands off someone else’s throat.
 Sometimes, the person we harbor the most resentment towards is our self. We can cruelly demean our own hearts for allowing us to either get into the circumstances we find ourselves or for staying in the situation.
 Regardless of the targets of our resentment, it only serves to gnaw at our souls. We serve ourselves (and others) better when we live calmer, healthier; free of resentments and bitterness.
 In this season of Easter, we remember the one who opened his own hands to extend the ultimate forgiveness. Doing so made it possible for us to be reconciled to Him —and each other.
 As written in the Gospel of Matthew 6:12 -- And forgive us our debts, as we also have forgiven our debtors. 
 https://www.foxnews.com/opinion/coronavirus-sick-resentment-peter-rosenberger
