Photos: See How Cities Turn Parks And Parking Lots Into Coronavirus Hospitals And Shelters
 Sign in to your Forbes account or
 register
 If this is your first time registering, please check your inbox for more information about the benefits of your Forbes account and what you can do next!
 Topline: As the worldwide coronavirus count surpassed 850,000 confirmed cases and resulted in nearly 50,000 deaths as of Tuesday, cities are scrambling to accommodate patients and the dead. Here’s what people living in coronavirus hotspots see in their day-to-day lives.
 On Monday, an emergency field hospital was being constructed in Central Park near Mount Sinai ... [+] Hospital in New York City.
 A closer look at the Central Park field hospital, set up by volunteers from the International ... [+] Christian relief organization Samaritans Purse.
 The U.S. Navy Ship USNS Comfort arrived in New York Harbor on Monday, bringing 1,000 hospital beds ... [+] to help relieve the city's overwhelmed hospital system.
 People are shown in social-distancing boxes at a temporary homeless shelter set up in an outdoor ... [+] parking lot at Cashman Center on Monday in Las Vegas, Nevada.
 Rooms for patients are set up at Jacob Javits Convention Center, which is arranged as a temporary ... [+] hospital amid the coronavirus outbreak, on Monday in New York City.
 The USNS Mercy Navy hospital ship is seen Saturday at dawn in the Port of Los Angeles after it ... [+] arrived to assist with the coronavirus pandemic.
 Workers build a temporary 200-bed field hospital on a soccer field for coronavirus patients in ... [+] Shoreline, Washington, about 12 miles north of Seattle.
 Healthcare professionals screen people for the coronavirus at a testing site organized by the ... [+] Maryland National Guard in a parking lot at FedEx Field on Monday in Landover, Maryland.
 A coronavirus testing tent was set up outside of Norwegian American Hospital in Humboldt Park on ... [+] Chicago's west side last week.
 Full coverage and live updates on the Coronavirus
 I am a Texas native covering breaking news out of New York City. Previously, I was a Forbes intern in London. I am an alum of City, University of London and Texas State…
 I am a Texas native covering breaking news out of New York City. Previously, I was a Forbes intern in London. I am an alum of City, University of London and Texas State
 I am a Texas native covering breaking news out of New York City. Previously, I was a Forbes intern in London. I am an alum of City, University of London and Texas State University.
 https://www.forbes.com/sites/carlieporterfield/2020/03/31/photos-see-how-cities-turn-parks-and-parking-lots-into-coronavirus-hospitals-and-shelters/
