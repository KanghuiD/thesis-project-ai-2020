The most sadistic figure in the Trump administration has the coronavirus. Is it wrong to cheer? | National Observer
 When word arrived that Stephen Miller, senior adviser to U.S. President Donald Trump tested positive for the coronavirus, many felt the pull of an inner moral struggle.
 Is it wrong to cheer such terrible luck for the most sadistic figure in the Trump administration? Or for Trump himself, the others in his administration, and the GOP senators who have so debased their office in his service?
 There's a strong sentiment abroad about not wishing ill on anyone, no matter the political differences. And I'm here to tell you that it's ok, just a little bit, to savour this moment.
 It's not like anyone got hit by a bus, or had a heart attack out of the blue.
 Trump and his enablers all knew how to prevent this disease. Entirely for political advantage, they refused to take the simplest precautions. 
 Having written this script, they can't complain when the audience laughs at the punch line. 
 Trump and this entire administration encouraged Americans to ignore and distrust science. They relentlessly amplified his deadly message through Fox and other media, while attacking anyone dismayed by their irresponsibility. They recklessly endangered White House employees like housekeeping, security, catering staff, their own families, the thousands attending Trump's endless rallies, or even the United States Joint Chiefs of Staff. 
 Miller has been at the forefront of America's immigration policy, where covid was used as a pretext to end refugee asylum. To say nothing of forced sterilization, kids in cages, and even, as we now know, tearing nursing babies from their mothers arms.
 In their calamitous mismanagement of the Covid response, the Trump administration abandoned millions, and even in some cases actively intervened to confiscate and re-direct desperately needed PPE and equipment. Their list of negligence is far too long to catalogue here. 
 Throughout it all, Trump demanded that governors genuflect before him, and attacked reporters seeking accountability, or simply the truth. 
 As a direct result of the president's spite, disinformation and neglect, at least tens of thousands of Americans needlessly lost their lives, while millions more fell gravely ill. Millions have lost jobs and health care. Children struggling to learn are falling behind. Some are going hungry or losing their homes as financial and health worries engulf families and the elderly. Thousands die every week during a lethal pandemic that has no end in sight.
 And all of this tragic loss—the death, suffering, and despair—was inflicted in pursuit of the most cravenly corrupt and selfish reasons.
 Every single one of these people knew what they were doing. Every one of them could have refused to be part of it. Not one can say they made an honest mistake. 
 In a perfect world, the powerful face justice for such heartless abuse. But the world is not perfect. No court will render a verdict on this cruelty, and yet these people have passed sentence on themselves. 
 This is a moment of enormous historic importance that none of us will forget. 
 It's ok just this once, to agree with Fate. 
 Just to be clear, separating children from their parents and then incarcerating them is TORTURE.#TheHague #ICJ #VOTE #BidenHarris2020
 Thank you!!! Totally agree.
 Thank you. I do a little blog just for fun, and made this the heart of my shortest post ever.
 http://brander.ca/c19#niceday
 https://www.nationalobserver.com/2020/10/07/opinion/most-sadistic-figure-trump-administration-has-coronavirus-it-wrong-cheer
