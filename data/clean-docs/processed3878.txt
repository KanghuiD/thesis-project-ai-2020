Gov.ie - COVID-19 (Coronavirus): Health advice
             This is a prototype - your
             feedback will help us to improve it.
         
     Until Tuesday 5 May, everyone should stay at home wherever possible.
     It can take up to 14 days for symptoms of COVID-19 (Coronavirus) to show. They can be similar to the symptoms of cold
  and flu.
     Common symptoms include:
     If you're confused about the differences between symptoms of COVID-19 (Coronavirus), the cold and flu, the HSE has more information.
     If you develop symptoms of COVID-19 (Coronavirus),
  you will need to self-isolate
  and phone your GP. Do not go to a GP surgery, pharmacy or hospital.
     The GP will assess you over the phone. If they think you need to be tested for COVID-19 (Coronavirus), they will arrange a test.
     To protect yourself:
     The most important action we can take to protect ourselves from COVID-19 (Coronavirus) is regular hand-washing and good respiratory hygiene.
     Read a step-by-step guide from the HSE on how to clean your hands.
     The government is urging people over the age of 70 or who have underlying medical conditions to cocoon.
     Cocooning means protecting people:
     The symptoms of COVID-19 (Coronavirus) can be harder to notice for people who have underlying health issues.
     If you're in one of these groups, you must take extra care to reduce interaction with other people. Don't go outside your home and garden.
     Learn more about cocooning.
     Everyone in Ireland has been asked to stay at home.
     But you need to restrict your movements
  further if you:
     You need to restrict your movements for at least 14 days. But if the person you live with has had a test and it's negative, you don't need to wait 14 days. You should still follow the advice for everyone - stay at home as much as possible.
     Visit www.hse.ie
  if you think you have been:
     You will be put in contact with your local Department of Public Health staff who will give you information and advice.
     If you develop symptoms avoid contact with people by self-isolating
  and phone your GP or emergency department.
     You can access the Interpreting Service for Deaf People here.
     COVID-19 (Coronavirus) is spread in sneeze or cough droplets. The advice to stay at home means you have less chance of catching it or passing it on to others if you do happen to have COVID-19 (Coronavirus).
     You could get the virus if you:
     As it's a new illness, we do not know how easily the virus spreads from person to person or how long the virus stays on surfaces. Spread is most likely from those who have symptoms.
     The virus may survive for up to 2 days if someone who has it coughs or sneezes on a surface. Simple household disinfectants can kill the virus on surfaces. Clean the surface first and then use a disinfectant.
     Close contact can mean:
     Close contact does not include someone you passed on the street or in a shop. The risk of contact in that instance is very low.
     More information on how COVID-19 (Coronavirus) spreads
  is available from the HSE.
     Infectious disease outbreaks like COVID-19 (Coronavirus) can be worrying. This can affect your mental health. However, there are many things you can do to mind your mental health during times like this.
 You can see a lot more supports and help here.
                             
                 Do not include any personal details in the box below.
                 The information you submit will be analysed to improve the
                 site and will not be responded to individually.
                         
 Max: [[ feedback_widget.data.text.length ]]/[[ feedback_widget.cfg.text.maxlength ]] characters
 https://www.gov.ie/en/publication/472f64-covid-19-coronavirus-guidance-and-advice/
