EC Report Finds Western Balkans Targeted by Coronavirus Disinformation Campaigns - Exit - Explaining Albania
 During the outbreak of the coronavirus, the Western Balkans have been a target of broad disinformation campaigns on the epidemic. 
 A European External Action Service report found that several false narratives have accompanied the outbreak, ranging from the idea that coronavirus is a biological weapon to claims that the virus is, in fact, a hoax, and does not really exist.
 A prominent narrative finding space in Western Balkans media has been the one alleging that “the virus was developed in a lab to target China and Iran.”
 The EEAS report warns that disinformation may be used strategically in the region during campaigns for the upcoming elections in Serbia and North Macedonia, maybe weaponized against migrants, and used by foreign sources to manipulate local information spaces.
 Fjalë kyçe: 
 Hyni ose Rregjistroni për të komentuar
 Duhet të keni hyrë në llogari që të komentoni.
 https://exit.al/en/2020/03/21/ec-report-finds-western-balkans-targeted-by-coronavirus-disinformation-campaigns/
