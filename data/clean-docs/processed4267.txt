Coronavirus: Morrisons store staff get bonus for coming into work - BBC News
 Morrisons is the latest supermarket to reward workers during the coronavirus crisis, with a threefold increase in bonus for the next 12 months.
 Marks & Spencer had already promised staff an extra 15% pay, while Aldi, Sainsbury's and Tesco have pledged 10%. 
 Asda meanwhile has offered an extra week's pay in June, working out as a 25% bonus for the month.
 Bosses say they are rewarding "dedication and commitment" and "outstanding work".
 "It's been an extraordinary couple of weeks and despite the enormous challenges, it's been incredible to see how colleagues have stepped up and responded, doing an outstanding job during this uncertain and difficult time," said Tesco UK boss Jason Tarry.
 Sainsbury's retail and operations director Simon Roberts said: "Our store, depot and customer service colleagues are working around the clock at the moment. 
 "Their dedication and commitment has been extraordinary."
 It varies depending on the supermarket chain, but all appear to be recognising the extra efforts that workers are being asked to make.
 Morrisons: The chain says all frontline staff will receive a 6% bonus on their earnings for the next 12 months, after staff complained they were initially offered just 0.75%. The changes represent a bonus of £1,050 for a full-time worker compared to the £351 that would have been paid last year. 
 Aldi: "Aldi has announced enhanced pay for its amazing colleagues, who have been working tirelessly throughout this extremely busy time. Store and distribution colleagues will receive a 10% bonus on hours worked, effective from 9 March 2020."
 Sainsbury's: "As a small thank you for all their efforts, we will be giving colleagues a payment of 10% of the hours they have worked since March 8. The 10% additional pay will be awarded in May to hourly paid colleagues in Sainsbury's and Argos retail, Sainsbury's transport and warehouse teams, Argos distribution and all Retail contact centres."
 Tesco: "Tesco will be giving a 10% bonus on the hourly rate for hours worked to colleagues across its stores, distribution centres and customer engagement centres. all permanent colleagues who are currently in work will receive the increased hourly pay rate till Friday 1 May, when we will review the situation."
 Asda: "There will be an extra week's pay in June to our colleagues in recognition of their extra efforts to protect the health of their colleagues who needed to step away during this time."
 Lidl: "Our teams are playing a crucial role in serving communities across the country and their incredible commitment and hard work is hugely appreciated. As a token of our gratitude, Lidl colleagues across the business are being given a £150 voucher each."
 Co-op: "It's important to reward our colleagues for working through these difficult times. That's why we've given them a bonus, money to spend and extra time off."
 Waitrose: "We are looking at a number of options to ensure we find a suitable way to recognise our partners who have shown incredible professionalism and dedication." 
 M&S: "Our frontline colleagues across stores and supply chain who continue to work will receive an additional 15% pay reward in recognition of the work they are doing to support their teams and the national effort to help customers access the products they need during these unprecedented times."
 The supermarkets have mostly announced plans to support staff who contract the coronavirus.
 "We've committed to supporting our colleagues that have been identified by the government as needing to self-isolate for 12 weeks, ensuring these colleagues receive full pay for their isolation period," said Asda boss Roger Burnley.
 The supermarket is also offering 12 weeks fully paid leave to those over the age of 70, or who are pregnant and classed as vulnerable, as well as the carers of extremely vulnerable people.
 M&S said: "Any colleague who needs to self-isolate for seven to 14 days can do so on full pay".
 On top of that it said: "frontline colleagues who have caring commitments or who are feeling more vulnerable are able to step away at this time. Any colleague who is furloughed for the coming weeks will do so on full pay."
 Some workers are in line for thank-you bonuses for their efforts in the current crisis.
 Compass Group, for instance, is awarding bonuses of between 8%-17% to its 7,800 employees supporting the NHS as ward hosts, hospital porters or cleaners.
 "Compass employees are playing an invaluable role in the country's battle against coronavirus, ensuring NHS patients and staff are fed and their wards are kept clean, and supporting key workers in the education, defence and infrastructure sectors," said managing director Robin Mills. 
 "I am enormously proud of them and want to thank them for the incredible dedication they are showing in unprecedented circumstances."
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/business-52119560
