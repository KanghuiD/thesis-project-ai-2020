Coronavirus blog: ABC7 Meteorologist Sandhya Patel opens up about navigating life during COVID-19 crisis
 I thought life was already hectic with a full-time career and three kids but I had no idea it was about to get crazier when the novel coronavirus pandemic hit.
 I felt a strong urge to start this blog because it has been therapeutic for me to put my feelings on paper. I wanted to give other people the opportunity to do the same in the hopes that it would help them.
 I would love to hear how you're feeling and coping so please feel free to share.
 Blog Post 1: Coping with COVID-19 crisisBlog Post 2: Our new realityBlog Post 3: Adjusting to new lifestyleBlog Post 4: Staying Optimistic
 Blog Post 1: Coping with COVID-19 crisis
 Blog Post 2: Our new reality
 Blog Post 3: Adjusting to new lifestyle
 Blog Post 4: Staying Optimistic
 Go here for the latest news, information and videos about the coronavirus.
 RELATED STORIES & VIDEOS:
 Live updates about coronavirus outbreak in US, around the world
 WATCH: ABC7's interactive town hall 'Race and Coronavirus: A Bay Area Conversation'
 Everything you need to know about the Bay Area's shelter-in-place order
 Stimulus calculator: How much money should you expect from coronavirus relief bill 
 What Bay Area tenants need to know about rent payments, eviction amid COVID-19 outbreak
 Here's how shelter in place, stay at home orders can slow spread of COVID-19
 Coronavirus Timeline: Tracking major moments of COVID-19 pandemic in San Francisco Bay Area/
 Coronavirus and the new normal of living in the San Francisco-Bay Area -- COVID-19 Diaries
 Happy hour goes virtual as people try to be sociable while social distancing during COVID-19 crisis
 Coronavirus Outbreak: Here's why you should practice 'social distancing'
 DRONEVIEW7: What the Bay Area looks like during the coronavirus shelter-in-place
 ABC7's drive around San Francisco shows empty streets, businesses shuttered
 Symptoms, prevention, and how to prepare for a COVID-19 outbreak in the US 
 List of stores, companies closing due to coronavirus pandemic
 Here's how you can help during COVID-19 pandemic
 How to maintain learning during school closures 
 No masks but here are 100+ products that may help protect you against novel coronavirus germs 
 Here's a look at some of history's worst pandemics that have killed millions
  Asian community fighting racism, xenophobia, bigotry as world fights COVID-19 
 https://www.msn.com/en-us/news/technology/coronavirus-blog-abc7-meteorologist-sandhya-patel-opens-up-about-navigating-life-during-covid-19-crisis/ar-BB125ZeU?ocid=hplocalnews
