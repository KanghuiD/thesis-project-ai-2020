Some governments are using coronavirus to restrict women's rights | Women's Rights | Al Jazeera
 Classing abortions as 'non-essential' is cruel and an assault on the rights of women to bodily autonomy.
 Women are prominent on the front lines of the world's response to the coronavirus pandemic.
 Globally, most of our health workers are women. They also do most of the world's unpaid care work - even in "normal times" - taking care of relatives and helping them recuperate both from extraordinary illnesses and everyday exhaustion.
 And yet, the rights of these women are coming under historic attacks even now.
 Back in early March, a potentially historic bill to liberalise abortion in Argentina was an early casualty; its review has been indefinitely postponed along with many other democratic debates.
 In the US, conservative states from Texas to Indiana are now banning most abortions during the pandemic. By classifying them as "non-essential", they are arguing that abortions can be delayed so that all doctors focus on COVID-19 first.
 Across the Atlantic, in Poland, a bill to further restrict abortion has been revived and will be heard in Parliament next week. When this first happened, in 2016, it was met with mass protests - which are currently prohibited under coronavirus emergency measures.
 Are governments and anti-abortion campaign groups taking advantage of the crisis to further restrict women's rights? 
 It would not be the first time. Around the world, organised ultra-conservative movements are looking for ways to use this moment to achieve what they always wanted; fewer rights for women over their bodies.
 Anti-abortion activists in Slovakia, Italy and the UK are also campaigning for abortions to be suspended during the pandemic, arguing that all resources must be focused on the coronavirus right now. They do not want women to have these rights after the crisis, either.
 This pandemic is also exacerbating and shining new light on the astonishing amount of red tape that has long limited women's access to abortion in places where it has been legal for generations. 
 In Italy, doctors can refuse to perform abortions (and up to 90 percent do, in some areas). Medical terminations (consisting of two pills, taken across several days), are only available during the first seven weeks of pregnancy, rather than nine as in many other European countries. And these pills must be taken in hospitals, unlike in other countries, where they are also available at clinics. 
 These details are crucial. Abortions are by definition time-sensitive procedures. Even before the coronavirus, women in Italy struggled to access them.
 Now hospitals are overwhelmed by the coronavirus and this access is increasingly impossible. As a result, women are being forced to endure unwanted pregnancies for longer and to have surgeries they do not want as medical abortions have been largely suspended. 
 In other countries, restrictive red tape includes mandatory counselling, waiting periods or requirements that two doctors sign off on an abortion.
 Such rules vary across borders but their effect is the same; making difficult experiences for women even harder, even in "normal times", and exacerbating these challenges today. 
 These restrictions have other things in common, too. Neil Datta at the European Parliamentary Forum on Sexual and Reproductive Rights told me they stem from compromises made when abortion was first legalised, which happened in the 1970s in Italy, for example. At that time, some doctors were still "diagnosing" women with hysteria.
 In other words, there is nothing enlightened about this red tape. And what ultimately lies beneath these restrictions is the toxic, patriarchal idea that women cannot be trusted to control their own bodies and make their own choices.
 Today, women's reproductive rights are being sidelined - again. For its part, the World Health Organization (WHO) has issued guidelines about domestic violence, contraception, childbirth and breastfeeding amid COVID-19.
 But so far, it has been noticeably silent on safe abortion during the pandemic.
 Thankfully, this is not the full picture. Big changes are also happening in response to the public health crisis and its fallout in all aspects of our lives.
 Some US cities have suspended evictions of renting tenants, for example. In Iran, thousands have been released from prison. Many things that would have seemed impossible a year ago, do not any more. 
 And we are seeing some evidence of this for women's right to choose, too. England and Wales, for instance, have issued new rules to enable women to take medical abortion pills at home and via telemedicine appointments. Ireland and France have done similar. Germany has at least made its mandatory counselling available online and by phone. 
 These practical moves are victories for sensibility amid crisis. They uphold rights and public health. If women do not need to travel to multiple appointments, this can help limit the spread of coronavirus and get us out of this emergency faster. 
 Indeed, during crises change can happen quickly. Archaic red tape can be cut. Toxic distrust of women could give way to a new common sense that prioritises rights and health over politics. And those who were afraid of women's autonomy might not find it so scary now that they have witnessed something a lot more frightening - a historic pandemic threatening lives, health systems and democracies worldwide.
 The views expressed in this article are the author's own and do not necessarily reflect Al Jazeera's editorial stance.
 Claire Provost is Global Investigations Editor at openDemocracy.
 	 
 https://www.aljazeera.com/indepth/opinion/governments-coronavirus-restrict-women-rights-200412095859321.html
