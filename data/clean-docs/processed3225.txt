Coronavirus - MarketWatch
             The latest news on COVID-19, the coronavirus disease that emerged in 2019.
             
         
             A series about lives lost during the COVID-19 pandemic
 Here’s a rundown of what being it’s like being on one with COVID-19—and the aftermath.
 Europe is taking more cautious steps toward reopening their battered and shuttered economies, as data show the growth in coronavirus cases has ebbed.
 New York Gov. Andrew Cuomo spoke about the dark dreams that haunted him during the early stages of COVID-19 in New York, the epicenter of the virus in the U.S.
 Low-paid South Asian laborers in the city-state have been the worst affected by COVID-19 and have the most trouble accessing health care.
 A new poll finds many people fear that they can’t afford COVID-19 medical care
 While the coronavirus pandemic has caused real-estate transactions to slow significantly, the jury is out on whether home prices will fall as a result.
 ‘You can’t ask the people of this state or this country to choose between lives lost and dollars gained,’ New York Gov. Andrew Cuomo said.
 ‘We will have coronavirus in the fall. I am convinced of that,’ Anthony Fauci, director of the National Institute of Allergy and Infectious Diseases, said Wednesday.
 The collapse in world crude oil prices since February is taking a toll on U.S. inflation expectations which are now at their lowest levels since the 2007-2009 recession but inflation may pick up as crude prices recover, cr...
 The Small Business Administration will allow some Indian casinos to apply for the Payroll Protection Program.
 President Donald Trump on Tuesday said he would sign an executive order on food supply, as the U.S. was working with Tyson Foods during the coronavirus pandemic. 
 Even industry defenders like Massachusetts Gov. Charlie Baker are horrified.
 Hannity lawyer Charles Harder sent a letter singling out several stories published in the Times, but the Times wasn’t having it.
 Berkshire Hathaway chairman is about to tell us what he really thinks about stocks, investing and the COVID-19 pandemic, writes Lawrence Cunningham.
 Protect relief checks from debt collectors and allow for immediate deposit, writes Miami Mayor Francis X. Suarez.
 Unconditional cash gives people greater control over their finances and their lives, writes Stockton, Calif. Mayor Michael D. Tubbs
 A new loan program for small businesses hurt by the coronavirus crisis is especially helping parts of the country not suffering as much from the pandemic, according to a National Bureau of Economic Research paper released ...
 The problem of both Social Security and Medicare is inherent in their structure
 Investors and consumers evidently have more faith the U.S. will rebound quickly from the coronavirus pandemic than the experts on the economy. 
 How’s the coronavirus pandemic affecting the way Americans feel about their retirement plans?
 Do Not Sell My Personal Information.
           Historical and current end-of-day data provided by FACTSET.
           All quotes are in local exchange time.
           Real-time last sale data for U.S. stock quotes reflect trades reported through Nasdaq only.
           Intraday data delayed at least 15 minutes or per exchange requirements.
 https://www.marketwatch.com/economy-politics/coronavirus
