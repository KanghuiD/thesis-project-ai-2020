If coronavirus costs Trump the election, he’ll have nobody to blame but himself (opinion) - silive.com
 NOW he wears a mask. (AP Photo/Alex Brandon)AP
 STATEN ISLAND, N.Y. – We were told on Saturday that President Donald Trump is “doing very well” in his battle with coronavirus.
 That’s good news. Let’s hope he makes a full recovery.
 But now the politics of this whole mess moves center stage. And that is nothing but bad news for Trump.
 During a briefing at midday on Saturday, Dr. Sean Conley, the president’s physician, said that Trump was “72 hours into diagnosis now.”
 That would have put Trump’s positive virus diagnosis at around midday on Wednesday. Which would have been a major shock because the rest of us didn’t learn about Trump’s condition until 1 a.m. on Friday morning.
 In those intervening days, Trump met with reporters at the White House; traveled to Minnesota for a fundraiser and campaign rally, and held a fundraiser at his golf course in Bedminster, N.J. All without a lot of mask-wearing going on.
 Conley later said that he meant to say that we’re into “day three” of diagnosis: Thursday night, Friday, Saturday.
 Glad he’s not my doctor. Take one pill twice a day. No, take two pills once a day. No, wait…
 Another bombshell came when a source told White House reporters that Trump’s vital signs over the last 24 hours had been “very concerning.”
 “We are still not on a clear path to full recovery,” the source said, contradicting Conley’s rosy prognosis.
 It was later reported that Trump chief of staff Mark Meadows was the source.
 It’s impossible to put faith in any of it. It’s a full-fledged crisis. What if there was an actual national emergency right now? Who’s steering the ship?
 Trump has marched to his own drummer on COVID-19 since the beginning. He’s questioned or dismissed the counsel of medical experts, most notably Dr. Anthony Fauci. He’s rejected minimal level mask-wearing that even the most pandemic-weary among us accept.
 And now he’s laid up in Walter Reed hospital with coronavirus. What more needs to be said?
 A lot actually. Because it’s not just Trump and First Lady Melania Trump who are infected. A host of government and campaign officials who attended a recent Rose Garden event with Trump and Supreme Court pick Amy Coney Barrett also have the virus.
 The ceremony might go down in history as the super-spreader event that cost Trump the presidency.
 Vice President Mike Pence was also at that Rose Garden event, even though he has so far tested negative for the virus. Incredibly, however, Pence is scheduled to attend a MAGA rally in Arizona on Thursday.
 Does Pence know that a virus is shooting through the uppermost echelons of the White House? Pence should be sealed inside a plastic bubble in the veep’s residence in the Naval Observatory. The very continuity of our government is at stake.
 Meanwhile, other serious questions sprang out of the Conley press conference as well.
 Conley refused to say outright whether the president had been given supplemental oxygen during all of this. It was then reported that Trump had received oxygen on Friday at the White House before traveling to Walter Reed.
 Conley said that Trump had been fever-free for 24 hours but wouldn’t say how high the president’s fever had gone. He wouldn’t say when Trump had had his last negative COVID test, or how often Trump had been tested in recent days.
 Medical patients deserve some measure of privacy, but this is the president of the United States we’re talking about. We need absolute clarity about his condition. The U.S. government is literally teetering.
 I’ve lost track of how many times Trump has been counted out politically, beginning even before election night in 2016. But if he manages to win re-election after this crisis, he will truly be Teflon Donald.
 Of course, he has to get healthy first. That’s the most important thing.
 Note to readers: if you purchase something through one of our affiliate links we may earn a commission.
 Community Rules  apply to all content you upload or otherwise submit to this site.
 Ad Choices
 https://www.silive.com/news/2020/10/if-coronavirus-costs-trump-the-election-hell-have-nobody-to-blame-but-himself-opinion.html
