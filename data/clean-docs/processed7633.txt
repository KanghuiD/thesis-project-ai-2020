Andrew McCarthy: On coronavirus restrictions, burden of proof is on government to show justification | Fox News
       This material may not be published, broadcast, rewritten,
       All market data delayed 20 minutes.
     
 'Government is assuming their regulations have priority and we have adjust our rights,' says Andy McCarthy, former Assistant U.S. Attorney, discussing if 'contacting tracing' is legal
 Get all the latest news on coronavirus and more delivered daily to your inbox.  Sign up here. 
 There is never a good time for a pandemic, but an election year in a deeply divided country is an especially bad time. Everything is politicized. I would add that even science is politicized, but that would suggest that this was something new.
 Sadly, we’re inured to the politicization of science, thanks to climate change and to the centrality of government funding to academic endeavors. Research resources are diverted toward our political conflicts, rather than being freely allocated where they could better advance the search for truth.
 The politicization of science has ingrained in our political life something about which we ought to be highly skeptical: The argument from authority. It is doing extraordinary damage to the republic, through governmental responses — federal, state and municipal — to the coronavirus.
 BRANDON JUDD: AS CORONAVIRUS SENDS UNEMPLOYMENT SKYROCKETING, TRUMP’S IMMIGRATION RESTRICTIONS CLEARLY NEEDED
 And it will keep doing damage unless and until we restore the burden of proof.
 There is no doubt that governments have a compelling interest in public safety, which includes preventing the spread of a potentially deadly infectious disease.
 It is nevertheless the foundational conceit of the American republic that governments are created to secure the fundamental rights of a nation’s citizens — our rights to life, liberty and the pursuit of happiness. Moreover, the legitimacy of government is dependent on the consent of the governed.
 In the United States, authority is subordinate to liberty. Government is the servant, not the master.
 https://www.foxnews.com/opinion/andrew-mccarthy-on-coronavirus-restrictions-burden-of-proof-is-on-government-to-show-justification
