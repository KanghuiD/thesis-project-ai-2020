        Coronavirus: Don't stockpile, batch cook instead! - The Guide Liverpool
     
                     23/03/2020                
 We have all seen the stock piling nightmares across the country and we have all seen the empty supermarket shelves here in Liverpool.
 Your thoughts might turn to a supermarket run and buying up frozen food, ready meals and dry goods, such as rice and pasta.
 But there’s something else you should consider – batch cooking. Here’s why…
 A ready meal can be quite costly. Making your own food and portioning it up is way, way cheaper! Having a freezer full of batch-cooked food also means you won’t be tempted to call for a takeaway (well, maybe one) during your time at home, which saves vital cash as well.
 If you’re at home trying to ward off a virus, you need vitamins and a batch-cooked chilli, for example, can be packed with veg.
 The same goes for home-made soup – you can whizz up a lot of seasonal root vegetables into soup, adding ingredients like immuno-friendly ginger and garlic, too.
 Grate and freeze garlic and ginger into ice cube trays – then they’re ready to add to something else if you need them, too. If you’re making some sauces (a pasta sauce can be whipped up and frozen easily), then put them in freezer bags, seal and lie flat in the freezer, so you can get more into the space.
 Tomatoes, peppers, onions and courgettes make a great pasta sauce – add some chilli if you like a kick.
 Batch cooking is an activity you can get the kids involved with – it’s a win-win as you choose the recipe and then portion it up together. Get them to write the labels with a marker, so they’re involved that way, too.
 The impact of coronavirus on the environment remains to be seen. Indeed, the amount of disposable wipes and bottles of hand sanitiser are yet to be counted. And stockpiling adds to that, too. Every ready meal in non-recyclable packaging or wrapped in plastic is a no-no, if you can help it.
 By contrast, batch cooking reuses containers you already have and can utilise fresh fruit and veg from the greengrocer that isn’t wrapped in plastic.
 Staying at home for two weeks with little to no exercise and fresh air means you will be feeling confined and helpless. Relying on meals from the supermarket could add to that, while batch cooking means you’re in control of what you eat. And if you do fall ill, cooking might be the last thing on your mind.
 You can make things as spicy as you like, add in the veg you really like, and at least your food will be something you are in charge of during this difficult time.
 Sign up with us to receive the latest news, straight to your inbox!
 Check your inbox or spam folder to confirm your subscription.
         
 2019 The Guide Liverpool
 Useful Links
 Liverpool News
 Wirral News
 Southport News
 info@theguideliverpool.com
 Legal
 Terms & Conditions
 https://theguideliverpool.com/coronavirus-dont-stockpile-batch-cook-instead/
