Coronavirus in Illinois updates: Here’s what’s happening Sunday - Chicago Tribune
 State health officials on Saturday reported an additional 1,293 known cases of COVID-19 in Illinois and 81 more deaths.
 That brings the statewide totals in Illinois to 19,180 known cases and 677 deaths, according to state figures.
 Around the globe on Sunday, Pope Francis delivered an Easter message of solidarity, while British Prime Minister Boris Johnson was released from a London hospital where he was treated in the intensive care unit for coronavirus. Meanwhile, new hot spots are emerging in Japan, Turkey and Britain.
 As the pandemic continues, the Tribune is keeping a running list of Chicago-area closings and cancellations, asking experts to answer your questions about COVID-19, tracking cases across the state and memorializing those who have died in the Chicago area.
 Here’s what’s happening on Sunday with COVID-19 in the Chicago area and Illinois:
 The DuPage County Health Department announced seven COVID-19 related deaths Saturday. Five deaths were associated with long-term care facilities, according to a news release from the health department.
 There are currently 1,057 associated cases of the disease and 46 deaths in DuPage County, according to officials. Read more here. –Jessica Villagomez
 Mayor Lori Lightfoot ordered Chicago’s building department to stop any demolitions expected to take place this week after Saturday’s demolition at the Crawford Coal Plant sent a cloud of dust into the Little Village neighborhood. 
 Starting Sunday, the city was expected to hand out masks to residents living near the plant located at 3501 S. Pulaski Road. The site had been shuttered since 2012 after a contentious push by activists that argued the plant symbolized environmental racism. It’s located near the Latino neighborhoods of Little Village and Pilsen.
 The demolition happened as officials tried to contain COVID-19, a respiratory illness. Ald. Michael Rodriguez’s whose 22nd Ward includes Little Village said at the Sunday news conference that the demolition only “exasperated” the fear and anxiety residents were already feeling because of the coronavirus pandemic. 
 In March, the Hilco Redevelopment Partners secured permits to demolish a smokestack on the property, city officials said. A video posted by the Chicago Fire Department showed a tower falling into the ground, releasing a heavy cloud of particles into the air that drifted into residential areas of Little Village. 
 “This is absolutely and utterly unacceptable,” Lightfoot said at a Sunday news conference. “It’s unsafe, it’s unsanitary. I would not tolerate it in my neighborhood and we won’t tolerate it here either.”
  The developers won’t be allowed to do any additional work until the city conducts an investigation into the demolition, Lightfoot said. Officials have collected samples of the dust and are testing it to see what particles were released. The city will also monitor the air quality on the site and in the neighboring areas, Lightfoot said. 
 Lightfoot also called on the company to start cleaning up parts of the Little Village neighborhood covered in the dust. Read more here. –Elvia Malagon
 On CBS’s “Face the Nation” Sunday, Mayor Lori Lightfoot said Chicago’s economy can’t reopen until there’s more widespread testing and decried the disproportionate number of African American deaths from coronavirus, saying the disease is “devastating our community.”
 Nearly 70% of the city’s COVID-19 deaths have been African Americans, who make up only about 30% of Chicago’s total population, according to a Tribune examination of data from the Cook County medical examiner’s office and the Chicago Department of Public Health published earlier this week.
 Lightfoot was asked about it on the nationally televised talk program and said, “This is an issue that’s not unique to Chicago, unfortunately.”African Americans across the country are being hit harder than others partly because of “the underlying conditions that people of color, particularly black folks, suffer from,” including diabetes, respiratory illnesses, and heart disease, Lightfoot said. Read more here. -Gregory Pratt
 The tweet said the testing site on Forest Preserve Drive, manned by Illinois National Guard soldiers in a former emissions testing facility on the Northwest Side, had been “experiencing low traffic” as of about 9 a.m. Read more here. –Katherine Rosenberg-Douglas
 With two COVID-19 cases already confirmed at Hesed House, the state of Illinois is moving the approximately 200 residents of the Aurora homeless shelter into a suburban hotel on Monday in an attempt to contain the outbreak.
 The location of the hotel that will take in the residents was not released as of Saturday night.
 According to Hesed House Executive Director Ryan Dowd, who was informed of the move late Friday night, this second largest homeless shelter in the state will be the first to make such a move, and will likely “serve as a model” as other shelters in Illinois make similar transfers.
 The move will not include families in the PADS Emergency Shelter at Hesed House, as they have already been relocated to a hotel.
 As of Saturday evening, Hesed House has two confirmed cases of coronavirus, with several other residents quarantined and awaiting results, said Dowd, who credits Kane County Sheriff Ron Hain as the person who doggedly worked to make the transfer of the residents to the hotel on Monday possible. Read more here. –Denise Crosby
 Porter County has reported its second COVID-19-related death in as many days, according to the Indiana State Department of Health.The death was part of the the ISDH daily recap on its website.
 In Lake County, where 25 people have died from the virus, there were no new deaths, according to the state. The first Lake County death was reported March 28.
 However, the county is reporting 68 new positive cases, bringing the county total to 744, second highest in Indiana.
 According to the state, Porter County has had 118 positive COVID-19 cases. The first Porter County case was reported March 24.Marion County, which includes Indianapolis, is at 2,887 cases, the ISDH report shows. Of the state’s 343 deaths, 123 have come from Marion County.According to the state, more than 42,000 people have been tested and 7,928 have come back positive. Read more here. –Post-Tribune, Associated Press
 Here’s a recap of coronavirus updates from Saturday:
 Here’s a recap of coronavirus updates from Friday:
 Here’s a recap of coronavirus updates from Thursday:
 Here’s a recap of coronavirus updates from Wednesday:
 Here’s a recap of coronavirus updates from Tuesday:
 Here’s a recap of coronavirus updates from Monday:
 https://www.chicagotribune.com/coronavirus/ct-coronavirus-pandemic-chicago-illinois-news-20200411-5lr3wkidvvgd5kcqhk2g66fnay-story.html
