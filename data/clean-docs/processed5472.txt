Opinion | Fox News
       This material may not be published, broadcast, rewritten,
       All market data delayed 20 minutes.
     
 New court documents finally handed over to Flynn’s lawyer contain exculpatory evidence that has been long sought, yet concealed until now.
 Our initiative, Resolve to Save Lives, released a brief Monday on how to prioritize testing for COVID-19 in the United States.
 Where is CNN's story on Biden accuser Tara Reade?
 A forgotten principle of public policy reform: focus on failure and you will get failure, focus on success and you will get success.
 Congressional Democrats are already talking about another massive spending bill, this one to include a huge bailout for state and local governments.
 The mindset of the shutdown zealots is the opposite of science.
 To make sense of the carnage at New York nursing homes, you don’t need to sprout wings and survey the scene from 30,000 feet. Just keep your BS detector on and connect a few big dots.
 New Jersey’s first COVID-19 patient entered our hospital in early March and the last six weeks have been unlike anything I’ve experienced in nearly four decades in health care.
 Bernie Sanders argued that the coronavirus outbreak proved the government should take over our nation's health care system. 
 Do quick shutdowns work to fight the spread of Covid-19? Joe Malchow, Yinon Weiss and I wanted to find out. We set out to quantify how many deaths were caused by delayed shutdown orders on a state-by-state basis.
 Tara Reade, a former aide for then-Sen. Joe Biden, has accused the presumed Democrat candidate of sexual assault.
 COVID-19 is so new and information is changing so rapidly that it is difficult to separate fact from fiction and truth from partial truth.
 A reporter asked me, “Are your caregiving duties different as you now care for your wife, Gracie, with the COVID-19 virus?”
 As the coronavirus pandemic rolls on, do you look for signs of good news – encouraging and uplifting indicators that everything is going to be OK? 
 It is not surprising that so many people are reporting a change in their mental health during this time, and not for the good.
 When you start to feel depressed by your suffering, pause and reflect on what God has done for you in the past. He is an unchanging God, and we can confidently set our hope in him.
 Though we are all experiencing the same global crisis, I believe God is bringing unique revelations to every single human heart, leading them to a higher calling.
 As Americans know all too well, the coronavirus pandemic forced government at all levels to impose protective measures that amounted to a national shutdown. Many of the measures have been necessary, but they have also been devastating. 
 As America begins opening up again, a significant priority should be to focus on the opening of hospitals for general admissions and individual solo medical practices.
 The politicization of science has ingrained in our political life something about which we ought to be highly skeptical: The argument from authority. It is doing extraordinary damage to the republic, through governmental responses — federal, state and municipal — to the coronavirus.
 With over 26 million Americans filing for unemployment benefits in the past five weeks due to the coronavirus pandemic, President Trump’s decision to issue an executive order Wednesday barring some categories of immigrants from entering the U.S. for at least 60 days is eminently reasonable and the right thing to do.
 Small protests popping up around the country against shelter-in-place orders that were imposed to slow the spread of the coronavirus pandemic are dangerous. The protesters are calling for conduct that would inevitably result in in more deaths and more hospitalizations from COVID-19, the respiratory disease caused by the virus
 Working together on character development (or hair) isn’t a one-person job.
 Mass quarantines under the best circumstances are really just a stopgap. The only way to tame a virus like this is with science.
 President Trump and his supporters are so despised by Democrats and their media enablers that many reflexively wrote off conservative skepticism about the official Chinese government story on the origin of the coronavirus.
 The coronavirus panic will grant us at least one unintended but beneficial side effect: The college scam that has contributed to the financial ruin of America’s middle class over the past four decades is finally coming to an end.
 We want protections in place for citizens who are truly struggling. But expecting landlords to foot the bill for all tenants regardless of income goes too far.
 The federal government should not replace lost state revenue. Sending the same taxpayer dollar to Washington first just to send it back to the state to fill a hole in the state budget isn’t logical.
 Even though there is real risk involved in beginning to reopen America, we have no choice. We cannot be frozen for months by the threat of the virus. Our society and economy will collapse, our people will grow ungovernable, and our capacity to work and innovate will be shattered.
 A newly surfaced explosive but unproven allegation of a 1993 sexual assault by then-Sen. Joe Biden is being ignored and denied by Democrats – the same Democrats who were quick to embrace clearly fabricated sexual assault allegations against now-Supreme Court Justice Brett Kavanaugh in 2018.
 https://www.foxnews.com/opinion
