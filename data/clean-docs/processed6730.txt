The latest coronavirus updates: Saturday, April 11, 2020
 Would you like to receive local news notifications on your desktop?
 Menu
 All of the updates on the coronavirus and the incredible impact it's having on our lives can be hard to keep up with. To help you keep up, we'll post this daily blog on our homepage. You can find all of our stories, and a map of Ohio cases, on our coronavirus page.
 7:32 PMIn an effort to help those in need take care of their pets during the COVID-19 a pet food pantry in Willowick gave away nearly 3,000 pounds of food Saturday. More here.
 6:43 PMThe city of Cleveland reported its third COVID-19 related death and 11 new cases, bringing the city's total to 293. More here.
 5:37 PMFOUNT leather store in Cleveland is helping University Hospitals and is seeking volunteers to sew face masks for healthcare workers. More here. 
 5:02 PMThe Country Fest, the annual three-day country music festival at Clay's Park Resort in North Lawrence, has been rescheduled due to COVID-19 concerns and changes to the lineup have been made—but the event is working to give back to the community through it all. More here.
 3:37 PMCleveland Public Library is working with the city of Cleveland to help protect first responders from COVID-19 while serving their communities. On Monday, the library will begin production of 2,000 face shields using the 3D printers inside the Louis Stokes wing of the library. More here.
 2 PMThe Ohio Department of Health confirmed Saturday that there are now 6,250 total cases of COVID-19 in the state. That's a jump of 372 from yesterday. A total of 247 deaths have been reported. Both the cases and deaths include the new cases that fall under the probable case and deaths. More here. 
 12:25 PMSaturday, the Cleveland Hope Exchange and Journey Church hosted a huge food giveaway to help those who are struggling to nourish their families. More here.
 12:24 PMThe United States has surpassed Italy to become the country reporting the most COVID-19 deaths in the world, a figure which experts have called an underestimation. More here.
 RELATED: The latest coronavirus updates: Friday, April 10, 2020
 
 Read our daily Coronavirus Live Blog for the latest updates and news on coronavirus.
 We're Open! Northeast Ohio is place created by News 5 to open us up to new ways of thinking, new ways of gathering and new ways of supporting each other.
 See data visualizations showing the impact of coronavirus in Ohio, including county-by-county maps, charts showing the spread of the disease, and more.
 View a global coronavirus tracker with data from Johns Hopkins University.
 Here is everything you need to know about testing for coronavirus in Ohio.
 Here's a list of things in Northeast Ohio closed due to coronavirus concerns
 See complete coverage on our Coronavirus Continuing Coverage page.
 Global Coronavirus Tracker: See map hereData from The Center for Systems Science and Engineering at Johns Hopkins University.
 https://www.news5cleveland.com/news/continuing-coverage/coronavirus/the-latest-coronavirus-updates-saturday-april-11-2020