Signals #3: Understanding the coronavirus crisis  | Ipsos
 The third edition of our digest brings together Ipsos’ latest research on coronavirus and draws on our surveys, social media monitoring and analysis from our teams around the world.
 The last two weeks have seen further countries moving into lockdown, accompanied by unprecedented interventions by governments to try to stave off job losses and protect their economies. Everything is disrupted.
 The aim of this digest is to highlight some of the things we are learning as we go through different stages of the crisis, explore how attitudes may be changing and start to anticipate what the longer-term implications will be for us all.
 You can download previous editions of Signals from the dedicated Ipsos COVID-19 web page.
 This edition features:
 This fourth edition sees us bring together our latest research on coronavirus and draws on our surveys, social media monitoring and the analysis of our teams around the world.
 Confronting Coronavirus
 April 9 - Every crisis is unique, but every crisis also has antecedents in history – especially with regard to the crisis’s effect on public opinion.
 Watch our on-demand webinar and listen as Ipsos’ experts review the first-wave results of our new regular COVID-19 HCP Surveillance Research of 300+ HCPs across seven specialties.
 Americans and Canadians are worried. They believe that the Covid-19 pandemic poses a threat to the country, their personal finances, and their health. Watch our on demand webinar featuring a live Q+A with Ipsos’ Darrell Bricker in Canada and Clifford Young in the US.
 April 22 - With 85% of S&P 500 companies now disclosing their ESG status, most corporations know they need to focus on ways to operate in a more sustainable manner, whether it be reducing their carbon footprint or overhauling their entire supply chain.
 Watch our on-demand webinar to explore the biggest future Payer challenges and look at how traditional ATUs can be complemented by a more dynamic approach to tracking, one which encompasses Payers in addition to HCPs and therapy monitoring.
 https://www.ipsos.com/en/signals-3-understanding-coronavirus-crisis
