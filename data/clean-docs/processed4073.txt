Trump pitches sunlight, humidity in coronavirus fight | Las Vegas Review-Journal
 The White House on Thursday pitched “emerging” research on the benefits of sunlight and humidity in diminishing the threat of the coronavirus as President Donald Trump encourages states to move to reopen.
 WASHINGTON — The White House on Thursday pitched “emerging” research on the benefits of sunlight and humidity in diminishing the threat of the coronavirus as President Donald Trump encourages states to move to reopen their economies.
 Past studies have not found good evidence that the warmer temperatures and higher humidity of spring and summer will help tamp down the spread of the virus.
 But William Bryan of the Department of Homeland Security said at a White House briefing that there are “emerging results” from new research that suggest solar light has a powerful effect in killing the virus on surfaces and in the air. He said scientists have seen a similar effect from higher temperatures and humidity. A biocontainment lab in Maryland has been conducting testing on the virus since February, Bryan said.
 “The virus is dying at a much more rapid pace just from exposure to higher temperatures and just from exposure to humidity,” Bryan said.
 Bryan said having more knowledge about this could help governors when making decisions about how and when to open their state economies. However, he stressed that the emerging results of the light and heat studies do not replace social distancing recommendations.
 Trump, who has consistently looked for hopeful news about containing the virus, was asked if it was dangerous to make people think they would be safe by going outside in the heat, considering that so many people have died in Florida.
 “I hope people enjoy the sun. And if it has an impact, that’s great,” Trump replied, adding, “It’s just a suggestion from a brilliant lab by a very, very smart, perhaps brilliant man.”
 “I’m here to present ideas, because we want ideas to get rid of this thing. And if heat is good, and if sunlight is good, that’s a great thing as far as I’m concerned,” the president said.
 Trump noted that the researchers were also looking at the effects of disinfectants on the virus and wondered aloud if they could be injected into people, saying the virus “does a tremendous number on the lungs, so it would be interesting to check that.” Bryan said there was no consideration of that.
 The president has often talked up prospects for new therapies and offered rosy timelines for the development of a vaccine.
 Earlier in the month, scientific advisers told the White House there’s no good evidence yet that the heat and humidity of summer will rein in the virus without continued public health measures.
 Researchers convened by the National Academies of Sciences, Engineering and Medicine analyzed studies done so far to test virus survival under different laboratory conditions as well as tracking where and how COVID-19 has spread so far.
 “Given that countries currently in ‘summer’ climates, such as Australia and Iran, are experiencing rapid virus spread, a decrease in cases with increases in humidity and temperature elsewhere should not be assumed,” the researchers wrote earlier in April in response to questions from the White House Office of Science and Technology.
 In addition, the report cited the global lack of immunity to the new virus and concluded, “if there is an effect of temperature and humidity on transmission, it may not be as apparent as with other respiratory viruses for which there is at least some pre-existing partial immunity.”
 They noted that during 10 previous flu pandemics, regardless of what season they started, all had a peak second wave about six months after the virus first emerged.
 In March, Dr. Michael Ryan, the World Health Organization’s emergencies chief, said, “We have to assume that the virus will continue to have the capacity to spread, and it’s a false hope to say yes, it will just disappear in the summertime like influenza.”
 Some states have started to ease closure orders, and Trump is expected to begin to highlight his administration’s work in helping businesses and employees.
 Hispanic leaders and advocacy groups are pushing Joe Biden to pick Nevada Sen. Catherine Cortez Masto as a running mate to energize the Latino vote in the general election.
 About half of Southern Nevada’s public employee union contracts are set to expire in June, just as the financial damage from coronavirus closures will be made more clear.
 Now, nearly 90 years later, the country is fighting a disease that presents the country with wrenching life-and-death challenges. Yet at the same time, it has served up something else as well: a rare opportunity to galvanize Americans for change.
 According to data from the Southern Nevada Health District, there are now 3,570 cases of the virus in the county and 174 deaths as of Saturday morning.
 A coronavirus outbreak behind prison walls could overwhelm the Nevada Department of Corrections, documents analyzed by the Las Vegas Review-Journal showed.
 For the first time, President Donald Trump cut off his daily coronavirus task force briefing on Friday without taking any questions from reporters. It may not be the last time.
 Nevada sportsbooks aren’t allowed to take bets on the U.S. presidential election, but South Point oddsmaker Jimmy Vaccaro announced odds for entertainment purposes only.
 The coronavirus outbreak brought Nevada’s economy to its knees. Now North Las Vegas is bracing for a $64 million revenue shortfall through the end of the next fiscal year.
 The Nevada National Guard distributed nearly a million pieces of personal protective equipment to 22 Southern Nevada nursing and assisted living facilities this week.
 Powered by WordPress.com VIP
 https://www.reviewjournal.com/news/politics-and-government/trump-puts-forth-idea-that-heat-humidity-could-help-fight-virus-2013186/
