If Scott Morrison is true to his word, October's post-coronavirus budget could be a doozy - Politics - ABC News (Australian Broadcasting Corporation)
         Posted 
     
       April 24, 2020 09:57:10
 With the coronavirus curve flattened, Scott Morrison is now hunting for steroids to drive up the curve of Australia's national productivity.
 The out-of-the-blue pandemic confronted the Prime Minister with health and economic crises unprecedented in our times. Out of that has come what is — in theory — an extraordinary opportunity for his Government to reshape Australia's economic landscape.
 Reality, of course, may be quite another story. Unless the right medicines are administered and the patient is compliant, it will be very difficult for the economy to achieve the large and accelerated recovery desperately needed.
 The International Monetary Fund forecasts the economy will shrink 6-7 per cent in 2020 before growing 6-8 per cent in 2021. They're breathtaking numbers.
 Despite the enormous financial cushioning from government handouts, the economy is currently headed for as close to ground zero as you'd ever want to see.
 Reserve Bank Governor Philip Lowe this week sketched a grim picture. National output to fall by about 10 per cent over the first half of this year; total hours worked down by about 20 per cent in that period; unemployment at about 10 per cent by June (although maybe lower if businesses retain their workers on shortened hours).
 Both Lowe and Morrison stress the economy on "the other side" won't look like it did before, but they're unclear how it might look.
 Lowe said the "overall challenge is to make Australia a great place where businesses want to expand, innovate, invest and hire people." Indeed.
 To go down this path, he suggested — and Morrison echoed this on Thursday — "we start off by reading the multitude of reports that have already been commissioned".
 Lowe summarised the thrust of these reports. "They say we should be looking again at the way we tax income generation, consumption and land in this country. They say we should be looking at how we build and price infrastructure.
 "They say we should be looking at how we train our students and our workforce, so they've got the skills for the modern economy.
 "They say we should be looking at how various regulations promote or perhaps hinder innovation and they say we should be looking at the flexibility and complexity of our industrial relations system."
 Implementing all this would be a huge agenda. The aspirations would find a good deal of common agreement across the political spectrum but get down to detail and it is another story. There's a reason why many good ideas have languished — adoption is painful and/or strongly resisted by vested interests.
 Morrison — who knows it's important to jawbone in this time of hiatus rather than leaving a vacuum in the debate — said he wanted to "harvest" ideas. In other words, in a policy sense, we're at a fresh start.
 There's more than one way of looking at what Morrison is doing.
 Primarily, the Government is driven by necessity. It will have to make a dash for growth and is desperately searching for ways to achieve this.
 Secondly, Morrison sees an opportunity to be a reformer who leaves a mark, a chance to push changes that would be inconceivable in normal circumstances.
 As part of this, the times might allow him — if he wished — to crash through some internal party roadblocks, notably on the issue of climate change. Critical in itself, that could be useful electorally.
 Morrison highlighted a Productivity Commission report he commissioned as treasurer, titled Shifting the Dial: 5 Year Productivity Review, which put forward a sweeping set of recommendations.
 The commission emphasised the importance of the Council of Australian Governments (COAG) restoring "its role as a vehicle for economic and social reform" saying "the scope for the vital big reforms will require commitment to a joint reform agenda by all jurisdictions".
 The national cabinet (COAG wearing another hat and slimmed-down clothes) has worked extremely well during the pandemic. A key has been its juggling of unity and disunity. Disagreements between the Commonwealth and states have been part of the process, but they've been accommodated rather than turning into divisive public slanging matches.
 For an effective reform agenda, some of the national cabinet's spirit needs to be retained, although it is pie in the sky to think the same level of cooperation as we're seeing would continue in dealings across a broad range of issues where there'd be differing interests and views.
 The first big test of Morrison's post-COVID-19 economic strategy will be the October budget. If he's serious about reform, much will have to be piled into that document.
 If this is done, it would be the first comprehensive "reform" budget since 2014. And we all remember what happened then.
 A feature of big reform programs is they have winners and losers. Without a lot of money to throw around in compensation (and that's unlikely to be available), some or many people would be unhappy, and vociferous.
 "Reform" is great unless you are one of those who is run over by it.
 An unknown is how the mood of the Australian public will be in another few months.
 So far, the Government has carried people along with the harsh measures required in tough times. Opinion polling indicates Australians believe the Government has handled the pandemic well — as it has. (In this context, it will be interesting to see whether it can segue this trust into take-up of its proposed tracing app.)
 It will, however, be a big jump to securing support for the ambitious economic reform measures Morrison is hinting at for the recovery phase.
 The Federal Opposition has for some time been moving back to a more contested political debate. One issue of contention in coming months is likely to be that while Morrison wants a heavily business-led recovery, the Opposition sees a bigger role for the Government, which has been highly interventionist during the crisis.
 Anthony Albanese will face some sharp choices in dealing with the Morrison reform agenda. It will be replete with opportunities for Labor to exploit its many pinch points. On the flip side, depending on the public mood, Albanese will need to be careful to avoid looking like a leader stuck in pre-COVID thinking.
 Michelle Grattan is a professorial fellow at the University of Canberra and chief political correspondent at The Conversation, where this article first appeared.
 Topics:
 government-and-politics,
 	politics-and-government,
 	federal-government,
 	federal-parliament,
 	economic-trends,
 	budget,
 	business-economics-and-finance,
 	covid-19,
 	australia
 If you have inside knowledge of a topic in the news, contact the ABC.
 ABC teams share the story behind the story and insights into the making of digital, TV and radio content.
 Read about our editorial guiding principles and the standards ABC journalists and content makers follow.
 Learn more
 This year, the ABC will be broadcasting Anzac Day events the public are unable to attend due to coronavirus restrictions in each state and territory.
 Whichever way you look at it, coronavirus is going to be with us for quite a while. 
 Out of a pandemic crisis has come what is — in theory — an extraordinary opportunity for Scott Morrison's Government to reshape Australia's economic landscape.
 What do feral sheep, Posh Spice, Donald Trump, and a potato have in common? They're all in this week's news quiz.
 AEST = Australian Eastern Standard Time which is 10 hours ahead of GMT (Greenwich Mean Time)
 https://www.abc.net.au/news/2020-04-24/coronavirus-scott-morrison-budget/12179364
