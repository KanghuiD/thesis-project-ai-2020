 Mullane: A small gym sweats out coronavirus in Falls Township - Opinion - Bucks County Courier Times - Levittown, PA
 “I don’t know what the future is, and that’s what’s scariest,” said Marty McLoughlin, owner of Extreme Fitness.
 The lights are on. Upbeat music plays. Kettlebells are neatly lined against a wall. Workout machines gleam. But no one’s pumping iron at Extreme Fitness, a private gym in Fallsington.
 It’s empty, except for the proprietor, Marty McLoughlin, 50, who spent 20 years building his fitness and weight-loss business. Idled by a state lockdown, he wonders if what he built is strong enough to withstand the coronavirus gale.
 “I don’t know what the future is, and that’s what’s scariest,” he said. He looked around the 7,000-square-foot warehouse-turned-workout center on Headley Place off Lower Morrisville Road, where he’s been for a decade. He looked, and he said nothing.
 “Waiting for it to populate,” he said at the screen, by which he meant he’d wait for more and more people to show up online. About 300 would.
 While the technology is awesome, it doesn’t bring in cash. It’s free. The online workouts keep the engines warm at Extreme Fitness so, hopefully, when the coronavirus ends, the gym roars like a summer Harley on the Levittown Parkway.
 But Marty’s not sure. No little guy with overhead and a slender margin is sure how this ends for them.
 “We’re all dying,” he said. “We have no money. Nobody’s working, that I know. I can’t ask people for money right now. And why would you make people pay? It’s wrong. It’s just wrong. Everyone’s in the same boat.”
 He looked at the laptop.
 “I see so many local business owners openly panicking online. Yeah, it’s that bad,” he said.
 All of it ― the panic, the uncertainty, the dread of financial ruin ― is on one side of the scale that measures the coronavirus catastrophe. On the other?
 “You have to be forward-looking,” he said. You frame it by considering your roots.
 He began as a trainer at LA Fitness in Langhorne in the early 2000s. He acquired some clients and invested in exercise equipment, which he shoehorned into a second-floor bedroom of his Levittown rancher in Bristol Township. As the business grew, he built a large shed in his backyard, moving operations there. Then, he moved to a storefront on Lincoln Highway in Falls. After that, Headley Place.
 Then, the coronavirus hit, and he was closed.
 All the downtime, is it spent in despair?
 No, Marty said. He snaps out of it. He fights. He tells himself don’t give up, don’t lose your spirit. It’s the way he coaches his clients to do one more rep, one last pull, one last 20-second interval on the eternal, dreaded treadmill.
 “This is a chance for anyone with a (small) business to think long and hard and ask, first, ‘How was I running things a month ago?’ When the money came in, were you just saying ‘pay me’? Or were you putting passion into it, and love and care into your business? Were you doing things for your customers, things that would make them never leave you, that would make them always want to come back to you?
 “Business owners have to be reinventing themselves. We all got to be asking, ‘How can I make my business more appealing after coronavirus?’ Stay positive. Show your passion. Mostly, remember why you started your business.
 “Those people like me, who grew a business out of their house, you got to go all the way back to the beginning, because (coronavirus) is forcing us all to hit the reset button.
 “It means we’re entering a brand-new world,” he said. “It’s not going to be the same one that ended a few weeks ago, for small businesses or for our customers. Things will not be the same, especially for a business like mine, or where people are in close contact. Everybody’s touching something that somebody else touched.
 “Can the fitness business really go back to the way it was after this experience? People are still going to be afraid. This panic, which is what it is, is going to last for a year. And the damage is deep. I don’t think most businesses like mine are going to feel like they’re actually running as strong for at least a year. We’ll see spikes of (the virus) pop up; we’ll see schools temporarily shut down again. This wave of panic, we’re riding it for a year.”
 Marty is keeping notes, making observations. He’ll survive. Because like every successful small business built with love and from scratch, he worked too damn hard.
 Columnist JD Mullane can be reached at 215-949-5745 or at jmullane@couriertimes.com.
 							Choose the plan that's right for you.
 							Digital access or digital and print delivery.
 						
 Original content available for non-commercial use under a Creative Commons license, except where noted.
 Gannett | USA TODAY NETWORK
 https://www.buckscountycouriertimes.com/opinion/20200401/mullane-small-gym-sweats-out-coronavirus-in-falls-township