Analysis & Opinion | South China Morning Post
 While some labour-intensive supply chains and those that target the US market may move out of China, sophisticated manufacturing clusters related to electronics and the internet of things are not easy to replicate quickly.
 For the first time in J. V. Yakhmi’s 55 years of living in Mumbai, the streets are eerily silent, as a raft of lockdown-imposed changes make it difficult to get anything done.
 Unlike markets heavily dependent on cross-border investment, South Korea has a large domestic investor base. The country’s property market is also benefiting from its success in containing the spread of Covid-19.
 The world is facing a trust deficit at a time when effective cooperation is needed more than ever. Yes, the coronavirus is terrible, but what is worse is a political virus, such as a Cold War mentality, which harms everyone.
 Instead of reshoring all US manufacturing, products crucial to public health and national security should be identified and incentives provided to move these production lines back home or to US allies.
 With university entrance examinations in full swing, now is not the time to lower guard against the coronavirus after two consecutive days of zero cases.
 This was supposed to be the year Europe and China sorted out their differences over trade. There is a lot at stake as each is a top trade partner of the other, and euro weakness means China might need to worry about its holdings of the currency.
 That’s just one of the disaster scenarios facing the world if the US, China and Russia can’t get along in the post-Covid-19 world.
 It might be a good PR move, but when it is helping, who cares? The bad actors that never intended or simply failed to deliver will be remembered by the paying public.
 The absence of critical health care coupled with food insecurity in highly indebted poor countries could set off social and political unrest. People from the developed world may find they have something in common with their developing world brethren – survival.
 China’s economy is expected to grow, it is capable of generating the jobs and trade needed for recovery, and looks ready to lead. In contrast, the US economy is shrinking and champions of free trade and investment are lacking in Trump’s ‘America first’ White House.
 In the wake of Covid-19, it is important that lessons be learned and that communities are educated in the need to implement them for the common good
 The plague that swept through Europe in the late 1340s shut many universities but ultimately led to a world view that valued science.
 Japan’s announcement of funding for companies that shift production lines back to the country marks a step away from its earlier strategy of trying to improve relations with China.
 Recent data has raised hopes of a slow recovery but the news might not be as good as it looks, writes Cary Huang.
 Things are feeling out of place, with local media seeming to bury news of a record number of cases and the enigmatic posts of the prime minister’s wife.
 Hongkongers, used to life in the fast lane, have struggled to adapt to spending more time in their often small homes. History, however, shows that time spent in isolation can be productive.
 Yonden Lhatoo is more alarmed than amused by the US president’s outlandish theories on curing Covid-19 by injecting patients with disinfectants or bombarding them with UV light.
 Two top conventions on biodiversity and climate change have been postponed but for respective hosts China and Britain, now is the time to lead the global discussion on conservation, linking up pandemics, public health and the environment.
 https://www.scmp.com/coronavirus/analysis-opinion
