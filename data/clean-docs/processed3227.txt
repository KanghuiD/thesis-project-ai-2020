Coronavirus live blog: 28 March-3 April | Campaign US
 Agencies start responding to UN brief.
 
 2.30pm: Agencies start responding to UN brief
 Last week, the United Nations released an open brief for creatives to tackle health and awareness during the pandemic. 
 Some agencies are already responding to this. A platform has been launched called #GlobalCreativeReview that aims to allow creative leaders to provide each other with feedback on new work to ensure it resonates with audiences around the world. 
 Meanwhile, We Are Social has created a lens on Snapchat that alerts the user if they're not at least two metres away from someone else.  
 2pm: Google and ITV ad pays tribute to NHS
 Google created a special ad in collaboration with ITV, ahead of the broadcaster’s pause on Thursday evening to applaud the NHS and other key workers. It pays tribute to the NHS and highlights how, more than ever before, people are searching for ways to help. The ad features the strapline: "Stay at home. Protect the NHS. Save lives."
 9.30am: ITV scraps director bonuses and cuts pay by 20%
 ITV has cancelled the annual bonus for its executive directors and management board, as well as any cash bonus payable for the broadcaster's 2020 performance. 
 It has also put in place a voluntary 20% cut in base salary for the executive directors and management board during the current lockdown. In a trading update this morning, ITV said that if the lockdown continues beyond 30 June, then it will review the salary cuts. Non-executive directors will also have their fees cut by one-fifth.
 The move follows a recruitment and salary freeze across the business. It is part of ITV's plans to manage cashflow during the pandemic. 
 7am: Cannes Lions 2020 cancelled
 "Cannes Lions announces today that the annual Festival of Creativity will not take place in October as previously planned. The next edition of the festival will run June 21-25, 2021," parent company Ascential said.
 Campaign reported earlier this week that several agency groups would not send delegates to the rescheduled festival. 
 "As the impact from Covid-19 continues to be felt across the world on consumers and our customers across the marketing, creative and media industries, it has become clear to us our customers’ priorities have shifted to the need to protect people, to serve consumers with essential items and to focus on preserving companies, society and economies," the company said.
 "We have tried to make our decisions as early as possible to give the industry total clarity on the situation, and that is why we are announcing this move today."
 Thursday 2 April
 4pm: #ClapForOurCarers returns for second week
 Following Channel 4’s ad break takeover last week, ITV is set to pause its programming tonight ahead of The Martin Lewis Money Show at 8pm, in tribute to the efforts of NHS and key workers.
 Alongside idents by ITV Creative, the broadcaster will encourage members of the public to stand on their doorsteps and balconies (while practising social distancing) to applaud those working on the front line of the pandemic, as well as to donate to NHS Charities Together, which distributes funds to more than 140 NHS charities across the UK.
 Happy clappers are invited to clap every Thursday at 8pm moving forward.
 3pm: TV viewing among 16-34s on the rise
 Almost half (44%) of 16- to 34-year-olds have increased their TV viewing, according to new research from Channel 4, while 93% are watching either the same or more.
 The study, which uses data from two of the broadcaster's research communities, 4Youth (16- to 24-year-olds) and Core4 (registered users of All 4), found high levels of trust among the age group for TV, which 82% of the demographic said they trusted, compared with 63% saying the same about newspapers and 41% about social media.
 Channel 4 is the youngest-skewing UK public-service broadcaster and is experiencing the largest growth in viewing among 16- to 34-year-olds, with last week up 38% year on year. Recent audience highlights include the return of Friday Night Dinner (pictured, above) on 27 March, which Channel 4 said attracted an overnight audience of 2.4 million, and the same evening's Gogglebox, which achieved the show's highest consolidated audience since 2016 (4.9 million).
 10.30am: How to access Covid-19 business advice and help
 With businesses remaining under huge pressure as work dries up, projects are postponed and clients cut fees or ask for "payment breaks", Campaign has compiled a guide for brands, agencies and media companies alike to deal with the crisis.
 This includes furloughing staff, seeking additional bank finance and utilising the government's £350bn relief package.  
 You can read the full guide here. 
 9.45am: Sky chief Jeremy Darroch donates salary
 Sky's chief executive, Jeremy Darroch, has said he will donate at least nine months of his salary to Covid-19-related charities, starting with the National Emergencies Trust. Brian Roberts, chief executive of Sky's parent company Comcast, is doing the same, as is Jeff Shell, his counterpart at NBCUniversal, which is also Comcast-owned.
 Sky did not provide a figure for how much Darroch's donation would be worth, but confirmed that his salary was above £1m per annum. According to a report from the High Pay Centre, Darroch, who has been chief executive of Sky since 2007, was paid £16.1m in the financial year ending 2017, when he was the fourth-highest-paid chief executive in the FTSE 100 – one place ahead of WPP's Sir Martin Sorrell. The following year, Darroch's pay fell below £8.8m, meaning he was no longer in the top 10 highest-paid cief executives.
 Wednesday 1 April
 5pm: Wimbledon cancelled
 The All England Lawn Tennis & Croquet Club has cancelled the 2020 Wimbledon championships. This is the first time this decision has been taken since World War II. The tournament was due to be played between 29 June and 12 July.
 It is estimated that refunds to ticket holders, broadcast partners and sponsors will exceed £200m. Brands with a presence at the competition include Evian, HSBC, IBM and Slazenger.
 In a statement, the club said the 134th tournament will instead be staged from 28 June to 11 July 2021. The news follows the postponement of both Euro 2020 and the Tokyo Olympic Games. 
 2pm: Evening Standard cuts salaries and magazine publication after 'dramatic' ad revenue drop
 The Evening Standard is cutting salaries as its advertising revenue slows "dramatically", the publisher has said. The company will place a number of full-time employees on furlough, meaning the affected staff will remain on the books but not be required to work for a period of time, and will receive help from the government’s Covid-19 relief scheme. Meanwhile, the Evening Standard will reduce the salaries of all other employees by 20%. It will also suspend the publication of ES magazine for the next two months in response to a market-wide decline in adspend on weekly titles.
 "We believe these difficult measures will help the Evening Standard weather the current storm and we hope to be able to bring employees back off furlough leave as soon as the advertising market begins to recover, though we regret the impact these measures will have on so many colleagues in the short term," Mike Soutar, chief executive of the Evening Standard, said. "We will continue to work with our advertising customers to encourage them to start investing again as soon as possible. And we will continue to plan for the future."
 11am: Fewer Brits believe media has over-exaggerated outbreak
 Just one in four UK residents (27%) believe the media has over-exaggerated coronavirus, down from a massive 83% at the beginning of March, according to research from global intelligence platform Streetbees.
 The study – based on a sample of almost 2,845 people – also found that government sites are the leading source of information surrounding Covid-19 (53%), followed by global news sites (46%), local news sites (35%) and social media (31%). However, only 10% of participants trust information shared by global news sites.
 Moreover, the lockdown appears to be making people feel safer, with 48% expecting to be personally affected, compared with 56% from the previous week.
 9.30am: The Wall Street Journal offers ad recall assurance for print ads
 In an attempt to drive print ad revenue, News Corp titles The Wall Street Journal and Barron's are offering brands an ad recall guarantee between 1 April and 30 June, AdWeek reports. The newspapers are working with third-party international research company Research and Analysis of Media to measure the recall rate. If this is not at 70%, the advertiser will be offered another ad slot for free within two weeks. 
 9am: WHO partners Rakuten Viber to fight misinformation
 Tuesday 31 March
 3pm: McVitie's postpones Team GB campaign
 McVitie's has pushed back the launch of its Team GB partnership campaign, following news that the Olympic Games have been postponed.
 The work – which was created by TBWA\London and set to run between 16 March and 9 August – features Team GB athletes including diver Tom Daley, boxer Frazer Clarke and gymnasts Max Whitlock, Rebecca Downie and Ellie Downie.
 Emma Stowers, brand director at McVitie’s, said: "Following confirmation from the International Olympic Committee that the Olympic Games will be postponed until 2021, we have made the decision to halt our marketing campaign around our Team GB sponsorship. We are, of course, disappointed to do so; however, in light of the current situation, we have taken the decision to not progress any further activity across any channels.
 "We will be in continuous communication with the British Olympics Association regarding the 2021 postponement plans and will adapt accordingly. We expect next year’s Games to be a brilliant moment of celebration and coming together for the nation.
 "As a brand, we are always looking to bring moments of positivity to connect people and communities, and that will remain our key focus during this current period of uncertainty."
 1.30pm: CIM launches digital marketing course
 The Chartered Institute of Marketing has unveiled an online-only "digital marketing channels" course tailored for marketing professionals and business owners working from home.
 Maggie Jones, director of qualifications and partnerships at CIM, said: "We already have a breadth of training materials and guidance online, but with so many organisations adapting to remote working environments, it is absolutely critical that we do everything possible to ensure that marketing professionals can continue to upskill and develop."
 11.30am: Barnardo’s to furlough 3,000 staff
 Barnardo’s chief executive Javed Khan has announced plans to furlough upwards of 3,000 of its 8,000-strong workforce.
 The move follows news that the children’s charity lost £8m of its £25m monthly income overnight.
 11.15am: Nudgestock postponed
 Rory Sutherland’s Nudgestock Festival – which focuses on behavioural economics and advertising – has been postponed as the pandemic continues to cause disruption.
 The event, which first took place in 2013, was due to take place on 12 June in Margate.
 "Due to the ongoing health crisis, it’s clear that we will no longer be able to bring you Nudgestock 2020 from Margate as we had planned," a press release said.
 More information is expected in the coming weeks.
 10am: Grazia celebrates NHS front-line staff in multiple covers
 Grazia, the Bauer Media title, is celebrating NHS front-line staff in a special issue that has four front covers available. The magazine features interviews with the workers as they battle through the pandemic. The issue will be offered free to all NHS staff as a digital download.
 9.45am: Budweiser backs Britain’s pubs with £1m gift card scheme
 Budweiser is launching a gift-card initiative to support pubs struggling amid the nationwide closure of bars. The Save Pub Life scheme allows housebound locals to pre-purchase drinks, with the beer brand pledging to match all purchases up to a combined cap of £1m. All pre-promised pints can be claimed once bars (finally) open their doors again.
 9.30am: Reach collates brand offers for NHS workers
 Reach, which publishes the Daily Mirror and the Daily Star, is highlighting the special deals that brands are offering health workers on its NHS Heroes site, including free bike hire from Brompton, free razors from Gillette and free NCP parking. As part of a campaign that will run across all Reach publications, readers are also invited to write a message of appreciation for NHS workers. The project is in collaboration with the Royal College of Nursing.
 9.15am: Ofcom brings telecoms together for 'stay connected' campaign
 Ofcom has launched a national campaign alongside Britain’s biggest telecoms companies – including EE, O2, Sky and Vodafone – in an attempt to help households get the speeds and reception they need to work and learn while on lockdown. The campaign outlines seven tips for internet users to get the most out of their broadband and mobile internet connections, including using landline instead of Wi-Fi calls, using ethernet cables for a more reliable connection and switching off tablets and smartphones to increase Wi-Fi speeds.
 Monday 30 March
 4.30pm: Messages pour in for ITV's 'Britain get talking' campaign
 ITV has received more than three million responses to its campaign encouraging the nation to connect with each other during the coronavirus crisis. The broadcaster relaunched "Britain get talking", its mental-health initiative, earlier this month when presenters Ant & Dec called on people to send messages of support to their loved ones. Many of those were featured in a film, created by Uncommon Creative Studio, that aired on Saturday ahead of Ant & Dec’s Saturday Night Takeaway. More messages will continue to be played across ITV over the next month.
 4.15pm: Creative Mentor Network launches virtual mentoring scheme during lockdown
 3.45pm: Harry Kane and Steph Houghton front FA campaign
 — England (@England) March 30, 2020
 The Football Association is promoting physical and mental well-being from the safety of being indoors in a new campaign, "Football’s staying home".
 Launched by the England national football teams' captains, Harry Kane and Steph Houghton, the work plays on Three Lions, which assured fans that "football’s coming home", when, of course, it did not.
 It is set to feature England players from past and present, with content recorded at The FA's national football centre, St George’s Park, including motivational advice on Mondays, throwback videos on Thursdays, skill tutorials on Saturdays and personal stories on Sundays. The campaign will be supported across the England teams and the association’s digital channels.
 2.30pm: Creative leader offers up virtual book crits
 Angus Macadam, executive creative director at Mcgarrybowen London, is calling on creative leaders to help the next generation of talent by doing virtual book crits for students. "This is no act of generous altruism. We’re all still looking for the next team in. And, as an industry, we need the next wave of talent now more than ever," he says.
 "Call it the Zoom crit, corona crit, whatever. Do it on the platform that works for you. Just remember there are lots of courses out there with students who desperately need our time. So, as you WFH this week, remember to get in touch and let them know."
 11am: Airbnb to pause all marketing
 Airbnb plans to halt all global marketing spend, chief executive Brian Chesky told employees in a video conference call late last week.
 A report from The Information suggested that Airbnb’s losses have already stretched into hundreds of millions of dollars. Meanwhile, a Reuters report said that Airbnb's suspension of marketing spend will save up to $800m this year and its founders will take no salary for the next six months.
 9.30am: United donates £80,000 of ad space to Unicef
 Unicef, the United Nations' mission to help children around the world, has unexpectedly benefited after United Airlines decided to pull its spring ad campaign.
 The airline was unable to cancel or shift the UK outdoor advertising space worth approximately £80,000 that it had booked around the 15 March planned launch of the campaign.As a result, United – along with agency partners Carat and Mcgarrybowen – decided to donate the space to Unicef given its efforts around education about Covid-19 and protecting children across the globe.
 9am: Purplebricks suspends TV ads
 Purplebricks, the online estate agent and prolific advertiser, has reacted to the impact of coronavirus on its business by suspending as much of its marketing as possible.
 It told the City this morning that it has stopped its TV and radio ads, and "significantly reduced" its online marketing costs.
 Purplebricks’ outlay on marketing ranks alongside property portals Rightmove and Zoopla, and totalled £26.7m in the UK in its 2019 financial year.
 It was forced into the cut after the government last week banned activities such as property appraisals and visits by prospective buyers in order to support social distancing. 
 See the previous week's developments
 	Get the very latest news and insight from 
 	
 	  Campaign 
 	with unrestricted access to 
 	  campaignlive.com
 	, plus get exclusive discounts to 
 	events 
   
 	// 
 	$('a[href^="http://"]').not('a[href*=campaignlive]').attr('target','_blank');
 https://www.campaignlive.com/article/coronavirus-live-blog-28-march-6-april/1679390