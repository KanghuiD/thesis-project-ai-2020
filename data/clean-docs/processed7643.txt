Opinion | Financial Times
 										Coronavirus business&nbspupdate
 										Get 30 days complimentary access to our Coronavirus Business Update newsletter
 									
 										30 days complimentary
 Add this topic to your myFT Digest for news straight to your inbox
 Soaring fees, worthless degrees and dicey investments have hurt the economy
 Isolation is having a profound impact beyond the virus, which will influence when and how to reopen the economy 
 Regulators should force promoters to hold their positions after touting them
 Candour about the fiscal deficit might return the Democrat to prominence
 We do not need another big lending programme; equity investments would get us out of the credit-versus-loans debate
 Beijing’s repeated pledges to shrink the state are both empty and impossible
 Supply lockdowns could cause bigger declines in global demand, with falling prices
 Schools can spread infection, but long absences mean gaps in learning
 The crisis exposed the system was on a knife edge, but the emergency changes are for the better
 Donald Trump’s efforts to end a price war backfired leaving the industry on its knees
 Even Margaret Thatcher saw that letting institutions go bust was a bad idea
 Call me naive, but I thought top-level football had something to do with promoting ‘role models’
 Coronavirus is highlighting more than the industry’s wasteful ways; its iniquitous supply chain is also in the spotlight
 The etiquette of video conferencing is sadly a work in progress
 Lockdown videos give us a new way to judge character
 Any apps to contain the spread of Covid-19 will require high uptake and regular testing
 If lenders maintain support, the company’s balance sheet may not need an urgent fix
 We need to adjust to a world of more debt, less globalisation and greater digitalisation
 “I was asking a question sarcastically to reporters like you just to see what would happen.”
 The family has been making fine wine in Napa Valley for four generations
 Higher education should not be treated as just another business
 Randall Stephenson’s tenure is emblematic of US business in the 2010s
 Even among other top Wall Street banks, David Solomon’s 2019 pay rise stands out
 Valuations do not reflect the risks that still lie ahead from coronavirus
 Our columnists’ thoughts on saving the eurozone, M&A risks, and strongman leaders
 What happens when our dream of a bucolic return to the land collides with reality?
 Prince Harry’s move to the west coast is less counterintuitive than it may appear
 Pandemic offers a once-in-a-generation opportunity to reset the reputation of the pharmaceuticals industry
 Man’s antediluvian need for his best friends has resurfaced during lockdown
 McDonald’s and Ikea are sharing recipes online to satisfy our cravings at home
 Minus prices are not uncommon, even if they suggest infinite losses and generate horror
 When a chart can speak 1000 words. Not all of them entirely sincere.
 Technology means small shareholders need not — and should not — be excluded from emergency equity fundraisings
 Tech-savvy stylists offer virtual consultations and algorithm-matched dyes
 In the face of lockdown, go online to tour the world’s best gardens and shop for bulbs
 Inject personality and soul by using natural materials and mixing antique with modern
 We already use data mining to prevent terrorism and money laundering, why not to stop infection?
 International Edition
 https://www.ft.com/opinion
