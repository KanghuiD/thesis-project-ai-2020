      Your DIY mask makes a difference against coronavirus — here’s what science tells us - MarketWatch
     
 With the coronavirus pandemic quickly spreading, U.S. health officials have changed their advice on face masks and now recommend people wear cloth masks in public areas where social distancing can be difficult, such as grocery stores.
 But can these masks be effective?
 President Donald Trump, in announcing the change in the Centers for Disease Control and Prevention’s guidance on April 3, stressed that the recommendation was voluntary and said he probably wouldn’t follow it. Governors and mayors, however, have started encouraging the precautions to reduce the spread of the virus by people who might not know they are infected.
 Some cities have gone as far as setting fines for failing to wear a mask. In Laredo, Texas, anyone over the age of five who walks into a store or takes public transit without their mouth and nose covered by a mask or bandanna could now be fined up to $1,000.
 These new measures are designed to “flatten the curve,” or slow the spread of the coronavirus responsible for COVID-19.
 Flattening the curve is another way of saying slowing the spread. The epidemic is lengthened, but we reduce the number of severe cases, causing less burden on public health systems. The Conversation/CC BY ND
 They’re also a shift from the advice Americans have been hearing since the coronavirus pandemic began.
 The World Health Organization and the CDC have repeatedly said that most people do not need to wear masks unless they are sick and coughing. In February, the U.S. surgeon general even urged the public to stop buying medical masks, warning that it would not help against the spread of the coronavirus. Part of the reason was to reserve N95 respirators and masks for health-care workers like myself who are on the front lines and exposed to people with COVID-19.
 Today, there is much more data and evidence on how COVID-19 is spread, and the prevalence of the disease itself is far more widespread than previously thought.
 As recently as early February, the World Health Organization stated that viral transmission from asymptomatic people was likely “rare,” based on information available at the time. But a growing body of data now suggests that a significant number of infected people who don’t have symptoms can still transmit the virus to others.
 A CDC report issued March 23 on COVID-19 outbreaks on cruise ships offers a glimpse of the danger. It describes how the testing of passengers and crew on board the Diamond Princess found that nearly half — 46.5% — of the more than 700 people found to be infected with the new coronavirus had no symptoms at the time of testing.
 The CDC explained that “a high proportion of asymptomatic infections could partially explain the high attack rate among cruise ship passengers and crew.”
 Dr. Harvey Fineberg, former president of the National Academy of Medicine and head of a new federal committee on infectious diseases, told CNN on April 2 that he will start wearing a mask in public, especially at grocery stores, for this very reason. “While the current specific research is limited, the results of available studies are consistent with aerosolization of virus from normal breathing,” he said.
 It is these “silent carriers” — people infected with the virus but without fever, cough, or muscle aches — that proponents of universal mask wearing point to as proof that more could be done beyond social distancing to slow the virus’s spread.
 While research on the effectiveness of universal mask wearing for reducing respiratory droplet transmission is still thin, there is evidence to support it.
 Research on SARS, another coronavirus, found that N95 masks were highly effective at blocking transmission of that virus. Even ill-fitting medical face masks have been found to interrupt airborne particles and viruses, keeping them from reaching as far when someone sneezes.
 Another study determined that, while masks made out of cotton T-shirts were far less effective than manufactured surgical masks in preventing wearers from expelling droplets, they did reduce droplets and were better than no protection at all.
 The surgical masks that doctors and nurses typically wear are designed for one-time use, while cloth masks used by the general public would likely be washed, which raises another concern.
 A study from Nepal on cloth masks designed to protect wearers from larger particles, such as pollution or pollen, found that washing and drying practices deteriorated the mask’s efficiency because they damaged the cloth material.
 It is clear that urgent research is needed on the best material suitable for universal masks, their storage and care, or the creation of proper reusable masks for the public.
 As an obstetrician-gynecologist and researcher, I believe that some protection for the public is better than none. A recent article in the medical journal The Lancet Respiratory Medicine states a similar rationale.
 The universal use of mouth and nose covering with masks is a low-risk intervention that can only assist in reducing the spread of this terrible illness. If everyone wears a mask, individuals protect one another, reducing overall community transmission. It could even remind people not to touch their faces after touching potentially contaminated surfaces.
 As the research shows, masks aren’t shields. It’s still important to help prevent transmission by practicing social distancing by staying at least 6 feet away from others in public, staying home as much as possible, and washing hands frequently and properly.
 Hector Chapa is a clinical assistant professor and director of Interprofessional Education at Texas A&M University’s College of Medicine in College Station. This was first published on The Conversation — “Why wear face masks in public? Here’s what the research shows”
 ‘It’s appalling to attach a dollar number to a human life — for noneconomists,’ said Colin Camerer, a professor of behavioral finance and economics.
           Historical and current end-of-day data provided by FACTSET.
           All quotes are in local exchange time.
           Real-time last sale data for U.S. stock quotes reflect trades reported through Nasdaq only.
           Intraday data delayed at least 15 minutes or per exchange requirements.
         
 http://www.marketwatch.com/story/your-diy-mask-makes-a-difference-against-coronavirus-heres-what-science-tells-us-2020-04-06
