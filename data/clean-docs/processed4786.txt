Coronavirus advice for employers and businesses - Doncaster Council
 This site requires a JavaScript enabled browser. Please enable Javascript or upgrade your browser to access all the features.
 This page provides guidance for employers and businesses on the coronavirus situation.
 You can use the government's Coronavirus business support finder tool to see what financial support is available for you and your business.
 Support Finder
 Local businesses are encouraged to visit the Business Doncaster Website which is being updated with the latest Coronavirus information. If you require a further discussion you can phone a Business Doncaster advisor on 01302 735555.
 Business Doncaster
 In response to the recent Covid-19 outbreak, the Chancellor announced the following measures to help support businesses. These include:
 Business Rates bills for 2020/21 have already been sent to Doncaster businesses. As a result these bills will only show entitlement to any relevant relief at the levels before these announcements were made.
 If you believe your business is eligible and would like to make a request for a small business and retail, hospitality and leisure grant payment please apply through the online form below:
 Grant Request Form
 Further information about COVID - 19 and the support available to businesses is also available on the Government website .
 The government has updated its COVID-19 guidance for employers and businesses. In addition there is guidance for businesses and other venues on social distancing measures.
 Advice is also available on decontamination for businesses, where a possible or confirmed COVID-19 case has spent time while symptomatic.
 Businesses are encouraged to display the government's coronavirus poster within their workspaces.
 You can find Coronavirus advice and guidance for businesses at Doncaster Chamber of Commerce.
 To assist with planning, it may be useful to refer to the Cabinet Office pandemic flu checklist for businesses. This refers to pandemic influenza but the checklist may be useful.
 Businesses should remind their staff to follow Public Health England advice to:
 Return to the Coronavirus (COVID-19) homepage
 Please sign in to use bookmarks.
 These facilities can be found in the header of every page:
 https://www.doncaster.gov.uk/services/health-wellbeing/coronavirus-advice-for-employers-and-businesses
