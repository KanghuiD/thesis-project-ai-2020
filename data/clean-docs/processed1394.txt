Coronavirus Map: Countries Where COVID-19 Has Spread | Time
 Coronavirus is continuing to spread globally with now more than 951,000 cases worldwide, Johns Hopkins University Center for Systems Science and Engineering confirmed eastern time Thursday morning, April 2. The global death toll has surpassed 48,000.
 COVID-19 cases and deaths continue to increase rapidly through Italy and the U.S.
 The World Health Organization (WHO) said Wednesday, April 1, that the past five weeks saw “near exponential growth in the number of new COVID-19 cases, reaching almost every country, territory and area.”
 “The number of deaths has more than doubled in the past week,” said Dr. Tedros Adhanom Ghebreyesus, WHO director-general, at a media briefing on Wednesday. “In the next few days we will reach 1 million confirmed COVID-19 cases, and 50,000 deaths.”
 In the central Chinese province of Hubei, where the virus is believed to have originated, the number of cases appears to be stabilizing, according to government figures. But the number of deaths outside mainland China is increasing, and now includes 13,155 in Italy, 10,003 in Spain and 4,043 in France as of Thursday morning, according to a virus tracker maintained by Johns Hopkins.
 Among the latest countries to confirm infections are Botswana, Burundi and Sierra Leone.
 On March 11, U.S. President Donald Trump announced a 30-day halt on travel to and from Europe, excluding the U.K. and on March 18, the U.S. announced it had reached an agreement with Canada to restrict “non-essential” border crossings. On March 21, a similar measure began on the southern border with Mexico.
 Keep up to date with our daily coronavirus newsletter by clicking here.
 The WHO announced Wednesday, March 11, that it is declaring the outbreak of COVID-19 a global pandemic. “This is not just a public health crisis, it is a crisis that will touch every sector,” Tedros said. at a media briefing. “So every sector and every individual must be involved in the fights.”
 The new global spread of the coronavirus may be attributable to the difficulty in detecting infected individuals. The virus’ long incubation period — the three to 14 days it usually takes before symptoms begin to show — and its often mild symptoms make it difficult to find. Complicating matters is that experts do not yet know all the ways the virus can be spread.
 “The big problem is that the transmissibility of this virus is still not understood,” David Heymann, a professor of Infectious Disease Epidemiology at the London School of Hygiene, tells TIME. “It’s understood how it is passed from person to person — social contact, a sneeze, a cough, face-to-face, and in hospital settings where there is not adequate infection prevention and control — but how easily this spreads in other circumstances is not known at present.”
 Here’s what to know about where the virus is spreading.
 All numbers are from the Johns Hopkins University Center for Systems Science and Engineering, unless otherwise specified, and are accurate as of 6 a.m. ET on April 2.
 216,722 cases; 5,137 deaths
 On Thursday, March 5, Congress passed an emergency $8.3 billion spending bill to battle the virus spread in the U.S. The U.S. Food and Drug Administration has also expanded a policy to allow more labs to apply for approval to conduct testing for the virus. On Wednesday, March 4, the CDC announced that anyone can get tested at a doctor’s request, but the rollout has been rocky and met with criticism.
 The U.S. has enacted strict travel restrictions and advisories, including a travel ban to Iran and increasing travel warnings to parts of Italy and South Korea. On Friday, March 20, the U.S. announced restrictions to non-essential travel across the southern border with Mexico. Travel to Iran is already significantly limited.
 110,574 cases; 13,155 deaths
 In a scramble to contain the unprecedented outbreak — concentrated primarily in the northern regions of Lombardy, Emilia-Romagna and Veneto — Italy announced a nation-wide quarantine on Monday, March 9.
 Countries neighboring Italy are taking measures to prevent disease outbreak.
 Experts do not yet know why Italy, a country that took especially tough early prevention measures, has been hit by an outbreak. (Italy, for example, was the first European Union country to ban flights to and from China.)
 104,118 cases; 10,003 deaths
 Spain’s government announced a two-week ban effective Monday, March 2, to all non-essential economic activity, according to the AP. Only employees of hospitals, pharmacies, the food supply chain and other “essential” industries are required to work until mid-April.
 77,981 cases; 931 deaths
 Chancellor Angela Merkel went into a home quarantine on Sunday, March 22, after a doctor who had treated her was diagnosed with the virus. On Monday, March 23, she tested negative for the virus, but will undergo further testing, according to the AP.
 On March 4, Germany’s task force to combat the virus spread announced it would be banning all exports of medical protective equipment in order to reserve them for health care professionals.
 57,763 cases; 4,043 deaths
 France was the first country to see a coronavirus death outside of China.
 On Sunday, March 8, France expanded a ban on indoor public gatherings. Now groups larger than 1,000 are banned, whereas previously the ban was on groups larger than 5,000. Fears about the virus led to a temporary shut down of Paris’ famed Louvre Museum on March 1, despite no known infections among museum staff, per AP. On March 3, French President Emmanuel Macron announced an official requisition on stocks of protective masks, and on March 4, he spoke with U.S. President Donald Trump to coordinate response efforts.
 47,593 cases; 3,160 deaths
 Iranian officials announced Friday, March 6, they could use “force” to limit travel between cities, the Associated Press reports. A WHO team of experts arrived in Tehran to support the country’s response to the outbreak on March 2. Members of Iran’s Revolutionary Guard, which includes about 125,000-plus troops and 600,000 mission-ready volunteers, are involved in relief efforts, according to the AP. Iran also cancelled Friday prayers for the first time in decades in an effort to limit the spread of the outbreak on Feb. 28 and then again on March 6.
 The head of Iran’s counter-coronavirus task force has contracted the virus, highlighting the country’s struggle to manage the outbreak. Prior to testing positive for the virus, Iraj Harirchi, the head of the counter-coronavirus task force, had given a news conference where he downplayed the severity of the outbreak. Following his diagnosis, Haririchi posted a video of himself quarantined at home, where he continued to assure the public that authorities had the situation under control.
 29,865 cases; 2,357 deaths
 On Feb. 10, health secretary Matt Hancock announced the department granted health service officials the legal power to isolate those who are believed to be at reasonable risk of having the virus or transmitting it. “Clinical advice has not changed about the risk to the public, which remains moderate,” Hancock said in a public statement. “We are taking a belt and braces approach to all necessary precautions to ensure public safety.”
 17,781 cases; 505 deaths
 The Swiss government on Friday, Feb. 28 banned large events hosting more than 1,000 people after nine people tested positive for COVID-19, resulting in the cancellation of the Geneva International Motor Show. “In view of the current situation and the spread of the coronavirus, the federal council has categorised the situation in Switzerland as ‘special’ in terms of the Epidemics Act,” the Swiss cabinet said in a statement.
 15,679 cases; 277 deaths
 15,348 cases; 1,011 deaths
 13,696 cases; 1,175 deaths
 10,842 cases; 158 deaths
 9,976 cases; 169 deaths
 Reuters reports that South Korean officials urged citizens to stay home on Feb. 28, and warned that it was a “critical moment” in the fight against the virus. On Feb. 29, the U.S. State Department issued a Level 3 travel advisory, urging all to reconsider traveling to South Korea due to the outbreak.
 9,731 cases; 112 deaths
 Canadian health officials have issued warnings against traveling to China and Iran. Officials have also implemented measures at 10 Canadian airports to identify passengers who might be sick, including requiring all passengers on returning international flights to respond to screening questions.
 8,251 cases; 187 deaths
 Portugal’s 71-year-old president Marcelo Rebelo de Sousa had self-isolated over fears of the coronavirus, according to the AP. He has no symptoms, the AP reports citing the president’s office, but made the decision in order to set an example of “taking preventative measures while continuing to work at home.”
 6,931 cases; 244 deaths
 On Feb. 26, Brazil was the first country in Latin America to diagnose a case of the coronavirus.
 6,211 cases; 33 deaths
 Israeli health officials have taken strict measures to try and contain the virus spread. The ministry has begun publishing flight details and other locations such as gyms and banks frequented by those who tested positive before their diagnosis, as well as the time they visited those locations. Anyone who was in those locations for 15 minutes or more at the times specified is being required to self-quarantine for 14 days.
 5,108 cases; 24 deaths
 On March 5, Australia announced that foreign nationals who have traveled to mainland China would not be permitted to enter the country until 14 days after they’ve left China. Among those who tested positive for the virus in Australia are actors Tom Hanks and Rita Wilson.
 4,947 cases; 239 deaths
 Swedish public health authority said Wednesday, March 4, that it would begin increasing and expanding coronavirus testing. Previously the authority had focused on testing those who had come into contact with people confirmed with the coronavirus, and people who had traveled to areas with clusters of the virus.
 4,898 cases; 45 deaths
 3,604 cases; 40 deaths
 3,548 cases; 30 deaths
 3,542 cases; 104 deaths
 Authorities in Denmark halted naturalization ceremonies throughout the country because handshakes are required as part of the ceremony, according to The New York Times.
 3,447 cases; 85 deaths
 3,116 cases; 50 deaths
 The coronavirus outbreak comes as the country is in the midst of political upheaval.
 3,031 cases; 16 deaths
 2,758 cases; 98 deaths
 2,633 cases; 107 deaths
 2,554 cases; 45 deaths
 2,460 cases; 94 deaths
 2,384 cases; 57 deaths (The Diamond Princess cruise ship: 712 cases; 11 deaths)
 Japan’s Prime Minister, Shinzo Abe, ordered elementary, middle and high schools to close from March 2 until spring break, typically around the beginning of April.
 Japanese Health Minister Katsunobu Kato declared Sunday March 1 that the Diamond Princess cruise ship, which was quarantined for 14 days until Feb. 19, was empty and fully vacated, according to the Associated Press. Earlier in the day, the last group of about 130 crew members left the Diamond Princess, per AP.
 With almost 20% of people who were on the ship infected, the quarantine measure has been heavily criticized by health experts as having exacerbated the outbreak. Rodney Rohde, a professor and chair of the Clinical Laboratory Science Program at Texas State University, tells TIME that a quarantine on land would likely have resulted in a smaller number of infections. “I refer to quarantined ships as floating petri dishes because you’re enclosing a living microbial agent with thousands of people in a small area.”
 2,319 cases; 29 deaths
 2,291 cases; 31 deaths
 2,032 cases; 58 deaths
 India is bracing for a widespread outbreak by screening international travelers at 30 airports and opening at least five large-scale quarantine centers, the Associated Press reported on March 6. Officials have closed schools, ordered a halt to exporting some pharmaceutical ingredients and has urged state governments to pause all Holi festivities, according to the AP.
 The country’s fragile health care system has international experts concerned the virus could have a severe impact on the world’s second-most populated country.
 Thailand
 1,875 cases; 15 deaths
 Thai authorities announced Monday, Feb. 24, that they are taking legal steps to implement emergency measures to control the spread, according to the AP. The measures include authorizing governors to temporarily shut down markets, entertainment centers, businesses and other activities deemed dangerous, the AP reports.
 It would also give disease control officials the authority to quarantine and detain people who are infected or are suspected of carrying COVID-19, according to the AP.
 1,790 cases; 170 deaths
 1,720 cases; 16 deaths
 1,518 cases; 17 deaths
 1,415 cases; 51 deaths
 1,380 cases; 5 deaths
 1,378 cases; 37 deaths
 1,323 cases; 47 deaths
 1,317 cases; 32 deaths
 1,284 cases; 57 deaths
 1,220 cases; 2 deaths
 1,133 cases; 33 deaths
 1,065 cases; 17 deaths
 1,060 cases; 28 deaths
 1,000 cases; 4 deaths
 Singapore Minister Lawrence Wong has called for large events to be canceled and said individuals should check their temperatures twice a day. It was also announced that inter-school activities and external activities have been suspended.
 963 cases; 6 deaths
 858 cases; 11 deaths
 847 cases; 58 deaths
 841 cases; 16 deaths
 835 cases; 2 deaths
 814 cases; 8 deaths
 804 cases; 20 deaths
 802 cases; 4 deaths, according to the Hong Kong Department of Health
 Authorities in Hong Kong are concerned that a new wave of infections could occur as more people from the U.S. and Europe return.
 797 cases; 1 death
 779 cases; 52 deaths
 Egyptian authorities quarantined a cruise ship off the coast of Luxor after 45 people on board tested positive on March 7, according to the AP. Those on board include foreign tourists and local crew members. Egypt Air, the country’s main airline, suspended flights to China in late January, the AP reports.
 728 cases; 52 deaths
 The outbreak in Iraq comes as many anti-government protestors have criticized Iraq’s relationship with Iran, according to the AP. Some have been critical of Iranian and Iraqi official’s handling of the virus. Officials closed Iraq’s borders to Iranian nationals, according to the AP, but not to Iraqi’s.
 676 cases; 39 deaths
 663 cases; 4 deaths
 649 cases; 8 deaths
 585 cases; 21 deaths
 569 cases; 4 deaths
 512 cases; 15 deaths
 479 cases; 16 deaths
 Health officials and experts expect the virus will spread throughout Lebanon as the country deals with financial crisis, anti-government protests and a shortage of medical supplies, according to Reuters.
 458 cases; 0 deaths
 449 cases; 10 deaths
 423 cases; 5 deaths
 423 cases; 12 deaths
 402 cases; 3 deaths
 400 cases; 5 deaths
 400 cases; 1 death
 390 cases; 15 deaths
 375 cases; 2 deaths
 354 cases; 11 deaths
 350 cases; 2 deaths
 339 cases; 5 deaths
 320 cases; 9 deaths
 317 cases; 0 deaths
 282 cases; 16 deaths
 278 cases; 5 deaths
 259 cases; 16 deaths
 255 cases; 6 deaths
 239 cases; 4 deaths
 236 cases; 28 deaths
 231 cases; 1 death
 Vietnam (222), Honduras (219 and 14 deaths), Cuba (212 and 6 deaths), Ghana (195 and 5 deaths), Cote d’Ivoire (190 and 1 death), Senegal (190 and 1 death), Uzbekistan (190 and 2 deaths), Malta (188), Nigeria (174 and 2 deaths), Belarus (163 and 2 deaths), Mauritius (161 and 7 deaths.)
 Please send any tips, leads, and stories to virus@time.com.
 Correction, March 4
 A previous version of this story misstated the number of countries where cases of COVID-19 have been diagnosed. It is roughly 80 as of March 4, not more than 100.
 Write to Jasmine Aguilera at jasmine.aguilera@time.com, Hillary Leung at hillary.leung@time.com, Amy Gunia at amy.gunia@time.com, Madeleine Carlisle at madeleine.carlisle@time.com and Sanya Mansoor at sanya.mansoor@time.com.
 https://time.com/5789631/coronavirus-covid-19-global-cases/