Coronavirus - University of Kent
 Latest Government advice is available on the gov.uk website
 We strongly advise all to use media outlets such as the BBC or the Government and NHS websites for latest UK guidance. There is a daily update from the UK Government every evening.
 In response to latest guidance, we have taken the following steps:
 For detailed updates, see our separate student, staff and public information pages. 
 We'd also like to reassure all applicants that the University has plans in place to support your application even without final exams.
 Find out more on how the University is supporting efforts to respond to the Covid-19 outbreak, including providing specialist equipment to the NHS and teaming up with local initiatives.
 Students and staff can also help Government efforts to build up additional data on the outbreak via the new NHS Coronavirus Status Checker
 For detailed information on what this may mean for you, please see the separate Staff, Student, Applicant and General Public information listed below
 Support and advice for students on studying from home, help with safety and wellbeing and what is available on our campuses
 Guidance for staff on working from home and changes to how the University is running for the foreseeable future
 What to do if you have made an application to study at Kent and want to know what happens next
 https://www.kent.ac.uk/coronavirus
