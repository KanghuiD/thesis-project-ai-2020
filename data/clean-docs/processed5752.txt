Lockdowns are the best hope to slow the spread of coronavirus, suggests new Chinese study | Daily Mail Online
 By Mary Kekatos Senior Health Reporter For Dailymail.com 
  Published:  20:51 BST, 25 March 2020   |  Updated:  04:14 BST, 26 March 2020   
  40
 Lockdowns and travel restrictions are the best methods to slow the spread of coronavirus, a new study of China suggests.
 Researchers from the University of Oxford in the UK and Northeastern University in Boston, Massachusetts, found that in Wuhan - the epicenter of the outbreak - restriction on travels came too late.
 Once the locked did occur, cases outside of the province linked to Wuhan fell 92 percent from 515 to 39.
 But outside of the Hubei province, where Wuhan is located, those that restricted travel and focus on testing and tracking cases did best at contain outbreaks. 
 Wuhan did not impose travel restrictions until January 31. Before that date, of all cases reported outside Hubei, 515 of them had a known travel history to Wuhan. Pictured: A staff member sprays disinfectant at Wuhan Railway Station, March 24, left; and a staff member disinfects an emergency room at Huayuanshan branch of Hubei Provincial Hospital of Traditional Chinese Medicine in Wuhan, March 15
 But, after January 31, just 39 cases had a travel history to Wuhan and symptom onset before that date. Pictured: A CDC mesage reminds travelers to wash their hands at Ronald Reagan Washington National Airport in Arlington, Virginia, March 25
 On January 31, Wuhan went into lockdown with no one allowed to leave the city either by plane or bus.
 However, it was more than four weeks after the virus was first detected and too late as China was experiencing the largest human migration in history due to the Lunar New Year.
 Other cities acted much sooner and were able to control the spread much more rapidly.
 The authors say that because the average incubation period is as little as five days, the benefits of travel restrictions were not seen for at least a week after the lockdown.
 Before January 31, of all cases reported outside Hubei, 515 of them had a known travel history to Wuhan and their symptoms began before the lockdown.
 But, after January 31, just 39 cases had a travel history to Wuhan and symptom onset before that date.  
 The researchers say, this demonstrates the effect of travel restrictions in decreasing the spread to other Chinese provinces.   
 'Our findings show that early in the coronavirus outbreak travel restrictions were effective in preventing the import of infections from a known source,' said Dr Moritz Kraemer, a research fellow at the University of Oxford. 
 'However, once COVID-19 cases begin spreading locally the contribution of new importations was much smaller. 
 Researchers say this is an important lesson for the US, as it becomes the epicenter, that early travel restrictions prevent the spread of infections. Pictured: Sparse crowds make their way to a check-in counter at the United terminal at Los Angeles International Airport, March 24
 In the US, there are more than 59,000 confirmed cases of the virus and more than 800 deaths. Pictured: Paramedics move a patient into the hospital during the outbreak of coronavirus in Manhattan, New York, March 25
 'This is where a full package of measures including local mobility restrictions, testing, tracing and isolation need to work together to mitigate the epidemic. '
 However, Dr Kraemer warned that Chinese provinces and other countries that have restricted restrictions should 'carefully' manage how they reinstate travel. 
 'The political will in many countries is lagging behind the spread of COVID-19,' said Dr Samuel Scarpino of the Network Science Institute (NetSI) at Northeastern University.  
 'Travel and mobility restrictions are the most useful right at the start, when local transmission has not yet become a factor. After transmission is established, physical distancing and the quarantine of sick individuals will work, but it takes time.'
 Published by Associated Newspapers Ltd
 Part of the Daily Mail, The Mail on Sunday & Metro Media Group
 https://www.dailymail.co.uk/health/article-8152871/Lockdowns-best-hope-slow-spread-coronavirus-suggests-new-Chinese-study.html
