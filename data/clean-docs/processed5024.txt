Coronavirus: Containment should be Indonesia's priority, says ex-finance minister
 Keep Me Logged In
 Getting the coronavirus outbreak under control should be the top priority for the Indonesian government, the country's former finance minister said on Friday.
 Indonesia has recorded 170 deaths related to COVID-19, the highest toll in Southeast Asia, according to data compiled by Johns Hopkins University. Confirmed cases in the country stand at 1,790 — fewer than Malaysia, the Philippines and Thailand, the data showed.
 "It's quite clear that getting outbreak under control should be the first priority of the government because containment of the virus will be the key of the recovery," Muhamad Chatib Basri, chairman of think tank Mandiri Institute, told CNBC's "Squawk Box Asia."
 "That's the reason why the government needs to act urgently, immediately," said Basri, who was Indonesia's finance minister from 2013 to 2014 under President Susilo Bambang Yudhoyono. 
 Some experts said a lack of testing could have masked the scale of the outbreak in Indonesia, the fourth most populous country in the world. Others said the government should impose stricter movement controls or even a full lockdown to manage to the situation. 
 Indonesian President Joko Widodo has announced "large-scale" social distancing measures to stem the spread of the virus, while capital city Jakarta — where most infections were found — has declared a state of emergency, reported Reuters.
 The country's Finance Minister Sri Mulyani Indrawati warned that the coronavirus outbreak could bring the Indonesian economy — Southeast Asia's largest — to a halt. She said annual growth could fall to between 0% and 2.5%, much slower than last year's 5.02%, according to a Reuters report.
 The Indonesian government has allocated 405 trillion rupiah ($24.5 billion) in additional spending to support the economy — a move that would expand the fiscal deficit in 2020 to 5.07% of gross domestic product, reported Reuters.
 Got a confidential news tip? We want to hear from you.
 Data is a real-time snapshot *Data is delayed at least 15 minutes. Global Business and Financial News, Stock Quotes, and Market Data and Analysis.
 Data also provided by 
 https://www.cnbc.com/2020/04/03/coronavirus-containment-should-be-indonesias-priority-says-ex-finance-minister.html
