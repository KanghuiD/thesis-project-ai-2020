Football: Will coronavirus have as bad an effect on football as the 2008 financial crisis? | MARCA in English
 Editions:
 En/football/spanish-football
 Rodri Baster, founder and director of football as Promoesport (a football agency with more than 350 representatives), analyses the impact of the coronavirus on football for MARCA.
 There are many unknowns that exist about the impact of the coronavirus epidemic on the football industry. Leaving aside the damaging health consequences that this virus is generating, if we solely focus on the economic side of football, I think here are reasons for optimism. 
 I founded Promoesport in 2002 and since then I've seen first hand as a football agent the dark age of Spanish football, just as the radical improvement in the organisation of the football industry in this country.
 Therefore, I think there are reasons to think that clubs are going to recover better and quicker than they did after the financial crisis of 2008.
 The big difference is that, when the 2008 crisis came about, the clubs were already in lots of debt, the management of players was very weak and many of the league's problems were managed in a collective manner. The current situation is completely different. 
 The strict control mechanisms of debts in LaLiga, added to a single management and some increased TV rights and a good distribution between all the clubs, we're in a better situation now than before the 2008 crisis.
 Obviously, there will be a certain financial impact. But the big difference will be determined by whether the season is completed or not. 
 If it gets finished, the losses could be between five and 10 percent, which are perfectly easy to make up again in the next few years. 
 If the season isn't finished, the impact could be between 25 and 30 percent - a proportion directly related to the part of the season that wouldn't be played and wouldn't bring in TV rights, sponsor money and other things.
 Some possible solutions to cushion the blow if the season doesn't get completed is reducing wages in exchange for pay rises in the future, putting in protectionist measures for the sector, like not paying income tax during the months of confinement and finding new, imaginative ways that allow future additional sources of income in situations like now.
 2020
  Unidad Editorial Información Deportiva, S.L.U. Todos los derechos reservados.
 Follow us
 https://www.marca.com/en/football/spanish-football/opinion/2020/03/27/5e7de0ea46163f882c8b45c4.html
