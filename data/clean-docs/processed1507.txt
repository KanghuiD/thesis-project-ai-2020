Opinion - Coronavirus a Challenge for China’s Economy
 The novel coronavirus (2019-nCoV) has now infected nearly 10,000 people worldwide, with over 200 mortalities reported as of January 31, 2020. The majority of cases have been in Wuhan, China, where it originated, though more are being identified across a dozen other countries.
 The World Health Organization held a second emergency committee meeting on
 January 30, 2020 and declared the coronavirus an international global health emergency. The epidemic is likely to negatively impact China’s economy and regional trading partners, at least in the short term. Depending on the international spread of the virus, other countries could also see a measurable impact on consumer demand.
 The virus has been compared to the SARS (Severe Acute Respiratory Syndrome) epidemic in 2003, due to its nature and
 geographic origins. The impact of SARS on China also provides a comparison that can help to better understand the potential
 macroeconomic impact on China (A (high), Negative trend) and other countries. Over the last week, the number of confirmed cases reported to the WHO has increased almost tenfold, suggesting a much more rapid spread than SARS, though
 the reported mortality rate remains relatively low at this early stage. The Chinese response to the coronavirus has been quite
 unlike the official response to the 2002-2003 SARS contagion that according to WHO data infected over 8000 people over
 the course of 4-5 months and caused nearly 800 deaths in at least 17 countries. Having courted blame then for a slow
 response and secrecy, the central government has been taking unprecedented steps to contain the outbreak, with the Standing
 Committee of the Chinese Communist Party – the country’s top political body, headed by President Xi Jinping – taking
 direct control of operations.
 Measures include locking down 16 cities (effectively restricting the movement of at least 60 million people) to contain the
 new coronavirus outbreak. Nationwide, many tourist attractions have closed, while foreign governments are actively repatriating their citizens. Restaurants and cinemas have also suspended operations either voluntarily or upon instructions. Trying
 to temporarily limit travel, the Chinese government extended the weeklong Lunar New Year holiday by three days. Workers
 are now scheduled to return to work on February 3.
 Implications for China and the Global Economy
 While it is still too early to ascertain the economic impact of the virus on China, given the speed at which the infection has
 spread, the impact is likely to be felt most in the first quarter. Consumption, especially retail sales, is likely to be affected
 as people across the nation limit their activities outside the homes. To a lesser extent, production will also likely be affected
 temporarily by the extension of the holidays, as well as by possible subsequent precautionary workplace measures to contain
 the virus. The spread of the coronavirus has prompted companies to limit their travel to China, which will affect hotels,
 restaurants and transportation. Hong Kong and other regional hubs for tourism (e.g., Thailand, Macau) are likely to see an
 even larger impact.
 China’s increasing share of the global economy coupled with its growing integration in global supply chains means a slowdown in China stemming from the virus could have a larger spillover than in the past. Back in 2003, when SARS hit the
 Chinese economy, the global fallout was limited. The country’s weight in global growth at that time was a modest 4%,
 compared with the 17% share of global GDP today. Fears surrounding the outbreak may cause a behavioral shift and impact
 travel and tourism globally. Chinese tourists have driven sustained growth in travel across Asia, having increased from 2%
 of the total number of tourists in 2002 to 9% in 2017. Global central bankers have also voiced their concern: the Federal
 Reserve in its January meeting stated that the coronavirus outbreak posed a risk to its economic outlook for the U.S. in the
 short-term, via a China slowdown that could spill over to its trading partners.
 China Has Policy Space to Fuel a Rebound
 In addition to measures to contain the virus, monetary and fiscal policy measures are likely to be used as necessary to
 provide liquidity and credit support to mitigate any lasting impact on growth. Earlier this week, the People’s Bank of China
 (PBoC) announced that in anticipation of an incoming liquidity shortage, it would provide sufficient liquidity support to
 support banks and businesses which were negatively affected. On the fiscal front, authorities could announce a bigger 2020
 budget deficit in March, if the cost of fighting the epidemic is substantial. Even with strict containment measures and scope
 for monetary and fiscal response in China, DBRS Morningstar expects a negative pressure on growth in the current quarter.
 If there is an effective policy response, the economic effects are likely to be contained to 1-2 quarters. However, it is too
 early to tell how quickly and how long the virus will continue to spread. DBRS Morningstar continues to monitor the
 Coronavirus situation for potential impact on its global sovereign ratings.
 Rohini Malkani
 	, 
 		Thomas R. Torgerson
 ,  
 								February 2
 								
 ‘Smart beta’ sounds like an oxymoron. How smart can it be to continue using the same strategy in such fickle markets? A portfolio manager calling on all his skills (‘alpha’) in analysing market environments (the source of ‘beta’) should be able to outperform an unchanged (...)
 https://www.next-finance.net/Coronavirus-a-Challenge-for-China
