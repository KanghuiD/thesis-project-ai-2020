EEOC Continues to Serve the Public During COVID-19 Crisis
 U.S. Equal EmploymentOpportunity Commission
 CONNECT WITH US 
     
 About EEOC
 Overview
 The Commission & the General Counsel
 Commission Votes
 Meetings of the Commission
 Open Government
 Newsroom
 Laws, Regulations, Guidance & MOUs
 Budget & Performance
 Enforcement & Litigation
 Initiatives
 Task Forces
 Interagency Programs
 Publications
 Statistics
 Outreach & Education
 Legislative Affairs
 FOIA
 Privacy
 Doing Business with EEOC
 Jobs & Internships
 EEOC History
 Office of Inspector General
      
   
 The U.S. Equal Employment Opportunity Commission (EEOC) wants you to know that we are continuing to enforce the nation's employment non-discrimination laws while ensuring that all of our activities are consistent with public health
 guidelines.
 The EEOC has closed its physical offices to the public and implemented agency-wide expanded telework. But our work continues remotely, across the private and federal sectors, and in our efforts educate the public about their workplace rights and
 responsibilities. The EEOC's private sector litigation continues, in accordance with the rules of the courts where the cases are filed.
 Private Sector: Preserving access to file a charge of discrimination is important because the laws the EEOC enforces have deadlines within which individuals must file discrimination charges. To begin the process of filing a
 charge of discrimination, employees and applicants are encouraged to visit the EEOC Public Portal to schedule an intake appointment by telephone. The system can also be accessed by going directly to our
 website at www.eeoc.gov. 
 Anyone who cannot use the portal can call the EEOC at 1-800-669-4000 to begin the process of filing a charge. 
 At the end of an investigation the EEOC generally issues a Notice of Right to Sue (Notice) to charging parties. Once you have received a Notice, you must file your lawsuit within 90 days. This deadline is set by law and cannot be changed by the
 EEOC. If you do not file in time you may be prevented from going forward with your lawsuit.  Recognizing that during the pandemic there are many issues that may prevent charging parties from being able to protect and exercise their rights,
 beginning March 21, 2020, the EEOC temporarily suspended the issuance of charge closure documents unless a charging party requests them. 
 Employers are encouraged to utilize the EEOC Public Portal to interact with the agency.
 Information about reaching an EEOC field office is available at https://www.eeoc.gov/field/index.cfm.
 Federal Sector:  The EEOC continues to provide leadership and guidance to federal agencies on all aspects of the federal government's equal employment opportunity program, while also working with parties where requests for
 hearings on EEO complaints have been filed, and adjudicating appeals from administrative decisions made by federal agencies on EEO complaints.
 The EEOC issued instructions regarding the processing of federal sector EEO complaints covered by 29 CFR Part 1614 during the pandemic. Included in these
 instructions the EEOC instructed agencies and employees to continue to process EEO complaints in a timely manner that will best preserve the legal rights of the parties involved, unless doing so would interfere with mission-critical operations for
 an agency.  Recognizing the need to preserve parties' rights during the pandemic, the EEOC has also asked agencies not to issue final actions on any EEO complaint, unless the investigation is complete, and the Complainant has requested that the
 final action be issued. The Commission is also suspending issuance of all appellate decisions via the U. S. mail until further notice. 
 We encourage employees in the federal sector EEO process to file requests for hearings and appeals
 electronically through the EEOC Public Portal, and agencies use the FedSep portal to send documents and other materials to the EEOC.
 To access your appeal online and allow the EEOC's Office of Federal Operations to adjudicate your appeal, you first need to register with the EEOC Public Portal.
 To help you easily use the Portal, we've prepared a User Guide for federal employees with appeals.
 Education and Outreach: The EEOC has a robust outreach and education program in place to reach employees, applicants, and employers about the laws we enforce. While trainings and conferences are on hold because of the coronavirus
 pandemic, the EEOC continues to provide information through our website, social media, the EEOC Training
 Institute, and our Outreach and Education Coordinators nationwide.
 The EEOC recently made COVID-19-specific publications available on our website, including the "What You Should Know About the ADA, the Rehabilitation
 Act, and COVID-19" and "Pandemic Preparedness in the Workplace and the Americans with Disabilities Act". The EEOC last updated the COVID-19 "What You Should Know" on Thursday, March 19,
 2020, and the Pandemic Preparedness document on COVID-19 on Saturday, March 21, 2020.
 The EEOC also posted a pre-recorded, online webinar addressing questions involving the intersection of the Federal equal employment opportunity laws and the COVID-19 pandemic.
 Individuals who are deaf or hard-of-hearing can reach the EEOC by videophone at 1-844-234-5122. If you have a disability which prevents you from accessing the Public Portal or you otherwise have difficulty with accessing the portal, please call
 1-800-669-4000.
 The EEOC will continue assessing the effects of the coronavirus pandemic on our activities and will provide updates to the public as soon as possible.
 https://www1.eeoc.gov/eeoc/newsroom/release/eeoc-continues-to-serve.cfm
