Coronavirus: Lockdown 'could boost wild flowers' - BBC News
 A plant charity is predicting a boost for wild flowers because some councils have stopped mowing verges and parks during the Covid-19 crisis.
 Plantlife has been urging councils for years to cut grass less often.
 It also wants them to delay cutting until flowers have had chance to seed.
 The charity says it has seen a shift in attitudes in recent years, but some councillors still say their citizens prefer neatly-manicured lawns and verges.
 Now Plantlife’s preliminary research suggests that municipal mowing has been among the first activities to be cut under the crisis.
 That’s partly because staff are sick or self-isolating, and partly to save money as budgets are squeezed.
 Plantlife’s Trevor Dines told BBC News that an upsurge in public support for wild flower verges had already persuaded some authorities to restrict cutting.
 Emptying bins
 “Obviously we’re extremely worried about the Covid crisis and want it to end as quickly as possible. But if councils do change their methods because of the crisis, they might find it wins public support, which would be good for the future.”
 Among the councils registering changes due to the Covid crisis are:
 Plantlife wants councils to delay cutting until the end of August or the start of September until after plants have seeded. 
 Meanwhile, the reduction of traffic during the Covid-19 crisis will produce another benefit for wild flowers.
 Typically, roadside verges are drenched with nitrogen emissions from vehicle exhausts. This fertilises the hardier species in the plant world, which can harness the nitrogen to grow and out-compete more delicate wild flowers. 
 Mr Dines said: “There has been a phenomenal change in the quality of air – we can see so much more clearly into the distance. The lack of pollutants is going to help wildflowers on verges.”
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/science-environment-52215273
