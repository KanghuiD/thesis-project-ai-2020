CDC recommends masks, U.S. deaths rise by more than 1,000 in one day
 
 The Centers for Disease Control and Prevention on Friday recommended that people wear "cloth face coverings," in places where it is hard to maintain social distancing — like grocery stores. Officials say surgical masks or respirators should be reserved for health care workers.
 President Donald Trump announced the recommendations but said he is choosing not to wear one.
 The U.S. recorded more than 1,000 deaths between Thursday and Friday, according to NBC News' tally. As of Friday night, more than 7,000 U.S. deaths have been linked to the disease. Globally, the death toll is more than 59,100, according to Johns Hopkins University.
 The U.S. economy lost 701,000 jobs in March, according to data released Friday by the Bureau of Labor Statistics. The unemployment rate soared to 4.4 percent from 3.5 percent.
 Support on Capitol Hill among both Republicans and Democrats for an independent 9/11-style commission to investigate the country’s response to the outbreak appeared to be growing.
 Full coverage of the coronavirus outbreak
 Download the NBC News app for latest updates on the coronavirus outbreak.
 This live coverage has ended. Continue reading April 4 coronavirus news.
 Amy Calvin
 As long-term care facilities become a growing concern during suffer coronavirus outbreaks, 67 patients and eight staff members at one San Antonio center have been infected.
 Mayor Ron Nirenberg on Friday prohibited nursing home staffers from working in multiple facilities in an attempt to slow the virus' spread. Southeast Nursing and Rehabilitation Center has had one resident die, and eight of its staff members have the virus, according to city and county officials.
 Sixty-seven residents of 84 have coronavirus, the mayor said. "Seniors are some of our most vulnerable residents," Nirenberg said in a statement.
 Texas Gov. Greg Abbott had been reluctant to use the most common tool to fight the virus, stay-at-home social distancing, but relented this week, saying Friday, "The best thing Texans can do to help maintain hospital capacity is to stay at home."
 SOUTHEAST NURSING & REHABILITATION:75 total people in connection with the Southeast Nursing and Rehabilitation facility have tested positive, including 59 residents yesterday.1 of the facility’s residents is included among the 9 we’ve lost to COVID-19...4/14
 Alicia Victoria Lozano
 U.S. Attorney General William Barr is calling for the release of eligible inmates at federal prisons hardest hit by the coronavirus outbreak. 
 In a memo to the Bureau of Prisons director, Barr said he wants to speed up the process of sending select federal inmates in Connecticut, Louisiana and Ohio to home confinement because of the threat the outbreak poses at those facilities for elderly prisoners and those with pre-existing health conditions.
 "While the [Bureau of Prisons] has taken extensive precautions to prevent COVID-19 from entering its facilities and infecting our inmates, those precautions, like any precautions, have not been perfectly successful at all institutions," Barr wrote in the memo. 
 Inmates who are identified as eligible for home confinement should be processed immediately and transferred following a 14-day quarantine at an appropriate facility, Barr said. 
 "It is vital that we not inadvertently contribute to the spread of COVID-19 by transferring inmates from our facilities," he wrote. "Given the speed with which this disease has spread through the general public, it is clear that time is of the essence. " 
 Half will go to Philadelphia's Temple University Hospital Emergency Fund, and the other $500,000 will go to Los Angeles' emergency fund, she said. Pink's mother worked at Temple for 18 years, according to the singer.
 "It is an absolute travesty and failure of our government to not making testing more widely available," she said in a tweet. "The illness is serious and real."
 Phil Helsel
 As of Friday night, more than 7,000 deaths in the United States have been linked to the coronavirus disease COVID-19, according to an NBC News count. Overall, the country has more than 275,500 cases.
 In New York City, which is currently considered the epicenter of the outbreak in the country, deaths rose by 305 and reached 1,867 as of 5 p.m., according to numbers from the city's health department. New York state overall has had more than 102,800 cases with more than 2,900 deaths, according to the NBC News count.
 More deaths were also reported in many other states, including more than 100 in New Jersey, where 646 people have died according to the state health department. Michigan has had 479 deaths as of Friday, and Louisiana has had 370 deaths reported, according to health departments.
 The count of more than 275,500 positive COVID-19 cases across the U.S. by Friday night reflects an increase of more than 30,000 cases from Thursday night, according to NBC News' tally. The count of coronavirus cases includes active cases, those who have recovered, those who have died and those who have been repatriated to the U.S.
 The Associated Press
 ISLAMABAD — Mosques were allowed to remain open in Pakistan on Friday, when Muslims gather for weekly prayers, even as the coronavirus pandemic spread and much of the country had shut down.
 Prime Minister Imran Khan is relying on restricting the size of congregations attending mosques and advice to stay at home from religious groups like the country’s Islamic Ideology Council. However, some provinces have issued their own lockdown orders to prevent Muslims from gathering for Friday prayers. 
 Still, mosques remain open in Pakistan, even as they have been shut down across much of the Middle East and elsewhere. The Middle East has confirmed over 85,000 cases of the virus and over 3,700 deaths, most of them in Iran. 
 Pakistan, with 2,450 confirmed cases and 36 deaths, has been sharply criticized for moving too slow to curb large gatherings, including a gathering of tens of thousands of Muslims from several Islamic countries in March. The gathering of Tableeghi Jamaat missionaries is blamed for several outbreaks of the new virus elsewhere in the world. 
 The pandemic will cost the global economy as much as $4.1 trillion, or nearly 5 percent of all economic activity, according to new estimates from the Asian Development Bank.
 The head of the International Monetary Fund said the recession sparked by the coronavirus pandemic is “way worse” than the 2008 global recession. At a news briefing in Geneva on Friday, IMF managing director Kristalina Georgieva described the situation as “a crisis like no other.”
 Los Angeles County on Friday opened a new drive-up testing site in the parking lot of a popular mall. 
 Tests are available by appointment only and limited to people with symptoms who are in high risk categories, including those over the age of 65 and people with underlying health problems.
 The new site, located at the South Bay Galleria in Redondo Beach, is one of 10 mobile testing locations in the county.
 HELENA, Mont. - Montana's June 2 primary will be conducted by mail in an effort to limit the spread of the coronavirus. The ballots will be mailed out on May 8.
 The U.S. Postal Service recommends ballots be mailed back a week before the election. Same-day registration and in-person voting will still be allowed. Counties will set the locations for late registration and ballot submission.
 County clerks say a new law that allows counties to begin opening mailed ballots on the Thursday before Election Day to prepare them for counting should lead to quicker results on election nights.
 https://www.nbcnews.com/health/health-news/live-blog/2020-04-03-coronavirus-news-n1175641