Drs. Frieden and Shahpar: Coronavirus testing 1, 2, 3 – it's time for plain talk on what's next | Fox News
       This material may not be published, broadcast, rewritten,
       All market data delayed 20 minutes.
     
 Nasal swab samples are mailed in for results; insight from Fox News medical contributor Dr. Marc Siegel.
 Get all the latest news on coronavirus and more delivered daily to your inbox.  Sign up here.
 Our initiative, Resolve to Save Lives, released a brief Monday on how to prioritize testing for COVID-19 in the United States.
 It’s time for plain talk on testing.
 Plain fact one: 
 We have nowhere near enough tests, and it’s not clear how many week or months away we are from having them. Facts are stubborn things, and so is math.
 ROBERT GARRETT: CORONAVIRUS – 6 LESSONS FROM A NEW JERSEY HOSPITAL SYSTEM ON THE FRONT LINES
 Ideally, we would test everyone in the country with symptoms of COVID-19, every one of their contacts, every hospitalized patient, and many people in places such as nursing homes, homeless shelters, prisons, and jails where the virus can spread explosively and kill quickly. That would be over a million tests a day, more than four times more than we are doing now. And that’s without even considering testing others at risk, such as health care and other essential workers who don’t have symptoms. That would require millions of more tests per day.
 Plain fact two: 
 Until we have enough tests, we have to prioritize. People who need to be managed carefully and safely in hospitals – such as those older than 60 and people with diabetes, lung or heart disease – need to be tested so they can be cared for more intensively, something that could save their life.
 Residents and staff of nursing homes, homeless shelters, prisons and jails who have symptoms must be tested and isolated to prevent explosive spread. Health care workers with symptoms must be tested so we can better protect our front-line heroes and keep our hospitals safe.
 Contacts of infected people need to be tested so we can box the virus in through testing, isolation, contact tracing and quarantine.
 And ideally, every hospitalized patient and every resident admitted to a nursing home would be tested to prevent spread.
 Testing just those groups is probably twice as many tests as we are doing today.
 Plain fact three: 
 No matter the reason tests are still scarce, we need to deal with it and do the best we can. In New York City over the past few weeks, we have simply assumed that people with cough and fever have the infection and encouraged them to stay home and not get tested if they are only mildly ill.
 There’s a lot that everyone can do until we have enough tests. Stay home. Wear a mask when out. Wash your hands often. Try to avoid touching your face when your hands might have been contaminated. 
 There’s a lot that everyone can do until we have enough tests. Stay home. Wear a mask when out: this protects others because you might have and spread the infection without knowing it. When others wear a mask, they are protecting you. Wash your hands often. Try to avoid touching your face when your hands might have been contaminated.
 Work with communities to ramp up contact tracing in order to support patients and contacts to limit the spread of the virus. Isolate the infected and quarantine the exposed so the virus stops with them. Protect our nursing homes and health care facilities by stopping visits, keeping sub-groups together, finding infections quickly and stopping outbreaks there before they spread.
 In our report, we address testing for the virus itself, currently done by a method called a polymerase chain reaction, or PCR. Testing for antibodies – which may reflect either current or prior infection with the virus – is different. We do a deep dive into testing in Monday’s edition of our weekly science briefing, for those who want to know more.
 We’re not certain the priority list we’ve suggested is the right one. Effective leadership through the COVID-19 pandemic means shifting advice to reflect the best information we have, when we have it. However the list evolves — some groups might need tests less urgently, other groups might quickly need to be prioritized — only by prioritizing can we make optimal use of the test capacity we have.
 We can get out into the new normal as soon and as safely as possible if we recognize that there isn’t one simple fix. The virus is a dangerous opponent. We have one strong tool against it – staying home. But we don’t want to and can’t do that forever. So we need to do whatever works, and do it as effectively as possible.
 That means being smart about testing as we scale up capacity. It means being safer as we resume some activities carefully – hand sanitizer, physical distancing, face masks and more. It means investing in and collaborating with public health and contact tracing.
 And it means recognizing that we’re all in this together. We’ll get through together by facing facts, acting accordingly and being creative about getting back to as much of our lives as soon and safely as possible.
 Dr. Cyrus Shahpar is director of the Prevent Epidemics Team at Resolve to Save Lives. 
 https://www.foxnews.com/opinion/coronavirus-testing-plain-talk-whats-next-drs-tom-frieden-cyrus-shahpar
