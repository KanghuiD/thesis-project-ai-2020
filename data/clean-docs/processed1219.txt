Exporting the coronavirus is reckless - The Boston Globe
 The US Immigration and Customs Enforcement agency knows there’s a coronavirus problem in its detention facilities, and that hundreds of detainees have already tested positive for COVID-19. To continue to deport immigrants right now, sending them to vulnerable places like Guatemala, Haiti, and Ecuador, is unconscionable. Such deportations show complete disregard for the health of millions of people in nations whose health care systems are already struggling, and it risks boomeranging on Americans by seeding the contagion in neighboring countries.
 In normal times, the US government deports foreign nationals for a range of reasons (a list that has expanded greatly under the Trump administration). But the administration has plenty of discretion to limit deportations during a crisis such as a global pandemic.
 The continued deportations are especially hypocritical for a White House that has used the threat of spreading coronavirus to crack down on immigration — for instance, by suspending the issuance of some green cards. But by continuing deportation flights, it’s the Trump administration that risks spreading the disease across international borders.
 As of Sunday, about 100 recent Guatemalan deportees had tested positive for COVID-19, a figure that represents about 20 percent of all known coronavirus cases in Guatemala. In two recent US deportation flights to the Central American nation, between 60 and 75 percent of migrants tested positive. It’s why, a little over a week ago, Guatemala’s president, Alejandro Giammattei, temporarily halted incoming flights with deportees from the United States, but it’s not clear how much political leverage he has to maintain the suspension. In a frantic attempt at stopping contagion, residents of a Guatemalan town threatened to burn alive a group of recent deportees who had tested positive for the coronavirus, according to a report from Telemundo. 
 Haiti is an extreme example of a nation that is in no position to handle an influx of coronavirus patients. There are only 100 ventilators in this country of 11 million residents and about 0.2 physicians per 1,000 people on average (in contrast with the US average of 2.56 doctors per 1,000). Haiti also suffers from the highest rate of tuberculosis in the Western hemisphere. Although Haiti has largely avoided coronavirus thus far, American deportations could change that. At least three deportees to Haiti have already tested positive, yet ICE deported 129 Haitians last Thursday; the deportees were promptly put into a 14-day quarantine in government-designated hotels.
 The US government can’t plead ignorance to the danger it is exporting. According to ICE’s latest figures, 425 immigrants under its custody in detention centers across the country have tested positive for the virus. (As of Tuesday, the agency had tested at total of 705 detainees. That represents about 2.2 percent of ICE’s total detainee population of 32,000 immigrants.) Immigrants in ICE detention are among the groups most vulnerable to infection, one reason several US federal courts have had to step in and order the federal agency to release some low-risk and sick detainees. A new study estimates that between 72 and 90 percent of detainees in ICE facilities across the country could become infected in the next three months if ICE does not take decisive action to mitigate the spread of the virus, such as by reducing jail crowding through the release of detainees vulnerable to the virus or those who pose no safety risk if freed. Otherwise, it would severely strain hospital capacity if COVID-19 infections are allowed to spread inside those facilities, which would put entire communities at risk.
 In a welcome but grossly overdue policy, ICE announced last week that it will start testing some migrants before deporting them. But if tests are too scarce to test everyone, the government should reduce deportations accordingly. This doesn’t mean releasing people who ought to be deported into American communities, but it does mean using greater scrutiny about who should stay in custody for now or be otherwise accommodated. The agency should not be sending anyone back to their home countries unless it’s certain they won’t be putting some of the most vulnerable parts of the world at even greater risk.
 Digital Access
 Home Delivery
 Gift Subscriptions
 Log In
 Manage My Account
 Customer Service
 Help & FAQs
 Globe Newsroom
 Advertise
 View the ePaper
 Order Back Issues
 News in Education
 Search the Archives
 Terms of Purchase
 Work at Boston Globe Media
 https://www.bostonglobe.com/2020/04/29/opinion/exporting-coronavirus-is-reckless/
