CSL, banks drag ASX to fourth day of losses
 That's it for the blog today.
 I'll return tomorrow to finish off the week. Hope to see you all then.
 Scutty.
 Despite a strong performance on Wall Street and further gains in crude oil futures, Australian shares slid for a fourth session on Thursday, weighed down by weakness in banks and healthcare providers.
 The benchmark S&P/ASX 200 fell 4.1 points, or 0.1 per cent, at 5217.1, a disappointing outcome given it had been up more than 1 per cent in early trade.
 Having tumbled to a multi-year low exactly a month ago, some believe the inability of the local market to rally further suggests a period of consolidation may be in order.
 “While forceful monetary and fiscal intervention and a belief that the first wave of the pandemic is peaking have fuelled a strong rebound in risk assets in recent weeks, we would be wary of extrapolating the uptrend too much further for now,” said Paul O’Connor, head of multi-asset at Janus Henderson Investors.
 “We believe the next major battle for market sentiment will be fought over the outlook for the world economy in the second half of the year.”
 In his opinion, markets are yet to price in the full impact from coronavirus disruptions, limiting the potential for further near-term gains in equities.
 “The continued slide in consensus estimates of economic growth and corporate earning suggests that neither economists nor equity analysts have yet fully factored in the scale of the coronavirus slump,” Mr O’Connor said. “We believe that a period of market consolidation is more likely from here.”
 In complete contrast to performance seen on Wednesday, gains in the big miners and energy stocks helped to offset weakness in the banks and healthcare giant CSL.
 BHP Billiton rose 2.7 per cent to $29.75 while Rio Tinto added 1 per cent to $86.00. Newcrest Mining also enjoyed a strong session, lifting 3.9 per cent to $28.48 on stronger gold prices overnight.
 After plunging to fresh 21-year lows on Wednesday, a rebound in Brent crude futures helped Santos rally 6.8 per cent to $4.26. Woodside Petroleum added 2 per cent to $20.00.
 Offsetting those gains, all the big four banks closed lower, led by the NAB with a fall of 1.1 per cent to $15.72.
 Healthcare stocks were also pressured with CSL slumping 2 per cent to $306.67. Ramsay Healthcare slid 5.9 per cent to $60.52 after raising $1.2 billion in fresh capital while Cochlear eased 1.4 per cent to $179.34.
 REITs also underperformed with Vicinity Centres and Scentre Group both tumbling 3.4 per cent to $1.275 and $2.01 respectively.
 UBS banking analyst Jonathon Mott will be looking out for several things from the upcoming bank reporting season, including bad debt provisioning and the outlook for dividend distributions.
 On provisioning for bad debts, Mr Mott believes overlays of $1.0-$1.5 billion per bank for 1H20 “appear reasonable and are consistent with the numbers implied by current share prices”.
 For capital management, Mr Mott says it will be a bad look if banks pay dividends with one hand while raising capital though discounted dividend reinvestment plans with the other.
 “APRA stated it expects banks to seriously consider deferring decisions on dividends until the outlook is clearer. We believe this is prudent,” he said. “Bank dividends should never be viewed as an annuity, irrespective of the wishes of some retail investors.”
 Jio Platforms, a wholly owned unit of Reliance Industries, brings together Jio's digital apps, ecosystems and the wireless platform offered by telecommunications carrier, Reliance Jio Infocomm, under one umbrella, according to the Mumbai-based company. The deal values Jio Platforms at a pre-money enterprise value of about $US66 billion, the Indian company said.
 You can read more here. 
 Priceline Pharmacy owner API has suspended its interim dividend and warned it will consider closing stores due to a drop in foot traffic if landlords won't negotiate on rents in the face of COVID-19.
 "We have engaged in discussions with our landlords and in most instances those discussions are proving fruitful, but where they are not fruitful, we are not afraid to permanently close stores or clinics," API chief executive Richard Vincent said in a conference call at the company's half-year results on Thursday.
 Mr Vincent said Vicinity was the largest landlord across Australian Pharmaceutical Industries' (API's) more than 1500 sites but refused to be drawn on whether one landlord, in particular, had been slower to negotiate. "Our conversations have been unfolding very slowly. In a lot of cases, they [landlords] have said 'we'll get to you, or a particular store, in due course'."
 The company said it had decided to suspend its interim dividend until it knows more about the revenue implications of the pandemic. The company paid out 3.75 cent interim dividend in 2019 and a 4¢ final dividend. 
 API shares are under pressure, sliding 11.1 per cent to $1.00.
 Emma has more here. 
 Ramsay Healthcare shares have fallen sharply today following its successful institutional capital raising.
 RHC is currently off 5.8 per cent to $60.57. However, that’s still above the $56 price it issued new securities at, securing a cool $1.2 billion in the process.
 Having had time to process the announcement, the analyst community have generally lowered their price targets for the healthcare provider, including Citi Research who cut Ramsay to neutral having previously been buy-rated on the stock.
 “Combined with the capital raising, our FY20-22 EPS forecasts decline by 1 per cent, 28 per cent and 7 per cent respectively,” Citi told clients. “Our target price moves marginally to $69. Given this is 7 per cent [below Tuesday’s closing price] we move to neutral.”
 Elsewhere, Macquarie cut its price target on Ramsay to $67 from $71 while retaining an outperform rating. UBS also made a minor downgrade, cutting its price target by 30 cents to $63.20 with a neutral stance.
 Morgan Stanley’s wealth management team are slowly starting to warm on the prospects for Australia’s equity market.
 “One of the reasons why we had been lowering our allocation to Australian equities between late 2019 and early 2020 was the unsustainable valuations against falling earnings estimates,” they told clients. “With Australian equities having underperformed around 6 per cent since January 31, and now trading at around 12.7 times forward earnings, we believe they’re are more realistically priced for the challenging economic environment.
 While Morgan Stanley believes the tailwinds from a lower Aussie dollar are now largely over, it believes local equities now look far more attractive than other asset classes.
 “With the equity risk premium now standing in excess of 7 per cent, the value is especially attractive versus bonds,” it said.
 Especially with the prospect of continued policymaker support, both domestically and indirectly from China.
 “We believe Australian equities should perform better in the months ahead and we reduce our underweight,” it said.
 Citi Research says it's not just supermarkets that have benefited from social distancing measures in the retail space.
 “A number of retailers are seeing sales growth because households have limited alternatives in directing their discretionary spend,” Citi said in a note. “Auto, baby goods, hardware, electronics pharmacy, sporting goods are all seeing better sales growth.”
 Citi believes this reflects spending being redirected into other categories.
 “Discretionary non-retail spending, such as travel, entertainment and gambling, is $283 per week per household, and is slightly larger than non-food retail at $281 per week,” Citi said. “The savings households have because they cannot spend in non-retail are being partly directed to retail.”
 It expects big retailers will emerge the strongest from the current downturn, partially as a result of accelerated structural change.
 “Large retailers are likely to see accelerated market share gains with better access to capital and investment in IT infrastructure to support online,” Citi said.
 Citi has buy ratings on a variety of listed retailers, including Baby Bunting, Beacon Lighting, Harvey Norman and Super Retail Group.
 Morgan Stanley have lowered their price target for Nearmap despite remaining bullish towards the aerial mapper.
 “We view NEA's message of no cash burn or capital required as a positive for the stock, especially given rebased growth expectations,” the investment bank told clients. “Annualised Contract Value (ACV) growth targets of 20-40 per cent are no longer market consensus.”
 While the market is expecting slower growth, Morgan Stanley believes there’s scope for the company to beat FY21 consensus expectations when businesses are more focused on the ability to work remotely.
 It has a $2.00 price target on NEA with an overweight rating. Its previously was targeting a move to $2.30 per share.
 Australians experiencing financial distress due to the coronavirus crisis have made 456,000 applications to access their superannuation early at a total cost of $3.8 billion, the Treasurer says.
 The Tax Office had approved those applications and they were "now with the superannuation funds for their payment over the next five days," Treasurer Josh Frydenberg said.
 "The average withdrawal is around $8,000. And just to remind you that you can access up to $10,000 from your super this financial year and up to another $10,000 next financial year."
 Our coronavirus live page can be accessed here. 
 https://www.brisbanetimes.com.au/business/markets/asx-turns-negative-as-csl-and-banking-stocks-slide-20200423-p54mfx.html
