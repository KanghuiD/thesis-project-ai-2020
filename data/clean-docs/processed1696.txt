Coronavirus live blog, April 21: Tenn. Gov. Lee reiterates cooperation with urban mayors - The Daily Memphian
 A health care worker reminds patients to blow their noses before getting tested at Christ Community Health’s drive-thru COVID-19 testing site in Whitehaven April 13, 2020. (Patrick Lantrip/Daily Memphian)
 You can protect yourself and help prevent the spread of coronavirus by:
 Here’s the latest from Memphis and Shelby County, below, when it comes to dealing with the novel coronavirus. To view our full coverage, check out The Daily Memphian’s  coronavirus landing page.
 And, to get breaking news delivered directly to your inbox when it happens, opt in to our Breaking News updates here.
 Memphis City Council members Dr. Jeff Warren and Ford Canale are proposing that face masks be mandatory in public within the city of Memphis. 
 Warren served notice of the proposal Tuesday as the council planned for a special April 28 meeting to take up another unrelated matter.
 It wasn’t clear if the mask rule would be an ordinance or a resolution. And other council members indicated they will likely have a lot of questions about how to carry out such a requirement during the special online meeting that begins at 2 p.m.
                                             ~
 Bill Dries 
 The AutoZone Liberty Bowl announced late Tuesday that it is postponing this year’s Distinguished Citizen Award presentation and dinner honoring Hall of Fame quarterback Peyton Manning due to ongoing health concerns surrounding COVID-19. Manning will now be honored on June 6, 2021, at the Hilton Memphis Hotel.
 The AutoZone Liberty Bowl golf classic also was postponed and now will be played at TPC-Southwind on June 7, 2021.
 John Varlas 
 Tennessee Gov. Bill Lee said he spoke Tuesday with mayors from Shelby County and the state’s other large urban areas.
 “We reiterated our support to help these areas determine the best pathway for their reopening as well,” Lee said Tuesday during his daily press conference.
 Some restrictions will stay in place to ensure safety. The state will discourage social gatherings of 10 or more and will continue to restrict visitors at nursing homes and hospitals to protect patients and healthcare workers.
 Dr. Kimberly Lamar is heading a statewide health disparities task force to determine the impact on minorities to address challenges and needs to deal with the COVID-19 crisis.
 “We are listening and developing strategies to make sure your voices are heard,” Lamar said in Tuesday’s press conference.
 It will make sure data is solid and sites are set up to meet the needs of minorities.
 The University of Tennessee Health Science Center will be part of the effort, along with Meharry Medical Center.
 Sam Stockard 
 Gov. Bill Lee’s press briefing is scheduled to begin at 3 p.m.
 Watch below:
  The Daily Memphian Staff 
 The Memphis & Shelby County Film and Television Commission has established an emergency COVID-19 grant fund for local crew members. 
 Applicants for funding must be legal residents of Shelby County who make more than half of their income through film/television production work and who can prove an urgent financial need at least partly caused by COVID-19.
 The commission started a GoFundMe campaign, which so far has netted $5,350, in addition to another $500 received in the form of checks.
 “What better way for the professional staff to spend their time than helping the crew --while filming is temporarily halted!” said Film Commissioner Linn Sitler, in a statement. “Our crew has worked hard on projects small and big – projects like Netflix’s 'Uncorked,' Hallmark’s 'Christmas at Graceland,' and NBC-Universal’s 'Bluff City Law.' All have had a huge impact on the Memphis economy and Memphis tourism! Now’s the time for all of us to try to assist the local crew.”
 Gale Jones Carson, the commission’s chair, said she is hopeful the body can secure major grants for the fund.
 Join us at 4 p.m. today for a live Q&A with reporter and columnist Chris Herrington, who talks about the behind-the-scenes access that allowed The Daily Memphian to report how Baptist Memorial Hospital’s staff is dealing with the COVID-19 epidemic on a daily basis.
 Hosted by The Daily Memphian’s Eric Barnes, the discussion can be viewed on our live blog.
 Inside Baptist’s COVID-19 ICU: No visitors, IV stations in the halls and experimental treatments
 Mississippi State Department of Health reports 204 new confirmed coronavirus cases and 14 new deaths.
 That brings the total number of cases for the state to 4,716, with 183 deaths.
 DeSoto County has 239 confirmed cases, resulting in three deaths. The agency is also monitoring an outbreak at a long-term care facility there.
 Marshall County has 39 confirmed cases, resulting in two deaths. 
 Hinds County, home to Jackson, tops the state with 357 confirmed cases. Those cases have resulted in six deaths. Authorities are monitoring outbreaks at six long-term care facilities. 
 Lauderdale County has the most fatalities, at 17. It has 232 confirmed cases. Authorities are monitoring outbreaks at nine long-term care facilities. 
 Elle Perry 
 There are 156 more confirmed cases statewide and five additional deaths, since the Tennessee Department of Health’s update yesterday.
 That puts cases at 7,394 and deaths at 157. 
 Nearly 4,000 people -- 3,828 -- are considered officially recovered from the disease. And 760 people have been hospitalized. 
 The Department of Health reports 1,873 confirmed cases in Shelby County, up from the 1,857 the Shelby County Health Department reported earlier in the day.
 Nashville’s Metro Public Health Department reports 1,936 confirmed cases in Davidson County and 22 deaths. 
 An employee with the Shelby County Sheriff’s Office who tested positive for the coronavirus has died.
 The employee who worked with inmates at the men’s jail at 201 Poplar.
 According to the Sheriff’s Office, there have been 96 jail employees who have been tested for the virus and 31 tested positive.  Last week in an interview with The Daily Memphian, Sheriff Floyd Bonner said 25 employees were waiting on their tests results.
 Bonner said last week that employees and inmates were given masks and all employees were tested daily when they entered all the county’s correctional facilities.
 “These are trying times that none of us have seen before,” Bonner said. “We’ve only had five to test positive out of 1,700 inmates and that’s a testament to our medical providers, our staff working extremely hard and the health department,” Bonner said. “We’re doing all we can to keep everyone safe.”
 Yolanda Jones 
 The Shelby County Health Department is now investigating coronavirus clusters at nine long-term care facilities. 
 Three of those facilities have seen deaths related to the disease, according to the Health Department: four at Carriage Court Assisted Living Facility, four at The Village at Germantown and one at King’s Daughters and Sons Home. 
 Shelby County now has 1,857 confirmed cases of coronavirus, according to the Shelby County Health Department’s latest data.
 As of Tuesday, April 21, deaths due to complications from the virus now stand at 39. This is an increase of 50 cases and one additional death over the numbers reported yesterday. 
 A total of 657 people have recovered from the diseases in Shelby County, according to the state.
 There have been 19,816 total tests taken in Shelby County with a 9.4% positivity rate.
 In Tennessee, there are 7,238 cases with 152 deaths as of Monday, April 20, according to the state’s department of health.
 There have been 100,689 total tests statewide, with a positivity rate of 7.2%.
 Omer Yusuf 
 The House Democratic Caucus is questioning Gov. Bill Lee’s decision to reopen the state’s economy, saying he is putting people’s lives at risk by acting too soon and causing “confusion” by not including Memphis and Shelby County in a statewide economic reboot.
 As Lee lifts a “safer at home” order April 30 and allows “nonessential” businesses in 89 smaller counties to reopen as early as April 27, leaders in Shelby and other urban areas are supposed to come up with their own restart plans.
 “It our position that Tennessee lives should be the priority,” state Rep. Antonio Parkinson said in a Tuesday video press conference. “Tennessee Democrats are in favor of reopening the state but not at the peril of one single life in the state of Tennessee.”
 House members from Memphis are especially concerned that the governor’s strategy allowing some parts of the state to open earlier than Memphis will create a chaotic situation and possibly allow the virus to spread when people from other counties go in and out of Shelby.
 “Regardless of what part of the state or area of the state that you’re from, we’re one state, we have one economy, and we all should be treated with the same level of priority,” Parkinson said.
 “There’s been a history and culture of singling out cities like Memphis and Shelby and Nashville and Davidson County in our state. This further exacerbates that culture, and it’s not good for the state, especially given the counties of Shelby and Davidson are the biggest contributors to the coffers of the state. And that needs to be respected.”
 State Rep. London Lamar pointed out the number of positive cases continues to increase, especially in Memphis, even though the state has seen a decline in the percentage of increases over the last two weeks.
 Until Memphis starts to see the number of cases go down and healthcare workers have plenty of personal protective equipment should Shelby start moving toward reopening, said Lamar, a Memphis Democrat.
 Mayors in Shelby and Davidson counties initiated nonessential business shutdowns well before Gov. Lee did. Yet the Democratic representatives said they would rather see a uniform opening statewide.
 “People are going to be confused,” Lamar said. “There’s already a lot of information overload and a lot of misinformation out there.”
 Democrats also contend the Lee Administration has proved to be “incapable” of managing the state under the governor’s “safer at home” order, citing shortages of personal protective equipment, “insufficient and incomplete” testing for COVID-19 and failure to put together a comprehensive contact tracing plan.
 The governor is planning to lift the order on April 30 and allow nonessential businesses to resume operations in 89 counties with lower numbers of cases as early as Monday, April 27. Shelby and other larger counties with more cases are expected to set their own reopening strategies.
 Yet, Democrats say the process for getting Tennesseans unemployment and financially supporting small businesses has been “a complete failure.”
 The Tennessee Department of Labor and Workforce Development has received 324,000 new unemployment claims since March 16, but it has sent out claims for only about half of those, last week distributing 170,000 claims for payment totaling $160 million. Those included $600 payments to those filing claims through the federal Pandemic Unemployment Compensation program.
 The state’s JOBS4TN website also went down Monday, making it unavailable for the public until 2:30 p.m. while the state processed claims. The down time was caused by a vendor’s inability to handle the task before the start of the business day.
 The caucus says Tennessee needs to “get the basics right” while the state is shut down before it takes the next step to reopen.
 Gov. Lee and Republican leaders say it is time to restart the economy while maintaining social distancing measures for the safety of businesses and customers.
 Lt. Gov. Randy McNally said Monday the governor has been “flexible and data-focused” in his decisions and that the COVID-19 curve is flattening, minimizing the impact on the people’s health.
 “We are testing more Tennesseans than ever and that will continue,” McNally said in a statement. “We are now well-prepared for a cautious reopening and economic reboot. Our state has consistently balanced the health of our people with the health of our economy. That remains the case today as we prepare to re-establish Tennessee’s strong and structurally sound economy.”
 The state tested more than 11,000 people over last weekend at 19 pop-up sites in rural counties where people can receive COVID-19 tests even if they don’t show symptoms of the disease. Tests can be administered at every health department in the county through the week, and another testing drive is scheduled this weekend statewide.
 In addition, the state is increasing testing at its prisons after seeing hot spots, including 150 cases at Bledsoe Correctional Facility in East Tennessee.
 Methodist Le Bonheur Healthcare is using alternate sources to obtain necessary personal protective equipment that meets its standards. Since the hospital system is receiving an influx of donations, it also released guidelines on how to safely contribute.
 Methodist is collaborating with state and local health departments to receive Strategic National Stockpile distributions, has relied on its internal disaster reserve inventory and has partnered with local companies outside of the household industry that have converted factories. 
 The hospital system’s clinical engineering team created a face shield prototype and is producing 1,000 face shields per week. Mary Karen Dixon, a registered dietician at Methodist Olive Branch, adapted a pattern to make isolation gowns in-house from plastic sheeting; employees make 150 to 250 gowns per day. And at Methodist Le Bonheur Germantown, an operating room team has been sewing an N95 mask alternative.
 And people outside of the hospital have wanted to contribute. Methodist reports that some hospitals have been surprised with donations or are receiving donations that are in violation of COVID-19 visitation policies. 
 People who would like to donate are asked to complete this online form www.methodisthealth.org/communitygiving and wait to hear from a hospital employee.
 “With sincere gratitude, we would like to thank all of those who have already supported our work by making a generous donation of PPE, food or funds,” Michael Ugwueke, president and CEO of Methodist Le Bonheur Healthcare, in a statement. “Please know how much we deeply appreciate your support of our mission now more than ever.”
                                             The Daily Memphian Staff
 Explore Bike Share is extending free bike rentals for an additional month in response to the COVID-19 pandemic.
 The nonprofit bike share operator reports that nearly 500 first-time riders took advantage of the first month of free rides. It said that it uses daily sanitization protocols, but riders are asked to bring their own disinfectant wipe and hand sanitizer to clean bike handles and touch screens before checking out a bicycle.
 “While parks are closed to vehicular traffic, parks are not closed,”  Explore Bike Share Executive Director Anton Mack, said in a statement. “This is an invitation to explore Memphis in an incredibly unique way. Traverse a new bike path or route among our hundreds of miles of bike infrastructure. Our streets are welcoming your daily solo exercise.”
 Rentals can be accessed through the BCycle app, a payment kiosk or explorebikeshare.com.
 The NFIB Research Center released a survey Monday, April 20, on the small business loan programs. Small business owners were asked about the Paycheck Protection Program and the Economic Injury Disaster Loan on April 17, the day after the programs ran out of money.
 About 20% of submitted applications have been fully processed with funds deposited in the borrower’s account, but 80% of respondents said they are still waiting, and many do not know where they are in the application process.
 Most small business owners believe it will take beyond 2020 to recover from the economic impact of COVID-19, with only one-third of owners believing their community will get back to a normal level of economic activity by the end of the year. A quarter of owners believe it will not be until 2022 or later before the economy returns to normal.
 “Small businesses were prepared and ready to apply for these programs, the only financial support options for most, and it is very frustrating that the majority of these true small businesses haven’t received their loan yet,” Holly Wade, NFIB director of Research & Policy Analysis, said in a release. “Small businesses make up nearly half of the economy, and it’s crucial that their doors stay open.”
 State-specific data was not available, but Tennessee NFIB Director Jim Brown said the survey confirms what the state organization is hearing from members.
 “They’re waiting in line to get an SBA loan and too many of them are on the brink of financial failure if they don’t get the money soon,” he said.
 https://dailymemphian.com/article/13176/coronavirus-live-blog-april-21
