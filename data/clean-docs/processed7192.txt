Opinion | The Advocate | theadvocate.com
 There is no excuse for a Louisiana State Police trooper to use foul language in the course of doing or discussing his or her job. More disturbing is hearing a trooper claim to beat the life out of someone. Yet a short audio clip indicates that happened. 
 The Bayou Classic will return. 
 For reasons that still evade me four years into America’s great let’s-elect-Donald-Trump-and-see-what-happens experiment, there are those who see the president as someone who tells it like it is, a mountain of documented lies notwithstanding. 
 This week marks Lawsuit Abuse Awareness Week, observed by Louisiana Lawsuit Abuse Watch and other grassroots organizations across the country that support civil justice reform. 
 I appreciate your attempt to be seemingly reasonable in your editorial regarding the Affordable Care Act. However, you miss many important points. 
 Ordinarily, we’d say that a grand and meaningless political statement would be a waste of time and taxpayers’ money at the State Capitol. 
 This November, New Orleans residents can begin to turn the page on the failed tough-on-crime era that needlessly punished thousands of our neighbors, separated families, devastated communities, and drained public funds away from investments in hou…
 On behalf of the neighborhood greatly affected by the new heliport at LCMC/Children’s Hospital, this letter is in response to that of Dr. Stephen Hales’ letter of Sept. 24. 
 “We’ve got some difficult days ahead.” 
 It was a good feeling when during the last governor’s race debate all the candidates picked Mike Foster as one of their favorite governors. 
 The rich guy as "man of the people" wasn’t invented by Donald Trump. As an imagemaker, Mike Foster had him thoroughly beat. 
 I read with interest Sept. 29 your lead story about the special session and how the leadership of the Legislature wants to exercise some control over the emergency declaration process that is currently by law in the hands of the governor, head of …
 As the whole world is racing to find a vaccine for COVID-19, it has become clearer than ever that we live in a global society that is incredibly interconnected. We all seem to understand that for one nation to completely overcome this pandemic, we…
 Go-Cups Return 
 During the 1960 presidential campaign, Republican candidate Richard Nixon injured his knee on a car door while campaigning in North Carolina. 
 Inevitably while watching evangelist revival meetings end with a teary flock of the newly converted head to the altar full of commitment and purpose, my late father-in-law, a Baptist minister, would wonder aloud how many would continue going to ch…
 More than a month has gone by since Lake Charles was assaulted by Hurricane Laura, perhaps the most ferocious storm to hit Louisiana since the Civil War. 
 On Sept. 27, Billy Arcement penned a letter with his recipe for curing what ails America. He claims we need to come together to preserve our precious democracy. 
 The presidential "debate" was a national embarrassment. Have we completely lost the ability to speak in a civil way? 
 When it comes to the redevelopment of Charity Hospital, it’s happening. 
 On Oct. 6, a federal court will begin hearings on whether to allow New Orleans to meet its commitment under the jail consent decree by retrofitting the second floor of the current jail rather than building an additional jail building. 
 This country is in a crisis right now, with the lives of too many Black people being destroyed because of hatred and bigotry. We, as Black people, are exhausted. From the ongoing pandemic, to being stuck in our homes, to the murdering of our peopl…
 The Manship School of Mass Communication is one of the jewels of LSU, and while we support every program and every student on campus, we take a special interest in media training. 
 High school football is back this weekend, another sign that we are reclaiming the joys of life in Louisiana. 
 Now that we are emerging from the COVID-19 crisis and fix the damage it has caused, we need to secure our future and recovery by ensuring one of our region’s most important energy and economic assets, the Gulf of Mexico, can keep making its contri…
 Democratic-based governments depend on accurate information, governmental oversight and an educated populace to elect competent officials and develop rational policies. 
 Despite today’s turbulent economic climate, there are numerous career opportunities available for Louisianans to adapt to the ever-changing economy. Louisiana’s public higher education system has options that can save both time and money, while bu…
 The shouting and bad behavior at a presidential debate may influence the contentious Nov. 3 election, but in the year of a global pandemic, the potential for an abrupt and essentially unplanned-for demise of the U.S. Affordable Care Act is a bigge…
 Columnist Michael Barone fears the Democrats in taking power will ensure the admission of new states to the Union, including — quelle horreur — the District of Columbia. A wave of off-colored senatorial seats is also portended, from Puerto Rico, G…
 In his Sept. 21 column, Archbishop Gregory Aymond wrote with the moral authority of his office to provide guidelines for Catholics in the upcoming presidential election. I was disappointed that, among the many issues he raises, the archbishop says…
 I enjoyed reading the article on Rosie. As a Black man, born in 1945, I questioned the lack of Black women in the 1943 picture. Over the years, I have heard that Andrew Jackson Higgins hired Black men/women at the same pay as White people. This wa…
 I was interested to read about the history of the Navy Yard in the Bywater in your Homefront series, although it brought to mind more recent events — namely the body of Brooke Buchler found inside the complex and the current trend of teenagers aro…
 Some of the most important lessons of the many we’ve learned during COVID-19 are those that have exposed the deficiencies in our current systems. Fortunately, the legislative special session offers an opportunity to address one of the most consequ…
 Watching C-SPAN’s recent commemorations of the centennial of the 19th Amendment that granted women the right to vote, one would have thought that only White women were instrumental in the fight for gender equality, eradicating sexism and obtaining…
 The recent article by Ben Myers titled; “Bridge builder or ‘secretary to Black people’? Cloudy role besets Lafayette’s minority affairs chief,” did an excellent job in shedding light on some of the issues on how this position was staffed and the o…
 Stop me if I’m going too far too fast, but the fact that Amy Coney Barrett is a faithful Catholic has little to do with whether she should serve as the newest U.S. Supreme Court justice. 
 The pandemic has made K-12 and higher education unlike anything we could’ve imagined before the coronavirus started forcing us to do things differently. Before the pandemic, some school and university officials had instituted online and hybrid lea…
 Jon Meacham, the Pulitzer-Prize winning historian whose subjects include no less than the “soul of America,” tweeted this out toward the end of Tuesday night’s presidential debate: “No hyperbole: The incumbent’s behavior this evening is the lowest…
 I’ve always thought of the Mike Foster era as being bookended by two memorable images. 
 Headlines are not facts. 
 I am responding to an opinion by Michael Gerson in which he stated that as pro-life person he would be voting for Joe Biden. I am happy to see him attempt to point out to President Donald Trump's supporters that this election can't be about a sing…
 News Tips:newstips@theadvocate.com
 Need help? 
 https://www.theadvocate.com/baton_rouge/opinion/