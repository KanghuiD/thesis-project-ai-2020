What impact is coronavirus having on the world's climate? | Climate Change | Al Jazeera
 Most Searched
 Inside Story14 Apr 2020 19:39 GMT Coronavirus pandemic, Climate Change, Global Warming
 India's capital is one of the world's most polluted cities, but its skies have turned blue and many people can see the Himalaya Mountains for the first time.
 In Italy's Venice, meanwhile, canal water is so clear fish can be easily seen.
 All this is an unexpected upside of the coronavirus crisis that has proved global air quality can be dramatically improved - and fast.
 The change has been created by lockdowns that have grounded flights and shut factories. But environmentalists warn it could be temporary. 
 Climate talks have been delayed to next year because of the outbreak, and there are fears countries could prioritise human and economic welfare before that of the environment.
 Many are questioning whether the world will just go back to business as usual when it recovers from the pandemic.
 So, moving forward, are there lessons the pandemic can teach us about living with nature?
 Presenter: Richelle Carey
 Guests:
 Francois Gemenne - Professor of environmental geopolitics and migration dynamics at The Paris Institute of Political Studies
 Meena Raman - Environmental lawyer and coordinator of Climate Change Program at the Third World Network
 Arunabha Ghosh - CEO of the Council on Energy, Environment and Water
       here.
      
 A renowned human rights lawyer, whose father was a forced labourer during World War II, shares lessons from his life.
 Europe, World War, War
 Former President de Klerk's denial that apartheid was a crime against humanity is cause for reflection on Freedom Day.
 Human Rights, South Africa, Africa
 As a child, I followed my mother on frenzied pilgrimages that taught me about the dynamics of wealth, race and religion.
 India, Religion
 Tide comes in, tide goes out. Boats spit oily fumes into the waves around me - but you already know this.
 Environment, Climate Change, Global Warming
 Beijing accuses the United States of deflecting blame from its own 'poor epidemic prevention and control measures'.
 Coronavirus pandemic, Health, Asia, Africa, Europe
 Banks frequently targeted during the economic crisis that has seen the Lebanese pound depreciate by 50 percent.
 Lebanon, Middle East, Banks, Business & Economy
 We explore life under lockdown as Beijing employs increasingly
 stringent measures to contain the coronavirus epidemic.
 Coronavirus pandemic, China, Health
 A Viking enthusiast confronts a neo-Nazi leader for misappropriating Viking symbols for white supremacist propaganda.
 Sweden, The Far Right, Racism
 The lack of a national paid family and medical leave policy has consequences for millions of workers in the US.
 United States, Health, Coronavirus pandemic
 101 East examines why more people are withdrawing from society in Japan.
 Japan, Asia Pacific, Arts & Culture
 	 
 https://www.aljazeera.com/programmes/insidestory/2020/04/impact-coronavirus-world-climate-200414185929278.html
