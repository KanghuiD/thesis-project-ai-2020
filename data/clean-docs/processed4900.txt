White House hopefuls target Trump on coronavirus response
 54ºF
 Alexandra Jaffe
 Associated Press
 18 minutes ago
 23 minutes ago
 43 minutes ago
 1 hour ago
 CHARLESTON, S.C. – Democratic White House hopefuls are seizing on President Donald Trump’s delayed response to the coronavirus outbreak, calling it the latest evidence of his incompetence and warning that the crisis may only deepen as a result.
 But some experts and Democrats warn that the candidates risk exacerbating a public health crisis if they go too far in politicizing the virus that causes the COVID-19 illness.
 Former Vice President Joe Biden, former New York Mayor Mike Bloomberg, Massachusetts Sen. Elizabeth Warren and Minnesota Sen. Amy Klobuchar all went after Trump during their CNN town halls Wednesday night. A number of the candidates have released their own pandemic policies, and Bloomberg is even airing an ad contrasting Trump’s response to the outbreak to his own handling of the aftermath of Sept. 11.
 It’s a potent political issue, as it gets at what Democrats see as two major potential weaknesses for Trump: questions about his competence as president and health care issues.
 “The threat from coronavirus and the chaos of the administration is front and center in everyone’s mind,” said Jesse Ferguson, a longtime Democratic strategist and former spokesman for Hillary Clinton’s campaign. “Not talking about it means you’re missing voters who are deeply worried about the public health threat and deeply concerned about the Trump administration’s incompetence.”
 Warren, Klobuchar and Bloomberg have all released public health plans detailing how they’d address and prevent similar outbreaks as president.
 During their CNN town halls, Warren warned that the economic impact of the new coronavirus could get worse. She and Klobuchar slammed Trump’s decision to put Vice President Mike Pence in charge of the coronavirus response, noting his controversial handling of an HIV outbreak in Indiana when he was governor.
 Biden has previously slammed Trump for “hysterical xenophobia and fearmongering” rather than respecting science on the issue.
 Hawaii Rep. Tulsi Gabbard, another 2020 Democratic hopeful, issued a statement Thursday night calling for Trump to allow her state to buy COVID-19 testing kits from Japan. She said the Centers for Disease Control and Prevention was “failing to provide them.”
 “As an island state, with responsible leadership, we can keep this virus out. But, we won’t be able to do that without our state and federal leaders taking it much more seriously than they are right now," she said.
 “Don’t open your mouth until you know what you’re talking about. This is politics. They need to listen to the scientists as well,” she said.
 That is a major criticism Democrats have lobbed at Trump — that he has botched his response and fostered more confusion by publicly contradicting the scientists in his administration about the severity of the virus.
 On Wednesday, the Republican president sought to minimize fears at a White House press conference in which he insisted the U.S. is “very, very ready” for an outbreak and predicted: “This will end … there’s no reason to be panicked.” But standing next to him, the health officials in charge of handling the outbreak predicted more cases are coming in the U.S.
 Democrats are not immune to the critique themselves, however. During Tuesday night’s primary debate, both Biden and Bloomberg made the erroneous claim that Trump cut funding for the CDC. While Trump proposed cuts to the CDC in his budget blueprint, he was overruled by Congress, and the eventual budget he signed included an increase in funding.
 But the public health system has a playbook to follow for pandemic preparation — regardless of who’s president or whether specific instructions are coming from the White House. Those plans were put into place in anticipation of another flu pandemic but are designed to work for any respiratory-borne disease.
 Jen Kates, senior vice president and director of global health and HIV policy at the Kaiser Family Foundation, warned that “any time political ideology starts to dominate the dialogue, it puts the public at risk.”
 “The history of good public health is that when things become politicized, we risk a good sound response and a response based on science and expertise,” she said. “This is a situation that’s changing by the moment, and that makes it all the more delicate.”
 Kates warned that there should be some “caution around not stoking panic and not using the partisan environment to steer away from basic public health messaging” — but acknowledged that will be tough “in a very partisan time, during campaign season.”
 Both parties are guilty of politicizing public health pandemics when they’re not the party in charge of the White House, she noted. During the Ebola outbreak in 2014, Republicans routinely slammed the Obama administration for similar critiques Trump is facing from Democrats — namely, that he was too slow to respond and didn’t appoint an adviser to coordinate the government’s response quickly enough.
 “We have the components of what could be a perfect storm. Are there ways to deal with it calmly and rationally? You bet. Is the United States well prepared? It seems like there are some gaps,” she said.
 She pointed to the fact that the initial White House funding request was just a fraction of what had been allocated for past viral outbreaks like Ebola, and Trump himself has largely left it up to Congress to sort out the details. She also noted that a number of key positions set up by Obama to deal with global pandemics have now either been eliminated or left vacant, and she called out Trump for contradicting his own scientists on the severity of the threat.
 Shalala agreed — but she warned Democrats to be “careful” to focus their critiques on the president and not the experts in the administration who are trying to tackle the crisis.
 “There are things that they can criticize, like the inadequate funding request and the president muddying the waters” at his press conference, she said, “but they shouldn’t be criticizing the agency heads and the very good scientist physicians that are trying to do their jobs.”
 But some Democrats say the conversation around the coronavirus is fair game because it gets at a much broader issue for Trump: questions surrounding his leadership.
 “Electing a president isn’t about a series of issue check boxes on a spreadsheet. It’s about the public’s confidence that you can lead the country, especially in times of crisis,” Ferguson said. “If we can’t demonstrate the fundamental failure of this administration to lead in this crisis, then we are not talking about the thing that people think about when electing a president.”
 ___
 Catch up on the 2020 election campaign with AP experts on our weekly politics podcast, “Ground Game.”
 Want the latest news and weather updates?
 https://www.clickondetroit.com/news/politics/2020/02/27/white-house-hopefuls-target-trump-on-coronavirus-response/
