5 A Day: what counts? - NHS
 Menu
 Close menu
 Back to Eat well
 Food and Drink Photos / Alamy Stock Photo
 Almost all fruit and vegetables count towards your 5 A Day, so it may be easier than you think to get your recommended daily amount.
 Some portions only count once in a day:
 Find out more about 5 A Day portion sizes
 Fruit and vegetables don't have to be fresh to count as a portion. Nor do they have to be eaten on their own: they also count if they're part of a meal or dish.
 These all also count towards your 5 A Day:
 Some ready-made foods are high in salt, sugar and fat, so only have them occasionally or in small amounts. 
 You can find the salt, sugar and fat content of ready-made foods on the label. 
 Find out more about food labels
 Remember to keep fruit juice and smoothies to mealtimes to reduce the impact on teeth. 
 For more details, see 5 A Day FAQs.
 No. Potatoes are a starchy food and a great source of energy, fibre, B vitamins and potassium. 
 In the UK, we get a lot of our vitamin C from potatoes. Although they typically only contain around 11 to 16mg of vitamin C per 100g of potatoes, we generally eat a lot of them.
 When eaten as part of a meal, potatoes are generally used in place of other sources of starch, such as bread, pasta or rice. Because of this, they don't count towards your 5 A Day. 
 Other vegetables that don't count towards your 5 A Day are yams, cassava and plantain. They're also usually eaten as starchy foods.
 Sweet potatoes, parsnips, swedes and turnips do count towards your 5 A Day because they're usually eaten in addition to the starchy food part of the meal.
 Potatoes play an important role in your diet, even if they don't count towards your 5 A Day. It's best to eat them without any added salt or fat. 
 They're also a good source of fibre, so leave the skins on where possible to keep in more of the fibre and vitamins. 
 For example, if you're having boiled potatoes or a jacket potato, make sure you eat the skin, too.
 To get the most from your 5 portions, eat a wide variety of fruit and vegetables. 
 For 5 A Day recipe ideas, see 5 A Day recipes.
 Find out more about having a healthy, balanced diet
       Page last reviewed: 8 October 2018
       Next review due: 8 October 2021
     
 https://www.nhs.uk/live-well/eat-well/5-a-day-what-counts/
