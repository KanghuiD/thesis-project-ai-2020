Coronavirus: Egypt extends lockdown, flights suspension by 15 days | Middle East Eye
 Egypt will extend a nationwide night-time curfew and the closure of airports by 15 days from Thursday to prevent the spread of the novel coronavirus, Prime Minister Mostafa Madbouly said on Wednesday. 
 The new decision is an extension of lockdown measures in place for the past two weeks, including the closure of schools, universities, cafes, nightclubs and entertainment venues.
 A night-time curfew will now start one hour later at 8pm instead of 7pm, Madbouly said. 
 The curfew bans the movement of individuals, including public transportation and private vehicles, on all public roads from 8pm to 6am every night.
 Shopping malls and supermarkets will be open from 6am to 5pm, according to the new measures, and will be completely closed on Fridays and Saturdays. 
 Sports, social clubs and youth centres will remain closed for the next two weeks. 
 Madbouly also said that ministers in his cabinet will donate 20 percent of their salaries for three months to support those who need treatment for Covid-19. 
 Up until Tuesday, Egypt has reported 1,450 cases of coronavirus and 94 deaths, while 276 patients have recovered. 
 Like other countries, Egypt has taken steps to curtail the impact of the pandemic on the economy.
 These include a surprise 3 percent interest rate cut, cash transfers to informal workers, salary payments for laid-off regular employees via an emergency fund, plus an injection of 20 billion Egyptian pounds ($1.27bn) to support the stock market.
 View the discussion thread.
 https://www.middleeasteye.net/news/coronavirus-egypt-extends-lockdown-flights-suspended
