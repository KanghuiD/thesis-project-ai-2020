Loneliness, anxiety, grief — dealing with the mental health impacts of the coronavirus - The Boston Globe
 The coronavirus pandemic is affecting nearly every country across the globe, and while it’s unknown how many will be impacted physically, practically everyone will be affected emotionally. There will be prolonged isolation for some, over-crowded homes for others, sustained levels of heightened stress and anxiety, as well as financial hardships, strained relationships, grief, and loss. It will heighten the mental health (diagnosable conditions such as anxiety and depression, and PTSD) and emotional health (the non-diagnosable conditions such as loneliness, grief, and loss) of billions of people around the world.
 Scientists and other professionals are collaborating across borders in a race to find treatments and a vaccine for COVID-19. The urgent need to combat the disease has made it patently obvious that such large-scale national and international efforts to safeguard and treat the physical health of the populace are essential. However, despite the fact that the psychological effects of the pandemic will linger long after the physical threat has abated, little effort has been made to address mental health.
 We know, for example, that chronic loneliness increases the likelihood of an early death by upward of 26 percent.In recent years, the rise of loneliness, especially among young adults and millennials, has led health experts to declare loneliness a public health epidemic, prompting Britain to appoint a minister of loneliness to manage their response. This, of course, was all pre-COVID-19 — before social distancing and self-isolation. How much more severe is the loneliness epidemic now, and how much worse than that will it be by the time the pandemic is over?
 Many around the world are already grieving, and many of us are experiencing anticipatory grief, fearing we’ll lose people we know and love. In addition, all of us are already dealing with loss — even if just the loss of life as we knew it or of our hopes and plans that have been disrupted for months and, in some cases, for years while we live through the Great Pause.
 Massive uncertainty about the future (e.g., whether we will contract the virus and, if we do, how we’ll manage financially, not knowing when life will return to a new normal and what that new normal will look like) is fueling anxiety, sleep disruptions, irritability, drops in mood, and even depression — all on a global scale. In addition, many health care professionals, emergency technicians, and other front-line workers might develop PTSD and require psychological intervention, which will be challenging to administer on such a large scale.
 The legacy of social distancing and stay-at-home directives will be difficult to unwind. Anxiety does not disappear because shutdowns ease and people are allowed to return to school and work. After months of carefully and habitually avoiding others, merely venturing out and being in proximity to others will be intensely anxiety-provoking for many. As much as we yearn to leave our homes and resume our lives, anxiety thrives on avoidance, and after refraining from social contact for so long, it will be hard to dislodge the visceral dread it will evoke.
 Beyond the acute emotional challenges we face as individuals, we must also consider how the situation is influencing our relationships. Being cooped up with friends and loved ones under incredibly stressful circumstances is causing an eruption of tensions and rifts that had previously been latent or manageable. How much damage our closest relationships will sustain by the time the crisis is over remains unclear, but it is safe to assume large swaths of the population will need help and guidance to repair frayed or ruptured relationships.
 We can see this mental health crisis coming, yet we lack both national and international programs to address it. Here in the United States, a substantial portion of the population will have acute mental health needs, though there are few programs or mechanisms with which to assist with the emotional challenges our exit from a global state of emergency will require.
 Governments and institutions need to act now to address the sweeping psychological challenges we are facing and will continue to face once we begin a staggered return to whatever normal will be in the aftermath of this pandemic. To address these issues, we need to develop and roll out broad-based psychological interventions for loneliness, various forms of anxiety, grief and loss, PTSD, and relationship repair. Much as vaccines do, such programs require resources and time to develop and test before they can be ready for broad-based public use. The sooner we start addressing people’s emotional and psychological needs, the sooner true recovery will come.
 Guy Winch is a licensed psychologist and author. 
 Digital Access
 Home Delivery
 Gift Subscriptions
 Log In
 Manage My Account
 Customer Service
 Help & FAQs
 Globe Newsroom
 Advertise
 View the ePaper
 Order Back Issues
 News in Education
 Search the Archives
 Terms of Service
 Terms of Purchase
 Work at Boston Globe Media
 https://www.bostonglobe.com/2020/04/21/opinion/loneliness-anxiety-grief-dealing-with-mental-health-impacts-coronavirus/