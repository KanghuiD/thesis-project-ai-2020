Coronavirus: How bad information goes viral - BBC News
 There's a huge amount of misleading information circulating online about coronavirus - from dodgy health tips to speculation about government plans. This is the story of how one post went viral. 
 The tips that follow are misleading or wrong. One says that you don't have the virus "if you have a runny nose".
 According to fact checking organisations Full Fact and Snopes, citing health authorities including the US Centers for Disease Control (CDC) and The Lancet medical journal, a runny nose is uncommon - but it's not unheard of among coronavirus patients.  
 The post also encourages people to "drink more hot water" and "Try not to drink ice". There's currently no medical evidence that either of those things will help prevent or cure coronavirus. 
 "That has no support," says Alex Kasprak of Snopes. "It's wild to see that in there, it's a big red flag."
 We attempted to contact the person who posted the information; she did not respond. 
 The new post built on the 7 February post with additional information. Although the new post stated "My classmate's uncle and nephew, graduated with a master's degree ... just called me and told me to tell my friends...", Glen didn't actually receive a phone call from an uncle. 
 He says the post was just "a forward that I got and forwarded it on". 
 The additional tips included some accurate advice - for instance, it tells people to wash their hands, a key preventative measure.
 But the new version also added some unsubstantiated and misleading information. 
 For instance, it described in very specific detail how the disease progresses. But doctors say coronavirus symptoms and severity are highly variable, and there's no one exact progression pattern. 
 For several weeks the post was confined to relatively minor outlets. But on 27 February, an 84-year-old former art gallery owner named Peter made it really go viral. 
 Peter's post was similar to Glen's, but again included some new information - some of which was wrong or misleading.
 Peter's post spread rapidly, bringing it to the attention of fact checkers including Full Fact and Snopes. Both organisations wrote detailed stories debunking the claims, citing reliable medical sources including the WHO, the US CDC, the UK National Health Service (NHS) and others.  
 For instance, one claim in the post stated that the virus "hates the Sun", but as of yet there's no evidence that sunlight kills the virus, and cases have been reported in many countries with hot and sunny climates. 
 Other claims in the post were factual. For instance, it repeated the advice about hand-washing.
 Peter, who lives in southern England, edited the misleading parts of his post after the fact checkers posted their stories. But by then, it had already been shared nearly 350,000 times.
 When contacted by the BBC, Peter would not say specifically where he got the information in the post, but said that he trusted his source at the time. 
 "I believed him actually to be a relation of this scientific guy, a medical guy who'd given all those facts and figures," he told us in a phone interview. 
 Peter says he was trying to help people protect themselves.
 "I try to be as factual as I can. And if I'm corrected, or if I discover myself that I've said something incorrectly, I apologise and I amend it," he says. 
 Despite his fact-based edits, the claims in the original version of Peter's post soon spread, and mutated. Some versions started to absorb further misleading information. 
 The post then spread - helped by celebrities, including a Ghanaian TV presenter and an American actor, but also by scores of ordinary people. 
 April's post was attributed to "a friend's nephew in the military". 
 At the same time the post was translated into several languages including Arabic, Amharic, Vietnamese, French, Spanish and Italian. 
 Again, some of the posts contained accurate or at the worst benignly misleading information - but other alleged "facts" had the potential to be harmful. 
 One piece of advice suggests doing a coronavirus "self-check" every morning by holding your breath for more than 10 seconds. But there's no evidence to indicate that your ability to do this means you are virus-free.  
 The best of BBC reporting, education and training to help you understand the challenges posed by disinformation and fake news.
 "Uncle with a master's" is a specific type of viral post - the slang term for it is "copypasta".  This is material that someone copies and pastes instead of using "share", "retweet", other sharing tools provided within social networks.
 That makes each post look original: it looks like it was written by someone you know, possibly a friend or relative, thus increasing the chance that you might trust it.
 Because there are so many "original" posts, it also makes it difficult for social networks and people who study them to keep track of exactly how far such posts have spread. 
 Fact checkers say that we all have a part to play - by thinking before we share. 
 "Pause for a second. Have a little look. See if you can find out more information about it," says Claire Milne from Full Fact. "Work out if it's correct or if you are sharing something with your friends and family that might be misleading and could harm them."
 If the source seems vague or the post is a long list or thread packed with information, compare the claims to known, trusted sources.
 The people sharing the "uncle with a masters" post - which in its various versions is still spreading online - generally aren't being malicious: they usually believe that they're passing on facts. Or they may be unsure about the veracity of the information, but believe that they're helping anyway, "just in case" what's outlined is true.
 In general, they aren't foolish or stupid. It's easy to get duped if you're anxious.
 The "uncle" post is also a blend of truth and nonsense, which makes it more pernicious and easily spread.
 "When things [that are false] are mixed with things that sound reasonable or true, it confuses people, and in some cases, it allows people to think that the rest of the advice is legitimate as well," says Mr Kasprak from Snopes.
 Sources: Full Fact, Snopes, BBC Monitoring
 Our radio show airs every Saturday on BBC World Service
 Answering life’s questions through daily features, quizzes and opinions.
 https://www.bbc.com/news/blogs-trending-51931394
