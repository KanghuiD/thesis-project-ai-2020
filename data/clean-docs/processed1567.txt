French Central Banker Says Lockdown May Threaten Corporate Solvency - The New York Times
 By Reuters
 PARIS — French companies will emerge from a nationwide lockdown next month with severely strained balance sheets needing government attention, the head of the French central bank said on Thursday.
 The lockdown in place since mid-March to stem the spread of the new coronavirus is due to be lifted on May 11, although the government has warned that some curbs may be kept in place and not all firms will be able to immediately go back to business.
 Even before the coronavirus crisis shut down vast swaths of the euro zone's second-biggest economy, French corporate debt was at record levels because companies have in recent years been taking advantage of record low interest rates to borrow heavily.
 Since the lockdown on non-essential activities was imposed, more than 170,000 firms have sought to take advantage of state guarantees worth 300 billion euros ($324 billion) on bank loans to help them cope with dwindling cashflow.
 "For companies and the self-employed, the exit from lockdown will be potentially perilous," Bank of France Governor Francois Villeroy de Galhau wrote in an opinion piece in Le Figaro.
 "Despite the strong liquidity measures, lost revenue that can't be recovered will leave their solvency seriously deteriorated," he said, adding that corporate debt had grown by 37 billion euros in March, up 2%.
 He said that state support for companies should therefore shift from liquidity to solvency by shoring up equity, targeting firms shut the longest with special aid and allowing some deferred taxes to be permanently written off.
 Another option, Villeroy said, would be to allow companies to book some of their losses this year on their 2019 accounts, which would help spread the pain over time.
 Meanwhile, the crisis will put unprecedented pressure on the public finances, but Villeroy said it was preferable to pursue a gradual path to sounder finances than tax hikes on households because of the negative impact on growth.
 Villeroy said the French government's proposal for a European recovery fund could help spread the costs of kick-starting the economy while the European Central Bank would keep interest rates low and liquidity abundant amid low inflation.
  (Reporting by Leigh Thomas; Editing by Hugh Lawson and Alex Richardson)
 https://www.nytimes.com/reuters/2020/04/23/world/europe/23reuters-health-coronavirus-france-villeroy.html
