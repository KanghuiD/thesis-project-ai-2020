Coronavirus: 'Pets no risk to owners' vets stress - BBC News
 Veterinary scientists have recommended cat owners keep their pets indoors to help prevent the spread of the coronavirus among animals. 
 But the British Veterinary Association stressed "owners should not worry" about risk of infection from pets. 
 "There isn't a single case of a pet dog or cat infecting a human with Covid-19," Dr Angel Almendros, from City University in Hong Kong, told BBC News.
 Research has shown cats may be able to catch the virus from other cats. 
 Dr Almendros added that it would be sensible to keep cats indoors - where it is safe and possible to do so - during the outbreak. 
 The British Veterinary Association (BVA) president Daniella Dos Santos told BBC News she agreed with that advice. But the association has since clarified that its recommendation to concerned pet-owners is to take the precaution of keeping cats indoors "only if someone in their own household showed symptoms".  
 Every pet-owner though should "practise good hand hygiene," she said. 
 "An animal's fur could carry the virus for a time if a pet were to have come into contact with someone who was sick." 
 In a recent paper on the subject, Dr Angel Almendros referred to the case of a 17-year-old pet dog in Hong Kong that tested positive for the Covid-19 virus - apparently infected by its owner. 
 "But even where we have these positive results, the animals are not becoming sick," he said. 
 "As in the previous Sars-Cov outbreak in Hong Kong, in 2003, where a number of pets were infected but never became sick, there is no evidence that dogs or cats could become sick or infect people."
 It appears cats may be susceptible to infection from respiratory droplets - virus particles suspended in air that people cough, sneeze or breathe out.  
 Following a case in Belgium where a cat tested positive about a week after its owner showed symptoms, scientists in China carried out lab tests that provided evidence of infected cats transmitting the virus to other cats.
 "It is interesting to note in the experimental evidence that cats can become infected, alongside the apparent infection of a tiger [at Bronx Zoo in New York]," Prof Bryan Charleston, director of the UK's Pirbright Institute, which specialises in the study of infectious disease, said, adding that the "evidence on the transmissibility" from humans to other animals was building.
 There is also evidence humans can transmit respiratory infections to wild great apes, which makes the global spread of Covid-19 a concern for conservationists working to protect critically endangered animals, including gorillas.  
 In all of these cases though, it is infected humans that pose the threat to other species. 
 Dr Almendros told BBC News: "We know that the virus did make the jump from an animal into humans [at the beginning of this crisis]". But, he explained, that this appeared to be linked to people eating those animals. 
 There is no evidence that domestic animals can pass this disease back to people.
 "Treat pets like other people in your household. So if you're feeling sick, it's better not to interact with them," said Dr Almendros.  
 "I hope pet owners can sleep a bit better with the right advice and information," he added. "It isn't easy these days, I know."
 Update 8th April: This story has been updated to reflect that the British Veterinary Association has issued a clarification about their position.
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/science-environment-52204534
