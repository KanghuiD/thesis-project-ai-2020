Andrew McCarthy: On projections of coronavirus deaths, government uses unreliable model | Fox News
       This material may not be published, broadcast, rewritten,
       All market data delayed 20 minutes.
     
 Health Metrics Sciences professor Dr. Ali Mokdad explains how the projections were determined.
 Get all the latest news on coronavirus and more delivered daily to your inbox.  Sign up here.
 To describe as stunning the collapse of a key model the government has used to alarm the nation about the catastrophic threat of the coronavirus would not do this development justice.
 In a space of just six days starting April 2, two revisions (on Sunday and Wednesday) have utterly discredited the model produced by the University of Washington’s Institute for Health Metrics and Evaluation.
 I wrote about the IHME’s modeling at National Review on Monday, the day after the first revision — which was dramatic, but pales in comparison to Wednesday’s reassessment. This was not immediately apparent because the latest revision (Wednesday) did not include a side-by-side comparison, as did the Sunday revision.
 MEET THE FORMER NYT REPORTER WHO IS CHALLENGING THE CORONAVIRUS NARRATIVE
 Perusal of the new data, however, is staggering, as is what it says about government predictions we were hearing just days ago about the likelihood of 100,000 deaths, with as many as 240,000 a real possibility.
 As I noted in my last post on this subject, by Sunday the projection of likely deaths had plunged 12 percent in just three days, 93,531 to 81,766. Understand, this projection is drawn from a range; on April 2, IHME was telling us cumulative COVID-19 deaths could reach as high as approximately 178,000. The upper range was also reduced on Sunday to about 136,000.
 On Wednesday, the projected cumulative deaths were slashed to 60,145 (with the upper range again cut, to about 126,000). That is, in less than a week, the model proved to be off by more than 33 percent.
 https://www.foxnews.com/opinion/andrew-mccarthy-on-projections-of-coronavirus-deaths-government-uses-unreliable-model
