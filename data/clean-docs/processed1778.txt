NYC Med Students to Graduate Early to Battle Coronavirus
 0 videos remaining. Log in for unlimited access.
 Hospitals on the front lines will soon get help from newly minted doctors. Columbia Universityâs Vagelos College of Physicians and Surgeons and the Icahn School of Medicine at Mount Sinai will graduate students about a month early on Wednesday so they can help hospitals battle the coronavirus.
 At Columbia, 84 of 137 students who graduated early signed up to start helping at NewYork-Presbyterian Hospital. They will not treat COVID-19 patients, but will fulfill roles like medical scribes and telehealth guides to lessen the overall burden on existing hospital staff. 
 Sixteen students who graduated from the Icahn School will join Mount Sinai hospital. Starting Monday, they will also help care for non-coronavirus patients to free up staff for more pressing work.
 âSo Iâm overwhelmed with the amount of excitement that I have,â said Icahn student Olamide Omidele, 25. âOf course, thereâs a bit of nervousness as well, but being able to work with colleagues that I got to know through medical school will help ease that tension,â he said. 
 The Committee of Interns and Residents is the largest and oldest union representing medical interns and residents. The organization says it recognizes the need for the young doctors, but raises concern about how the new doctors will be supervised and supported. 
 âWe are just concerned about the support they will receive,â said Margot Riphagen, the executive director of the Committee on Interns and Residents. âFrankly because everything is stretched thin.â
 The Icahn School of Medicine says it is implementing some protective measures like weekly check-ins and virtual seminars to help the medical interns adjust to working in the hospital during an unprecedented time.
 https://www.ny1.com/nyc/all-boroughs/coronavirus-blog/2020/04/14/med-students-graduate-early
