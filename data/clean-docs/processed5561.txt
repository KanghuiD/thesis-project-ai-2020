Live: Coronavirus Updates | FOX 10 Phoenix
 FOX 10 is working for you by coordinating with our other affiliates across the country to help keep you informed and educated on what you need to know about the coronavirus. Every weekday on FOX News Now, our live coverage begins at 7 a.m. MST reporting the latest news, prevention tips and treatment information.
 According to the CDC, coronaviruses come from a large family of viruses. There are actually a variety of previously known human coronaviruses, however, the virus that has now infected over 200,000 people worldwide is new. 
 The newly identified COVID-19 virus refers to the novel coronavirus first detected in Wuhan China. This virus is different from the previously identified coronavirus 229E, NL63, OC43, or HKU1 which have previously been known to circulate among humans causing mild illness likened to the common cold. 
 A virus previously thought to only infect animals has now emerged to spread among people. The CDC says the first infections were associated with live animal markets in China but has now been known to spread person-to-person globally. 
 Symptoms for the COVID-19 virus could appear in as few as 2 days, or as long as 14 days after exposure, says the CDC.
 Reported illnesses have ranged from mild symptoms such as cough, shortness of breath, and a fever, to severe and even fatal.
 FULL COVERAGE: fox10phoenix.com/coronavirus
 Coronavirus (COVID-19)How it spreads, symptoms, prevention, treatment, FAQhttps://www.cdc.gov/coronavirus
 Arizona COVID-19 ResponsePublic resources, FAQ, webinarshttps://www.azdhs.gov/coronavirus
 In order to protect yourself from a possible infection, the CDC recommends:
 RELATED CONTENT:
 Coronavirus: Symptoms, testing and how to prepare amid growing COVID-19 outbreak
 How coronavirus differs from flu: Symptoms to watch for
 Does wearing a face mask protect you from coronavirus and other infectious diseases?
 Should you cancel your trip? CDC urges travelers to avoid several countries impacted by coronavirus
 Coronavirus more contagious than SARS or MERS, can live on surfaces for up to 9 days, studies say
 Cleaning tips to keep your workspace safe amid coronavirus worries
 https://www.fox10phoenix.com/live-coronavirus-updates