Indiana coronavirus: Blogger launches Geeking@Home online comic con
 Tony Troxell concedes there's no way to replicate a pop culture convention during the coronavirus pandemic.
 Because of "stay at home" orders, fans can't pose for pictures with cosplay enthusiasts or meet celebrities during autograph sessions.
 The stories of Star Wars, Star Trek, Harry Potter, Marvel and DC Comics and other fictional worlds are celebrated at fan events worldwide.
 "There’s a certain energy when you hit a convention," said Troxell, founder of the blog Geeking in Indiana. "There’s a certain positive energy that right now we’re sorely missing."
 The schedule of high-profile Indianapolis conventions includes Gen Con (July 30-Aug. 2), Indy PopCon (July 10-12), Days of the Dead (July 17-19) and HorrorHound Weekend (Sept. 18-20).
 Organizers cited the coronavirus pandemic when shifting Indiana Comic Convention from April 10-12 to June 26-28.
 In the meantime, Troxell launched an online project to help connect pop culture creators, vendors and fans.
 Billed as Geeking@Home Con, Troxell's brainchild is a virtual gathering place that showcases "Author's Avenue," "Artist's Alley," "Cosplay Corner" and "The Vendor Hall."
 Troxell also hopes to stream concerts and panel discussions as part of the Geeking@Home Con.
 There's no entry fee for creators and vendors who would like to be represented on the Geeking@Home pages. Submissions are accepted at geekinginindiana.com.
 Geeking@Home is giving exposure to companies ranging from Indiana's Who North America, a store that specializes in Doctor Who items, to Pittsburgh-based video series "Heroineburgh."
 Troxell said a role-playing game creator from Italy asked to be listed.
 "I’ve been trying to keep it as local as I can," Troxell said, "but I’m not going to be a jerk and say, ‘You don’t have a Hoosier-based zip code, I’m not going to help you out.’ "
 What to watch: Check out 40 Netflix titles with Indiana ties
 Presently, 40 creators and vendors are participating in Geeking@Home.
 "For a lot of independent creators, their only sources of income are online sales and conventions," Troxell said.
 In-person conventions provide crucial opportunities to make sales, Troxell said.
 "It's where they can grab those people who are just walking by," he said. "They see their colorful artwork and books."
 https://www.indystar.com/story/entertainment/events/2020/04/11/indiana-coronavirus-blogger-launches-geeking-home-online-comic-con/5000755002/
