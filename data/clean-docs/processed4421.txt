Cyberattack Hits HHS During Coronavirus Response - Bloomberg
 We’re tracking the latest on the coronavirus outbreak and the global response. Sign up here for our daily newsletter on what you need to know.
 He said “HHS and federal networks are functioning normally at this time.”
 While a foreign state is suspected in the attack, the administration hasn’t yet confirmed who it was, according to a U.S. official.
 The attack, which involved overloading the HHS servers with millions of hits over several hours, didn’t succeed in slowing the agency’s systems significantly, as was apparently intended, according to one of the people familiar with the matter. They requested anonymity to discuss details of the sensitive incident.
 An HHS spokeswoman said in a statement that the agency had “put extra protections in place” as it prepared to respond to the coronavirus outbreak.
 “On Sunday, we became aware of a significant increase in activity on HHS cyber infrastructure and are fully operational as we actively investigate the matter,” said the spokeswoman, Caitlin Oakley. “We are coordinating with federal law enforcement and remain vigilant and focused on ensuring the integrity of our IT infrastructure.”
 Just before midnight on Sunday, the National Security Council issued a tweet warning without elaboration about “fake” text messages. The tweet was prompted by a message from an unknown sender warning that the person’s “military friends” had heard in a briefing that the “president will order a two week mandatory quarantine for the nation.”
 Text message rumors of a national #quarantine are FAKE. There is no national lockdown. @CDCgov has and will continue to post the latest guidance on #COVID19. #coronavirus
 Secretary of State Michael Pompeo and other Trump administration officials are aware of the cyber attack, according to one of the people, and the hack doesn’t appear to have resulted in any data being taken from HHS systems.
 General Paul Nakasone, who leads the National Security Agency and U.S. Cyber Command, is looking into the situation, one of the people said.
 — With assistance by Josh Wingrove
 https://www.bloomberg.com/news/articles/2020-03-16/u-s-health-agency-suffers-cyber-attack-during-covid-19-response
