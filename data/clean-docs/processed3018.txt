Coronavirus: Expert panel to assess face mask use by public - BBC News
 Should more of us wear face masks to help slow the spread of coronavirus?
 This question is to be assessed by a panel of advisers to the World Health Organization (WHO).
 The group will weigh up research on whether the virus can be projected further than previously thought; a study in the US suggests coughs can reach 6m and sneezes up to 8m.
 The panel's chair, Prof David Heymann, told BBC News that the new research may lead to a shift in advice about masks. 
 The former director at the WHO explained: "The WHO is opening up its discussion again looking at the new evidence to see whether or not there should be a change in the way it's recommending masks should be used."
 The WHO recommends keeping a distance of at least 1m from anyone coughing or sneezing to avoid the risk of infection.
 It says people who are sick and show symptoms should wear masks.
 But it advises that healthy people only need to wear them if they are caring for others suspected of being infected or if they themselves are coughing or sneezing.
 It emphasises that masks are only effective if combined with frequent hand-washing and used and disposed of properly.
 The UK, along with other countries including the US, advises that social distancing should mean staying at least 2m apart.
 This advice is based on evidence showing that viruses can only be transmitted while carried within drops of liquid.
 The understanding is that most of those drops will either evaporate or fall to the ground near to the person who released them.
 Researchers at the Massachusetts Institute of Technology (MIT) in Cambridge, US, used high-speed cameras and other sensors to assess precisely what happens after a cough or sneeze.
 They found that an exhalation generates a small fast-moving cloud of gas that can contain droplets of liquid of varying sizes - and that the smallest of these can be carried in the cloud over long distances.
 The study - conducted in laboratory conditions - found that coughs can project liquid up to 6m away and that sneezes, which involve much higher speeds, can reach up to 8m away.
 The scientist who led the study, Prof Lydia Bourouiba of MIT, told me that she is concerned about the current concept of "safe distances".
 "What we exhale, cough or sneeze is a gas cloud that has high momentum that can go far, traps the drops of all sizes in it and carries them through the room," she said.
 "So having this false idea of safety at one to two metres, that somehow drops will just fall to the ground at that distance is not based on what we have quantified, measured and visualised directly."
 Prof Bourouiba's view is that in certain situations, especially indoors in poorly ventilated rooms, wearing masks would reduce the risks.
 For example, when facing someone who's infected, masks could help divert the flow of their breath and its load of virus away from your mouth.
 "Flimsy masks are not going to protect from inhaling the smallest particulates in the air because they do not provide filtration," Prof Bourouiba said.
 "But they would potentially divert the cloud that is being emitted with high momentum to the side instead of forward."
 According to Prof Heymann, the new research from MIT and other institutions will be evaluated because it suggests that droplets from coughs and sneezes could be projected further than originally thought.
 He said that if the evidence is supported, then "it might be that wearing a mask is equally as effective or more effective than distancing."
 But he adds a warning that masks need to be worn properly, with a seal over the nose. If they become moist, Prof Heymann explained, then particles can pass through. People must remove them carefully to avoid their hands becoming contaminated.
 He adds that masks need to be worn consistently.
 "It's not on to wear a mask and then decide to take it off to smoke a cigarette or eat a meal - it must be worn full time," he said. 
 The panel, known as the Strategic and Technical Advisory Group for Infectious Hazards, is due to hold its next virtual meeting in the next few days.
 A spokesperson for Public Health England said there was little evidence of widespread benefit from wearing masks outside clinical settings.
 "Facemasks must be worn correctly, changed frequently, removed properly, disposed of safely and used in combination with good universal hygiene behaviour in order for them to be effective.
 "Research also shows that compliance with these recommended behaviours reduces over time when wearing facemasks for prolonged periods."
 Long popular in many countries in Asia, masks are now being assessed for public use by the US Centers for Disease Control (CDC).
 In Austria, the police now wear them and anyone dealing with the police will have to wear one too. Supermarkets there will insist that customers do too.
 A once-rare sight in Europe is becoming more common, and new advice from the WHO would accelerate that change.
 The US sees 5m more jobless claims as the economy struggles in the coronavirus outbreak.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/science-environment-52126735