Opinion | How are York Region teens coping, staying healthy during the coronavirus pandemic? | YorkRegion.com
 As we all know, for the past few weeks most of us have been stuck at home in self-isolation to help stop the spread and flatten the curve of the COVID-19 virus. 
 In Ontario, students are off school until at least the beginning of May. 
 Usually, around this time, students are back at school after the March break, but recently most Canadians are self-isolating in their homes. Stuck at home all day, every day can really take a toll on youth mental health. 
 As a high school student myself, I am not a medical professional, but as a teen I know firsthand how not being able to talk to others can affect day-to-day life during these hard times. 
 Personally, as a social person, I know how much getting together with friends and family is crucial for support in everyday life. 
 A few York Region teens feel the same way on this matter. 
 Local teen James Cairns expressed that “this pandemic has been hard to deal with mentally.” 
 He went on to say that he finds it easy to feel “isolated and lonely.”
 Another local teen, Maya Sinclair, expressed that being isolated from others has taken a toll on her well-being as well.
 “I've been really struggling with the changes in my routine and not being able to connect as much with the people I love,” she said. 
 This solitary time may be tough for many, so here is what these local teens are doing to pass the time and stay healthy while self-isolating. 
 Both teens mentioned they have been focusing on the things they love most to take their minds off of self-isolation. 
 For example, Cairns has been spending time with his family (with whom he is self-isolating) and talking to his friends online to feel less lonely. 
 He said that talking to friends and family has been extremely beneficial to his mental health. 
 Sinclair said she has been trying not to put too much pressure on herself. 
 As we currently do not really know how long we are going to be in self-isolation, we should embrace the time we have and invest it into ourselves. 
 Talking to friends and family online or reading a book are just some ways that teens in our community have been spending their time in self-isolation. 
 Of course for some, self-isolation may be difficult to get through. 
 Fortunately, there are resources to help youth and kids specifically get through these tough times. 
 If you or anyone you know is having a hard time dealing with this crisis, call or text Kids Help Phone at 1-800-668-6868.
 — Jake MacAndrew is a Grade 10 student from Whitchurch-Stouffville. Currently, he is preparing to enter into the world of journalism.
 https://www.yorkregion.com/opinion-story/9921602-how-are-york-region-teens-coping-staying-healthy-during-the-coronavirus-pandemic-/
