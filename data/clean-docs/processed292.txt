Indonesia's Palu endured a triple disaster, now coronavirus looms | Indonesia News | Al Jazeera
 Thousands still without housing or hospitals after 2018 earthquake, tsunami and liquefaction destroyed area.
 Citizens of Palu, tens of thousands still in temporary shelters, had just begun rebuilding their health facilities when the novel coronavirus arrived. 
 It was here and across Indonesia's Central Sulawesi province that the deadliest earthquake of 2018 struck, killing 4,845 people and destroying more than 100,000 homes. The 7.4-magnitude earthquake swallowed entire neighbourhoods and triggered a tsunami so fast that even geologists were surprised.
 Indonesia's health workers brace for coronavirus onslaught
 COVID-19 burials fuel tension in Indonesia
 Palu struggles to rebuild a year after triple disaster
 When the earthquake hit, Anutapura hospital's main nursing and housing wing collapsed on itself with the loss of 250 hospital beds in a province with only 1,100 beds in all Class A and B hospitals and a population of more than 2.6 million. The rebuilding was supposed to finish next month. 
 But what locals fear most is what might happen if the virus enters the shelters that remain home to tens of thousands of people across the city, despite 18 months of promises of fast aid.
 "As soon as the government said there were cases in Palu, I began panicking," said Tari Yalijama, a 32-year-old mother of three who lives in a temporary shelter. Ten families share the toilet she uses. Her former home still lies in a pile of rubble, thrust 150 metres downhill.
 "But until someone is tested positive, everyone will go about their lives normally. People think the heat will kill it," she said. 
 The virus can spread in people without symptoms, and tests are hard to come by.
 "These shelters are far from appropriate for physical distancing measures," said David Pakaya, a doctor and medical lecturer at Palu's Tadulako University. "Each family lives in an area less than 20 square metres [215 square feet] and without adequate ventilation."
 The government's 699 temporary shelters house close to 9,000 people. Twelve families share four bathrooms and a single kitchen. In the shelters built by private foundations, thousands more are housed in small, single-room huts with metal roofs and a bathroom shared by even more people.
 "We're facing a second crisis as we still recover from the last," said Dewi Rana, who leads a local NGO focused on women, Libu Perempuan.  
 NGOs are still delivering food aid, 74 health facilities were still damaged at the beginning of the year, and damage to irrigation systems has left many livelihoods unviable and water sources unreliable.
 "Some friends are only just beginning to recover, but we're confronted by this virus," Rana said. 
 Just as health facilities have yet to be rebuilt, many residents are still living in the emergency tents that were erected just after the September 2018 disaster.
 It is conditions like these that could become a breeding ground for the virus, experts say.
 "If the SARS-CoV-2 virus infects one of the shelters' people, it will spread easily," said Pandu Riono, a statistician and public health expert at the University of Indonesia. 
 Combined with weakened hospital facilities, "it will be a disaster and increase the number of deaths in conditions like those," Riono said. 
 Across Indonesia, experts predict the virus has spread much further and far deeper than medical facilities can test. Hundreds of thousands will die without intervention, according to a study by a team that includes Riono at the University of Indonesia.
 Central Sulawesi has detected two dozen positive cases of the virus, but hundreds more are suspected and three deaths have already been recorded, including the country's first public official - the district chief of North Morowali, whose accompanying team and family have all tested positive.
 The shelters have become the focus of the government's response to the pandemic, officials tell local media. Palu's government has tried to intervene, spraying disinfectant throughout temporary shelters, despite evidence that it is insufficient and can harm human health. 
 On the other hand, it does appear to have provided some reassurance to the community and calmed nerves.
 "I'm afraid this could become another path the virus could use," said Rana, speaking of residents'   already poor health and weakened immune systems.  
 Poor nutrition can also weaken immune systems, and inconsistent water supply makes it more difficult to keep facilities and hands clean. 
 "There are health problems in the temporary shelters, including nutritional problems, psychosocial stress, degenerative diseases, and other infectious diseases such as tuberculosis, and dengue fever, all of which can lower the body's immune system," said Pakaya.
 The area continues to see high rates of respiratory infections as a result of the dust from the rubble of the destroyed infrastructure and unhygienic living arrangements. On top of that, many residents have trouble buying phone credit to access the internet, which means that few have much understanding of the disease.
 "Insufficient access to valid information about the virus, ranging from its causes signs, symptoms, and prevention methods means residents are not alert and can't identify if they themselves have the virus," said Pakaya.
 Without aid to repair homes, many are afraid to live inside cracked walls and ceilings that could collapse with another earthquake, which occur often. Dewi fears for the thousands still in these tents living around the city and into the mountains. 
 "You can't go outside your home, so access to food is limited because there is no aid now. Access to health facilities is limited, and water access is limited. As you can imagine this can become much more difficult for people living in tents," said Rana.  
 Rana and her organisation distributed sugar to survivors of the earthquake living in tents near one of the neighbourhoods where the earth turned liquid and swallowed homes.
 While others across Indonesia and the world are told to stay inside as much as possible, many of the new homes that private foundations, NGOs and the government have built get too hot during the day, raising the risk of dangerous heatstroke.
 Last year, officials said the first homes would be finished by the end of 2019, but none has been completed. Thousands of people are yet to receive three types of government-mandated aid that they were promised: two months of daily compensation, compensation for the death of family members, and money to rebuild homes.
 Fathi Zubaidi, the deputy director of Anutapura hospital, Palu's second-biggest, says he is confident the hospital now has adequate facilities, despite the stalled construction on the hospital's destroyed building. 
 But access to health facilities remains limited, says Rana, whose NGO Libu Perempuan is logging spikes of gender-based violence in the close-quartered temporary shelters. The cramped spaces and compounding crises of lost livelihoods and unreliable food supply has led to reports of domestic violence, child marriage, and invasions of privacy. 
 "We're building a database for all the temporary shelters, so we know who is pregnant, when they will give birth, and how many kids, because access to hospitals or medical clinics is still limited," Rana said. 
 Minnie Rivai, a resident of Palu, tried to get tested for coronavirus last month but had to jump through dozens of hoops as her fever got worse. In the end, she was struggling to breathe, and she said the healthcare facilities were at a loss as to what to do. 
 She eventually got a test. It came back negative.
 "The only way to get tested is to be stubborn," Rivai said. 
             Al Jazeera News
 	 
 https://www.aljazeera.com/news/2020/04/indonesia-palu-endured-triple-disaster-coronavirus-looms-200420061531575.html
