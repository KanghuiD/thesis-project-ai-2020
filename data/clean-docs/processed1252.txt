Coronavirus Live Blog: Shelby County COVID-19 deaths up from 26 to 28; Strickland announces loans, field hospital - The Daily Memphian
 
 You can protect yourself and help prevent the spread of coronavirus by:
 Here’s the latest from Memphis and Shelby County, below, when it comes to dealing with the novel coronavirus. To view our full coverage, check out The Daily Memphian’s  coronavirus landing page.
 And, to get breaking news delivered directly to your inbox when it happens, opt in to our Breaking News updates here.
 The Village at Germantown confirms 7 coronavirus cases
 As the week began, 119 people with the COVID-19 virus were in local hospitals and another 127 were also hospitalized and under investigation for the virus. 
 According to the Healthcare Resource Tracking System figures for Shelby County, most of the 246 were in acute care beds -- 103 “persons under investigation” -- or PUIs -- and 81 who have tested positive for the virus.
 With COVID-19 tests out, number of cases uncertain
 Of those in Intensive Care Unit beds locally, 38 tested positive for COVID-19 and another 24 were under investigation.
 And the report shows 20 of those with the virus were on ventilators with another 13 persons under investigation for a total of 33.
 Compared to numbers released by the local COVID-19 task force from Thursday, the new numbers show a significant drop in the numbers of persons under investigation in intensive care and on ventilators.
 The drop could mean that test results in the interim showed that many of those under investigation for COVID-19 were sick and critically ill for other reasons.
 Doctors at several of the city’s major medical institutions warned last week that the PUI numbers could change dramatically to reflect a rate of roughly 20% of those tested showing positive results for COVID-19.
                                             ~
 Bill Dries 
     The University of Tennessee Health Science Center will host a panel of experts Tuesday, April 14, for a webinar on Telemedicine in the time of COVID-19.  The webinar, from 11 a.m. to noon, is offered by the Department of Diagnostic and Health Sciences in the College of Health Professions. It is open to all.
 Components include regulations, documentation and practice.
 Use this link: https://zoom.us/j/988404020.  
  
 Jane Roberts 
 The Mid-South COVID-19 Regional Response Fund is distributing $156,500 this week to 10 organizations. According to the Community Foundation of Greater Memphis, the fund has awarded $977,500 since Wednesday, March 18.
 This week’s grants are as follows:
 Community Legal Center: $2,500 to provide for the increased cost of virtual Deferred Action for Childhood Arrivals (DACA) consultations
 Legacy of Legends Community Development Corporation: $10,000 to provide for increased needs of the residents of Frayser as a result of the COVID-19 outbreak 
 The PAIGE: $10,000 to provide COVID-19 related referrals and other support to LGBTQ+ communities of color 
 Room in the Inn - Memphis: $10,000 to provide medical respite for recently discharged patients seeking shelter and considered to be medically fragile 
 Society of St. Vincent de Paul: $5,000 to provide increased service due to COVID-19 to individuals experiencing homelessness 
 SRVS: $6,000 to provide food and supplies to the 55 residential homes they are serving during the COVID-19 outbreak
 Whole Child Strategies: $100,000 to provide coordinated response to the COVID-19 outbreak for families in eight neighborhoods
 The Women’s Advocacy Center: $3,000 to support five women and families affected by domestic violence since COVID-19
 Individual, corporate and foundation donors have contributed $1.6 million to the fund.
 Elle Perry 
 According to the latest update from the Tennessee Department of Health, there are 5,610 confirmed coronavirus cases statewide resulting in 109 deaths. 
 Across the state, 76,195 people have been tested. Nearly 600 people have been hospitalized for the disease and 1,671 people have been classified as “recovered.”
 The agency reported 1,331 cases in Shelby County Monday afternoon, up from the 1,269 number the Shelby County Health Department released earlier in the day. 
 Nashville’s Metro Public Health Department reported 1,385 cases and 13 deaths in Davidson County. 
  The Daily Memphian Staff 
 Today’s joint COVID-19 Task Force/Shelby County Health Department update is set to feature Memphis Mayor Jim Strickland, Memphis Fire Department Director Gina Sweat and health department officials.
 Watch below:
 During today’s joint COVID-19 Task Force/Shelby County Health Department update, Memphis Mayor Jim Strickland announced micro-loan programs -- two of them -- to “assist Memphis businesses experiencing hardship.” 
 He also announced that the second overflow field hospital will be at 495 Union, in the old Commercial Appeal offices. The former CA site will be the primary site, although work is underway at the Jackson Avenue strip retail center.
 The building that once housed The Commercial Appeal newspaper at 495 Union Ave. will be a field hospital. (Daily Memphian file)
 Strickland said the criteria on reopening businesses is the number of positive cases declining and to have ability for widespread testing -- more than we have now -- and a dramatic increase in ability to contact trace, isolate and quarantine. That’s all to ensure that hospitals are not overwhelmed. 
 If we “loosen the faucet,” Strickland said cases have to be trending in the right direction. Strickland emphasized that the U.S. does not have enough tests, he said in 6-8 weeks there will be more developments.
 Strickland said he’s talked with business owners who have “teetering” businesses. The goal is to fill gaps for them “for a little bit” to help them survive.
 The Economic Development Growth Engine for Memphis and Shelby County, is also developing another assistance program to serve a wide range of business with working capital for rent and vendor payments, etc.
 The small business resiliency loan fund will offer $5,000 to $35,000 and delay repayment for 90 days. This is for businesses denied by the SBA (Small Business Administration) or SBA lenders, must be located in distressed communities and in business for three years and certified with the city’s business diversity and compliance dept. This is a federal block grant program.
 The second loan is $2,000 to $5,000 and no interest for six months for those in the city, with less than $1 million in annual revenue and three years in businesses. 
 EDGE’s is a neighborhood emergency economic development program to be approved by the EDGE board. It will give $5,000 to $10,000 to businesses that remain open with a 25% or greater reduction in revenue and plan for 90 days of continuous operation. Also $5,000 will be given for those temporarily closed by pandemic, but that plan to reopen within 90 days and stay open for 90 days. They must be qualified for new market tax credits in that case.
 Strickland said he’s hoping to help small businesses bridge gaps. Applications open Tuesday and by early May for others, with no fees to apply for the city efforts.
 Strickland said none of the loan programs he’s outlined are “forgivable” loans. They all involve delaying payments and interest for an amount of time.
 Shelby County Health Department Director Alisa Haushalter said now 28 are dead (two new deaths). There’s a 9% positivity rate with a jump of 53 cases in a 24 hour period. Nevertheless, the rate of increase is declining in Shelby County. Rates had been doubling every eight days. Now it is 10 days.
 Haushalter on nursing home outbreaks: We anticipate we will see clusters or small outbreaks and one of the most critical is nursing homes or assisted living. Carriage Court had another death to a total of three in that facility from COVID. Village at Germantown has seven cases. Parkway facility has seven positive: five residents and two staff.
 So there are three nursing homes or assisted living facilities with confirmed cases at this point.
 Haushalter says the Health Department has to help facilities of this kind with PPE (personal protective equipment).
 Memphis Fire Department Director Gina Sweat said in 28 years as a firefighter, she’s never seen anything like this.
 Sweat is part of the COVID-19 Task Force and her top priority is enough protective equipment now and get inventory moving into the community.
 Sweat pushed the “safer-at-home” order. The Fire Department is working with University of Tennessee Health Science Center at the Fairgrounds (Tiger Lane) and will be working in outreach mobile testing into the community to be announced later this week. A Pipkin Center triage center is being worked on at Fairgrounds.
 UTHSC will also run the field hospital on Jackson Avenue.
 Sweat says Thursday and Friday the fire department will have a blitz and go door to door in The Dr. R.Q. Venson Center Apartments, 449 Beale St. Tower Thursday and set up appointments to be tested, and go from there.
 Sweat said the Fire Department began planning for the pandemic before it became an issue. She said they did ask firefighters to stretch use of masks as others on the front lines have. She said they have an adequate supply now but the supply chain has been slow. She expects a couple of large orders of PPEs to come in this week to the task force.
 The Memphis Police Department has 27 positive cases. The Shelby County Sheriff’s Office has 15 positive cases. The Fire Department has 19 positive cases, seven have recovered and returned and 250 employees were quarantined -- 42 are still in quarantine. There were also two positive cases in fire recruit class.
 The Mississippi State Department of Health reported a total of 2,942 confirmed COVID-19 cases in the state and 98 deaths.
 DeSoto County has seven new cases for a total of 178 and remains at two deaths, according to MSDH.
 MSDH reported Marshall County has one new case for a total of 34. The county remains at two deaths.
 The county is monitoring outbreaks at 58 long-term care facilities. One is in DeSoto County.
 Abigail Warren 
 Shelby County now has 1,269 confirmed cases of coronavirus as of Monday, April 13, according to the Shelby County Health Department’s latest numbers.
 At a press briefing Monday afternoon, two more deaths from complications due to coronavirus were confirmed by Shelby County Health Department Director Alisa Haushalter.  The death toll is now at 28. 
 There have been 13,692 total tests taken in Shelby County with a 9.3% positivity rate. 
 In Tennessee, there are 5,308 cases with 101 deaths as of Sunday, April 12, according to the state’s department of health.
 There have been 70,747 total tests statewide, with a positivity rate of 7.5%.
 Omer Yusuf 
 https://dailymemphian.com/article/12890/coronavirus-live-blog