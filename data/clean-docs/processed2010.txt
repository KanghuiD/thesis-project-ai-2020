Gov.ie - Ireland's response to COVID-19 (Coronavirus)
             This is a prototype - your
             feedback will help us to improve it.
         
     A virus that has not previously been seen in humans was identified in Wuhan, China in December 2019.
     In February 2020, the World Health Organization (WHO) officially named this new Coronavirus ‘COVID-19’. This type of coronavirus is also known as SARS-CoV-2. It was previously known as 2019-nCov or novel coronavirus.
     The WHO is sharing statistics
  on confirmed cases of COVID-19 (Coronavirus) around the world. The data is updated regularly.
     Coronaviruses are a large family of viruses that circulate among animals including camels, cats and bats. They can be spread from animals to humans.
     Coronaviruses cause illness in humans ranging from the common cold to more severe respiratory (lung) diseases such as Middle East Respiratory Syndrome (MERS) and Severe Acute Respiratory Syndrome (SARS).
     The first cases of the new virus were identified in people working in a seafood and live animal market in Wuhan, China.
     It is thought that humans picked up the virus from animals at the market.
     There are 10,647 confirmed cases of COVID-19 (Coronavirus) in Ireland. View analysis of confirmed cases of COVID-19 (Coronavirus) in Ireland here.
     In each case, the National Public Health Emergency Team was notified.
     The patients will remain in isolation.
     In line with our Containment Protocol, a full investigation into other people who may have been in contact with the patients will be underway. This is known as contact tracing.
     Appropriate measures aimed at preventing further spread (delay measures) will be taken.
     The general public are advised to follow advice from the Health Service Executive
  (HSE) and the Health Protection Surveillance Centre
  (HPSC) to protect their health.
     In the event of any confirmed case, a clinician will speak to the patient to get details of places they visited and the people they’ve been in contact with since they became unwell. This will provide a detailed picture of the people we need to contact, such as family members, colleagues or fellow travellers.
     This list of people will be contacted with instruction and advice on what to do if they display symptoms.
     If a member of the contact list displays symptoms, we isolate and test this individual and provide treatment, if confirmed.
     A close contact involves either face-to-face contact or spending more than 15 minutes within 2 metres of an infected person. We do not contact trace persons that may have passed by on the street or in a shop. The risk of contact in that instance is very low.
     Ireland has advanced plans in place as part of its comprehensive preparedness to deal with public health emergencies such as COVID-19 (Coronavirus). These plans have helped us to respond to previous incidents such as pandemic influenza, SARS and MERS.
     In the containment phase, the focus is on all cases no matter how mild.
     In the delay phase, initiatives are put in place to slow the spread of the virus.
     In the mitigation phase, the focus is on cases experiencing the most severe symptoms.
 Containment Strategy: In this phase, irrespective of case severity, all efforts are focused on identifying cases and their contacts early, in order to prevent further transmission (secondary spread).
 Delay Phase: Ireland is currently in a delay phase. We know that COVID-19 (Coronavirus) is contagious and many people will catch it. Our delay strategy is planned to slow down the spread of the virus. 
     This means that for example, if 1,000 people are going to catch the virus, the delay initiatives should result in 200 people per week catching the virus over five weeks, rather than 500 people per week catching the virus over 2 weeks. That way we will be able to reduce the burden on our GPs and hospitals.
 Mitigation Strategy: Ireland is prepared to initiate a second phase, if necessary, called the mitigation phase. This will be activated where containment is no longer effective in controlling the spread of COVID-19 (Coronavirus). In this phase, our focus will be on identifying the cases who are most severely unwell.
     The deployment of these strategies is in sync with global strategies, guided by the World Health Organization
  and the European Centre for Prevention and Disease Control
  (ECDC).
     The National Public Health Emergency Team, chaired by the Chief Medical Officer Tony Holohan, meets weekly to assess the international data, receive guidance regarding the outbreak and to review Ireland’s ongoing preparedness in line with advice from the WHO and the ECDC.
     Read minutes of the meetings of the National Public Health Emergency Team.
     HSE Public Health and HSE Health Protection Surveillance Centre have been monitoring the COVID-19 (Coronavirus) situation since it was notified by the WHO.
     The HSE High Consequences Infectious Diseases Planning and Coordination Group (HCID) has been working at a detailed level on this situation since early January and has put in place detailed plans and issued guidance and information in preparedness across the health service.
     An Expert Advisory Group was established in early February. The group, chaired by Cillian De Gascun, Laboratory Director at the National Virus Reference Laboratory, will provide advice to the National Public Health Emergency Team, the HSE and others on an ongoing basis.
     Ireland will continue to monitor the global developments of this new virus and respond accordingly.
     See the latest travel advice for COVID-19 (Coronavirus).
     This ethical framework includes a number of substantive ethical principles and procedural values that can be applied to, and employed during, the decision-making process in a pandemic. Ethical principles apply to the decisions that are made, whereas procedural values relate to the manner in which those decisions are made.
     This high-level framework is intended for policymakers and healthcare planners and providers in acute and community settings. It is also designed to assist clinicians in implementing the ethical principles outlined below in their clinical practice. It is not designed to guide individual clinical decisions but to assist healthcare workers in thinking through the difficult decisions that will need to be made.
     On Friday 3 April the National Public Health Emergency Team (NPHET) approved the guidance document “Ethical Considerations Relating to Critical Care in the context of COVID-19”.
     This guidance is directed at clinical staff who may be involved in making decisions regarding the prioritisation of critical care resources in the context of COVID-19.
     The Department of Health has been working intensively with the HSE during the last month to significantly increase the critical care capacity within the hospital system.
     The extensive measures introduced in recent weeks aim to “flatten the curve” and ensure that hospitals will have the necessary resources to treat those who require hospitalisation as a result of COVID-19.
 Read more
     The COVID-19 pandemic has forced us to change the usual ways we deal with the loss of a loved one. This guide is concerned with the period from the time a death occurs up to the burial or cremation. It is intended to provide clear advice as to what bereaved families might expect as they make funeral arrangements. In particular, it outlines the changes that have become necessary to what are often fundamental and time honoured rituals and processes associated with marking the passing of a loved one during the COVID-19 pandemic.
                         A Guide for the Bereaved during the COVID-19 Epidemic
                     
                             
                 Do not include any personal details in the box below.
                 The information you submit will be analysed to improve the
                 site and will not be responded to individually.
                         
 Max: [[ feedback_widget.data.text.length ]]/[[ feedback_widget.cfg.text.maxlength ]] characters
 https://www.gov.ie/en/publication/a02c5a-what-is-happening/
