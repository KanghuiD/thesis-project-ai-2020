This blogger wanted to bring Egypt's treasures to a locked down public. He was arrested | Middle East Eye
 With Egypt and much of the world locked down under the threat of coronavirus, Wael Abbas has sought to bring his country's natural and cultural treasures to an isolated public through virtual tours.
 On Monday his efforts were brought to a sudden halt when he was briefly detained while taking photos in the Wadi Degla natural reserve in Cairo.
 According to Abbas, a former activist and journalist, he left Qatameya police station on Monday evening after hours of detention, along with a number of people who were taking part in a trip to the nature spot.
 'I don’t know why the state is literally trying to destroy me and scare people off me, instead of helping me with my new and beneficial work that supports tourism and entertains people'
 - Wael Abbas
 Abbas said he was falsely accused of posting photos of individuals online without a permit, and that he had been “demonised” by authorities. 
 He added that his phone and camera have been confiscated, unlike other people who were detained with him, and he was detained separately. His request for medicine had been ignored, he said. 
 “My car was searched as if I was a drug dealer… all because I have a camera.”
 The virtual tour creator said he regretted that authorities still target him, despite his attempt to support the country’s tourism. 
 “I don’t know why the state is literally trying to destroy me and scare people off me, instead of helping me with my new and beneficial work that supports tourism and entertains people.”
 Following the countrywide curfew imposed by Egyptian authorities last month to contain the spread of coronavirus, Abbas posted a list of free virtual reality tours and panoramic pictures of major tourist attractions for people to enjoy while staying at home, particularly during the Coptic Easter holidays last weekend. 
 Below are 10 virtual experiences Abbas shared before Easter, including ancient Egyptian sites as well as some of Cairo's iconic mosques, churches and cafes. All pictures were taken before the lockdown. 
 بهو اعمدة مدخل مجموعة زوسر
 سقارة
 #planetegypt360
 ابو الهول
 داخل جامع محمد علي بالقلعة
 كنيسة القديسة بربارة
 مصر القديمة
 خان الخليلي
 داخل مقهى الفيشاوي
 تجربة لجودة ال 4K
 4K test
 مغارة العائلة المقدسة 
 (إختبأت فيها العائلة المقدسة المسيح والعذراء ويوسف النجار أثناء هربهم في مصر من بطش الرومان)
 كنيسة أبو سرجة
 القاهرة
 #planetegypt360°
 هرم سنفرو المنحني
 على يساره الهرم الاسود المنهدم (امنمحات الثالث)
 على يساره الهرم الاحمر
 خلفه في الافق الهرم المدرج (سقارة)
 الكنيسة المعلقة
 داخل مسجد الحسين
 View the discussion thread.
 https://www.middleeasteye.net/news/egypt-blogger-virtual-tours-tourist-attractions-arrested
