Coronavirus live updates: Trump stops WHO funding, accusing it of covering up outbreak  
 As the number of confirmed coronavirus cases around the world nears 2 million, with more than 125,000 confirmed deaths, President Donald Trump said he'd halt U.S. funding for the World Health Organization after the organization criticized his early response to the pandemic. 
 In the U.S., the recorded death toll topped 23,500, according to NBC News' tally.
 Los Angeles County announced on Tuesday that it'd suffered the worst day yet of the pandemic, losing 40 more lives to the disease, bringing the death toll to 360 in that metropolis.
 The toll of COVID-19 has hit no city harder than New York, and official counts in the five boroughs might even be understated. While the city's health department listed the confirmed death toll at 6,589 by 1 p.m., the "probable" number of fatalities is at least 3,778 more — which would bring the staggering total to more than 10,000, according to data obtained by NBC News.
 Here's what to know about the coronavirus, plus a timeline of the most critical moments.
 Download the NBC News app for latest updates on the coronavirus outbreak.
 The Associated Press
 New Zealand’s top officials are taking a 20 percent pay cut for six months in acknowledgment of the community’s sacrifices in dealing with the coronavirus.
 Prime Minister Jacinda Ardern says it applies to government ministers, chief executives of government organizations, and also that opposition leader Simon Bridges had volunteered to join them.
 She said it wouldn’t apply to any front-line staff like doctors or nurses.
 Ardern’s salary of $286,000 is a comparatively high amount for a country with only 5 million people.
 Dennis Romero
 A Florida man released from jail based on fears that coronavirus could spread in corrections facilities is accussed of killing someone the next day,  authorities said Tuesday.
 Edward Williams, 26, of Tampa, Florida, was arrested Monday and is facing charges of murder, gun possession, violently resisting an officer,and drug possession, the Hillsborough County Sheriff's Office in Tampa said.
 Williams was freed six days after a March 13 arrest on drug charges and is suspected in a March 20 shooting that left a man dead, sheriff's officials said. He's now behind bars with no bond.
 Read the full story here.
 Josh Lederman and Phil Helsel
 President Donald Trump's name will appear on paper coronavirus relief checks mailed to Americans as part of a massive $2 trillion package passed by Congress last month.
 A U.S. Treasury Department official confirmed Tuesday that the checks will have "President Donald J. Trump" printed on the front, but it will not be a signature.
 The Washington Post, which first reported the story, said the process of adding Trump's name to the checks could slow their delivery by a few days.
 The Treasury Department official disputed that and said there would not be any delays. The majority of coronavirus relief payments, which includes direct cash payments of up to $1,200 for individuals, are expected to go out by direct deposit, but some people will get paper checks.
 Reopening the economy is preferable to preventing a new wave of coronavirus deaths, an Indiana congressman said Tuesday.
 "It is policymakers’ decision to put on our big boy and big girl pants and say it is the lesser of these two evils," U.S. Rep. Trey Hollingsworth, R-Indiana, told radio station WIBC-FM. "It is not zero evil, but it is the lesser of these two evils and we intend to move forward that direction."
 His push for the end of isolation for much of the country aligns with President Donald Trump's desire to get the nation back to work. But medical experts, including Dr. Anthony Fauci of the National Institute of Allergy and Infectious Diseases, have said ending stay-at-home orders too soon could spark a new wave of COVID-19.
 Read the full story here. 
 SEOUL, South Korea — South Korean voters wore masks and moved slowly between lines of tape at polling stations Wednesday to elect lawmakers in the shadows of the spreading coronavirus.
 The government resisted calls to postpone the parliamentary elections billed as a midterm referendum for President Moon Jae-in, who enters the final years of his term grappling with a historic public health crisis that is unleashing massive economic shock.
 While South Korea’s electorate is deeply divided along ideological and generational lines and regional loyalties, recent surveys showed growing support for Moon and his liberal party, reflecting the public’s approval of an aggressive test-and-quarantine program so far credited for lower fatality rates compared to worst-hit areas in China, Europe and North America.
 WASHINGTON — Barbershops at some Marine Corps bases are abuzz with demand for high-and-tight haircuts.
 Despite social distancing and other Defense Department policies on coronavirus prevention, Marines are still lining up for the trademark cuts, at times standing only a foot or two apart, with few masks in sight.
 On Tuesday, Defense Secretary Mark Esper acknowledged it’s tough to enforce new virus standards with a force of 2.2 million spread out all over the world.
 Esper said he provided broad guidance about following Centers for Disease Control and Prevention guidelines and other health protections, but added he doesn’t wade into every detail, including whether or not Marines should get haircuts.
 According to the Marine Corps, barbershops at many bases are closed, and the standards on hair length have been relaxed. But at other bases, such as the massive Camp Pendleton in California, the cuts continue.
 Sarah Kaufman
 A Nebraska shopping mall with more than 80 stores and restaurants plans to reopen next week.  Nebraska Crossing Outlets in Gretna will have a “soft opening” April 24 and an official “grand opening” by May, the property owner said in a news release.  
 On Tuesday, the nation's top infectious disease expert, Dr. Anthony Fauci, said that that the country lacked the critical testing and tracing procedures needed to begin reopening the nation’s economy.
 As part of the initiative to reopen in the midst of a pandemic, the Nebraska complex will add 200 shields for workers, thermometers for every store to take employees’ temperatures and wipe and hand sanitizer stations, according to the statement. 
 There is no shelter-at-home order for Nebraska. Last week, Gov. Pete Ricketts issued an order to close theaters, barbershops, beauty salons, tattoo shops and massage businesses until April 30.
 Nebraska Crossing Outlets was never officially completely closed to the public. Some restaurants and stores have been providing curbside pickup. It is unclear how many businesses will participate in the planned reopening.
 Tim Stelloh
 Major League Baseball confirmed Tuesday that 27 teams will participate in a study looking for COVID-19 antibodies among club employees and their relatives.
 The Athletic first reported that 10,000 volunteers will participate in the study, which will be conducted with the Sports Medicine Research and Testing Laboratory, Stanford University and the University of Southern California.
 The study will aim to measure the prevalence of COVID-19 among people across the United States by testing for a blood protein that the body creates in response to the infection, the Athletic reported.
 The Athletic, citing Stanford researcher Jay Bhattacharya, reported that players, families, team staff, concessionaires, ushers and other part-time employees of all ages, backgrounds and genders will participate.
 https://www.nbcnews.com/health/health-news/live-blog/2020-04-14-coronavirus-news-n1183181/ncrd1184011
