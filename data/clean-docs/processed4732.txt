A Review of the Most Promising Strategies for Defeating Coronavirus: Vaccines, Antivirals, Gene Therapies
 
 By Frontiers
 April 23, 2020
 In an unprecedented effort, hundreds of thousands of researchers and clinicians worldwide are locked in a race against time to develop cures, vaccines, and better diagnostic tests for COVID-19, the illness caused by the virus SARS-CoV-2.
 Over 1,650 articles on COVID-19 are already listed in databases such as Google Scholar, while dozens more are added daily. The register ClinicalTrials.gov lists over 460 ongoing clinical trials on COVID-19, although the majority are still in the earliest stages. Given the diversity of experimental approaches among these studies, a systematic review of possible clinical strategies is timely and welcome.
 In a new study in Frontiers in Microbiology, aimed at the research community but also comprehensible for non-specialists, experts from the University of North Carolina at Chapel Hill review possible strategies against dangerous coronaviruses — not only SARS-CoV-2 and its relatives such as SARS-Cov (causing Severe Acute Respiratory Syndrome, SARS) and MERS-Cov (causing Middle East Respiratory Syndrome, MERS), but also as yet unknown strains which will inevitably emerge in the future.
 They propose that the most promising approaches for fast progress are selected antivirals such as remdesivir, and gene therapy.
 “Coronaviruses represent a true threat to human health and the global economy. We must first consider novel countermeasures to control the SARS-Cov-2 pandemic virus and then the vast array of high-threat zoonotic viruses that are poised for human emergence in the future,” says Dr Ralph Baric, William R. Kenan, Jr. Distinguished Professor in the Department of Epidemiology and Professor in the Department of Microbiology and Immunology at UNC Chapel Hill.
 “To help focus the global search for a treatment, we here aim to provide a comprehensive resource of possible lines of attack against SARS-Cov-2 and related coronaviruses, including the results from all preclinical and clinical trials so far on vaccines against SARS and MERS.”
 The authors discuss one-by-one the possible strategies against the coronavirus. First, and most effective are vaccines.
 In the present case, the most successful are likely to carry the Receptor Binding Domain (of the virus’s S-protein), which allows it to bind to and fuse with host cells. Besides the traditional live attenuated, inactivated, and subunit-based vaccines, modern types such as DNA/RNA-based and nanoparticle- or viral vector-borne vaccines should be considered.
 Because the amino acid sequence of the S-protein is very different across coronaviruses (e.g., 76-78% similarity between SARS-Cov and SARS-Cov-2), vaccines against one strain typically won’t work against another.
 But because the development and testing of new vaccines takes one to several years, other approaches are essential in the meantime.
 The second-most likely effective are broad-spectrum antivirals such as nucleoside analogs, which mimic the bases in the virus’s RNA genome and get mistakenly incorporated into nascent RNA chains, stalling the copy process.
 But because coronaviruses have a so-called “proofreading” enzyme which can cut such mismatches out, most nucleoside analogs don’t work well. Exceptions seem to be β-D-N4-hydroxycytidine and remdesivir, proposed by the authors as good candidates against SARS-Cov-2.
 Third, convalescent blood plasma from patients who recovered, with low levels of a range of antibodies against the virus; or preferably (but slower to develop), monoclonal antibodies, isolated and mass-produced through biotechnology. Such “passive immunization” can give short-term immunity.
 The authors discuss a range of options from fusion inhibitors, to inhibitors of human proteases, to immune modulators such as corticosteroid hormones, and others.
 Finally, and in the authors’ view the most attractive alternative until a vaccine is produced, is gene therapy delivered through the adeno-associated virus (AAV). This would entail the fast, targeted delivery of antibodies, immunoadhesins, antiviral peptides, and immunomodulators to the upper airways, to give short-term protection. Because the rapid turnover of cells here, risks of toxicity are minimal. They estimate that such tools can be developed, adapted, and tested within a month.
 “AAV-based passive immunization can be used as a quick alternative. It is straightforward and only contains two components, the viral vector and the antibody. Multiple AAV vectors have been proven to be safe and effective for human use,” says author Dr Long Ping Victor Tse.
 “In theory, a single dose could mount a protective response within a week and last for more than a year. The currently high price could be reduced when treating infectious diseases, which have a larger market. It may or may not already be too late to use AAV to treat SARS-CoV-2, but it is certainly not too late for future outbreaks.”
 Reference: “The Current and Future State of Vaccines, Antivirals and Gene Therapies against Emerging Coronaviruses” by Longping V. Tse, Rita M. Meganck, Rachel L. Graham and Ralph S. Baric, 24 April 2020, Frontiers in Microbiology.
 DOI: 10.3389/fmicb.2020.00658
 Health
 Science
 Name 
 Website
  
 April 21, 2020
 Just like we orbit the sun and the moon orbits us, the Milky Way has satellite galaxies with their own satellites. Drawing from data on…
 April 20, 2020
 April 19, 2020
 https://scitechdaily.com/a-review-of-the-most-promising-strategies-for-defeating-coronavirus-vaccines-antivirals-gene-therapies/
