Hungry? Want to eat healthy? These tasty meals can be delivered to your door during the coronavirus - oregonlive.com
 Portland-based Farm to Fit delivers healthy, ready-to-eat meals
 Are you hungry for new tastes while hunkering in your home during the coronavirus? Local restaurants and meal-kit delivery companies will bring dinner to your front door, and most grocery stores, like New Seasons and Whole Foods, offer take-and-bake options.
 Another reason: The U.S. Food and Drug Administration reports it has found no evidence that the coronavirus can be spread via food or food packaging.
 Other benefits to letting professionals figure out recipes and ingredients: You save time otherwise spent creating a menu and then shopping for it. You’re introduced to new spices, food and cuisines, and portions are a healthy size.
 Customers of Portland-based Farm to Fit brag they’ve lost weight.
 Portland-area restaurants open for takeout or delivery
 We're tracking which Portland-area restaurants are offering takeout or delivery
 Farm to Fit, which was launched eight years ago by G. Scott Brown and Dre Slaman, makes and delivers nutritious, ready-to-eat meals that are portion-controlled by calories and sizes, and based on low-carb, keto, paleo and other healthy food programs.
 The husband-and-wife owners and their staff service clients living in a wide radius around Portland, from Camas, Washington, to Salem. “It’s a big circle,” says Slaman.
 Customers can order a variety of heat-and-serve dishes, from three to 21 a week, delivered to their door on Sundays and Wednesdays (offices on Mondays). Meals are also available to be ordered and picked up at Farm to Fit’s commercial kitchen at 5411 N.E. Portland Hwy. and other locations.
 Slaman says regular clients are now ordering more meals than before because they’re home all day. New customers like that the company offers meal plans without a long-term commitment or forced subscription.
 “We have become not only their solution to not leaving their house but also their source for food,” says Slaman, who purchases from local farms and producers. “Have you seen the empty shelves at the stores? My husband and I feel we provide a valuable service and we are so appreciative we’re in this business.”
 Farm to Fit is currently making up to 100 more meals a day to allow people to order extra as they need it for family, friends and neighbors, says Slaman. “We’re adjusting our system as we move forward. We will get meals to people.”
 Price: Starting at $14 per serving for a 1,200-calorie dinner. There are also 1,600- and 2,000-calorie choices (gift cards are available)
 Pitch: Spend less time cooking and more time enjoying life
 Sample menus: Zucchini & Yellow Squash Omelet with basil, brie, roasted red pepper; Shrimp Arrabbiata with fennel, zucchini, red bell pepper and orecchiette; and Chicken Picatta with lemon-caper sauce, penne, broccoli and summer squash
 Meal-kit delivery companies have drivers who drop off insulated, cold boxes filled with ingredients and recipe cards for customers to prepare a menu they have selected from a list of choices.
 Here are five such online companies that source from sustainable farms and avoid non-GMO ingredients. Fish is most often certified a Best Choice or a Good Alternative by the Monterey Bay Aquarium’s Seafood Watch.
 Menus generally can be ordered in portions for two or four people, and include dishes for vegetarians and pescetarians. Some can be customized for people who don’t eat shellfish, pork or lamb, or have other dietary considerations such as diabetes. There are also choices for those that follow WW and other wellness program’s guidelines.
 Blue Apron
 Blue Apron is the best known meal-kit company in the U.S. Recipes are simple with step-by-step instructions, which might annoy seasoned home chefs but are appreciated by novice cooks and people who just want an easy activity.
 Price: Starting at $7.49 per serving
 Pitch: By cooking with Blue Apron, you’re helping to build a food system that’s better for everyone
 Sample menus: Coconut Curry Wonton Noodles with zucchini, carrots and Poblano pepper; Ginger Pork Meatballs with bok choy and brown rice; and Seared Chicken and Creamy Italian Dressing with mashed potatoes and balsamic-glazed vegetables
 Marley Spoon features Martha Stewart's recipes and cooking techniques.
 Martha & Marley Spoon: Martha Stewart’s recipes require some cooking experience, but that effort is evident in the edible outcome. There are 22 seasonal recipes offered each week that are made with ingredients from family-owned businesses and producers. Categories include Health & Diet, Vegetarian & Vegan, Meat & Fish, Under 30 Minutes, and Family-Friendly.
 Price: Around $10.49 per serving plus $8.99 shipping
 Pitch: A smarter way to cook: How does 6 steps and less than 30 minutes for a chef-worthy meal sound?
 Sample menus: Crispy Pork Katsu & Rice with bok choy and Japanese barbecue sauce; Salmon & Veggie Mixed Grill with parsley oil and red pepper pesto; and Pear-Tahini Date Shake and Pomegranate, Raspberry & Mint Smoothie
 Sun Basket
 Sun Basket: Did your doctor suggest you avoid certain foods or are you eating strictly paleo or organic? This service makes it easy to find these and other gluten- or dairy-free meals oven-ready, pre-prepped or with ingredients and easy-to-follow recipes. Lean & Clean choices have less than 600 calories a serving.
 Price: Starting at $10.99 per serving
 Pitch: Eat well and shine with easy, delicious dinners now ready in as little as 5 minutes
 Sample menus: Dairy-free Spicy Chinese Zhajiang Noodles with pork and shiitake mushrooms; Carb-Conscious Crustless Chicken Pot Pie with mushrooms and carrots; and Soy-free Mediterranean turkey meatballs with red pepper–cashew crema
 HelloFresh
 HelloFresh acquired Green Chef, a certified organic meal-kit delivery company with offerings created to follow paleo, gluten-free, vegan and keto programs. HelloFresh is the largest meal-kit provider in the United States, and also operates in Canada, Western Europe, New Zealand and Australia, according to the company. Customers are able to skip weeks, swap recipes and cancel anytime.
 Pitch: Cook it. Love it. Tag it #HelloFreshPics
 Sample menus: Crispy Chickpea Tabbouleh Bowl with creamy feta dressing; BBQ Pineapple Flatbread with caramelized and pickled onion; and Pork Cutlets with a cranberry pan sauce and rosemary-roasted root veggies
 Home Chef
 Home Chef delivers pre-portioned ingredients and recipes for veggie, low-carb and calorie-conscious meals.
 Price: Starting at $6.99 per serving
 Pitch: Effortlessly create and plate your meals in no time. Dinner is solved!
 Sample menus: Garlic-Parmesan Crusted Filet Mignon with red wine sauce, roasted red potatoes and parsnips; no-cooking-required Apple & Chicken Salad; and Crispy Teriyaki Tofu Tacos with jalapeño
 How you can help local businesses 
 How can local businesses survive? They need your help.
 --Janet Eastman | 503-294-4072
 jeastman@oregonian.com | @janeteastman
 CORONAVIRUS IN OREGON: THE LATEST NEWS
 Note to readers: if you purchase something through one of our affiliate links we may earn a commission.
 Community Rules  apply to all content you upload or otherwise submit to this site.
 Ad Choices
 https://www.oregonlive.com/coronavirus/2020/03/hungry-want-to-eat-healthy-these-tasty-meals-can-be-delivered-to-your-door-during-the-coronavirus.html
