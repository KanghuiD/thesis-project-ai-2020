Coronavirus (COVID-19) update for Quad Cities businesses and employers | Quad Cities Chamber
 There is a lot of new information to process and respond to during these rapidly evolving and uncertain times. The Quad Cities Chamber is here for you – advocating for your organization at the state and fedleral levels and providing resources to help you protect the health of your workforce and operations. We are committed to bringing you the latest information as quickly as possible. We will get through this together.
 This page will be updated regularly with pertinent information for businesses in the Illinois and Iowa Quad Cities region.
 Helpful hint: use “CTRL F” to search this page by keyword.
 The Quad Cities Chamber and Visit Quad Cities in collaboration with Tri-City Electric Co. launched QC Local Challenge to encourage Quad Citizens to safely support businesses throughout the region amid the coronavirus pandemic. Participants who complete a bingo card filled with a dozen fun ways to show love for area businesses become a "Local Champion" and can enter to win one of four staycation prize packages. 
 To-go menu guide and and gift card directories
 Two of the QC Local Challenge bingo categories – "carry-out" and "dining delivered" – promote supporting restaurants. 
 The Chamber on April 15 hosted a webinar with Media Link, Inc. to equip small business owners with the tools to sustain their marketing efforts during COVID-19 and any other disruptive time.
 Grants for minority-owned businesses
 SheaMoisture and We Buy Black created a Business Relief Fund to support female entrepreneurs of color and minority-owned businsses affected by COVID-19. Grants of up to $5,000 will be awarded to companies supporting and reaching out to their communities in innovative ways. 
 Check the status of your Economic Impact Payment
 The Treasury Department and the Internal Revenue Service unveiled an online tool to help people access their Economic Impact Payments. 
 Illinois liquor license update
 Illinois-issued liquor licenses that expire March 31, April 30 and May 31 were extended to July 31. License holders are encouraged to renew their licenses as soon as possible through their MyTax Illinois Account.
 City of Rock Island Small Business Grant Program
 The City of Rock Island this week designated $250,000 for its Small Business Grant Program.
 Modern Piping has a solution to keep employees and customers healthy and safe during the coronavirus pandemic. Learn more about Hands-Free Door Pulls. 
 In a Quad-City Times guest editorial published Tuesday, April 14, Chamber President & CEO Paul Rumler issues a series of thank-yous on behalf of the business community and spells out how the Chamber is pivoting its priorities to help businesses during the COVID-19 crisis.
 The Quad Cities Chamber continues to monitor the talks coming out of Washington, D.C., related to more relief for businesses. We also are monitoring the states of Iowa and Illinois for their plans on how to start lifting the restrictions once the curve starts to flatten.
 Numerous nonprofit organizations with a 501c6 designation, including business leagues, real estate associations and chambers of commerce, currently do not qualify for financial assistance programs offered through the Paycheck Protection Program. The Chamber would like your support for the inclusion of 501c6 organizations in the next phase of federal funding. Let our elected officials know through the Chamber's Advocacy Action Center.
 For Spanish speakers
 RSM has created resources to help navigate the Federal Reserve's $600 billion lending program launched April 9 to help small- and medium-sized businesses deal during the coronavirus pandemic. Download RSM’s Business Guide to the Main Street Lending Program.
 Quad-City Times - Buy Local gift card directory
 The Quad-City Times this week launched a new online marketplace to help drive support to Quad Cities businesses during this difficult time. Businesses, including those temporarily closed due to COVID-19, may join the Buy Local gift card directory for free by filling out this form.
 TapOnIt - Local Lists and gift card directory
 TapOnIt created Local Lists as a free resource for people to find current hours, menus, purchasing options and ways they can help support locally owned businesses across the Quad Cities during the COVID-19 pandemic. If your business is not listed, make sure to Join The List. Every Quad Cities business on the list will have a chance to win a free marketing campaign from TapOnIt.
 In addition, TapOnIt is promoting gift card sales for businesses to 70,000 Quad Cities area residents by text, stressing the importance of supporting locally owned establishments. Contact Jennifer at TapOnIt to feature your business in these promotions. Chamber members, Downtown Davenport Partnership and Downtown Bettendorf Organization businesses get an exclusive discounted $50 rate. Download the flyer with all the details. The Chamber is proud to partner on this effort.
 As the coronavirus pandemic continues, Congress will likely pursue additional action to support businesses affected by COVID-19. In this next phase, the Chamber anticipates Congress will explore some of the shortfalls of the CARES Act and seek an expansion of that law. The Chamber continues to advocate for its members’ needs with the Quad Cities Congressional delegation.
 The Chamber is hosting a virutal conversation with U.S. Sen. Tammy Duckworth at 2 p.m. Tuesday, April 14, about programs in place as part of the federal economic stimulus package signed into law. The Coronavirus Aid, Relief and Economic Security (CARES) Act allocates  $350 billion to help small businesses keep workers employed amid the pandemic and economic downturn, among other things.
 Advance registration is required to participate in this webinar, sponsored by Exelon.
 More than $24 million in grants announced for Iowa businesses
 The Iowa Economic Development Authority (IEDA) this week announced more than $24 million in grants for 1,000-plus businesses through the Small Business Relief Program.
 Iowa Dept. of Public Health issues new guidance for businesses
 The Iowa Department of Public Health has issued additional guidance for businesses to keep employees and customers safe during this time. The guidance includes information to consider to prevent, detect and manage outbreaks that may occur in a business, facility or manufacturing plant. The guidance includes a link to a screening algorithm to be used to check employees for symptoms of COVID-19. It also asks businesses to electronically report to the Iowa Department of Public Health when 10% or more of their employees are reporting COVID-19 symptoms. Business names will not be shared publicly. 
 Scott County Health Dept. message for businesses
 "Thank you for all that you are doing to keep your employees and customers safe during this time. We recognize the challenges that COVID-19 is creating for businesses and appreciate the steps that you are taking to change operations, close, social distance, encourage handwashing, screen your employees and the list goes on. Your actions are crucial to helping stop the spread of COVID-19 in our community and to save lives."
 Beginning at 7:30 a.m. Monday, April 13, the Center for Industrial Research & Service (CIRAS) is hosting 30-minute virtual roundtables for manufacturers to share and discuss:
 Interested manufacturers may join the discussion through Zoom. Contact Glenn Volkman at gvolkman@iastate.edu or 515-205-3786 with any questions or needs. 
 The Federal Emergency Management Agency (FEMA) steps into provide support and resources when state resources are overtaxed due to a declared disaster. Per FEMA’s website, "Emergency response is most successful when it is locally executed, state-managed and federally supported.”
 Interested businesses should properly register in the appropriate databases, such as the System for Award Management and FEMA’s Industry Liaison Program. 
 The Quad Cities Chamber is seeking webinar presenters and topics for its upcoming Virtual Member Resource Series. This is an opportunity for Chamber members to provide timely presentations in their area(s) of expertise to help fellow members survive, stabilize or recover from the COVID-19 pandemic.
 In alignment with the Chamber’s areas of work, we are seeking presentations on the following topics:
 Webinar requirements/overview:
 To be considered, please forward the following information to Marcy Hyder at mhyder@quadcitieschamber.com:
 The Chamber appreciates all submissions. Approved programs will be contacted to schedule a presentation date. The Chamber may charge registrants a fee to participate in these events.
 The Chamber is hosting a webinar with Media Link, Inc. at 1 p.m. Wednesday, April 15, to equip small business owners with the tools to sustain their marketing efforts during COVID-19 and any other disruptive time.
 Presenters Adrian Vander Wille and Natalie Linville-Mass from Media Link will share a five-step process that introduces:
 The process can be applied by any small business owners no matter their experience in marketing. The presentation will also provide real-world examples and resources for attendees.
 Advanced registration is required to participate in the webinar.
 The Chamber on April 9 hosted a virtual conversation with U.S. Rep. Dave Loebsack about federal financial assistance programs available for businesses affected by COVID-19, sponsored by COVID-19. 
 The U.S. Department of Treasury released an updated FAQ document to address common inquiries and requests for information pertaining to the Paycheck Protection Program (PPP).
 The State of Iowa this week announced two rounds of grant recipients for the Small Business Relief Program. 
 Iowa Department of Public Health released COVID-19 Outbreak Guidance for Businesses.
 The State of Illinois awarded $14 million in emergency grants to more than 700 small bars, restaurants and hotels.
 On behalf of the Quad Cities COVID-19 Coalition, the Chamber has encouraged area businesses to donate their extra personal protective equipment (PPE) to the Salvation Army amid critical supply shortages created by the pandemic.
 The business community’s response to the coalition’s call has been astonishing. Read the full story about Quad Cities businesses stepping up during this time of need. 
 The Chamber is presenting a virtual twist on the networking event favorite – Business Exchange.
 This event (normally held over lunch) will be hosted via Zoom at 11 a.m. Thursday, April 16. Rick Davis, Davis Presentation Group LLC, will facilitate this virtual networking event and share tips and strategies on how to become more confident and comfortable networking in any environment.
 The program will feature a series of virtual breakout rooms, allowing participants to connect with other Chamber members in small groups. Now, more than ever, connections are vital to grow business and professional opportunities. Business Exchange is all about relationship building and establishing referrals with other Chamber members.
 What to expect:
 Register by noon Wednesday, April 15.
 This event is only open to Chamber members, and this first virtual Business Exchange will be free to attend.
 The U.S. Small Business Administration's Minnesota District Office is hosting a webinar in Spanish at 6 p.m. Thursday, April 9, to give an overview of the SBA's Economic Injury Disaster Loan (EIDL) and the EIDL Emergency Advance. 
 The Red Backpack Fund is offering a $5,000 grant to assist majority women-owned businesses and nonprofits affected by COVID-19. Applicants must meet the following criteria: 
 A new interactive resource guide to help businesses, entrepreneurs and nonprofits during the COVID-19 pandemic is live on the Chamber's website.
 The guide, which has numerous categories and a search function, will be updated regularly with reliable and accurate information as new resources are announced. This blog also will continue to be updated daily with the latest local, state and federal COVID-19 news and information for businesses, including details about upcoming virtual events. 
 The Chamber strives to be your one-stop business resource, providing you the latest updates to help you protect the health of your workforce and operations. 
 Explore the Chamber's COVID-19 Resource Guide.
 Chamber conversation with Rep. Dave Loebsack
 The Chamber is hosting a virtual conversation with Rep. Dave Loebsack at 1 p.m. Thursday, April 9, about federal financial assistance programs for businesses affected by COVID-19. The CARES Act allocates $350 billion to help small businesses keep workers employed amid the pandemic and economic downturn, among other things.
 Illinois DCEO/Illinois Chamber webinar
 Business owners in Illinois are invited to learn more about federal and state funding programs available to them during a webinar from 3-4:30 p.m. Thursday, April 9. This event is presented by the Illinois Department of Commerce and Economic Opportunity (DCEO) in partnership with the Illinois Chamber of Commerce. This is an opportunity for business owners to ask Illinois DCEO Director Erin Guthrie and her team any funding-related questions.
 SCORE Q&A
 Join SCORE’s expert business mentors for a live Q&A webinar at 12 p.m. Thursday, April 9. Presenters will address questions and concerns regarding the current COVID-19 crisis to help businesses adapt and weather the storm.
 Beginning April 10, independent contractors and self-employed individuals may apply for funds through the U.S. Small Business Administration's Paycheck Protection Program (PPP).
 Express Bridge Loans
 The Express Bridge Loan Pilot Program allows small businesses that already have a business relationship with an SBA Express Lender to quickly access up to $25,000 to overcome the temporary loss of revene due to COVID-19. These loans can provide support while the business applies for the SBA's Economic Injury Disaster Loan (EIDL).
 EIDL Emergency Advance
 The EIDL Emergency Advance provides up to $10,000 of economic relief to small businesses experiencing temporary difficulties due to COVID-19. Funds will be made available following a successful EIDL application. 
 Debt Relief
 As part of the SBA's debt relief efforts, 
 Chamber President & CEO Paul Rumler has a new message for Quad Cities businesses:
 "We're preparing everything that we can to help you through this very challenging and unique circumstance of responding to COVID-19," Rumler says in the video below.
 "If you don't know where to go, please contact the Chamber. We have a team that is positioned to: 
 Thank you for all you're doing. We'll get through this together."
 
 All contributions will support the Quad Cities Disaster Recovery Fund at the Quad Cities Community Foundation. 
 “Coming together in times of need – and in times of joy – is who we are as Quad Citizens,” said Sherry Ristau, president and CEO of the Quad Cities Community Foundation. “This fund was activated nearly three weeks ago because community leaders, including the United Way of the Quad Cities and Regional Development Authority, had the foresight that we were going to need to lift up and rally behind vulnerable people that could be impacted. In a short period of time, nearly $650,000 has been raised—and we’ve received more than $2.5 million in grant requests.”
 Ways to give:
 The Quad Cities Chamber is hosting a virtual conversation with Rep. Dave Loebsack at 1 p.m. Thursday, April 9, about federal financial assistance programs for businesses affected by COVID-19. The CARES Act allocates $350 billion to help small businesses keep workers employed amid the pandemic and economic downturn, among other things.
 At 10 a.m. Tuesday, April 7, Rep. Cheri Bustos is hosting a webinar about federal financial relief programs for small businesses in Rock Island, Henry, Whiteside and Mercer counties.
 Businesses interested in joining this webinar must fill out this form.
 More business closures
 To encourage further social distancing and mitigation efforts for COVID-19, Iowa Gov. Kim Reynolds on April 6 announced additional business closures effective at 8 a.m. Tuesday, April 7, through April 30: malls; tobacco or vaping stores; toy, gaming, music, instrument, movie or adult entertainment stores; social and fraternal clubs, including those at golf courses; bingo halls, bowling alleys, pool halls, arcades and amusement parks; museums, libraries, aquariums and zoos; race tracks and speedways; roller or ice skating rinks and skate parks; outdoor or indoor playgrounds or children’s play centers; and campgrounds. In addition, all unsolicited door-to-door sales are prohibited.
 Previously announced closures, including the ban on gatherings of more than 10 people, are still in effect through April 30. The proclamation also calls on law enforcement to assist in the enforcement of these mitigation efforts.
 Iowa CARES Act benefits and unemployment claims
 Iowa Unemployment Insurance Tax Extension
 Eligible employers have additional time to make first quarter unemployment insurance contributions due to COVID-19. The first quarter contributions, due April 30, will be delayed until the end of the second quarter, July 31. To be eligible for the delay, an employer must:
 Employers must notify the Iowa Department of Workforce Development of their intent to delay payment by 4:30 p.m. Friday, April 24. 
 Iowa Restaurant Employee Relief Fund
 The Iowa Restaurant Association created an industry relief fund to help hospitality workers displaced by the recent suspension of onsite dining in Iowa restaurants and bars.
 Childcare assistance for essential workers
 Illinois Humanities COVID-19 Emergency Relief Grant
 Illinois Humanities is providing $2,500 general operating grants to humanities and cultural nonprofits outside the City of Chicago with annual budgets of $300,000 or less.
 #AllinIllinois
 Illinois Gov. J.B. Pritzker recently announced a new campaign called "All in Illinois." The state has enlisted Illinoisans across the state, including notable celebrities, to remind residents to stay home, stay safe and stay healthy. The state has developed an #AllinIllinois toolkit with key messaging, sample social media posts and more. 
 The Office of Inspector General is alerting the public about potential fraud schemes related to economic stimulus programs offered by the U.S. Small Business Administration (SBA).
 Quad Cities Chamber President & CEO Paul Rumler shares five ways Quad Citizens can help during the COVID-19 pandemic. "It’s important that we’re in this together because we can win this together.”
 The Chamber encourages businesses and nonprofit organizations to remain patient as they work with their bank or credit union to prepare their application materials for the Paycheck Protection Program. While today marks the program’s launch, lending institutions that will be processing loan applications are still waiting on detailed guidance from the U.S. Small Business Administration (SBA). Our regional financial institutions are working tirelessly around the clock to fulfill this important responsibility given to them by Congress, and they are committed to helping businesses during this difficult time.
 How to calculate the maximum amount you can borrow
 For more details, see pages 5-19 of this SBA report, "What do borrowers need to know and do?"
 ALSO: U.S. Chamber of Commerce guide on the SBA's COVID-19 EIDL, including information on $10,000 emergency grant.
 Under the federal CARES Act and Response Act, separate refundable tax credits for paid wages are available for many businesses financially affected by COVID-19. Employers may receive both tax credits for paid wages under these acts, but the same wages cannot be counted for both credits.  
 CARES Act tax credit
 Under the CARES Act, Employee Retention Credit is available for many businesses, but it cannot be used if a Paycheck Protection Program loan is granted.  
 Employee Retention Credit details:
 Go to the IRS website for more information. 
 Response Act tax credit
 The Response Act requires certain employers to pay sick leave or family leave wages to employees who are unable to work or telework due to COVID-19. Employers are entitled to a refundable tax credit for the required leave paid, up to specified limits. 
 Recent guidance from the Department of Labor and the IRS clarifies what employers need to request from employees to substantiate the need for leave and what they need to justify their efforts to secure applicable tax credits.
 What employers should require when employees request FFCRA leave
 IRS FAQs about refundable tax credits
 The Chamber on April 3 hosted a virtual Q&A session with Sen. Chuck Grassley about the new federal economic stimulus package signed into law late last week. The CARES Act allocates $350 billion to help small businesses keep workers employed amid the pandemic and economic downturn, among other things.
 Those using Zoom for virtual meetings and events should follow these helpful tips on how to keep uninvited guests out of your Zoom event.
 Dr. Linda Wastyn, President of Wastyn & Associates, is presenting a webinar next week for the Quad Cities Chapter of Association of Fundraising Professionals. 
 The National Alliance on Mental Illness (NAMI) of Des Moines is hosting a free eight-week speaker series, featuring some of the top motivational speakers in the country.
 To minimize the spread of COVID-19 in the Quad Cities, mayors and other community leaders from throughout the region are encouraging Quad Citizens to do their part by staying home as much as possible.
 The Chamber is proud to support this Together Quad Cities promotion, an initiative of the Quad Cities COVID-19 Coalition.
 The Quad Cities Chamber on April 2 hosted a virtual panel presentation about new federal financial assistance programs being implemented under the CARES Act.
 Most of the webinar focused on the SBA’s $349 billion Paycheck Protection Program that launches Friday, April 3. Representatives from the U.S. Chamber of Commerce, Wells Fargo Bank, Quad Cities SCORE and the Small Business Development Center provided an overview of the program, application process, what businesses should do now to prepare and financing options.
 What you need to know as you watch this recording:
 Beginning April 3, businesses and nonprofits may apply for a forgivable 1% interest loan with a two-year term through local SBA-certified lenders participating in the program. However, lenders are still waiting on detailed program information from the SBA, so the Chamber encourages businesses and entrepreneurs to contact area SBDCs, Quad Cities SCORE and their bank or credit union for assistance.
 See the Chamber’s blog update from April 1 for more information about the Paycheck Protection Program, including loan forgiveness details and Treasury Department resources.
 The Chamber is hosting a virtual Q&A session with Sen. Chuck Grassley at 9 a.m., Friday, April 3, about the new federal economic stimulus package signed into law late last week. The CARES Act allocates $350 billion to help small businesses keep workers employed amid the pandemic and economic downturn, among other things.
 Advanced registration is required to participate in the webinar, sponsored by Exelon.
 To slow the spread of COVID-19, Iowa Gov. Kim Reynolds on April 2 extended the closure of schools and these non-essential businesses through April 30: restaurants and bars (except delivery and takeout); fitness centers; swimming pools; salons; medical spas; barbershops; tattoo establishments; tanning facilities; massage therapy establishments; theaters; casinos and gaming facilities; senior citizen centers and adult day care centers; and other nonessential retail establishments.
 Illinois Gov. J.B. Pritzker on April 1 announced the Arts for Illinois Relief Fund. The program will provide financial assistance to artists, artisans and cultural organizations affected by COVID-19.
 Quad Citizens who have experienced change in employment status due to COVID-19 may be eligible to enroll in Affordable Care Act coverage or other coverage plans, the Iowa Insurance Division reports.
 The $2 trillion stimulus package provides penalty-free early retirement account distributions and increases 401(k) loan limits.
 Nick Westergaard, who presented earlier this year at a Chamber Edge Business series event, shares these marketing tips to remain relevant during this fast-moving media environment: Market Like Mister Rogers.
 Program overview
 The U.S. Treasury Department and Small Business Administration (SBA) on March 31 unveiled details of the $349 billion Paycheck Protection Program, including instructions for businesses to apply for funds. Established by the federal CARES Act, this program is designed to help businesses keep their workforce employed during the COVID-19 crisis. 
 Who, what, when?
 Beginning Friday, April 3, businesses with fewer than 500 employees, including sole proprietors, private nonprofit organizations and veterans organizations, may apply for forgivable 1% interest loans of up to $10 million. Beginning April 10, independent contractors and self-employed individuals may apply for funds. 
 How to apply
 The Chamber encourages businesses and entrepreneurs to contact area Small Business Development Centers and their bank or credit union for immediate assistance.
 Sample Paycheck Protection Program application
 For additional support, register for the Chamber's virtual panel presentation about this program and others on April 2.
 Loan forgiveness
 The SBA will forgive loans if the funds are used to cover payroll costs, including benefits, rent, mortgage interest and utilities for eight weeks after the loan is made and all employee and compensation levels are maintained. Due to likely high subscription, at least 75% of the forgiven amount must be used for payroll.
 More Treasury Department resources
 The Quad Cities Chamber is hosting a virtual panel presentation from 12-1 p.m. Thursday, April 2, about new federal financial assistance programs being implemented as a result of the CARES Act, including the SBA's:
 The webinar will provide an overview of the federal legislation, the application process, what businesses should do to prepare, application pre-work and financing options. There also will be a Q&A portion. The panelists are: 
 Advanced registration is required to participate. 
 The Chamber hosted a conversation with U.S. Sen. Joni Ernst on April 1 to discuss the new federal economic stimulus package signed into law late last week. The CARES Act allocates $350 billion to help small businesses keep workers employed amid the pandemic and economic downturn, among other things.
 https://quadcitieschamber.com/news/blog/covid-19