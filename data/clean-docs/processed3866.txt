Bangkok Post: Opinion channel
 One of the most common complaints arising from the current self-isolation is fighting off boredom. This is especially the case for kids, even though they have smartphones and other electronic gadgets with which to amuse themselves. I don't envy parents of young children.			
 A famous quote, "Numbers never lie", comes to my mind often as the country copes with the coranavirus outbreak which all of a sudden has some Thais obsessed with numbers and case counting.			
 The erosion of free expression continues in Thailand. In Reporters Without Borders (RSF)'s annual 2020 World Press Freedom Index, Thailand dropped four spots, ranking 140th out of 180 countries on the open media barometer, placing it one spot below Myanmar.			
 Editorial
 Thitinan Pongsudhirak
 Ploenpote Atthakor
 Anchalee Kongrut
 Sanitsuda Ekachai
 Atiya Achakulwisut
 Veera Prateepchaikul
 Kavi Chongkittavorn
 Chartchai Parasuk
 New York Times
 One of the most common complaints arising from the current self-isolation is fighting off boredom. This is especially the case for kids, even though they have smartphones and other electronic gadgets with which to amuse themselves. I don't envy parents of young children.
 A famous quote, "Numbers never lie", comes to my mind often as the country copes with the coranavirus outbreak which all of a sudden has some Thais obsessed with numbers and case counting.
 The erosion of free expression continues in Thailand. In Reporters Without Borders (RSF)'s annual 2020 World Press Freedom Index, Thailand dropped four spots, ranking 140th out of 180 countries on the open media barometer, placing it one spot below Myanmar.
 A recent fire at the zoo operated by the Pata department store where "Bua Noi", or "Little Lotus" the gorilla is being kept in captivity serves as a wake-up call regarding the safety of an animal who has been there for most of her life.
 If things go as planned, we will see parts of the country reopened in less than a week after the continuing diminution of coronavirus cases.
 Malaysia is back at it again, denying the entry of overloaded boats of Rohingya refugees and pushing them back to sea. On April 16, it was reported that the Malaysian navy intercepted a boat with around 200 Rohingya refugees and prevented the boat from entering Malaysian waters, under the justification that such measures were part of prohibition for foreigners from entering the country.
 Making an appeal in writing from the top to the country's richest is short-sighted and misguided on many levels.
 For the global oil industry, it has been a double whammy.
 The fury, and desperation, reflected by people's criticism of the army's arms procurement plan in the face of the Covid-19 outbreak should be taken into account.
 We are experiencing the biggest threat to the fabric of our society in generations. For billions, their lives are in danger, their livelihoods are at risk, and their daily routines have been upended. The world economy has ground to a halt.
 https://www.bangkokpost.com/opinion
