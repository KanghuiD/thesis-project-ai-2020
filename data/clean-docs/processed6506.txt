[OPINION] Leaving no one behind: Internal displacement and the virus
 Welcome to Rappler, a social news network where stories inspire community engagement and digitally fuelled actions for social change. Rappler comes from the root words "rap" (to discuss) + "ripple" (to make waves).
 Read more
 'Displaced communities are in a particularly critical situation, and they should be included and considered in the development of quarantine frameworks and mobility lockdowns'    
 In the Philippines, such realities of displacements have become a natural occurrence post-crisis. According to the Internal Displacement Monitoring Center (IDMC), around 4 million Filipinos were displaced in 2018. As earthquakes, volcanic eruptions, typhoons, and armed conflict continue to happen in the country, millions of people live in displacement for weeks, months, or even years. As displacements endure, funding and development support dwindle through time. IDPs slowly become a forgotten population, both in the human development frameworks and in the national consciousness. 
 On top of these already difficult realities, the new coronavirus poses bigger threats on the lives and well-being of an already vulnerable, and usually forgotten, displaced populace. 
 At the governance side, governments are at a loss on how to quickly respond to the devastation of the virus. With limited resources on hand and a strict public safety framework, local government units cannot fully respond to the health emergency. The plight of displaced communities is usually not included in welfare programming. The social amelioration program provides certain support for families without homes, yet the program cannot fully address the persistent issues that IDPs face. They remain in cramped spaces in evacuation sites, with limited access to water, and without necessary protective and hygiene items.
 In the 22nd anniversary of the UN Guiding Principles of Internal Displacement, we remember the challenges faced by displaced communities, the resilience of IDPs, and the primary duty of governments to ensure that IDPs are able to achieve their desired durable solutions. Their rights did not end when they become displaced; it is by the virtue of their displacement that they must be given ample importance. As the deadly disease ravages the country, we must protect our displaced communities through their meaningful inclusion in crisis response. 
 In this difficult time of COVID-19 emergency, we must not leave our IDPs behind. – Rappler.com
 Reinna Bermudez is OIC Chief of the Commission on Human Rights' Center for Crisis, Conflict, and Humanitarian Protection. She is also Juris Doctor student at the University of the Philippines College of Law.
 These stories made other people 
 Password:
 Thank You.
 View your profile page
 here OR
 Fields with * are required.
 Password*:
 First Name:
 Last Name:
 Birthday:
 Gender:
  Fields with * are required. 
 Thank You.You have successfully updated your account.
 https://www.rappler.com/move-ph/ispeak/258474-opinion-leaving-no-one-behind-internal-displacement-coronavirus
