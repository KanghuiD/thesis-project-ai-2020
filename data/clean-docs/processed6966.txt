Wine Spectator Home
 Coronavirus Coverage
 Learn how shutdowns are affecting the wine and restaurant industries
 Search the largest wine database with more than 385,000 ratings
 Look to star regions like Friuli and Alto Adige for top Pinot Grigio and Sauvignon Blanc values
 Apr 23, 2020
 Our editors share the memorable wine moments that are helping brighten dark times
 New reviews of Chardonnay, Pinot Noir, Zinfandel and more from Sonoma, Santa Barbara and beyond
 Apr 20, 2020
 Free access: high-quality reds and whites from around the world up to 93 points, all $30 or less
 Apr 30, 2020
 Discover these bright and fruity reds from Fleurie, Morgon, Brouilly and more
 Apr 16, 2020
 Recently reviewed wines to try for all tastes and budgets, free to all readers
 New reviews of Willamette Valley's top grape from new faces and well-known names
 Apr 13, 2020
 Discover Riesling's energy with these new reviews of 2018 whites from the Mosel, Pfalz, Rheingau and Rheinhessen regions
 Apr 9, 2020
 Apr 27, 2020
 Apr 24, 2020
 Apr 22, 2020
 Apr 17, 2020
 This is a savory red with firm tannins, dark fruit and an espresso note ... Play the game!
 Our editors pick their favorite memoirs, histories and reference books
 The Internet is your oyster for midpriced wines, hard-to-find vintages and just plain convenience
 In part 2 of this two-part video, Zinfandel stars Morgan Twain-Peterson and Chris Cottrell talk about what the wine industry has taught them, a few of their biggest mistakes, and much more!
 In part 2 of this two-part video, Zinfandel stars Morgan Twain-Peterson and …
 Burgundy is one of France's most prestigious wine regions, producing some of …
 The California wine industry veteran and founder of Lail Vineyards talks about …
 In Part 1 of this two-part video, Zinfandel stars Morgan Twain-Peterson and …
 Napa wine pioneer Peter Newton founded his namesake estate 40 years ago. We …
 We visited fine Italian foods importer Gustiamo in the Bronx, N.Y., for a look …
 With restaurants closed and shops shuttered, cheesemakers and 'mongers are looking for new ways to survive
 A new study suggests that older Americans who drink may be healthier because they're more social
 The Wine Spectator Scholarship Foundation, dedicated to furthering wine and hospitality, has donated $250,000 to the hunger relief organization
 The Alexander Valley winery has met stringent requirements for energy efficiency, water use and green design; it is the world's largest manufacturing facility to earn Living Building status
 A punishing thunderstorm damaged vineyards in St.-Emilion and other prime regions
 With exports banned and domestic alcohol sales outlawed during the pandemic, winemakers wonder how they will persevere
 Apr 21, 2020
 A maître d' to the stars, the Italian immigrant created New York's most stylish restaurant
 The Bluegrass State removes prohibitive restrictions on alcohol shipments into and out of the state
 From boutique operations to major conglomerates, wine and spirits producers are raising big bucks for coronavirus victims, through donations, Cabernet hand sanitizer, even golf-putting contests—with help from some famous friends
 Apr 3, 2020
 Can't go shopping right now? Make the most of what's already in your fridge, freezer and kitchen cabinets
 Our editors pick their favorite movies and TV shows to savor
 Restaurants are taking a huge blow from the coronavirus pandemic. 6 wine pros have tips on how you can help
 "For Robin Lail, it's not just a passion for wine. It’s a passion for who she is in this valley. She's a part of it. She's one of the treasures of this valley."—Molly Chappellet
         
 https://www.winespectator.com/
