News and current affairs from Germany and around the world | DW
 Germany's health minister called a spike in coronavirus infections to over 4,000 in one day "worrying." But the country's infectious disease agency warned that number could jump to as high as 10,000. 
 Go to article
 The Republican and Democratic nominees clashed over policy issues, but neither was able to clinch a clear victory.
   
 The EU's three main bodies have put forth three different sets of climate protection goals. The most ambitious came from lawmakers, but everyone will have to agree for anything — other than the climate — to change.
 There is no "plausible" explanation for Navalny's poisoning besides Russian involvement, the two countries said. They will push for EU sanctions targeting individuals and "an institution."
 Derek Chauvin, the former police officer charged for the murder of George Floyd, has posted a $1 million bond to be released from jail.
 Chemical scientists Emmanuelle Charpentier and Jennifer A. Doudna have been awarded the Nobel Prize in chemistry for their research into genome editing and the discovery of CRISPR-Cas9 "genetic scissors."
 Berliners are debating the pros and cons of the latest anti-coronavirus measures. With much of the city labelled as "hotspots" due to alarming infection rates the government is clamping down.
 Many people who contracted the coronavirus disease struggle with long-term health issues. Maria Alzenir, a Sao Paulo local, got infected in July but is still dealing with a chronic shortness of breath and muscle pains.
 Protesters and striking workers have entered the third day of clashes with police against a new reform which they say undermines workers' rights. Thousands have taken to the streets and hundreds have been arrested.
 In 2014 and 2015 the group, nicknamed "The Beatles" due to their accents, filmed themselves beheading British, American, and Japanese journalists and aid workers, as well as a group of Syrian soldiers.
 Russian punk rock group Pussy Riot hung rainbow flags outside multiple government building on President Vladimir Putin’s birthday to send one message: "The state should not interfere in the life of the LGBTQ community."
 Delegates at the World Economic Forum will be packing hiking boots instead of skis this year. The annual forum is usually held in Davos in January, but it will now take place later in the year in Lucerne.
 The movement staged the blockades, stating that environmental devastation should be deemed a crime. Despite the coronavirus pandemic, some 350 activists took part in the demonstrations.
 A 16-year old social media campaigner took on the role of Finland's Prime Minister Sanna Marin for one day in order to promote girls' rights. The teenager, Aava Murto, called for more girls to be taught digital skills.
 Poland's anti-trust regulator has ordered Russia's Gazprom to pay an enormous fine over the construction of the Nord Stream 2 pipeline. Warsaw has long been opposing the mammoth project to deliver Russian gas to Germany.
 The rare participation of Egypt's rural poor in recent protests shows "Egyptians have had enough," human rights observers say. But authorities have consistently shown no hesitation in using a heavy hand to silence them.
 Evangelicals make up one of the most important voting blocs in the upcoming US election and a growing number are young anti-abortion activists. Why does rejecting abortion matter more to them than any other issue?
 The US president's antics since his COVID-19 diagnosis have appalled many, but not his supporters, says Ines Pohl.
 Coming up at 10:30 UTC: Business
 Ahead of a Nations League double, Germany hosted Turkey in a friendly in Cologne. Joachim Löw's decision to start some new faces may have helped his Euro 2021 squad take form but questions about the playing style remain.
 Journalists in Russia increasingly report of draconian laws and heavy-handed intimidation against them.
 Many of the stateless Rohingya have never stepped foot in Bangladesh, but Saudi Arabia wants them to leave.   
 Global super rich increased their combined wealth by a quarter during the coronavirus crisis, PwC and UBS have revealed.
 The US-Austrian academic and Auschwitz survivor had been a vocal critic of Holocaust museum culture.
 Some parts of Europe are re-introducing restrictions as infections spike. Are we ready for a second wave?
 Drugs are known to be bad for our health, but what impact do they have on the environment?
 Candidates, campaigns and big issue topics — what's at stake when the US goes to the polls on November 3?
 Legal notice |
 Contact
 | Mobile version
 https://www.dw.com/en/top-stories/s-9097
