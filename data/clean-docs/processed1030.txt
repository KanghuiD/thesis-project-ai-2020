Cooking in Quarantine: Shelter-In-Place Jook  | Coronavirus pandemic | Al Jazeera
 A Chinese-American disabled activist shares her ultimate comfort food while isolating from the coronavirus pandemic.
 Jook, also known as congee, was the ultimate comfort food for me as a Chinese-American kid growing up in the American Midwest.
 We would eat it for breakfast with an array of small dishes like ground fried fish or pork floss, thousand-year preserved duck eggs, pickles, shao bing (sesame covered hot pockets) or youtiao, fried sticks of dough.
 Another popular variation is to make jook with a whole chicken until the meat falls off the bone with a bunch of green onions and ginger.
 Jook was also something served plain when I was sick; it filled my belly when it could not handle anything seasoned or oily.
 It could be thick like oatmeal or soupy, depending on your taste. I always enjoyed the thicker consistency and would scoop large bites with a traditional blue-and-white ceramic Chinese teaspoon patterned with little translucent grains of rice.
 As my family and I started sheltering in place this March, our rice cooker remains our constant friend throughout these rough times.
 It never lets us down and allows us to cook a meal in one pot set at the perfect time and temperature. The variations are endless with jook - savoury, sweet, vegan, vegetarian. Using stock instead of water will give more flavour and not require any additional salt.
 It is a dish made for frugal times that can be supplemented with fried dumplings, boiled eggs, peanuts, kimchi or anything crunchy on the side for a heartier meal. 
 In my childhood memories, steaming bowls of jook mean comfort, healing, and sustenance - all of the things we need right now during the coronavirus pandemic with such uncertainty and no end in sight. 
 For a simple meal that can use a wide range of pantry items and leftovers, jook made on the stove or in a rice cooker can get you through another day. You can also make jook using a slow-cooker or Instant Pot, following the manufacturer's instructions.
 Ingredients 
 1 cup of short-grain white rice (you can use brown short-grain rice if you soak it for two hours before cooking)
 6-8 cups of chicken or vegetable stock, salted water to taste, or water with miso paste to taste
 1-2 cups greens (see below)
 1 cup chopped protein (see below)
 Greens: fresh or frozen spinach, sauteed kale, bok choy, chard, or any other green you have handy
 Proteins: cooked tofu, beef, ham, chicken, duck, sausage, ground beef, bacon canned fish, rehydrated dried scallops or pork
 Optional: sauteed peppers, onions and mushrooms, edamame, diced butternut squash, or shitake mushrooms.
 Garnishes: Chopped green onions, thinly sliced ginger, sesame seeds, kimchi, peanuts, boiled eggs.
 Rice cooker instructions 
 1. Wash the rice in the rice cooker bowl. Wash and drain the water 3-4 times until the water runs clear.
 2. Add stock and let the rice soak for an hour before turning on the rice cooker.
 3. Select the "congee" or "porridge" setting on your rice cooker. Press "start". If you do not have a congee setting, just hit the "cook" or "start" button and come back and check on things in 45 minutes.
 4. Once the timer beeps, add spinach and ham or anything you have in your refrigerator (see optional ingredients).
 5. Close the lid and leave it on the "Stay Warm" mode for another 30-45 minutes.
 6. Serve with any of the garnish options (or combination) given above either topping the jook or on the side.
 Stovetop instructions 
 1. Bring the rice and cooking liquid of choice to a boil in a saucepan with a lid.
 2. Lower the heat, cover, and allow to simmer gently for at least 45 minutes and up to two hours. Check on it every half hour or so.
 3. Once the jook has reached the consistency you want, you can add the greens, proteins and optional ingredients.
 4. Serve with any of the garnish options (or combination) given above either topping the jook or on the side.
 Recipe courtesy of Alice Wong
 Alice Wong is a disabled activist based in San Francisco and the editor of an upcoming anthology.
 A renowned human rights lawyer, whose father was a forced labourer during World War II, shares lessons from his life.
 Former President de Klerk's denial that apartheid was a crime against humanity is cause for reflection on Freedom Day.
 As a child, I followed my mother on frenzied pilgrimages that taught me about the dynamics of wealth, race and religion.
 	 
 https://www.aljazeera.com/indepth/features/cooking-quarantine-shelter-place-jook-200325120945596.html
