Opinion: EU must address pandemic′s effects on economies | Opinion | DW | 08.04.2020
 Eurozone finance ministers broke off talks on measures to address the effects of the coronavirus pandemic on national economies. This is no ordinary EU drama, DW's Bernd Riegert writes: This time, there is much at stake.
 The fact that finance ministers from the 19 eurozone countries were forced to break off negotiations after 16 hours of talks is rather unsettling. The impasse means that the pressing issues of how to deal with the impact of the coronavirus pandemic and how to share the burden of rebuilding economies afterward remain unresolved.
 DW's Bernd Riegert
 Nine of the eurozone countries want to take on debt to solve the problems, whereas four — including Germany — oppose that approach. But this isn't a fight in which parties are posturing on principle: It is about pure economic survival. Unlike the comparatively harmless situation brought on by Greece's debts in previous years, the current discussions go far beyond whether flinty rich countries are willing to extend credit to their poorer allies.
 Permalink https://p.dw.com/p/3aekt
 Right now, no one can say whether there will even be wealthy countries to help poorer ones when the coronavirus pandemic is over. The largest recession in the past 90 years looks set to ravage France, Italy, Spain and Greece; Germany, the US and the UK will also take potentially devastating hits. The pandemic will ultimately hobble the entire global economy.
 German Finance Minister Olaf Scholz, who recently said he would approve about €50 billion ($54 billion) in loan guarantees to domestic businesses, is hesitant to make similar promises to eurozone partners. That is exactly what the introduction of eurobonds, coronabonds or shared debt would be.
 Italian Prime Minister Giuseppe Conte is insisting on collectivizing debt. He knows full well that he cannot count on financial markets to generate the €400 billion he recently promised his country. There are rumors that Italian officials have threatened to leave the Eurozone — perhaps even the EU — if the country does not receive the credit that the government it is demanding.
 Thus, it would seem that everything is at stake this time: the economic survival of individual member states, as wells as the future of the European Union itself. It's no wonder that German Chancellor Angela Merkel has called the pandemic the biggest test that the European Union has faced since it came into existence.
 One more round
 Finance ministers are scheduled to resume talks on Thursday. And, next week, the heads of state and government — who recently broke off their own negotiations — are set to tackle this massive challenge. At that point they will have to make a decision, for every day they continue to dither costs eurozone countries billions. Yet, so far, EU countries have been unable to find consensus on a path forward.
 European Commission President Ursula von der Leyen was set to present a road map for the normalization of life in the EU on Wednesday — until she was forced to reconsider after receiving a number of calls from furious heads of state and government. Now, rather than finding a path forward together, each EU member state is acting alone to protect its citizens.
 At this rate it will be months before life and business return to normal in the European Union. But do we have that much time? And will the EU survive?
 A detained man crouches in front of policemen enforcing the lockdown. Dhaka, Bangladesh. 
 A Nepalese police officer maintains distance as he detains a man defying the lockdown imposed by the government. Kathmandu, Nepal, March 29.
 Police force residents to do sit-ups as a punishment for breaking the lockdown. Chennai, India, April 1.
 Thai police officers wearing protective masks stop a man on a motorcycle at a checkpoint. Bangkok, Thailand April 3.
 Members of a rapid action force patrol a neighborhood urging people to remain indoors. Ahmedabad, India, April 1.
 Police attempt to disperse revelers swimming in the Indian Ocean near Lido Beach as part of measures to prevent the spread of the coronavirus. Mogadishu, Somalia, April 3.
 A police community support officer speaks with a beachgoer at Brighton Beach in the UK, April 4.
 Israeli police detain an ultra-Orthodox Jewish man in the Mea Shearim neighborhood during a partial lockdown. Jerusalem, Israel, March 30.
 Members of Guatemala's National Civil Police escort men detained for breaking curfew. Guatemala City, Guatemala, April 3.
 Los Angeles Police Department officers verify permitted passenger travel at Union Station. Los Angeles, California, April 4.
 During a snowfall in Moscow's Red Square, a police officer gives instructions to pedestrians after the city authorities announced a partial lockdown. Moscow, Russia, March 31.
 A police officer orders a sunbather to leave a closed beach. Rio de Janeiro, Brazil, March 28.
 Soldiers and a police officer are seen among shacks in Khayelitsha township as authorities attempt to enforce a nationwide lockdown. Near Cape Town, South Africa, March 27.
 DW's editors send out a selection of the day's news and features. Sign up here.
 Nearly a third of all coronavirus infections are in the United States, as the World Health Organization warns the pandemic is "far from over." Follow DW for the latest. 
 As some small stores are allowed to reopen in Germany, Chancellor Angela Merkel has said "it can also be a mistake to proceed too quickly," urging people to temper desires for business as usual. Follow DW for the latest. 
 In normal times, Europe accounts for half of the world's tourist arrivals. With travel brought to a prolonged standstill by the coronavirus pandemic, the European Union now fears losses of up to €400 billion for its tourism industry. 
   
 Legal notice |
 Contact
 | Mobile version
 https://www.dw.com/en/opinion-eu-must-address-pandemics-effects-on-economies/a-53068398
