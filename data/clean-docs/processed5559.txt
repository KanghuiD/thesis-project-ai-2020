Robert Reich: Coronavirus won’t respond to trickle-down economics – Raw Story
 Published
 on
 By
 But they would be useless. They’d be too slow to stimulate the economy, and wouldn’t reach households and consumers who should be the real targets. And they’d reward the rich, who don’t spend much of their additional dollars, without getting money into the hands of the poor and middle-class, who do.
 In short, our imminent coronavirus and economic crises won’t respond to trickle-down economics.
 Instead, Congress must immediately enact an emergency $400 billion.
 1. Coronavirus testing and treatment.
 2. Paid sick leave and family leave this year, renewable for next year if necessary.
 3. Extended Medicaid and unemployment insurance.
 4. Immediate one-time payments of $1000 to every adult and $500 per child, renewable for next year if necessary.
 I don’t think this is an over-reaction to what’s imminent. It will help us prevent a health and economic calamity.
 Enjoy good journalism?
 … then let us make a small request. The COVID crisis has cut advertising rates in half, and we need your help. Like you, we here at Raw Story believe in the power of progressive journalism. Raw Story readers power David Cay Johnston’s DCReport, which we've expanded to keep watch in Washington. We’ve exposed billionaire tax evasion and uncovered White House efforts to poison our water. We’ve revealed financial scams that prey on veterans, and legal efforts to harm workers exploited by abusive bosses. And unlike other news outlets, we’ve decided to make our original content free. But we need your support to do what we do.
 Raw Story is independent. Unhinged from corporate overlords, we fight to ensure no one is forgotten.
 … then let us make a small request. The COVID crisis has cut advertising rates in half, and we need your help. Like you, we believe in the power of progressive journalism — and we’re investing in investigative reporting as other publications give it the ax. Raw Story readers power David Cay Johnston’s DCReport, which we've expanded to keep watch in Washington. We’ve exposed billionaire tax evasion and uncovered White House efforts to poison our water. We’ve revealed financial scams that prey on veterans, and efforts to harm workers exploited by abusive bosses. We need your support to do what we do.
 Shortly before British Prime Minister Boris Johnson was diagnosed with COVID-19, he insisted that people would be “pleased to know” that a global pandemic would not stop him from greeting hospital patients with a handshake. Clearly chastened by the disease that landed him in the intensive care unit, Johnson admitted upon discharge from the hospital that, contrary to former Conservative PM Margaret Thatcher’s pithy pitch for her individualist worldview, “there really is such a thing as society.”
 The lies spewing from President Trump clearly cannot be stopped. But they can be defanged, and the place to start is with the one monster lie that paved the way for all that followed.
 This mother of all Trumpian lies first began to take shape on July 17, 2015. It seemed fairly harmless, and even amusing, at the time. “He’s not a war hero,” Trump famously groused about Senator John McCain during a Republican primary event in Ames, Iowa. “He was a war hero because he was captured. I like people who weren’t captured.”
 Vox (4/23/20) calls the idea that you can treat the coronavirus by injecting bleach “highly unlikely.”
 Masthead |
 https://www.rawstory.com/2020/03/robert-reich-coronavirus-wont-respond-to-trickle-down-economics/
