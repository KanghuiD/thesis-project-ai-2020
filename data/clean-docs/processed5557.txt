The Conversation: In-depth analysis, research, news and ideas from leading academics and researchers.
 Chris Tyler, UCL and Peter Gluckman
 Nina Moeller, Coventry University and JM Pedersen, Coventry University
 Matthew Smith, University of Strathclyde 
 Paul Chatterton, University of Leeds
 David Andress, University of Portsmouth
 Ian Boyd, University of St Andrews
 Edward Stoddard, University of Portsmouth
 Danny Dorling, University of Oxford
 Chris Tyler, UCL
 Lyndal Collins, Monash University
 Gareth Dorrian, University of Birmingham and Ian Whittaker, Nottingham Trent University
 Christine Berberich, University of Portsmouth
 Ken Chitwood, Concordia College New York
 Hugh McLachlan, Glasgow Caledonian University
 Jennifer Holm, Wilfrid Laurier University and Ann Kajander, Lakehead University
 Harmony Otete Omeife, University of Central Lancashire
 Ute Lotz-Heumann, University of Arizona
 Imran Awan, Birmingham City University
 Jon Gluyas, Durham University
 Ra Mason, University of East Anglia
 Elena Miltiadis, Durham University
 On the Practice of Film Criticism with Jean-Michel Frodon
               —
               University of St Andrews
 York Festival of Ideas
               University of York
 Festival of Ideas
               University of Hertfordshire
 Essex Analytics, Data Science and Decision Making Summer School
               University of Essex
 Got a news tip or article idea for The Conversation?
 Tell us
 The Conversation has a monthly audience of 18 million users, and reach of 42 million through Creative Commons republication.
   
 Write an article and join a growing community of more than 102,700 academics and researchers from 3,280 institutions.
 Register now
 The Conversation relies on sector, government and reader support. If you would like to help us have even better conversations, then you may like to make a one-off or on-going donation.
 Donate
 https://theconversation.com/uk
