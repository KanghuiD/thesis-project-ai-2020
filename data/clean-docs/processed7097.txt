Coronavirus live updates: U.S. death toll tops 60,000 
 As states around the U.S. consider reopening, the country's death toll topped 60,000, according to an NBC News count. Globally there have been more than 227,000 deaths from the coronavirus pandemic, according to data from Johns Hopkins University.
 Meanwhile, South Korea recorded no new domestic COVID-19 cases for the first time in 72 days. The country dealt with the first major outbreak outside China, and brought the outbreak under control with a massive testing campaign and intensive contact tracing.
 Florida became the latest state to announce that it would slowly reopen. Gov. Ron DeSantis said the plan to lift restrictions "in a very measured, thoughtful and data-driven way," will go into effect Monday in every county except the three where most of the state's COVID-19 cases have been reported.
 California however, looked to be tightening some of its restrictions with Gov. Gavin Newsom expected to announce Thursday that beaches statewide would be closed starting Friday to prevent crowding.
 Here's what to know about the coronavirus, plus a timeline of the most critical moments:
 Download the NBC News app for latest updates on the coronavirus outbreak.
 Matthew Mulligan and Reuters
 A cacophony of tin pans, drums, whistles, and horns has reverberated through much of Serbia on Wednesday as stuck-at-home citizens vented their anger at the government and its strict containment measures to curb the coronavirus.
 For the fourth night in a row on Wednesday evening, people across Serbia responded to the call by the Don’t Let Belgrade Drown Initiative to protest by making noise against what they called dictatorship in the country, local news reported. The noise starts at 8:05 p.m. after the applause for medical staff. Later in the evening on Wednesday, supporters of the ruling Serbian party organized a counter-noise protest, lighting torches and cheering President Aleksandar Vucic.
 Serbia, which has reported 8,497 confirmed COVID-19 cases and 173 deaths from the virus, introduced stringent measures last month, including a state of emergency, closure of borders, daily curfew from 4 p.m. local time, and total lockdowns all weekend, including all four days of the Easter holiday. While the government has started to lift restrictions as the rate of infections slows, it said that a lockdown during the Labor Day holiday on May 1 — a important celebration in Serbia — should remain in place.
 Peter Jeary
 Isobel van Hagen 
 Britain will more than likely miss its target of carrying out 100,000 virus tests a day by the end of April, Justice Minister Robert Buckland said on Thursday. Around 52,000 tests were carried out on Wednesday, according health officials.
 "Even if it isn't met, we are well on our way to ramping this up," Buckland told a BBC morning show on Thursday. "Even if we don't meet the target today, the effort that's been put in to increase these numbers is remarkable," he said.
 The original target was set by the U.K.'s health minister earlier this month. The government has faced growing criticism over low levels of virus testing, as well as complaints from health workers over a lack of sufficient protective gear.
 Alexander Smith
 Senior British doctors have warned that 250 ventilators the United Kingdom bought from China risk causing "significant patient harm, including death," if they are used in hospitals, according to a letter seen by NBC News.
 The doctors said the machines had a problematic oxygen supply, could not be cleaned properly, had an unfamiliar design and a confusing instruction manual, and were built for use in ambulances, not hospitals.
 The British case is not an isolated one, and it comes as a stark example of a procurement problem that has plagued many countries as the coronavirus has spread throughout the world.
 Read the full story here.
 Nearly half of the world's workforce is at immediate risk of losing their livelihood because of coronavirus, the International Labour Organization warned on Wednesday. 
 The continued sharp decline in working hours, as well as lockdown measures due to the outbreak means that 1.6 billion informal economy workers around the world “stand in immediate danger of having their livelihoods destroyed,” according to the organization's analysis.
 “For millions of workers, no income means no food, no security and no future. As the pandemic and the jobs crisis evolve, the need to protect the most vulnerable becomes even more urgent," the U.N. agency’s director general, Guy Ryder, told a briefing on Wednesday.
 Oil giant Royal Dutch Shell on Thursday cut its dividend to shareholders for the first time since World War II, following a dramatic slide in oil prices amid the coronavirus crisis.
 The board at Shell said it had decided to reduce the oil major’s first-quarter dividend to $0.16 per share, down from $0.47 at the end of 2019. That’s a reduction of 66 percent.
 “Shareholder returns are a fundamental part of Shell’s financial framework,” Chad Holliday, chair of the board of Royal Dutch Shell, said in a statement.
 Read the full story.
 Stella Kim
 South Korea reported no new domestic virus cases on Thursday for the first time since February, the Korea Centers for Disease Control and Prevention said. The country reported four new infections that were all imported, taking the national tally to 10,765. The death toll rose by one to 247 on Thursday, while 9,059 have been discharged.
 South Korean President Moon Jae-in thanked the public on the news of zero reported domestic cases saying in a message, “This is the strength of South Korea and the power of the people.”
 The health authorities also concluded no local transmission occurred from a parliamentary election earlier this month, according to Reuters, where authorities took safety measures like requiring voters to wear masks and plastic gloves when casting ballots.
 Matthew Bodner 
  Russia passed several coronavirus milestones on Thursday, reporting a record 7,099 new confirmed infections, bringing the total number of cases to more than 100,000.
 According to data published by Russia’s Coronavirus Crisis Response Center, Russia also saw 101 deaths in the past day, bringing the total fatalities to 1,073.
 About 50 percent of Russia’s cases are in Moscow, but the share of new confirmed cases has shifted outside the city and into regions less prepared to manage an outbreak. President Vladimir Putin this week extended stay-at-home restrictions through May 11, claiming Russia has not yet reached its peak — and will not get there until at least mid-May. 
 https://www.nbcnews.com/health/health-news/live-blog/2020-04-29-coronavirus-news-n1195006
