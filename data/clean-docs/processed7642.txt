										Coronavirus business&nbspupdate
 										Get 30 days complimentary access to our Coronavirus Business Update newsletter
 									
 										30 days complimentary
 Add this topic to your myFT Digest for news straight to your inbox
 From Bert Ely, Ely & Company, Alexandria, VA, US
 From James Winpenny, Chipping Norton, Oxon, UK
 From Gary Goodman, Hove, E Sussex, UK
 From Keith Corkan, Richmond upon Thames, Surrey, UK
 From Melissa Cook, Patterson, NY, US
 From Malcolm J Heslop, Stratham, NH, US
 From Jack Winkler, London N1, UK
 From Robert Chandler, London W14, UK
 From David Robertson, West Malvern, Worcs, UK
 From Peter P Edwards, Chemistry Dept, University of Oxford, UK
 From David Tuckwell, Canberra, ACT, Australia
 From George Horsington, Edlibach, Switzerland
 From Neeraj Nandurdikar, Reston, VA, US
 From Raj Parkash, London, UK
 From Jem Eskenazi, London, UK
 From Andrew Mitchell, London, UK
 From Dalya Horowitz, Skokie, IL, US
 From Heather Davidson, Letters editor, January 1999-April 2020
 From Dr Antonio Carrarini, Munich, Germany
 From Dr Desmond Lachman, Washington, DC, US
 From John V Baldwin, Cernobbio, Italy
 From Toby Simon, Bath, Somerset, UK
 From Peter Wahl, Boston, MA, US
 From Dr Andy Sloan, Guernsey, CI
 From Dr Ruth Brown, Secretary, Labour CND
 International Edition
 https://www.ft.com/letters
