MA coronavirus deaths increase by 8, cases up 454, hospitalizations keep rising – Lowell Sun
 Massachusetts health officials on Tuesday reported eight new coronavirus deaths and 454 new cases, as hospitalizations continue to climb.
 The 454 new cases remained below the 700-plus cases on Thursday and Friday — the highest numbers since spring — but testing figures were also down in comparison on Tuesday.
 The eight new coronavirus deaths bring the state’s COVID-19 death toll to 9,538, the state Department of Public Health said. The three-day average of coronavirus daily deaths has dropped from 161 in May to 10 now.
 The state has logged 135,957 cases of the highly contagious disease, an increase of 454 confirmed cases and 41 probable cases since Monday. Of the 135,957 total cases, at least 113,768 people have recovered.
 The daily percentage of tested individuals who are positive has been hovering around 3%. That figure at the start of September was between 1% and 2%, but the rate was 3.2% on Saturday, 4.4% on Sunday and 3% on Monday — the most recent day of available data.
 The seven-day weighted average of the Bay State’s positive test rate again held steady at 1.1% on Tuesday.
 Coronavirus hospitalizations on Tuesday went up by 21 patients, bringing the state’s COVID-19 hospitalization total to 494.
 The highest peak of Massachusetts’ coronavirus hospitalizations was 3,965 on April 21. The three-day average of coronavirus hospitalizations has increased from 308 three weeks ago to 468 now.
 There are 85 patients in the ICU, and 31 patients are currently intubated.
 An additional 12,785 tests have brought the state’s total to more than 4.4 million tests.
 The state reported 25,128 residents and health care workers at long-term care facilities have now contracted the virus.
 Of the state’s 9,538 total coronavirus deaths, 6,150 are connected to long-term care facilities.
 More than 210,000 Americans have died. The country’s death toll is the highest in the world, which eclipsed 1 million deaths last week.
 The U.S. has recorded more than 7.4 million coronavirus cases — also the most in the world. More than 2.9 million people have recovered.
 OK
 https://www.lowellsun.com/2020/10/06/massachusetts-coronavirus-deaths-increase-by-8-cases-up-454-hospitalizations-keep-rising/