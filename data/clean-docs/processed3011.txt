Crafting a company response to coronavirus: Six steps for doing the right thing | The FCPA Blog
 Harry CassinPublisher and Editor
 Andy Spalding Senior Editor
 Jessica Tillipman Senior Editor
 Bill Steinman Senior Editor
 Richard L. Cassin Editor at Large
 Elizabeth K. Spahn Editor Emeritus
 Cody Worthington Contributing Editor
 Julie DiMauroContributing Editor
 Thomas Fox Contributing Editor
 Marc Alain BohnContributing Editor
 Bill Waite Contributing Editor
 Shruti J. Shah Contributing Editor
 Russell A. Stamets Contributing Editor
 Richard Bistrong Contributing Editor
 Eric Carlson Contributing Editor
 Coping with the coronavirus will require enhanced vigilance by compliance teams. As boards and senior executives navigate the way forward with a focus on sustainability and financial resilience, compliance officers must ensure they remain at the forefront and visible.
 In a crisis situation there is a temptation to think that normal rules no longer apply. This is a mistake. In a post-Covid future that is currently hard to envisage, the virus will not provide any form of defense. Businesses, like governments, are already under considerable scrutiny for their response to the virus, with daily headlines to damn or praise. Our heightened awareness and the global scale of the crisis means that memories are likely to be long. How businesses conduct themselves in the coming months will matter in the long-term.
 So how can compliance teams ensure that everyone in their organization understands that normal rules still apply? Here are six key steps that compliance officers should be taking now.
 The current situation presents an opportunity for businesses to do the right thing and many already are. For some this will involve direct help such as producing ventilators, sanitizers or protective equipment. For others it will be about acting fairly and responsibly towards all stakeholders during a time of unprecedented pressure. The steps above are a good place to start.
 Leo Martin is the founder and director of GoodCorporation. He has worked internationally for GoodCorporation clients, leading many of our anti-corruption assessments. He also works with international organizations to evaluate the effectiveness of their ethics and compliance programs across multiple jurisdictions. He has chaired over 50 ethics and compliance debates at the House of Lords and is a regular guest lecturer in business ethics.
 We set out in 2007 to bring our readers free and unrestricted coverage of all Foreign Corrupt Practices Act enforcement actions — the first to do that in real-time.
 Since then we’ve published more than 7,500 posts by 600 different authors.
 Our mission is to help compliance professionals and others everywhere understand how corruption happens, what it does to people and institutions, and how anti-corruption laws and compliance programs work.
 https://fcpablog.com/2020/04/06/crafting-a-company-response-to-coronavirus-six-steps-for-doing-the-right-thing/
