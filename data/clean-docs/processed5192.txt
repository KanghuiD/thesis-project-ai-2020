Visits to A&E drop amid fears of catching coronavirus in hospital | Metro News
 135.6m shares
 A&E admissions have fallen by nearly half since the coronavirus crisis hit the UK, leading to concerns other serious health concerns could be missed.
 A comparison of 50 hospitals showed a fall from 104,251 visits in the final week of March compared with 58,447 at the start, a drop of 43 per cent.
 There have also been sharp falls in waiting times, now the lowest for at least six months, as well as declining demand for inpatient beds.
 The data was part of a report by the Royal College of Emergency Medicine, which said people may be avoiding hospitals because they were scared they would catch or spread the coronavirus.
 Dr Katherine Henderson, president of the Royal College of Emergency Medicine, said medics fear people are avoiding hospitals.
 She told Mailonline: ‘We are worried that some patients, and particularly some parents, are delaying seeking healthcare advice because they are understandably concerned that they might be overburdening the health service, or fear that they or their child might catch the virus by going to the doctor.
 ‘But it is really important that people with potentially serious conditions still access healthcare.  
 ‘Delaying going to hospital for something such as appendicitis, heart attack or complications of pregnancy may lead to bigger and avoidable problems both for the individual and for the health service.’
 The statistics cover ‘a representative sample’ of about 50 hospitals from the more than 150 across the UK, and form part of the college’s Winter Flow Project that has tracked NHS performance since 2015.
 Waiting times have fallen with 81 per cent of patients now being seen within four hours at the end of March, up from about 73 per cent at the start of the month.
 The Royal College of Paediatrics and Child Health however warned that sick children are not being seen with reports that some have fallen seriously ill or died due to advanced illnesses. 
 Across the UK a further 708 deaths were reported on Saturday by people who had contracted coronavirus. This brings UK death toll to 4,313 after another record daily rise since the beginning of the outbreak.
 A total of 41,903 people have now tested positive for Covid-19.
 For more stories like this, check our news page.
 Not convinced? Find out more »
 https://metro.co.uk/2020/04/05/visits-ae-drop-half-amid-fears-catching-coronavirus-hospital-12511434/
