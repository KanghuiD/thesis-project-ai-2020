Coronavirus Michigan | Bridge Michigan
 Coronavirus Tracker | Case counts push seven-day average to nearly 900
 October 6, 2020
 Confusion reigns in Michigan over masks, business rules after COVID ruling
 October 5, 2020
 Nessel won’t enforce Michigan Gov. Whitmer’s COVID orders after court ruling
 October 4, 2020
 Coronavirus powers ruling puts spotlight on Michigan Supreme Court election
 Michigan 2020 Election Tracker | Biden up 9 points in Michigan, new poll shows
 Michigan Supreme Court rules Whitmer lacks COVID-19 emergency powers
 October 2, 2020
 GOP leader: No mask mandates, Michigan needs to learn to live with coronavirus
 October 3, 2020
 October 3, 2020 | Jonathan Oosting
 October 6, 2020 | Bridge Staff
 October 5, 2020 | Riley Beggin, Jonathan Oosting
 October 4, 2020 | Jonathan Oosting
 This continuously updated post includes details of Michigan's confirmed COVID-19 cases, maps of locations, state curve data, what the state is doing to contain the spread, and expert suggestions on how you can stay safe.
 October 6, 2020 | Erik Nordman
 October 6, 2020 | Ron French
 The administration of Gov. Gretchen Whitmer is moving quickly to reinstate a school coronavirus reporting mandate that was struck down Friday by the state’s high court.
 October 6, 2020 | Jonathan Oosting
 A Michigan Supreme Court ruling invalidating executive orders from governor also could jeopardize unemployment pay extensions. Tensions are flaring, with Gov. Gretchen Whitmer calling Senate Majority Leader Mike Shirkey an “anti-masker.”
 October 6, 2020 | Olivia Lewis, Mike Wilkinson
 Once a COVID hotspot, Detroit lost 1,300 people to the virus. The city and its residents fought back and say limited interactions, face masks and frequent hand-washing make the difference. Detroit now has one of the lowest rates in the state.
 Gov. Gretchen Whitmer’s administration moves to reimpose mask and social distancing rules through her health department. Republicans question if she has that right after a court ruling limited her power. Meanwhile, COVID is spiking again.
 October 5, 2020 | Mansur Shaheen
 Gov. Gretchen Whitmer’s emergency powers were effectively ended by a 4-3 decision Friday, with the court majority nominated by Republicans. Voters will decide two seats in November that could tip the balance.
 October 5, 2020 | Ron French
 How does Friday’s Supreme Court ruling upending Gov. Gretchen Whitmer’s emergency power authority impact coronavirus mandates in Michigan schools? We answer some of your major questions.
 October 5, 2020 | Paula Gardner
 Dozens of executive orders have regulated businesses over the last six months. After a chaotic few days of court rulings and political chess games in Lansing, it’s unclear which of them still apply.
 Michigan Attorney General Dana Nessel says a Friday court order means she will no longer enforce Gov. Gretchen Whitmer's emergency coronavirus orders. At least one local health department is issuing orders instead, but the Legislature is unlikely to adopt sweeping measures favored by the governor.
 October 3, 2020 | Kris Nicholoff
 Cancer is already a serious problem in Michigan, which ranks 14th in cancer mortality nationwide. As physicians committed to holistic health and wellness, none of us wants to wait to begin saving lives.
 One day after Michigan Supreme Court rules Gov. Gretchen Whitmer can’t impose emergency powers during pandemic without input from lawmakers, Senate Majority Leader Mike Shirkey says the panel is unlikely to agree to far-reaching restrictions. ‘The virus ultimately was going to win,’ he says.
 October 2, 2020 | Jonathan Oosting, Riley Beggin
 Hundreds gather at Michigan Capitol to blast Gov. Gretchen Whitmer’s pandemic orders Friday, the same day that President Donald Trump tested positive for COVID-19. His diagnosis set off a brief panic among Republicans.
 October 2, 2020 | Riley Beggin, Paula Gardner
 Michigan Gov. Gretchen Whitmer requires the U.P. to adhere to the same guidelines as most of the Lower Peninsula. The move comes as cases have exploded in western counties near Wisconsin.
 October 2, 2020 | Jonathan Oosting, Riley Beggin, Kelly House
 A divided court declared unconstitutional  a 1945 law Gov. Gretchen Whitmer has used to issue orders without input from the Legislature. The GOP declares it a ‘great day’ and the fate of 120-plus orders from Whitmer are now in question.
 October 1, 2020 | H. Luke Shaefer
 Expanded unemployment assistance, an eviction moratorium and diverse collaborations played a major role in shoring up the financial stability for millions of Michiganders.
 October 1, 2020 | Debra Furr-Holden
 The results have been astounding. Once disproportionately represented in COVID cases and deaths, African Americans are now underrepresented.
 October 1, 2020 | Robin Erb
 The latest executive order also loosens community dining restrictions in facilities and expands reporting requirements of coronavirus cases to include legal guardians and prospective residents or staff.
 September 30, 2020 | Nushrat Rahman
 As the new school year ramps up and the economic downturn of the COVID-19 pandemic continues, parents are having to make tough financial decisions. Nonprofits and social service agencies say they see families struggling to purchase materials for school, access child care and put food on the table.
 September 30, 2020 | Ron French, Robin Erb, Kathryn Dugal
 How much you’ll learn about coronavirus cases in your student’s school depends on where you live. A Bridge Michigan analysis shows parents still can’t depend on schools or local health departments to voluntarily alert them to outbreaks.
 https://www.bridgemi.com/tags/coronavirus-michigan
