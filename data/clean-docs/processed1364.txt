LIVE BLOG: Coronavirus In Pittsburgh, April 13-19 | 90.5 WESA
 Editor's note: This post will be frequently updated with the latest news.
           
 Thursday, April 16, 2020
 10:02 a.m. — Mayor Peduto to give online speech in response to COVID-19
 Mayor Bill Peduto is scheduled to give a speech today at 3 p.m. about the city's response to COVID-19. Streams will be available via:
 https://www.youtube.com/channel/UC-YfIv9wvBjGT3LMxo9hLoQ
 http://pittsburgh.granicus.com/player/camera/1?publish_id=4
 8:23 a.m. —  500 construction workers to go back to work on Shell's ethane cracker
           Credit Reid R. Frazier / StateImpact Pennsylvania    
 Hundreds of construction workers will be returning to work on Shell’s ethane cracker in Beaver County, after state officials told the company that parts of the construction project don’t need a waiver to stay open.
 The company suspended construction last month, after workers raised concerns over the spread of the coronavirus. The company then applied for a waiver from the state to be able to resume some work. 
 7:46 a.m. —  What's it going to take to end the shutdown?
 Public health experts say social distancing appears to be working, and letting up these measures too soon could be disastrous. Until there is a sustained reduction in new cases — and the coronavirus' spread is clearly slowing — we need to stay the course.
 NPR's Selena Simmons-Duffin and Allison Aubrey break down five keys to containing the coronavirus, from rapid testing to isolating the sick and vulnerable. 
 Wednesday, April 15, 2020
 5:41 p.m. —  Wolf mandates masks
 Going to a Pennsylvania business that remains open during the pandemic? Be prepared to wear a mask. That goes for workers, too.
 Many commercial buildings that serve the public will be required to make sure customers wear masks — and deny entry to anyone who refuses — under an order signed Wednesday by the state health secretary.
 Employees will also have to wear face coverings, including those who work in warehouses, manufacturing facilities and other places that remain in business but aren’t open to the public.
 The mask mandate was included in a wide-ranging order that will govern many aspects of how a business operates — from how it arranges its break room to how many patrons it can allow inside at any one time — as the administration of Democratic Gov. Tom Wolf confronts a pandemic that has killed at least 647 in Pennsylvania and sickened thousands more.
 Wolf said the latest order is meant to protect supermarket cashiers, power plant operators and other critical workers who can't stay home and are at heightened risk of contracting the virus.
 5:12 p.m. —  PPS plans virtual commencement ceremonies
 Pittsburgh Public Schools seniors will graduate online this year. A district administrator confirmed Wednesday that a committee is planning virtual commencement ceremonies.
 The district moves to remote learning this week. Seniors were the first to receive laptops and online instruction. All 23,000 students will be learning remotely by April 22.
 Other regional districts have postponed commencement until late summer.
 3:59 p.m. —  Gender Equity Commission highlights disparities for Pittsburgh families
 Pittsburgh’s Gender Equity Commission says the city’s marginalized communities are disproportionately feeling the effects of COVID-19. Representatives from the LGBT community, maternal health care, and domestic violence shelters say issues of inequity are being exacerbated by financial insecurity and lack of access to resources.
 During a virtual town hall today, Tammy Thompson with the nonprofit Circles Greater Pittsburgh said families in poverty are struggling.
 “It’s tough in normal circumstances,” Thompson said. “But to add trying to survive and maintain and care for your children and make sure your children are safe and secure during a global pandemic is something that most of us would hope that we never have to imagine.”
 The commission also said it’s increased virtual doula support for pregnant women, as well as resources for people experiencing intimate partner violence.
 3:37 p.m. —  Many COVID-19 deaths are in long-term care facilities
 Pennsylvania Health Secretary Rachel Levine says roughly half of all COVID-19 fatalities in Pennsylvania are in long-term care facilities. That includes 16 of the 26 total deaths in Allegheny County.
 “We continue to work closely with these facilities to provide infection control support and personal protective equipment.”
 Neither the county nor the state has released the names of nursing homes, personal care homes and assisted living facilities with COVID-19 cases. There are 54 confirmed cases among staff and residents at the county-run Kane Community Living Center in Glen Hazel. Four residents have died.
 2:23 p.m. — The residential child care’s dilemma: lose income or potentially expose families to coronavirus?
 Most childcare providers in Pennsylvania were closed under state order last month. 
 But providers that operate out of their homes are allowed to stay open.
 As Keystone Crossroads’ Miles Bryan reports, that’s forced them to make a difficult choice: lose income or potentially expose their families to coronavirus?
 12:00 p.m. — Pennsylvania COVID-19 cases increase by 1,145
 The statewide total of COVID-19 cases is now at 26,490. The state also reported 63 new deaths, bringing the total across the state to 647.
 11:24 a.m. — Allegheny County reports two more deaths
 There are now 26 COVID-19 related deaths in Allegheny County. The number of confirmed cases is now at 904, an increase of 11 from the previous day.
 10:52 a.m. —  City employee dies of complications from COVID-19
 During today's city council meeting, councilors offered their condolences to the family.
 “I am praying for his family, friends and co-workers in these tragic times,” Mayor Bill Peduto said. “The pandemic may be global but it’s still hitting us very close to home.”
 10:46 a.m. — Mapping the invisible spread of coronavirus through simulations 
           Credit Ansys    
 While some cities and states are using police to enforce social distancing, a team of scientists and engineers at Canonsburg-based Ansys are hoping to encourage the public to follow safety measures by showing how they work. 
 The company created 3D simulations that show how respiratory droplets travel, so people can visualize why distancing measures are so important.
 8:39 a.m. — Councilors want to turn vacant lots into urban gardens
           Credit Sarah Kovash / 90.5 WESA    
 Some Pittsburgh City Council members want to turn city-owned vacant lots into plots for urban farming. As the COVID-19 pandemic continues, councilors say there are increased concerns about a lack of access to food.
 7:32 a.m. — Nonprofit partners restaurants with health care workers
 The coronavirus pandemic has stressed health care workers and put restaurant employees out of work. New initiatives in Pittsburgh are trying to address both problems at once.
 The volunteer-run project called Off Their Plate uses donated money to pay restaurants hit by the shutdown to make meals for health-care personnel. Off Their Plate began in Boston and expanded to cities including Pittsburgh, where it served its first meals April second.
 The organization recruited Square Café, in Regent Square, and The Vandal, in Lawrenceville, to prepare about 1,000 hot meals so far for doctors, nurses, and other staff at hospitals run by UPMC, Allegheny Health Network and more.
 Tuesday, April 14, 2020
 7:20 p.m. — COVID-19 appears to be slowing, but precautionary guidelines aren't easing quite yet
 Health Secretary Dr. Rachel Levine says the state has managed to flatten the curve of the virus significantly. Though the state has more than 25,000 COVID-19, Levine says the numbers could have been much higher without stay at home orders. 
 “We have to continue those efforts until the time is right in a slow progressive fashion to start to open businesses and cancel stay at home orders in a regional, maybe countywide basis in an iterative fashion," Levine said.
 Since Monday, there has been a 4.7 percent growth in new cases. That rate is the lowest it has been since before the state’s stay-at-home orders began in March.
 6:19 p.m. - Voters seek record number of mail-in and absentee ballots
 Pennsylvania counties have processed about 283,000 applications for mail-in and absentee ballots, and nearly three times more applicants are from Democrats compared to Republicans. The state’s primary has been moved back to June 2, so the numbers are likely to continue to climb. Four years ago, before mail-in balloting was allowed in the state, about 84,000 Pennsylvanians cast primary votes by absentee ballot.
 4:48 p.m. — Health experts urge lawmakers not to rush into easing COVID-19 rules yet
 Partisan tensions are growing in Harrisburg over the appropriate way to respond to the coronavirus pandemic — with many Republicans hoping to scale back business closures and get the economy back up and running, and Democrats largely urging a more cautious approach.
 “We recognize the severity of the disease and what the immediate quarantine did in terms of buying us time,” said House Majority Leader Bryan Cutler, a Republican. “Our concerns looking to the future though, and really what has driven some of the discussion this week, is what will the next steps be?”
 Medical experts are also wrestling with the question of when it will be appropriate to start returning to normal life. But asked to weigh in on several bills moving through Pennsylvania’s GOP-controlled legislature, two doctors — one from Pittsburgh, one from San Francisco — had similar responses: it isn’t time yet to start relaxing restrictions.
 Read more here.
 3:59 p.m. — U.S. Department of Transportation will send millions to Pennsylvania airports
 Pittsburgh International Airport will receive $36 million in emergency support from the federal government. Airports across the state will see a combined $239,219,867, according to a release Tuesday from the Federal Aviation Administration. The money is expected to cover operating costs such as payroll, as well as capital projects and debt payments.
 The $2.3 trillion stimulus package passed by Congress in March included $10 billion for the nation’s airports affected by response to the coronavirus. The appropriation is intended to “fund the continued operations of our nation’s airports during this crisis and save workers’ jobs,” department secretary Elaine Chao said in a statement.
 Pittsburgh International planned to begin work on its $1.1 billion terminal project this spring, but the work was put on hold. 
 Airports are encouraged to spend the money “immediately” to counter the effects of the public health crisis.
   
 3:07 p.m. — Three Rivers Arts Festival canceled 
 The Pittsburgh Cultural Trust announced today in a statement that because of the coronavirus pandemic, it has suspended all events through June 14, including what would have been the 61st annual arts festival. It is the first time the festival has ever been canceled. The annual Children’s Theater Festival was also canceled.
 "We are working diligently to bring good news about what is to come when we can all gather again and to reschedule as many of our affected events as possible, but in the current pandemic we must first protect the health and safety of our guests, staff, volunteers, and artists," read the statement, in part.
 The arts festival, founded in 1960, draws hundreds of thousands to Point State Park and a swath of Downtown each June for music, arts and crafts. The Cultural Trust said it would provide digital spaces to mark this year’s arts festival and children's theater festival.
          In this image made with a fisheye lens, an array of umbrellas covers a walkway at the annual Three Rivers Arts Festival on Saturday, June 3, 2017, in Pittsburgh. Credit Keith Srakocic / AP    
 2:26 p.m. — Coroners, state health officials at odds over who counts COVID-19 deaths
 The Pennsylvania Coroners Association wants to work more closely with the state Department of Health to make sure doctors and other medical staffers get an accurate count of the deadly effects of the coronavirus pandemic.
 Sara Simon of Spotlight PA tells 90.5 WESA's The Confluence that medical examiners and coroners disagree with state officials about what their role should be in the crisis. They’re citing a statute that says their role is to investigate deaths known or suspected to be due to contagious disease or constituting a public hazard. The state health department disagrees, Simon says. COVID-19 deaths are currently considered “natural,” or not suspicious, and not beholden to those rules.
 Health care professionals currently have four days to enter a confirmed or suspected case into the Electronic Death Registration System, which coroners and medical examiners already use to communicate data directly to the Centers for Disease Control and Prevention. Coroners tell Simon that if state officials truly want fast and accurate reporting, they should yield those responsibilities to people already familiar with the technology.
 1:50 p.m. — Some black-owned businesses worry about how they'll survive pandemic
 As small businesses across the country continue to be impacted by the coronavirus pandemic, some of Pittsburgh's black business owners fear for their financial futures and ability to access relief. 
 Ariel Worthy talks to business owners to see how they're feeling.
 12:04 p.m. — Pennsylvania COVID-19 cases reach 25,345
 The Pennsylvania Department of Health reports 1,146 new cases of COVID-19 and 60 new deaths. 
 So far, 108,286 people have tested negative for COVID-19.
 11:19 a.m. — Allegheny County reports three more deaths
 The Allegheny County Health Department reports three more COVID-19 deaths, bringing the total to 24. The total number of confirmed cases is now 893. 
 9:52 a.m. — Fourth resident dies at Kane Center in Glen Hazel
 A fourth resident of the Allegheny County-run Kane Community Living Center at Glen Hazel has died from COVID-19. Thirty-four other residents have tested positive at the facility, with two tests pending; 24 residents have tested negative. Nineteen staff at Glen Hazel have also tested positive. No residents or staff at Kane's other Centers in Scott, McKeesport, and Ross have tested positive, though a total of ten tests are pending at the facilities.
 8:43 a.m. — Mellon Foundation to give out $15 million in emergency funding
 The Richard King Mellon Foundation approved a $15 million pandemic solutions package, which will distribute grants to organizations in southwestern Pennsylvania. The foundation says the package will include "health-innovation grants" that aim to help find a cure for coronavirus, including almost $200,000 for the University of Pittsburgh's vaccine research. The organization will also give grants to struggling nonprofits and provide economic-development grants to "prevent further job loss and stimulate recovery."
 8:12 a.m. — How to get your $1,200 emergency payment faster
 Those $1,200 federal payments to help Americans through the coronavirus crisis have started arriving in some people's bank accounts via direct deposit. But many people will have to wait longer — and there could be pitfalls, such as debt collectors grabbing the money before you do.
 Those who'll be getting checks in the mail may not see them for weeks or even months. To get the money faster, millions of people will have to provide direct-deposit account information to the IRS.
 NPR's Chris Arnold reports on what to expect.
 7:35 a.m. — Food Bank holding distribution in Butler today
 The Greater Pittsburgh Community Food Bank is holding a drive-up emergency food distribution this morning in Butler. Hours are 11 a.m. to 2 p.m. at Alameda Park. The Food Bank will provide two boxes of food to each vehicle — no more than 1,000 vehicles will be served. Yesterday the Food Bank held its third weekly distribution in Duquesne at the group's headquarters.
 Monday, April 13, 2020
 6:16 p.m. — City councilors introduce urban agriculture pilot program
 City Councilors Erika Strassburger, Deb Gross and Theresa Kail-Smith introduced a series of legislation that would create a pilot program that "identified publicly owned parcels" to "be made and used to foster Urban Agriculture." The Department of Finance, under the proposal, would identify properties owned by the city that would be used for the pilot program.
 The legislation was introduced "to minimize the negative affect of COVID-19 and previous cuts to hunger programs."
 5:52 p.m. — How Meals on Wheels programs are adjusting to COVID-19
 Demand for meals is increasing slightly, as more people find themselves stuck indoors. There’s also heightened interest in volunteering. But some volunteers have had to take a break for now—often at the urging of their children.
 Read more here. 
 3:45 p.m. — Gov. Wolf announces plan with six other states to restart the economy
 Wolf joined the governors of New York, New Jersey, Connecticut, Rhode Island and Delaware to announce that they will share information and form a task force to help guide the reopening of the states' economies once the crisis recedes.
 President Donald Trump asserted Monday that he is the ultimate decision-maker for determining how and when to reopen the country.
 Wolf, however, said that considering governors had the responsibility for closing states down, “I think we probably have the primary responsibility for opening it up.”
 Wolf also asserted that it is a "false choice" to choose between public health or the economy.
 “I think this regional compact is premised on the idea that you’re not going have a healthy economy if you have an unhealthy population,” Wolf said on a conference call with the other governors. "So we’ve got to do both. We’ve got to get people healthy. The sequence is you’ve got to get people healthy first and then you can reopen the economy, not until, or the economy’s not going to work.”
 3:30 p.m. — Congress struggles with what to include in next aid package
 Republicans want narrow legislation to boost funding for small business loans, while Democrats want to add to the small business loan program as well as boost funds for local efforts and hospitals. On Monday, Democrat Conor Lamb said there's more than just one issue that needs more attention.
 “We absolutely should give more money to small businesses, I agree with that,” he said in an interview with WESA. “It's just that hospitals need a lot of money, and we're still not testing nearly enough people every day, and there's record lines of people at food pantries and food banks all around the country.”
 The drastic increase in people filing for unemployment due to the coronavirus pandemic has overwhelmed the agencies in charge of processing applications. Lamb said federal officials need to prioritize the issue.  He said instead of holding daily press briefings at the White House, the Trump administration should be making sure people are getting unemployment checks.
 “You don't need to stand in front of a camera for two hours every single day when instead you could be making people all around the country accountable for the failure of these systems,” he said.
 Lamb said there's no good explanation for why it's taken so long for people to get their money. Last month, Congress boosted unemployment benefits, so those who have filed will get an extra 600 dollars a week.
 The Democrat said he's the lack of tests and equipment supply shortages show that the White House's efforts have fallen short. Lamb says officials should be held accountable for the administration's inaction, but he said it's hard to do so when most members of Congress are not in Washington.
 2:46 p.m. — State police academy closed, cadets to continue training online
 The Pennsylvania State Police training academy in Hershey, Pa. will be closed for two weeks after a staff member tested positive for COVID-19. The 100 cadets currently in training will continue instruction online. 
 All other PSP-related activities have been suspended.
 2:41 p.m. — State Correctional Institution at Phoenix, Montgomery County has first COVID-19-related inmate death
 The Department of Corrections announced an inmated died at the medical facility on the prison's campus on April 8 due to acute respiratory distress related to COVID-19. The inmate was 67 years old.
 1:21 p.m. — Allegheny County Exec seeks all-mail primary
 The top official in Pennsylvania’s second-most populous county is pushing for the authority from the state to conduct the June 2 primary election entirely by mail. Allegheny County Executive Rich Fitzgerald on Monday said holding an in-person election in the midst of the coronavirus crisis would be a “disaster.” He wants Gov. Tom Wolf to expand the state’s emergency declaration to allow the move. Officials in a pair of suburban Philadelphia counties, Montgomery and Chester, are also backing the idea of an all-mail election. And Philadelphia is making preparations for it in case an all-mail election is ordered.
 12:06 p.m. — 1,300 new COVID-19 cases in Pennsylvania
 The state Department of Health reports 1,366 new positive cases of COVID-19, bringng the statewide total to 24,199. Additionally, the health department reports 17 new deaths, bringing the total to 524. 
 11:21 a.m. — Allegheny County reports two more COVID-19 deaths
 Allegheny County reports two more people have died from COVID-19, bringing the total number to 21. There are now 876 confirmed cases, up from 857 on Sunday.
 10:33 a.m. — Blood donation organization looking to collect plasma from those who recovered from COVID-19
 Blood-donation non-profit Vitalant is planning to begin collecting "convalescent plasma" from people who've recovered from COVID-19.  The organization says the plasma may contain immune-boosting antibodies that could be used to treat critically ill COVID-19 patients.  Last week the Food and Drug Administration issued new guidance on the collection and study of convalescent plasma, which the agency regulates as an "investigational product."
 8:52 a.m. — Coronavirus creating a hotbed of disinformation
 Researchers are seeing more than five times as much disinformation online related to the coronavirus than to the average natural disaster. A team from Carnegie Mellon University has found that these stories also have a longer shelf life, because they are continually rebroadcast around the globe. Instances of disinformation range from the silly – like you don’t have COVID-19 if you can hold your breath for ten seconds – to the serious – like that the virus is a U.S. biological weapon.
 7:01 a.m. — Catching up from the weekend
 The Associated Press contributed to this report.
 The World Health Organization has declared the novel coronavirus outbreak a pandemic, and also an “infodemic.”  
 The World Health Organization has declared the novel coronavirus outbreak a pandemic, and also an “infodemic.” 
 The Allegheny County Health Department has begun breaking down COVID-19 deaths and hospitalizations by race – something that activist have been demanding in recent days amid concerns about infection rates among African Americans nationwide.  
 The Allegheny County Health Department has begun breaking down COVID-19 deaths and hospitalizations by race – something that activist have been demanding in recent days amid concerns about infection rates among African Americans nationwide.
 More Pennsylvanians will likely need help paying for groceries as the economy remains shutdown due to the coronavirus. But as the pandemic pokes holes in social safety net programs, there are new obstacles for food stamps recipients. 
 More Pennsylvanians will likely need help paying for groceries as the economy remains shutdown due to the coronavirus. But as the pandemic pokes holes in social safety net programs, there are new obstacles for food stamps recipients.
 Sunday, April 12, 2020
 5:22 p.m -- Local US attorney's office seeks to intercept masks, stumbles across scam instead 
 https://www.wesa.fm/post/live-blog-coronavirus-pittsburgh-april-13-19