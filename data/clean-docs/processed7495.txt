Reopening, Coronavirus, N.F.L. Draft: Your Weekend Briefing - The New York Times
 Here’s what you need to know about the week’s top stories.
 By Remy Tumin and Elijah Walker
 Here are the week’s top stories, and a look ahead.
 1. After weeks of shutdown, the nation is beginning to slowly and cautiously re-emerge.
 Like the decisions to shut down and order residents to stay at home, the plans to reopen have come state by state or region by region. Even as states like Oklahoma, above, Georgia and Alaska took steps toward something resembling normalcy, it was anything but business as usual.
 With the known U.S. death toll nearing 50,000 — a quarter of confirmed global deaths — the heated debate over when to restart the U.S. economy has obscured an issue that may prove just as thorny: How to do it. The economy is a complex web of supply chains whose dynamics don’t necessarily align neatly with epidemiologists’ recommendations.
 Have you been keeping up with the headlines? Test your knowledge with our news quiz. And here’s the front page of our Sunday paper, the Sunday Review from Opinion and our crossword puzzles.
 2. Testing shortages continue to impede governors’ abilities to reopen their states.
 In both red and blue states, government and health officials are finding innovative ways to cope, but still lack what experts say they need to track and contain outbreaks. And while the U.S. has made strides over the past month in expanding testing — about 1.2 million tests were done in one week alone — its capacity is nowhere near the level President Trump suggests it is.
 Antibody tests are crucial to reopening the economy, but public health experts have raised urgent concerns about their quality. A team of 50 scientists evaluated 14 available tests for coronavirus antibodies. Only three passed muster.
 3. With no clue when the pandemic may subside, New Yorkers are growing grimmer.
 Evidence of a mood shift could be seen in little spikes on data compiled by the city. Complaints to 311 rose in reports of loud televisions and a brand-new category — lax social distancing.
 “There is this grieving of life as we once knew it that wasn’t there before, as we try to come to terms with the new reality,” a psychologist in Manhattan said. Above, a drive-by funeral in Brooklyn.
 The pandemic has especially laid bare the inequities of New York City’s health care system. Our journalists examined those fault lines at one public hospital in a section of Brooklyn that was hit hard by the outbreak. 
 4. The pandemic is testing Joe Biden’s patience and political imagination.
 Walled off from voters, Mr. Biden, the former vice president, has developed a routine, of sorts — briefings over the phone, video chats with voters — as he attempts to win the presidency from his basement. We spoke to dozens of people who revealed a newly detailed picture of Mr. Biden’s life in seclusion.
 A rash of ominous new polls and President Trump’s erratic briefings have the G.O.P. worried about a Democratic takeover of both the presidency and the Senate in November if Mr. Trump doesn’t put the nation on a radically improved course.
 5. Ramadan looks very different this year.
 The coronavirus pandemic is forcing Muslims to adapt, observing the holy month more at home than in the mosque, more online than in person and with greater uncertainty about the future. Our photographers sent in dispatches from around the world. Above, the empty courtyard of the Jama Masjid in New Delhi, one of India’s largest mosques.
 Some couples in the Middle East, undeterred by the coronavirus, are pressing ahead with scaled-back weddings. The determination stems from a simple truth: Marriage may be important everywhere, but in the Middle East, it is the doorway to so much of adult life that it often cannot wait. And the authorities aren’t always thrilled.
 6. Bored in isolation, the faux Munchs, Kahlos, Kandinskys and Picassos are piling up.
 Perhaps it’s time to consider the nude selfie as high art, an act of resilience in isolation, a way to seduce without touch, the novelist Diana Spechler argues in The Times’s Opinion section.
 Are you living alone? We want to hear from you. Modern Love is seeking personal stories of lives transformed by sudden solitude.
 7. During lockdown, it’s 5 o’clock everywhere.
 Our columnist examines the birth (the Prohibition-era), the death (the tech boom) and the soaring return of cocktail hour to American households. It’s just the ritual we need, she writes.
 The trick to drawing out your inner bartender while staying home is to improvise and innovate, and above all, to keep it simple. Here’s how to properly stock your liquor cabinet.
 8. If the N.F.L. were a high school cafeteria, the A.F.C. table would be the popular one. This year’s draft was no exception.
 Quarterbacks Joe Burrow, above, Justin Herbert and Tua Tagovailoa, presumptive stars-to-be, went directly to the A.F.C. in quick succession. The division, formally the American Football Conference, “is capturing the glitz, style and pioneering spirit of a new generation of players,” our reporter writes.
 But the question remains: Will they play in the fall? Teams and officials have been dropping hints this week that the hurdles are daunting, particularly the idea of having fans in attendance.
 9. The Hubble Space Telescope just keeps giving.
 The picture of stormy star birth in a nearby galaxy was released Friday by the Space Telescope Science Institute in Baltimore, keepers of the Hubble, in honor of the 30th anniversary of its launch.
 Among Hubble’s triumphs over the decades have been charting the effects of dark energy on the growth of the universe and surveying the weather throughout the solar system, inspiring awe at a universe we can see.
 10. And finally, dig into one of our Best Weekend Reads.
 In this week’s edition: The owner of a shuttered, beloved Manhattan bistro writes about the future of her restaurant; Brazil’s Indigenous tribes, above, face an existential threat; Silicon Valley’s preppers enjoy some vindication; and more.
 For ideas on what to read, watch and listen to, may we suggest these 11 new books our editors liked and more books to watch for in May, a glance at the latest small-screen recommendations from Watching and our music critics’ latest playlist.
 It’s the last weekend of April. Here’s hoping its showers bring May flowers.
 Your Weekend Briefing is published Sundays at 6 a.m. Eastern.
 What did you like? What do you want to see here? Let us know at briefing@nytimes.com.
 https://www.nytimes.com/2020/04/26/briefing/reopening-coronavirus-nfl-draft.html
