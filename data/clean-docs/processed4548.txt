	Coronavirus: Advice for Business
 Browser does not support script.
 Major boost for Welsh SME's as New Bounce Back Loans Scheme is announced
 Bounce Back Loans Scheme
 The UK Government have launched a new 'support finder' tool to help businesses and self-employed people across the UK to quickly and easily determine what financial support is available during the COVID-19 pandemic.  The finder tool asks business owners to fill out an online questionnaire, which can take minutes to complete, and directs you to a list of all the financial support you may be eligible for.
 New Business Support Finder Tool
 The Welsh Government launched the Economic Resilience Fund. 
 THE ECONOMIC RESILIENCE FUND HAS BEEN PAUSED [28th April 2020]
        
 Access to Finance
 Coronavirus Business Interruption Loan Scheme (CBILS) CBILS has been significantly expanded along with changes to the scheme's features and eligibility criteria.  The changes mean even more smaller businesses across the UK impacted by the coronavirus crisis can access the funding they need.
 Development Bank of Wales: www.developmentbank.wales/coronavirus-support-welsh-businesses
 Important to note that the HMRC portal to process the Coronavirus Job Retention Scheme (CJRS) will have the capacity to handle 450,000 claims an hour. 
 Once claims have been submitted, payment for furloughed workers can be expected within 4 to 6 working days. 
 Most importantly, companies will be able to claim furlough payments 14 days in advance of an employer's payroll run, thereby easing immediate cash flow pressures.  
 We will keep this page updated with the latest information as these schemes are developed and made available.  Please see more authoritative sites and links below:
   
 Follow us @ValeEconomy for the latest information.
 IMPORTANT UPDATE FOR THOSE WAITING FOR THEIR GRANTS OR WHO HAVE NOT YET APPLIED [24th April 2020]
 Many businesses in the Vale of Glamorgan have already been paid their emergency grants.
 1,535 grants have been paid out totalling £19,490,000.
 However, where details may not match those on the NNDR system, the Council is requesting some further information in order to pay the grants to eligible businesses.  The Council will contact businesses that need to submit additional information. 
 You do not need to do anything if you have already applied.
 Welsh Government have recently announced that they will support businesses with a grant of £10,000 or £25,000 depending on the rateable value of the business. The flowchart provides information on whether a business is entitled to a grant or not. Please note this grant only applies if you are in a rateable business premise for which you hold the responsibility for rates.
 All business grant applications now include questions regarding State Aid, further information on State Aid can found below. 
 Before you make an application for a Business Grant, please ensure that you have a copy of your bank statement ready to upload.
 PLEASE DO NOT APPLY BEFORE YOU HAVE THIS OR THIS MAY SLOW DOWN YOUR APPLICATION.
 The application form for this grant is now available.  The easiest and most efficient way to apply for a grant is online.  
 In order to ensure that the Council has all the necessary information to enable the administration of the grants quickly and accurately, all businesses are being asked to complete the application form and to provide the following supporting evidence:
 This process relates specifically to the Coronavirus Grants, Business Rates relief information will be sent in due course.
 If you wish to make enquiries on your application form further, due to the unprecedented amount of phone calls that are currently being received it is therefore recommended that you contact the Council by e-mail to businessgrants@valeofglamorgan.gov.uk and a response will be issued as soon as possible.   We are currently experiencing high volumes of business grants applications and e-mails consequently responding to your enquiry may take longer than normal.
 In this unprecedented economic crisis, the Vale of Glamorgan Council will be keeping businesses updated with the latest information on Coronavirus via this Business Support web page. This will be updated on a regular basis.
 Please follow us on @ValeEconomy for the latest updates.
 Covid-19: Support for Businesses 
 Online courses and webinars launched
 Support for the Self Employed
 COVID-19: support for businesses
 Tax helpline to support businesses affected by Coronavirus (COVID-19)
 Coronavirus (COVID-19): Information for individuals and businesses in Wales
 Guidance for employers and businesses on coronavirus (COVID-19)
 COVID-19 and your business: free webinars for businesses affected by COVID-19.
 Coronavirus: What support is available to Welsh businesses?
 Emergency support for businesses hit by coronavirus 
 https://www.valeofglamorgan.gov.uk/en/working/Business-Support/Coronavirus-Advice-for-Business.aspx
