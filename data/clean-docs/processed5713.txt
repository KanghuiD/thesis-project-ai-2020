      Coronavirus (COVID-19) information - The Dudley Group NHS Foundation Trust  
 Statement issued on 20th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 The Dudley Group NHS Foundation Trust is currently treating patients who have tested positive for COVID-19 (coronavirus).
 Sadly, patients being cared for at our hospital, and had tested positive for COVID-19, have died. Up to date information for the Trust can be found here.
 Our thoughts and condolences are with the families and friends of those patients at this very difficult and distressing time.
 Statement issued on 17th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 We are overwhelmed by the support being shown us by our community and understand how people want to demonstrate this support for their hospital. This is a great source of strength for our staff, however our main concern is that people stay safe, and the best way to help us is to stay at home.
 Our staff at all levels take a few minutes out of their week each Thursday to share the weekly #Clapforcarers campaign with their colleagues. This happens at shift handover time, when people are coming to work or leaving the site. It occurs at hospitals and trusts up and down the country.
 We are acutely aware of the huge impact this virus is having on our whole country and individuals in our communities, and our condolences go to those who have lost loved ones during this really stressful time. The loss of any patient is very keenly felt by all those involved in their care.
 We also have a duty to care for the mental health of our workforce and allow them a time to share in the feelings of goodwill being displayed across the country.
 Our message to everyone is to stay at home and stay safe.
 Statement issued on 15th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 154 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Nine of those have been announced today.
 The patients were aged between 63 and 94 years old and all patients had underlying health conditions.
 Statement issued on 14th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly we can confirm that a man who was being cared for at Russells Hall Hospital, and had tested positive for COVID-19, has died.
 The patient, who died on 12th April, was in his 60s and had underlying health conditions.
 His family has been informed and our thoughts and condolences are with them at this difficult and distressing time.
 Statement issued on 13th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 144 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Sixteen of those have been announced today.
 The patients were aged between 61 and 97 years old and all patients had underlying health conditions. 
 Statement issued on 12th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 128 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Two of those have been announced today.
 One patient was in their 60s and one was in their 70s. Both patients had underlying health conditions. 
 Statement issued on 11th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Diane Wake, Chief Executive of the Dudley Group NHS Foundation Trust, said:
 Sadly, we can confirm that 126 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Seven of those have been announced today.
 The patients were aged between 61 and 80 years old and all but one patient had underlying health conditions.
 Statement issued on 10th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 119 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Nine of those have been announced today.
 The patients were aged between 70 and 96 years old and all patients had underlying health conditions.
 Statement issued on 9th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 110 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Four of those have been announced today.
 The patients were aged between 76 and 86 years old and all patients had underlying health conditions. 
 Statement issued on 8th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 106 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Eleven of those have been announced today.
 The patients were aged between 61 and 94 years old and all patients had underlying health conditions.
 Statement issued on 7th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 95 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Fifteen of those have been announced today.
 The patients were aged between 50 and 96 years old and all patients had underlying health conditions.
 Statement issued on 5th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 80 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Sixteen of those have been announced today.
 The patients were aged between 67 and 100 years old and all patients had underlying health conditions. 
 Additional statement issued on 4th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 64 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Ten of those have been announced today.
 The patients were aged between 57 and 90 years old and all but two patients had underlying health conditions. 
 Statement issued on 4th April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 54 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Three of those have been announced today.
 The patients were aged between 80 and 89 years old and all three patients had underlying health conditions.
 Statement issued on 3rd April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 51 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Nine of those have been announced today.
 The patients were aged between 73 and 91 years old and all nine patients had underlying health conditions.
 Statement issued on 2nd April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that a woman who was being cared for at Russells Hall Hospital, and had tested positive for COVID-19, has died.
 The patient, who died on 29th March, was in her 70s and had underlying health conditions.
 Her family has been informed and our thoughts and condolences are with them at this difficult and distressing time.
 Statement issued on 1st April by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 41 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Eleven of those have been announced today.
 The patients were aged between 55 and 85 years old and all eleven patients had underlying health conditions.
 Statement issued on 31st March by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 30 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Four of those have been announced today.
 The patients were aged between 57 and 88 years old and all four patients had underlying health conditions.
 Statement issued on 30th March by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 26 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Six of those have been announced today.
 The patients were aged between 50 and 97 years old and all six patients had underlying health conditions. 
 Statement issued on 29th March by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 20 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Three of those have been announced today.
 The patients were aged between 69 and 85 years old and all but one had underlying health conditions.
 Statement issued on 28th March by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 The patient, who died on 25th March, was in his 70s and had underlying health conditions.
 Statement issued on 27th March by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 Sadly, we can confirm that 16 patients being cared for at our hospital, and had tested positive for COVID-19, have died. Five of those have been announced today.
 The patients were aged between 65 and 90 years old and all five patients had underlying health conditions.
 Statement issued on 24th March by Diane Wake, chief executive of The Dudley Group NHS Foundation Trust
 “The Trust can confirm that two more patients, one in their 70s and one in their 90s, with underlying health conditions have died. The patients had tested positive for Covid-19.
 “The families have been informed and our condolences and our thoughts are with them at this difficult time.
 Statement issued on 23rd March by Diane Wake, chief executive of  The Dudley Group NHS Foundation Trust
 “The Trust can confirm that a patient in their 90s being treated for underlying health conditions has sadly died. The patient had tested positive for COVID-19.
 “The family has been informed and our condolences and our thoughts are with them at this difficult time.
 Statement issued on 21st March by Diane Wake, chief executive of  The Dudley Group NHS Foundation Trust
 “The Trust can confirm that a patient in their 60s being treated for underlying health conditions has died. The patient had tested positive for Covid-19.
 Joint statement issued on 20th March by Public Health Dudley, The Dudley Group NHS Foundation Trust and Dudley Clinical Commissioning Group:
 People in Dudley are being urged to do all they can to help stop the spread of coronavirus as the number of confirmed cases nationally and locally continues to rise.
 There are now a number of confirmed cases of coronavirus in Dudley, and a large proportion of patients have been discharged from hospital and are isolating at home. But given that the number of cases is expected to continue to rise in the coming days, it is even more important that communities follow the advice of health professionals and adhere to the measures announced by the Government in recent days to help minimise the spread of coronavirus further.
 Deborah Harkins, Dudley Council’s Director of Public Health, said: “It is important that people should not be unduly worried by the increase in the number of coronavirus cases which are being confirmed nationally and locally – but it is equally important that we now all take steps to help prevent the further spread of coronavirus.
 “In particular, this means following the social distancing measures announced by the Government earlier this week.
 “If you are well, are under 70 or do not have an underlying health condition, you are advised to limit your social contact as much as possible, including using less public transport, working at home if you can and not going to pubs, restaurants, theatres and bars.
 “If you are aged 70 and over, are younger but have an underlying health condition, or you are pregnant, you need to be particularly stringent in following the social distancing measures, and to also significantly limit face-to-face interaction with friends and family as much as possible.
 “Washing your hands with soap and water more often, for at least 20 seconds, is still the best way that you can protect yourself and others. Cover your mouth and nose with disposable tissues when you cough or sneeze. If you don’t have a tissue, sneeze into your elbow, not into your hand. Put used tissues in the bin immediately and wash your hands afterwards.
 “Clearly, the next few weeks are going to be incredibly difficult for everyone as we reduce our social interaction to protect our families and other people – but it is vitally important that we all follow this guidance so that we can all do our bit to help prevent and slow the spread of coronavirus.”
 The Dudley Group NHS Foundation Trust is now routinely testing patients for coronavirus if they display symptoms of respiratory illness. As reflected in the national figures, a large proportion of patients tested are negative.
 Public health laboratories are working hard to get test results out in a timely manner and prioritising those that are for the most unwell patients in hospital. At all times, regardless of whether the results are confirmed, the Trust is taking steps to cohort patients to ensure that they and others receive the care that they need in a safe environment.
 Meanwhile, the Trust, council and Dudley Clinical Commissioning Group are writing to all vulnerable residents providing important guidance to those most at risk. The council has also developed a webpage with advice specifically for vulnerable patients living in Dudley about how to access support from Dudley service. For more information, please visit www.dudley.gov.uk/coronavirusolderpeople
 There’s also lots of advice on how people can protect themselves and their families online at www.nhs.uk/coronavirus. For further information and guidance, please also visit www.dudleyccg.nhs.uk/coronavirus/
 Statement issued on 19th March by Diane Wake, Chief Executive of the Dudley Group NHS Foundation Trust:
 “The Trust can confirm that two more patients, one in their 80s and one in their 40s, with underlying health conditions have died. The patients had tested positive for Covid-19.
 Statement issued on 18th March by Diane Wake, Chief Executive of the Dudley Group NHS Foundation Trust:
 “The Trust can confirm that a patient in their 70s being treated for underlying health conditions has died. The patient had tested positive for Covid-19.
 Statement issued on 17th March by Diane Wake, Chief Executive of the Dudley Group NHS Foundation Trust:
 “The Trust can confirm that two more patients, one in their 80s and one in their 90s, being treated for underlying health conditions have died. The patients had tested positive for Covid-19.
 Statement issued on  15th March by Diane Wake, Chief Executive of the Dudley Group NHS Foundation Trust:
 “The Trust can confirm that a patient in their 80s being treated for underlying health conditions has died. The patient had tested positive for Covid-19.
 Statement issued on 11th March by Diane Wake, Chief Executive of the Dudley Group NHS Foundation Trust:
 NHS 111 has an online coronavirus service that can tell you if you need medical help and advise you what to do.
  Use this service if:
  Do not go to a GP surgery, pharmacy or hospital. Call 111 if you need to speak to someone.
 Like the common cold, coronavirus infection usually occurs through close contact with a person with novel coronavirus via cough and sneezes or hand contact. A person can also catch the virus by touching contaminated surfaces if they do not wash their hands.
 Testing of suspected coronavirus cases is carried out in line with strict guidelines. This means that suspected cases are kept in isolation, away from public areas of GP surgeries, pharmacies and hospitals and returned home also in isolation. Any equipment that come into contact with suspected cases are thoroughly cleaned as appropriate. Specific guidance has also been shared with NHS staff to help safeguard them and others. Patients can be reassured that their safety is a top priority, and are encouraged to attend all appointments as usual.
 Everyone is being reminded to follow Public Health England advice to:
 http://www.dgft.nhs.uk/wuhan-novel-coronavirus-advice/
