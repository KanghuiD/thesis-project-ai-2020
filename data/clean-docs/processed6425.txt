 Gianficaro: No pomp due to circumstance regarding high school graduation ceremonies - Opinion - The Intelligencer - Doylestown, PA
 Despite the risk of becoming infected by the coronavirus at high school graduation ceremonies, some parents are urging school districts to reconsider the decision to cancel commencement.
 They represent the most unimaginable bookends to lives whose dreams have barely left the ground.
 They’re high school seniors, born in the shadow of 9/11, and now, peripheral victims of the coronavirus pandemic at the very moment graduation comes into view.
 Lives bracketed by terror and trepidation nearly two decades apart. Unforeseen and unfair. The final chapter of their senior year canceled; an untreatable virus representing a chilling wind gust that blew away fallen leaves that formed a blazing path to commencement.
 No in-school classes since mid-March. No sports. No theater. No band. No senior class trip to Disney World. No prom. No final goodbyes and embraces in hallways. No graduation ceremony. Cap and gone. No pomp due to circumstance. No classic ending. For them, this is Bogart skipping the airport.
 Much like his fellow senior classmates, our son laments the loss of those final moments of high school: donning cap and gown, walking across a stage to receive his diploma, scanning the crowd to find Mom, Dad and younger sister shedding tears of joy. The pickpocketing of those school-days benchmarks stings, disappoints, and, at times, angers. But, he acknowledged, the shutdown is not without good reason.
 “People need to think about how dangerous this virus really is, and not complain or push for the district to try to hold a ceremony,” he said, echoing the decision by superintendents of 22 school districts in the county, as well as the Montgomery County Board of Commissioners. “People need to stay home and stay safe for as long as possible so no one else gets sick.
 “Yes, as a senior, I am very disappointed I won’t experience these things. But the lives of other people are more important.”
 Yet a deafening hue and cry has been sounded by some parents clamoring for a graduation ceremony for their children. Their desires, at times, border on fury.
 “We’ve waited all their lives for this moment! … Why decide today to cancel graduation? … Let’s wait until later this summer to hold commencement! ...We’re getting cheated out of one of the most important days of their life!”
 Among the suggestions proposed is to postpone June graduation until the summer or early fall, when it is presumed the spread of the virus will have slowed considerably. By then, many of the grads will have scattered about the country, attending college, trade schools or the military. How fair would it be for a graduate to miss out on a delayed commencement ceremony simply because of military commitment or because their college choice is on the other side of the country? How cheated would those who couldn’t make it back home feel?
 Among the more irresponsible suggestions I’ve seen is this:
 “Let’s hold graduation in June. Everybody will wear their mask. It’ll be OK.”
 Our son’s graduation class size is just over 1,000. Factor in parents, family members, administration and security, and that makes for one crowded assembly in the middle of a pandemic that has infected nearly 1 million Americans and caused the death of nearly 58,000 in less than four months.
 How confident might you be that the person seated beside or behind you will keep their mask on when their child is receiving a diploma, not sliding it away from their mouth to scream their name and hoot and holler? How might you feel as a parent if a week after graduation, your child comes down with symptoms consistent with COVID-19? How might you feel if the test comes back positive? For them? For you? How important might commencement feel then? The risk is low. But is it one you’re willing to take?
 I suppose it all comes down to what’s most important to you during a pandemic the likes of which we’ve never experienced:
 Your child walking across a stage to receive a piece of paper.
 Or their continued good health.
 Which classic ending matters most?
 							Choose the plan that's right for you.
 							Digital access or digital and print delivery.
 						
 Original content available for non-commercial use under a Creative Commons license, except where noted.
 Gannett | USA TODAY NETWORK
 https://www.theintell.com/opinion/20200429/gianficaro-no-pomp-due-to-circumstance-regarding-high-school-graduation-ceremonies
