This Week in Virology | A podcast about viruses - the kind that make you sick
 This is a placeholder for your sticky navigation bar. It should not be visible.
 Daniel Griffin provides his weekly update on the COVID-19 clinical situation, followed by our discussion of tests of an inactivated vaccine, results of serological surveys, an inhibitor of the cell protease needed for virus entry, and answers to listener questions.
 Hosts: Vincent Racaniello, Alan Dove, Rich Condit, and Kathy Spindler
 Guest: Daniel Griffin
 Become a patron of TWiV!
 Intro music is by Ronald Jenkees.
 Ralph Baric, Mark Heise and Nat Moorman discuss their non-profit initiative READDI, Rapidly Emerging Antiviral Drug Discovery Initiative, to start making drugs now for the next pandemic virus.
 Hosts: Vincent Racaniello, Rich Condit, and Kathy Spindler
 Guests: Ralph Baric, Mark Heise, and Nat Moorman
 Kostya Chumakov discusses the hypothesis that oral poliovirus vaccine can provide non-specific protection against many other viruses, and might prevent infection with SARS-CoV-2.
 Host: Vincent Racaniello
 Guest: Kostya Chumakov
 Daniel Griffin provides his weekly update on the COVID-19 clinical situation, followed by results of seroprevalence studies, analysis of CpG dinucleotide frequencies in the SARS-CoV-2 genome, and answers to listener questions.
 Hosts: Vincent Racaniello, Dickson Despommier, Alan Dove, Rich Condit, Kathy Spindler, and Brianne Barker
 Stanley Perlman joins TWiV to discuss immune responses to coronaviruses, including seasonal CoV, MERS, SARS, and SARS-CoV-2, including prospects for a vaccine.
 Hosts: Vincent Racaniello, Rich Condit, Kathy Spindler, and Brianne Barker
 Guest: Stanley Perlman
 Coronavirus expert Christian Drosten joins Vincent to provide a view from Germany on COVID-19 and SARS-CoV-2.
 Guest: Christian Drosten
 Doris Cully joins TWiV to discuss inhibition of SARS-CoV-2 in cell culture by ivermectin, followed by continuing analysis of the coronavirus pandemic caused by SARS-CoV-2 including COVID-19 in Nadia the tiger, and prediction of respiratory failure by levels of IL-6.
 Hosts: Vincent Racaniello, Alan Dove, Rich Condit, and Brianne Barker
 Guest: Doris Cully
 Daniel Griffin MD returns to TWiV from a hospital parking lot to provide updates on COVID-19 diagnostics, clinical picture, and therapeutics, followed by our coverage of the coronavirus pandemic caused by SARS-CoV-2. 
 Hosts: Vincent Racaniello, Dickson Despommier,Rich Condit, and Kathy Spindler
 Guest: Daniel Griffin, MD
 Immunologist Jon Yewdell joins Vincent and Rich to discuss immune responses in the context of infection with SARS-CoV-2.
 Hosts: Vincent Racaniello and Rich Condit
 Guest: Jon Yewdell
 MicrobeTV is an independent podcast network for people who are interested in the sciences. Our shows are about science: viruses, microbes, parasites, evolution, urban agriculture, communication, and engineering.
 https://www.microbe.tv/twiv/
