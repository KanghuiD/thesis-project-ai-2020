Advertising
  UK News | Published: Mar 18, 2020 
 Once the link is clicked, recipients are asked to provide personal details, including sensitive banking information, that allows the scammer to take money directly from their account.
 “It is abhorrent that unscrupulous individuals are using this difficult time as an opportunity to immiserate others further,” said Leon Livermore, chief executive of the CTSI.
 “Consumers should now be extra vigilant and aware of these new scams that take many forms.
 “The general rule of scams applies – if it seems too good to be true, then it probably is.”
 The CTSI has also asked the public to be wary of fake smartphone apps, which claim to provide updates on the virus.
 One, known as CovidLock, which is available through unofficial websites, locks the phone and displays a message demanding that the user pay a sum of money to unlock it.
 The fresh warning comes after guidance was issued by the UK’s cyber security agency to firms and employees adopting more work from home practices in response to the Covid-19 pandemic.
 Advice from the National Cyber Security Centre (NCSC) – which is a part of GCHQ – tells organisations how they should prepare for increased remote working and ensure staff are vigilant with work devices that may contain sensitive data.
 Browse the businesses in your area and find what is perfect for you.
  Browse directory 
 If you are searching for a job around your local area, use our online system.
  Job portal 
   Back to top  
 https://www.expressandstar.com/news/uk-news/2020/03/18/tax-rebate-scam-emails-target-concerned-households-during-coronavirus-crisis/
