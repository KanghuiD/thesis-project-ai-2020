Coronavirus update LIVE: Australia's COVID-19 cases in 24-hour period lowest since March, Ruby Princess scheduled to depart Port Kembla Thursday, Australia death toll at 74
 Thanks for joining us. We're wrapping up the blog for the night.
 Here's what you may have missed today:
 Our coronavirus coverage will continue overnight and into Thursday in a new blog, which you can read here.
 Sarah Keoghan signing off.
 South Sydney coach Wayne Bennett has cancelled planned training sessions for next week as the NRL considers sending independent spies into clubs to ensure biosecurity protocols are being followed.
 With speculation rife that several clubs were planning secret sessions next week, and that some teams may have already breached government rules, the NRL emphasised the severity of punishments that would be applied during a phone hook-up with clubs on Wednesday afternoon.
 Wayne Bennett at South Sydney training before the season was suspended.Credit:Getty Images
 The NRL threatened to dock competition points and hand down heavy fines for those clubs in breach of the rules.
 It comes as ARLC chairman Peter V'landys met with Channel Nine chief executive Hugh Marks at the network's Willoughby offices on Wednesday morning to continue discussions around a potential three-year broadcast rights extension with the current partners.
 Read more here. 
 Virgin Australia has told staff its subsidiary Tigerair will remain an essential part of its low-fares offering after the troubled company emerges from voluntary administration.
 Staff dialling into a briefing on Tuesday have also been told Virgin's administrators are "not contemplating redundancies at this point", The Age and The Sydney Morning Herald have confirmed.
 Tigerair: prepare for not taking off.Credit:James Morgan
 Tigerair staff had been facing a nervous wait for news after Virgin Australia called in administrators on Monday night.
 In March, Virgin Australia announced it would make all pilots at its budget wing redundant after flights were grounded by the coronavirus pandemic. Since then, there have been questions about the future of Tigerair.
 Despite the assurances, Flight Attendants' Association of Australia industrial relations manager Stephen Reed said Tigerair staff remained nervous about the company's future.
 Each child will have just one parent or guardian at matches and recent overseas travellers will be banned from playing under a draft set of biosecurity measures that aim to get grassroots rugby league up and running in NSW by July.
 NSW Rugby League wants its community competitions across the state to resume in winter, easing fears more than 100,000 players would be forced to miss an entire season due to the COVID-19 pandemic.
 Replay
 NSWRL chief executive David Trodden announced on Wednesday plans for shortened junior and bush seasons to begin on July 18, the end of school holidays before term three.
 A draft set of biosecurity protocols, set to be finalised in June, will limit the amount of people at matches with children and schedule further time between games.
 Belgrade: Serbia's elderly have ventured outside for the first time in more than a month as authorities eased some of the strict measures that have been in place against the new coronavirus.
 The government now allows people over 65 years old to go out for a walk three times a week during a daily curfew when the rest of the population must stay indoors. Authorities say they want to keep the vulnerable elderly residents safe.
 An elderly woman wearing a mask against the spread of the new coronavirus walks during a curfew in downtown Belgrade, Serbia.Credit:AP
 Serbia has introduced some of the toughest rules in Europe as part of efforts to curb the spread of the virus. People over 65 were only allowed to go shopping for groceries once a week and very early in the morning.
 Most people who ventured out on Tuesday evening, local time, were wearing face masks and some couples were holding hands. One man said he was out for the first time in 35 days.
 The country has reported 6890 cases of new coronavirus infection, while 130 people have died.
 AP
 The state's 2200 public schools will have access to enough soap and hand sanitiser to last 12 months when they start returning to classrooms next month.
 The Herald also understands 10,000 thermometers will be available to allow teachers to do temperature checks.
 Private school expects 900 students on Tuesday after parents rejected remote learning Credit:Michele Mossop
 Premier Gladys Berejiklian's push to have all students back at school full-time by term three includes a significant roll-out of key products to ensure schools can meet strict health requirements.
 Forehead thermometers will be handed out to schools as part of the staggered return to classrooms as NSW continues to deal with the COVID-19 crisis and teachers will have priority for testing.
 Surgeons are preparing to make "tough decisions" about which patients will be able to have their elective surgery - including cataract removal, hip and knee replacements - when the ban on many non-essential procedures lifts next week.
 Patients with multiple chronic health conditions such as heart disease, high blood pressure and diabetes, including some elderly patients, may have to keep waiting for procedures that could put them at risk of complications, as doctors draw up guidelines to prioritise "low-risk, high value care".
 Not everyone waiting for elective surgery will be able to get it when the ban lifts next week. Credit:iStock
 Royal Australian College of Surgeons President Tony Sparnon said surgeons would have to weigh up multiple factors in deciding which patients to book in for surgery in the coming weeks, including how much pain they were in and the likelihood of requiring a stay in intensive care.
 "This is where the surgeons and clinicians will have to make some tough decisions," he said.
 The final health report lodged by the Ruby Princess before passengers were allowed to disembark was out-of-date and did not include all passengers who had symptoms consistent with COVID-19.
 As a special commission of inquiry into the cruise ship began on Wednesday, the hearing was told the senior doctor on board did not finalise the illness log until the day after passengers disembarked.
 The Ruby Princess and (inset) senior doctor Dr Ilse Von Watzdorf.Credit:Kate Geraghty, supplied
 Bret Walker SC, is the commissioner overseeing the inquiry into the Ruby Princess debacle, which has seen more than 650 cases of COVID-19 in passengers and 21 deaths. There have also been more than 150 coronavirus cases diagnosed in the ship's crew.
 The commission heard the last "human health report" provided to Australian authorities on March 18 was incomplete and did not include all passengers and crew who had acute respiratory or influenza-like symptoms.
 The family of a Victorian cancer patient who died in an isolation ward while awaiting a coronavirus test say they feel “some measure of satisfaction” after receiving an apology from Frankston Hospital.
 Mother-of-two Gill Rayson who had terminal lung cancer was refused a transfer to palliative care in her final hours in late March. Her children were allowed just five minutes to say their goodbyes and Mrs Rayson's mother was banned from visiting her while she was in isolation.
 Gill Rayson at Point Leo Estate in April last year, before she left for a round-the-world trip with her husband Mark.
 Frankston Hospital told Mrs Rayson's family that the case had since been reviewed by senior doctors and that the hospital would make changes to the way such patients were treated in future.
 “Gill would like to know that someone benefited from our traumatic experience, because that’s the sort of person she was,” said her husband, Mark Rayson.
 The Morrison government will not intervene in state bidding wars to rescue Virgin Australia as the troubled airline fields bids from investors and governments to help it recover from a financial collapse.
 The federal government has rejected the airline’s pleas for cash and will wait for bidders to emerge over the next three weeks before any decision on further support.
 In a briefing with company chiefs, Treasurer Josh Frydenberg restated the government's commitment to having two airlines but made it clear the government was not interested in "bailing out" Virgin shareholders with deep pockets.
 The Morrison government is letting states outbid each other to help Virgin Australia.Credit:Wolter Peeters
 The mass mail-out from ACTU president Michele O’Neil said the airline’s collapse is a “do or die” moment for the Morrison government, but MPs are backing the government’s response so far.
 https://www.smh.com.au/national/coronavirus-updates-live-donald-trump-suspends-immigration-to-us-australian-death-toll-stands-at-72-as-covid-19-cases-near-2-5-million-worldwide-20200421-p54lvm.html
