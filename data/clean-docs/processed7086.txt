'The US is no match for China': Beijing's state media compares America to a 'primitive society' | Daily Mail Online
 By MailOnline Reporter 
  Published:  16:16 BST, 21 April 2020   |  Updated:  21:09 BST, 21 April 2020   
  11
 A Chinese newspaper has slammed America's handling of the coronavirus crisis and compared the US to 'a primitive society' in a recent opinion article.
 Beijing's state media Global Times claimed that the US is 'no match for China in terms of anti-epidemic organization and mobilization' in a column published on Sunday.
 A volunteer looks out near a Chinese national flag during a farewell ceremony for medical workers who came from outside Wuhan to help the city during the coronavirus outbreak
 A Chinese newspaper has slammed America's handling of the coronavirus crisis, labelling the US as 'a primitive society' in a recent opinion article. People are seen waiting in line for a coronavirus test at one of the new walk-in COVID-19 testing sites that opened in New York City
 It comes as Chinese officials stated yesterday that the enemy of the US is the coronavirus rather than China.
 In a column titled 'Smearing China a lame trick to aid reelection for White House', the Global Times described the escalating epidemic in the US is 'like that of a primitive society', blasting Washington's poor handling of the virus outbreak.
 'The COVID-19 spread in the US is almost like that of a primitive society. It should not have been like this if the US had the slightest science and organization,' the Chinese state media outlet said.
 'To put it bluntly, the U.S. is no match for China in terms of anti-epidemic organization and mobilization.
 'The US political system has been hit by the pandemic on its weak side and we were willing to show understanding for that. After all, every system has its weaknesses,' the newspaper asserted.
 The Global Times described the escalating epidemic in the US is 'like that of a primitive society', blasting Washington's poor handling of the virus outbreak. The file picture shows the Chinese President Xi Jinping visiting Wuhan during the coronavirus epidemic
 The Global Times also suggested that the US President Donald Trump tries to gain an advantage in the upcoming election by smearing Beijing. President Trump is pictured speaking at the daily coronavirus briefing at the White House yesterday
 Despite the nation has come under criticism of covering up the crisis, Beijing's state media insisted that 'the Chinese government has done its utmost to fight the epidemic'.
 'China's achievements stand the test of time and all kinds of doubts. The Trump administration only wants to shirk responsibility by blackening China.' 
 The Global Times also suggested that the US President Donald Trump tries to gain an advantage in the upcoming election by smearing Beijing.
 'The Trump administration has repeatedly found fault with China, blatantly accused China of fabricating epidemic achievements, eagerly passed the buck to China and unscrupulously attempted to win reelection by stepping on China,' the Global Times concluded. 
 Despite the nation has come under criticism of covering up the crisis, Beijing's state media insisted that 'the Chinese government has done its utmost to fight the epidemic'. The picture taken on February 6 shows medical workers transferring a patient at a hospital in Wuhan 
 The Global Times suggested that the US President Donald Trump tries to gain an advantage in the upcoming election by smearing Beijing. Hundreds of people gather to protest against the lockdown yesterday in Sacramento, California
 'Some believe that American democracy is dying. Indeed, the recent chaos in the country has proved its in decay,' the newspaper suggested in the article titled 'Divisive US politics leads to democracy's fast decay'.
 The Global Times first noted that the US democratic system has 'a balance of power between states and the federal government'.
 'However, as this public health crisis expands into to the economic, political and social fields with many subsequent effects surfacing, the weakness of the system is being exposed,' the newspaper added.
 'It allows infighting between the federal and the states at this critical time, aggravates US society's fragmentation and polarization, and will erode the country's long-term interests.
 'And it is the American people who will bear the losses and pain,' the Chinese state media said.
 Although the Global Times claimed that the article was based on an interview with Wang Yiwei, a Chinese professor. The column did not indicate or use any quotes from the academic.
 Hua Chunying, a spokeswoman from China's Ministry of Foreign Affairs, said in a tweet on Monday that 'right decisions can only be built on Facts'. The picture shows her speaking at a press conference in Beijing on March 30
 The columns come as Chinese officials stated yesterday that the enemy of the US is the coronavirus rather than China.
 Hua Chunying, a spokeswoman from China's Ministry of Foreign Affairs, said in a tweet on Monday that 'right decisions can only be built on Facts'.
 'Some people in the US should know that their enemy is the virus, not China.'
 Another Beijing's spokesperson, Geng Shuang, also called the US to stop blaming China for the coronavirus crisis during a press conference yesterday.
 'The virus is the enemy of the humankind. China, like other countries, has been attacked by the virus. We are the victim, not the culprit or the accomplice of the virus,' said Geng.
 Geng Shuang, a spokesperson for Beijing, called the US to stop blaming China for the coronavirus crisis during a press conference yesterday. Geng is pictured during a daily briefing at the Ministry of Foreign Affairs office in Beijing on March 18
 On Thursday US Secretary of State Mike Pompeo demanded that China 'come clean' after unsubstantiated reports that coronavirus may have originated in a lab in Wuhan. Mr Pompeo speaks during a news briefing with members of the Coronavirus Task Force at the White House
 China and the US have been in a consistent war of words over the source of the deadly disease that has infected nearly 2.5 million people worldwide.
 On Thursday US Secretary of State Mike Pompeo demanded that China 'come clean' after unsubstantiated reports that coronavirus may have originated in a lab in Wuhan.
 Australian minister Peter Dutton on Friday also ordered China to 'answer those questions' before Ms Payne on Sunday called for an independent inquiry.
 Australian minister Peter Dutton on Friday ordered China to 'answer those questions' before Ms Payne on Sunday called for an independent inquiry. The picture shows a worker inside the  controversial P4 laboratory in Wuhan
 Meanwhile, Chinese academics have accused Australia of joining an 'anti-China crusade' with the US to 'smear China'.
 Beijing's state media have been promoting debunked conspiracy theories and suggesting that the pandemic started in the US.
 It comes as a CGTN presenter hinted in an Arabic programme that the contagion could have escaped from a US lab or been brought into China during the Military World Games in Wuhan last October.
 China has accused the United States of threatening and blackmailing after Washington decided to stop funding the World Health Organization (WHO).
 US President Donald Trump last week said that America would withhold some $500million in WHO funding, accusing the group of failing in its basic duty in its handling of the pandemic.
 The Chinese Ministry of Foreign Affairs on Tuesday labelled Washington's move as a 'typical' representation of hegemony. 
 Medical workers are pictured transporting a patient outside of a special coronavirus intake area at Maimonides Medical Centre in New York on April 20
 Geng Shuang, a spokesperson of the Ministry, said during a daily press briefing in Beijing: 'The United States relies on the fact that it is the largest financial contributor to the WHO and thinks the WHO should listen to its command. This is a typical hegemony mentality.'
 Mr Geng praised the WHO for being objective and just.
 He said: '[Because] it did not appease the US side, the US side then stopped contributing, this is typical threatening and blackmailing [behaviour].
 'The US side attacked and smeared the WHO without factual basis. [Its] pressuring and intimidation will not win hearts.'
 Trump said last Tuesday that the US would withhold the WHO funding while an investigation into its handling of the pandemic was carried out.
 Trump singled out what he called the WHO's 'dangerous and costly decision' to argue against international travel bans to combat the pandemic.
 The US is the largest single contributor to the WHO, paying in some $893million between 2018 and 2019 which made up around 15 per cent of the agency's total budget during that period.
 The head of the WHO said he 'regrets' Trump's decision and called for 'solidarity' in tackling the coronavirus pandemic.
 	    Share what you think
           
       The views expressed in the contents above are those of our users and do not necessarily reflect the views of MailOnline.
     
 Published by Associated Newspapers Ltd
 Part of the Daily Mail, The Mail on Sunday & Metro Media Group
 https://www.dailymail.co.uk/news/article-8240963/The-no-match-China-Beijings-state-media-compares-America-primitive-society.html
