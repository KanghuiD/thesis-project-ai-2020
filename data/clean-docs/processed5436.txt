Coronavirus: Impact on App Installs and Budgets | AppsFlyer
 The rapid spread of the Coronavirus (COVID-19) is sending shock waves across the globe as we all try to adapt to a new reality. 
 Businesses are scrambling to understand what they are facing, and what actions they need to take to keep going during an economic crisis.
 What will 2020 look like for mobile apps? As people stay home, they turn to their mobile phones to perform multiple activities, which presents an opportunity for many verticals. But in the longer-term, an economic recession will decrease overall consumer spend and lower user lifetime value.
 Clearly, these are challenging times, filled with uncertainty. At AppsFlyer, we hope to empower app marketers and the market with data updated every week to help them make smarter decisions. 
 The data sample included 2.5 billion organic installs, 2 billion non-organic installs, and 950 billion sessions (app opens) during the past eight weeks.
 Click on the image below for the complete Coronavirus (COVID-19) Marketing Updates report, covering key trends in 16 markets:
 https://www.appsflyer.com/blog/coronavirus-impact-app-installs-marketing/
