Coronavirus: Donors urged to travel further to give blood - BBC News
 People are being urged to go the extra mile to give blood during the coronavirus pandemic.
 Travelling to donate is classed as "essential travel" and the Welsh Blood Service is now asking people to travel to regional hubs to donate.
 While stocks in Wales are currently healthy, pressure on the service means less blood is being taken.
 About 100,000 units of blood need to be donated to supply Wales' 19 hospitals every year.
 The service said since the outbreak 30% less blood had been collected - but with an equal reduction in demand from hospitals, stock remained healthy.
 However, the service said it was under pressure and was now reducing collections to protect staff and maintain the supply.
 To allow people to donate close to their home or work, weekly collections had been held in 30 locations across Wales.
 Now donors will be asked to travel to one of five regional hubs, held in different places each week.
 Director Alan Prosser said: "We are asking our amazing blood donors to go the extra mile to help our NHS at this difficult time."
 Under the changes, existing donors will be notified by text or phone call of any donation hub sessions taking place within a 15-mile (24km)radius of their usual donation point.
 They will then need to make an appointment at their nearest hub.
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/uk-wales-52136718
