Timaru Herald: Timaru/South Canterbury news | Stuff.co.nz
                     An 18-year-old man has been arrested about 75 kilometres south of where he initially fled police.
                 
                     Timaru automotive technicians have dealt with Warrant of Fitness requests not flat batteries as anticipated.
                     As we navigate Alert Level 3 and start to look at what comes next, planning is under way to rebuild tourism in the regions.
                     After being closed for five weeks with no income and with bills mounting a Timaru hairdresser is holding onto hope her business will be able to open at alert level 2.
                     Duck shooters who normally travel long distances to their favourite spot are likely to be forced to hunt closer to home as the delayed season gets underway.
                     A campaign to actively encourage residents of South Canterbury to support the local economy is gaining traction.
                     Now New Zealand is out of alert level 4, and with businesses starting to reopen we asked leaders, experts and business owners how well they think South Canterbury will fare through the Covid-19 crisis.
                     A young girl sexually abused by her stepfather for more than three years believed it was "normal for a father to show his love this way".
                     The South Island has recorded a fifth straight day without any new Covid-19 cases.
                     Open community Covid-19 testing in Tekapo on Thursday.
                      Dr John Trent, who studied at Timaru Boys' High School, is now exploring potential coronavirus treatments in the US.
                     A Timaru optometrist took on the frustrating task of building a kitset greenhouse with no instructions because he was in lockdown.
                     An Orari drink driver who slammed into a bridge abutment at 70 kilometres an hour while driving his friend home from a party has been sentenced.
                     Eleven of the 17 Covid-19 cases have recovered, while no new Covid-19 cases were reported for South Canterbury on Wednesday.
                     A number of schools around Timaru re-opened on Wednesday with few students as parents who could, kept their children at home.
                     Pupil numbers were low as South Canterbury schools reopened for those from year 10 down on Wednesday.
                     Octogenarians enjoy a drive to the river to listen to the fantails under new level 3 freedom.
                     OPINION: Keeping anxiety under control at the moment is no easy task, but a river helps.
                      OPINION: Meanwhile: we go hard, we go early, and we stay the course.
                     OPINION: Can we get away from continuous consumption of stuff we don't need?
                     Opinion: A four-week lockdown to counter the growing threat of Covid-19 can be a bit of a challenge but it can also throw up some amazing benefits.
                     Opinion: When people appearing in court get sentenced to home detention, I've always wondered how much of a punishment/deterrent to crime that really is.
                     A V8 car owner watches with frustration as fuel prices drop.
                         The South Island has recorded a fifth straight day without any new Covid-19 cases.
                     View obituaries from around the region.
                     View marriage and birth notices from around the region.
                     Keep up with @TimaruHerald headline tweets.
                     Join the conversation on The Timaru Herald page.
 Breaking news?
 Send us your photos, videos and tip-offs:
 Phone0800 697 8833
 SecureDrop
 Share with us securely
 https://www.stuff.co.nz/timaru-herald
