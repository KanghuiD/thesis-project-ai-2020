India coronavirus: World's largest postal service turns lifesaver - BBC News
 India has the largest postal service in the world - and now it is stepping in to help deliver lifesaving medicines during a countrywide lockdown aimed at tackling the coronavirus pandemic. The BBC's Ayeshea Perera in Delhi reports.
 Red postal vans are a familiar sight in India. They make thousands of journeys every day, criss-crossing the country's wide network of post offices in 600,000 villages.
 The postal service does much more than deliver letters and packages. It is also a bank, a pension fund and a primary savings instrument for millions of Indians. Now it will also be transporting medical equipment and drugs to where they are needed most, at a time when transport has come to a standstill.
 Then, he got a call from Alok Ojha, the senior superintendent of the postal service in Uttar Pradesh, India's most populous state.
 The postal service had already partnered with the IDMA in the western state of Gujarat to deliver medicines and equipment as a priority. Mr Ojha was offering to do the same on a much wider scale. 
 "We were definitely looking for a solution, and the postal service has unhindered access the country," Mr Madan said.
 "We thought we could help with this as we have a supply chain that is intact. Many people I spoke to said this would help as it helps keep drugs in the market and prevents hoarding," Mr Ojha told the BBC.
 As word spread, many people began calling and asking for help.
 Dr Ujjala Ghoshal, a microbiologist at the Sanjay Gandhi Postgraduate Institute of Medical Sciences in the northern city of Lucknow, told the BBC she got in touch with Mr Ojha when a batch of Covid-19 testing kits she urgently needed was stuck in the capital Delhi, 550km (340 miles) away.
  "The Institute of Medical Research told us that we would have to send someone to Delhi to collect the kits because the courier company they usually used was not working, but there was no way we could do it because of the lockdown," she said.
  The postal service, she said, made an exception and actually went to the institute to pick up the kits, instead of having them dropped off at a post office. She received them a day after she made the request.
 Many other institutions and companies have made similar requests. Mr Ojha says ever since the lockdown began, the postal service has been used to deliver everything from batches of lifesaving drugs to Covid-19 tests, to N95 masks and ventilators, moving medicine and equipment between major cities and states - mostly via the red postal vans.
 For longer or very urgent journeys - such as a consignment of defibrillators that had to be transported from the state of Tamil Nadu in the south to Uttar Pradesh in the north - cargo planes are used. Sometimes, the consignments must be handled with special care - one drug manufacturer who asked for help said his medicines needed cold chain maintenance, which means they need to be frozen while transported. And so far, every request made to the department has been fulfilled.
 "We are the best-connected service in India. We are everywhere. And in this case, we knew we could help," Mr Ojha says.
 And with the lockdown set to be extended, he anticipates that the service will play a larger role in the weeks ahead.
 The World Health Organization "failed in its basic duty" over coronavirus, Donald Trump says.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-asia-india-52268601
