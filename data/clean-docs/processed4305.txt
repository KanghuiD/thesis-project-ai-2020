Coronavirus: Group Homes Are Vulnerable & Need Help | National Review
 National Review
 								The 142 Most Absurdly Locked-Down Counties in America							
 								Blame Bill de Blasio							
 								Can Restaurants Survive?							
 								Criminalizing Politics: The Investigation of General Flynn							
 								Planned Parenthood’s Ambassador to CBS News							
 Last week the New York Times reported that 105 persons had died in group homes in the New York City metropolitan area. Now the number is over 200. People living in group homes are five times more likely to die where they reside than are those living in nursing homes. The situation has been addressed also in an opinion piece in the New York Daily News.
 The history of how the developmentally disabled have been treated is reprehensible. One hundred years ago psychologist Robert Goddard, affiliated with the eugenics movement, used intelligence tests to determine those who should be sterilized. Many involuntary medical operations robbed people of the gift of bearing children, becoming a father, and creating a home for a family.
 To reduce the disproportionate fatality rate in group homes, the federal government as well as state and local governments should give them immediate special funding to reduce the number of infections and deaths. At the same time, the homes need ongoing financial help at an increased level, during the pandemic and afterward. Where might that help come from?
 The immediate emergency situation can be traced to several factors, and there can be interventions for each. Many of the group homes are small, and so their cubic air capacity is much less than in most nursing homes, making the air itself more virulent. The behavior of the developmentally disabled makes true social distancing nigh impossible. Many have repetitive behaviors such as scratching the face on their skin until it bleeds. They are prone to hug and also to roughhouse, which brings them into close contact with others. And how can they social-distance if they can’t understand the reason for it?
 				jwplayer('jwplayer_XtkmEUkk_8HR1M1dH_div').setup(
 				{"displaydescription":false,"playlist":"https:\/\/cdn.jwplayer.com\/v2\/playlists\/XtkmEUkk?contextual=true&search=__CONTEXTUAL__&recency=30D","ph":2}
 			);
 		
 	(function() {
 		if("function" === typeof jwplayer) {
 			var jwp = jwplayer(document.querySelectorAll(".jw-player-container div.jwplayer")[0]) || null;
 			if(jwp) {
 				var episode = jwp.getConfig().playlist[0];
 				var pubdate = new Date(episode.pubdate * 1000);
 				var prelimMonth = pubdate.getUTCMonth() + 1;
 				var month = prelimMonth.length > 1 ? prelimMonth : "0" + prelimMonth;
 				var prelimDate = pubdate.getUTCDate();
 				var date = prelimDate.length > 1 ? prelimDate : "0" + prelimDate;
 				var finalDate = pubdate.getUTCFullYear() + "-" + month + "-" + date;
 				var title = episode.title;
 				var labelMapping = "c3=\"nationalreview\", c4=\"*null\", c6=\"*null\", ns_st_st=\"National Review\", ns_st_pu=\"National Review\", ns_st_pr=\"" + title + "\", ns_st_ep=\"" + title + "\", ns_st_sn=\"*null\", ns_st_en=\"*null\", ns_st_ge=\"News\", ns_st_ia=\"0\", ns_st_ce=\"0\", ns_st_ddt=\"" + finalDate + "\", ns_st_tdt=\"*null\"";
 				jwp.on("ready", function () {
 					ns_.StreamingAnalytics.JWPlayer(jwp, {
 						publisherId: "23390304",
 						labelmapping: labelMapping,
 					});
 				});
 			}
 		}
 	})();
 Medical problems and the treatment they necessitate add to the spread of infection. Many residents, often owing to obesity or hypotonia (which restricts air intake), need to be on compressed-air machines. Their lungs do not work well, making the virus more deadly for them. The CPAP (continuous positive airway pressure) machines need to be thoroughly cleaned. It is a herculean task for the workers.
 Meals are fertile breeding grounds for the virus. Many need close hand-over-hand physical guidance to bring food from their plate to their lips. This makes the job dangerous for staff. Owing to their muscle and bone structure, some residents have severe problems with eating and need the help of highly trained speech and language pathologists to do the feeding and to instruct others on how to do it. This never was a glamorous or sought-after job, and in the aftermath of the pandemic it will become even less so.
 The travel of staff back and forth between the group homes and their own residences may be the worst source of the virus. A single infection brought into a group home can spread fast. The temperature checks and other tests are imperfect, and infected workers may pass them and enter the home. Once that happens, a medical crisis occurs quickly, followed by deaths.
 A major question is how to keep the staff free of the virus so that the spread of infection in the homes can be stopped. Police and fire departments, certain businesses, and now meatpacking plants have developed procedures for employees to be transported to a hotel or safe living environment between shifts. At the meatpacking plants, employees can live safely on the premises, run on twelve-hour shifts, and stay quarantined.
 A staffer for Senator Charles Schumer (D, N.Y.) is considering add-on legislation to a bill affecting nonprofit social-welfare organizations. The bill, which would supersede the CARES Act and its iterations, would be administered by FEMA (the Federal Emergency Management Agency) and provide for federal grants that agencies could apply for. The funds received could be used for any procedures that will help keep the virus out of the group homes. Extra hazard pay for the staff would be included in the bill. Most importantly, the bill would ensure funding for transportation of staff between the group home and an uninfected site, such as a college dorm, that could be donated for use. There just isn’t enough money to pay for hotel lodging (which some police and fire departments use), so other nonprofits, large county venues, or other workable places need to be found. Partnerships will emerge.
 As someone who has worked as a clinical psychologist in this system for decades, and is father to a young man with Down syndrome, I think that the above federal grant would be lifesaving for many people. It would protect the lives of the staff as well.
 What of the day-to-day work of the agencies? There will be much more to do throughout the period of flattening the curve — as well as, perhaps, during any period of developing herd immunity and vaccine interventions, neither of which the scientists can guarantee will happen. Medicaid funding of the agencies has been reduced over the past ten years. Ratios of staff to clients are much lower than in earlier eras. In one state with over 150 agencies serving the developmentally disabled, it is well known that an orchestrated pattern of squeezing certain agencies financially is forcing some to close or merge.
 This may be an issue on which both major political parties can join together in bipartisan efforts. There are about 7 million people with developmental disabilities in the United States. When one includes parents and families and those who work with them, at least 25 million Americans, I would estimate, have a stake in these individuals, who are among the most vulnerable in our population, and who have suffered so much blatant abuse and neglect in the past. It might not be asking too much to consider designating “group-home residents” as a legally protected class of people. That would contribute to the protection of their rights and to their enhanced protection, now and in the future, throughout their lives.
 Editor’s Note: This article has been updated to clarify that legislation to fund social-welfare organizations is being considered only by a staffer for Senator Schumer.
 Get our conservative analysis delivered right to you. No charge.
 https://www.nationalreview.com/2020/04/group-homes-vulnerable-during-the-pandemic-need-help/
