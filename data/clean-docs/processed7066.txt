King Steve Sisolak said his coronavirus decisions are guided by medical professionals. Local health officials say he doesn’t consult with them. | Las Vegas Review-Journal
 Local health officials accused Steve Sisolak of ignoring them when he issues emergency directives.
 King Steve Sisolak has some explaining to do.
 On Monday, a bombshell letter from local health officials leaked to the Review-Journal. They accused His Majesty of consistently ignoring them when he issues his emergency directives. Signers included top officials from the Southern Nevada Health District, Washoe County Health District and each region’s Board of Health.
 “It is inappropriate that the local health authorities are not engaged and consulted in these discussions and when decisions are made,” the officials wrote.
 That accusation stands in stark contrast to the governor’s frequent claims that science guides his decisions.
 “I’m listening to the medical people,” King Sisolak told CNN in April. “I’m listening to scientists. They will decide, along with the virus and behavior of our citizens, when it’s time to start reopening in a phased-in approach.”
 He has repeated versions of this claim for months. It’s what he used to justify lockdowns, even as the economy tanked.
 “When we began reopening, I committed that we would remain flexible and let data and recommendations of public health and emergency management professionals dictate the best course of action for protecting Nevadans,” the governor said in July when he shut down bars a second time.
 Apparently, those professionals didn’t include the local health officials responsible for more than 85 percent of the state’s population.
 “Since we are not consulted and engaged, we receive little to no advance notice of what these policy changes may be, and we are forced to react after decisions have been made and announcements are occurring,” the health officials wrote.
 This letter makes King Sisolak sound like the world’s worst boss. He doesn’t tell you what he’s doing and then he’s upset you aren’t already doing it.
 The impetus for this letter was the governor’s decision to require health districts to review and approve the safety plans of large gatherings. In the guidelines released by his office, it says King Sisolak made these changes “in consultation with federal, state, and local health officials.”
 In a tactful way, local health officials accused His Highness of lying.
 “Local health officials were not consulted,” they said in their letter. “We were informed by (Sisolak senior adviser Scott) Gilles of what you were going to announce after 6 p.m. on the evening preceding the announcement. This was, however, more consideration than local health officials have received from you in the past.”
 Ouch.
 What’s interesting is the letter doesn’t accuse Sisolak of reopening Nevada too quickly. The officials are upset about the lack of communication and being blindsided by mandates. This is what King Sisolak’s chief of staff Michelle White didn’t seem to understand in her amazingly passive-aggressive response.
 “At no time did officials representing your communities report that easing restrictions would place an additional burden on the local health authorities,” White wrote.
 That’s probably because King Sisolak didn’t tell them he was going to dump a bunch of work in their laps.
 This incident adds more evidence to what’s been obvious for months. King Sisolak doesn’t know what he’s doing. He’s making it up as he goes along, careening wildly from one plan to another as his top-down directives don’t work as intended. It doesn’t help that the governor seems to allow White to keep him isolated from local government officials and business leaders.
 There is an easy solution. Stop misleading Nevadans. Delegate all emergency authority to the counties. Let them decide on the best course of action.
 It’s time for King Sisolak to give up his crown and go back to being Gov. Sisolak.
 Contact Victor Joecks at vjoecks@reviewjournal.com or 702-383-4698. Follow 
 If it’s safe enough to have 1,000 people at a convention, it’s safe enough to put kids in schools.
 If Joe Biden really believed Catholic teaching, the left would tear him apart. Just look at how they’re attacking Amy Coney Barrett.
 There’s now more evidence that Democrats are committing mail ballot fraud than that President Donald Trump colluded with Russia.
 It’s fair for President Donald Trump to blame America’s high coronavirus death toll on blue states. Red states have both a lower death rate from coronavirus and lower unemployment.
 There should be fans in the stands to watch this Sunday’s South Point 400 NASCAR playoff race.
 Democrats are lining up to oppose putting a woman on the U.S. Supreme Court. If the roles were reversed, you’d be hearing cries of sexism.
 From Israel to masks to wildfires, the last week shredded whatever credibility elitists had left.
 President Donald Trump’s Sunday rally was so “dangerous” that King Steve Sisolak now thinks it’s safe to reopen bars in Clark County.
 If President Donald Trump wins Nevada, he should send a big thank you card to King Steve Sisolak.
 There is far more evidence that Black Lives Matter protesters are systemically violent than there is that police are systemically racist.
 Powered by WordPress.com VIP
 https://www.reviewjournal.com/opinion/opinion-columns/victor-joecks/victor-joecks-king-sisolak-said-his-coronavirus-decisions-are-guided-by-medical-professionals-local-health-officials-say-he-doesnt-consult-with-them-2141194/