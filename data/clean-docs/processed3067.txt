Why are deaths from coronavirus so high in Italy? | Live Science
 By
 Rachael Rettner - Senior Writer
 27 March 2020
 Italy now has the highest number of deaths from COVID-19 in the world.
 Updated on March 26 with new information about COVID-19 in Italy. It was originally published on March 10.
 Deaths from the new coronavirus in Italy continue to soar, with the country reporting 919 deaths in a single day on Friday (March 27) — the biggest single-day death toll reported in any country since the start of the outbreak. But why are deaths in Italy  so high?
 Italy now has the highest number of deaths in the world from COVID-19, the disease caused by the new coronavirus. As of Friday (March 27), the country had reported more than 9,100 deaths, according to worldometer, a website tracking COVID-19 cases. And the country's fatality rate from COVID-19 — at 10% — is much higher than the global average of 3.4%, according to the World Health Organization. 
 One factor affecting the country's death rate may be the age of its population — Italy has the oldest population in Europe, with about 23% of residents 65 or older, according to The New York Times. The median age in the country is 47.3, compared with 38.3 in the United States, the Times reported. Many of Italy's deaths have been among people in their 80s, and 90s, a population known to be more susceptible to severe complications from COVID-19, according to The Local.
 What's more, older adults appear to make up a greater proportion of cases in Italy, with about 37% of cases ages 70 and older, compared with 12% of cases in China, according to a paper on the issue of deaths in Italy, published March 23 in the journal JAMA.
 Related: Live updates on COVID-19
 —What are the symptoms?
 —How deadly is the new coronavirus?
 —Is there a cure for COVID-19?
 —How does it compare with seasonal flu?
 —How does the coronavirus spread?
 —Can people spread the coronavirus after they recover?
 The overall mortality rate is always going to depend on the demographics of a population, said Aubree Gordon, an associate professor of epidemiology at the University of Michigan. In this case, the reported mortality rate is not "age standardized," which is a way to adjust for the underlying demographics of a population, she said.
 Given Italy's older population, "you would expect their mortality rate to be higher on average, all else being held equal," compared with a country with a younger population, Gordon told Live Science.
 In addition, as people age, the chances of developing at least one condition that weakens their immune system — such as cancer or diabetes — increases, said Krys Johnson, an epidemiologist at the Temple University College of Public Health. Such conditions also make people more susceptible to severe illness from coronavirus, she said.
 Another issue may be the number of people in a given area who require medical care — having a lot of severely ill people in a single region could potentially overwhelm the medical system, Gordon said. She noted that this was likely the case in Wuhan, China, where the coronavirus outbreak began and which saw the majority of COVID-19 cases in China. A recent report from WHO found that the fatality rate was 5.8% in Wuhan, compared with 0.7% in the rest of the country, Live Science previously reported.
 Finally, the country may not be catching many of the mild cases of COVID-19. Often, as testing expands within a community, more mild cases are found, which lowers the overall death rate, Gordon said. This was the case in South Korea, which had tested more than 295,000 people as of March 18 and had a death rate of about 1%, according to Business Insider.
 "We probably don't know how many people have actually become infected," Johnson said. People with more mild symptoms, or those who are younger, may not be going to get tested, she said. 
 Indeed, although Italy initially conducted extensive testing of both symptomatic and asymptomatic contacts of people with COVID-19, the Italian Ministry of Health issued more stringent testing policies on Feb. 25, according to the JAMA paper. The policy prioritized testing for people with severe symptoms and limited testing for asymptomatic people or those with mild symptoms. This could result in an increase in the fatality rate because patients with more mild symptoms are not getting tested, the paper said. 
 Italy has conducted a substantial number of tests — more than 134,000 as of March 17,  according to The New York Times. However there is likely "quite a sizeable outbreak" in the area, which would need even more testing to identify, Gordon said.
 Editor's note: This article was updated with additional information from epidemiologist Krys Johnson.
 Originally published on Live Science.
 OFFER: Save at least 53% with our latest magazine deal!
 With impressive cutaway illustrations that show how things function, and mindblowing photography of the world’s most inspiring spectacles, How It Works represents the pinnacle of engaging, factual fun for a mainstream audience keen to keep up with the latest tech and the most impressive phenomena on the planet and beyond. Written and presented in a style that makes even the most complex subjects interesting and easy to understand, How It Works is enjoyed by readers of all ages.
 View Deal
 Stay up to date on the coronavirus outbreak by signing up to our newsletter today.
 There was a problem. Please refresh the page and try again.
 Future US, Inc. 11 West 42nd Street, 15th Floor,
 New York,
 NY 10036. 
 https://www.livescience.com/why-italy-coronavirus-deaths-so-high.html
