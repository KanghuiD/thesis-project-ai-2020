Covid-19: Live updates from New Zealand and around the world on 21 April | RNZ News
 As Covid-19 spreads around the world, it can be daunting keeping up with the information. For RNZ, our responsibility is to give you verified, up to the minute, trustworthy information to help you make decisions about your lives and your health. We'll also be asking questions of officials and decision makers about how they're responding to the virus. Our aim is to keep you informed. 
 Read back over the day's events for everything related to Covid-19 in New Zealand today.
 The Medical Association, which represents doctors, said the government promised $22million to help GPs with the extra costs caused by the pandemic.
 It said they had received half of that, and were told they would no longer be getting the rest.
 Director-General of Health Ashley Bloomfield said the funding decision was part of an "ongoing conversation" between GPs and health authorities.
 Read back over RNZ's live blog for the day's Covid-19 news:
 National MPs will pursue the case on the Epidemic Response Committee this morning, putting questions to the Ministers for Finance, Small Business, and Employment.
 The Opposition has taken to pouring doubt on the Health Ministry's assertion it can trace 5000 people a day who have had contact with a Covid-19 carrier. Audio
 New Zealanders largely welcomed the government's decision to move to level 3 next week, but there has been some criticism of extending level 4 until then. Video
 At 11.59pm on Monday April 27 New Zealand will move to alert level 3 - five days and a holiday weekend longer than first anticipated.
 But Prime Minister Jacinda Ardern says our "team of five million"… Video, Audio
 The national alert level 4 response to the Covid-19 coronavirus began at 11.59pm Wednesday, 25 March and is due to end at 11.59pm on Monday, 27 April.
 for ad-free news and current affairs
 New Zealand RSS
 Follow RNZ News
 https://www.rnz.co.nz/news/national/414708/covid-19-live-updates-from-new-zealand-and-around-the-world-on-21-april
