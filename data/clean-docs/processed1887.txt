Opinion Politics and Policy - Bloomberg
 The government is doubling down on many lockdown measures, even as it reopens parts of the economy. Citizens haven’t been told what the plan is.
 Beijing is after political influence, and its coronavirus aid will count for much.
 National workplace safety rules would protect employees from coronavirus and employers from lawsuits.
 The purpose of the federal stimulus isn’t to reward the deserving. It’s to prevent an economic collapse.
 Political adviser Dominic Cummings's presence on a key science advisory committee isn't what's scary. A lack of transparency is.
 The president is more interested in wielding a cudgel than preserving a vital service.
 Germany has managed the coronavirus outbreak well, but there’s a danger that its citizens might be too relaxed about the country’s reopening.
 The U.S., like all countries, has made its share of mistakes, but that doesn’t make it a “failed state.” 
 For all Beijing’s claims to support Tehran, it is spooked by American sanctions.
 If oil producers don’t cut supply, negative prices will come back to force them.
 Board nominees for Italian state-owned enterprises follow a depressingly familiar pattern.
 The continent has some unique strengths in the pandemic, but food insecurity is a special vulnerability.
 With schools closed and health systems overwhelmed, the pandemic may impose high costs on vulnerable kids.
 China’s “wolf warriors” are just responding to pressures that envoys everywhere are feeling in the age of social media. 
 The chaos party is planning for a chaos election.
 States should have to justify which businesses are nonessential.
 The angry resignation of Justice Minister Sergio Moro promises to mire the country deeper in political crisis and economic misery.
 A Q&A with Lawrence Wright, whose new novel saw a pandemic coming.
 Teleconferencing and other remote technologies are here to stay, but security hasn’t kept pace with ubiquity.
 Many U.S. rivals may not be able to take advantage of America’s inattention. 
 Forbearance should be used by borrowers in trouble, not by those who can pay. 
 Maybe somebody should warn him about the U.S. Oil Fund ETF.
 New Zealand demonstrates what a truly effective coronavirus lockdown looks like.
 On even basic questions about the pandemic response, Donald Trump still seems lost. 
 Britain's virtual Parliament shows how to hold government accountable during a crisis.
 Antibody checks will provide a handy picture of the Covid-19 outbreak, but their usefulness will be limited in ending the lockdowns.
 President Erdogan’s measures to contain the pandemic and its economic impact fail to reassure a jittery public. 
 Mutual anger over Covid-19 will make co-existence difficult. The alternative is worse.
 A grand American tradition of putting personalities ahead of power politics seems to matter less and less. 
 A top antitrust lawmaker wants a temporary ban on mergers during the pandemic. Right instinct, wrong answer.
 Any plan for reopening is going to have to resolve some sticky liability issues.
 Get over it, Mitch McConnell. Our government has to manage the risk of a pandemic, and it needs to get better at it.
 There are other ways to raise revenue for coronavirus relief.
 At times, the president seems to be opposing his administration’s own policies. Why?
 https://www.bloomberg.com/opinion-politics-and-policy
