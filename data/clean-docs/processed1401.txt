Opinion - The latest news from TRT World
 In countries like India and Israel Muslims are being scapegoated for the spread of Covid-19.
 Humour helps us deal with anxiety and the current pandemic is a testament to that notion.
 The devil is in the details, but EU leaders this week will wrangle a deal which might be a starter’s pistol for Brussels to fall on its sword.
 The US president has targeted the global organisation for its perceived bias towards China.
 A Turkish citizen in London is dealing with these hard times with grace, and she's thankful for the power of technology.
 Wealthy nations will have less food security as developing nations cut back on food exports.
 Emerging from political deception and disenchantment in 20th-century India, the Islamic organisation, which has been traditionally apolitical, grapples with an intense political storm engineered by the far-right.
 The time to act in Africa is now, before the pandemic peaks on the continent.
 Several leaders have seized the moment to further entrench authoritarianism under the cover of Covid-19.
 Sport can play a large role in the recovery after the coronavirus pandemic, and has proven its worth in the past.
 The only way to counter the diminished writ of Somalia's central authority is to strengthen community action.
 The US healthcare system is clearly broken and the pandemic shows us why.
 Racist attacks and hate speech have spiked proving that right-wing extremists are exploiting the global crisis.
 The prospect of Covid-19 hitting Yemen is terrifying considering the already ongoing humanitarian catastrophes in the country.
 Reporting in conflict zones was no preparation for the calamity that has hit Italy in the form of a pandemic.
 https://www.trtworld.com/opinion/the-coronavirus-reveals-another-virus-white-privilege-35007
