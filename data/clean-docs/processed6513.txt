Coronavirus in the Netherlands: all you need to know (updated daily) – DutchReview
 Up-to-date information on coronavirus in the Netherlands
 The Netherlands reported its first confirmed case of coronavirus on February 27, 2020. Since then, the numbers have rapidly increased.
 THE LATEST: Coronavirus update: Netherlands reports 145 new COVID-19 deaths
 (+386 in last 24-hours)
 (+145 in last 24-hours)
 (+76 in last 24-hours)
 The numbers are expected to continue to climb over the coming weeks, peaking in early May.
 Note: RIVM warns that the number can be misinterpreted because not all of these occurred in the past 24-hours. “On Tuesdays, the number of notifications has consistently been the highest of the week,” they explained. “This can be explained by the fact that many of the deaths and admissions are processed and reported on Friday, Saturday and Sunday on Monday.”
 The majority of the Dutch cases in the Netherlands are in the province of Noord-Brabant and Utrecht.
 The first Dutch case of coronavirus was in Tilburg, followed by a case in Diemen and in Delft.
 The current measures implemented in the Netherlands include:
 A recent survey (April 3) released by the I&O Research bureau in collaboration with the University of Twente gave us some good news. 99% of Dutchies are keeping to the 1.5 meter distance rule, 97% are washing their hands more often, and 93% are trying to stay at home as much as possible.
 During Easter weekend (April 10-13), the police issued 1800 fines for people who were violating the coronavirus measures in place. That would amount to approximately £700,000. People are also scared of getting fines with more police surveillance, perhaps that helps too.
 According to the Outbreak Management Team (OMT), intensive care units will go back down to their regular capacity by 1 May. That means, in practice, that there will be 1200 total beds, of which 700 will be occupied by coronavirus patients.
 Currently, the Netherlands has managed to double its total ICU capacity, with 2400 beds available nationally, of which 1900 are available to coronavirus patients. Intensive care units all across the country were expanded to deal with the crisis.
 As of yesterday, 1279 of those 1900 beds were occupied, a number that has been decreasing by 20-30 people per day for the last three days.
 — TAG (@itsTAGofficial) April 15, 2020
 The government has said that in order for normal life to begin to resume once again, three criteria need to be met. There needs to be less pressure on the healthcare system, there needs to be sufficient testing capacity, and there needs to be a way to track infections.
 European leaders have been meeting virtually to discuss how they plan to finance the coronavirus pandemic that has taken Europe (and other parts of the world) by storm. Of their list of topics discussed is the European Stability Mechanism (EMS), an emergency fund comprising of over €400 billion in a conference call.
 Countries such as Italy and Spain, which have been severely affected by the virus, wanted to use the money to prevent financial shocks from occurring. However, the Netherlands and Germany want to take a more cautious approach and explore other alternatives.
 Now, the Netherlands has proposed a European emergency fund of 10-20 billion euros, which Minister Hoekstra has suggested the Netherlands will donate one billion euros to.
 There will be a cap on the number of customers allowed to enter supermarkets. This is in order to ensure a minimum distance of 1.5 meters between customers.
 Fewer shopping baskets will be available, and once the maximum number of customers is reached, those who want to enter will need to wait so that another customer leaves. These policies are implemented to protect both the employees and the customers.
 May brings with it two events of great historical significance to the Netherlands. May 4, Remembrance Day, is an opportunity to remember all Dutch people who have lost their lives in conflicts around the world. Meanwhile, May 5, Liberation Day, is dedicated to the liberation of the Netherlands during WW2 from the Nazis.
 Unfortunately, both of these events have been cancelled, given the new policy banning all events until September 1 (announced on April 21). This is not to say that the Dutch will not find a way to celebrate these events, be it from their windows and balconies.
 For better or for worse, the centralised school exams for this year’s school year have been cancelled. The exams were due to start on the 7th of May. Instead, now pupils will receive their final marks based on their previous marks in school exams as not to delay their process of continuing their education. This new policy was announced by the Minister of Education this morning, reports NOS.
 New measures have been implemented on the 31st of March, but have been updated the 21st of April. These measures include closing down all restaurants, bars, cafes, coffeeshops, clubs and other venues until the 20th of May. Schools are closed for the time being but will begin to open up after May 11.
 The Netherlands has a pandemic protocol in case any virus starts spreading uncontrollably.
 As per the pandemic protocol, special quarantine areas must be developed to take care of sick patients, there will be regulation in regards to how patients are treated, criteria for hospital admissions, and when the vaccine is developed, there will be rules on who gets it first.
 While the WHO has declared coronavirus to be a pandemic, little has been said about how much of the above has been implemented.
 Hospitals will also develop special quarantine sections for the care of patients.
 Virologists suggest that you should be extra vigilant about taking public transport.
 If you really need to go somewhere, then take public transport. Just make sure you stay at a safe distance from other people, that you do not touch your face after touching objects outside (such as handles in the tram), and that you wash your hands with soap as soon as you get back home.
 If you’re in a risk group, don’t take public transport. And of course, if you’re sick just stay at home.
 Remember: cold or flu symptoms do not mean coronavirus. RIVM says if you have a runny nose or a cough to stay home and self-isolate. However, if you have a fever over 38 degrees and persistent chest pain, follow the steps below.
 Firstly, do not go to your doctor’s office. Instead, call the office and speak with a doctor or nurse. They’re trained to detect the symptoms of coronavirus and will make a decision as to whether you need further testing. There’s no need to panic, but you do need to be proactive.
 In the meantime, avoid going outside unless absolutely necessary. If you have to go outside wear a face mask so you don’t risk infecting other people. If you can have groceries and other essentials delivered or dropped off by friends or family do this.
 While the coronavirus is something to be taken seriously, there is also no need to overreact, panic excessively or compulsively buy toilet paper.
 Odds are that even if you do get the coronavirus, you will not die, nor will you have a severe form of it. So unless you are very old, have a bad immune system or already have dangerous pre-existing conditions, you will probably not have a terrible case.
 In fact, statistically, the majority of people don’t need hospitalisation and fully recover. But, we’ve all experienced bad cases of the anxieties before, so we prepared a nice little coronavirus anti-anxiety fact list to ease your nerves.
 It’s unfortunate that at times our society is tested we break apart instead of pulling together. A sad side-effect of the coronavirus is the emergence of racism towards Chinese people in the Netherlands.
 About two weeks ago, there’s been a case of a woman being attacked in her dormitory by some Dutch people singing the racist coronavirus song. Other people have also been harassed for their Chinese identity.
 We strongly reject any kind of racism of any form. If you notice anyone harassing Chinese people report the case to the police.
 The official source for information on coronavirus in the Netherlands is RIVM (Netherlands National Institute for Public Health and the Environment). The most up to date information can be found on the RIVM website. This article is also constantly updated with the most current information.
 Feature Image: Canva
 Fantastic idea!!!  I will keep sharing this.
 According to several respected sites (https://www.worldometers.info/coronavirus/country/netherlands/, https://www.arcgis.com/apps/opsdashboard/index.html?fbclid=IwAR3exv1Ep6vsgSOqwNu7Y1DVM4aT0SXL02ZLje0eIModx5LSTockb7xpDyM#/bda7594740fd40299423467b48e9ecf6) which get their information directly from the RIVM (National Institute for Public Health
 and the Environment, Ministry of Health, Welfare and Sport), the rate of recovery in the Netherlands is far lower than any other country. While I have heard that this is due to the Dutch reporting methods, it still does not explain this: Cases which had an outcome: 437; Recovered: 3; Deaths: 434. Are we really experiencing a 99% death rate for this virus?
 ‘Outcome’ presumably refers to either death or recovery. In other words, there are many people infected who have not yet ‘made it out’ either way. But the great majority will recover. That seems to be the difference in the reporting method.
 I follow the same site and I’ve been wondering something similar since almost 3 weeks ago and that is the difference in cases vs mortality in NL compared to Belgium and Germany. I also asked in Quora NL about it (https://nl.quora.com/Hoe-komt-dat-de-sterftekans-in-Nederland-percentage-wise-veel-hoger-ligt-dan-buurlanden-Duitsland-en-Belgi%C3%AB-Heeft-dat-mee-te-maken-met-de-kwaliteit-van-de-gezondheidszorg-in-NL-of-er-zijn-andere-redenen).
 Nowadays, if you want to talk about it percentage-wise, it comes down to amount of testing and ICU beds (Germany has 29.2 per 100.000 inhabitants and the Netherlands only 6.4) and GE still tests as much as possible and NL, already for almost 2 weeks, mainly the very sick and very old. Also, to my knowledge -I’ve lived in this country for 31 years), the healthcare system here is subpar to Germany and Belgium (I’ve chosen from the beginning to compare NL to its neighbours since there should be more similarities than discrepancies, you would think). Two times I have had to go back to my own country (Chile) for testing and treatment since my own experience here is that they are behind not only in knowledge about medicine but also equipment.
 And what I read recently about doctors thinking about asking older corona patients if they want care in a hospital or at home…that’s basically asking them if they rather stay home and just die since they are old and ill. They do stretch the meaning of palliative care in this country, I saw this with my father in law couple years ago. They were very eager to put him out of misery instead of trying treatment, he was not even 80.
 So…now trying to answer your questions. They don’t really track the people that recover, but that’s sloppy if people have been treated in a hospital (don’t they keep track of the people discharged?). The ratio cases/deaths is high due to minimal testing and testing only critical cases (leaving aside hospital staff). But on the other hand GE has almost 84 million people, NL little over 17 million. GE has 433 deaths at this moment, NL has 639….think about that.
 I do not think keeping takeaway shops open is such a good idea, netherlands is one of the countries with quite a few deaths, even though its not as bad as italy or spain, it can get out of control really quickly.
 In fact during the next weeks you´ll see the big expansion of Corono Virus in the Netherlands. It´s a question of time. Soon I will eat my shit words about European Southern countries.
 I must be the unluckiest person in the Netherlands because in my neighbourhood not even 10% of Dutch people keep the official 1.5 m safe distance. The aforementioned 99% sounds like a joke (or a dream, it depends on the point of view). Dutch people are unaware of the situation. I keep seeing groups gathering, birthday celebrations with more than 10 children together and the corresponding number of parents… I guess this is a government’s trick to clean up the economy by eliminating the elderly.
 We have to keep business going with a mitigated risk as well as isolating those who are subject to heightened mortality! The problem with the UK and America is that they just don’t have the Medical resources to cope with the onslaught. The Netherlands is nearing capacity, but thing are no were near exponentially increasing and it’s become more linear in the last few days.
 So we don’t need to be so critical of the Netherlands, they are rule breaker (Just don’t be rule breaker i there society 😉 ) and they are well educated in statistics and science!
 Recoveries are there, but they are not centrally registered and therefore not reported. Individual hospitals do however and they report them, mostly daily to personnel. Hotspot Amphia hospital in Breda reported at a certain day cumulative 34 corona deaths and 109 recovered. So that is better.
 As far as Germany goes, they choose for much wider testing, so had many more cases reported, but also many with few symptoms. As Germans are very good at registering they reported also the very many recoveries of light cases. As Ge now has many more more severe cases, their number of deaths has now overtaken that of the Netherlands. In time their numbers will increass more, reflecting the size of their bigger population.
 Last factor, coronadeaths where corona was not tested do not count as corona deaths. Statistical analysis of deaths in march till now suggest a doubling of reported corona deaths is likely.
 What sort of lockdown are we in!!
 Live in Enschede where you can still go clothes shopping, shoe shopping, even underwear shopping and plenty more
 They pass you a wipe from a cheap pack
 Where the shops are full of people
 Even Germans. (Lockdown)
 They even set up the small market stall yesterday fml
 Dutch are not on no lockdown…
 one thing thou. 3 people or more in the car you get slapped with a 400 euro  fine
 Says it all about what the Dutch government is doing ( all about the money)
 I live in Koudekerk aan den Rijn (near Leiden). I phoned the doctor 3 weeks and 2 days ago. The bottom line was: “No fever, no dangerously short breath… no appointment, no test”.
 I went through hell since then. Strange that I never had a fever, but the list of symptoms was/is as follows: Chills (like a fever, but not a fever), Sore throat comes and goes, Dry cough (but not a constant cough), Severe muscle/joint pains (and what felt for like deep abdomen and lung pain for almost 3 weeks), Dizziness, and of course very weak.
 Never more than 2 symptoms at one time; most all symptoms have passed except for the severe muscle/joint pain the past 4 days. Barely a cough now. Improvement every day for the past 3 days. I am told it is possible once this pain  goes away I may be in the clear.
 My point is not to complain about my experience — but to say this:  If I was not allowed to see the doctor, and not allowed to be tested… then the statistics must be way off the mark! There must be literally THOUSANDS OF PEOPLE UNACCOUNTED FOR AND SIMPLY STAYED HOME AND SUFFERED THROUGH IT.  Hopefully surviving it. 
 But not being allowed the opportunity to be tested not only seems like a senseless health risk… but grossly unfair to those of us isolated from our families, especially the children. How many were not positive for Covid-19 yet were/are separated from their loved ones??
 Article keeps using some statistics but it really doesn’t seem to match reality. There are people drinking wine together in groups on canals and parties held between neighbours. When I once saw a police van going past a group of 8 students sitting together at one big table I thought they were all going to be fined. Instead, the police didn’t even slow down.
 This situation is just an opportunity for the Dutch government to kill off those whose financial input is already done. My friend’s 70y.o. father who was in hospital and had signed a form asking to be resuscitated, the next day received a call from his own GP informing the family that with all in mind, he overruled that signature..
 This is not a country of compassion but paracetamol induced incredible indifference.
 I couldn’t agree more. I have to add, that even allowing fewer people in the supermarkets is not the solution. The most dangerous people are the supermarket employees. Yes, they wear a nice t-shirt that says “keep a 1.5 m distance”, but they are the first that walk close to customers all the time. In conclusion, Dutch people are terrible at following rules or they just think that 30-40 cm is 1.5 m. The only solution is a better upbringing, but that takes years.
 Well said 🙌🏻🙌🏻
 Why have the Dutch stopped reporting their “Recovered” data?
 Probably the most important metric.
 I seem to think that this was was reported daily until recently.
 Why the change?
 This country has a much higher death rate than most other countries, even those with a lesser developed health system. Why is it so bad??
  By using this form you agree with the storage and handling of your data by this website. *
 E-mail
 Firstname
 Lastname
 Event Type 2
 All
 Amsterdam
 Eindhoven
 Keukenhof
 Leiden
 Rotterdam
 Scheveningen
 The Hague
 Tilburg
 Utrecht
 We're constantly hunting for the latest, greatest, and most Dutch spots for our readers. Want your business to reach an unrivalled expat and international audience?
 We love you already
 https://dutchreview.com/news/coronavirus-netherlands/
