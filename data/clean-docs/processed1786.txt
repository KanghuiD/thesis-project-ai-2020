Coronavirus could spark legal action by the families of those who have died - ABC News (Australian Broadcasting Corporation)
       Updated 
     
       April 06, 2020 08:44:30
 The health and economic devastation caused by the coronavirus is unmistakable and the legal fallout from the contagion could also be severe — resulting in the largest negligence class action in Australia's history.
 The pattern of infections from other countries suggests that tens of thousands of Australians could be infected with COVID-19.
 And while the science on beating the coronavirus has not changed (break the chain of infection), the continually changing Government position could be legally problematic.
 The mortality rate of the illness worldwide is on average over 4 per cent, although in Australia it is currently less than 1 per cent and in Spain more than 9 per cent. 
 Who bears the responsibility for these deaths?
 This question will become more urgent once statistical and hypothetical deaths translate into identifiable people — all with relatives, aggrieved that the lives of their loved ones have been cut short.
 In Austria, a legal precedent is already being tested. About 2,500 skiers have so far joined a class action lawsuit over the way authorities managed a coronavirus outbreak at a resort in the province of Tyrol. 
 The lawsuit alleges authorities acted with "negligent endangerment" for failing to protect skiers from the outbreak. Hundreds of Europe's first cases of coronavirus have been traced to the resort at Ischgl.
 The legal fallout from the pandemic is likely to be immense. 
 Apart from Government organisations — which are not immune from legal liability — all individuals and institutions have a duty of care to take reasonable steps to not transmit the coronavirus.
 This could have considerable implications for institutions such as medical facilities if they do not provide proper protective equipment for staff, cruise ship operators and even people who are infected and wantonly ignore social-distancing protocols.
 A class action has already been proposed against the operators of the Ruby Princess, which is the source of seven Australian deaths from coronavirus.
 A duty of care, whether it be by a business, individual or government, will have been breached where there is a failure to take reasonable steps to avoid harm that was reasonably foreseeable.
 The nature of the precautions that are necessary is commensurate with the severity of the risk and governments are liable for their omissions as well as their actions.
 Because the coronavirus outbreak is a freak occurrence there is no well-defined protocol for best-case practice.
 However, there is a clear legal methodology for responding to risk and the key determinant is the severity of the risk if it eventuates.
 Shortly after the first confirmed coronavirus case hit Australian shores on January 25, there was clear scientific information that established the nature of infection. This included its severity, its pattern of spread and means of control.
 In particular, it was known that the only mechanism to reduce the spread of the virus was to break the chain of infection. This could only be achieved by considerably limiting human contact.
 Against this background, the Australian approach of business as usual (plus hand washing) until a critical number of infections occurred — and then going into incremental lock down — is likely to be carefully scrutinised by future courts.
 Given that it was likely that business as usual would lead to a growth in infections, the most cautious approach would have been to immediately put in place clear interventions to stop the chain of infection.
 Prevention is better than cure. 
 This principle applies much more strongly in situations such as the coronavirus as there are no conceivable counterarguments to the premise that "prevention is better than non-cure".
 This is the philosophy adopted by New Zealand which went into near total lockdown a week ago when it had fewer than 500 infections.
 The main potential legal problem associated with the Australian approach stems from shifting behavioural restrictions imposed on Australians.
 In the space of just over three weeks the government has gone from stating that mass sporting events are acceptable, to limiting gatherings to 500 people, then 100, then 10 and now two (plus a 1.5 metre zone of separation).
 This does not have the hallmarks of a methodology, science-based approach to the crisis.
 The reality is that the science on how to control the virus is unchangeable — break the chain of causation.
 Instead the approach seems reactionary: an attempt to catch up to a contagion that was let to run freely (and perhaps too freely) at the outset.
 I suspect that the reason for this is that the government attempted to balance economic considerations against health objectives, and hence permitted trade and commerce to flow for as long as possible.
 It is clear that future courts will not seriously entertain an economic justification for a failure to do everything reasonably possible to curtail the greatest public health threat in many decades.
 A stricter risk mitigation approach (shut down travel six weeks ago — as some states have done recently and forcibly quarantine every confirmed coronavirus case) would almost certainly see Australia with fewer cases.
 The approach by Australian governments in dealing with the virus is in stark contrast to not only that in New Zealand but also Singapore, which is also a nation island but one which is far more densely populated than Australia. 
 Singapore had its first reported case of coronavirus two days before Australia. It now has less than a fifth of the number cases as we do.
 Rather than encouraging the community to go about business as usual once the virus was detected, the Singaporean government implemented concrete measures to reduce the spread the virus within three days of its detection in that country.
 Extensive efforts were made to identify, isolate and monitor all positive cases and people with whom they had contact. Mass gatherings were quickly halted. Schools remain open but students have daily temperature screenings.
 One aspect of the government's approach that is likely to be scrutinised very carefully by future courts is the lack of enforcement of self-isolation measures.
 While people diagnosed with coronavirus have been told to self-isolate since the first cases were diagnosed, the concept of self-isolation was initially not even defined and it is only recently that it has been enforced.
 Prior to that, people with coronavirus were permitted to freely go about their daily activities.
 Relying on the goodwill of citizens to do the right thing as part of a solution to a crisis is always a losing strategy — this is underlined by the behaviour of many supermarket shoppers in recent weeks.
 Whether the financial pain of this crisis will be compounded will almost certainly be determined by future courts. 
 To this end, hindsight will inevitably play an important role in evaluating the appropriateness of the Government's response.
 The fact that the rate of increase in new infections has dropped significantly and finally governments are coercively enforcing self-isolation will be powerful considerations in support of the current approach.
 The question that future courts will need to determine is whether it was too little too late.
 Professor Mirko Bagaric is the Dean of the Swinburne Law School and author of Torts: Compensation for Harm.
 Topics:
 law-crime-and-justice,
 	covid-19,
 	epidemics-and-pandemics,
 	courts-and-trials,
 	austria,
 	australia
     First posted 
       April 06, 2020 05:00:59
 If you have inside knowledge of a topic in the news, contact the ABC.
 ABC teams share the story behind the story and insights into the making of digital, TV and radio content.
 Read about our editorial guiding principles and the standards ABC journalists and content makers follow.
 Learn more
 While the rest of the world is distracted by the coronavirus outbreak, a dangerous new phase has begun in a region already struggling with unrest, corruption and inequality. 
         By Anna Kelsey-Sugg and Bec Zajac for Life Matters
 Alone but for coyotes and rattlesnakes, Claire knew she might die after a serious fall at Joshua Tree National Park. She resolved to change her life if she made it out alive.
         By ABC editorial director Craig McMurtrie
 Some of the language thrown around in the aftermath of the High Court ruling on George Pell seems to ignore the first principles of journalism and the facts, writes ABC editorial director Craig McMurtrie.
         By Phoebe Hosier
 Meet the Tasmanian circus performer who's determined to teach kids and adults how flying through the air and hanging upside down can reframe how we think about bodies, boundaries and consent. 
 AEST = Australian Eastern Standard Time which is 10 hours ahead of GMT (Greenwich Mean Time)
 https://www.abc.net.au/news/2020-04-06/coronavirus-law-class-action-negligence/12113220
