Coronavirus: Quiz banking facilities extended to July - Retail Gazette
 Quiz has confirmed that its existing banking facilities have been extended until the end of July as the coronavirus pandemic continues.
 The facilities, with HSBC, had been due to expire on April 23.
 The fast fashion retailer currently has an overdraft facility of £2 million and a working facility of £1.5 million – against which a total of £3.5 million can be drawn.
 Quiz had a net cash of £6 million as of April 20.
 ”We are taking the actions needed to preserve cash and manage liquidity throughout this unprecedented and challenging period in order to ensure that Quiz remains well positioned to deliver its strategic plans over the longer term,” chief executive Tarak Ramzan said.
 “We are grateful for the constructive dialogue with and ongoing support of our stakeholders, including colleagues, suppliers and partners as well as our bank, HSBC.”
 Quiz said it expects a further update on the group’s long-term banking facilities towards the end of June.
 The retailer recently reopened its website after encouraging social distancing and safety measures for warehouse stock pickers.
 The group has warned that its full-year profits will be affected by the Covid-19 crisis due to a “substantial” drop in online traffic and the forced closure of its stores on March 22.
   
  Editorial: 0208 222 0503  Commercial: 07401 241 979
 UK: Four Cavendish Square, Marylebone, London, W1G 0PG
 https://www.retailgazette.co.uk/blog/2020/04/coronavirus-quiz-banking-facilities-extended-to-july/
