EUobserver
 Sunday
 26th Apr 2020
 The EU is reorienting funds to boost the Libyan coast guard - amid fears that the escalating violence in the north African country plus the coronavirus will force a large exodus of people to take boats towards Europe.
 In tackling the pandemic, we must avoid Europe sleepwalking into a permanent expanded surveillance state.
 While much of the transport sector is pushing for unconditional state aid and to postpone climate policy action, experts believe that this crisis could help to transform the EU's transportation system, making it more resilient, sustainable and flexible.
 EU leaders agreed on the need for a fund to support the recovery of Europe's economy from the coronavirus pandemic, but disagreed on details. The commission will come with proposals tied to the new long-term EU budget.
 With his opinion article Kenneth Roth joins what are now dozens of critics to criticise the Orbán government and insult the Hungarian people with the charge that Hungary's extraordinary measures to fight Covid-19 amount to "dictatorship."
 Universities in the EU ought to appoint civilian spy-catchers to stop China and others stealing secrets, the European Commission has suggested.
 The EU commission is double-checking emergency measures in every member state, as fundamental rights have been temporarily abrogated. But Hungary and Poland are problematic, yet no actions are planned.
 While women are in the frontline on fighting the pandemic, they are also exposed more to the economic crisis that will follow. The pay gap could also grow. More security for flexible jobs, and investment in care work, could help.
 Numbers appear precise, but can also be unintentionally misleading when it comes to the pandemic, as experts warned that worldwide data is difficult to compare.
 Several Asian countries have seen the coronavirus surge back after easing restrictions - despite their initial success in restraining the outbreak. As a result, EU member states maybe starting to loosen restrictions without having a 'concrete' example to learn from.
 EU leaders hold a videoconference Thursday to bridge divisions over financing Europe's recovery from the corona crisis, in talks interlinked with the bloc's long-term budget.
 Now (as before) there are lessons to learn about how a number of Eastern countries are tackling this coronavirus crisis. We have to drop our bipolar world view of ​​the 'free West' against the 'authoritarian East'.
 A few weeks ago, the European Union underwent a fundamental change: it ceased being a bloc of exclusively democratic states.  Even worse - leaders across Europe barely flinched.
 Russia and China are still bombarding the EU with coronavirus lies, including "malicious" Russian "fake cures" which put lives at risk.
 Up to €400bn will be lost by the tourism and travel sector due the pandemic, French internal market commissioner Thierry Breton told MEPs on Tuesday.
 EU candidate countries such as Albania, Montenegro and Serbia have relatively poor press freedoms - but still fare much better than Bulgaria, an EU state whose ranking in the World Press Freedom index has plummeted.
 By March, the emergency had forced every government in Europe into an impossible choice - letting many people die and health systems collapse, or ground much of public life and inflict massive harm on their economic lives.
 Covid-19 is no excuse to allow authoritarian minds more leeway. While Polish government uses the pandemic to conduct unfair elections, EU countries must see it as an existential political threat alongside the health and economic crisis.
 The 1918 flu pandemic "was just another thing to put up with" for people numbed by World War One - but there were also parallels with today, a British academic says.
 Italians were hit hardest when the coronavirus landed in Europe but the European Union was slow to help the country. The president of the European Commission Ursula von der Leyen has apologised — twice.
 The US was making matters worse in the global fight against coronavirus, the EU's foreign affairs chief Josep Borrell said, amid geopolitical competition to exploit the crisis.
 Madrid added its voice to the debate on how to come up with big enough recovery plan that would help protect the euro and stabilise economies worse-hot by the corona-virus. EU leaders will flesh out ideas on Thursday.
 The European Commission vice-president Vera Jourova recognised that the EU's pharmaceutical sector had a 'morbid dependency' on third-country suppliers. But some experts from the pharma field have warned export bans undermine global supply chains.
 On 4 March, the European Commission's legal service handed president Ursula von der Leyen an analysis of the Greek government's controversial decision to temporarily freeze asylum applications. The commission will not now release the document.
 After more than 22,000 coronavirus-related deaths and over a month of lockdown, Italy's health emergency is taking its toll from the social point of view too. Stress is skyrocketing.
 'As a single mother, I am now caring for my severely disabled son alone, 24/7, without any assistance, without rest, with more demand for shopping, cooking, feeding and intensive care.'
 Interior ministers from France, Germany, Italy, and Spain sent a joint-letter to the European Commission outlining ideas ahead of the upcoming overhaul of the EU-wide asylum system.
 As president of the Italian region of Emilia-Romagna and of the Council of European Municipalities and Regions, I have witnessed firsthand the efforts and sacrifices of our doctors, nurses, police officers, waste collectors, civil servants, volunteers and countless others.
 EU leaders will again try to hammer out a compromise on how to fund an economic recovery after the deepest crisis since the Second World War - but divisions are still running deep. MEPs meanwhile will quiz various commissioners, online.
 An average global temperature rise above 1.5 or 2°C creates risks society cannot handle. This dwarfs the fallout of Covid-19.
 Member states agreed on Thursday that Covid-19 mobile applications used for contact-tracing should not process location data of individuals, amid rising concerns over privacy.
 A Dutch far-right politician behind a 2016 referendum on Ukraine and the EU had fishy ties to Russia, according to new revelations.
 Even though some countries in Europe are considering to ease lockdowns and restrictive measures, the European branch of the World Health Organization warns that the number of infections and death for coronavirus keeps growing in the region.
 A coronavirus outbreak in Gaza would be a "nightmare", a UN official has warned, but the pandemic's economic cost alone could be too much to bear.
 Russia has defended its credibility on coronavirus data, after Belarus said its neighbouring country was "ablaze" with infections - and the EU accused Moscow of other "lies".
 https://euobserver.com/
