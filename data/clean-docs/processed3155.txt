Coronavirus cases jump to more than 7,000, as four countries report new infections outside of China.
 By
           
 
 Elliot Hannon
   China confirmed Thursday that a case of coronavirus reported in Tibet meant that the outbreak had now spread to every region of the country. Chinese health officials said there were now 7,711 confirmed cases, marking a significant 1,500-case jump from the previous day. There are 170 reported deaths, all in mainland China. The virus has now spread globally with roughly 100 cases reported across at least 19 countries, according to the CDC, including five confirmed cases in the U.S. across four states, California, Washington, Arizona, and Illinois.
   The World Health Organization announced it will meet again Thursday to reassess whether the outbreak now meets the threshold for a global health emergency. “In the last few days the progress of the virus, especially in some countries, especially human-to-human transmission, worries us,” WHO Director-General Tedros Adhanom Ghebreyesus said Wednesday. “Although the numbers outside China are still relatively small, they hold the potential for a much larger outbreak.”
   China has imposed aggressive travel restrictions around Wuhan, the epicenter of the illness, quarantining some 50 million people in Hubei Province in an effort to stop the virus’s spread. There have been no reported deaths outside mainland China as of yet, as countries race to evacuate and quarantine citizens, but a handful of countries—Taiwan, Germany, Vietnam, and Japan—have now reported cases of coronavirus in people who had not been to China at all. That’s an unwelcome sign that shows the virus could be set to spread among populations that have no connection with China.
   Roughly 200 Americans arrived in California Wednesday after being evacuated from Wuhan; the State Department is arranging further evacuation flights for American citizens as early as next week. By the New York Times’ count, other countries currently reporting coronavirus cases are Thailand with 14 cases of infection; Japan, 11; Hong Kong and Singapore, 10; Taiwan, eight; Australia, Malaysia, and Macau, each with seven; France, five; South Korea, Germany, and the United Arab Emirates, each with four; Canada, three; Vietnam, two; and India, the Philippines, Nepal, Cambodia, Sri Lanka, and Finland, each with one.
   Many countries are carrying out medical screening at airports for travelers arriving from China. Russia indicated it intended to close its border with China to curb the virus’s spread in the country. Australia has controversially said it will quarantine its evacuated citizens on Christmas Island, a small island 2,000 miles off the country’s coast that has an unsavory past as a detention center for refugees and migrants.
 Elliot Hannon is a Slate staff writer.
 You’ve run out of free articles for the month. Join Slate Plus to continue reading, and you’ll get unlimited access to all our work—and support Slate’s independent journalism. You can cancel anytime.
 Start Free Trial Now
  Already a member?
         Sign in here.
         Slate is published by The Slate Group, a Graham Holdings Company.
       
 Slate relies on advertising to support our journalism. If you value our work, please disable your ad blocker.
 By joining Slate Plus you support our work and get exclusive content. And you'll never see this message again.
 https://slate.com/news-and-politics/2020/01/coronavirus-cases-jump-wuhan-human-infections-outside-china.html