Coronavirus in UK live blog: latest as PM said to be 'cautious' about easing lockdown for fear of second infection wave | Wigan Today
 UK laboratory makes breakthrough in its Covid-19 antibody test development
 Wigan mum's campaign to buy face creams for medics' bruised faces
 We will be providing live updates until 6pm this evening.
 Last updated: Monday, 20 April, 2020, 17:44
 The director of the centre for evidence-based medicine at Oxford University  has said the Government lockdown is now likely to do more damage than coronavirus itself.
 Speaking on BBC Radio 4's Today programme, practising GP Professor Carl Heneghan said too much attention had been paid to prediction models that often proved to be "some way out" and that not enough testing had been carried out.
 "The key is no-one has really understood how many people actually have the infection," he said.
 Prof Heneghan said a better understanding of the extent of the virus could be found "really quickly",  with the random sampling of a thousand people in London who thought they had symptoms.
 "You could do that in the next couple of days and get a really key handle on that problem and we'd be able to then understand coming out of lockdown much quicker.
 "In fact, the damaging effect now of lockdown is going to outweigh the damaging effect of coronavirus."
 Professor of vaccinology at Oxford University Professor Sarah Gilbert has told BBC Breakfast that her team hopes to begin clinical trials of a coronavirus vaccine by "the end of this week."
 Prof Gilbert said that with the virus still circulating there was a lot of pressure to go as quickly as possible.
 "Unfortunately we are under a lot of pressure now," she said, "because we need to be testing the vaccine at a time when there is still virus circulating in the community and that means we need to go as quickly as possible.
 "We are actually hoping to start testing at the end of this week not next week."
 Prof Gilbert added the same vaccine technology had been used before and had performed well.
 "We think it is the best thing to use," she added, and said said her team had been developing the vaccine since January - it would normally take five years to get to the stage they are now at.
 Prof Gilbert said her team had been developing the vaccine since January 11 and it would normally take five years to get to the stage they are now at.
 She said that none of the normal safety steps had been missed out.
 BBC Radio 4's Today programme, Chris Hopson, chief executive of NHS Providers, has said some hospitals are now running low on personal protective equipment.
 "There is no doubt that at the moment, we have now got trusts who have definitely got shortages of gowns," he said.
 Hopson added that trust leaders are doing "absolutely everything they can" to try and address the problem, including washing fluid-repellent gowns at 60c, which appear to remain resistant when washed a maximum of three times.
 Others had staff in boiler suit-type outfits that also passed PPE guidelines, he said.
 "A great example would be that, last week, there was an order for 200,000 gowns due to come in from China," he told BBC Radio 4's Today programme, "but only 20,000 actually arrived."
 "We know that with other orders, when the boxes were opened up and it said on the outside 'Gowns', when you opened it up, they were actually masks.
 Culture Secretary Oliver Dowden has told told BBC One's Breakfast show he was hopeful a package of 84-tonnes of PPE would arrive from Turkey on Monday after being delayed.
 He said there had been "challenges" in acquiring the consignment, which includes 400,000 gowns.
 "We are very hopeful that later today that flight will take off and we will get those gowns," the Cabinet minister said.
 "I don't want to start making more and more promises but I understand that that flight will take off this afternoon and they will be delivered."
 Dowden also defended the Prime Minister after reports in the Sunday Times claimed he skipped a number of early emergency Government meetings on dealing with Covid-19.
 "That does not mean the Prime Minister is not engaged," he said. "The Prime Minister was being briefed on an hourly and daily basis, took a very close personal interest in this and clearly, as the crisis progressed, he then took over chairing.
 "That's a normal course of events."
 Professor Sarah Gilbert, professor of vaccinology at Oxford University, has been speaking of her and her team's work on finding a coronavirus vaccine this morning.
 She's said it's still not clear that once a vaccine is discovered, whether people will need a single dose or an annual one.
 "The first thing is to have a vaccine that works at all," she's said, "and then to start looking at how long the immunity lasts for.
 "We have been hearing about the antibody tests picking up people who have been infected and then also sometimes describing those as being immune, well we don't know how long that immunity lasts for either.
 "There's still a long way to go. I suspect that in younger adults, say under the age of 55, it won't need to be an annual vaccination, maybe it will need to be for older people."
 Culture Secretary Oliver Dowden has confirmed the Government is looking into holding a minute's silence for NHS workers who have lost their lives to coronavirus.
 "I think it is a very good idea," Dowden told the BBC. "We are looking into it.
 "As Culture Secretary, I have responsibility for ceremonials and things like minute-silences."
 Asked whether it could be an official Government-led event, he said: "Yes, I think it could be but we will make an announcement on that at an appropriate time."
 It follows a campaign by Labour leader Keir Starmer to recognise the sacrifice health workers have made during the pandemic.
 And some good new from Oliver Dowden now, who has said reports suggesting pubs could remain closed until Christmas are "pure speculation".
 "We're all desperate to end this lockdown," he said.
 "We need to do it in a sensible way because the worst thing we could possibly do is to prematurely ease the restrictions and then find a second peak."
 Speaking to BBC Breakfast, Jim Harra, chief executive of HM Revenue & Customs, has said he is "confident" the system running the government's coronavirus job retention scheme would work.
 "There is a limit to the capacity of the system," he said, "so if every employer tries to use it at 8 o'clock this morning some will be asked to queue or come back later, that doesn't mean the system has crashed, it simply means that it's full.
 "But employers can claim any time over the few days, between now and Wednesday, and we will have the money in their bank account by April 30."
 Mr Harra said the Coronavirus Job Retention scheme system had been tested at "up to 450,000 claims per hour".
 Culture Secretary Oliver Dowden has also confirmed this morning that he has been in talks with sporting organisations, including the Premier League, about their preparations for continuing with fixtures.
 He told BBC Radio 4's Today programme: "In response to future events, we are working with the relevant sporting organisations to understand their plans.
 "For example, I've had conversations with the Premier League and others.
 "But we have been clear throughout all of it that the Government will not consent to events taking place unless we can be sure it is safe to do so."
 Dowden also said clubs should look internally before seeking Government support.
 "I have said in the past that for furloughing schemes, that football and sporting clubs taking advantage of it, should be as a last resort," he said.
 "Clubs should be looking to themselves first to see if they can make savings before coming to Government."
 "I'm afraid it is the case that this scheme won't help everyone who might possibly need help," Jim Harra, chief executive of HM Revenue & Customs said when asked about people who would miss out from benefiting under the coronavirus job retention scheme.
 "The scheme is the way it is," he said. "It's been designed and it's launched today. But there is help available as well as this. So for example, universal credit and tax credits have been made more generous.
 "We want to help as many people as we possibly can."
 Chris Hopson, chief executive of NHS Providers, has been speaking further on the widely reported delayed shipment of gowns expected from Turkey.
 "We can only guarantee that gowns are going to reach the front line when they actually landed on UK soil," he told BBC Breakfast, "the boxes have actually been opened and checked and they have then been safety tested."
 He added that while the 400,000 gowns would be welcome, NHS staff were getting through approximately 150,000 gowns a day.
 Mr Hopson emphasised the global shortage of PPE, adding "every single nation on the earth is currently chasing these gowns".
 When asked about anxieties over PPE shortages, Mr Hopson said it was "understandable" for staff to be worried and trust leaders will "move heaven and earth" to provide equipment.
 Oliver Dowden has clashed with presents Piers Morgan, as Morgan accused the Government of "grotesquely" underplaying the number of people who have died in the NHS and care homes as a result of coronavirus on Good Morning Britain.
 Dowden said Government records showed 43 people had died while working in the NHS and care homes.
 Morgan responded by saying Morgan that "it is not, is it?" while showing a double-page Daily Mail spread with the faces of 80 people who are said to have died while treating patients with Covid-19.
 Mr Morgan said the figure was "believed to be 7,500", and asked Mr Dowden: "Why are you so grotesquely underestimating the scale of what is going on on the front line?"
 Mr Dowden said: "I'm using the most up-to-date figures I have been given, and the figure I was given this morning is the one I quoted to you. Of course there is a time lag with non-hospital figures."
 Sir Richard Branson has said that the survival of Virgin Atlantic is dependent on government intervention.
 "This would be in the form of a commercial loan - it wouldn't be free money and the airline would pay it back (as easyJet will do for the £600 million loan the Government recently gave them).
 "The reality of this unprecedented crisis is that many airlines around the world need government support and many have already received it.
 "Without it there won't be any competition left and hundreds of thousands more jobs will be lost, along with critical connectivity and huge economic value."
 https://www.wigantoday.net/health/coronavirus/coronavirus-uk-live-blog-latest-pm-said-be-cautious-about-easing-lockdown-fear-second-infection-wave-2543216
