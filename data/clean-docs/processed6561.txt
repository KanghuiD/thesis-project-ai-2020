Celebs are ‘super-spreaders’ of coronavirus misinformation, study says
 Get the latest BPR news delivered free to your inbox daily. SIGN UP HERE.
 While it’s true the left-wing establishment media spread (and produce) a large swath of fake news, researchers from Queensland University of Technology’s Digital Media Research Centre say celebrities are the ultimate “super-spreaders” of garbage.
 Led by Prof. Axel Bruns, a research team “looked into a range of COVID-19-related disinformation circulating online, and found that spikes in circulation nearly always correlated with celebrity or media endorsements,” according to the Brisbane Times.
 He added: “These are the super-spreaders. These are the people who are really making something go viral.”
 Trump’s eighth-grade lunchroom revolt.
 The White House tried to move a reporter to the back of the press room, but she refused. Then Trump walked out. https://t.co/oT32A5ktVU
 — Jeff Jarvis (@jeffjarvis) April 26, 2020
 Only the worst fascists use the power of government to intimidate the press.
 This is the behavior of dictators. https://t.co/70fYJ7ukXt
 — Fourth Estate (@FourthEstateOrg) April 26, 2020
 The tweets above that Bruns retweeted both pertain to the incident involving CNN correspondent Kaitlan Collins that occurred last Friday. And as seen above, they falsely paint Collins as a victim and the president as a bad guy.
 One bit of fake news that Bruns’ team examined was the conspiracy theory that 5G cell phone towers are somehow linked to the spread of the coronavirus.
 “Spikes also occurred when UK tabloid the Daily Express wrote an article about it, when a Nigerian pastor’s warning about the theory went viral, and when a UK boxer posted a similar video to a sports group with almost 26 million members.”
 What’s bizarre is that one of Bruns’ team members, fellow researcher Dr. Timothy Graham, may have tried to portray these individuals and outlets as somehow pro-Trump.
 “Social media has been a “‘payday’ for disinformers — many of them pro-Trump, Q’Anon posters — trying to sway people to their world view, says fellow researcher Dr Timothy Graham,” the Times reported.
 Because of the way the Times wrote the story, it’s unclear whether the paper itself was accusing the “disinformers” of being pro-Trump or whether the accusation originated with Graham. Either way, the accusation seems to be demonstrably false.
 Last year, Hollywood actor Woody Harrelson accused the president of boasting a “God complex.” And in a tweet posted before the 2016 election, rapper Wiz Khalifa urged his fans to vote, lest “this fool Trump” win.
 Look:
 Make sure you guys get out there and vote. It’s better than not voting and actually giving this fool Trump a chance at winning.
 — Wiz Khalifa (@wizkhalifa) October 11, 2016
 These don’t appear to be the words of “pro-Trump” people …
 “Now the public is genuinely confused, and genuinely divided. They’re pointing fingers at China… and at each other. This is payday for disinformers. This is the best thing that could happen to them and the worst thing that could happen for democracy,” the Times quoted Thomas as explicitly saying.
 It’s not clear what his beef with people “pointing fingers at China” happens to be, given as the Communist Party of China is responsible for the coronavirus pandemic.
 As for a solution to the epidemic of fake news circulating on social media, Bruns recommended starting with fact-checking. But the problem is that social media fact-checkers have themselves been known to produce and share fake news.
 Moreover, on more than one occasion, the so-called fact-checkers have completely bungled their fact-check.
 “Way back on Feb. 23, The Post ran an opinion piece by Steven Mosher saying that we couldn’t trust China’s story about the origins of COVID-19,” the New York post reported earlier this month.
 The social media network’s fact-checkers decided the piece wasn’t factually accurate. Yet weeks later, it’s now been confirmed that actually, the piece was likely correct.
 — Conservative News (@BIZPACReview) April 16, 2020
 The most effective solution, Bruns added, would be to encourage people to learn how to perform research and think critically.
 “You have these celebrities, politicians and others with very large audiences, and getting them to not share something or not talk about something that they have no idea about – that may be very difficult to do, but it is a really important point where further transmission … can be stopped,” he said.
 Unfortunately, critical thinking skills are on a steady decline these days, particularly among clueless celebrities.
 Gab Social
 Send this to a friend
 https://www.bizpacreview.com/2020/04/26/celebs-are-super-spreaders-of-coronavirus-misinformation-study-says-913413
