Opinion & Analysis | The Irish Times
   
 The world has loved, hated and envied the US, Now, for the first time, we pity it
 Solving the economic crisis we find ourselves in due to Covid-19 is actually quite simple
 Can the crisis create a more fair world, or will imbalance of power grow?
 The choice for the Greens is to deal with the world as it is, or to wait in hope that the world changes
 Plan might help people adhere to coronavirus restrictions as public’s patience wanes
 Neglect of nursing homes during coronavirus pandemic has had disastrous results
 Raising money is almost entirely absent from debate on government formation
 Policy response to Covid-19 must include our psychological and social wellbeing
 A Fianna Fail and Fine Gael senator make the case for the middle ground
 Older people have for too long been regarded as a sort of foreign species in our society
 People may complain about getting vouchers not refunds but they’ll still return for more
 Some of the State’s most successful administrations have been propped up by Independents
 This may be the best chance to achieve a fairer balance since the postwar period
 Pandemic is an opportunity to offer solidarity to the poorest continent
 The Dutch and the Italians not the only ones adhering to national stereotype
 FF-FG ambivalence makes prospect of meaningful regional autonomy possible
 The strategy to ease the lockdown cannot be dictated by public health experts alone
 The longer the crisis continues, the more difficult it has become to see the processes behind political decision-making
 Scientists around the world are working at an unprecedented pace to develop a vaccine against the novel coronavirus
 https://www.irishtimes.com/opinion/opinion-analysis
