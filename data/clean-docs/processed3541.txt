Opinion: Local news collaboration in the time of coronavirus – The Colorado Sun
 						Special to The Colorado Sun					
 The image is familiar, even mundane, to most of us now: a dozen or more faces peer out against various backgrounds – some real, some green-screened – talking and planning across many miles. Together, apart.
 But it was an unprecedented group that first gathered on April 1 – in more ways than one. First, the faces were of journalists from newsrooms from across Colorado, who in previous years have been more accustomed to competition than collaboration. Second, the group was finding unity in a common purpose – how working together could help each of them reach more Coloradans with accurate, timely, potentially life-saving updates and rigorous reporting in the unprecedented time of COVID.
 Since that first Zoom call less than a month ago, nearly 100 journalists representing over 40 newsrooms across the state have joined the Colorado News Collaborative – which now includes the state’s largest and smallest newspapers, radio and television stations, digital news outlets, and professional journalism associations. A major factor prompting each to join the COLab, as it’s called, is a common need to figure out how to do more with less.
 Why? While online news traffic is surging due to reader demand for trustworthy local news and information, Colorado’s newsrooms themselves are running on fumes.
 Last fall, the Colorado Media Project reported that at least 30 Colorado counties — most of them rural — have been left with only a single source of original local journalism.
 Now, with local advertising plummeting to historic lows, the COVID crisis is already further expanding Colorado’s news deserts, reshaping the local news landscape in ways it may never recover.
 The formation of the Colorado News Collaborative is a bright spot in these dark days. Participating newsrooms are joining forces to coordinate coverage to avoid duplication and maximize resources; cover more stories about people and communities statewide that are currently going untold; collaborate across newsrooms on data-driven accountability journalism; and facilitate wider distribution of stories, in both English and other languages, in order to better serve the public.
 Today and throughout the coming week, in front-page newspaper stories and broadcast features across Colorado, COLab partners are releasing their first major collaborative storytelling project: COVID Diaries Colorado.
 On April 16, the deadliest day to date in the U.S. coronavirus pandemic, scores of reporters from across Colorado set out to find how people were coping. They found stories of grit, ingenuity and hope. You can find some of their stories in this publication, and all of them are online at colabnews.co. 
 So now if you’re sufficiently inspired, and wondering: How can we support these dedicated local journalists — among the short list acknowledged as essential workers by Gov. Jared Polis — in this challenging time? 
 UNDERWRITTEN BY TOBACCO-FREE KIDS ACTION FUND
 OUR UNDERWRITERS SUPPORT JOURNALISM.   BECOME ONE.
 Most subscription-based newsrooms have already taken down their paywalls to allow free, full access to their COVID-19 coverage. 
 As Coloradans, many of us are rallying to support our local restaurants, retail outlets, and service workers — let’s add local newsrooms to our list of worthy causes.
 Melissa Milios Davis is acting director of the Colorado Media Project and vice president for informed communities, Gates Family Foundation.
 				Support independent, Colorado-owned journalism by joining The Colorado Sun.			
 Denver attorney Steve Zansberg imagines what different leadership in the White House briefing room might look like.
 Colorado’s system of 52 school-based health centers has grown into a crucial element of overall population health, delivering more than 100,000 visits last school year.
 After a lifetime of watching big moments in sport history together, the coronavirus has changed that bonding experience for us for the foreseeable future
 https://coloradosun.com/2020/04/26/opinion-local-news-collaboration-coronavirus/
