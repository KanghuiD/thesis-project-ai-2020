One hundred Italian doctors have died of coronavirus |  News | Al Jazeera
 Doctors said being asked to fight the coronavirus pandemic without adequate protective equipment was 'not a fair fight'.
 One hundred Italian doctors have died after contracting coronavirus since the pandemic reached the Mediterranean country in February, Italy's FNOMCeO health association said on Thursday.
 "The number of doctors who have died because of COVID-19 is 100 - perhaps even 101 at the moment, unfortunately," a spokesman for the association told AFP.
 The toll includes retired doctors who the government began calling in a month ago to help fight a virus that has officially claimed a world-topping 17,669 lives in Italy.
 Italian media reports estimate that 30 nurses and nursing assistants have also died of COVID-19, the disease caused by the virus.
 "We can longer allow our doctors, our health workers, to be sent to fight without any protection against the virus," FNOMCeO president Filippo Anelli said on the association's website.
 "It is an unfair fight."
 Rome's ISS public health institute estimates that 10 percent of those infected with the novel coronavirus in Italy work in healthcare.
             AFP news agency
 	 
 https://www.aljazeera.com/news/2020/04/italian-doctors-died-coronavirus-200409211435347.html
