Life Insurance and the Coronavirus: Your Questions, Answered - NerdWallet
 We will continue to update this page as developments occur.
 Get answers about stimulus checks, debt relief, changing travel policies and managing your finances.
 Life insurance is one way to provide financial stability for your loved ones after you die. Understanding your policy and how COVID-19 affects your coverage is important for both you and your family.
 To that end, NerdWallet has put together a guide on what you should know about life insurance and the current global pandemic. We have answers on:
 Life insurance policies will pay out in the event of a death from COVID-19. Although a life insurance company may change guidelines for future applicants, insurers aren’t able to alter any policies that have already been sold. This means if you already have life insurance, your company won’t be able to suddenly change the policy and deny your beneficiary a payout if you die from the coronavirus. However, an insurer may reject any life insurance claim if the policyholder submitted an inaccurate life insurance application or didn’t pay insurance premiums.
 Right now, insurers are asking people to wait until they have recovered from the coronavirus to apply for life insurance. When applying for a policy, you’ll be asked to disclose your medical history, including whether you are currently ill. Some companies are specifically asking applicants if they have been treated for COVID-19. Because the coronavirus can make someone terminally ill so quickly, you likely won’t even be able to purchase a guaranteed issue policy, a type of life insurance that doesn’t require a medical exam or extensive health questions and offers a relatively small death benefit.
 Insurance companies are responding to the pandemic in different ways. Prudential, for instance, has suspended applications for 30-year term life insurance at least through June. Other companies are restricting applications based on the applicant’s recent travel history. And while most insurers continue to issue life insurance policies, this could change as the situation evolves.
 For now, applying for life insurance has changed in a few key ways in the wake of the coronavirus pandemic, including:
 Life insurance rates have remained the same as before the pandemic, but this could change in the future.
 Applicants are usually asked to disclose any international travel plans when applying for life insurance. While this information is still usually required, now there also are waiting periods for anyone who has traveled anywhere overseas. However, this guidance could change and insurers may place additional restrictions.
 Some companies are extending medical exam time frames. Certain companies are still offering in-person exams, but availability may be limited and depend on your location. In addition, insurers may ask you to postpone the exam if you’re sick or have recently traveled internationally. Some insurance companies are offering coverage — both term and permanent insurance — without a medical exam through a virtual application for healthy applicants. Customers can receive life insurance (generally from $1 million to $3 million) by answering questions online or through a phone interview.
 Contact your insurer if you don’t think you will be able to pay your premium. There is usually a grace period to pay an insurance bill, around 30 days. If the bill is paid during this time, your policy will remain in force. Some companies are extending grace periods, and state regulators are requiring it in certain states. Outside of a coronavirus-related financial hardship, if a premium isn’t paid after the grace period, the policy is typically voided. Most insurers let customers apply for reinstatement three to five years after insurance is lost, as long as they prove they aren’t too risky to insure. To determine how healthy they are, policyholders might have to answer medical questions or take another life insurance medical exam. Other options to help with costs include changing how often you pay your bill (monthly, quarterly) or reducing your coverage.
 AIG:
 Life: 800-888-2452
 Variable Universal Life: 800-340-2765
 Guardian Life: 888-482-7342
 John Hancock: 800-732-5543
 Lincoln Financial Group: 800-487-1485
 MassMutual:
 All life insurance: 800-272-2216
 Guaranteed acceptance and simplified whole life insurance: 844-872-2200
 Nationwide: 855-590-9777
 New York Life: 800-225-5695
 Northwestern Mutual: Try your local office or call 866-950-4644
 Pacific Life: 800-800-7646
 Penn Mutual: 800-523-0650
 Prudential: 800-556-8527
 State Farm: Call your State Farm insurance agent. The general number is 1-800-782-8332.
 Transamerica: 800-797-2643
 Allstate: Call your Allstate agent. The general number is 800-255-7828.
 American Family: 800-692-6326
 Amica: 800-242-6422
 Auto Club of SoCal (AAA in Southern California): 800-924-6141
 Auto Club MI Group (AAA in several Midwest states): 800-222-1134
 Auto-Owners: 800-288-8740
 CSAA (AAA in Northern California and several other states): 800-922-8228
 Erie: 800-458-0811
 Farmers: 888-327-6335
 Geico: 800-207-7847
 The Hartford: Non-AARP members call 877-896-9320; AARP members call
 800-423-6789.
 Kemper: 866-860-9348
 Liberty Mutual: 800-290-7933
 Mercury: 800-503-3724
 MetLife: Call 800-438-6381 if you have MetLife home or auto insurance through an employer; call 800-422-4272 if you have individual coverage.
 National General: 888-293-5108
 Nationwide: 877-669-6874
 NJM: 800-232-6600
 Progressive: 800-776-4737
 Safeco: Call your local independent agent. The general number is 800-332-3226.
 State Farm: Call your State Farm insurance agent. The general number is 800-782-8332.
 Travelers: 800-842-5075
 USAA: 800-531-8722		
 				Kayda is an insurance writer at NerdWallet. She has covered many types of insurance, including auto, homeowners and life, and enjoys helping others understand their options and make better financial decisions.
 									Read more
 Get free quotes from top companies
 The Best Life Insurance Companies
 1. Northwestern Mutual
 2. Pacific Life
 3. Guardian Life
 4. MassMutual
 5. Principal Financial
 Calculators & Tools
 Which is right for you: Term life or whole life
 How much life insurance do you need?
 How medical conditions affect life insurance rates
 NerdWallet Compare, Inc. NMLS ID# 1617539
 NMLS Consumer AccessLicenses and Disclosures
 California: California Finance Lender loans arranged pursuant to Department of Business Oversight Finance Lenders License #60DBO-74812.
 https://www.nerdwallet.com/blog/insurance/life-insurance-coronavirus-faq/
