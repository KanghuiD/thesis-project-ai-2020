(Empty)
       Tutor in Law, Swansea University
     
       Lecturer in Law, Swansea University
 The authors do not work for, consult, own shares in or receive funding from any company or organisation that would benefit from this article, and have disclosed no relevant affiliations beyond their academic appointment.
 Swansea University provides funding as a member of The Conversation UK.
 The Conversation UK receives funding from these organisations
 View the full list
 The restrictions that have been introduced as a result of the coronavirus pandemic are having a well documented effect on businesses. Various sectors are all but closed until further notice, while others are functioning well below their usual capabilities. One important consequence, which has received virtually no attention, is that this potentially leaves many businesses liable for breaching contracts which have been made very difficult to carry out.  
 The law that governs this area in England and Wales is known as frustration of contract. It says that when something occurs through no fault of either party that makes it impossible to meet contractual obligations, the contract effectively comes to an end. When a contract is frustrated in this way, no one can be sued for damages because contractors are freed from their liabilities.
 This sounds like common sense, but it must be balanced against the potential injustice to the other party when a contract is terminated. For this reason, the courts strictly limit the circumstances in which frustration applies. 
 They protect businesses from liability if they have been rendered utterly unable to carry out their contractual duties, but not those who are still technically able to meet them. In the present crisis, this denies protection for many with duties to fulfil. In effect, the law is incentivising such businesses to act irresponsibly and ignore the government health advice to avoid breaching commercial contracts – with potentially dangerous knock-on effects for public health. 
 Likewise, contracts are likely to be frustrated where they cannot be performed because one of the businesses has been ordered to cease trading. This would probably, for example, protect a restaurant or pub from being sued for refusing to take a big delivery of meat from its supplier. However, this would only be applicable if the restaurant exclusively served people on the premises, as otherwise the purpose of their supply contracts would remain in effect. 
 The point here is that frustration will only aid businesses who are completely unable to trade. Those who remain able to perform some of their usual activities, for example takeaway food or mail-order wines, may remain bound by contracts with their suppliers – unless they can negotiate a resolution with their suppliers. If it should become uneconomical to perform these activities without also continuing to engage in on-premises sales, it will make no difference; they will potentially still be bound by any supply contracts or whatever. 
 These are all socially responsible choices made in the public interest, but none of them will be protected by the traditional frustration doctrine. The doctrine can never be invoked in response to “self-induced” events, regardless of the motives behind the decision.
 This is why the current law appears to punish businesses that are being socially responsible. It incentivises them to be irresponsible. The doctrine of frustration normally provides much needed certainty to anyone entering into a contractual agreement. The irony in the current crisis is that the same limitations are actually creating uncertainty instead. 
 As some cases find their way to court, judges may well find ways around these difficulties. In times of war, for example, the courts have seen fit to “bend” the existing rules to further the social policy objective of protecting national security: in Spolka Akcyjna v Faribairn Lawson Combe Barbour (1943), the court disregarded the usual rules regarding the written terms of the contract, as parties may otherwise have found themselves out of pocket for refusing to trade with occupied Poland during the second world war.
 The current crisis is in the same kind of territory, so a similar argument could well be persuasive with regard to public health. To treat socially responsible parties favourably, judges may apply a liberal interpretation to “self-inducement” or to what counts as “illegal” in the context of the pandemic restrictions.  
 Yet this possibility is of little help to businesses that need to make decisions in the here and now, and such optimism would be reckless. Judges may equally be reluctant to extend the existing law due to fears that it would set precedents that could make the frustration doctrine too wide in other situations. They might also worry that they were permitting businesses to exploit the doctrine in “bad faith” to escape liabilities – knowing that those with the deepest pockets will probably be able to take the most advantage. 
 The only way to provide sufficient certainty for businesses is for the government to act immediately. This might involve ordering non-key businesses to close immediately; or placing distancing guidelines on a binding, statutory footing; or passing legislation absolving companies of liability in these contracts where they are voluntarily engaging in socially desirable behaviour such as distancing. 
 Governing the actions of businesses by diktat in this fashion may seem draconian, but it is the only way to provide certainty. It is certainty that commercial ventures need most right now: a scarce resource in these trying times.
 On the Practice of Film Criticism with Jean-Michel Frodon
           —
           St Andrews, Fife
 York Festival of Ideas
           York, York
 Festival of Ideas
           Hatfield , Hertfordshire
 Essex Analytics, Data Science and Decision Making Summer School
           Colchester, Essex
 Advanced Biobased Materials and Composites Conference
           Portsmouth, Hampshire
 https://theconversation.com/businesses-face-an-ugly-choice-ignore-coronavirus-advice-or-risk-being-sued-for-breach-of-contract-136064
