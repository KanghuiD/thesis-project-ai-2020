County attorneys available for free legal advice during coronavirus outbreak – Colorado Daily
 					
 Editor’s note: The byline on this story has been updated to correctly identify its author.
 Due to the coronavirus crisis, the Boulder County Bar Association will set up appointments with its member attorneys for those who are in need of free legal advice.
 While the group’s offices remain closed until further notice, it is focusing on using technology to maintain connections and engage with their membership.
 “Before all of this crisis hit, we did a once-a-month free legal clinic,” said Laura Ruth, executive director of the organization. “Once this order came down, we couldn’t do it anymore, but there is still a huge need in our community for people needing someone to talk to.”
 The group is aiming to replace its monthly face-to-face meetings with the free legal advice from member attorneys on a number of different issues.
 “There are so many people struggling with employment issues, mortgage issues, or issues like updating their will,” Ruth said. “There’s so many members of the population that just can’t hire an attorney at this point in time who can still benefit. I think our legal clinics have been a huge success to giving people access to what they might not normally have access to due to income restrictions.”
 Citizens in need of legal advice can apply at www.boulder-bar.org.
 Have you been Zooming and Skyping a lot recently? All this working from home is resulting in some “interesting” hair...
 Remember: in times of loneliness, music can lift your spirits, bring joy to your heart and heal your soul. Even...
 After all this stay-at-home time, we’re really going to need some hair care when we can get out again! Make...
 Commemorating the life of a departed loved one helps family members and friends pay a respectful farewell. Greenwood & Myers...
 Spending a lot of time at home lately? And tired of looking at the same old boring walls? Make a...
 OK
 https://www.coloradodaily.com/2020/04/13/county-attorneys-available-for-free-legal-advice-during-coronavirus-outbreak/