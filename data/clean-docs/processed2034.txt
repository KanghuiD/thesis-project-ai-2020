Coronavirus: Stephen Fry to star in online play - BBC News
 Stephen Fry will star in a new online radio play to raise funds for the theatre industry as it deals with the effects of the coronavirus pandemic.
 Emily Atack, Sarah Hadland and Russell Tovey will also appear in The Understudy, based on the novel by David Nicholls.
 The play will be broadcast online in two parts in May to members of the public who have bought a £5 'ticket'.
 All the actors involved will record their lines separately at home.
 Nicholls said he was "hugely excited" to see his 2005 novel "come to life on a new online stage".
 Fry said he hoped lots of people would "combine a good time with support for our wonderful theatre industry".
 Proceeds will be donated to the Theatre Development Trust, Acting for Others and other charities.
 The Understudy tells of a struggling actor who is hired to understudy a film star in a West End show.
 Tovey plays the lead role of Stephen McQueen, while Jake Ferretti plays the actor whose shoes he hopes to fill.
 Minar Anwar, Sheila Atim and Layton Williams are among the other actors involved.
 Produced by the Lawrence Batley Theatre in Huddersfield, the play will be broadcast on 20 and 27 May at www.understudyplay.com.
 For just £4, meanwhile, theatre fans can stream Fleabag, the original one-woman show that spawned Phoebe Waller-Bridge's hit BBC sitcom.
 Waller-Bridge's solo performance, recorded at London's Wyndham's Theatre in 2019, is available from the Soho Theatre's website for just three weeks.
 Proceeds will be donated to UK-based charities "on the front lines of combating the Covid-19 pandemic".
 Waller-Bridge said she hoped the film of her performance "can help raise money while providing a little theatrical entertainment in these isolated times.
 "Now go get into bed with Fleabag!" she continued. "Its for charity!"
 On Monday, the Society of London Theatre (SOLT) announced it was cancelling all West End theatre performances until 31 May.
 "We are sorry that in these testing and difficult times you are not able to enjoy the show you have booked for," it told ticket holders in a statement.
 The theatre industry is one of many struggling during the coronavirus pandemic - most have been closed since the UK lockdown was announced last month.
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/entertainment-arts-52185466
