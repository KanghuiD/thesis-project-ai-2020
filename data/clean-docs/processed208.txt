Home • Chico News & Review
 A note on this feature: The candidates’ answers have been minimally edited, mainly for grammar and punctuation. This is an extended version of the Q&A that appeared in the Oct. 8 print edition. Seats for […]
 Sending ballots by mail has become a big story in America leading up to the 2020 General Election, but in Butte County, voters already have some experience with the process. For the March primary, Butte […]
 Asked in downtown Chico. Isaac Zinkwindow washerProbably Washington because I have family there. Somewhere on the Olympic Peninsula. Becky Wallacehair stylistProbably North Carolina. Somewhere close to the beach. Why not? David Stachurabiology professorOregon, probably. But […]
 A cloud of smoke grew enormous on Butte County’s eastern horizon Sunday afternoon (Sept. 27) as high northeasterly winds predicted for the day fanned the flames of the North Complex Fire and stoked a new […]
 When Emily Zimmerman lost her Paradise home to the Camp Fire, her cupcake shop, Lovely Layers Cakery, became her refuge. She leaned into her second home, continuing to work and bake sweet treats while she […]
 In late March, as coronavirus started to spread in the northern Sacramento Valley, Dr. Steve Zlotowski made a four-minute video that went viral on the internet. An emergency medicine physician at Enloe Medical Center, he […]
 Last Friday (Sept. 4) marked six months since Gov. Gavin Newsom declared a state of emergency in California (on March 4) in response to the COVID-19 pandemic. On Aug. 28, the governor rolled out the […]
 Citing “a troubling number of positive COVID-19 cases on campus,” Chico State President Gayle Hutchinson has canceled in-person classes and ordered the closure of university housing. Hutchinson made the announcement Sunday (Aug. 30), six days […]
 Butte County Public Health announced today (Aug. 26) that a cluster of more than 15 new positive COVID-19 cases have been identified at one apartment complex near the Chico State campus. Those infected are between […]
 Just before spring break this past March, when school districts across Butte County shifted rapidly to remote instruction in response to coronavirus, Caitlin Dalby also made a quick pivot. In her eighth year as a […]
 When schools closed in March due to COVID-19, Carob Bradlyn and Justin Cooper started to look for ways to keep their son engaged at home. Bradlyn, a local artist and Yuba City High School ceramics […]
 In early March, as coronavirus started to spread through the Sacramento Valley, Katy Thoma remained optimistic. She understood the gravity of the situation—Butte County officials had recently declared both a local emergency and a local […]
 You -- the Chico News and Review -- are the Chico community.
 My connection to Chico and our rad community is in your hands...so please have some money to keep going.
 https://chico.newsreview.com/
