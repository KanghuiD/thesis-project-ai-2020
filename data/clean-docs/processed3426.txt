Live blog: KC Metro COVID-19 updates for April 22
 Would you like to receive local news notifications on your desktop?
 Menu
 KANSAS CITY, Mo. — The novel coronavirus, or COVID-19, continues to spread across the world, and in Missouri and Kansas.
 41 Action News is keeping track of all closings and cancellations, as well as tracking where positive coronavirus cases are located.
 We are also keeping track of which metro jurisdictions are under stay at home orders for the foreseeable future, only allowing for essential business and activities. 
 Updates on the spread of the virus and how it is affecting the metro can be found below for April 22.
 9:03 a.m. | Patricia Gray, the principal at Notre Dame de Sion, joined 41 Action News to discuss how life has changed and how her role has evolved.
 8:37 a.m. | University of Kansas Health System officials gave their daily COVID-19 update. They were joined by Chief Medical Officers from Advent Health, Truman Medical Center and Liberty Hospital to talk about how treatment is going across the Kansas City area.
 8:27 a.m. | St. Joseph Medical Center in Kansas City, Missouri, wants to remind patients that they are open for all medical emergencies. Anyone experiencing something like a heart attack or stroke should not let COVID-19 fears keep them from visiting the emergency department.
 7:14 a.m. | Because of shutdowns around the world caused by COVID-19, pollution has plummeted and wildlife is thriving.
 6:22 a.m. | Criminal records may be keeping small business owners from getting COVID-19 relief from the federal government.
 5:36 a.m. | McDonald's is offering free "thank you" meals to first responders and healthcare workers battling COVID-19. The meals can be chosen off a specific menu and are available at select locations. They will be served in Happy Meal boxes with a note of appreciation.
 5:15 am. | The memorial for fallen Kansas City, Missouri, Fire Department EMT Billy Birmingham is scheduled to take place Wednesday. Several blocks of Troost Avenue will be closed this afternoon as firetrucks line the street to pay their respects.
 Here's the route for today's procession to honor @KCMOFireDept EMT Billy Birmingham. Things get started at 11 a.m. Birmingham died of COVID19 last week.
 — Charlie Keegan (@CharlieKeegan41) April 22, 2020
 5 a.m. | The International Committee of the Red Cross sent out guidelines for planning for mass COVID-19 fatalities. Most of the instructions were pointed at other regions of the world, but the committee did recommend authorities be prepared with building to store bodies, have enough PPE to protect those burying the dead and make sure the deceased are buried respectfully and their families respected as well.
 Previous coverage: Monday, March 16Tuesday, March 17Wednesday, March 18Thursday, March 19Friday, March 20Saturday, March 21Sunday, March 22Monday, March 23Tuesday, March 24Wednesday, March 25Thursday, March 26Friday, March 27Saturday, March 28Monday, March 30Monday, March 30Tuesday, March 31Wednesday, April 1Thursday, April 2Friday, April 3Friday, April 3Saturday, April 4Monday, April 6Tuesday, April 7Wednesday, April 8Thursday, April 9Friday, April 10Saturday, April 11Sunday, April 12Monday, April 13Tuesday, April 14Wednesday, April 15Thursday, April 16Wednesday, April 15Thursday, April 16Friday, April 17Saturday, April 18Sunday, April 19Monday, April 20Tuesday, April 21
 Report a typo
 Johnson County, KSCOVID-19 Video Update
 https://www.kshb.com/news/coronavirus/live-blog-kc-metro-covid-19-updates-for-april-22
