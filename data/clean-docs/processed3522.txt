COVID-19 (novel coronavirus) | Ministry of Health NZ
 This section provides the latest updates, information and advice on COVID-19.
 New Zealand has 1,490 confirmed and probable cases of COVID-19. More information is available on our current cases page. With continued vigilance the chance of widespread community outbreak is expected to remain low.
 New Zealand is currently at Alert Level 3 – Restrict. To find out more see:
 COVID-19 information on this website is reviewed and updated regularly. This includes advice for Alert Level 3.
 For COVID-19 health advice and information, contact the Healthline team (for free) on 0800 358 5453 or +64 9 358 5453 for international SIMS.
 All news and media updates
 Our advice is under active review and is updated regularly. The latest updates include the following changes to the website:
 8 May:
 7 May:
 6 May: 
 5 May:
 2 May:
 1 May:
 Page last updated: 08 May 2020
 https://www.health.govt.nz/our-work/diseases-and-conditions/covid-19-novel-coronavirus
