Coronavirus (COVID-19) Resource Center - Ogletree Deakins
 COVID-19 Update for Virginia Employers – April 14, 2020
 COVID-19 Update for New Jersey Employers – Part 2 – April 15, 2020
 Mastering Work from Home during COVID-19: Avoiding the Pajama Predicament and Other Pitfalls
 Understanding the EEOC’s Recent Guidance Concerning COVID-19 in the Workplace
 California COVID-19 Series: Answers to Your Frequently Asked Questions
 Key Considerations for Employers As the Coronavirus Spreads
 Labor Issues Implicated by the Coronavirus
 Third Thursdays with Ruthie: Unionized Employers and COVID-19
 US DOL – COVID-19 or Other Public Health Emergencies and the Family and Medical Leave Act Questions and Answers
 EEOC – What You Should Know About the ADA, the Rehabilitation Act, and COVID-19
 EEOC – Pandemic Preparedness in the Workplace and the Americans With Disabilities Act
 CDC – Interim Guidance for Businesses and Employers
 IRS – COVID-19 Tax Credits for Small and Midsize Businesses
 OFCCP – National Exemption for New Contractors from Federal Affirmative Action Obligations
 Please understand that merely contacting us does not create an attorney-client relationship. We cannot become your lawyers or represent you in any way unless (1) we know that doing so would not create a conflict of interest with any of the clients we represent, and (2) satisfactory arrangements have been made with us for representation. Accordingly, please do not send us any information about any matter that may involve you unless we have agreed that we will be your lawyers and represent your interests and you have received a letter from us to that effect (called an engagement letter).
 https://ogletree.com/solutions/coronavirus-covid-19-resource-center/