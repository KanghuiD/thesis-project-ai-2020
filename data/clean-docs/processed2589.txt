      Human ingenuity will solve the coronavirus, but I’m worried we might screw up everything else - MarketWatch
     
 It is very, very hard to be short human ingenuity.
 For example, when you buy commodities, you are betting, partially, that people will be incompetent at producing or obtaining them. Occasionally this happens, but not for very long.
 Commodity prices rose in the mid-2000s because of other factors, such as a sharp rise in inflation expectations, resulting in large institutional allocations to commodities via commodity index swaps. But it wasn’t because we screwed things up.
 Commodity prices generally go down over time. The long-term returns on commodity indices have been bad. Crop yields keep going up, and fracking has substantially reduced the price of oil
         CL.1, 
         +80.20%
       and natural gas
         NG00, 
         -1.19%.
       The world is having some trouble getting its hands on more gold, but those of you with gold
         GC00, 
         -1.33%
       in your portfolios aren’t sad about that.
 To be clear: A bet on rising commodity prices is either a bet on macro factors, like inflation, or on profound political shifts. It is hard work betting against human ingenuity.
 When you short the stock market
         SPX, 
         -1.78%,
       you are also betting against human ingenuity. There have been a handful of times in history when this has worked. The financial crisis 12 years ago was one of them. But even the financial crisis, peak to trough, was over in 21 months.
 The European debt crisis, which began in earnest in 2010, was equally devastating, but only resulted in a decline of about 20% in U.S. stocks. The LTCM/Russian debt default in 1998 also only resulted in a decline of about 20%. The 1991 recession, as severe as it was, was just a blip in historical terms.
 In 2008, a group of short-sellers became famous by shorting the U.S. housing market. Investors have spent the past 10 years trying to replicate that feat, unsuccessfully.
 Human beings do occasionally screw up — there are financial accidents from time to time. But the imbalances and excessive leverage that were present just a month ago started building long before that.
 You could have been short in 2015, 2016, 2017, 2018 and 2019 — and people were. If you get one of these bear markets right, the timing will be mostly luck, and you will pay a lot of carry in the process.
 There is a lot of pessimism around COVID-19. The pessimism mostly comes from New York, where the number of infections is greatest, and from people working in health care, who are witnessing truly awful things every day.
 Many people seem to believe that:
 • We will not figure this out.
 • We will be under quarantine for months.
 • Social distancing will not work.
 • Hospitals will be overwhelmed.
 • Life will be altered forever.
 Humanity has had a pretty good track record of solving problems, whether it was terrorism, the cold war or other health scares. Nobody is really paying attention to the good news.
 The good news is that we were technologically more prepared to deal with a pandemic in 2020 than at any point in history. Zoom
         ZM, 
         -0.71%,
       and other platforms, are connecting us all — but all the media is focusing on is the security risks.
 Technology is helping us deal with the crisis and allocate resources. The news is about the ways resources are misallocated.
 The vast majority of people are practicing social distancing. The news is about the people who aren’t — the spring breakers in Miami Beach.
 Every step of the way in this crisis, we have consistently underestimated government, underestimated the private sector and underestimated health-care workers. And yet, the rate of change of COVID-19 cases is actually slowing.
 Of course, the actual number of COVID-19 cases is not slowing, but the rate of change is. 
 Typically when people run into trouble with bonds or options, it’s because they don’t understand the second derivative. The market discounts, and by the time it becomes apparent that the number of infections will flatten, the market will have already rallied. It will be too late to buy in at the lows ... or maybe it already is.
 The virus doesn’t scare me; our financial response to the virus scares me.
 We have flipped the switch on unlimited deficit spending, unlimited quantitative easing (QE), and what is basically modern monetary theory (MMT). We are not going to put that toothpaste back in the tube.
 That is the part I am worried about. We have crossed the Rubicon.
 If commodity prices rise significantly, it will probably happen because of macro factors — mistakes at the policymaker level.
 If equity prices decline significantly, it will probably happen because of macro factors — mistakes at the policymaker level.
 Stocks won’t go down because of the virus. We will solve the virus in the short term. Stocks may go down because of what comes after the virus, which is too terrible to contemplate.
 I believe in human ingenuity. I believe in our ability to solve problems.
 I also believe in our capacity to make problems worse. I believe in the human tendency to question the reality of economic trade-offs — as some think you can have your cake and eat it, too.
 Higher education should not be so recast as an entitlement but rather the decadence of present arrangements should be targeted for pro-market reforms.
           Historical and current end-of-day data provided by FACTSET.
           All quotes are in local exchange time.
           Real-time last sale data for U.S. stock quotes reflect trades reported through Nasdaq only.
           Intraday data delayed at least 15 minutes or per exchange requirements.
         
 https://www.marketwatch.com/story/human-ingenuity-will-solve-the-coronavirus-but-im-worried-we-might-screw-up-everything-else-2020-04-18
