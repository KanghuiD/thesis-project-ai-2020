Coronavirus: Raab urges UK public not to ruin lockdown progress - BBC News
 Foreign Secretary Dominic Raab has urged the public to stay indoors over this Easter weekend, telling people: "Let's not ruin it now."
 He said after almost three weeks of lockdown "we are starting to see the impact of the sacrifices we've all made".
 Mr Raab said it was still "too early" to lift the restrictions.
 A total of 7,978 people have now died in hospital after testing positive for coronavirus, up by 881 on Wednesday.
 Speaking at the government's daily briefing, Mr Raab said a decision on whether to ease the lockdown measures would not come until "the end of next week".
 He was deputising for the prime minister, who has been in hospital since Sunday after contracting coronavirus. 
 Boris Johnson was moved out of intensive care on Thursday evening, with a No 10 spokesman saying: "He is in extremely good spirits."
 Mr Raab stressed that the lockdown restrictions would have to stay in place until evidence showed the UK had moved beyond the peak of the virus.
 He said: "After all the efforts everybody has made, after all the sacrifices so many people have made let's not ruin it now.
 "Let's not undo the gains we've made, let's not waste the sacrifices so many people have made.
 "We mustn't give the coronavirus a second chance to kill more people and to hurt our country."
 The first secretary of state was speaking ahead of a bank holiday weekend which has been forecast to be warm, and Downing Street earlier said it gave its "full backing" to police forces to enforce the lockdown rules.
 The announcement of another 881 deaths of people with coronavirus is yet another tragic piece of news.
 And we know that the true death toll to date is higher: this figure doesn't include people who have died with coronavirus but whose death has not yet been reported to the Department for Health and Social Care.
 However this is a fall in the daily total compared to Wednesday's announcement of 938.
 Any fall in the daily figure is to be welcomed, but the scientists advising the government have warned that we shouldn't be surprised if tomorrow's figures once again set a record.
 They have suggested that the peak of the epidemic may not arrive before next week.
 The trends over the last week do suggest that the measures that everyone are taking are having an effect on the epidemic.
 Until last Saturday, the number of deaths was doubling every three-and-a-half days, growing by just over 20% every day.
 Since then, the growth in the number of deaths has halved, down to about 10% a day.
 Even once we pass the peak, we will see more people fall victim to this virus - but there are growing suggestions in the data that the lockdown is having the expected effect.
 Sir Patrick Vallance, the UK's chief scientific adviser, said social distancing measures were curbing the number of new cases and hospital admissions.
 He explained that the death toll would continue to rise for about two weeks after intensive care admissions stabilise, as deaths lag behind admissions.
 David Rosser, chief executive of University Hospitals Birmingham, warned people must not become "falsely reassured" by the flattening of the curve.
 Dr Rosser said he did not want hospitals "to start reaping the consequences" next week if people broke the rules.
 According to new coronavirus laws, the health secretary must review the restrictions at least once every 21 days, with the first review due by 16 April.
 There are now 65,077 confirmed coronavirus cases in the UK, an increase of 4,344 on Wednesday.
 Amid all the speculation about when and how the UK's lockdown may be relaxed, it's worth looking back at the original scientific advice that led to the measures in the first place.
 It makes clear that nothing is likely to change soon.
 The government's scientific advisory committee, Sage, has always suggested that a 13-week programme of interventions will be needed. 
 And although that sounds like very precise timing, it all depends on how the British public responds. 
 The scientists made a fairly pessimistic assumption: that only 50% of households would observe the requirements. 
 So what might a timetable look like? 
 Once the peak in daily deaths has been reached - possibly in the next week or so - even the best-case scenario suggests that it will take a month or two for the numbers dying to fall to low levels. 
 That gets us well into May and maybe to early June, and it'll be a brave political decision to ease the restrictions any earlier if there's a risk of a 'second peak', with a resurgence of the virus. 
 Mr Raab earlier chaired a virtual meeting of the emergency Cobra committee to discuss the lockdown measures.
 And on Thursday evening he held a conference call with all opposition leaders to update them on the government response to the pandemic.
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/uk-52235492
