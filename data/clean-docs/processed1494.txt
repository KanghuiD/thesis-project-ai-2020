                European shares jump on hopes of coronavirus progress - Reuters
 2 Min Read
 (For a live blog on European stocks, type LIVE/ in an Eikon news window) 
 April 9 (Reuters) - European stock markets gained for a fourth straight day on Thursday on hopes the coronavirus pandemic was close to peaking, with investor attention also focused on a meeting of European Union finance ministers to discuss an economic rescue package. 
 The pan-European STOXX 600 index was up 1.2% at 0702 GMT, with battered travel and leisure stocks, autos and miners leading early gains. 
 The benchmark index has rebounded more than 5% this week and recouped about $1.7 trillion in market value since hitting an eight-year low in March but is still about 24% below its record high, as sweeping lockdown measures crush business activity and spark mass layoffs. 
 The number of U.S. jobless claims — the most timely data on economic health — likely totalled a staggering 15 million in the last three weeks, and economists expect U.S. job losses of up to 20 million in April. 
 Meanwhile, European Union finance ministers are set to resume talks on a half-a-trillion euro economic support package on Thursday after failing to reach an agreement earlier this week. 
 Oil and gas stocks bounced 1.9% ahead of a meeting of the world’s largest oil producers to discuss production cuts. (Reporting by Sagarika Jaisinghani in Bengaluru; editing by Uttaresh.V)
 All quotes delayed a minimum of 15 minutes. See here for a complete list of exchanges and delays.
 https://www.reuters.com/article/europe-stocks/european-shares-jump-on-hopes-of-coronavirus-progress-idUSL3N2BX1ZM
