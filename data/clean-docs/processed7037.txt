BLOG: Coronavirus updates from Wednesday, April 1
 Would you like to receive local news notifications on your desktop?
 Menu
 FLORIDA – ABC Action News is committed to providing Tampa Bay area residents all of the updates on the coronavirus, COVID-19, and the impact it's having on our way of life. To help you stay on top of it all, we've compiled all the updates for Wednesday, April 1. For today's updates, visit www.abcactionnews.com/virus. Don't forget we are in this together!
 10:10 PM
 9:15 PM
 6:55 PM
 6:00 PM
 4:49 PM
 4:47 PM
 4:20 PM
 1:22 PM
 11:40 AM
 10:40 AM
 10:05 AM
 — Amalie Arena (@AmalieArena) April 1, 2020
 9:30 AM
 9:25 AM
 7:10 AM
 6:00 AM
 5:00 AM
 Important updates from Tuesday, March 31
 Important updates from Monday, March 30
 Important updates from Sunday, March 29
 Important updates from Saturday, March 28
 Important updates from Friday, March 27
 Important updates from Thursday, March 26
 Important updates from Wednesday, March 25
 Important updates from Tuesday, March 24
 Important updates from Monday, March 23
 Important updates from Sunday, March 22 
 Important updates from Saturday, March 21
 Important updates from Friday, March 20
 Important updates from Thursday, March 19
 → CORONAVIRUS CLOSINGS & CANCELLATIONS IN THE TAMPA BAY AREA
 Important updates from Wednesday, March 18
 → CORONAVIRUS IN FLORIDA: COUNTY-BY-COUNTY INTERACTIVE MAP
 Important updates from Tuesday, March 17
 → COMPLETE COVERAGE OF CORONAVIRUS
 Report a typo
 https://www.abcactionnews.com/news/coronavirus/live-blog-latest-coronavirus-updates-for-wednesday-april-1
