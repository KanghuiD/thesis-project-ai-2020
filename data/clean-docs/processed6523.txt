Coronavirus cancels world events; divided opinions on reopening
 ATLANTA (AP) — Spain called off the Running of the Bulls in July, the U.S. scrapped the national spelling bee in June and Germany canceled Oktoberfest five months away, making it clear Tuesday that the effort to beat back the coronavirus and return to normal could be a long and dispiriting process.
 Amid growing impatience over the shutdowns that have thrown tens of millions out of work, European countries continued to reopen in stages, while in the U.S., one state after another — mostly ones led by Republican governors — began taking steps to get back to business.
 Business owners in the U.S. who got the go-ahead weighed whether to reopen, and some hesitated, in a sign that commerce won't necessarily bounce back right away.
 Mark Lebos, owner of Strong Gym in Savannah, Georgia, where Gov. Brian Kemp announced that gyms and salons can reopen this week, said it would be professional negligence to do so right now.
 “We are not going to be a vector of death and suffering,” he said.
 With deaths and infections still rising around the world, the push to reopen has set off warnings from health authorities that the crisis that has killed well over 170,000 people globally — including more than 42,000 in the U.S. — is far from over and that relaxing the stay-at-home orders too quickly could enable the virus to come surging back.
 The economic damage mounted as oil prices suffered an epic collapse and stocks registered their worst loss in weeks on Wall Street.
 The crisis hit home at Trump ’s Mar-a-Lago club in Palm Beach, Florida, which laid off 153 workers, including bartenders, cooks, dishwashers and housekeepers.
 The U.S. Senate on Tuesday approved nearly $500 billion in coronavirus aid for businesses, hospitals and testing after a deal was reached between Congress and the White House. Trump urged House members to quickly pass the measure.
 Treasury Secretary Steven Mnuchin said during the daily White House briefing Tuesday that while some big businesses obtained access to government loans under an earlier aid package, its intent was to help mostly companies with 10 or fewer workers. He and the president said bigger businesses should return those funds.
 Trump also said he will stop issuing certain immigration green cards for 60 days to limit competition for jobs and “protect American workers” already suffering in an economy devastated by the pandemic.
 In Europe, meanwhile, Denmark, Austria, Spain and Germany began allowing some people back to work, including hairdressers, dentists and construction workers, and some stores were cleared to reopen or will soon get the OK.
 But in an indication that it will be a long time before life returns to normal, Spain canceled its Running of the Bulls in Pamplona, the more than 400-year-old event made world-famous by Ernest Hemingway's 1926 novel “The Sun Also Rises.” It was also called off during the Spanish Civil War in the 1930s.
 The Scripps National Spelling Bee in the U.S. was canceled. The competition has been held since 1925 and was last scrubbed in 1945, during World War II.
 Germany called off the centuries-old Oktoberfest beer festival in Munich, which draws about 6 million visitors each year. It was previously canceled during the two world wars; during a period of hyperinflation in Germany in 1923; and twice because of cholera outbreaks in the 1800s.
 “We agreed that the risk is simply too high,” Bavarian governor Markus Soeder said.
 In Italy, Premier Giuseppe Conte confirmed that businesses can start reopening on May 4 but dashed any hopes of a full end to the country's strict lockdown any time soon, saying: ”A decision of that kind would be irresponsible.’’
 In the U.S., some states, including Tennessee, West Virginia and Colorado, announced plans this week to begin reopening in stages in the coming days. Sunbathers quickly flocked to the sand after some South Carolina beaches reopened with the governor's backing.
 Political tensions were high.
 Some sheriffs in Washington state, Michigan and Wisconsin said they won’t enforce stay-at-home orders. Angry protesters demanding the lifting of restrictions marched in Alabama, North Carolina and Missouri with signs like “Enough is enough.” And Wisconsin Republicans asked the state’s high court to block an extension of the stay-at-home order there.
 During an online ceremony Tuesday to donate masks, ventilators and other desperately needed medical supplies to hard-hit New York City, Chinese Consul-General Huang Ping indirectly appealed to Trump to tone down his recent rhetoric against the Asian country where the virus first emerged.
 After weeks of elaborate praise of Chinese President Xi Jinping's response to the pandemic, Trump has turned to blaming China and halting U.S. contributions to the World Health Organization, accusing it of parroting misinformation from Beijing.
 “This is not the time for finger-pointing,"Huang said. “This is the time for solidarity, collaboration, cooperation and mutual support.”
 Trump, meanwhile, said he had a “very productive" meeting with New York Gov. Andrew Cuomo at the White House on Tuesday during which they discussed how they could work to expand screening “with the goal of doubling testing” in New York in the next few weeks.
 The meeting marks a sharp shift in rhetoric between the two politicians. Days earlier, Trump had called on Cuomo to work harder to secure testing material for his state, while Cuomo pushed back that the president should turn off his television and get back to work.
 Numerous governors and local leaders have said that before they can relax social distancing restrictions, they need help from Washington in expanding testing to help keep the virus in check.
 “If some of these reopenings are done the wrong way, it’s going to affect all of us,” New York Mayor Bill de Blasio said on CNN.
 De Blasio said Tuesday that New York City will honor health care workers and first responders with a ticker tape parade — once it’s safe to hold large gatherings again.
 (function() {'use strict';window.addEventListener('message', function(event) {if (typeof event.data['datawrapper-height'] !== 'undefined') {for (var chartId in event.data['datawrapper-height']) {var iframe = document.getElementById('datawrapper-chart-' + chartId) || document.querySelector("iframe[src*='" + chartId + "']");if (!iframe) {continue;}iframe.style.height = event.data['datawrapper-height'][chartId] + 'px';}}});})();
 
 Station Contact Info:
 KKTV520 E. ColoradoColorado Springs, CO 80903Office:  (719) 634-2844Fax:  (719) 632-0808News Fax:  (719) 634-3741
 Viewers with disabilities can get assistance accessing this station's FCC Public Inspection File by contacting the station with the information listed below. Questions or concerns relating to the accessibility of the FCC's online public file system should be directed to the FCC at 888-225-5322, 888-835-5322 (TTY), or fccinfo@fcc.gov.
 Public File Liaison:
 publicfile@kktv.com - (719) 457-8272
 https://www.kktv.com/content/news/WHO-warns-rush-to-ease-coronavirus-rules-could-cause-resurgence-569812171.html