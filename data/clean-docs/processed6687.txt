(LEAD) Military postpones reserve forces' training again due to coronavirus | Yonhap News Agency
 All Headlines
 North Korea
 Sports
 Top News
 Most Viewed
 Korean Newspaper Headlines
 Today in Korean History
 Yonhap News Summary
 Editorials from Korean Dailies
 URL is copied.
 (ATTN: UPDATES with latest info in last 3 paras) 
  SEOUL, April 29 (Yonhap) -- The military has decided not to hold regular training sessions for reserve forces in the first half of this year due to the new coronavirus, the defense ministry said Wednesday. 
  The training for the country's 2.75 million-strong forces was slated to kick off in June across the nation, after being put off twice from the original schedule in March due to COVID-19. 
  South Korean reservists are required to go through one training session of one to three days per year.
  The ministry said it will make an announcement 45 days prior to the resumption of the training after evaluating the virus situation. 
  "We have decided not to hold training sessions in the first half considering the COVID-19 situation," the ministry said in a release. "The defense ministry will continue to take measures to prevent the spread of COVID-19 and fully prepare for a combat readiness posture."
  The total number of coronavirus cases in the military stood at 39 on Wednesday, with the last infection reported in mid-March.
  Nationwide, South Korea reported nine more cases of the new virus on the day, bringing the nation's total infections to 10,761, including 246 deaths. It marked the 11th day in a row that the number of new infections stayed at 15 or below.
  Amid a marked slowdown in the number of new cases, the ministry on Tuesday lifted the designation of military hospitals in the central city of Daejeon and the southeastern city of Daegu as institutions specialized in infectious disease treatment. 
  scaaet@yna.co.kr(END)
 National
 Economy
 Market
 Science
 Culture
 Images
 Videos
  Korean Newspaper Headlines
 Korea in Brief
 Useful Links
 Festival Calendar
 Weather
 Advertise with Yonhap News Agency
 https://en.yna.co.kr/view/AEN20200429005351325
