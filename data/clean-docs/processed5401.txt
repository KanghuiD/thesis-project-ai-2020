Coronavirus: What measures are countries taking to stop it? - BBC News
 
 Countries around the world are imposing severe restrictions on their populations in a bid to stop the spread of coronavirus.
 Many countries have effectively closed their borders to all but their own citizens, imposed strict controls on internal travel and ordered people to stay in their homes.
 Some of the first restrictions were on travel from China, but then other countries were added as virus hotspots began to emerge elsewhere.
 Japan, which earlier banned entry to visitors from certain areas of China and South Korea, has now extended this to 21 European countries and Iran, and is telling arrivals from the US to go into quarantine for 14 days.
 Australia and New Zealand have banned entry to all foreigners, with Australia telling all citizens and residents who return to the country they must go into quarantine for two weeks.
 Singapore has done the same, stopping visitors entering and requiring all citizens, permanent and long-term residents to self-isolate at home for 14 days. 
 And South Korea has said that anyone arriving from abroad, including their own citizens, will have to self-isolate for two weeks. 
 India has suspended all visas for foreigners until mid-April.
 Canada, the US and the European Union have also imposed restrictions.
 The EU sealed its external borders on 18 March to anyone from outside the bloc for at least 30 days..
 The US has closed its northern border with Canada and is cracking down on people trying to cross illegally from Mexico.
 And China itself, where the Covid-19 outbreak started, has now banned all foreign visitors, concerned that new cases of the virus are starting to come from abroad.
 Across the world more and more countries are placing restrictions on the movement of their citizens, which in some places has led to confusion about what's allowed and what is not.
 Italy - severely hit by the epidemic - began a major lockdown on 12 March, which has been extended beyond the original end date of 25 March, and been gradually tightened.
 France and Spain have also told people they need permission to move around, with the restrictions being tightened as cases of the virus continued to increase. 
 The UK has joined other countries in severely limiting movement, although people are allowed out once a day to exercise, to shop for basic necessities, for medical reasons or to go to work if absolutely necessary.
 There have been questions in the UK and elsewhere about how to interpret the rules, and some criticism of the way the authorities are enforcing them.
 The authorities in France, Spain, Italy and the UK have introduced fines for people who ignore the rules.  In one part of Italy - Lombardy - these are as high as €5,000 (£4,400).
 The lockdown rules in Germany vary slightly from state to state, but do allow people to leave their homes for fresh air.
 There are now major restrictions across US states. The US state of California is stopping people leaving home unless it's absolutely necessary and is forcing businesses deemed non-essential to close.
 New York State, which has the highest number of coronavirus cases in the US, also introduced a strict lockdown.
 When the outbreak began in China, the authorities there restricted travel nationwide, and told people to stay at home, relaxing this only recently.
 In Hubei province, where the virus started, travel restrictions are now being eased, allowing some people to enter and leave from elsewhere in China.
 And the city of Wuhan is due to come out of lockdown on 8 April.
 India has imposed a strict lockdown on its 1.3 billion population after a sharp rise in coronavirus cases there, and Russia has urged people to stay at home.
 Many other countries have also limited movement to varying degrees, although one exception is Sweden where there are fewer restrictions than in other European countries.
 The World Health Organization has urged countries to test as much as possible to find out who's infected, and so help curtail the spread of the virus.
 But there has been a wide variation in testing. 
 South Korea tested the most people per head of population while others have tested much less, although they are increasing that now.
 The United States, which was relatively slow in getting its testing programme under way, has now significantly ramped this up across the country.
 Other steps have included shutting down venues where large numbers of people gather, for example closing schools and educational institutes.
 The UN estimates that about 87% of those enrolled in education around the world have been affected by school and college closures.
 The UN's educational, scientific and cultural body Unesco says that as of 30 March, more than 180 countries had closed  their schools.
 The coronavirus pandemic has also had a major impact on the sporting calendar, as countries have sought to limit mass gatherings.
 The Tokyo 2020 Olympic and Paralympic Games have been postponed until next year.
 But there's also been a huge impact on a whole host of other major sporting events, including football, rugby union, Formula 1, tennis, cricket, golf and others.
 You can find a list of events around the world that have been affected here.
 There have also been cancellations of major cultural and religious events around the world, including film festivals, major music events and religious pilgrimages.
 Read more from Reality Check
 Send us your questions
 The US sees 5m more jobless claims as the economy struggles in the coronavirus outbreak.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-51737226