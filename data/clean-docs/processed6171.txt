Taming coronavirus rampage: There is enough stock of essentials | The Daily Star
 The apprehension of a potential lockdown to rein in the coronavirus spread has led to the stockpiling of some essential commodities but businesses said there was nothing to be worried about.
 Stocks of essential items such as rice, flour, edible oil, sugar and pulse are good enough to meet the demand in the coming weeks, thanks to a good domestic harvest of rice and arrival of import-based commodities in the last three months, according to import and production data.  
 The disclosure comes as Bangladesh is set to go for a shutdown for 10 days from tomorrow to slow the outbreak of COVID-19 that has infected 39 and killed four until yesterday in the country.
 Businesses said there would be no shortage of essential commodities in the market if the government allows uninterrupted movement of vehicles of essential foodstuff and other daily necessities and ensure port activities to release imported goods.
 "But there will be an extra pressure on the stock if panic buying seen in the last few days continues," said Abul Bashar Chowdhury, chairman of BSM Group, a commodity importer based in Chattogram.
 The stock of edible oil, sugar, pulse and wheat is sufficient, but more will arrive soon in the market, he said.
 Amid the panic buying, the retail prices of all types of rice, flour, soybean oil, lentil, onion, garlic, ginger, egg and potato increased in Dhaka over the last one week, according to the Trading Corporation of Bangladesh. 
 "We have to ensure that the supply chain between the ports and the processing units or the market remains uninterrupted. Otherwise, there might be a problem," he said.
 Bangladesh's private and public sectors imported wheat of 47 lakh tonnes between July 1 last year and February 13 this year, a 36 per cent jump year-on-year, according to import data from the food ministry.
 It is the same for rice: farmers had a good harvest in the last two seasons of aus and aman in the November-December period.
 Millers said the prices of fine rice edged up recently because of a depleting stock as the most popular variety, known as Miniket, was grown during the boro rice season and harvested in April-May last year.
 Save for this, the other varieties of the grain are available to meet the country's demand until the next harvest that will begin next month.
 Several factors contributed to the price spiral of the cereal: the recent buying spree, stockpiling by many consumers, and the government's purchase of paddy and rice in the second biggest rice season of aman.
 "There is no deficit of rice. Only it needs to be ensured that people should buy as much as they need," said KM Layek Ali, general secretary of the Bangladesh Auto, Major and Husking Mills Association.
 Anup Kumar Saha, deputy executive director of consumer brands at ACI, which has four automatic rice mills, echoed Ali.
 "There will be no crisis of rice in the country. The main thing is to ensure the flow of supply without any interruption. Failure to do so will fuel prices," he said.
 ACI also markets flour, pulse, edible oil and salt, and Saha said there is no shortage of supply of the commodities.
 Bangladesh requires 1.35 lakh tonnes of sugar and 1.50 lakh tonnes of edible oil a month, according to a commerce ministry report.
 Businesses imported 2.88 lakh tonnes of sugar, mainly raw, during the December-February period and another 5.60 lakh tonnes are on the way.
 Some 1.47 lakh tonnes of mainly crude soybean oil have been imported and 1.94 lakh tonnes are in the pipeline.
 "We are continuously supplying goods and our production has been on to meet the increased demand. We are running in three shifts in order to keep the supply chain intact," said Asif Iqbal, deputy managing director for FMCG at Meghna Group of Industries, one of the leading importers and processors of commodities.
 Meghna always maintains a stock for two-three months to meet the demand, he said, adding that his company has no dearth of stock for essential commodities.
 "We had a preparation for upcoming Ramadan but it appears that the pressure of demand has come in advance," Iqbal said.
 Traders also shared the same view that the market has enough supply and they will keep their stores open to serve customers.
 "We have seen worldwide that even after lockdown by countries to slow the coronavirus contamination, supermarkets remain open. We are here to serve and help," said Kazi Inam Ahmed, president of the Bangladesh Supermarket Owners' Association, which represents chain retail stores such as Shwapno, Meena Bazar and Agora.
 Sabbir Hasan Nasir, executive director of ACI Logistics that owns the largest supermarket chain Shwapno, said their normal business operations will remain on during the shutdown.
 "We will stand beside people so that the market remains normal," he said, while stressing the need for smooth movement of vehicles to ensure supply of essential commodities.
 However, the availability of truckers would be a problem as many truck drivers are unwilling to go to Dhaka amid fear and a lack of availability of return trips, said Nirod Boron Saha, president of the Naogaon Dhan O Chal Arathdar Babshayee Samity, an association of rice wholesalers and commission agents in the northwest district.
 "There would be no problem of rice supply if transport is available," he added,
 However, Rustom Ali Khan, general secretary of the Bangladesh Truck and Covered Van Owners Association, said there is no shortage of trucks.
 "But many drivers don't want to go on trips for fear of coronavirus infection. We will not close our service. We will remain on during shutdown to transport goods."
 Grameenphone:Type START <space> BR and send SMS it to 22222
 Robi:Type START <space> BR and send SMS it to 2222
 Banglalink:Type START <space> BR and send SMS it to 2225
 https://www.thedailystar.net/business/news/there-enough-stock-essentials-1885552
