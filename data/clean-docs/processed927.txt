Texas' governor declares state of disaster from coronavirus - The Monitor
 DALLAS — Texas’ governor declared a state of disaster Friday as the coronavirus pandemic spread to all of the state’s biggest cities.
 The declaration came as the number of COVID-19 cases climbed and reached into San Antonio and Austin, where illnesses were reported for the first time Friday.
 Republican Gov. Greg Abbott said that drive-thru testing for people, including first responders and high-risk patients, will begin in the state, with the first location being in San Antonio.
 Abbott said Texas has 39 confirmed cases of COVID-19, and 220 people have been tested so far.
 The coronavirus that causes COVID-19 was declared a pandemic by the World Health Organization on Wednesday. It causes only mild or moderate symptoms for most people, but can cause more severe illness, including pneumonia, in older adults and people with existing health problems. The vast majority of people recover from COVID-19 within weeks.
 Texas’ biggest cities had already been declaring local disasters on their own.
 On Friday, San Antonio banned large public gatherings and postponed its giant Fiesta celebration until November.
 That followed Dallas Mayor Eric Johnson issuing a proclamation late Thursday declaring a local state of disaster for the city of 1.3 million people, shortly after a countywide ban on large public gatherings of 500 or more people was announced.
 Dallas County Judge Clay Jenkins said smaller gatherings of 250 people should be canceled or rescheduled. He said schools, office towers, airports and movie theaters are exempt, the Dallas Morning News reported.
 “I know Dallas County is up to the challenge,” Jenkins said. “Use your brains, as we’re all very good at doing in this community. … I want everyone to soberly consider and take responsibility for your life decisions.”
 Also late Thursday, Dallas County announced five more cases of the illness, including one that was being investigated as community-spread.
 Abbott has resisted calling for a statewide ban on large gatherings.
 San Antonio’s Fiesta was set to be held in April. The president of the Fiesta San Antonio Commission, Jeanie Travis, said the celebration will be rescheduled for Nov. 5-15. On Friday morning, Mayor Ron Nirenberg issued an emergency declaration banning gatherings of 500 people or more for the city of about 1.5 million. It was issued after the first travel-related case of COVID-19 was detected in the city.
 Fiesta’s website says it has an economic impact of more than $340 million with about 2.5 million attendees.
 In Austin, local health officials said early Friday that the first two cases of the coronavirus have been confirmed in the area.
 Austin health officials said the two patients are a man in his 60s who is hospitalized in critical condition and a woman in her 30s who is quarantined at home. The man had already been ill when he was transferred from a rural area elsewhere in the state. The woman was connected to another previously diagnosed case in the Houston area.
 Officials said the two cases are not connected and they are working to reach anyone they may have been in contact with, said Dr. Mark Escott, interim health authority for Austin.
 “These do not represent community spread. They are linked to another case or jurisdiction,” Escott said. “We expect to see more cases, we expect there to be some element of community, person-to-person spread.”
 Austin Mayor Steve Adler said the city had been preparing for the virus to arrive and had taken advance steps to mitigate, most notably canceling the South by Southwest music, movie and tech festivals that bring more than 400,000 people to Austin. The city canceled schools Friday ahead of next week’s spring break.
 “This is a virus that we have known was coming to us. It has no come to us,” Adler said. “Let’s be calm and deal with what’s in front of us.”
 https://www.themonitor.com/2020/03/13/texas-governor-declares-state-disaster-coronavirus-pandemic-spreads-states-big-cities/
