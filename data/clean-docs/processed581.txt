Coronavirus: More newly-hired staff will get paid - BBC News
 More than 200,000 more employees could now be furloughed following changes to the government scheme to help pay people's wages.
 The Coronavirus Job Retention Scheme, which covers 80% of workers' pay, will take applications from Monday from companies which have laid off workers. 
 Initially, it only supported those already employed on February 28. The cut-off date is now to 19 March.
 However, many recently employed workers will still miss out.
 Workers need to have been on the payroll by 19 March - the day before the scheme was first announced. This will not cover people who were not put on the PAYE system until later in the month.  
 Employers would need to have notified HM Revenue and Customs that a new employee was on the payroll. This is done through the Real Time Information (RTI) system which updates the tax authority when someone is paid.
 So somebody paid late in March is unlikely to be covered by their current employer. 
 However, the Treasury wants to guard against businesses hiring "ghost" employees to fraudulently claim furlough payments.
 The plight of new starters has prompted a campaign for them to be included in the furlough scheme by unions, opposition parties, and the workers themselves.
 HMRC has promised to release wages for furloughed workers by the end of April. The scheme currently runs until 1 June.
 But there are fears firms could start to cut staff unless the government soon clarifies whether the scheme will be extended. 
 The Confederation of British Industry (CBI) said it is worried companies will be forced to start redundancy procedures this Saturday to comply with the minimum 45-day consultation period.
 CBI director general Carolyn Fairbairn, said: "We are very concerned that businesses will be forced into a position potentially of having to make people permanently redundant."
 The Treasury said the scheme, which pays wages for March, April and May, could run for longer.
 "The scheme is open for an initial three months and we hope conditions will improve sufficiently during this period. However, the Chancellor has been clear he will review extending it for longer if necessary," said a Treasury spokesperson.
 But Ms Fairbairn said businesses need clarity from the government before 18 April: "What we are saying to government is that firms need to be able to plan. 
 "These are massive decisions being taken on a day-to-day basis that affect people's lives and livelihoods, and having that clarity of a 45-day notice period for business is absolutely vital."
 HMRC chief executive Jim Harra told the BBC's Today programme that the chancellor Rishi Sunak "has been clear that if it needs to be extended then he will look to do that". 
 Mr Harra added that the system through which companies can claim funding to pay their furloughed workers will be accessible from Monday.
 He said he was "confident" employers will get the money in time to pay people by the end of the month.
 "Most employers run their payroll on the last banking day of the month which would be 30 April and there is time to get your claims in in time and to get money before then," he said.
 Some people who changed jobs around this time have found themselves without any income. 
 Felicity Williams, age 30, handed in her notice at the Richmond-on-Thames estate agency where she worked on 27 February, with her last day set for 28 March.
 "Obviously between those two dates it became apparent that the coronavirus was going to shut things down and there would be some difficulties with me starting my new job on 1 April," she said.
 Although government guidelines state that Ms Williams can go back to her previous employer and ask them to furlough her, she said the company is unwilling to help.
 "I've been to them four times now and pleaded with them to re-employ me and put me on furlough, just so I've got some sort of income coming in, and every time it has been a no," she said.
 Ms Williams said she is also unable to claim universal credit because she lives with her boyfriend, who has savings and an income.
 She said: "I have my own bills, I have my own credit cards, my own loans that I need to pay off, and obviously I've frozen them for the short term. But it is not going to help me out in terms of paying rent and bills and food."
 Mr Harra said: "I think in all of these schemes designed to help the economy, we've had to design them so they can be implemented very quickly and time, in some senses, has been the enemy of perfection.
 "But there are a whole range of schemes available to help businesses and people and I'm confident that the vast majority of employees who have been furloughed will get help."
 The Commission president says Italy was let down by "too many" at the start of the outbreak.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/business-52279455
