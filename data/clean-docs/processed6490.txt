Coronavirus in N.Y.C.: Latest Updates - The New York Times
 New York Today
 It’s Monday. 
 Weather:  Cloudy, then breaks of sun, with a high of about 60. 
 Alternate-side parking: Suspended through April 28. Meters are in effect. 
 Mayor Bill de Blasio on Sunday continued to call on President Trump to direct desperately needed federal funding to American cities, and to criticize the president’s silence on the matter.
 “How on earth do you not have an opinion on aid to American cities and states?” Mr. de Blasio said during his daily press briefing.
 He compared Mr. Trump’s lack of response to the financial shortfall facing New York City in particular to President Gerald Ford’s dismissal of the city during the fiscal crisis of the 1970s.
 “There was that famous Daily News cover that said ‘Ford to City: Drop Dead,’” Mr. de Blasio said. “So my question is, Mr. Trump, Mr. President, are you going to save New York City or are you telling New York City to drop dead? Which one is it?”
 [Get the latest news and updates on the coronavirus in the New York region.]
 Gov. Andrew M. Cuomo said Sunday that data indicated that New York was “past the high point” of the coronavirus outbreak.
 “If this trend holds, we are past the high point, and all indications at this point are that we are on a descent,” he said, adding, “We are on the other side of the plateau, and the numbers are coming down.”
 The governor asked that New Yorkers remain vigilant, pointing out that 1,300 people had been hospitalized for the virus the previous day.
 [Coronavirus in New York: A map and case count.]
 During a news conference on Sunday evening, President Trump thanked Mr. Cuomo, saying that their partnership had been effective, including in extending the city’s hospital capacity.
 “We built a little bit more than we needed — that’s good,” the president said. “As opposed to building a little bit less — that’s not good.”
 Mr. Trump also played a two-minute clip from Mr. Cuomo’s Sunday news conference in which the governor praised the coordination between federal and state governments in response to the outbreak.
 ‘The Worst Case Scenario’: New York’s Subway Faces Its Biggest Crisis
 11 Numbers That Show How the Coronavirus Has Changed N.Y.C.
 Inside the Nursing Home Where 70 Died and Body Bags Piled Up
 He Went to 3 Hospitals. When He Finally Got a Bed, It Was Too Late.
 Want more news? Check out our full coverage.
 The Mini Crossword: Here is today’s puzzle.
 Local college students are using 3-D printers to make face shields. [Bronx Times]
 Here is an inside look at how health care workers are responding to the outbreak on Long Island. [Newsday]
 How did a dead man in Queens get nominated for a position with the local Democratic County Committee? [Queens Chronicle]
 Antonio de Luca, Sasha Portis and Adriana Ramić write:
 Windows are often described as the eyes of a building. They are a symbol of pondering, an aperture through which we can experience the world outside while remaining inside — an important feature now that millions of New Yorkers have had to move their lives indoors.
 We reached out to 17 illustrators and artists sheltering in place in neighborhoods across the city and asked them to draw what they see out of their windows — to show us what it feels like to be in New York at this rare moment in time.
 “For three weeks, I have not seen anything move,” said Yuko Shimizu, an artist in Morningside Heights, Manhattan, who drew the scene above.
 See all 17 illustrations here.
 It’s Monday — what’s outside your window?
 Dear Diary:
 It was 1978, and I was walking uptown on a snow-covered part of lower First Avenue. Local merchants had shoveled a one-person-wide path in the middle of the sidewalk.
 A young man and woman were walking toward me. As they got closer, we all realized we would need to do a little dance to pass one another while staying on the path.
 They went single file with the woman in front. We all turned sideways to create uptown and downtown “lanes” for ourselves to the degree possible.
 Just before the woman passed me, she turned back toward her companion.
 “By the way,” she said, “when do you want to get married?”
 — Gary Maciag
 Updated April 11, 2020
 This is a difficult question, because a lot depends on how well the virus is contained. A better question might be: “How will we know when to reopen the country?” In an American Enterprise Institute report, Scott Gottlieb, Caitlin Rivers, Mark B. McClellan, Lauren Silvis and Crystal Watson staked out four goal posts for recovery: Hospitals in the state must be able to safely treat all patients requiring hospitalization, without resorting to crisis standards of care; the state needs to be able to at least test everyone who has symptoms; the state is able to conduct monitoring of confirmed cases and contacts; and there must be a sustained reduction in cases for at least 14 days.
 The Times Neediest Cases Fund has started a special campaign to help those who have been affected, which accepts donations here. Charity Navigator, which evaluates charities using a numbers-based system, has a running list of nonprofits working in communities affected by the outbreak. You can give blood through the American Red Cross, and World Central Kitchen has stepped in to distribute meals in major cities. More than 30,000 coronavirus-related GoFundMe fund-raisers have started in the past few weeks. (The sheer number of fund-raisers means more of them are likely to fail to meet their goal, though.)
 If you’ve been exposed to the coronavirus or think you have, and have a fever or symptoms like a cough or difficulty breathing, call a doctor. They should give you advice on whether you should be tested, how to get tested, and how to seek medical treatment without potentially infecting or exposing others.
 The C.D.C. has recommended that all Americans wear cloth masks if they go out in public. This is a shift in federal guidance reflecting new concerns that the coronavirus is being spread by infected people who have no symptoms. Until now, the C.D.C., like the W.H.O., has advised that ordinary people don’t need to wear masks unless they are sick and coughing. Part of the reason was to preserve medical-grade masks for health care workers who desperately need them at a time when they are in continuously short supply. Masks don’t replace hand washing and social distancing.
 If you’re sick and you think you’ve been exposed to the new coronavirus, the C.D.C. recommends that you call your healthcare provider and explain your symptoms and fears. They will decide if you need to be tested. Keep in mind that there’s a chance — because of a lack of testing kits or because you’re asymptomatic, for instance — you won’t be able to get tested.
 It seems to spread very easily from person to person, especially in homes, hospitals and other confined spaces. The pathogen can be carried on tiny respiratory droplets that fall as they are coughed or sneezed out. It may also be transmitted when we touch a contaminated surface and then touch our face.
 No. Clinical trials are underway in the United States, China and Europe. But American officials and pharmaceutical executives have said that a vaccine remains at least 12 to 18 months away.
 Unlike the flu, there is no known treatment or vaccine, and little is known about this particular virus so far. It seems to be more lethal than the flu, but the numbers are still uncertain. And it hits the elderly and those with underlying conditions — not just those with respiratory diseases — particularly hard.
 If the family member doesn’t need hospitalization and can be cared for at home, you should help him or her with basic needs and monitor the symptoms, while also keeping as much distance as possible, according to guidelines issued by the C.D.C. If there’s space, the sick family member should stay in a separate room and use a separate bathroom. If masks are available, both the sick person and the caregiver should wear them when the caregiver enters the room. Make sure not to share any dishes or other household items and to regularly clean surfaces like counters, doorknobs, toilets and tables. Don’t forget to wash your hands frequently.
 Plan two weeks of meals if possible. But people should not hoard food or supplies. Despite the empty shelves, the supply chain remains strong. And remember to wipe the handle of the grocery cart with a disinfecting wipe and wash your hands as soon as you get home.
 Yes, but make sure you keep six feet of distance between you and people who don’t live in your home. Even if you just hang out in a park, rather than go for a jog or a walk, getting some fresh air, and hopefully sunshine, is a good idea.
 That’s not a good idea. Even if you’re retired, having a balanced portfolio of stocks and bonds so that your money keeps up with inflation, or even grows, makes sense. But retirees may want to think about having enough cash set aside for a year’s worth of living expenses and big payments needed over the next five years.
 Watching your balance go up and down can be scary. You may be wondering if you should decrease your contributions — don’t! If your employer matches any part of your contributions, make sure you’re at least saving as much as you can to get that “free money.”
 https://www.nytimes.com/2020/04/20/nyregion/coronavirus-nyc.html
