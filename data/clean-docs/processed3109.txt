Coronavirus update: Stock shortage and travel curbs hit traders of vegetables, grains in Delhi - delhi news - Hindustan Times
 					Apr 14, 2020-Tuesday -°C
 							Humidity  -
 							Wind  -
 Metro cities -  Delhi, Mumbai,
 					Chennai, 
 					Kolkata
 Other cities -  Noida, Gurgaon,
 					Bengaluru, Hyderabad, Bhopal
 					, Chandigarh , Dehradun,
 					Indore,
 					Jaipur,
 					Lucknow, Patna,
 					Ranchi
 					Powered by 
 Wholesale traders of perishable goods, such as vegetables fruits and grains, have been facing several problems over the last four days, in the absence of proper supply and the need to dispose of their stock.
 Traders said that trucks with daily supply are not being allowed to enter the borders and even if a few managed to enter, the drivers were stopped and asked to return from the markets. The shortage of stock also resulted in higher prices in a few retail markets.
 With the authorities taking stock of the situation, the supply of these essential items is likely to be restored from Friday, said traders. The Delhi government on Thursday said that they had started distributing curfew passes to traders and workers in wholesale markets to ensure that the supply of essential items is not affected.
 Traders at the Azadpur Mandi on Thursday said that while retail prices of many vegetables had shot up because of an increase in demand in the wholesale markets, they are forced to sell their stock for ₹10-20 less, as transport was restricted and shelf life of three to four days for most fruits and vegetables. “The authorities have only started distributing the passes today (Thursday). On Sunday, I was forced to sell eggplant for ₹3 a kilogram, which is priced for wholesale at ₹16 per kilogram,” said Raghuvar Das, a vegetable dealer in Azadpur Mandi.
 Before the lockdown, nearly 70 trucks would go around the city every day to distribute around 350 tonnes of onions from Azadpur Mandi. Over the last few days, however, fewer than 20 trucks have managed to reach the wholesale market to pick up stocks. Not just local distribution, traders also complained that trucks bringing in stocks from other states were being stopped at the borders till Wednesday night.
 In Azadpur Mandi alone, there are 438 big shops and 1,000 small shops. In all, there are at least 10,000 people working in this market. 
 Anil Malhotra, a member of the Agricultural Produce Marketing Committee (APMC) in Azadpur, said that although the trade of potatoes and onions, which have a long storage life, has not been affected much, traders selling vegetables and fruits, such as tomatoes, okra and eggplants, are suffering. He said that the business of fruit traders has also been severely hit and there is likely to be a shortage of supply in the coming days. “Many retailers are taking advantage of the fact that there is a disruption in the supply chain and goods are not being able to reach markets. As of Thursday, the Azadpur Mandi has no supply of pomegranate, grapes and certain varieties of oranges. The stock of ginger was also finished today (Thursday),” Malhotra said.
 He warned that the supply of tomatoes will also take a blow in the longer run, as the crates, which are used by farmers to sell fresh supplies to wholesalers, are not being returned. “By itself, empty crates do not come under essential items, but it makes for an important link in the supply chain. Traders are also ordering less and are not giving credit to retailers and other buyers, which will eventually affect business,” he said. 
 To avoid such a situation and ensure that the balance between demand and supply of the city is maintained, representatives of the Delhi government said that overall supply that reaches Azadpur Mandi is not being sent to other states now.
 Adil Ahmad Khan, the chairman, Azadpur APMC, said, “Looking at the present situation, we are only allowing quantity that is needed to meet Delhi’s. Normally, Azadpur Mandi used to receive around 700 tonnes of potatoes from UP, Punjab and Haryana, but not all is consumed in Delhi. Delhi’s demand is around 400 tonnes, while the rest is sent to other states,” Khan said.
 According to data provided by the APMC, nearly 350 tonnes of tomatoes, 302 tonnes of potatoes, 350 tonnes of onions, 82 tonnes of cauliflower and cabbage, 70 tonnes of orange and 313 tonnes of apple were supplied at the Azadpur Mandi on Thursday. The government data shows that the supply was slightly affected on Tuesday and Wednesday.
 Retailers are also apprehensive of a sudden spike in prices of vegetables and grains, unless the government imposes strict caps. “Some local shopkeepers might be taking advantage of the shortage to sell at higher rates, but if this shortage of supply continues in retail markets, the prices will automatically start going up,” said Vishukant Tripathy, owner of a chain of local supermarkets in southwest Delhi’s Dwarka.
 Naresh Gupta, a member of Delhi Grain Merchant Association, assured that there is enough stock in the city’s godowns to cater to the residents’ demands of wheat and rice for at least a fortnight, if supply is completely hit.
 There are three main grain mandis — at Lawrence Road, Narela and Najafgarh — which receive stock from parts of the city and others states. Besides this, grains are also separately procured by big companies, flour mills and rice mills.
 Gupta accepted that the grain distribution in the city was hit briefly, but maintained that the supply chain was “not yet disrupted”. He said that the number of vehicles bringing wheat, rice and pulses has gone down by around 20%. “There was initially some problem in goods supply when the lockdown was announced as the transporters were confused whether they would be allowed to enter the city. But the is no need to panic as Delhi has enough stock to meet the city’s demands, even when there is no daily supply for over a fortnight,” he said.
 https://www.hindustantimes.com/delhi-news/traders-of-perishables-faced-with-stock-shortage-transport-problems/story-QE5HAldeot02VMgvNo7NSM.html
