Vox - Understand the News
 By choosing I Accept, you consent to our use of cookies and other tracking technologies.
 The proposal also includes money for hospitals and testing.
 Without the Test Kitchen, the magazine’s stars are taking us into their homes.
 Here’s what we know about why the coronavirus took off in New York and Michigan but not in Ohio and Florida (so far).
 "I don’t know what ‘flattening the curve’ really means in this part of the world," one expert told Vox.
 Calculating the rate at which the virus spreads can help show how well stay-at-home measures are working.
 Most outdoor spaces will open right away, and some businesses will need to maintain social distancing guidelines to operate.
 Trump has already restricted immigration during the coronavirus pandemic. Now he wants to go a step further.
 Two once-in-a-lifetime crises have had devastating consequences for millennials.
 Filed under: 
 How to navigate society’s new normal.
 
 Get our newsletter in your inbox twice a week.
 Get our newsletter in your inbox once a week.
 https://www.vox.com/