The Maddowblog - The Rachel Maddow Show | NBC News
 From The Rachel Maddow Show
 Team Trump's inability unveil a new task force is emblematic of the White House's limitless capacity for dysfunction.
 Steve BenenApril 16, 2020 / 2:40 PM UTC
 The research exploring possible COVID-19 treatments are a chaotic mess in need of centralized, federal coordination.
 Steve BenenApril 16, 2020 / 2:00 PM UTC
 Trump wants to appear powerful, unaware of the extent to which these efforts backfire when he discovers that he doesn't have the authority he wishes he had
 Steve BenenApril 16, 2020 / 1:20 PM UTC
 The total from the newest report is a little lower than the last few weeks, but that just means things are getting worse slightly slower.
 Steve BenenApril 16, 2020 / 12:43 PM UTC
 To borrow the phrasing of a nine-year-old TV commercial, it's time for Trump to see the response to the pandemic as a project of national significance.
 Steve BenenApril 16, 2020 / 12:00 PM UTC
 Today's edition of quick hits.
 Steve BenenApril 15, 2020 / 9:30 PM UTC
 The White House was supposed to launch its "Opening Our Country Task Force" yesterday. Instead it was apparently replaced with something... odd.
 Steve BenenApril 15, 2020 / 4:50 PM UTC
 Today's installment of campaign-related news items from across the country.
 Steve BenenApril 15, 2020 / 4:00 PM UTC
 Big Oil and Gas Versus Democracy—Winner Take All: Rachel Maddow's Blowout offers a dark, serpentine, riveting tour of the unimaginably lucrative and corrupt oil-and-gas industry.
 "You've got to put your name on stuff," Trump said two years ago. He's clearly embraced this principle with a bit too much enthusiasm.
 Steve BenenApril 15, 2020 / 12:00 PM UTC
 Steve BenenApril 14, 2020 / 9:30 PM UTC
 Every stay-at-home order makes exceptions for "essential businesses," though Florida appears to have pushed the issue in a direction that's tough to defend
 Steve BenenApril 14, 2020 / 6:57 PM UTC
 Obama released a video this morning endorsing his former vice president, but he also had a few things to get off his chest about Trump's Republican Party.
 Steve BenenApril 14, 2020 / 6:13 PM UTC
 The Trump campaign tried to highlight actions the president took in February to address the coronavirus crisis. It didn't go especially well.
 Steve BenenApril 14, 2020 / 4:48 PM UTC
 Steve BenenApril 14, 2020 / 4:00 PM UTC
 Governors, like Zelensky, are in a vulnerable position, are heavily dependent on White House support, and have an incentive to stay in Trump's good graces.
 Steve BenenApril 14, 2020 / 2:55 PM UTC
 The Trump campaign threatened to sue stations that aired a campaign ad that quotes Trump. Evidently, it wasn't an idle threat.
 Steve BenenApril 14, 2020 / 2:09 PM UTC
 Team Trump put together a video intended to show a "timeline of action" at the White House. They failed to notice the important month-long gap.
 Steve BenenApril 14, 2020 / 1:20 PM UTC
 Republicans effectively dared voters to put themselves at risk. The party apparently didn't realize that quite a few voters were willing to do exactly that
 Steve BenenApril 14, 2020 / 12:40 PM UTC
 Last month, Trump said, "I don't take responsibility at all." Now, he's claiming "total" authority over all pandemic decision making.
 Steve BenenApril 14, 2020 / 12:00 PM UTC
 Steve BenenApril 13, 2020 / 9:30 PM UTC
 Trump said Obama's response to H1N1 was a "debacle" and a "disaster" because 17,000 Americans died. That standard has come back to haunt him.
 Steve BenenApril 13, 2020 / 6:26 PM UTC
 Nearly four years after Donald Trump declared, "I alone can fix it," he's deliberately looking to governors to lead in his stead.
 Steve BenenApril 13, 2020 / 1:20 PM UTC
 As federal policymakers ponder a series of economic rescues, the USPS will obviously be among the key beneficiaries, right? Wrong.
 Steve BenenApril 13, 2020 / 12:40 PM UTC
 Americans need a better and more competent federal response to the crisis. There's no reason to think a fourth task force will make that happen.
 Steve BenenApril 13, 2020 / 12:00 PM UTC
 The handiest thing so far to figure out what parts of the country ought to be freaking out more than they are.
 April 11, 2020 / 12:44 AM UTC
 Steve BenenApril 10, 2020 / 9:30 PM UTC
 When it comes to talk of re-opening society, experts and Trump have very different ideas in mind.
 Steve BenenApril 10, 2020 / 7:17 PM UTC
 I care that the attorney general is wrong about the Russia scandal probe, but I care more about what Barr intends to do about his misguided assumptions.
 Steve BenenApril 10, 2020 / 6:41 PM UTC
 It's the Friday before a holiday weekend. I shudder to think what kind of "news dump" Trump may have in mind.
 Steve BenenApril 10, 2020 / 4:37 PM UTC
 Read Maddowblog posts from previous weeks
 https://www.msnbc.com/maddowblog