Coronavirus, Joe Biden, Easter: Your Weekend Briefing - The New York Times
 Here’s what you need to know about the week’s top stories.
 By Remy Tumin and Sandra Stevenson
 Here are the week’s top stories, and a look ahead.
 1. The U.S. has suffered the largest number of coronavirus deaths in the world. The country’s toll has climbed to more than 20,000, and as many as 2,000 people die every day. Here’s the latest.
 Starting at the beginning of January, an array of figures inside President Trump’s government — from top White House advisers to experts deep in the cabinet departments and intelligence agencies — identified the threat, sounded alarms and made clear the need for aggressive action.
 But the president was slow to absorb the scale of the virus’s threat and to act accordingly, focusing instead on controlling the message and protecting the economy.
 Here are five takeaways from what Mr. Trump knew as the virus spread. 
 Have you been keeping up with the headlines? Test your knowledge with our news quiz. And here’s the front page of our Sunday paper, the Sunday Review from Opinion and our crossword puzzles.
 Access to much of our coronavirus coverage is free, as is our Coronavirus Briefing newsletter. If you can, please consider supporting our journalism with a subscription.
 2. When will the country reopen?
 The answer, President Trump said, will be “the biggest decision I’ve ever had to make.” Bankers and corporate executives are pleading with him to reopen the economy, while medical experts beg for more time to curb the coronavirus.
 On Friday, Mr. Trump suggested he wanted to reopen soon, but he also promised to listen to public health officials. In actuality, the choice is not entirely his to make.
 Restarting America will put human lives at risk. The Times Magazine talked to five experts on the ethics of the decision.
 3. Nearly 2,000 people have died as the coronavirus ravages nursing homes in the New York region.
 Facilities knew that frail and aging residents were especially vulnerable to the outbreak. But with poor access to testing and protective equipment for workers, they were unable to stop it. The Brooklyn nursing home pictured above had to create a makeshift morgue.
 Overall, deaths in New York, New Jersey and Connecticut have risen past 10,000. Here’s the latest from the region.
 4. “I said the prayer for the dead, and then I changed my gloves and continued my round.”
 Doctors and nurses on the front lines have become symbols of sacrifice in Italy. But priests and nuns have also joined the fight. The virus has killed nearly 100 priests across the country, many of them older and especially vulnerable.
 Pope Francis, above on Sunday in Rome, has called them “the saints next door, priests who gave their lives in service.”
 Hospital chaplains are no strangers to death, illness and tragedy. But chaplains across the U.S. are now carrying more of their own grief and fear.
 5. With restaurants, hotels and schools closed, many of the nation’s largest farms are facing a ghastly effect of the pandemic: plunging sales, leading to staggering amounts of food waste. Above, a field of onions in Idaho that would have gone to market were waiting to be buried instead.
 Even as retailers see spikes in food sales to homebound Americans, farmers are being forced to destroy tens of millions of pounds of perishable food they can’t find buyers for. Some surplus has gone to stunningly needy food banks, but there is only so much fresh food that charities can store.
 6. Bernie Sanders dropped out of the presidential race this week, leaving Joe Biden the presumptive Democratic nominee for the general election.
 “I cannot in good conscience continue to mount a campaign that cannot win and which would interfere with the important work required of all of us in this difficult hour,” Mr. Sanders said, adding, “While this campaign is coming to an end, our movement is not.”
 We examined a sexual assault allegation against Mr Biden by Tara Reade, a former Senate aide. She has accused Mr. Biden of assaulting her in 1993 and says she told others about it. A Biden spokeswoman said the allegation is false, and former Senate office staff members did not recall such an incident.
 7. Easter looks different.
 Religious gatherings like Easter and Passover are not the only rituals affected. Spring — the season of rebirth — is unfolding without baseball, family reunions, bluebonnet viewings and more.
 New York City’s annual Easter Parade won’t happen this year. We can still celebrate what it represents in absentia thanks to the photo archive of the late Times photographer Bill Cunningham. Take a peek.
 8. 100 m.p.h., nearly 3,000 miles, less than 27 hours.
 The nation’s epically uncrowded highway system allowed a white 2019 Audi A8 L sedan with extra fuel tanks to leave the New York City garage above and arrive in Los Angeles less than 27 hours later, a record for the sprint known as the Cannonball Run.
 Traffic levels have dropped more than 90 percent in some major cities, and at least 50 percent nearly everywhere. 
 “It’s amazing how fun it is to drive on the highways right now,” said a former Cannonball Run record-holder. “It’s like having an American autobahn.”
 Caution: Here’s our guide to the current travel rules across the 48 contiguous states.
 9. No milk? No problem. You probably have something in your kitchen that will work.
 If you’re missing an ingredient, we came up with a list of alternatives. They may not work in every case, but if you consider the ingredient’s texture, flavor and cook time, and make decisions according to taste, your options expand.
 Have you buckled to the sourdough bread craze yet? 
 Some starters never die — they just get filed away at the world’s only sourdough library in the village of St. Vith, Belgium.
 10. And finally, dig into one of our Best Weekend Reads.
 The enduring appeal of Weird Al Yankovic, in praise of quarantine clapping, inside a jigsaw puzzle factory and more great stories top this week’s lineup.
 For more ideas on what to read, watch and listen to, may we suggest these nine new books our editors liked, a glance at the latest small-screen recommendations from Watching, and our music critics’ latest playlist.
 Hope you find something to cheer about this week.
 Your Weekend Briefing is published Sundays at 6 a.m. Eastern.
 What did you like? What do you want to see here? Let us know at briefing@nytimes.com.
 https://www.nytimes.com/2020/04/12/briefing/coronavirus-joe-biden-easter.html
