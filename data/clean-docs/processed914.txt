Opinion: What can we learn from the coronavirus pandemic? | Building Design + Construction
 
        
 Sponsored
 Hot Topics: 
 	The coronavirus pandemic will soon end, soon be in the rear-view mirror, but we can still take lessons learned as directions for going forward.
 	Jerry Yudelson, LEED Fellow
 By 2030, experts are almost certain that climate change’s effects will begin to resemble a continuous pandemic, threatening daily life in our cities and suburbs and on our farms–droughts, floods, fires, heat waves, and tropical diseases spreading north. Photo: Pixabay
    
 	“Never let a good crisis go to waste” – Rahm Emanuel
 	It’s hard to imagine anyone who hasn’t had some significant loss during the current crisis. Perhaps you’ve had the virus; worse still, it might have killed someone among your family or friends. Almost all of us have lost, for some period yet unknown, significant parts of our life savings, and some have lost businesses they may have just started or have invested considerable resources in establishing. 
 	What have we learned so far from the coronavirus pandemic? It will soon end, soon be in the rear-view mirror, but we can still take lessons learned as directions for going forward. Even in the most fire-ravaged forest, new seeds begin sprouting almost immediately after the first rain.
 	 
 	First, as individuals, we’ve already learned that can get by with much less than we are used to. Maybe this crisis might be a wake-up call to simplify our lives, to focus on what really matters, which will strengthen our ability to respond in the future.
 	Second, few of us are really prepared for the intellectual, emotional and spiritual challenges of dealing with ramifications of the Covid-19 virus and its impact on our lives. The social and economic crisis we are living through may lead us to think more deeply about how well we are living our values and how we might explore more inner-directed practices such as meditation and mindfulness for a more satisfying life.
 	Third, despite shortages of essential items and long lines at stores, we are far more resilient as a society than we might have expected. Resiliency is a primary response that we will need for dealing with accelerating climate change in the decade ahead, so we can take some comfort from how we all have responded.
 	That’s the good news for us as a society, but what lessons can we, as design, construction and operations professionals, take into our work?
 	By 2030, experts are almost certain that climate change’s effects will begin to resemble a continuous pandemic, threatening daily life in our cities and suburbs and on our farms – droughts, floods, fires, heat waves, tropical diseases spreading north, etc. 
 	To avoid the worst effect of climate change, UN experts have sketched out a “carbon emissions budget” that we shouldn’t exceed over the next decade to have a chance to keep global warming below 1.5C and 2.0C. 
 	Based on current trajectories, including a massive expansion of coal power in China and most of Asia, it’s difficult to see how we can avoid using up the world’s entire “carbon emissions budget” long before the end of the decade. Just as “what happens in Vegas stays in Vegas,” what we put into the atmosphere stays there for a LONG time, hundreds of years into the future.
 	But experts such as architect Edward Mazria have shown us how to change this trajectory, with building design and construction playing a key role.
 	Three things we can learn from the Coronavirus pandemic that we must put into practice immediately:
 2. More importantly, materials choices matter even more than the energy-use profile, especially in the next decade; zero embodied carbon will be the next design standard. The good news: we have the information systems in place to make this goal attainable in practice.
 3. Fully healthy commercial and public buildings, schools and colleges, retail, and hospitals will emerge as the new owner's requirement. Zero net energy is easy; zero net carbon is harder, but we can do it; healthy buildings have yet to be built (and operated) on a mass scale. 
 	The task of renovating buildings to be simultaneously zero net carbon AND healthy will challenge the design and construction industry as never before. 
 	The good news is that all the effort we've expended over the past twenty years, learning how to do (1) - (3), is now available for immediate implementation. Isn't it time for architects and engineers, builders and owners to abandon a strictly economics-based, "values neutral" professionalism and embrace the task that we really need to do (and for the most part, want to do): play our part in making sustainability in practice into our daily work?
 	If there's anything we as building industry professionals can learn from this close brush with our own mortality and with the country's economic death (and of course it's not over yet), it's that "tomorrow we'll get it right" has to be replaced with "today we did it right."
 Jerry Yudelson is a LEED Fellow (Emeritus) and the author of 14 green building books. His next book, The Godfather of Green: An Eco-Spiritual Memoir, will be published on Earth Day 2020.
 	The SMPS Foundation and Building Design+Construction are studying the impact of the coronavirus...
 Despite the economic downturn from the coronavirus pandemic, the overwhelming majority of renters in professionally managed multifamily housing paid all or part of their rent by mid-April. Image courtesy Pixabay.
 	In its second survey of 11.5 million units of professionally managed apartment units across the country,...
 	First and only quarantine shelter in Boston-area to convert a shuttered hospital for homeless patient occ...
 A project architect at Krueck + Sexton used a 3D printer in his apartment to produce the first batch of face shields requested by AMITA Health Resurrection Medical Center Chicago. Images: Krueck + Sexton
 	The architecture firm Krueck + Sexton is producing 100 shields for a Chicago-area hospital.
 	U.S. Army Corps of Engineers Maj. Gen. R. Mark Toy is briefed by Tarlton construction executives on the site of the alternate care facility. From left: Senior Project Manager Cameron Denison, who managed the night team; Vice President Joe Scarfino, who led the effort and managed the day team; and EVP John Doerr. 
 	Missouri’s first Alternate Care Facility ready for coronavirus patients
 	This Starbucks and an adjacent bank in Bothell, Wash., were opened thanks to a virtual building inspection by the city. The coffeeshop's drive-thru has stayed open through the COVID-19 pandemic.
 	    
 	Bothell, Wash., issues a certificate of occupancy to developer after inspecting the property online. 
 Courtesy Pixabay
 	Association officials urge quick enactment of infrastructure investment, relief for hard-hit firms and pe...
 Photo courtesy Autodesk
 	The outbreak of COVID-19 represents an unprecedented test for the global healthcare system. Managing the...
 The STAAT Mod system provides a fully functioning hospital environment for treating high-acuity patients. Images: HGA
 	A STAAT Mod system can be ready to receive patients in less than a month.
 Typical COVID-19 patient room layout in an alternate care facility. Photo: Henderson Engineers
 	COVID-19: Converting existing unused or underused hospitals, hotels, convention centers, and other altern...
 	SAFTI FIRST has several facilities in Merced, CA dedicated to manufacturing USA-made fire rated glass, framing and doors.  This is a photo of President and CEO Bill O’Keeffe with the supervisors and managers of the different manufacturing facilities.
 VIEW ALL BLOGS
 David Barista
 Editorial Director
 dbarista@sgcmail.com
 847.954-7929
 Robert Cassidy
 Executive Editor
 847.391.1040
 rcassidy@sgcmail.com
 John Caulfield
 Senior Editor
 732.257.6319
 jcaulfield@sgcmail.com
 David Malone
 Associate Editor
 847.391.1057
 dmalone@sgcmail.com
  
 https://www.bdcnetwork.com/opinion-what-can-we-learn-coronavirus-pandemic