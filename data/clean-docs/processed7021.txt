Coronavirus: Hackers posing as WHO officials to steal bank details - Business Insider
 Hackers are exploiting growing anxiety over the coronavirus crisis by posing as officials from the World Health Organisation and government officials in order to steal the personal details of people online.
 Cybercriminals are also posing as officials US' leading public health institute, Centers for Disease Control and Prevention, the report said.
 Politico also reports that US cybersecurity firm FireEye has evidence that groups in Russia, China, and North Korea were exploiting the growing chaos to target national governments like the US, Japan, and Italy.
 There is concern that hackers will find it easier to secure confidential information like banking details in the coming weeks and months, as a large number of people will be working on insecure internet networks in their homes.
 For example, in Brussels, thousands of staff are working from home after the EU's institutions cancelled all non-essential meetings and said only critical staff should work from the bloc's offices.
 In London, the House of Commons has cancelled all events other than parliamentary votes and debates, while many MPs have told their staff to work remotely. 
             Frank Augstei n- WPA Pool/Getty Images
           
 Boris Johnson's UK government has set up a unit for combating disinformation relating to coronavirus. 
 The UK government said last week it had assembled a counter-disinformation unit that would work with social-media companies to "identify and respond" to these and other conspiracy theories spread by foreign powers.
 Posts included claims that the virus was a bid by the US to "wage economic war on China," that it was a bioweapon engineered by the CIA, and that it was fostered "to push anti-China messages."
 The campaign was first spotted in mid-January, with several thousand accounts — many of which were previously tied to Russian activities — posting "near-identical" messages about coronavirus, according to a report produced by the State Department's Global Engagement Center.
 https://www.businessinsider.com/coronavirus-hackers-posing-as-world-health-organisation-offiicals-2020-3
