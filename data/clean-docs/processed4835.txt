Coronavirus in sport LIVE: Premier League LATEST, Olympics 2021 could be CANCELLED, update on UFC ‘Fight Island’ – The US Sun
 PREMIER LEAGUE action remains suspended due to the global coronavirus epidemic and there are fresh concerns over being able to finish the current season.
 The Olympic Games may be cancelled altogether as it has been revealed chiefs are worried Tokyo event will not be able to go ahead even in 2021.
 Follow all the latest news and updates as the sporting world deals with the threat of coronavirus...
 ARSENAL ACES 'REJECT PAY CUT'
 Arsenal stars have rejected pay cuts across the next year in a vote on Monday afternoon.
 The Telegraph claims players were asked to consider taking 12.5 per cent cuts over the next 12 months after football was suspended due to the coronavirus crisis.
 Incentives on new contracts were offered as terms, with any deducted wages being added on top of a fresh salary agreement.
 Arsenal also offered to hand back the deducted money to any player who left for a transfer fee.
 But the terms were unanimously rejected by players, with doubts over the summer transfer window and the long-term financial effects of the coronavirus crisis.
 Arsenal are now left footing an enormous wage bill that runs to £230million each year.
 Players agreeing to the 12.5 per cent cut would have saved Arsenal £25m.
 Chiefs were willing to bring the figure down to 7.5 per cent if Arsenal qualified for the Europa League.
 And there was a full rebate on the cards for Champions League qualification.
 ITALY EYES FAN BAN
 Serie A will reportedly ban fans from attending games until 2021.
 Sport Mediaset claim to have unearthed Italian government documents suggesting Serie A stadiums will remain empty until January 2021 at the earliest.
 It comes after federation president Gabriele Gravina confirmed plans to resume action in May.
 Serie A has been on hold since early March after Italy became the first European country to strongly suffer the effects of coronavirus, and there are fears the campaign could be scrapped.
 But players could start to train next month with the hope of resuming matches shortly afterwards, and some reports even suggests all 20 teams could relocate to Rome for a gladiatorial battle.
 Gravina said: “As soon as the conditions are right, we’ll finish the championship.
 “Soon there will be a meeting. We will establish the procedure which we will then communicate.
 “We will start, I hope, at the beginning of May with tests to ensure that players are negative and training can follow.
 “Will we play through the summer? We don’t have a deadline but the idea is to finish the championships.”
 WELBECK'S £10K AID
 Ex-Arsenal and Manchester United striker Danny Welbeck has donated £10,000 to improve conditions in his mum's hometown in Ghana. reports say.
 The Watford striker, 29, who has won 42 England caps, is said to have paid for food and sanitary relief for residents in Nkawiepanin in the Atwima Nwabiagya municipality of the Ashanti region.   
 SCOTTISH RUGBY PLAYERS' PAY CUT 
 All rugby players in Scotland earning above £50,000 will be asked to take pay cuts of ten to 25 per cent until September due to the coronavirus pandemic.
 Scottish Rugby also plans to furlough around three-quarters of their 450-strong staff, while their chief executive Mark Dodson will lose 30% of his wages.
 The union acted amid  “increasing unpredictability” in world rugby, with other executives taking a 25% reduction.
 NEYM YOUR PRICE
 Neymar and Kylian Mbappe – both linked with Manchester United in the past year – are among seven PSG superstars who could face the exit door after the coronavirus pandemic.
 Ex-Old Trafford winger Angel Di Maria, plus another player in Edinson Cavani who is thought to have interested United are also on the illustrious list as the French champions look to cut costs.
 Marca suggest PSG will need to curb their spending as football anticipates a huge financial hit due to the shutdown for Covid-19.
 Barcelona remain strong favourites to re-sign Brazil hero Neymar, while his strike-partner and French World Cup winner Mbappe is tipped for Real Madrid.
 Meanwhile, Di Maria has just a year left on his deal while defenders Thomas Meunier and Layvin Kurzawa are also named as likely departures. 
  Ex-Vikings QB Tarvaris Jackson dead at 36 following car crash in Alabama
  Football star says drug-crazed gang of young men 'raped and abused' his cow
  Ighalo 'will have to take £200k-a-week pay CUT to join Man Utd on permanently'
  Champions League streaker Wolanski exercises in lockdown in skintight tank top
  Tommy & Molly-Mae join Tyson & Paris for couple workout as AJ blanks call
 https://www.the-sun.com/sport/509745/coronavirus-live-sport-premier-league-olympics-ufc-249/
