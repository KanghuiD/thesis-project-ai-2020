Free Legal Information Available During COVID-19 Pandemic  
 AUSTIN, Texas -- We're all asking questions we never imagined we'd need answered. When it comes to legal advice, itâs important to make sure those answers are coming from experts. 
 LegalShield's Coronavirus Legal Resource Center is essentially a database of legal advice compiled by attorneys across the country, providing answers to every question they've received in the last month or so related to the COVID-19 pandemic. 
 The list of topics they're offering legal advice on includes:
 "âWhere to file for a small business loan?â for example. âWhat to do with expecting the supplemental checks that are coming in from the government?â says LegalShieldâs Tara Paustenbach. âIt's going to be accurate information, because it's coming from an attorney - one that none of us had to hire.
 RELATED: Attorneys Stress Importance of Having Affairs in Order Amid COVID-19 Pandemic 
 âForever, attorneys have cost too much money, and so what most people do is they turn to Google and, unfortunately, we cannot guarantee that every answer we find on Google is going to be actual, correct legal advice,â Paustenbach adds.
 The answers are written in everyday language (none of that legal jargon!) so it's easy to comprehend.
 The database is updated daily, so you know you're getting the most up-to-date information. 
 https://spectrumlocalnews.com/tx/san-antonio/news/2020/04/13/free-legal-advice-available-during-covid-19-pandemic--