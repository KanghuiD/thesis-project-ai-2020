Coronavirus: What's the matter with Arkansas, Iowa, South Carolina?  - Los Angeles Times
 I’ve got some words for the governors of Alabama, Arkansas, Missouri and the eight other states that have yet to adopt statewide stay-at-home orders to stop the spread of the coronavirus pandemic. And they’re not nice words.
 What the #@*& are you waiting for?
 As I was writing this, the confirmed cases of COVID-19 across the globe topped 1 million. Nearly a quarter of them in the United States. Every state has cases. Every. State. 
 So far, 39 U.S. states have followed the recommendations of public health experts and told all their residents to stay at home and shut down nonessential businesses to make it harder for the virus to spread. A few states have localized shutdowns and way too many have no restrictions at all. That’s dangerous for their residents and for all of us. Viruses don’t respect borders. All states need to take the same measures to create an effective barrier against community spread.
 And do so immediately.
 In this pandemic, time matters. There’s mounting evidence that the SARS-CoV-2 virus can be spread by people without symptoms, which makes early intervention important. Waiting until hospitals are overrun is costly and deadly. 
 Those states that took steps early will probably fare better than those that waited until just the last few days to take action. California and Washington were among the first wave, and there are signs that their social distancing measures are working. 
 States that moved slowly, such as Florida and Georgia, might not be so fortunate. It wasn’t until his state had about 8,000 confirmed cases that Florida Gov.  Ron DeSantis on Wednesday finally relented to pressure to issue a statewide order. He had resisted doing so, he said, because President Trump hadn’t told him to. Way to show leadership, governor! Who knows how many people have been or will be hurt by this foot-dragging.
 Georgia Gov. Brian Kemp wasn’t just boneheadly stubborn, his staff went to far as to chastise local governments for issuing their own stay-at-home orders to quell surges in COVID-19 cases. On Wednesday he, too, gave in and ordered statewide social distancing measures. 
 Why had it taken him so long to reach this sensible conclusion? Kemp explained that he didn’t realize until then that people without symptoms could spread the virus. Despite the fact that health officials have been making that very point practically since the day the outbreak reached the United States in January.
 Well, now he knows. And so do the holdout governors of Alabama, Arkansas, Iowa, Missouri, North Dakota, Nebraska, South Carolina, South Dakota, Tennessee, Utah and Wyoming. 
 Stop stalling. Do the right thing. Protect your people and tell them to stay at home.
 Get thought-provoking perspectives with our weekly newsletter.
 https://www.latimes.com/opinion/story/2020-04-02/opinion-arkansas-kansas-south-carolina-shut-down-now
