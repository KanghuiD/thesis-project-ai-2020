The FT View | Financial Times
 										Coronavirus business&nbspupdate
 										Get 30 days complimentary access to our Coronavirus Business Update newsletter
 									
 										30 days complimentary
 Add this topic to your myFT Digest for news straight to your inbox
 Popular funds have survived a period of intense market stress
 Singapore’s second wave of infections and US meat packers offer cautionary tales
 Programmes should be flexible and help workers move to in-demand sectors
 Business cannot expect emergency cash without sensible conditions
 Brazil’s Jair Bolsonaro is building the case for his own impeachment
 A collapse in demand is reducing prices even as central banks print money
 Pandemic combined with collapsing oil prices spells real hardship
 Candour about the fiscal deficit might return the Democrat to prominence
 Schools can spread infection, but long absences mean gaps in learning
 Higher education should not be treated as just another business
 Crash course in communications technology will have lasting benefits
 China could rebuild trust by allowing a multilateral expert mission to Wuhan
 Postal voting should be an option for all in the coming presidential election
 Test-and-trace programme is essential to avoid a second virus peak
 Pandemic has shown that businesses neglected vital safety margins
 Governments must be mindful to preserve global production capacity
 More needs to be done to help developing world fight virus and avoid economic collapse 
 Action to stimulate domestic demand would help other economies
 Furlough schemes will not be enough to prevent mass unemployment
 ‘Piracy’ in obtaining vital supplies may rebound on those responsible
 Emmanuel Macron is right that without solidarity the single currency could fail    
 Without a Covid-19 vaccine life will not quickly snap back to how it was before
 Crisis-fighting measures need to be transparent and unwound quickly
 Tech investment group has potential to do much good for the world
 Suspending US funding to global health body is grossly irresponsible 
 International Edition
 https://www.ft.com/ft-view
