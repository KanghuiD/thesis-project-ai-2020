COVID-19: Hand sanitizers inactivate novel coronavirus, study finds
 Tests have confirmed that two hand sanitizer formulations recommended by the World Health Organization (WHO) inactivate the virus that causes coronavirus disease 19 (COVID-19). The tests also provide reassurance that store-bought sanitizers combat the virus. 
 The results of the new tests have been published as a preprint in the journal Emerging Infectious Diseases.
 In the absence of a vaccine or effective antiviral drugs, hand hygiene is a mainstay of efforts to prevent the spread of severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2), the virus that causes COVID-19.
 People who have the infection may show few, if any, symptoms, but still be able to transmit the virus. The virus spreads via droplets in the air or on commonly used surfaces, such as door handles.
 Washing the hands thoroughly with soap and water for at least 20 seconds is a highly effective way to defend against harmful bacteria and viruses.
 Handwashing isn’t always practical, however, especially for healthcare workers. This is due to a lack of access to running water, and a lack of sufficient time to wash the hands thoroughly. Meanwhile, this group may be exposed to infection from a variety of sources throughout the course of each day.
 Alcohol-based hand sanitizers provide a quick, simple alternative. However, there has been a lack of hard evidence that they are effective against SARS-CoV-2.
 Guidelines to date have stemmed from research showing that the sanitizers inactivate other coronaviruses.
 The WHO recommend two alcohol-based sanitizer formulations to prevent the spread of pathogens in general. Now, scientists in Germany and Switzerland have tested the sanitizers’ effectiveness against SARS-CoV-2.
 The first sanitizer comprises:
  The second sanitizer comprises:
 The researchers exposed SARS-CoV-2 virus particles to each formulation for 30 seconds.
 When they tested the subsequent ability of the virus to infect cells in lab cultures, they found that both formulations had inactivated the virus. 
 “We showed that both WHO-recommended formulations sufficiently inactivate the virus after 30 seconds,” says Prof. Pfänder.
 The results of the present study are particularly relevant because supplies of hand sanitizers have been running low throughout the coronavirus pandemic. 
 The new study gives community and hospital pharmacies the green light to make their own sanitizers quickly and easily using the WHO formulations, confident that they will be effective.
 The authors conclude:
 “Our findings are crucial to minimize viral transmission and maximize virus inactivation in the current SARS-CoV-2 outbreak.”
 This may not always be the case in practice. 
 In its advice to the public on hand hygiene, the Centers for Disease Control and Prevention (CDC) suggest that sanitizing the hands with gel should take about 20 seconds.
 The scientists also tested ethanol and isopropanol — the active ingredients of each WHO-recommended formulation — in isolation and in varying concentrations. 
 Their results suggest that either ethanol or isopropanol at a concentration of at least 30% vol/vol is sufficient to inactivate SARS-CoV-2.
 For comparison, the CDC recommend that hand sanitizers contain at least 60% alcohol. 
 Hand sanitizers sold in pharmacies and shops typically have an alcohol concentration of around 60%.
 Homemade hand sanitizers made without the necessary skills, equipment, and medical-grade ingredients, however, may not contain alcohol concentrations high enough to inactivate SARS-CoV-2. 
 For example, pure rubbing alcohol, or “surgical spirit” in the United Kingdom, and vodka have alcohol concentrations of around 70% and 40%, respectively.
 But repeated use of rubbing alcohol on the hands can dehydrate the skin, causing inflammation and irritation. 
 And vodka is unsuitable for making hand sanitizer, according to Food and Drug Administration (FDA) guidelines, because it is not the right grade of alcohol.
 Proper hand washing can help prevent the spread of germs and disease. Learn proper hand washing steps with a visual guide, along with helpful tips…
 Due to the global spread of SARS-CoV-2, some supplies may be limited, including hand sanitizers. People may choose to attempt to make their own. But…
 Dry hands can be uncomfortable and sometimes may crack or bleed. Learn about some of the most effective home remedies for dry hands here.
 There are many steps a person can take to help prevent the spread of coronavirus, including frequent hand-washing and social distancing. Learn more…
 In this Special Feature, we explain what steps you can take right now to prevent infection with the new coronavirus — backed by official sources.
 https://www.medicalnewstoday.com/articles/covid-19-hand-sanitizers-inactivate-novel-coronavirus-study-finds
