Page Not Found | Federal Trade Commission
 Skip navigation
 Tidying up: Decluttering the COPPA FAQs
 By: Lesley Fair | Jul 22, 2020  11:49AM 
 Maybe it’s the influence of that best-selling book on home organization or perhaps the silos of stuff in our makeshift home offices are becoming more noticeable. Either way, people are in a decluttering mood – and we are, too. Our recent project: updating and streamlining Complying with COPPA: Frequently Asked Questions, known as the COPPA FAQs. But not to worry. The revisions don’t raise new policy issues and our COPPA Rule review continues.
 Continue Reading  Tidying up: Decluttering the COPPA FAQs
 The page you're looking for is currently unavailable or does not exist.
 It is possible that this page has been moved or renamed. You can use your browser's Back button to return to the previous page, go to the homepage, or you can browse or search for the information you're looking for.
 You may also find the information you’re looking for at our frequently asked questions page.
 Browse FTC scam alerts by date or topic.
 Competition Matters
 Consumer Information
 BCP Business Center
 Data Spotlight
 Tech@FTC
 Military Consumer blog
 Stay Connected with the FTC
 https://www.ftc.gov/news-events/blogs
