Coronavirus pandemic is bad business for the bad guys too
 The coronavirus pandemic is not only having a big economic impact on legal industries such as tourism, retail and entertainment; it’s also hitting Mexican criminal groups.
 Citing several Mexican media sources, a report by InSight Crime, a foundation dedicated to the study of organized crime in Latin America and the Caribbean, says that criminal groups are struggling to source illicit items from China such as fake luxury goods and chemical precursors to make fentanyl because of the outbreak of Covid-19.
 It was reported in February that La Unión de Tepito, a criminal group that controls much of the counterfeit goods trade in Mexico City, was facing pushback from businesses when its members went to collect extortion payments known as cobro de piso. The businesses said that they couldn’t afford to make the payments because they were not receiving illegal merchandise from China.
 A cell within La Unión de Tepito, which is based in the notorious Mexico City neighborhood of the same name, has been going to China since 2010 to purchase counterfeit clothes, jewelry and other goods to be sold later in shops and markets in neighborhoods such as Tepito, Lagunilla and the capital’s historic center.
 However, due to the outbreak of coronavirus in Wuhan, China, late last year, the members of the cell known as Los Marcos Polos have been unable to travel to the east Asian nation to purchase new supplies. La Unión de Tepito reportedly told shopkeepers that the trips to China have been suspended.
 While some business owners are threatening to stop paying extortion payments after paying for merchandise from China that hasn’t arrived, La Unión de Tepito remains determined as ever to collect them.
 Business owners in the central Mexico City borough of Cuauhtémoc who spoke with the newspaper Milenio said that even though their sales are down due to the growing outbreak of Covid-19 in Mexico, members of La Unión have made it clear that they will not be stopping the collection of payments.
 One person who asked to remain anonymous told Milenio that a gang member said: “Start saving because there won’t be a reprieve because of the virus.”
 “We don’t give a damn that there are not a lot of people [shopping, frequenting bars, etc.]. … [If you don’t pay] there will be abductions, you toe the line or you’re fucked.”
 Milenio reported that La Unión has incurred massive losses due to the failure to secure and sell counterfeit goods from China and is determined to make up for them by charging an additional “tax” in areas where it holds sway.
 The business owners said that young, armed men on motorbikes are sent to collect the cobro de piso payments and threaten them. They renewed their call for the National Guard to be deployed to central Mexico City to combat crime.
 As La Unión de Tepito sees its revenue fall due to the disruption of supply chains in China, another much larger criminal organization is also experiencing an interruption to business as usual as a result of the coronavirus pandemic.
 Citing a source within the federal Attorney General’s Office (FGR), MVS Noticias journalist Óscar Balderas reported on the radio show Nación Criminal earlier this month that the China contacts of the Jalisco New Generation Cartel (CJNG) have been unable to ship precursors to make the synthetic opioid fentanyl due to the outbreak of Covid-19.
 According to the FGR source, the CJNG may be forced to raise its fentanyl prices and could lose customers to other criminal groups as a result.
 Headed by Nemesio “El Mencho” Oseguera, the Jalisco cartel is one of two main importers of fentanyl into the United States, according to the U.S. Drug Enforcement Administration.
 InSight Crime reported that the supply chain troubles of La Unión de Tepito and the CJNG are likely only the beginning of the impact of the coronavirus pandemic on organized crime.
 In an analysis of the situation, it predicted that “criminal groups across the region will feel the squeeze.”
 “Countries across Latin America are shutting down borders and preventing air travel, which is likely to significantly disrupt criminal economies like drug trafficking, contraband smuggling and human trafficking. With most aircraft grounded, illicit drug flights that have become a mainstay of drug trafficking in the region may become easier to track,” Insight Crime said.
 Larger organizations, such as the CJNG, are better equipped to withstand the coronavirus-fueled economic slowdown but smaller groups such as La Unión de Tepito may struggle, the foundation said.
 The brewer of the Mexican beers Corona, Victoria and Pacífico has formed an alliance with a financial firm to provide loans to its small business customers.
 Life carries on in the Guerrero capital, where hardware stores, clothing boutiques, pawn shops and street stalls have resisted the order to close.
 Mexicans are growing increasingly weary of stay-at-home orders that have now been in place for more than a month, according to data collected by Google.
 The businesses and workers could be at risk if the government doesn’t support them through the coronavirus crisis, according to Oxfam México.
 There is a risk that corruption will infiltrate the loan scheme intended to help businesses through the coronavirus crisis, President López Obrador said.
 Presumed members of the Jalisco New Generation Cartel disbursed food and other necessities to poor residents over the weekend.
 The manner in which former employees of the tourism industry are forced into moving from the cities and back to small towns is aggravating tensions.
 Each year Mexico rolls the dice and makes a huge options bet on the future direction of oil prices. It’s called the “Hacienda Hedge” or the “Pemex Hedge.”
 Though it would be better not to have to face all of this upheaval, there is much goodness to be seen, had, and acted upon.
 For every 100 students who start school only 35 will graduate from high school. The Escalera Foundation is working to improve those numbers.
 Beans in general, and black beans in particular, are extremely healthy foods, high in antioxidants, zinc and fiber.
 There are essentially two craft figures that claim the title – wood carvings from the state of Oaxaca, and papier-mache figures from Mexico City.
 THE STORY: Manufacturers call on Mexico to reopen factories to maintain supply chain
 https://mexiconewsdaily.com/news/coronavirus-is-bad-business-for-the-bad-guys-too/
