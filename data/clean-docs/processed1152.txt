Coronavirus in Massachusetts - masslive.com
 Rubin, 82, had been hospitalized in Florida.
 The nation’s top expert on infectious diseases and a member of the COVID-19 task force for the White House will keep his job, at least for now.
 The COVID-19 pandemic has decimated the livelihoods of musicians worldwide.
 The NHL has been suspended since mid-March
 The new order requiring employees in businesses in Worcester to wear mask will go into effect Monday night.
 A non-profit foundation is raising money to purchase iPads for residents.
 The university is not laying off employees but responding to unexpected losses.
 Massachusetts Gov. Charlie Baker's office confirmed discussions between him and other governors about reopening the economy.
 Longer control cables and elimination of battery sources improved the system.
 A college senior from Lexington is breaking down cases by municipality.
 The state's Department of Public Health released the latest coronavirus statistics on Monday.
 Baker said utility companies will send crews to restore power once winds fall below 30 mph if it’s safe for workers to go out. Those crews have protocols to limit person-to-person contact.
 Speaking to reporters on Monday, Walsh said that 52% of the city’s caseload came between April 5 and April 12.
 After being told he was to be arrested on a felony charge of making threats, Figueroa ran to hide in a bedroom, police said.
 Several police officers who tested positive for COVID-19 have recovered and returned to work.
 We are expendable. The only thing that does matter is the corporations and the stock market.
 Yankee Candle closed its factory back in March.
 If he doesn’t want to answer questions, he shouldn’t attend press briefings.
 Each member of the Class of 2020 will receive a special surprise package in the mail during the summer.
 The total number of positive cases found during testing has reached 2,689 since the emergency began.
 Trinity has tested 7,933 people, and 62% of tests have been negative.
 This “Real World” we sent you into has been a challenge for your generation, and you have risen to face that challenge, just as we hoped you would, those many years ago, when you sat in front of us in our classrooms.
 Once the painful, but correct, decision was made to cancel or postpone all of the St. Patrick’s Parade-related events the outpouring of support and encouragement that our Parade Committee received was nothing short of overwhelming!
 The federal CARE Act has expanded benefits to make it so unemployed workers can receive an extra $600 a week for up to six months.
 The state expects to resume testing Tuesday.
 We know how crucial our museums, theaters, historic sites and science centers are to our community and how much our artists and cultural workers contribute to the education of our children.
 More veteran residents died at the home over the weekend.
 Americans are starting to see COVID-19 stimulus checks arrive in their bank accounts today.
 Looking for something to watch?
 Buy bulk meats and seafoods without having to leave the house.
 The Springfield-based health provider in still awaiting test results for 11 others.
 How about viewing the process as sport for our shutdown era? Place your bets. Form your fantasy leagues, with the draft staged on Zoom, of course.
 The $841 million, sent Friday to 8,746 health care providers , includes $32.1 million for Springfield based Baystate Health.
 The number of patients on ventilators in the intensive care units at Baystate Medical Center and other area hospitals is declining, officials said Monday.
 Hanover Insurance announced eligible customers will receive a 15% refund for April and May on auto premiums.
 Announced 21 deaths at its Longmeadow nursing home Friday.
 While hospitals and municipalities have been struggling to insure they have enough personal protective gear (PPE), some have been burned along the way.
 Under the agreement, workers holding licenses related to their occupation will receive the $10 increase. All other workers will receive the $5 pay increases.
 A pilot program that tested every employee and resident in nursing homes in Cambridge revealed 212 cases of the coronavirus.
 The nuclear aircraft carrier garnered attention after Capt. Brett Crozier pleaded for help as the virus swept through his crew of 5,000. Crozier has since been replaced.
 Testing for coronavirus at MGH Chelsea begins today for anyone reporting COVID-19 symptoms.
 Universal Plastics Group has been deemed an essential business by the commonwealth.
 A few Boston hospital employees got a big surprise from actor John Krasinski, a Newton native, and celebrated Red Sox hitter Big Papi.
 Humans can't always recognize each other in face masks, so imagine the confusion that wild animals in captivity must feel.
 The rise in COVID-19 cases are along its northern border with Russia and all but 10 of the cases were imported, and seven of the local infections were in the Heilongjiang province, a northern region where authorities are increasing restrictions and monitoring after a rise in people with COVID-19 crossing the Russian border.
 The Worcester City Council voted last week to create a 'Take Out Day' to help small businesses.
 BirchTree Bread Company joined other restaurants in Worcester as it closed its doors amid the coronavirus pandemic.
 The Millers Falls brewer is employing the distillery side of its business to make hand cleaner.
 Community Rules apply to all content you upload or otherwise submit to this site.
 Ad Choices
 https://www.masslive.com/coronavirus/
