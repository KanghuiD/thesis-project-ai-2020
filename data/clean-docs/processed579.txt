Homepage | Automotive News Europe
 The eventual restart at both plants in Mexico will be "gradual and under strict hygiene measures," VW said.
 The Swedish supplier expects to improve cash flows despite the coronavirus-induced crisis as cost cuts and efficiency actions started to show results.
 The supplier's profit fell 23 percent as the company said it had seen a recovery in demand and output in the world’s largest auto market.
 The UK's financial watchdog is introducing the measures to support customers facing payment difficulties due to the crisis.
 The brake maker's chairman said companies should "hook up to the German train" as automakers and suppliers in Europe's biggest car market get back to production.
 DS CEO Beatrice Foucher says the PSA unit would have a "bigger basket of technology" to choose from and the increased ability to share development costs if the French automaker's proposed merger with Fiat Chrysler Automobiles goes through. She spoke about this and more with Automotive News Europe.
 Chinese EV maker Suda Electric Vehicle Technology has started shipping vehicles to Germany as part of a major export push. 
 The factory, which typically builds about 1,200 light commercial vehicles daily for Fiat and PSA, will reopen on April 27.
 The brand was to develop a small electric car, with sales possibly starting by 2022. But the Chinese EV market has become much more competitive as government incentives are reduced.
 Interim CEO Clotilde Delbos said Renault, which is 15 percent owned by the French state, is organizing credit lines and aid when possible, including in France and emerging markets.
 Updated April 24
 -- U.A.W. opposes May restarts
 -- Nissan UK to make safety aprons
 -- U.S. sales could get online boost
 -- Seat to perform virus tests
 Automotive News Europe has listed shutdowns and scheduled dates of reopening of European car plants.
 -- Updated with Fiat Chrysler, Mercedes, Jaguar Land Rover restarts.
 After nearly a month of zero vehicle production in Europe, Volvo CEO Hakan Samuelsson has had enough. "This cannot continue," he said.
  Sign up to receive your free link to each monthly issue of Automotive News Europe as soon as it's published. 
 Founded in 1996, Automotive News Europe is the preferred information source for decision-makers and opinion leaders operating in Europe.
 1155 Gratiot Avenue
 Detroit MI  48207-2997
 Tel: +1 877-812-1584
 ISSN 2643-6590 (print)
 ISSN 2643-6604 (online)
 https://europe.autonews.com/
