EUobserver
 Wednesday
 29th Apr 2020
 The commission will out forward guidelines for safe travel, as some member states and companies are already drawing up plans to restart tourism under the threat of the pandemic.
 Theatres are dusting off a 19th century play: Ibsen's An Enemy of the People. It is about truth, freedom and tyranny. It deals with the loner versus the group, the role of the elite and the power of the majority.
 East Africa is facing a "Level 3" food emergency as serious as the coronavirus crisis, as climate change creates ideal conditions for 'Biblical' swarms of locusts.
 Political extremists are using the pandemic to incite violence and foreign spies are trying to hack sensitive video-talks, Belgium's security service has warned.
 Legal, human-rights and anti-poverty organisations are now demanding the EU's financial watchdog launch an audit into how the EU supports war-torn Libya.
 No surprise thus that the global pandemic serves as inspiration and influences the modus operandi of terrorists, both in terms of tactics and target selection.
 Germany is set to lead the EU's recovery plan - linking climate protection with economic perspectives - in conjunction with its upcoming EU's presidency.
 The United Nations special rapporteur on human rights and the environment has said "this is the most important climate change court decision in the world so far, confirming that human rights are jeopardised by the climate emergency."
 In tackling the pandemic, we must avoid Europe sleepwalking into a permanent expanded surveillance state.
 As debate around the issue of contact-tracing grows, the Bureau of Investigative Journalism reveals that the new science of predicting and monitoring population movements is already here – and EU agencies have been testing it on refugees and migrants.
 Doctors, nurses, paramedics, carers, cleaners, porters and ambulance drivers are at the sharp end of this pandemic. 8,000 Dutch healthcare workers have tested positive, with several in intensive care. Spain has 24,000 infected care-workers and Italy 16,950, with 150 dead.
 There is a pan-European systemic issue: from Spain and Italy to France, Belgium, or Poland, harvests are under threat and Europeans are entirely dependent on the hard labour and sweat of foreign-born workers. The elephant in the room? Regularisation.
 EU ministers will discuss how and when to reopen borders after the pandemic abates, as lockdown-Europe waits with baited breath for summer.
 At this time of mutual solidarity, but also anxiety and concern, we must consider the European security and relevant geopolitical connotations. If we fail to do this, we could rapidly slide from a potential 'Rook' to that of a 'Pawn'.
 The Berlin-based public relations agency Story Machine has been working with European Commission president Von der Leyen since her days as a candidate for the top EU post. Despite offering the same in-house service, the Commission defended the president's choice.
 A decentralised approach to coronavirus contact-tracing apps is starting to gain ground in the privacy debate within the EU and beyond - especially after centralised solutions are reported to pose a risk for fundamental rights.
 With his opinion article Kenneth Roth joins what are now dozens of critics to criticise the Orbán government and insult the Hungarian people with the charge that Hungary's extraordinary measures to fight Covid-19 amount to "dictatorship."
 The EU commission is double-checking emergency measures in every member state, as fundamental rights have been temporarily abrogated. But Hungary and Poland are problematic, yet no actions are planned.
 The 1918 flu pandemic "was just another thing to put up with" for people numbed by World War One - but there were also parallels with today, a British academic says.
 EU leaders agreed on the need for a fund to support the recovery of Europe's economy from the coronavirus pandemic, but disagreed on details. The commission will come with proposals tied to the new long-term EU budget.
 While much of the transport sector is pushing for unconditional state aid and to postpone climate policy action, experts believe that this crisis could help to transform the EU's transportation system, making it more resilient, sustainable and flexible.
 The EU is reorienting funds to boost the Libyan coast guard - amid fears that the escalating violence in the north African country plus the coronavirus will force a large exodus of people to take boats towards Europe.
 Universities in the EU ought to appoint civilian spy-catchers to stop China and others stealing secrets, the European Commission has suggested.
 While women are in the frontline on fighting the pandemic, they are also exposed more to the economic crisis that will follow. The pay gap could also grow. More security for flexible jobs, and investment in care work, could help.
 Several Asian countries have seen the coronavirus surge back after easing restrictions - despite their initial success in restraining the outbreak. As a result, EU member states maybe starting to loosen restrictions without having a 'concrete' example to learn from.
 EU leaders hold a videoconference Thursday to bridge divisions over financing Europe's recovery from the corona crisis, in talks interlinked with the bloc's long-term budget.
 Now (as before) there are lessons to learn about how a number of Eastern countries are tackling this coronavirus crisis. We have to drop our bipolar world view of ​​the 'free West' against the 'authoritarian East'.
 A few weeks ago, the European Union underwent a fundamental change: it ceased being a bloc of exclusively democratic states.  Even worse - leaders across Europe barely flinched.
 Russia and China are still bombarding the EU with coronavirus lies, including "malicious" Russian "fake cures" which put lives at risk.
 Up to €400bn will be lost by the tourism and travel sector due the pandemic, French internal market commissioner Thierry Breton told MEPs on Tuesday.
 EU candidate countries such as Albania, Montenegro and Serbia have relatively poor press freedoms - but still fare much better than Bulgaria, an EU state whose ranking in the World Press Freedom index has plummeted.
 By March, the emergency had forced every government in Europe into an impossible choice - letting many people die and health systems collapse, or ground much of public life and inflict massive harm on their economic lives.
 Covid-19 is no excuse to allow authoritarian minds more leeway. While Polish government uses the pandemic to conduct unfair elections, EU countries must see it as an existential political threat alongside the health and economic crisis.
 Italians were hit hardest when the coronavirus landed in Europe but the European Union was slow to help the country. The president of the European Commission Ursula von der Leyen has apologised — twice.
 The US was making matters worse in the global fight against coronavirus, the EU's foreign affairs chief Josep Borrell said, amid geopolitical competition to exploit the crisis.
 Numbers appear precise, but can also be unintentionally misleading when it comes to the pandemic, as experts warned that worldwide data is difficult to compare.
 https://euobserver.com/
