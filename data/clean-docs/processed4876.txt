Pelosi Says Senate Coronavirus Bill Is Discriminatory To Residents In DC, One Day Before House Vote | The Daily Caller
 Photo by Alex Wong/Getty Images
 Speaker of the House Nancy Pelosi on Thursday said she believes the Senate’s $2 trillion coronavirus emergency relief package is discriminatory to the District of Columbia, just one day before the House votes on the legislation.
 Pelosi held a press conference Thursday morning, hours after the Senate passed a late relief package Wednesday night, to address the coronavirus pandemic. In the press conference, Pelosi began to speak about what she would like to see in the next coronavirus relief package, despite the House, not yet passing the third.
 (Photo by JIM WATSON/AFP via Getty Images)
 One of the things Pelosi said is that the legislation, which she will vote on Friday, “treats the District of Columbia in a very discriminatory way.” Saying she would like to see D.C. be treated as a state for its funding. (RELATED: Senate Finally Passes $2 Trillion Coronavirus Emergency Relief Package)
 Pelosi says the next stimulus bill should also include stronger Occupational Safety and Health Act (OSHA) protections for workers, more money for food stamps, and more money for state and local governments. Pelosi also said there should be free testing for those who think they are infected so that they are not scared to do so.
 The Senate passed the package 96-0.
 TRENDING
 https://dailycaller.com/2020/03/26/pelosi-says-coronavirus-bill-discriminatory-washington-residents/