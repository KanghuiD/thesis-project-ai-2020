Expert Opinions on the COVID-19 coronavirus outbreak - Worldometer
 We're concerned that in some countries the level of political commitment & the actions that demonstrate that commitment don't match the level of the threat we all face. This isNOT a drillNOT the time to give upNOT a time for excusesThis is a time for pulling out all the stops
 Expert opinions listed in reverse chronological order. Click on a quote to expand.
 Hunter said that while cases were declining in China, the weekend had seen some “extremely concerning developments elsewhere.”
 He said the surge in South Korean cases had been unprecedented so far in the epidemic, adding: “The identification of the large cluster of cases in Italy is a big worry for Europe and we can expect there to be quite a few more cases identified in the next few days."
 "Once the virus establishes an endemic foothold in the human population; it will become difficult if not impossible to eradicate, like other betacoronaviruses (beta-CoV) that infect humans causing seasonal outbreaks of respiratory illness, albeit of lower severity."
 "WHO works with many people around the world, and people can express views on likely scenarios; and many of you have seen the various ranges of R0 that have been proposed. 
 All of that is important in generating hypotheses on possible scenarios in the future, but we've dealt with this previously, because we had some of the same predictions last week from other scientists. [...] "
 "The real issue here is whether we’re seeing efficient community
 transmission outside of China. And at the present time, we’re not
 observing that [...]. So therefore I think we have to be very, very
 careful not to drive fear in the world right now, and be very cautious
 in using the words you have used. We’ve said that the risk is very high
 in China, it’s high regionally and it’s high around the world. That is
 not, ‘the risk is high of a pandemic.’ The risk is high that the disease
 may spread further, and I think at face value, that is true."
 "Why do I think a pandemic is likely? The infection is in many parts of China and many countries in the world, with meaningful numbers of secondary transmissions. The scale is much larger than SARS for example (where the US had many introductions and no known onward transmission)
 Why do I think 40-70% infected? Simple math models with oversimple assumptions would predict far more than that given the R0 estimates in the 2-3 range (80-90%). Making more realistic assumptions about mixing, perhaps a little help from seasonality, brings the numbers down.
 Pandemic flu in 1968 was estimated to _symptomatically_ infect 40% of the population, and in 1918 30%. Those likely had R0 less than COVID-19. Below is from https://stacks.cdc.gov/view/cdc/11425
 What could make this scenario not happen? 1) conditions in Wuhan could be so different in some fundamental way from elsewhere that we are mistaken in expecting further outbreaks to have basic aspects in common. No reason I know of to think that but a formal possibility 
 2) There could be a higher degree of superspreading than has been appreciated ("dispersion in R0") which could mean that many locations outside Wuhan could "get lucky" and escape major onward transmission. https://hopkinsidd.github.io/nCoV-Sandbox/DispersionExploration.html
 This seems the most likely way a pandemic might be averted, but given the number of countries infected and likely missed imports in many of them that seems a lot to hope for 
 3) Control measures could be extremely effective in locations that have had time to prepare. Maybe in a few, but seems unlikely that is the case in all, especially countries with stretched health systems. 
 4) Seasonal factors could be much more powerful at reducing transmission than we currently expect. That doesn't help the Southern hemisphere, and is not consistent with behavior in China (preprint in queue from @MauSantillana
 et al.) 
 So that's my reasoning. It is as tight as I can make it and is an effort to use the science as we have it to make our best estimate about the future. Predictions can be wrong and I very much hope this is, but better to be prepared."
 , Feb. 14, 2020
 "[In China] Right now there is no evidence to me that this outbreak is under control. It's definitely not controlled."
 CNN, Feb. 13, 2020
 "It’s clear that there are probably many cases in countries where we haven’t yet found them. This is really a global problem that’s not going to go away in a week or two
 it appears that the rate of increase in new cases in China has slowed relative to the exponential growth we saw before. Some people are cautiously hopeful that that’s due to the success of control measures rather than the inability to count many cases [...]
 Transmissibility [is] perhaps lower than SARS, which was about 3 and higher than pandemic flu, which can be up to about 2. But what makes this one perhaps harder to control than SARS is that it may be possible to transmit before you are sick [...]
 On severity, estimates are that it’s worse than seasonal flu, where about one in 1,000 infected cases die, and it’s not as bad as SARS, where 8 or 9 percent of infected cases died. [...]
 I think we should be prepared for the equivalent of a very, very bad flu season, or maybe the worst-ever flu season in modern times [...]
 There is some evidence — and we’re working on quantifying it — that coronaviruses do transmit less efficiently in the warmer weather. So it’s possible that we will get some help from that, but I don’t think that will solve the problem, as evidenced by the fact that there’s transmission in Singapore, on the equator.
 The Harvard Gazette, Feb. 11, 2020
 Zhong was optimistic the new outbreak would soon slow, with the number of new cases already declining in some places.
 The peak should come in middle or late February, followed by a plateau and decrease, basing the forecast on mathematical modeling, recent events and government action.
 “We don’t know why it’s so contagious, so that’s a big problem.” 
 He said there was a gradual reduction in new cases in the southern province of Guangdong where he is based, and also in Zhejiang and elsewhere. “So that’s good news for us.”
 Prof. Dr. Nanshan Zhong was the first to describe SARS coronavirus as the main pathogen in 2003. He is a Medical Professor of Guangzhou Medical College, and was former President of
 Chinese Medical Association. 
 Reuters, Feb. 11, 2020
 “Sixty per cent of the world’s population is an awfully big number”
 The overriding question is to figure out the size and shape of the iceberg.
 Most experts thought that each person infected would go on to transmit the virus to about 2.5 other people. That gave an “attack rate” of 60-80%.
 Even if the general fatality rate is as low as 1%, which Leung thinks is possible once milder cases are taken into account, the death toll would be massive.
 Prof. Gabriel Leung is one of the world’s experts on coronavirus epidemics, played a major role in the Sars outbreak in 2002-03, and works closely with other leading scientists at Imperial College London and Oxford University.
 The Guardian, Feb. 11, 2020
 In a study Prof. Gabriel Leung co-authored, the following was expressed:
 If the transmissibility of 2019-nCoV were similar everywhere domestically and over time, we inferred that epidemics are already growing exponentially in multiple major cities of China with a lag time behind the Wuhan outbreak of about 1–2 weeks.
 Given that 2019-nCoV is no longer contained within Wuhan, other major Chinese cities are probably sustaining localised outbreaks. Large cities overseas with close transport links to China could also become outbreak epicentres, unless substantial public health interventions at both the population and personal levels are implemented immediately. Independent self-sustaining outbreaks in major cities globally could become inevitable because of substantial exportation of presymptomatic cases and in the absence of large-scale public health interventions. Preparedness plans and mitigation interventions should be readied for quick deployment globally.
 Nowcasting and forecasting the potential domestic and international spread of the 2019-nCoV outbreak originating in Wuhan, China: a modelling study - The Lancet, January 31, 2020
 The coronavirus is “not nearly as challenging for us as influenza” when seen strictly by the number of deaths.
  “We don’t know much about its transmissibility. We don’t necessarily have accurate diagnostic tests. And we don’t really know where the outbreak is going to go."
 “The only thing we have at present, absent vaccines or drugs, is containment.” 
 Lipkin said he estimates the mortality rate of the coronavirus will ultimately be less than 1%.
 But the figure is “speculative” because more antibody tests need to be conducted “so we can figure out who might have been infected but not manifested signs of disease.”
 Probably 10% or less of all infections are being detected in China at the current time.[..] Overseas [..] we might be detecting about 1/4 of all infections.
 We estimate that the infection is doubling in size every 5 days. Death rate is still unknown.
 “Even 1 percent mortality would mean 10,000 deaths in each million people.”
 The New York Times, Feb. 2, 2020
 Professor Piot co-discovered ebola and the presence of Aids in Africa
 “It’s a greater threat [than Ebola] because of the mode of transmission. The potential for spread is much, much higher.” 
 “If the number of people who get infected is huge, then that will also kill a number of people.”
 “It is therefore likely that it will spread, as flu and other organisms do, but we still don’t know how far, wide or deadly it will be.”
  In the early days of the 2009 flu pandemic, “they were talking about Armageddon in Mexico, but it turned out to not be that severe.” 
 “That is not to say that the disease won’t get ahead of the Chinese authorities completely or get ahead of the other countries that are containing it, but there’s enough evidence to suggest that this virus can still be contained.”
 “So there are clear indications obviously that the disease numbers are growing. But there is also some contradictory evidence as well that doesn’t completely align with the kinds of R0s that are being estimated.”
 If that’s the case, “we’re living with a new human virus, and we’re going to find out if it will spread around the globe.” 
 McGeer cautioned that because the true severity of the outbreak isn’t yet known, it’s impossible to predict what the impact of that spread would be, though she noted it would likely pose significant challenges to health care facilities. 
 https://www.worldometers.info/coronavirus/coronavirus-expert-opinions/
