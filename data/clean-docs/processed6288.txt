Mafia-linked funeral investigated amid coronavirus lockdown | Italy | The Guardian
 Sicilian procession for brother of Cosa Nostra boss Luigi Sparacio claimed to have broken Covid-19 safety laws
 Lorenzo Tondo in Palermo
 Tue 14 Apr 2020 17.25 BST
 Italian prosecutors are investigating the funeral of the brother of a former Sicilian mafia boss for allegedly breaching Italy’s coronavirus lockdown.
 Photographs showed a funeral procession in Messina attended by dozens of people. Family and friends gathered on the streets to accompany the coffin carrying Rosario Sparacio, 70, the older brother of Luigi Sparacio, who was considered one of the most important heads of the Cosa Nostra in the 1990s and who eventually turned supergrass.
 The news, first reported by the newspaper La Gazzetta del Sud, has sparked a row in Italy where since the beginning of March a government decree has banned all religious gatherings, including funerals and weddings, in order to contain the spread of Covid-19.
 In the cities hit hardest by the coronavirus pandemic, coffins are awaiting burial, held in churches, and the corpses of those who have died at home are being kept in sealed rooms.
 Claudio Fava, the president of Sicily’s anti-mafia commission, said: “While in Italy there are no funerals and weddings how is it possible that in Messina dozens of people accompanied the dead body of the brother of a Mafia boss to the cemetery?”
 The deceased man’s brother, Luigi Sparacio, was once considered the most powerful mafioso in eastern Sicily and was very close to the Sicilian boss of bosses, Nitto Santapaola. Sparacio was also delegated to maintain the Cosa Nostra’s relations with the Calabrian ’Ndrangheta mafia on the Italian mainland before, in 1994, he decided to work with the authorities.
 https://www.theguardian.com/world/2020/apr/14/mafia-linked-funeral-investigated-amid-coronavirus-lockdown
