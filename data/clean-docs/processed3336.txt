Non-Discrimination Policy | Resources
 
 Coronavirus Shutdown
  
 School Board
 Media
 Parent Info
 Resources
 Summer Programs
 This site provides information using PDF, visit this link to download the Adobe Acrobat Reader DC software.
 The District encourages informal resolution of discrimination complaints.  A formal complaint resolution procedure is available, however, to address allegations of violations of the District’s equal educational opportunities policy.
 Any questions concerning this policy, or policy compliance, should be directed to:
 School District of Altoona
 1903 Bartlett Avenue, Altoona WI 54720
 715-839-6224
 The responsibilities of the individual identified above include serving as the District’s Title IX Coordinator (sex discrimination and sexual harassment issues and complaints), Section 504 Coordinator (handicap/disability discrimination issues and complaints), and coordinator of all other student nondiscrimination-related issues and complaints.
 Opportunity for Residents to Review Final Report
 The School District of Altoona provides assurance that no student is discriminated against because of the student’s sex, race, color, religion, national origin, ancestry, creed, pregnancy, marital or parental status, sexual orientation, or physical, mental, emotional, or learning disability.
 The District has completed a self-evaluation of its status of pupil nondiscrimination in the following areas:
 Copies of the final report are available for review below, or by contacting the district office:
 1903 Bartlett Ave
 Altoona WI 54720
 715-839-6032
 Engage. Equip. Empower.
 Offering Large School Opportunities with a Small School Approach
 https://www.altoona.k12.wi.us/parents/school-resources/non-discrimination-policy