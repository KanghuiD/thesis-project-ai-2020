Queen Elizabeth's coronavirus speech fails to reassure all Britons
 Issued on: 06/04/2020 - 10:33Modified: 06/04/2020 - 10:34
                     Queen Elizabeth II's address on the Covid-19 crisis was broadcast Sunday across the world. The royal speech was just the fifth time the monarch has addressed the nation, outside her annual Christmas message. Some older Britons found it “uplifting”, while others, many younger, were less moved.
                 
 In the pre-recorded message, the 94-year-old British monarch spoke from Windsor Castle with only one cameraman in the spacious room, respecting the necessary social distancing rules.
 Her speech of under 5 minutes was described by many who spoke to RFI as “reassuring”, “inspiring”, “something we needed to hear”.
 Queen Elizabeth II thanked health workers from the NHS and those “helping to protect the vulnerable”, and stressed the need to stay home.
 Her call for solidarity was broadcast on a warm Sunday evening when reports from across the UK showed a large number of people sunning in parks or meeting by the sea.
 “Though self-isolating may at times be hard, many people are discovering that it presents an opportunity to…reflect,” the monarch said. “I hope in the years to come everyone will be able to take pride in how they responded to this challenge.”
 While the Queen is undoubtedly a figure that commands respect, some chose not to watch or listen to her address brought by the coronavirus crisis.
 Megan, a 26-year-old nanny, told RFI that even though she supports the monarchy, she did not watch the Queen.
 “I would be more inclined to hear support and encouragement from more relevant people with first-hand experience such as doctors and the police,” she said.
 “We need to hear more truths, not messages from those so far removed,” Megan added, citing an example of NHS health workers being held at knifepoint for their ID badges, which took two weeks to be reported.
 Volunteers flock to help British NHS coronavirus response
 Engineering student Ahmed, 26, did not switch on to the Queen’s speech as he cannot see how she could “possibly relate” to him both on a “cultural” and “financial” aspect.
 A young trainee nurse told RFI she did not watch the address because, being on the frontline herself, she knows what is happening. She criticised the media for not “faithfully” reporting what is taking place.
 “I want harder facts out there, not more encouraging speeches,” she said.
 Zack, a 27-year-old health and social care coordinator watched the Queen’s speech. “Hers is an opinion we haven’t heard so far,” he said. “Unlike [UK Health minister] Matt Hancock who talks to us every five minutes."
 “If we remain united and resolute, then we will overcome it" -- words from Queen Elizabeth II which resonate with 54-year-old Jane who was reassured by “this woman speaking directly from her own experience, having been through World War II".
 “She ends with ‘we will meet again’ leaving us with fantastic hope,” Jane added.
 Daily news briefReceive essential international news every morning
 Keep up to date with international news by downloading the RFI app
 Saudi Arabia rejects south Yemen declaration of independence
 Europe lays out exit strategies as countries begin to lift lockdown
 India's Covid-19 lockdown to be extended in hotspots beyond 3 May
 Johnson returns to Downing Street as UK looks for answers on ending lockdown
 India's Muslims mark subdued Ramadan in shadow of coronavirus
 WHO warns against 'immunity passports' as global virus deaths top 200,000
 French schools awarded Anzac Day WWI prize by Australian embassy
 Nations back UN global vaccine drive as Trump 'bleach' theory sparks outrage
 China increases coronavirus funding to WHO after US pulls support
 Ramadan begins as Covid-19 lockdowns clash with tradition of gatherings
 'Foreign trash' Covid-19 comic underlines racism against foreigners in China
 Covid-19: India warns of severe prison sentences as attacks on medical staff spiral
 The content you requested does not exist or is not available anymore.
 http://www.rfi.fr/en/international/20200406-queen-elizabeth-s-coronavirus-speech-fails-to-reassure-all-britons-message-solidarity-covid-19
