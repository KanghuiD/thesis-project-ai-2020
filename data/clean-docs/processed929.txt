The coronavirus pandemic: Why did history repeat itself? | Arab News
 https://arab.news/prhyd
 DUBAI: “Fool me once, shame on you. Fool me twice, shame on me.”
 That expression, commonly used when a person falls for the same deceit twice, probably sums up the thoughts of leading epidemiologists as they watch COVID-19 cause global havoc.
 Since late last year, the world has been largely defenseless against a contagion whose death toll has surpassed 178,000 and is still rising, with more than 2.5 million confirmed cases as of Thursday.
 In a few brief months, the coronavirus pandemic has thrown the future of public-health programs, employment and the world economy into question.
 Many believe that the failure of governments and global bodies to contain not the first or second but the fifth strain of coronavirus is unforgivable.
 In recent decades, the world has dealt with at least a dozen outbreaks, with SARS (2003), H1N1 “swine flu” (2009), Ebola (2014) and MERS (2012) the most obvious examples.
 MERS, or Middle East Respiratory Syndrome, is a COVID-19-like illness caused by the MERS-CoV virus, with direct contact with camels identified as a risk factor for human infection.
 All cases of MERS have been linked — either by travel or residence — to countries in and near the Arabian Peninsula.
 Now, much of the Middle East is under lockdown owing to the threat posed by a different coronavirus.
 So far, there is no indication that certain groups of people in the Middle East are more prone to the COVID-19 virus than others.
 Dr. Sundar Elayaperumalm, an Abu Dhabi-based microbiologist, said the new coronavirus poses a threat to all communities and knows no borders.
 However, he said that it is too early to say “whether people who have already been exposed to other strains of coronavirus may be less symptomatic than others.”
 Local medical experts are also conscious of the debate surrounding the possibility of a cured coronavirus patient becoming infected a second time.
 The human body’s antibody response seven to 10 days after the onset of an infection “means it is unlikely that patients who recover from COVID-19 can become re-infected so soon after contracting the virus,” said Elayaperumalm, who is also chairman of infection control at the UAE’s Burjeel Hospital.
 That said, there is still no clarity on what kind of immunity a recovered patient has from re-infection — temporary or long term.
 Elayaperumalm attributes the steady increase in the number of confirmed cases in the Gulf region to the scale and reach of mass testing programs.
 This section contains relevant reference points, placed in (Opinion field)
 The UAE has carried out more than 640,000 tests in a population of 9.6 million people, while as of April 19, Saudi Arabia had completed 180,000 tests.
 “Mass testing helps to detect cases that are asymptomatic or had contact with positive COVID-19 patients,” Elayaperumalm said.
 Mass testing is useful in identifying infected people before they can spread the virus and in providing them with the necessary care.
 Elayaperumalm believes ramped-up testing is particularly helpful in detecting infections among health workers.
 The hope is that precautionary measures such as social distancing, effective handwashing, and the use of face masks and protective gloves will help  “flatten the curve” of infections over time.
 “Masks may help. But experts keep returning to social distancing as the single best tool to stop the chain of transmission,” he said.
 Lockdowns, cancelation of events, working from home and school closures also will slow the spread of the virus.
 The World Health Organization (WHO) has emphasized that social distancing restrictions are only part of the response and are not cost-free.
 “Shutdowns” and “lockdowns” can slow COVID-19 transmission by limiting contact between people, but can have a profound negative impact on individuals, communities and societies by bringing social and economic life to a near stop, a WHO spokesperson told Arab News.
 “Such measures disproportionately affect disadvantaged groups, including people in poverty, migrants, internally displaced people and refugees, who most often live in overcrowded and under-resourced settings, and depend on daily labor for subsistence.”
 The WHO believes that public-health measures can be balanced with “adaptive strategies that are implemented with the full engagement of all members of society.”
 Such an approach aims to “encourage community resilience and social connection, protect incomes and secure the food supply,” the spokesperson said.
 While the fight against the coronavirus continues, the scale of the contagion has left many wondering if any action plan was in place, and whether a contingency strategy exists for future contagions.
 There is no denying that humanity had been warned — in the form of science-fiction novels, Hollywood films and lectures by leading thinkers.
 In a TED Talk in 2015 that went viral after the coronavirus outbreak in China, Microsoft co-founder and leading philanthropist Bill Gates cautioned that the world was “not ready for the next epidemic.”
 Misplaced government funding and lack of investment has resulted in under-strength health-care systems and virus-fighting capabilities, he said.
 “If anything kills more than 10 million people in the next few decades, it’s most likely to be a highly infectious virus rather than a war — not missiles but microbes,” he told the TED Talk audience.
 The clock is clearly ticking for mankind to get its priorities right.
 Coronaviruses are a large family of viruses that can cause diseases ranging from the common cold to SARS, MERS and COVID-19.
 After Israeli Prime Minister Benjamin Netanyahu and opposition leader Benny Gantz on Monday formed a coalition government, following the country’s third election in a year, they announced that the annexation of major parts of the West Bank will begin on July 1.
 Gantz said this would be subject to American support. However, Pompeo on Wednesday said annexation is an Israeli decision that the US will monitor.
 “We’re happy … a new government is formed. A fourth election, we think, wouldn’t have been in Israel’s best interest … We think it’s not in the world’s best interest,” he added.
 “As for the annexation in the West Bank, the Israelis will ultimately make those decisions. That’s an Israeli decision, and we’ll work closely with them to share with them our views of this in a private setting.”
 Pompeo was speaking at a press conference that began with him wishing Muslims a happy Ramadan, and during which he mentioned the first anniversary of the killing of hundreds Christians in Sri Lanka last Easter, and Israel’s commemoration of the Holocaust.
 He also revealed that the US is sending $5 million in aid to the Palestinian Authority to help combat coronavirus.
 He reiterated that the Trump administration had halted other financial aid because of concerns about where the money would end up.
 “The reason we stopped providing assistance previously was that these resources weren’t getting to the place they needed to (go), to the Palestinian people,” Pompeo said.
 “We hope that this money, this $5 million, will get where it needs to go to provide real assistance to the Palestinian people, who … are going to need a lot of help as they move through this.”
 He said the Trump administration will evaluate whether the aid has been used properly and whether there are any additional resources “that are appropriate or can be delivered in a way that actually gets to the Palestinian people.”
 The administration began to suspend financial aid to the Palestinians in January 2018 when it halted all funding to the UN Relief and Works Agency, which supports Palestinian refugees displaced by Israel’s wars in 1948 and 1967.
 The following August, US President Donald Trump ordered further cuts of more than $200 million to funding for aid programs in the West Bank and Gaza, saying the money would instead be used to fund projects elsewhere.
 This was seen by many analysts as an attempt to apply pressure on the Palestinians in an attempt to force them back to the negotiating table with Israel.
 In February 2019, the US pushed through legislation that would potentially expose Palestinian aid recipients to lawsuits filed by Jews in America over acts of alleged terrorism in the Middle East.
 As a result, the Palestinians asked that the aid they were still receiving from the US, which was provided by the US Agency for International Development, be suspended to avoid exposure to legal action.
 https://www.arabnews.com/node/1663931/middle-east
