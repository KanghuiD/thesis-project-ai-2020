•  COVID-19 pandemic - Aftermath measures 2020 | Statista
                                                     Industry Overview
                                                 
 Dossiers
 Get a quick quantitative overview of a topic
 Outlook Reports
 Trend forecasts in the Statista Market Outlooks
 Surveys
 Market insights from an expert and consumer perspective
 Toplists
 Company rankings for sales and analysis
 Market Studies
 Complete markets in evaluation
 Industry Reports
 All key figures for an industry
 Global Business Cities Reports
 Global Business City rankings and key figures on cities
 Brand Reports
 Insights and benchmarks on brands worldwide
 Country Reports
 Countries and their potential
 Further Studies
 Get a deeper insight into your topic
 Everything you need to know about Consumer Goods
 Identify market potentials of the digital future
 Key topics in mobility
 Key economic and social indicators
 Insights on most important technology markets
 Figures and insights about the advertising and media world
 Find studies from all around the internet
 Sales and employment figures at a glance
 Data sets for your business plan
                     
                     Directly accessible data for 170 industries from 50 countries
                     and over 1 Mio. facts.
                 
                             
                     Customized Research & Analysis projects:
                         
                                 
                     Quick Analysis with our professional Research Service:
                     Content Marketing & Information Design for your projects:
 More Information
                         KPIs for more than 20,000 online stores
                                     Additional Information
                             Show source
  Show sources information
                              Show publisher information
 April 2020
 Germany, United Kingdom, United States
 April 14 to 20, 2020
 2,132 respondents
 18 years and older
 Online survey
 Multiple answers were possible.
 State of Health
 COVID-19 cases in Indian states April 2020 by type
 COVID-19 cases worldwide as of April 21, 2020, by country
 COVID-19 confirmed, recovered and deceased cumulative cases in India April 2020 
 Rate of COVID-19 testing in most impacted countries worldwide as of April 21, 2020
                             Register in seconds and access exclusive features.
                         Free
                                                         $39 per month*
                                 (billed annually)
                         This feature is limited to our corporate solutions.
 You only have access to basic statistics.
 Corporate solution including all features.
 https://www.statista.com/statistics/1110470/opinion-on-measures-in-the-aftermath-covid-19-coronavirus-pandemic/