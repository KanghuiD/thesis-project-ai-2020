Coronavirus Pandemic Updates - CNET
 News, advice and tools for staying healthy and engaged.
 Having antibodies doesn't mean you're immune.
 Tracking the spread of infectious diseases like COVID-19 is more complex than following numbers. The way we talk about it matters too.
 As schools shift to online teaching, students lacking broadband at home could be left behind.
 From kitchen accessories to tech gadgets to fitness gear, here are the tools to help you survive an extended shelter-at-home order.
 Whether you're hiding out from the coronavirus or work remotely full-time, here's what you need to stay comfortable and productive.
 Here's how you can find work-from-home gear, fitness equipment, health care products and more for delivery.
 Here are the free games, movies, ebooks, video tools and more to help you survive self-quarantine.
 Get today's most important stories delivered to your inbox.
 The company's retail vice president told employees in a video update, according to Bloomberg.
 The tech giant already warned that the pandemic would affect its sales. On Thursday, we'll learn how much.
 Today's major tech stories include Apple's possible production delay of the iPhone 12, a review of Google's Pixel Buds 2 and Apple and Google's reworking of their Covid-19 tracing software.
 Wearing face masks and face coverings is becoming more and more common. Here's what you need to know.
 The current advice on what homemade face coverings do, how to wear them and how they can and can't help you prevent COVID-19 compared with N95 masks.
 Researchers at the Hong Kong University of Science and Technology say their new disinfectant spray can "inactivate" viruses from surfaces for 90 days, like the one causing COVID-19.
 You'll receive your stimulus payment quicker if you set up direct deposit with the IRS than if it's sent through the mail. Here's how to set up an electronic funds transfer so your coronavirus check goes straight to your bank account.
 Here's how to use the IRS' online tracker to check the status of your coronavirus stimulus payment.
 The IRS is now mailing and depositing the first wave of stimulus payments. Find out if you're eligible for a check and when you may expect to receive it. Those receiving SSI and VA benefits may need to act soon.
 We found some possible reasons why the Get My Payment tool for direct deposit and check tracking might not be working for you as intended.
 We hope you're safe and healthy. While the coronavirus is causing fear and economic uncertainty, we want you to know we're focused on offering honest advice, fact-based news and stories that help you understand what's going on. We're also bringing you the stories, features, how-tos, best lists and buying guides that may help you pass time and make each day a little easier. Let us know if there's anything you'd like to learn more about; we're standing by. In the meantime, keep calm -- and wash your hands. Often.
 https://www.cnet.com/coronavirus/
