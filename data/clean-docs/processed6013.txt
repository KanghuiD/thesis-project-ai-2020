Coronavirus | Coronavirus: Solskjaer and Neville talk social responsibility in football - AS.com
 Football
 Premier League 
 Football as a sport has come under fire in recent days following accusations that the Premier League in particular is not doing enough to help the fight against the coronavirus.
 Secretary of State for Health, Matt Hancock, has twice taken aim at footballers’ salaries while a number of English clubs have been criticised for announcing that they would be making use of the government’s furlough scheme; although Liverpool have since reversed their decision to do so.
 In defence of football, many have pointed to the charity work done by the players, with Marcus Rashford having raised over £20million for the charity FareShare. Rashford’s manager Ole Gunnar Solskjaer spoke to former teammate Gary Neville to discuss football’s response to the pandemic.
 "Football is an easy target. Players do a great amount in the community"Ole Gunnar Solskjær talks player pay cuts and public criticism in an exclusive sit down with @GNev2 and @GeoffShreeves 👇
 "For me football is an easy target sometimes," Solskjaer said during the video call for SkySports. "It's unfair to call on any individuals or footballers as a group because I already know players do a great amount of work in the community”.
 In such a shifting landscape, football as a whole is struggling to tread the line between a social conscience and the financial imperatives but Solskjaer believes that those involved in the decision-making have good intentions at heart.
 "Mistakes are being made and have been made by loads of people and that's how we learn as well. Now it's about making better decisions, good decisions. I think we all want to help the NHS, the communities, and I think it's important every single club do what they think is right.”
 Gary Neville had previously offered Matt Hancock the chance to appear on a special episode of Monday Night Football to discuss his thoughts on footballers’ wages but the former England international did concede that football needed to “resurrect public opinion” in light of recent events.
 He also asked the Manchester United boss if he thought football had a role to play in society despite the indefinite suspension of fixtures.
 “First of all we are role models”, Solskjaer said. “Sometimes we're heroes in football, at the moment we have to take a back step, the real heroes are the workers on the front line, I think everyone appreciates and admires what they're doing.”
 “Football is very important for mental health for many, and for communities and society… [Football] is such a big thing, so even a little fun game of FIFA, it's great that they can just show that footballers are normal people who are good at kicking a ball once in a while."
 You can stay up-to-date with the latest developments in the coronavirus pandemic by following our daily live blog.
 0 
 Comentarios        
 Para poder comentar debes estar 
    registrado                
 y haber iniciado sesión. 
 ¿Olvidaste la contraseña?
 Te recomendamos en English
 Suscríbete a nuestra newsletter
 C-Date
  Join to the sexy contacts online community, live your adventure. ¡Sign up!.
 Vintage Football Store
 Official retro t-shirts English football clubs, be inspired by the most important moments of the premier. ¡Exclusive here!
 Ediciones internacionales
 Apps
 Síguenos
 https://en.as.com/en/2020/04/08/football/1586300432_269707.html
