Opinion - Bloomberg
 A survey of San Francisco’s Mission District gives this virus lab its best chance yet at understanding coronavirus.
 The president still shows no sense of urgency in dealing with the pandemic, and Senate Republicans are along for the ride.
 From Disney’s doldrums to mothballed movies and accelerated cord-cutting, here’s how the pandemic is upending the world of the big and small screens. 
 Opening up now won’t save businesses, but workers scared of going back could be kicked off unemployment insurance.
 Hopes that Japan’s largest brokerage had overcome its international troubles proved unfounded again.
 The pandemic and its economic turmoil may help avert a ninth default by Latin America’s second-biggest economy.
 After months of Moscow’s provocations, the U.S. and U.K. sent surface ships to the Barents Sea for the first time in 35 years.
 From Michigan to Germany, the federal model is coming under strain. The consequences for financial markets could be severe.
 Fixing the country’s chronic political dysfunction can only begin with new elections.
 Australia’s response to Covid-19 is upending how decisions are made in a one-time recession-proof nirvana.  
 States can ramp up high-quality screenings faster with federal coordination.
 The Pandemic Anti-Monopoly Act isn’t just unnecessary, it’s downright harmful.
 https://www.bloomberg.com/opinion
