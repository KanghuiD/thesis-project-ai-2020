Qantas, Virgin resume routes to London, Los Angeles, Hong Kong, Auckland as Australia records 28th COVID-19 death - ABC News (Australian Broadcasting Corporation)
       Updated 
     
       April 03, 2020 23:45:00
 Australians trying to get back into the country will have to get to London, Los Angeles, Hong Kong or Auckland to do so, after the Federal Government, Qantas and Virgin resumed those routes for repatriation purposes.
 Foreign Minister Marise Payne said the Federal Government was in discussions with the airlines regarding flights to "less accessible destinations, including South America and the Pacific".
 Look back on Friday as it unfolded.
 Topics:
 covid-19,
 	diseases-and-disorders,
 	health,
 	government-and-politics,
 	australia
     First posted 
       April 03, 2020 06:25:06
 Contact Simon Smale
 If you have inside knowledge of a topic in the news, contact the ABC.
 ABC teams share the story behind the story and insights into the making of digital, TV and radio content.
 Read about our editorial guiding principles and the standards ABC journalists and content makers follow.
 Learn more
 Scientists are desperately searching for a coronavirus vaccine, but before that can happen there's a whole bunch of stuff they need to discover.
         By Catherine Hanrahan
 Seven in 10 people are satisfied with how our governments are handling the coronavirus pandemic, but we're not so happy with each others' behaviour.
 Heavy fines, jail time and an over-reliance on police without safeguards can lead to serious abuses of power, and may not be effective in enforcing coronavirus measures.
         By Anna Kelsey-Sugg
 When Charley Douglass cobbled together a mysterious laughter machine in the 1950s, he changed the way TV studios made comedies — and the way we watch them.
 AEST = Australian Eastern Standard Time which is 10 hours ahead of GMT (Greenwich Mean Time)
 https://www.abc.net.au/news/2020-04-03/coronavirus-live-blog-updates-covid-19-australia/12116038