Coronavirus lockdown: European countries try tailor-made approach - BBC News
 The lifting of Europe's lockdown measures is not at all co-ordinated. 
 EU member states have 27 different healthcare systems, 27 different infection rates and 27 different priorities as regards kickstarting their coronavirus-battered economies. 
 Measures even differ within countries. The north Italian region of Lombardy resisted reopening small, non-essential shops last week, while neighbouring Veneto welcomed the opportunity. 
 There are noticeable differences of opinion within governments too. Last week, the Austrian prime minister promoted "staycations", where people remain at home for the holidays. His tourism minister then promoted the idea of bilateral agreements between European countries to open borders during the summer season. 
 Governments face a challenging balancing act - juggling economic, social and health risks - until a coronavirus vaccine can be found. 
 Denmark has reopened primary schools. Spanish children haven't been allowed outside for over five weeks now. Who's right and who's wrong? No-one can be sure until more is known about Covid-19. 
 German Chancellor Angela Merkel is a reference point for many in Europe: calm, authoritative and very careful about over-promising. Her chosen way forward is to test widely, then trace and quarantine those who've been in contact with newly infected individuals. The European Commission and Council presidents agree, but they've played a pretty backstage role so far.
 EU leaders jealously guard the right to decide their own national health and border controls, especially in an emergency. At the same time, they're quick to blame "the European Union" (i.e. fellow European leaders) for not demonstrating sufficient solidarity in the coronavirus crisis. 
 The French president and the prime ministers of Spain, Italy and Portugal have all recently warned the future of the EU is now in danger. 
 The US leader will sign an executive order to suspend migration, but it's not clear how he will do it.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-europe-52355721
