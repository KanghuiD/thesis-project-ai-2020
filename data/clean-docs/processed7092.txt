Coronavirus: The porous borders where the virus cannot be controlled - BBC News
 As nations shut borders, a surge of people pouring unmonitored over international boundaries in a volatile and vulnerable part of the world has sparked warnings about the unchecked spread of the virus there.
 In March, more than 150,000 Afghans spontaneously returned from Iran, one of the countries worst hit by the coronavirus - thousands more arrive daily. 
 Tens of thousands have also recently returned from Pakistan - among the worst affected countries in South Asia. 
 Officials are struggling to control this unprecedented movement across what have always been porous and often lawless borders.
 So far, Afghanistan is not among countries severely hit by the virus, reporting 423 cases and 14 deaths, but this influx has raised fears of much higher transmission rates.
 "With the numbers of likely infected people who have crossed the border, I expect the numbers of cases and deaths [in Afghanistan] to go up significantly," says Natasha Howard, associate professor of global health and conflict at the National University of Singapore.
 If there is an explosion of cases, like we've seen in the US, Spain and Italy, war-ravaged and impoverished Afghanistan's health system would be completely overwhelmed.
 Abdul Maez Mohammadi and his family were in Iran for eight years. But after the boss at the construction company where he worked stopped paying his salary, he gathered his wife, brother and one-year-old son and headed home.
 This week they crossed from the Islam Qala border into Herat as undocumented migrants and will head back to their Taliban-controlled village where there are no health facilities.
 "The situation in Iran of Covid-19 is very dangerous and I heard there is nowhere to admit cases," says Mr Mohammadi. 
 At this border crossing there is no quarantine centre on either side. The provincial authorities are conducting basic health checks, but they are overwhelmed by the number of people.
 Herat has a shortage of Covid-19 testing kits and results take four or five days for those who do get tested - and by then it is likely they would have already left for their villages.
 Mr Mohammadi says he will have to earn money as soon as he is back in his village, but he knows they will have to take precautions.
 "We have to do hand-washing when we wake up from sleep, brush teeth three times a day, avoid mass gatherings, not travel to neighbouring areas and food should be well cooked," he says.
 The International Organization for Migration (IOM), part of the UN, has set up centres to provide humanitarian assistance for the most vulnerable of those crossing back into Afghanistan.
 Aziz Ahmad Rahimi, senior regional director for IOM in Herat province, says when they see anyone showing Covid-19 symptoms they transfer them to the local hospitals. Ten to 15 people so far have tested positive he says. 
 A similar situation is playing out on the border with Pakistan.
 The Afghan government requested Pakistani authorities to open border crossings to allow Afghans who had become stranded after Pakistan shut its borders to return home.
 Authorities said they would allow 1,000 people a day but 20,000 are reported to have crossed at the Chaman border in the last two days prompting authorities to abandon the stipulation that only those with valid documents be allowed to cross.
 Afghan authorities had made arrangements to quarantine 4,000 Afghans for 14 days at Torkham but were quickly overwhelmed by the numbers, reports say.
 In total 60,000 crossed into Afghanistan in three days, according to IOM. 
 An unverified video that has been widely shared by media outlets shows people rushing across the border without showing any documentation.
 And these are the people going through official checkpoints. For many years there has been illegal cross-border movement between Afghanistan and Pakistan - those numbers are much harder to track.
 All of this has led aid agencies and NGOs working in the region to give a dire warning about the spread of coronavirus across these borders.
 And if, as they fear, cases erupt in the next few weeks, how Afghanistan might deal with the numbers given developed countries with top-class health systems are struggling, is leading to some daunting estimates.
 The Afghan ministry of public health forecasts that 16 million out of a population of more than 30 million could get the coronavirus, citing the World Health Organization (WHO).
 Waheedullah Mayar, spokesperson for ministry of public health, says in the worst case scenario 700,000 people will require hospitalisation, 220,000 of them may require ICU treatment. From that number 110,000 people may die due to Covid-19.
 Afghanistan has 10,400 hospital beds in the entire country. In Herat province, some estimates put the number of ventilators at as little as 12.
 "Afghanistan will not have such a number of beds even in 10 years," he says, adding that health authorities are focused on preventative measures.
 Kabul is now under completely lockdown and public gatherings have been banned in Herat.
 But this is a population where many have pre-existing conditions like tuberculosis, cancer, diabetes and there are approximately 2.5 million malnourished children, according to charity Save the Children.
 But if the situation in Afghanistan is so dire why are so many desperate to come back?
 Mostly it is driven by by the Covid-19 outbreaks in Iran and Pakistan and the lockdowns that have squeezed out financial opportunities. People are also afraid they just won't get medical attention there.
 "The impressions among returnees is, if I'm going to die it's better to die in my home country," says Mr Rahimi.
 Some experts believe it's already too late to prevent the virus from spreading widely in Afghanistan. And help is unlikely to come from the international community consumed by the virus itself.
 "The situation is likely to get extremely desperate - we could almost consider it a ticking time bomb," says Natasha Howard from the National University of Singapore.
 Additional reporting by Mahfouz Zubaide in Kabul and M Ilyas Khan in Islamabad.
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-asia-52210479
