Chemistry news, research and opinions | Chemistry World
 Sitewide message
 Book your free demo and find out what else Mya 4 from Radleys can do
 2020-04-24T15:26:00+01:00By Alexander Whiteside
 The unexplained appearance and dramatic spread of a new form of HIV drug ritonavir hurt patients and cost its makers almost $250 million.
 By Monique Wilhelm
 Evidence of lead contamination was dismissed as resulting from a laboratory refurbishment
 By Andy Extance
 Academics and tech giants are using neural networks to represent electronic behaviour
 By Rachel Brazil
 The first person to link carbon dioxide to atmospheric warming has almost been forgotten. Rachel Brazil uncovers her story
 2020-04-23T13:30:00+01:00
 Accidents involving cleaning products have been on the rise this year, most likely due to the Covid-19 pandemic 
 2020-04-22T13:30:00+01:00
 Public health benefits of restricting emissions of the heavy metal will no longer be taken into account
 2020-04-21T14:06:00+01:00
 Goal is to minimise disruption to students and researchers without putting anyone at unnecessary risk
 2020-04-17T08:28:00+01:00
 Mapping out drug discovery routes with artificial intelligence
 2020-04-17T09:43:00+01:00
 Courses are as effective for teaching Stem students as regular lectures, study finds
 2020-04-24T08:30:00+01:00
 Suzuki–Miyaura cross-coupling catalyst atoms leach away but then return to their supports
 2020-04-23T09:30:00+01:00
 Visual bridge between physical and organic chemistry
 2020-04-23T08:32:00+01:00
 Study finds that chair and flat hexamers can’t explain structure of thin layers of water
 2020-04-22T08:30:00+01:00
 Radiation accelerates early stages of crystal growth and creates larger pores that can adsorb six-times more gas
 2020-04-20T08:58:00+01:00
 Solidified resin case impregnated with organocatalyst is easy to remove and reuse
 Sponsored by JMP
 Sponsored by Waters Corporation, 
 			by Emma Marsden-Edwards
 Sponsored by Umicore, 
 			by Philip Wheeler
 Voice of The Royal Society of Chemistry
 By Mike Sutton
 Mike Sutton unravels Hermann Staudinger’s long hunt to understand macromolecules, which began 100 years ago
 Can we explain the strange properties of water by thinking of it as two different liquids? Rachel Brazil dives into the ongoing debate
 By Kit Chapman
 How difficult is it to build a world-class research facility in the Middle East? Kit Chapman investigates
 By Clare Sansom
 Clare Sansom looks at the complex world of the spliceosome, a molecular machine in all our cells
 Sponsored by Oxford Instruments
 Sponsored by Waters Corporation
 Sponsored by Schrödinger
 Sponsored by Netzsch
 Sponsored by Dassault Systèmes
 By Nicola Jones
 By Angeli Mehta
 By Rebecca Trager
 By Phillip Broadwith
 By Vanessa Zainzinger
 By Derek Lowe
 Sometimes you prefer the high-risk, high-reward project to the ‘sure thing’
 Research funding and academic reward systems should get an overhaul to recognise mentoring and public engagement
 By Jennifer Newton
 Blavatnik award winner Kirsty Penkman discusses her research developing techniques to date fossils
 By Laura Vilaça
 After a new Brazilian government order forced universities to send only one faculty member to international conferences, an uproar ensued
 Sponsored by Hyper Recruitment Solutions
 From creating life-saving drugs to life-changing technologies, making a difference through candidates is in our DNA 
 The flexible electronics maven talks about science’s biggest problem, orchids and Campbell’s chicken noodle soup
 The synthetic chemist and communicator on parenting and putting on a show
 Staying on track means learning from past outbreaks
 How long will it take to develop a coronavirus vaccine and why is it so hard?
 Companies need to demonstrate community spirit in the face of the coronavirus pandemic
 Sponsored by Honeywell Research Chemicals
 Sponsored by Erlab
 Sponsored by Restek
 Sponsored by Advion
 Sponsored by Agilent
 Sponsored by PatSnap
 By Alexander Whiteside
 By Ben Valsler
 How a drug that prevents HIV from donning its protective ‘coat’ now makes up one arm of the World Health Organisation’s Covid-19 trial
 Will new clinical trials for Covid-19 give remdesivir a second chance? Ben Valsler introduces the broad-spectrum antiviral that didn’t quite make it as the Ebola drug it was originally planned to be
 Like a modern-day rollercoaster ride through Mary Shelley’s Frankenstein, this novel is an exploration of the scientific community’s incremental crawl towards evolutionary perfection
 A collection of two decades-worth of correspondences, this book reveals humanity’s fascination with the stars
 The story of Crispr-Cas9 gene editing technology, and where it could and should take humanity
 George ZaidanDutton2020 | 320 pp | £19.99ISBN 9781524744274
 This book covers every inch of our known universe, from planets and their moons, to asteroids, comets, dwarf planets, exoplanets, stellar objects and the galaxies beyond
 Use this book to plot visits across Europe to the homesteads, graveyards, laboratories, apartments, abbeys and castles of your chemistry heroes
 The link between spa treatments and a common piece of lab equipment
 USAF pilots flew into mushroom clouds to bring back samples that turned out to contain new elements – one of them didn’t make it home
 Charging ahead with protein separation
   document.write(new Date().getFullYear());
 Registered charity number: 207890
 Site powered by Webvision Cloud
 https://www.chemistryworld.com/
