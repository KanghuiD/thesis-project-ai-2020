Coronavirus — News | The Colorado Sun
 Health officials are urging everyone to get a flu vaccine this year to limit the strain on hospitals from a potential flu-coronavirus double whammy
 President Trump was flown to to a military hospital. He is reported "fatigued," given experimental drugs.
 Businesses in underserved neighborhoods of Colorado say federal pandemic-relief programs didn't come through for them, so they are leaning on private foundations or going it alone.
 Critics say the initiative is a new payroll tax on already struggling businesses. Supporters say it will help small businesses compete.
 The train’s hiatus this season is another blow to Colorado’s ski industry, which is trying to figure out how to safely reopen resorts this winter amid the pandemic
 Even for the Colorado districts or schools that are back in-person, there’s no return to normal for the PE and nutrition programs that make up the core of school-based health.
 State officials initially said the “Exposure Notifications Express” system would go live before the end of September
 The county, dealing with an outbreak at CU Boulder, has already passed one of the thresholds that could warrant a shutdown similar to what was mandated in March
 Gov. Jared Polis said he fears some Colorado parents are trying to home school their kids without proper planning and curriculums
 As new varieties of rapid COVID-19 tests hit the market, knowing which test tells you what is becoming even more important
 https://coloradosun.com/category/news/coronavirus/
