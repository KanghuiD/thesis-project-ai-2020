Nurse and healthcare assistant are latest NHS victims to die from coronavirus | Metro News
 135.6m shares
 A healthcare assistant who ‘would go the extra mile for anyone’ has died after contracting Covid-19.
 Jenelyn Carter, 42, was treated by her colleagues at Morriston Hospital, in Swansea, but she sadly did not make it. 
 Mark Madams, nurse director of the hospital, said: ‘Jenelyn would go the extra mile for anyone, and was a lovely caring person inside and out, with a heart of gold.
 ‘We are devastated by her death and offer our sincere condolences to her family and friends.’
 Acute Care Unit nurse Michael Allieu also died yesterday at East London’s Homerton university hospital after contracting the killer virus.
 The hospital’s chief executive Tracey Fletcher added: ‘Michael was a vibrant, larger-than-life character on our acute care unit, and was well known and very well liked throughout the hospital.
 ‘He will be greatly missed by all his colleagues both in the ACU and the wider Homerton Hospital community. 
 ‘Our thoughts and condolences are with his family at this sad time.’
 Swansea Bay University Health Board also paid tribute to Carter who worked on the admissions ward at Morriston Hospital.
 The healthcare worker was originally from the Philippines. 
 It said she was ‘loved by all her colleagues and patients’.
 Carter’s colleagues started a GoFundMe page for her when she was hospitalised after saying she was not eligible for sick pay because she was on a zero-hour contract.
 Another fundraiser has now been set up by her friend Alelie Paiste to pay for her funeral.
 ‘Can I ask anyone who can help and contribute anything that they could or ask friends and colleagues in work to support our Fundraising for our Jen.’
 Meanwhile, another 596 people have died in hospitals after contracting coronavirus, taking the UK death toll to at least 16,060, the Department of Health (DoH) has confirmed.
 Today’s jump in deaths is significantly lower than yesterday’s increase of 888, but death figures tend to drop at the weekend due to delays in fatalities being registered.
 Not convinced? Find out more »
 https://metro.co.uk/2020/04/19/nurse-healthcare-assistant-latest-nhs-victims-die-coronavirus-12578816/
