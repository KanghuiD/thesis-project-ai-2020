85% of coronavirus cases in Egypt do not need medication: WHO official - Egypt Independent
 Eighty-five percent of coronavirus cases in Egypt do not need medication and recover by themselves, said the representative of the World Health Organization in Egypt, John Jabbour, on Sunday evening in response to rumors that Egypt’s authorities were hiding the true number of cases.
 googletag.cmd.push(function() { googletag.display('div-gpt-ad-1488287610204-3'); });
 His remarks came during a phone call with host Amr Adib on MBC Masr.
 Some coronavirus cases show minor symptoms that do not need treatment, he explained.
 The detection and isolation of positive cases is one of the most important recommendations of the World Health Organization to all countries, Jabbour stressed, adding that cases with moderate to severe symptoms cannot be hidden.
 As five percent of coronavirus cases require respirators it is not in the interest of any state to hide case numbers, he said.
 Jabbour also praised the efforts made by Health Minister Hala Zayed and her assisting team, working around the clock to counter the spread of the virus and detect cases.
 On Sunday, Egypt announced 103 new cases in addition to seven deaths, bringing the number of cases so far registered in Egypt to 1,173.
 The ministry announced that 247 cases were fully recovered and left quarantine hospitals.
 23 hours ago 
 4 days ago 
 5 days ago 
 6 days ago 
 2 weeks ago 
 1 week ago 
 7 days ago 
 https://www.egyptindependent.com/85-of-coronavirus-cases-in-egypt-do-not-need-medication-who-official/
