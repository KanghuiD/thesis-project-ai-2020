Coronavirus: Philippines quarantines island of 57 million people | Philippines News | Al Jazeera
 For daily wage earners, no work means no pay 'so no choice' and social distancing is impossible, critics say.
 Manila, Philippines - President Rodrigo Duterte has put the entire Philippine island of Luzon under an "enhanced community quarantine" until April 12 to stop the spread of coronavirus infections.
 In a televised public address on Monday, Duterte said public movement would be restricted to only buying food, medicine and other essential items necessary for survival.
 The president instructed the labour and social welfare departments to implement measures that would alleviate the burden of the lockdown on small business and wage workers. He also urged businesses to release the mandatory 13th-month pay to their employees.
 According to the health department's latest figures, there are 142 confirmed COVID-19 cases in the Philippines and 12 deaths.
 Effectively a lockdown of the country's largest and most populous island, the measures affect about 57 million people. 
 "We already have an enhanced community quarantine which we started two days ago [over Manila], we are just expanding it to the entire Luzon," said Panelo.
 The move is the most aggressive among Asian countries struggling to cope with the COVID-19 disease, which has already infected more than 164,000 people, killed at least 6,500, and crippled public health systems worldwide. 
 The announcement of the lockdown came after a failure to implement social distancing measures to stave off the spread of coronavirus.
 The measure made Arnold Vega's morning commute look like a mass exodus. 
 Vega pushed and jostled his way through a throng of passengers to get on a public utility vehicle and cross the city border that separates his suburb of Bulacan from the mega-metropolis of Manila where he works as a nurse in a health clinic. 
 Police and military personnel stationed at checkpoints took each person's temperature, pointing thermal scanners at foreheads, ready to separate those who showed any signs of fever. Many were not wearing adequate protective gear themselves. Passengers also had to show proof that they worked in Manila by showing either a company ID or a certificate of employment. 
 Public utility vehicles operated at half capacity as strict social distancing required passengers to sit one seat apart. Some drivers asked passengers to pay twice their fare to make up for the unoccupied seat. 
 It took Vega four hours to get to work. "It was total chaos."
 Earlier on Monday, several malls across the various districts of Manila announced their closure for one month. Mayors met with mall owners to discuss ways to alleviate the impact of closures on employees who are mostly contractual daily wage earners.
 Other cities declared a state of calamity or went into their own version of lockdown.
 Robert Mendoza, president of the Alliance of Health Workers, lambasted the government's attempt at quarantine. "What we need is mass testing, more trained healthcare workers, and an increased health budget." 
 The Duterte administration slashed the 2020 health budget by $197m but committed an additional $44.5m to buy protective gear for healthcare workers. 
 "Where is that promised budget? Even as COVID-19 cases keep climbing, our healthcare workers continue to work without proper protective gear. Who will take care of everyone else if health workers get sick?" 
 According to Mendoza, there are about 46 healthcare workers who have coronavirus symptoms and are currently being monitored.
 Renato Reyes, secretary-general of the lef-wing alliance Bagong Alyangsang Makabayan (New Patriotic Alliance), said the government must think of ways to protect low-income daily wage earners who will be hit hardest by the lockdown.
 "Social distancing and work from home is impossible for daily wage earners. For them, it is no work, no pay so no choice. They will risk getting COVID-19 to keep their jobs," he added.
 Opposition Senator Risa Hontiveros estimated at least 650,000 Filipino households would become the "new poor" in Metro Manila alone because of the economic implications of the lockdown and moved for a $250 payment for those affected by the financial whiplash.
 "The president's directive for an 'enhanced community quarantine' in Luzon ... should be implemented as a public health measure that takes into account the welfare of the most vulnerable," said Hontiveros in a statement. 
 Some of the provisions of the quarantine measures made citizens nervous as the threat of arrest for those in violation of the lockdown were reminiscent of martial law. The Philippines has a history of martial law in the 1970s under the Marcos dictatorship and alleged extrajudicial killings under the Duterte administration. 
 But security analyst Jose Antonio Custodio did not agree. "It's far from martial law. It's more like a massive humanitarian crisis response that leaves much to be desired due to poor planning and knee-jerk reactions." 
 NewsFeed
 Panic buying over coronavirus fears
             Al Jazeera News
 	 
 https://www.aljazeera.com/news/2020/03/coronavirus-philippines-quarantines-island-57-million-people-200316161225532.html
