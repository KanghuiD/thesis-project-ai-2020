Letters: Hancock’s interview says it all (4/21/20) – The Denver Post
 					
 			Trending:		
 Re: “I’m going to err on the side of saving lives,” April 20 news story
 Need anyone say any more than the following quote from your interview with Mayor Michael Hancock?
 “I’d rather dig us out of an economic challenge than dig graves for people when we could have prevented needless loss of life.”
 I think that says it all and to the protesters, it is important for them to understand the mayor’s concerns to keep all above the ground.
 Jay Weinstein, Dillon
 Seriously? The first question you ask Mayor Hancock in this embarrassing puff piece is what snack does he “dive” into at the end of the day? There are tens of thousands of hard-working, at-risk and now out-of-work Coloradans.
 This entire article seemed like an attempt to garner sympathy for the Denver Mayor and tug on our heartstrings. I am sure there are very few economically suffering Denverites who will now think, “Oh, the poor mayor. Now I don’t feel so bad for myself, even though I have lost my job, and don’t know where my family and I will live.”
 Has he taken a pay cut to really commiserate with we lowly subjects? If not, he, the city council and the entire state legislature need to if they want to show that they really have skin in the game.
 Steve Denham, Aurora
 Re: “Protester: Colorado needs to start living again,” April 20 news story
 The words “freedom” and “liberty” were prominent in Sunday’s protest (or was it a Trump 2020 rally) against the stay-at-home restrictions we’re all living under as an effort to flatten the curve of coronavirus infection and death.
 And they accuse Gov. Jared Polis of being a “tyrant” for putting these restrictions in place. Perhaps if they had spent a little more time studying world history, they would have learned about some true tyrants like Stalin, Hitler, Idi Amin, and Saddam Hussein. The protestors like to throw around words of which they have no clue of the meaning.
 When they say they want freedom, I guess they want freedom from civic responsibility, freedom from facts, freedom from expertise. And I guess they want the liberty to lack any kind of empathy for their fellow human beings.
 I saw a lot of American flags being displayed on Sunday. That’s a pretty easy way to express patriotism; doesn’t require much work or sacrifice. I praise our true patriots, the front line health care workers who are putting their lives on the line.
 To quote the great American songwriter John Prine, who died recently of the coronavirus:
 “I got my window shield so filled
 With flags I couldn’t see.
 So, I ran the car upside a curb
 And right into a tree.
 Your flag decal won’t get you into heaven anymore.”
 Stephen Catterall, Roxborough Park
 If the doomsday politicians and pundits are correct, the past week’s large outdoor massed gatherings in Denver and around the country, hundreds of persons in close quarters demanding an end to the siege mentality of cowering at home, we will shortly be seeing at least half of them ill at home or hospitalized and dozens dead.
 If the effect on these freedom desiring citizens (Nancy Pelosi would call them “unpatriotic and dangerous”) is to the contrary, then perhaps the theory of “herd immunity” is a better solution. Get out, get it, get over it, get a life and let’s get back to work.
 Stan S. McWhinnie, Denver
 OK
 https://www.denverpost.com/2020/04/26/letters-hancocks-interview-says-it-all-4-21-20/
