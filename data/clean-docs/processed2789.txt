Coronavirus outbreak could cost world's airlines up to $314bn | Business | The Guardian
 Industry body says latest assessment is three times worse than ‘worst-case scenario’ just five weeks ago
 Gwyn Topham Transport correspondent
 Tue 14 Apr 2020 17.37 BST
 Global airline revenues are now forecast to drop by more than half – $314bn (£249bn) – in 2020, as the industry warned that its “outlook grows darker by the day”.
 The International Air Transport Association’s (Iata) latest estimate adds a further $62bn of lost revenue from its previous assessment in late March and is almost three times worse than its “worst-case scenario” from five weeks ago, with around 95% of international passenger traffic now lost due to travel restrictions.
 The figures would mean a 55% drop in revenue from 2019, as Iata factored in the extension of travel restrictions, a deepening recession and the further spread of Covid-19 to Africa and Latin America.
 Iata’s director general, Alexandre de Juniac, said: “The industry’s outlook grows darker by the day. The scale of the crisis makes a sharp V-shaped recovery unlikely. Realistically, it will be a U-shaped recovery with domestic travel coming back faster than the international market.”
 Airport operators in the UK said that the change in Foreign Office advice to “indefinite” warnings against non-essential travel led to widespread cancellations of summer bookings, and that airports were now not expecting to see business restored to normal levels until 2021 or even 2022.
 The Airport Operators Association said that while its members had furloughed between 50% and 80% of their staff, and passenger operations had come to a near-complete halt, the government’s support schemes did not go far enough. It called on the chancellor to swiftly announce an extension of the job retention scheme beyond May, or airports would start the process of making widescale redundancies from 18 April, allowing the statutory minimum 45-day consultation period.
 https://www.theguardian.com/world/2020/apr/14/coronavirus-outbreak-could-cost-worlds-airlines-up-to-314bn
