This Week in Virology | A podcast about viruses - the kind that make you sick
 This is a placeholder for your sticky navigation bar. It should not be visible.
 Stanley Perlman joins TWiV to discuss immune responses to coronaviruses, including seasonal CoV, MERS, SARS, and SARS-CoV-2, including prospects for a vaccine.
 Hosts: Vincent Racaniello, Rich Condit, Kathy Spindler, and Brianne Barker
 Guest: Stanley Perlman
 Become a patron of TWiV!
 Intro music is by Ronald Jenkees.
 Coronavirus expert Christian Drosten joins Vincent to provide a view from Germany on COVID-19 and SARS-CoV-2.
 Host: Vincent Racaniello
 Guest: Christian Drosten
 Hosts: Vincent Racaniello, Dickson Despommier, Alan Dove, Rich Condit, Kathy Spindler, and Brianne Barker
 Guest: Daniel Griffin
 Doris Cully joins TWiV to discuss inhibition of SARS-CoV-2 in cell culture by ivermectin, followed by continuing analysis of the coronavirus pandemic caused by SARS-CoV-2 including COVID-19 in Nadia the tiger, and prediction of respiratory failure by levels of IL-6.
 Hosts: Vincent Racaniello, Alan Dove, Rich Condit, and Brianne Barker
 Guest: Doris Cully
 Daniel Griffin MD returns to TWiV from a hospital parking lot to provide updates on COVID-19 diagnostics, clinical picture, and therapeutics, followed by our coverage of the coronavirus pandemic caused by SARS-CoV-2. 
 Hosts: Vincent Racaniello, Dickson Despommier,Rich Condit, and Kathy Spindler
 Guest: Daniel Griffin, MD
 Immunologist Jon Yewdell joins Vincent and Rich to discuss immune responses in the context of infection with SARS-CoV-2.
 Hosts: Vincent Racaniello and Rich Condit
 Guest: Jon Yewdell
 TWiV covers trials of hydroxychloroquine and convalescent plasma therapy for COVID-19, and answers listener questions on blood tests for antibodies, cross-protection among coronaviruses, acquiring infection from food or the gas pump, face masks containing copper, and much more.
 Hosts: Vincent Racaniello, Alan Dove, and Rich Condit
 Daniel Griffin MD joins TWiV from a hospital parking lot to provide updates on COVID-19 diagnostics, clinical picture, and therapeutics, and then the TWiV team continues coverage of the coronavirus pandemic caused by SARS-CoV-2. 
 Hosts: Vincent Racaniello, Dickson Despommier, Alan Dove, Rich Condit, and Kathy Spindler
 Ian Lipkin joins Vincent to talk about his experience as a COVID-19 patient, and issues surrounding the disease and SARS-CoV-2 including limiting transmission, antivirals, vaccines, and much more.
 Hosts: Vincent Racaniello
 Guest: W. Ian Lipkin
 Pediatric infectious disease physician and coronavirologist Mark Denison joins Vincent for a discussion of COVID-19 and SARS-CoV-2 with an emphasis on antiviral therapeutics.
 Guest: Mark Denison
 
 MicrobeTV is an independent podcast network for people who are interested in the sciences. Our shows are about science: viruses, microbes, parasites, evolution, urban agriculture, communication, and engineering.
 https://www.microbe.tv/twiv/