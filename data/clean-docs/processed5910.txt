Steve Hilton: What do we have to do to get our leaders to follow the coronavirus data and science and end the shutdown? | Fox News
       This material may not be published, broadcast, rewritten,
       All market data delayed 20 minutes.
     
 What do we have to do to get our leaders to follow the data and science?
 Get all the latest news on coronavirus and more delivered daily to your inbox. Sign up here.
 As the coronavirus shutdown drags on, backed by the work-from-home elitists on TV, the majority of Americans who can't make a living from their laptop are screaming, "What do we have to do to get our leaders to follow the data and the science?"
 Here's the data: 21 percent of New York City residents have had coronavirus, nearly half a million in Los Angeles. In Santa Clara County, California, up to 85 times more people have had it then the official statistics claimed.
 LOS ANGELES COUNTY SAYS THOSE IN LOWER INCOME COMMUNITIES MORE VULNERABLE TO CORONAVIRUS
 The fatality rate, therefore, is not the 3.4 percent or 2 percent or 1 percent we were told. It's closer to 0.01 percent. Yes, seasonal flu is just more contagious. That's why it comes in a rush. That's why we had to flatten the curve and slow the spread.
 But we did that. So, why are we still shut down?
 Here's the science. Viruses don't disappear because we stay at home. They keep going until there's not enough people left to infect. A vaccine is over a year away, they say, but millions of Americans have already had the virus.
 Most didn't know they had it. Eighty percent have mild or zero symptoms. That's more than you can say for the victims of the shutdown. Their toll is rising every day. And here's what our leaders think about it.
 New York Gov. Andrew Cuomo: Economic hardship -- yes, very bad. Not death. Emotional stress from being locked in a house -- very bad; not death. Domestic violence on the increase -- very bad; not death.
 Have you ever heard anything so callous or stupid? Economic hardship causes death.
 Here's what else causes death: Millions of medical procedures cancelled because of wildly inaccurate coronavirus projections. Just one example: In the state of Arkansas recently, there were 80 people in hospital with coronavirus and 8,000 empty beds.
 The mindset of the shutdown zealots is the opposite of science. Instead of adapting their thinking in the light of new information, they cling to their old position, despite new information.
 This insanity has to stop. We know who's most vulnerable and how to protect them. Twenty percent of U.S. coronavirus deaths are in nursing homes. In some states, it's over half. So, what are our leaders doing about that? They're making it worse.
 For weeks, New York Gov. Andrew Cuomo, instead of sending nursing home coronavirus patients to the nearly empty Navy hospital ship, sent them back into nursing homes. With that one demented decision, he is responsible for a big part of America's death toll.
 We know who's at risk and how to protect them. Why aren't our leaders surging medical capacity and equipment to our nursing homes and care facilities, where the greatest generation is so obviously vulnerable?
 I'll tell you why. Because they're too busy putting sand in skate parks, flying creepy surveillance drones, and worst of all, hiring armies of busybody contact traitors to implement this idiotic, unscientific reckless establishment groupthink that the only way we can open up is widespread testing, contact tracing and isolating.
 Rep. Steny Hoyer, D-Md.: We need to test. We need to isolate those who test positive. We then need to contact trace.
 Chris Hayes, MSNBC host: We cannot truly reopen the economy without widespread testing.
 Anderson Cooper, CNN anchor: Testing, testing, contact tracing, testing, contact tracing. Different forms of testing, antibody testing. You know, testing people who are asymptomatic in order to get the country back to work.
 MSNBC had a two-hour special this week based entirely on this misinformation. It was literally called "Testing and the Road to Reopening."
 Testing, contact tracing and isolating are great if you're trying to contain an outbreak at the start, where you have hundreds of cases. It is totally absurd after a pandemic with millions infected, where you'll miss most of them anyway because they have no symptoms. And the absurdity rises to a level of grave danger if you're making this a condition for reopening, knowing that every day you delay will kill more Americans.
 Last week, we set up questions for Dr. Anthony Fauci. Well, he may not care about being accountable to you, but our state and local leaders do listen to him. He's the one that started this nonsense, still using an old playbook, even though the facts have changed.
 I beg you, Dr. Fauci, tell the governors, tell the mayors, how to protect the vulnerable in our nursing homes who are dying in droves because of inadequate infection control. Tell them that if we properly protect the vulnerable, we don't need the costly, complicated, technocratic nightmare of testing and contact tracing.
 And tell the American people so terrified by months of misinformation that many are scared to go out even if states do reopen. Tell them that most Americans, according to the data, have nothing to fear from coronavirus. Tell them that there is no scientific basis -- as long as we properly protect the vulnerable -- for this shutdown.
 The mindset of the shutdown zealots is the opposite of science. Instead of adapting their thinking in the light of new information, they cling to their old position, despite new information. It's not science, it's ideology. It's not based on data, but dogma.
 Dr. Fauci, tell the world that, based on the data, based on the science, we must protect the vulnerable and end the shutdown and save lives now.
 Adapted from Steve Hilton's monologue from "The Next Revolution" on April 26, 2020.
 https://www.foxnews.com/opinion/steve-hilton-leaders-follow-coronavirus-data-and-science-and-end-the-shutdown
