China sees rises in new coronavirus cases and asymptomatic patients | The Japan Times
 15
 M/CLOUDY
 Reuters
 Beijing – Mainland China reported 39 new coronavirus cases as of Sunday, up from 30 a day earlier, and the number of asymptomatic cases also surged, as Beijing continued to struggle to extinguish the outbreak despite drastic containment efforts.
 The National Health Commission said in a statement on Monday that 78 new asymptomatic cases had been identified as of the end of the day on Sunday, compared with 47 the day before.
 googletag.cmd.push(function() { googletag.display('div-gpt-ad-1499653692894-0'); });
 Imported cases and asymptomatic patients, who have the virus and can give it to others but show no symptoms, have become China’s chief concern in recent weeks after draconian containment measures succeeded in slashing the infection rate.
 Of the new cases showing symptoms, 38 were people who had entered China from abroad, compared with 25 a day earlier. One new locally transmitted infection was reported, in the southern province of Guangdong, down from five a day earlier in the same province.
 The new locally transmitted case, in the city of Shenzhen, was a person who had traveled from Hubei province, the original epicenter of the outbreak, Guangdong provincial authorities said.
 RELATED STORIES
 				China's lockdown of Wuhan may have prevented 700,000 virus cases, researchers say			
 				Politics aside, U.S. relies on China supplies to fight coronavirus			
 				‘Our goal is to survive’: Wuhan cautiously emerges from lockdown			
 				A city traumatized: As lockdown eases, Wuhan residents fret over future			
 The Guangdong health commission raised the risk level for a total of four districts in the cities of Guangzhou, Shenzhen and Jieyang from low to medium late on Sunday.
 Mainland China has now reported a total of 81,708 cases, with 3,331 deaths.
 Daily infections have fallen dramatically from the peak of the epidemic in February, when hundreds were reported daily, but new infections continue to appear daily.
 The country has closed off its borders to foreigners as the virus spreads globally, though most imported cases involve Chinese nationals returning from overseas.
 The central government also has pushed local authorities to identify and isolate the asymptomatic patients.
 						Click to enlarge
 					
 China, health, covid-19 
 Strength in numbers: A more open approach to tracking the virus in Japan			
 Designing for good: Creators in Japan respond to the coronavirus			
 Resetting the political calendar after COVID-19			
 In era of COVID-19, a shift to digital forms of teaching in Japan			
 Where we want to go in Japan once this is all over			
 DEEP DIVE			
 Episode 47: The current state of Japan’s emergency			
 LAST UPDATED: Apr 15, 2020			
 Just for kicks: Japan’s sneaker obsession rebounds
 LAST UPDATED: Feb 29, 2020
 				Directory of who’s who in the world of business in Japan
 			
 LAST UPDATED: Apr 27, 2020			
 Sponsored contents planned and edited by JT Media Enterprise Division.
 広告出稿に関するおといあわせはこちらまで
 Read more
 https://www.japantimes.co.jp/news/2020/04/06/asia-pacific/china-new-coronavirus-cases-asymptomatic-patients/
