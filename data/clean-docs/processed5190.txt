Coronavirus Update (Live): 3,081,410 Cases and 212,337 Deaths from COVID-19 Virus Pandemic - Worldometer
 Graphs
 SARS-CoV-2 Transmission - Air distance, air currents, duration in air, humidity, airborne transmission, duration on objects and surfaces, floor
 The coronavirus COVID-19 is affecting
 210 countries and territories around the world and 2 international conveyances. The day is reset after midnight GMT+0. The list of countries and territories and their continental regional classification is based on the United Nations Geoscheme. Sources are provided under "Latest Updates". Learn more about Worldometer's COVID-19 data
 Report coronavirus cases
     var tabs = ["today","yesterday"];
     var cur_continent = 'All';
     var cur_table = 'main_table_countries_today';
     $(document).ready(function() {
         tabs.forEach(function(val) {
             $('#nav-' + val + '-tab a').click(function(e) {
                 e.preventDefault();
                 $(this).tab('show');
             })
             $('#main_table_countries_' + val).dataTable({
                 "scrollCollapse": true,
                 "scrollX": true,
                 "order": [
                     [1, 'desc']
                 ],
                 //"dom": '<"top"f><"clear">',
                 "dom": "<'row pills_row'<'col-sm-9 col-md-9'><'col-sm-3 col-md-3'f>>" +
                     "<'#ctabs-row.row'<'#continents-tab" + val + ".w-100'>>",
                 "paging": false,
                 columnDefs: [{
                     data: function(source, type, val) {
                         if (type === 'set') {
                             source[5] = val;
                             return;
                         } else if (type === 'sort') {
                             // source[iColIndex] = is the cell value (<td> DATA THAT NEEDS TO BE SORTED <span>Data Label</span> </td>)
                             // return the right data to sort
                             if (source[5] == 'N/A') {
                                 return 0;
                             }
                         }
                         return source[5];
                     },
                     type: 'num-fmt',
                     aTargets: [5]
                 }],
                 initComplete: function(settings, json) {
                     var ctabs = $('#ctabs').clone();
                     ctabs.attr('id', 'ctabs' + val);
                     ctabs.appendTo($('#continents-tab' + val));
                     ctabs.find('a').on('click', function() {
                         cur_continent = $(this).html();
                         var cur_continent_original = cur_continent;
                         if (cur_continent == 'Oceania') {
                             cur_continent = 'Australia/Oceania';
                         if (cur_continent != 'All') {
                             $('.total_row_body').hide();
                         } else {
                             $('.total_row_body').show();
                         cur_table = 'main_table_countries_' + val;
                         if (val == 'today') {
                             // trigger yesterday
                             $('#ctabsyesterday li').removeClass('active');
                             $('#ctabsyesterday').find('a:contains("' + cur_continent_original + '")').parent().addClass('active');
                             $('#main_table_countries_yesterday').find('.row_continent').hide();
                             $('#main_table_countries_yesterday').DataTable().draw();
                             $('#main_table_countries_yesterday').find('tr[data-continent="' + cur_continent + '"]').show();
                             // trigger today
                             $('#ctabstoday li').removeClass('active');
                             $('#ctabstoday').find('a:contains("' + cur_continent_original + '")').parent().addClass('active');
                             $('#main_table_countries_today').find('.row_continent').hide();
                             $('#main_table_countries_today').DataTable().draw();
                             $('#main_table_countries_today').find('tr[data-continent="' + cur_continent + '"]').show();
                         $('#main_table_countries_' + val).find('.row_continent').hide();
                         $('#main_table_countries_' + val).DataTable().draw();
                         $('#main_table_countries_' + val).find('tr[data-continent="' + cur_continent + '"]').show();
                     })
                     ctabs.show();
                     $('#main_table_countries_' + val).DataTable().draw();
                 }
             });
             //$( '.main_table_countries th' ).stickybits();
             $.fn.dataTable.ext.search.push(
                 function(settings, data, dataIndex) {
                     // Don't filter on anything other than "myTable"                    
                     /*   if (settings.nTable.id !== cur_table) {
                            return true;
                        }*/
                     // Filtering for "myTable".
                     if (cur_continent == 'All') {
                         return true;
                     }
                     if (
                         data[12] == cur_continent
                     ) {
                     } else {
                         return false;
             );
             $('#main_table_countries_today_filter input').on('change', function() {
                 $('#main_table_countries_yesterday_filter input').val($(this).val());
                 $('#main_table_countries_yesterday_filter input').keyup();
             $('#main_table_countries_yesterday_filter input').on('change', function() {
                 $('#main_table_countries_today_filter input').val($(this).val());
                 $('#main_table_countries_today_filter input').keyup();
         });
     });
     .small_font_td {
         font-size: 12px;
     }
     .total_row td {
         font-weight: bold;
         text-align: right;
     .main_table_countries_div {
         font-size: 15.5px;
         text-align: left;
         width: 100%;
         overflow-x: unset !important;
         overflow-y: unset !important;
     .main_table_countries td {
         border-color: #ccc !important;
     .main_table_countries th {
         font-size: 14px;
         color: #666666;
         position: sticky !important;
         position: -webkit-sticky !important;
         -webkit-transform: translateZ(0) !important;
         transform: translateZ(0) !important;
         top: 0;
         background: #fff;
         box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);
     .main_table_countries th>* {
     .mt_a {
         text-decoration: underline;
     .mt_pills a {
         padding: 0 !important;
         padding-top: 4px !important;
         padding-bottom: 4px !important;
         padding-left: 14px !important;
         padding-right: 14px !important;
         text-decoration: none;
     .mt_pills_c.active a {
         background-color: #6C757D !important;
         color: #fff !important;
     .mt_buttons {
         margin-bottom: 16px !important;
     @media (min-width: 900px) {
         .mt_buttons {
             position: absolute;
             top: 0px;
         }
     .total_row_world {
         text-align: right !important;
         background-color: #DFDFDF !important;
         color: #363945 !important;
         font-weight: normal !important;
     .total_row_world td {
         background-color: #DFDFDF;
     #ctabs-row {
         margin-left: 0;
         margin-right: 0;
     .nav-tabs {
         border-bottom: none !important;
     .nav-tabs a {
         /*color: #fff !important;*/
     #main_table_countries_today,
     #main_table_countries_yesterday {
         margin-top: 0px !important;
     .pills_row {
     /* fixed header fixes 
     table.dataTable {
         margin-top: 0 !important;
     }*/
 Now
 Yesterday
 All
 Europe
 North America
 Asia
 South America
 Africa
 Oceania
 Country,Other
 TotalCases
 NewCases
 TotalDeaths
 NewDeaths
 TotalRecovered
 ActiveCases
 Serious,Critical
 Tot Cases/1M pop
 Deaths/1M pop
 TotalTests
 Tests/
 1M pop
 Continent
 1,092,150
 +1,354
 61,643
 +94
 169,611
 860,896
 15,453
 1,313,271
 +12,000
 124,592
 +595
 451,954
 736,725
 24,895
 487,554
 +4,328
 17,716
 +128
 240,423
 229,415
 5,947
 145,212
 +1,038
 6,793
 +63
 51,646
 86,773
 9,773
 8,305
 +14
 103
 +1
 6,918
 1,284
 45
 Australia/Oceania
 34,197
 +161
 1,475
 +7
 10,639
 22,083
 138
 721
 15
 645
 61
 World
 3,081,410
 +18,895
 212,337
 +888
 931,836
 1,937,237
 56,255
 395
 27.2
 USA
 1,010,507
 +151
 56,803 
 +6
 139,162
 814,542
 14,186
 3,053
 172
 5,696,928
 17,211
 Spain
 232,128
 +2,706
 23,822 
 +301
 123,903
 84,403
 7,764
 4,965
 510
 1,345,560
 28,779
 Italy
 199,414
 26,977 
 66,624
 105,813
 1,956
 3,298
 446
 1,789,662
 29,600
 France
 165,842
 23,293 
 45,513
 97,036
 4,608
 2,541
 357
 463,662
 7,103
 Germany
 158,758
 6,126 
 114,500
 38,132
 2,409
 1,895
 73
 2,072,669
 24,738
 UK
 157,149
 21,092 
 N/A
 135,713
 1,559
 2,315
 311
 719,910
 10,605
 Turkey
 112,261
 2,900 
 33,791
 75,570
 1,736
 1,331
 34
 918,885
 10,895
 Russia
 93,558
 +6,411
 867 
 +73
 8,456
 84,235
 2,300
 641
 3,139,258
 21,511
 Iran
 92,584
 +1,112
 5,877 
 +71
 72,439
 14,268
 2,983
 1,102
 70
 442,590
 5,269
 Brazil
 67,446
 +945
 4,603 
 +60
 31,142
 31,701
 8,318
 317
 22
 339,552
 1,597
 Canada
 48,500
 2,707 
 18,268
 27,525
 557
 1,285
 72
 733,705
 19,440
 Belgium
 47,334
 +647
 7,331 
 +124
 10,943
 29,060
 876
 4,084
 633
 220,204
 19,000
 Netherlands
 38,245
 4,518 
 33,477
 905
 2,232
 264
 193,950
 11,319
 India
 29,451
 939 
 7,137
 21,375
 21
 0.7
 716,733
 519
 Switzerland
 29,264
 +100
 1,677 
 +12
 22,200
 5,387
 185
 3,381
 194
 256,500
 29,637
 Peru
 28,699
 782 
 8,425
 19,492
 598
 870
 24
 239,563
 7,266
 Portugal
 24,027
 928 
 1,357
 21,742
 176
 2,356
 91
 360,155
 35,321
 Ecuador
 23,240
 663 
 1,557
 21,020
 146
 1,317
 38
 61,529
 3,487
 Ireland
 19,648
 1,102 
 9,233
 9,313
 3,979
 223
 127,319
 25,785
 Sweden
 18,926
 2,274 
 1,005
 15,647
 399
 1,874
 225
 94,500
 9,357
 Saudi Arabia
 18,811
 144 
 2,531
 16,136
 117
 540
 200,000
 5,745
 Israel
 15,589
 +34
 208 
 +4
 7,375
 8,006
 1,801
 302,691
 34,971
 Mexico
 15,529
 +852
 1,434 
 +83
 9,086
 5,009
 378
 120
 11
 71,103
 551
 Austria
 15,357
 569 
 +20
 12,580
 2,208
 140
 1,705
 63
 232,537
 25,819
 Singapore
 14,951
 +528
 14 
 1,095
 13,842
 20
 2,556
 121,774
 20,815
 Pakistan
 14,079
 +164
 301 
 +9
 3,233
 10,545
 111
 64
 157,223
 712
 Chile
 13,813
 198 
 7,327
 6,288
 426
 723
 10
 161,235
 8,434
 Japan
 13,614
 385 
 1,899
 11,330
 300
 108
 150,692
 1,191
 Belarus
 12,208
 +919
 79 
 1,993
 10,136
 92
 1,292
 161,028
 17,041
 Poland
 12,089
 +187
 570 
 +8
 2,655
 8,864
 160
 319
 297,859
 7,870
 Romania
 11,616
 +277
 650 
 3,404
 7,562
 243
 604
 150,309
 7,813
 UAE
 11,380
 +541
 89 
 2,181
 9,110
 1,151
 1,057,326
 106,904
 Qatar
 11,244
 10 
 1,066
 10,168
 3,903
 85,709
 29,749
 S. Korea
 10,752
 244 
 8,854
 1,654
 55
 210
 608,514
 11,869
 Indonesia
 9,511
 +415
 773 
 1,254
 7,484
 35
 79,618
 291
 Ukraine
 9,410
 +401
 239 
 +19
 992
 8,179
 121
 215
 98,719
 2,257
 Denmark
 8,851
 +153
 427 
 5,959
 2,465
 1,528
 74
 166,846
 28,805
 Serbia
 8,275
 162 
 1,209
 6,904
 85
 947
 19
 67,917
 7,773
 Philippines
 7,958
 +181
 530 
 975
 6,453
 31
 89,889
 820
 Norway
 7,599
 206 
 32
 7,361
 52
 1,402
 164,316
 30,310
 Czechia
 7,449
 223 
 2,842
 4,384
 696
 226,255
 21,128
 Australia
 6,731
 +11
 84 
 5,626
 1,021
 42
 530,679
 20,811
 Bangladesh
 6,462
 +549
 155 
 +3
 139
 6,168
 39
 0.9
 54,733
 332
 Dominican Republic
 6,293
 282 
 993
 5,018
 144
 580
 26
 22,498
 2,074
 Panama
 6,021
 +242
 167 
 +2
 455
 5,399
 89
 1,395
 27,834
 6,451
 Malaysia
 5,851
 +31
 100 
 4,032
 1,719
 36
 181
 144,686
 4,470
 Colombia
 5,597
 253 
 1,210
 4,134
 118
 110
 90,899
 1,786
 South Africa
 4,793
 90 
 1,473
 3,230
 81
 178,470
 3,009
 Egypt
 4,782
 337 
 1,236
 3,209
 47
 90,000
 879
 Finland
 4,740
 +45
 193 
 2,500
 2,047
 56
 855
 85,784
 15,482
 Morocco
 4,246
 +126
 163 
 739
 3,344
 115
 30,368
 823
 Argentina
 4,003
 197 
 1,140
 2,666
 51,900
 1,148
 Luxembourg
 3,729
 88 
 3,123
 518
 5,957
 141
 39,102
 62,465
 Algeria
 3,517
 432 
 1,558
 1,527
 40
 80
 6,500
 148
 Moldova
 3,481
 102 
 925
 2,454
 212
 863
 25
 11,763
 2,916
 Kuwait
 3,440
 +152
 23 
 2,241
 1,176
 67
 806
 179,000
 41,915
 Kazakhstan
 2,982
 +147
 25 
 725
 41
 159
 216,276
 11,518
 Thailand
 2,938
 54 
 2,652
 232
 0.8
 178,083
 2,551
 Bahrain
 2,810
 +87
 8 
 1,246
 1,556
 1,651
 121,260
 71,263
 Hungary
 2,649
 +66
 291 
 516
 1,842
 49
 274
 30
 67,172
 6,953
 Greece
 2,534
 136 
 577
 1,821
 43
 13
 66,094
 6,341
 Oman
 2,131
 +82
 364
 1,757
 417
 Croatia
 2,039
 59 
 1,166
 814
 497
 14
 32,817
 7,994
 Uzbekistan
 1,939
 +35
 958
 973
 58
 0.2
 242,536
 7,247
 Armenia
 1,867
 +59
 30 
 866
 971
 630
 19,296
 6,512
 Iraq
 1,847
 1,286
 473
 46
 73,782
 1,834
 Afghanistan
 1,828
 +125
 58 
 228
 1,542
 9,000
 231
 Iceland
 1,792
 1,624
 158
 5,251
 29
 46,377
 135,906
 Cameroon
 805
 842
 12
 Azerbaijan
 1,678
 22 
 1,162
 494
 165
 128,807
 12,704
 Estonia
 1,660
 +13
 50 
 240
 1,370
 1,251
 49,527
 37,336
 Bosnia and Herzegovina
 1,585
 63 
 682
 840
 483
 27,603
 8,413
 Ghana
 1,550
 11 
 155
 1,384
 50
 0.4
 100,622
 3,238
 New Zealand
 1,472
 19 
 1,214
 239
 305
 126,066
 26,143
 Slovenia
 1,408
 86 
 1,099
 677
 50,290
 24,190
 North Macedonia
 1,399
 65 
 553
 781
 672
 15,120
 7,257
 Cuba
 1,389
 56 
 525
 808
 123
 41,651
 3,677
 Bulgaria
 1,387
 +24
 222
 1,107
 200
 27,000
 3,886
 Slovakia
 20 
 423
 941
 253
 75,866
 13,896
 Lithuania
 1,344
 44 
 536
 764
 17
 16
 111,809
 41,072
 Nigeria
 1,337
 40 
 255
 1,042
 10,918
 53
 Ivory Coast
 1,164
 499
 651
 44
 0.5
 Guinea
 1,163
 7 
 246
 910
 Hong Kong
 1,038
 4 
 811
 145,640
 19,426
 Djibouti
 1,035
 2 
 477
 556
 1,048
 12,250
 12,399
 Bolivia
 1,014
 +64
 53 
 98
 87
 5,791
 496
 Tunisia
 967
 39 
 279
 649
 18
 82
 21,081
 1,784
 Latvia
 836
 +18
 13 
 267
 443
 51,561
 27,336
 Cyprus
 822
 15 
 659
 681
 50,220
 41,595
 Albania
 750
 431
 289
 261
 7,758
 2,696
 Andorra
 743
 385
 318
 9,616
 1,673
 21,653
 Senegal
 736
 9 
 284
 466
 28
 Lebanon
 717
 24 
 145
 548
 105
 30,879
 4,524
 Diamond Princess
 54
 Kyrgyzstan
 708
 416
 109
 45,627
 6,994
 Honduras
 702
 +41
 64 
 79
 559
 71
 3,643
 368
 Niger
 701
 29 
 287
 5,013
 207
 Costa Rica
 697
 6 
 404
 137
 12,670
 2,487
 Burkina Faso
 635
 42 
 469
 124
 Uruguay
 620
 386
 219
 178
 17,505
 5,039
 Sri Lanka
 592
 7  
 134
 451
 0.3
 17,715
 827
 San Marino
 538
 41 
 433
 15,856
 1,208
 2,082
 61,360
 Guatemala
 530
 +30
 7,200
 402
 Channel Islands
 35 
 352
 3,020
 201
 5,342
 30,725
 Georgia
 511
 156
 349
 128
 11,504
 2,884
 Somalia
 480
 26 
 440
 DRC
 471
 Malta
 458
 303
 151
 1,037
 30,528
 69,139
 Jordan
 449
 342
 100
 63,737
 6,247
 Taiwan
 429
 307
 116
 61,684
 2,590
 Réunion
 418
 467
 Mali
 408
 113
 272
 2,172
 107
 Mayotte
 401
 1,470
 9,164
 Jamaica
 328
 3,621
 1,223
 Kenya
 363
 114
 235
 17,992
 335
 El Salvador
 345
 +22
 97
 21,364
 3,294
 Palestine
 83
 257
 5,293
 Mauritius
 334
 302
 263
 14,445
 11,358
 Venezuela
 329
 142
 177
 440,966
 15,507
 Montenegro
 321
 189
 125
 5,085
 8,096
 Isle of Man
 308
 248
 3,622
 2,903
 34,140
 Tanzania
 299
 48
 241
 Sudan
 275
 Vietnam
 270
 212,965
 2,188
 Equatorial Guinea
 258
 1 
 184
 854
 609
 Maldives
 245
 453
 5,296
 9,798
 Paraguay
 230
 95
 126
 7,925
 1,111
 Gabon
 211
 3 
 724
 325
 Congo
 180
 Rwanda
 93
 6,959
 537
 Faeroe Islands
 187
 3,827
 6,730
 137,732
 Martinique
 175
 77
 84
 37
 Guadeloupe
 149
 12 
 372
 Myanmar
 5 
 0.09
 7,215
 133
 Gibraltar
 131
 4,185
 2,198
 65,240
 Brunei 
 315
 13,149
 30,056
 Liberia
 16 
 Madagascar
 75
 2,357
 Ethiopia
 0.03
 15,668
 136
 French Guiana
 415
 Cambodia
 122
 119
 11,721
 Trinidad and Tobago
 59
 1,572
 1,123
 Bermuda
 60
 1,766
 96
 1,682
 27,008
 Cabo Verde
 106
 196
 791
 1,423
 Aruba
 937
 1,626
 15,230
 Togo
 99
 62
 6,141
 742
 Sierra Leone
 Monaco
 2,421
 102
 Zambia
 5,284
 Liechtenstein
 2,151
 900
 23,605
 Bahamas
 203
 Barbados
 278
 1,751
 6,093
 Uganda
 27
 27,432
 600
 Haiti
 76
 776
 68
 Mozambique
 1,688
 Sint Maarten
 33
 1,726
 266
 6,204
 Guyana
 51
 94
 464
 590
 Guinea-Bissau
 1,500
 762
 Cayman Islands
 1,065
 17,468
 Eswatini
 65
 714
 615
 Benin
 0.08
 Libya
 1,623
 236
 French Polynesia
 206
 8,241
 Nepal
 54,826
 1,882
 CAR
 Chad
 Macao
 69
 Syria
 Eritrea
 Saint Martin
 983
 78
 Mongolia
 7,292
 2,224
 Malawi
 690
 Zimbabwe
 23
 6,834
 460
 Angola
 0.06
 Antigua and Barbuda
 1,154
 Timor-Leste
 322
 244
 Botswana
 6,016
 2,558
 Laos
 1,815
 249
 Belize
 936
 2,354
 Fiji
 1,007
 Grenada
 1,555
 New Caledonia
 4,456
 15,608
 Curaçao
 304
 1,853
 Dominica
 383
 5,320
 Namibia
 704
 277
 Saint Kitts and Nevis
 282
 293
 5,508
 Saint Lucia
 2,429
 St. Vincent Grenadines
 135
 Nicaragua
 Falkland Islands
 3,736
 353
 101,437
 Turks and Caicos
 310
 2,144
 Burundi
 Montserrat
 2,204
 7,212
 Greenland
 1,200
 21,138
 Seychelles
 112
 Gambia
 166
 Suriname
 689
 MS Zaandam
 Vatican City
 11,236
 Papua New Guinea
 Sao Tome and Principe
 Mauritania
 1,032
 Bhutan
 9,865
 12,785
 British Virgin Islands
 198
 St. Barth
 607
 South Sudan
 Western Sahara
 Caribbean Netherlands
 191
 4,195
 Anguilla
 Saint Pierre Miquelon
 173
 Yemen
 China
 82,836
 4,633 
 77,555
 648
 Total:
 395.3
 483,226
 +10,951
 17,588
 +345
 236,575
 229,063
 5,981
 1,090,796
 +26,193
 61,549
 +1,594
 168,606
 860,641
 15,449
 1,301,271
 +24,463
 123,997
 +2,112
 446,464
 730,810
 24,907
 144,174
 +6,254
 +441
 51,611
 85,833
 9,772
 8,291
 +5
 6,844
 1,345
 34,036
 +1,357
 1,468
 +39
 10,575
 21,993
 3,062,515
 +69,223
 211,449
 +4,532
 921,320
 1,929,746
 56,297
 393
 27.1
 82,830
 77,474
 1,010,356
 +23,196
 56,797 
 +1,384
 138,990
 814,569
 3,052
 229,422
 +2,793
 23,521 
 +331
 120,832
 85,069
 4,907
 503
 +1,739
 +333
 +3,742
 +437
 +988
 +150
 +4,309
 +360
 +2,131
 +95
 91,472
 +991
 5,806 
 +96
 70,933
 14,733
 3,011
 1,089
 432,329
 5,147
 87,147
 +6,198
 794 
 +47
 7,346
 79,007
 597
 3,019,434
 20,690
 66,501
 +3,642
 4,543 
 +272
 30,816
 313
 291,922
 1,373
 +1,605
 717,451
 19,009
 46,687
 +553
 7,207 
 +113
 10,878
 28,602
 903
 4,028
 622
 214,042
 18,468
 +400
 +43
 +1,561
 +58
 665,819
 482
 29,164
 +103
 1,665 
 +55
 5,299
 3,370
 192
 245,300
 28,343
 +1,182
 +54
 +163
 +25
 +521
 +386
 +15
 +286
 +80
 +1,289
 15,555
 +112
 204 
 8,151
 129
 1,797
 15,274
 +49
 549 
 12,362
 2,363
 1,696
 14,677
 +835
 1,351 
 +46
 8,354
 4,972
 67,635
 14,423
 +799
 13,314
 13,915
 +587
 292 
 3,029
 10,594
 150,756
 +482
 +165
 11,902
 +285
 562 
 +27
 2,466
 8,874
 314
 11,339
 +303
 641 
 3,141
 7,557
 227
 589
 143,834
 7,477
 11,289
 +826
 75 
 1,740
 9,474
 1,195
 153,845
 16,281
 +957
 10,839
 +490
 82 
 2,090
 8,667
 1,096
 10,738
 +10
 243 
 8,764
 1,731
 209
 601,660
 11,735
 9,096
 +214
 765 
 7,180
 75,157
 9,009
 +392
 220 
 864
 93,519
 2,138
 8,698
 +123
 2,312
 1,502
 155,810
 26,900
 +233
 7,777
 +198
 511 
 932
 6,334
 +72
 205 
 7,362
 7,445
 2,826
 4,396
 695
 218,474
 20,401
 6,720
 83 
 5,586
 1,051
 517,063
 20,277
 +158
 5,913
 +497
 152 
 5,630
 50,401
 306
 5,820
 +40
 99 
 3,957
 1,764
 5,779
 +241
 165 
 369
 5,245
 1,339
 26,642
 6,175
 +218
 +247
 +248
 4,695
 +119
 2,002
 847
 82,437
 14,878
 4,120
 3,263
 29,254
 793
 +111
 49,905
 1,104
 +135
 3,288
 +213
 1,012
 2,254
 770
 2,931
 52 
 2,609
 2,835
 +118
 720
 205,560
 10,948
 2,723
 +76
 1,218
 1,497
 1,600
 117,374
 68,980
 2,583
 280 
 498
 1,805
 65,625
 +17
 2,049
 +51
 1,675
 1,904
 892
 1,004
 57
 1,808
 +62
 848
 931
 610
 18,547
 6,259
 +84
 1,703
 +172
 57 
 220
 1,426
 8,694
 +33
 1,647
 233
 1,364
 1,242
 48,406
 36,491
 1,565
 60 
 846
 26,822
 8,175
 1,469
 1,180
 123,920
 25,698
 1,449
 474
 934
 532
 106,775
 39,223
 221
 1,098
 674
 49,607
 23,862
 39,828
 3,516
 1,381
 18 
 403
 960
 74,099
 13,572
 1,363
 +167
 787
 247
 950
 818
 434
 49,235
 26,103
 28 
 422
 286
 256
 7,491
 2,603
 +65
 710
 541
 104
 29,367
 4,303
 292
 661
 61 
 521
 606
 375
 216
 174
 17,063
 4,912
 588
 15,240
 500
 436
 +44
 444
 459
 381
 450
 1,019
 29,456
 66,712
 290
 350
 3,262
 323
 226
 20,109
 3,100
 +38
 7,630
 1,070
 +16
 6,588
 134,826
 6,987
 14,588
 127
 11,576
 692
 1,532
 5,951
 719
 2,586
 22,318
 488
 1,644
 769
 940
 14,303
 51,367
 1,763
 617
 6,395
 430
 5,023
 2,136
 1,796
 3,952
 260
 4,887
 9,829
 12,738
 392.9
 Highlighted in green
 = all cases have recovered from the infection
 Highlighted in grey
 = all cases have had an outcome (there are no active cases)
 [back to top ↑]
 New York State Governor Cuomo said that preliminary findings from an antibody study conducted on 3,000 people at grocery stores across New York State found a 13.9% had coronavirus antibodies, suggesting a 13.9% actual infection rate statewide (21.2% in New York City), which translates to an estimate of about 2,700,000 actual cases in New York State (10 times more than the about 270,000 cases that have been detected and reported officially). Governor Cuomo acknowledged that the official count reported by New York State (which still is not including probable deaths as recommended by the new CDC guidelines) of about 15,500 deaths is "not accurate" as it doesn't account for stay at home deaths. Based on Worldometer's count (which includes probable deaths reported by New York City) of about 21,000 deaths and the 2,700,000 case estimate from the new antibody study, the actual case fatality rate in New York State could be at around 0.78% [source]
 NOTE: Montgomery County (PA) has lowered the reported number of deaths substantially, affecting today's count as well as the previous days statewide figures.
 The French Government has corrected its case numbers after a quality and verification process of data from EMS & EHPAD establishments highlighted over-reporting and overestimation of cumulative cases [source]. To reflect the correction made by the French Government, Worldmeter has adjusted the numbers for today and historically. [source]
 NOTE: Case total remains at 1451 as 3 of yesterday's cases were officially retracted as already recorded elsewhere. Yesterday's total has been changed to 1448. 
 Death of a 62-year-old woman with an underlying condition. She had been intubated and sedated for the last 2 weeks. Arrangements were made to allow her family to be with her when she died. Director-general of health Dr Ashley Bloomfield addressed concerns that the family had been given false hope when the woman's condition was reported as stable earlier in the week. The term "stable" meant there had been no change in her condition, he explained. On Tuesday, Bloomfield said at his daily press conference none of the patients in hospital were critical; yesterday he said they were stable, according to NZ Herald. Prime Minister Jacinda Ardern said the Government was considering changing the way it reported patients' conditions.  [source] [source] [source]
 January Timeline:
 There are three parameters to understand in order to assess the magnitude of the risk posed by this novel coronavirus: 
 The attack rate or transmissibility (how rapidly the disease spreads) of a virus is indicated by its reproductive number (Ro, pronounced R-nought or r-zero), which represents the average number of people to which a single infected person will transmit the virus.
 WHO's estimated (on Jan. 23) Ro to be between 1.4 and 2.5. [13]
 Other studies have estimated a Ro between 3.6 and 4.0, and between 2.24 to 3.58. [23]. 
 Preliminary studies had estimated Ro to be between 1.5 and 3.5. [5][6][7]
 An outbreak with a reproductive number of below 1 will gradually disappear.
 For comparison, the Ro for the common flu is 1.3 and for SARS it was 2.0.
 See full details: Coronavirus Fatality Rate
 The novel coronavirus' case fatality rate has been estimated at around 2%, in the WHO press conference held on January 29, 2020 [16] . However, it noted that, without knowing how many were infected, it was too early to be able to put a percentage on the mortality rate figure.
  A prior estimate [9] had put that number at 3%. 
 Fatality rate can change as a virus can mutate, according to epidemiologists.
 For comparison, the case fatality rate for SARS was 10%, and for MERS 34%.
 See full details: COVID-19 Coronavirus Incubation Period 
 Symptoms of COVID-19 may appear in as few as 2 days or as long as 14 (estimated ranges vary from 2-10 days, 2-14 days, and 10-14 days, see details), during which the virus is contagious but the patient does not display any symptom (asymptomatic transmission).
  See latest findings: Age, Sex, Demographics of COVID-19 Cases and Deaths 
 According to early estimates by China's National Health Commission (NHC), about 80% of those who died were over the age of 60 and 75% of them had
 pre-existing health conditions such as cardiovascular diseases and
 diabetes.[24]
 According to the WHO Situation Report no. 7 issued on Jan. 27:
 A study of 138 hospitalized patients with NCIP found that the median age was 56 years (interquartile range, 42-68; range, 22-92 years) and 75 (54.3%) were men.[25] 
 The WHO, in its Myth busters FAQs, addresses the question: "Does the new coronavirus affect older people, or are younger people also susceptible?" by answering that: 
 As of Jan. 29, according to French authorities, the conditions of the two earliest Paris cases had worsened and the patients were being treated in intensive care, according to French authorities. The patients have been described as a young couple aged 30 and 31 years old, both Chinese citizens from Wuhan who were asymptomatic when they arrived in Paris on January 18 [19].
  The NHC reported the details of the first 17 deaths up to 24 pm on January 22, 2020. The deaths included 13 males and 4 females. The median age of the deaths was 75 (range 48-89) years.[21]
 See full details: WHO coronavirus updates
 On January 30, the World Health Organization declared the coronavirus outbreak a Global Public Health Emergency.
 For more information from the WHO regarding novel coronavirus: WHO page on Novel Coronavirus (2019-nCoV) 
 https://www.worldometers.info/coronavirus/
