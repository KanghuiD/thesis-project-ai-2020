	Coronavirus | Financial Advice for Businesses - Antrim & Newtownabbey Borough Council
 The Department for the Economy has announced a range of support packages for businesses throughout Northern Ireland via the NI Executive which is explained in further detail via the links below.
 ​Invest NI can also provide advice and support through its Business Support Line on T. 0800 181 4422
 We understand that many businesses have questions regarding tax and we would like direct you to GOV.UK  or the HMRC helpline on T. 0800 0159 559 for guidance.
 Summary of Additional Support Announced for Businesses & the Self Employed
 On 20 March, the Chancellor announced new steps to provide support for businesses affected by the coronavirus.
 These measures include:
 > Coronavirus Job Retention Scheme
 > Deferral on VAT payments
 > Increase in the Universal Credit allowance
 > Extension to the interest-free period for the Coronavirus Business Interruption Loan Scheme
 Business Support Grant Schemes
 The Northern Ireland Executive has announced two grants will be available to local businesses impacted by the coronavirus.  
 > A grant of £10,000 will be provided to all small businesses who are eligible for the Small Business Rate Relief Scheme (ie all businesses with a NAV up to £15,000). This will benefit around 27,000 businesses.
 			For rental properties, the scheme is being designed to benefit the small businesses and not the landlord or managing agent who is responsible for paying the rates. For this reason, no grant payments are being automatically issued to landlords or managing agents of properties with a rateable value of £1,590 or less. Landlords, managing agents and small businesses in this situation should not use the Small Business Grant Support online form to register for the scheme, but should wait until a new online form for rental properties is made available in the coming days.
 			 
 > A grant of £25,000 will be provided to companies in the retail, tourism and hospitality sectors with a rateable value between £15,000 and £51,000. This should assist 4,000 business. Applications are now open. 
 Frequently Asked Questions
 Support available through the Coronavirus Job Retention Scheme
 Under the Coronavirus Job Retention Scheme, all UK employers will be able to access support to continue paying part of their employees’ salary for those employees that would otherwise have been laid off during this crisis.
 NI Chamber is also hosting a live webinar, in partnership with Pinsent Masons, on Thursday 9 April. The webinar will provide an update on the Coronavirus Job Retention Scheme and other measures employers are considering during this uncertain period in dealing with short term and longer term employment structures, including dealing with the uncertain areas under the CJRS.
 			Please note, this is for NI Chamber members only.
 Invest NI - Job Retention Scheme Webinar
 			Thursday 9 April, 11am-12pm
 			Employment law specialists A&L Goodbody will deliver an hour-long live webinar on the UK Government’s Coronavirus Job Retention Scheme (JRS). They will discuss details of the Scheme, key considerations and concerns, and how and when businesses can access the support. They will also provide an overview of the Self-employment Income Support Scheme and other tax deferral measures.
 Coronavirus Business Interruption Loan Scheme for Small Businesses (CBILS)
 CBILS is a new scheme, announced by the Chancellor at Budget 2020, that can provide facilities of up to £5m for smaller businesses across the UK who are experiencing lost or deferred revenues, leading to disruptions to their cashflow.
 			There have also been some changes to the Coronavirus Business Interruption Loan Scheme - for small businesses
 Coronavirus Large Business Interruption Loan Scheme
 Provides a government guarantee of 80% to enable banks to make loans of up to £25m to firms with an annual turnover of between £45m and £500m.
 Coronavirus: Deferred VAT payments
 The government will be automatically deferring business payments for Valued Added Tax (VAT).
 The deferral will apply from 20 March 2020 until 30 June 2020.
 NOTE: you still must file VAT returns as normal
 Coronavirus: Deferred Income Tax Payments
 If you’re self-employed, Income Tax payments due in July 2020 under the Self-Assessment system will be deferred to January 2021.
 For Income Tax Self-Assessment, payments due on the 31 July 2020 will be deferred until the 31 January 2021.
 Coronavirus: Support for Paying Sick Pay to Employees
 Sign up to hear the latest news and event information from Antrim and Newtownabbey Council.
 Civic Centre,
 50 Stiles Way,
 Antrim BT41 2UB
 T: 028 9446 3113
 Mossley Mill,
 Newtownabbey
 BT36 5QA
 T: 028 9034 0000
 Key To Map
 https://antrimandnewtownabbey.gov.uk/coronavirus-financial-advice-for-businesses/
