Coronavirus: 3,060 dead globally as world economy hit
 Thank you for your reading and interest in the news Coronavirus: 3,060 dead globally as world economy hit and now with details
 Seoul/Beijing: The coronavirus continued to exact a toll worldwide with the number of deaths now crossing 3,000. Almost 90,000 people have been infected by the disease.
 The coronavirus spread further in Asia, Europe and the Middle East. The virus has made its presence felt in 53 countries, with more than 6,500 cases and more than 100 deaths.
 But this figure is a conservative estimate.
 Iran, which has officially said the virus has killed 66 people among 1,501 confirmed cases, is widely suspected of under-reporting deaths and infections.
 South Korea, one of the worst victims of the virus outside China, sought murder charges against leaders of a secretive church at the heart of its outbreak on Monday. However,the Chinese province at the epicentre reported a fall in new cases.
 World stock markets regained some calm as hopes for global interest rate cuts to soften the economic blow of the virus steadied nerves after last weekâs worst plunge since the 2008 financial crisis.
 The global death toll was up to 3,060.
 Reuters reported that in the largest outbreak outside China, South Korea has had 26 deaths and reported another 599 infections on Monday, taking its tally to 4,335 following Saturdayâs biggest daily jump.
 Of the new cases in South Korea, 377 were from the city of Daegu, home to a branch of the Shincheonji Church of Jesus, to which most of South Koreaâs cases have been traced after some members visited Chinaâs Wuhan city where the disease emerged.
 The Seoul government asked prosecutors to launch a murder investigation into leaders of the church, a movement that reveres founder Lee Man-hee. Lee knelt and apologised to the country on Monday that one church member had infected many others, calling the epidemic a âgreat calamityâ.
 British health authorities said on Monday there had been four more confirmed cases of coronavirus, bringing the total to 40.
 Chris Whitty, the Chief Medical Officer of England, said the four new patients had recently travelled from Italy.
 Earlier on Monday, British Prime Minister Boris Johnson said the country needed to be prepared for the new coronavirus to spread further.
 In the United States, there were new signs that the coronavirus was spreading undetected in some communities for weeks while the government resisted calls for more widespread testing. With testing now ramping up, the Seattle suburb of Kirkland has become an epicentre of both illness and fear, much of it focused on a nursing facility where six coronavirus cases have been confirmed and many more residents and employees have complained of illness.
 Wuhan, at the centre of the epidemic in Hubei province, closed the first of 16 specially built hospitals, hurriedly put up to treat people with the virus, after it discharged its last recovered patients, state broadcaster CCTV said on Monday.
 News of the closure coincided with a steep fall in new cases in Hubei, but China remained on alert for people returning home with the virus from other countries.
 âThe rapid rising trend of virus cases in Wuhan has been controlled,â Mi Feng, a spokesman for Chinaâs National Health Commission, told a briefing.
 âOutbreaks in Hubei outside of Wuhan are curbed and provinces outside of Hubei are showing a positive trend.â The virus broke out in Wuhan late last year and has since infected more than 86,500 people, most in China.
 Global factories took a beating in February from the outbreak, with activity in China shrinking at a record pace, surveys showed, raising the prospect of a coordinated policy response by central banks.
 The global spread has forced the postponement of festivals, exhibitions, trade fairs and sports events. It has crippled tourism, retail sales and global supply chains, especially in China, the worldâs second-largest economy.
 Global airlines stand to lose $1.5 billion this year due to the virus.
 The Organisation for Economic Cooperation and Development warned that the outbreak was pitching the world economy into its worst downturn since the global financial crisis, urging governments and central banks to fight back.
 âWith inputs from agencies
 It is also worth noting that the original news has been published and is available at Gulf News and the editorial team at AlKhaleej Today has confirmed it and it has been modified, and it may have been completely transferred or quoted from it and you can read and follow this news from its main source.
 https://alkhaleejtoday.co/international/51542/Coronavirus-3060-dead-globally-as-world-economy-hit.html
