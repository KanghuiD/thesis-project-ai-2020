How might Covid-19 affect apparel sourcing and trade | Apparel Industry Blog | just-style
  Hannah Abdulla | 8 April 2020
 The fast spread of the coronavirus around the globe has created an unprecedented situation for the world economy. But how might Covid-19 affect apparel sourcing and trade? 
 Bangladesh could stand to lose around US$6bn in export revenues as brands and retailers continue to cancel orders from the apparel sourcing major.
 The Vietnamese textile and garment sector could see a VND11trn (US$467m) hit from the fallout of coronavirus, one of the country's main textile groups has said.
 Tunisia's textile and garment sector is warning of major commercial and humanitarian damage because of production stoppages and order cancellations resulting from the ongoing coronavirus crisis.
 Romania's clothing and textile industry is facing a recruitment crunch and experts worry that it will struggle to find a strategy to hire sustainably to ensure long-term growth.
 Amidst the chaos of order cancellations and payment delays experienced by clothing manufacturers around the world due to the coronavirus crisis, the good news is that some brands and retailers are continuing to offer support to their suppliers.
 UK value fashion retailer Primark has set up a fund to cover the wages component of orders that it cancelled in Bangladesh, Cambodia, India, Myanmar, Pakistan, Sri Lanka and Vietnam because of the coronavirus pandemic.
 And Spanish fashion giant Inditex, which owns the Zara fashion chain, is to pay its Spanish workers their full salaries until 15 April – including those in its domestic factories.
 After deteriorating at the quickest pace on record in February, factory activity in China rebounded in March – but orders fell as the pandemic worsened across the rest of the world.
 A textile trade body in China has said the number of firms cancelling export orders is growing by the day, leading to increasing pressure on upstream firms' supply chains.
 While US footwear brands are seeing a slump in consumer demand as the fallout from the coronavirus outbreak continues.
 US apparel and footwear retailers and importers are cautiously optimistic at reports the Trump Administration is planning to stop collecting duties on imports for at least three months to help ease the economic fallout of the coronavirus.
 The International Apparel Federation (IAF) is adding to calls for solidarity and collaboration by the apparel supply chain and its stakeholders – including governments and consumers – to reduce the damage to suppliers in the face of the coronavirus crisis.
 At this time of crisis it's critically important that apparel brands and retailers avoid taking any drastic action that could sink the entire supply chain.
 The fashion industry needs to re-think how it does business, in light of the impact of Covid-19, and purchasing practices must be reformed for social and environmental sustainability, a new academic paper has suggested.
 Are we witnessing the end of globalisation? The simple answer is no. But there is a more nuanced response: globalisation will survive, but it will be different from what we've known.
 Resources have been launched to help governments, suppliers and workers navigate the Covid-19 pandemic.
 And just-style has a series of running articles to keep you up-to-date with the latest industry happenings related to Covid-19
 Sectors: Apparel, Fibres & fabrics, Finance, Footwear, Manufacturing, Mergers & acquisitions, Retail, Social & environmental responsibility, Sourcing, Technology, Trade
 NEWS
 Esquel shutters some factories as orders fall
 BLOG
 Industry efforts to support garment factories & workers through Covid-19
 Apparel brands and retailers – including Adidas, C&A, H&M Group, Inditex, M&S and Primark – have joined employer organisations, unions and the International Labour Organization (ILO) to push for emerg...
 5 ways to overhaul the apparel supply chain for the future
 Concerns mount as Covid-19 impacts supplier orders
 The coronavirus (Covid-19) continued to dominate our news coverage last week as supplier orders continue to be cancelled, apparel sales continue to plummet, and industry jobs remain on the line....
 Timeline – How coronavirus is impacting the global apparel industry – FREE TO READ
 Preparing fashion for a post-coronavirus world
 While there are no playbooks to guide the apparel industry through the Covid-19 crisis and beyond, thoughts are turning to ways to prepare for a post-coronavirus world....
 Pandemic risks garment workers in Eastern Europe
 Covid-19 leads to countrywide lockdowns
 The coronavirus pandemic continues to cause chaos throughout the global apparel industry, with countrywide lockdowns and the switch of production to personal protective equipment (PPE) all dominating ...
 Most Popular
 Insights
 News
 Market research
 Login | 
 Join
 Password
 Forgot your password?
  Menu
 Login
  MAGAZINE
 Buy apparel market research
 New apparel market research
 Apparel sector market research
 Apparel companies market research
 Apparel market research publishers
 Apparel industry consultants
 PLM software buyers' guide
 Apparel industry webinars
  Magazine
 Advertise on just-style
 Site map
 About just-style
 Published by Aroq Ltd. Address: Aroq House, 17A Harris Business Park, Bromsgrove, Worcs, B60 4DJ, UK. 
 Tel:  Intl +44 (0)1527 573 600. Toll Free from US: 1-866-545-5878. Fax: +44 (0)1527 577423.
 Registered Office: John Carpenter House, John Carpenter Street, London, EC4Y 0AN, UK | Registered in England No:  4307068.
 Aroq publishes: just-auto.com | just-food.com | just-drinks.com | just-style.com
 Get full access to all content, just €1 for 30 days
 just-style gives you the widest apparel and textile market coverage.
 But only paid just-style members have full, unlimited access to all our exclusive content - including 20 years of archives.
 I am so confident you will love complete access to our content that today I can offer you 30 days access for €1*.
 It’s our best ever membership offer – just for you.
 Leonie Barrie, editor of just-style
 Get 30 days for €1* 
 * plus VAT if applicable
  Daily Alert: 
                  Get the daily apparel and textile news as it happens
                Weekly News Summary: 
                  Get our weekly best-of roundup
                Apparel and textile Research Updates: 
                  Discover the latest apparel and textile research
 Sign up today 
  Weekly News Summary: 
 Apparel and textile Research Updates: 
 I want to hear about apparel and textile industry webinars and relevant offers
 https://www.just-style.com/the-just-style-blog/how-might-covid-19-affect-apparel-sourcing-and-trade_id2624.aspx
