Coronavirus Australia live blog: Newmarch House death toll soars
 Tragic news has emerged from a Sydney aged care home riddled with coronavirus, with four more residents dying in the past 20 hours.
 		The death toll at a coronavirus-plagued nursing home in Sydney has jumped sharply, with four more residents dying since last night.	
 Newmarch House in Caddens in the city’s west has now lost 11 residents in the past two weeks from COVID-19.
 More than 50 staff and residents have tested positive since the outbreak was detected, which began when an infected employee worked for several days before being diagnosed.
 The shocking news comes as multiple states confirmed their plans earlier on Tuesday to begin relaxing some social distancing restrictions. Although, not all jurisdictions are on board.
 Australia has now recorded 6728 confirmed cases of COVID-19, including 3009 in New South Wales, 1351 in Victoria, 1033 in Queensland, 438 in South Australia, 550 in Western Australia, 214 in Tasmania, 106 in the Australian Capital Territory and 28 in the Northern Territory.
 A total of 88 deaths have been recorded nationally.
 And more from Queensland – two police officers have been fined for breaching social distancing restrictions.
 The sergeant and senior constable from the Southern Region district were each slapped with $1300 fines for attending an “outdoor gathering” in Toowoomba on Sunday, Queensland Police said in a statement.
 They were among 12 people in attendance.
 "A third person has also been issued with an infringement notice. Investigations are continuing to identify another 12 people believed to have attended the gathering," Queensland Police said.
 A Queensland politician is in hot water for breaching coronavirus social distancing rules.
 Opposition Leader Deb Frecklington has released a statement confirming she’s accepted Mr Watts’ resignation after he was fined by police for a “gathering” at the weekend.
 “I expect the highest standards from my team, especially my shadow ministers,” Ms Frecklington said.
 Labor politician Joel Fitzgibbon was on 2SM Radio this morning and savaged Barnaby Joyce over the Nationals MP's opposition to the COVIDsafe app.
 Mr Joyce was on Sunrise yesterday when he gave a rambling explanation of why he’s not on board with the health tracking effort.
 “You know, I live in an area where all the time we have things getting hacked,” he told the Channel 7 program. 
 “Obviously there’d be people out there trying to decompile the data to recode what they want to re-inject it to try and create man-in-the-middle sites.”
 That obviously makes absolutely no sense. But Mr Joyce continued.
 “I mean, obviously, McDonald’s can tell you where I am, and this app will tell you who I’m speaking to. And look, it’s a free choice.”
 “He threw out these assertions about people hacking into telephones, etcetera. But Barnaby’s problem is, he’d have at least six apps on his phone right now that would expose him more to those sorts of security issues then would, in my view, the coronavirus app.”
 Mr Fitzgibbon accused Mr Joyce and other COVIDsafe opponents of engaging in “populism”.
 “Barnaby is Barnaby and he always hunts down that four per cent of the votes that the National Party needs to stay relevant in the parliament.”
 Ouch.
 “I'd do anything reasonable and safe to further stop the spread of the virus to get our lives back to some form of normality, and of course to get the economy going again.”
 In terrible news out of Sydney, a coronavirus-plagued nursing home has just released a statement confirming four more residents have died since last night.
 Overall, 11 people have now passed away from coronavirus at the facility in Caddens in the city's west in the past two weeks.
 Anglicare, which runs the home, said it is "deeply saddened" by the latest deaths.
 "We extend our deepest sympathies to these families for the losses they are experiencing," it said.
 More than 50 staff and residents have tested positive for COVID-19 since the serious outbreak began, which was sparked when an unwell employee worked for six days before being diagnosed.
 "On the best advice available, we know that it will be some weeks before the home is clear of the virus," Anglicare said. 
 "For those residents with a positive COVID-19 diagnosis, NSW Health has provided the support and guidance of Nepean Blue Mountains Local Area Health Network and an Infectious Diseases Specialist to ensure residents receive the best medical care possible."
 The Federal Government was forced to intervene last week when it emerged that Newmarch House was significantly understaffed.
 Loved ones of residents voiced their concerns about the quality of care being offered.
 "This is a tragic time not only for the families who have lost their loved ones but for other residents and families," Anglicare said. 
 "It is also taking a deep toll on our staff who cared for and knew these residents and families so well."
 More from Tasmanian Senator Jacqui Lambie’s interview on ABC just now – she hasn’t downloaded the COVIDsafe app and doesn’t plan to.
 But her reasons for steering clear of the health tracking effort were… unclear.
 “They don’t have a good track record in the path, the Coalition, with keeping information where it’s supposed to be,” Senator Lambie said. “That’s a bit of a trust issue.”
 When pressed, the independent admitted that she hasn’t “signed up” for the app but defended her decision, saying that she’s “in complete lockdown”.
 “Trust me, I'm not venturing anywhere.
 “I do have the lady that I’m sharing my house with at the moment, she has the app loaded on her phone. We are keeping a very, very close eye on it.”
 Her hesitation aside, Senator Lambie that she wants to see more people sign up.
 “I would like to see it grow,” she said.
 She was pressed on her specific problems with the COVIDsafe app and her explanation was fairly confusing.
 “I just think there is way too much of our own information up there, floating around in cyberspace. We have seen cyberattacks going on. That's always… that will be a row for the future. 
 “In the meantime, you know, to stay politically alive for me, it doesn't take much to do tracing on Jacqui Lambie. Go to her social media. You know? 
 “So, it is just… how much more information do we have to get out, give out these IT systems. There is information floating everywhere, I think. That is a worry. 
 “It is also – we have seen information from the government, like I said before, that was basically told it was there for one thing and it's been used for other (purposes) along the way to pass out to other agencies.”
 Later in the interview, she added: “I’m just a bit worried about the data.”
 Senator Lambie also questioned the 40 per cent figure – that is, Prime Minister Scott Morrison said he wants to see 40 per cent of Australians download and use the app before he’s comfortable easing social distancing restrictions
 “Where did the 40 per cent (figure) come from? They flicked it out of the air. I don’t think it’s near enough. In other parts of the world, they are saying you need 80 per cent.”
 She revealed that she met with the Prime Minister’s Office today for a briefing about the app but remained unconvinced about the privacy protections promised by the government.
 Tasmanian Senator Jacqui Lambie is sitting on a parliamentary committee examining the government’s response to the coronavirus crisis.
 Today, the inquiry heard from Treasury officials about the measures to support workers and cushion the blow to the economy.
 And in an interview on ABC just now, Senator Lambie dropped quite the bombshell.
 “You know, we're going to have to go back to the table, talk about the two lots of tax cuts that apparently they want to go through with, (and consider) are they viable,” Senator Lambie said.
 “There's a lot to talk about now. They have a perfect chance here of really cleaning up the economy and saying, ‘you know what, if you have an economic idea, nothing is off the table’. 
 “We want to see what you've got. Let's see it all in front of us and let's start from there and pick the best possible solutions on the table.”
 There are two rounds of tax cuts on the horizon for most Australian workers and Senator Lambie said a repeal of those arrangements should be considered.
 Latrell Mitchell and Josh Addo-Carr have been charged with firearms offences, it's just been revealed.
 The NRL stars are alleged to have breached the Firms Act and have been issued court attendance notices.
 Mitchell was charged with giving a firearm to a person not authorised by licence or permit. His licence was suspended and officers seized “a number of firearms”, a NSW Police spokesperson said.
 Ado-Carr was charged with use of an unauthorised firearm.
 The pair will appear at Taree Local Court on August 4.
 A hoax SMS sent to some Australians yesterday is now the subject of an Australian Federal Police investigation.
 At a press conference earlier, Health Minister Greg Hunt said the text – which claimed to have tracked the recipient’s movements and advised them to call a 1300 number to explain their apparent breach of restrictions – is illegal.
 "Firstly, any misuse of telecommunications for a hoax is illegal,” Mr Hunt said. 
 “This case has already been referred to the Federal Police for investigation and that investigation has begun. Anybody who is found responsible will be charged with a significant criminal offence. 
 “The second thing, though, is this is deeply unAustralian. At a moment when Australians have been coming together, when nearly 2.5 million Australians have downloaded and registered for the COVIDSafe app, when Australians have been doing difficult things, to have a few people, or it may just be one person, who are doing something contrary to the public health messages, this isn't a game. 
 “This is about life and death. This is about saving lives and protecting lives.”
 The person or people behind the fake message “should be afraid”, Mr Hunt said.
 “But more than that, they should stop and think,” he continued. 
 “It's no game, because the more people that are able to download and register, the more people who will be protected against inadvertently contracting a life-threatening disease. And that's my message to those who are at odds with the rest of the community. 
 “Stop, think, and don't do it.”
 Four NRL players have been hit with fines for breaching coronavirus social distancing restrictions. Latrell Mitchell and Josh Addo-Carr have been fined $50,000 each, with more than half of that sum suspended.
 Nathan Cleary copped a $10,000 whack, with $6000 of it suspended, while Tyronne Roberts-Davis received the same penalty.
 My colleagues on the sports desk have more on this breaking story.
 A planeload of Australians will land in Perth tomorrow, coming from Johannesburg in South Africa, with all 257 passengers to be immediately quarantined.
 West Australian Premier Mark McGowan said passengers will receive a health assessment before leaving the airport and then enter a 14-day mandatory quarantine.
 “At this stage, we plan to take these passengers – or many of them to Rottnest Island,” Mr McGowan said.
 “They're generally families. If their health condition is good, they'll be going back out to the island where it is, I think, a much better environment than one of the hotels – certainly for a family – to have a unit at the island, where you can get a bit of fresh air, and it's a pleasant environment.”
 Anyone with complex health needs will remain in the CBD in hotels in order to be close to hospital, he said.
 A NOTE ABOUT RELEVANT ADVERTISING: We collect information about the content (including ads) you use across this site and use it to make both advertising and content more relevant to you on our network and other sites. Find out more about our policy and your choices, including how to opt-out.
 Powered by WordPress.com VIP
 https://www.news.com.au/lifestyle/health/health-problems/coronavirus-australia-live-blog-aussies-have-to-change-behaviour-permanently/live-coverage/0037f7a0730d2a37db63ca6be1484490
