Cuomo: NY's rising death rate from coronavirus is 'bad news' - San Antonio Express-News
 By Brendan J. Lyons
 Caption
 Close
  Gallery
 ALBANY — The rate of deaths due to the coronavirus increased sharply Thursday, as Gov. Andrew M. Cuomo said New York has had 385 fatalities attributed to COVID-19 as the number of confirmed positive cases in the state topped 37,000.
 The tally of confirmed cases includes people who have recovered from the illness.
 "This is really bad news: The number of deaths are increasing," Cuomo said at the Capitol on Thursday. "We now have people who have been on a ventilator 20 days, 30 days. … That is what has been happening; those are the people who we are losing. The longer (hospital) stays without recovery lead to a higher death rate."
 There are 5,327 people "currently hospitalized," including 1,290 people who are in intensive care units, the governor reported. More than 1,500 people have been released from hospitals. The total number of people confirmed to have COVID-19 stands at 37,258.
 The rate of positive coronavirus cases in New York has in recent days increased more than anticipated. Cuomo has said the height of the infections could strike the state in two or three weeks, when as many as 140,000 hospital beds may be needed.
 But he cautioned that many people may have the illness but have not been tested, and that at least 80 percent of those who are infected do not require hospitalization and recover at home.
 "What we’re looking for is not a reduction in the number of cases … (but) in the rate of the increase in the number of cases," the governor said. "The rate of increase should reduce, as opposed to the number of cases."
 The governor said New York City's hospitals are reporting they have adequate supplies of personal protective equipment for staff, but he remains concerned there will be a shortage of ventilators, which have been crucial for people placed in intensive care units — especially elderly and immune-compromised patients.
 Earlier: McCoy: Public not doing enough to stop spread of coronavirus
 Latest coronavirus-related cancellations, postponements
 The latest coronavirus numbers in NY
 Full coronavirus coverage
 Cuomo also offered his perspective on people have not been able to work, saying they have not "lost their jobs" but succumbed to a situation never faced by this nation.
 "That’s why, look, it is going to change us — I really believe that," he said. "It is going to help form a new generation, I can see it in my daughters' eyes. … I can see the fear. … It will transform who we are and how we think."
 State Department of Health officials have still not said whether they are tracking the number of those who recover from the illness. Some county leaders said they are submitting that information to a state database, but others said they have not tracked the number of people who are fully recovering because they were not instructed to do so.
 That discrepancy in the total confirmed cases with the number of those who recovered had created a wrinkle in the data. The administration had been calculating the rate of hospitalizations based on the number of people hospitalized against the total number who tested positive, even though many have recovered. Cuomo on Thursday did not offer the percentage of hospitalizations in his daily figures.
 Still, increasing deaths and a surge in cases, Cuomo said, are driving his prediction that there will be a critical need for hospital beds in the coming weeks.
 The steps being taken by the state include ordering hospitals to increase their bed space and converting convention centers and university dormitories into makeshift hospitals. The task force overseeing that effort is also examining whether hotels could be transformed into hospitals, and developing a plan to have upstate hospitals absorb some of the patients if downstate facilities become overrun.
 "Almost any scenario that is realistic will overwhelm the capacity of the current hospital system," Cuomo said. "You cannot get the curve down low enough so that you don’t overwhelm the hospital capacity ... we have to increase the hospital capacity."
 https://www.mysanantonio.com/news/article/Cuomo-NY-s-rising-death-rate-from-coronavirus-is-15158907.php
