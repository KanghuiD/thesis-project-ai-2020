Covid-19 will have lasting negative impact on women, French minister warns
 Issued on: 28/04/2020 - 18:14Modified: 28/04/2020 - 18:40
                     The coronavirus crisis is a major threat to women's rights across the world, according to France's junior minister for equality, Marlène Schiappa, who says the damaging impact will be much longer and further reaching than the spike in violence and domestic abuse already reported across the world under lockdown.
                 
 Whether in terms of sexual violence or economic inequality, French junior minister for equality Marlène Schiappa says the coronavirus crisis will have a far-reaching impact on the lives of women regardless of where they live.
 In an opinion piece for the French social think tank Fondation Jean-Jaurès on Monday, she explained that basic human rights have become endangered and must be defended.
 She points out that women form a major part of the frontline of the battle against the virus, as they represent 90 percent of nursing staff across the world, according to the World Health Organization.
 The World Bank calculated that more than 66 percent of the world's teaching staff were women – many of them mothers having to juggle the needs of schooling their own children, and preparing classes at a distance during lockdown.
 Women also make up the majority of service providers, such as cashiers in supermarkets who have continued working during the lockdown, often for no extra remuneration while facing considerable risk.
 Furthermore, many women are involved in non-paid care work, whether related to their own families, aged care or with charities.
 According to the European Commission, a quarter of all non-declared work, for example cleaning, babysitting or other odd jobs, fell to women.
 In countries like France, the confinement period has seen women's list of daily obligations multiply, working from home while attending their children's school work – on top of more household chores than usual, with the whole family at home. 
 A recent poll found that for 63 percent of families, women cooked all the meals.
 Another government study showed 58 percent of women were spending two hours a day doing chores compared to 35 percent of men during lockdown.
 Schiappa is concerned that only a small portion of women are visible in decision-making arenas and policy making during a crucial moment and that they tended to sacrifice career advancement for taking care of their families.
 The gender equality charter for representation in quality television or media debates has gone out the window since the beginning of the crisis, she says, with an excessive presence of male experts designed to give an image of being more "reassuring" to viewers.
 When it comes to sexual and domestic violence, the junior minister outlined several aspects she finds worrying.
 Lockdown and the difficulties of families being confined together 24 hours a day has lead to an increase in domestic abuse.
 In France, there were five times more reports of abuse through a government hotline, since the beginning of confinement.
 Being confined with a violent partner, women have less options to seek outside help or to avoid confrontation, while young girls who are unable to go to school are more at risk of being the target of a sexual assault.
 Schiappa pointed to the rise in cyber-bullying, often with sexual overtones, since the beginning of lockdown.
 With schools relying on distance learning, and digital technology, young people are having to spend much more time online, and are susceptible to being harassed or trolled.
 She points to a trend emerging in France called "fisha" whereby photos or videos of very young girls in sexual scenarios are published along with their names and contact details with the objective of humiliating them.
 Fifty or so victims have come forward and the material has been taken off line after pressure by lawyers and associations.
 Schiappa says there has also been a rise in the consumption of pornographic material online, in France and elsewhere, much of it featuring aggressive situations involving young women and girls.
 She fears that this will evolve into violent sexual behaviour once the confinement period is lifted, perpetuating negative stereotypes of women and undermining the notion of consent.
 On a global level, Schiappa is concerned that women's rights will be eroded and hard won battles for access to abortion, sexual health and preventing child marriages will be undermined.
 She called on all governments to maintain their funding, research and international cooperation on these fronts despite the ravages occurring due to Covid-19.
 Daily news briefReceive essential international news every morning
 Keep up to date with international news by downloading the RFI app
 Marseille football club hosts women victims of domestic violence during lockdown
 'Not just a health issue': How Covid-19 is quietly eroding women's rights
 Covid-19: Women trapped between a deadly virus and a deadly partner
 France enters coronavirus -driven recession with biggest drop in GDP since 1949
 International students in Paris await end of confinement whenever that may be
 France to investigate how many Covid-19 patients are dying at home
 French supermarkets to sell masks from May 4 in preparation for lockdown ease
 French lockdown: Key future dates in ending the lockdown across France
 French coronavirus strain may be local and may not have come from China
 10 key points that will drive France’s exit out of coronavirus lockdown
 Colour-coded map of France to indicate level of lockdown restrictions
 French Covid-19 figures continue to drop as country prepares to lift lockdown
 French MPs back prime minister's plan to cast off coronavirus lockdown
 French PM outlines roadmap for gradual exit from Covid-19 lockdown
 French PM faces contentious vote on easing Covid-19 lockdown
 The content you requested does not exist or is not available anymore.
 http://www.rfi.fr/en/france/20200428-covid-19-will-have-lasting-negative-impact-on-women-french-minister-warns-sexual-domestic-violence
