'This is unacceptable': New York City mayor denounces coronavirus discrimination
 At a media roundtable Wednesday, New York City Mayor Bill de Blasio condemned coronavirus-related discrimination against Asian communities.
 De Blasio’s denouncement comes after two separate attacks against Asian Americans in New York on March 10. That morning, a 23-year-old woman told police another woman punched her in the face and made anti-Asian slurs, and the victim had to go to the hospital.
 “Right now, we've seen particularly troubling instances of discrimination directed at Asian communities, particularly in Chinese community,” de Blasio said. “This is unacceptable.”
 The New York City Police Department said that it is a “possible biased incident” and that it is an ongoing investigation by the Hate Crime Task Force.
 Later in the evening, a 59-year-old man said he was approached by a suspect who used anti-Asian remarks and then kicked him, causing him to fall. The NYPD said the victim refused medical attention at the scene.
 Full coverage of the coronavirus outbreak
 The March 10 attacks are the latest in a wave of xenophobic harassment against Asian communities across the globe following the outbreak of the coronavirus. Anti-Asian attacks led the Congressional Asian Pacific American Caucus to send a letter to colleagues Feb. 26 urging them to “help prevent hysteria, ignorant attacks and racist assaults” against Asian Americans resulting from misinformation about the coronavirus.
 While de Blasio did not directly address the March 10 incidents, he pointed out that many hate crimes go unreported, and he implored people to report hate crimes to the NYPD so they can be dealt with.
 “Any act of discrimination could very well be something that constitutes a breaking of the law of the city, which we have a very strong human rights law,” de Blasio said. “So people are discriminated against in employment, in terms of their day to day lives, shopping, housing, anything, we want to know about it, we want to stop it.”
 Download the NBC News app for full coverage of the coronavirus outbreak
 New York Gov. Andrew Cuomo said in a statement March 11 that he was “disgusted” to hear of the attack on the 23-year-old woman, saying that it was apparently motivated by a “bigoted notion” and that he is directing the State Police Hate Crimes Task Force to help investigate.
 “To be clear: there is zero evidence that people of Asian descent bear any additional responsibility for the transmission of the coronavirus,” Cuomo said.
 Natasha Roy is an intern with NBC News Digital.
 https://www.nbcnews.com/news/asian-america/unacceptable-new-york-city-mayor-denounces-coronavirus-discrimination-n1157026
