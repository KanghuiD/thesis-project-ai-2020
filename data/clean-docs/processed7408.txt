Opinion: If we will ever overcome the coronavirus, we need hope – The Daily Iowan
 We cannot continue to consume information that does not allow us to see the good that still exists in the world. 
 Katie Goodale
 A new mural is seen on the side of Hothouse Yoga on June 22, 2019. (Katie Goodale/The Daily Iowan)
  Hannah Pinski, Opinions Columnist April 23, 2020
 I don’t know about you, but if the news wasn’t depressing before it sure is now.
 Every day for the last month, our community and government officials tell us the statistics and facts ­— how many people are sick, how many people have died, and how close we are to finding a solution. (Spoiler: We’re not too close to finding a vaccine or a cure yet.)
 Even though there is a dire need for vaccines, tests, and a cure, the world needs more right now than just medical solutions. What we need most right now is hope.
 We have all lost something amid this virus, and reports that we’re a ways away from returning to normal continue to feed into our fear that things will never change. While the statistics and information are important, we have to keep spreading the good that still exists.
 We have to remain hopeful that there is going to be an end, that a vaccine will be created soon, and that we will be able to go back to school or to our jobs or be able to see a sporting event.
 For example, when Italy was hit hard, videos went viral of Italian villages singing together. Italians used music to say, “Things may be difficult and scary right now, but we must have hope for the future. We must try and see the light at the end of the tunnel.”
 A little closer to home, John Krasinski started his show “Some Good News” on YouTube, which hit 15 million views on his first episode. He’s done everything from hosting a virtual prom to surprising a Hamilton lover with a Zoom performance of Alexander Hamilton including the original cast.
 This is what we need most in our lives, whether it be in the form of music, shows, or just FaceTiming our family and friends. Although we have to be physically separated, it doesn’t mean that we are not still connected.
 Our way of spreading the good can be anything from shopping for community members who can’t get to grocery stores to calling our friends and family to check in. Whatever form it is, acts of kindness allow us to bring light into not only our lives, but to others’ lives during a time where there is widespread uncertainty and fear.
 Most of us can’t help with creating vaccines or more testing, or jump immediately into the frontlines as a health-care worker to help fight this virus.
 Vaccines and cures will heal us physically, but we also must heal mentally. The most we can do right now is to keep social distancing, maintain human connection, provide compassion and hope, and spread the good.
 If we don’t try to see the light and dwell on all that the novel coronavirus has taken from us, our lives will continue to be filled with darkness and fear. We should trust our medical and health-care workers and remain hopeful for the future.
 Where does Iowa stand in flattening the coronavirus curve?
 Tristan Wirfs selected No. 13 overall by Tampa Bay Buccaneers in NFL Draft
 Father of Iowa City homicide victim one of two men charged with attempted murder in Coralville shooting
 Governor’s office looking at data to decide what businesses to reopen after April 30
 Custom Magnets
  The Blanch Law Firm
 Asphalt Driveway MN
 Automobile Accident Cases
  Bigos - St. Paul Apartments 
 Creditor Harassment Attorney
 SEO Minneapolis
 Laser eye surgery NYC
 Buy peptides
  Online Casino Real Money
  Math Help at payformathhomework.com
  CasinoslotsNZ 
  Workers' Compensation Insurance
  Peptides For Sale
  Best carpet cleaners near me
  Green carpet cleaning Orange County 
  Rug cleaners near me
  Carpet cleaners Orange County 
  Carpet cleaners Costa Mesa 
  Green carpet cleaning near me 
  Dryer vent cleaning service 
  Carpet cleaning Irvine
  Best Peptides and SARMs Online
  bankruptcy attorney san diego ca 
 Columns
 Opinion: Take note of those who profit from pandemics
 Opinion: Mindfulness during COVID-19
 Opinion: Lawmakers should check Iowa governor’s emergency powers
 Opinion: College students deserve more than just a housing and dining refund
 Opinion: Iowans shouldn’t have to pay rent during COVID-19 pandemic
 Opinion: Students should not binge watch during COVID-19
 Opinion: Lectures are impractical to conduct via Zoom
 Opinion: Kim Reynolds is right to refrain from shelter-in-place order
 Opinion: Growing pains and plants during COVID-19 quarantine
 https://dailyiowan.com/2020/04/23/opinion-if-we-will-ever-overcome-the-coronavirus-we-need-hope/
