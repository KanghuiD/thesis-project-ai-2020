Editorials: Today's Editorial Opinions, Pages, News, Articles, Today's Editorial Column |  The Indian Express
 H C Sarin, principal adviser to the Governor of Assam said that the government sought the postponement as opposition leaders in Parliament had to be consulted before the resumption of the talks.
 The dissemination of nude photos and videos of a victim engaging in a sexual act deserves to be defined as a continued sexual violation for what is once put in the digital space can rarely be wholly retracted.
 The WHO cautioned that “we have seen broad variations in naïve estimations of CFR that may be misleading”. Adjusting for the stage of the pandemic in each setting is essential to correctly interpreting COVID-19 statistics.
 The cricket controversy speaks of a skew towards batsmen, against bowlers. It needs to be reviewed — and given a new name
 Chief Minister and his deputy present united front. But AIADMK, post Jayalalithaa, may need more than that
 The fact that his detractors tried to persistently target him, despite him prevailing in trials in courts and in public opinion, was resented by his support base. That not only helped consolidate his hold over them in the state, but also started the bandwagon of similar support nationwide.
 Given the prevailing unholy mix of growth and inflation, it is tempting to categorise India’s economic situation as one of “stagflation”. But, in our view, it is too early to conclude decisively on this matter, given the fluid nature of things.
 Post Covid, classroom will need to be re-imagined. Local administrations and communities must have freedom to decide 
 Artificial intelligence offers limitless opportunities to empower individuals and increase the ease of doing business.
 The emphasis of NEP 2020 on philanthropic education will reboot the idea of education as human resource development. 
 The conclusion of two years of AB PMJAY should serve as an occasion to celebrate of how far India has come in its UHC journey, but also a reminder of the long and difficult road that lies ahead
 Iraqi planes delivered their heaviest attack on Teheran since the start of the Gulf war, striking at the capital’s airport and vital oil refinery, and industrial targets stretching several kilometres around it.
 Like everything else in this year of change, diplomacy, too, may need an emotional upgrade. Ambassador Lindner shows the way
 This is hardly surprising when the man in charge is himself a tainted figure and doesn’t believe in the due process of law or respect for human dignity.
 Asymmetric representation in both societies has generated a long and complex debate concerning women’s representation. But the constitutional histories and judicial action vary.
 With a flurry of FIRs, UP government criminalises protest, takes a vengeful view of its mandate 
 To break impasse in GST Council, Centre must listen to all states. Both politics and economics are key  
 The reform measures address basic needs — to revive the economy and tackle barriers in the expansion of firms. 
 Modi represents values that are beyond governance and politics. He appeals to the best in India and Indians, and manages to bring out the best from them.
 The Centre has created a bureaucratic structure accountable to the Ministry of Rural Development and that may deliver the right outcomes in the short run, but it remains to be seen how to manage this in the longer run.
 There have been glaring omissions and controversial choices in the award’s history. The committee should strictly follow the criteria laid out
 Our cold response to crimes against the oppressed and marginalised is a reflection of our socio-political prejudices.
 Ensuring equality without compromising quality of research, by creating an environment of individual freedom and institutional trust, requires concerted effort from policy makers, institutions and individuals.
 In Pakistan, as parties join hands against ruling regime, a familiar tumult is back. Where latest long march is headed is still unclear
 https://indianexpress.com/section/opinion/
