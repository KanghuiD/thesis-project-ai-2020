
 In these times, it's been heartening to see people coming together — from checking in on neighbors to applauding healthcare workers. 
 That’s the spirit many companies are starting to channel as they try to answer a difficult question: how should employers be communicating about the coronavirus? 
 Companies have had to scrap their old employer branding plans and focus on meeting the moment at hand. 
 Here's a closer look at how employer branding messages have changed, which messages are resonating the most, and where to go from here. 
 In late February and early March, coronavirus was making international news, but it wasn’t affecting employer branding yet. It was one of many stories circulating, and employers were occasionally starting to talk about it. 
 By mid-March, it was the main story and employer branding postings started to reflect that.
 That’s the key period when the reality of the situation really set in on a global scale, beyond China and East Asia. On March 9, Italy went on nationwide lockdown; on March 11, the NBA suspended their season; on March 17, France and the San Francisco Bay Area implemented shelter-in-place, with the entire states of California and New York following suit over the next few days. 
 The change was even more pronounced in certain industries. Coronavirus-related posts made up nearly 50% of posts by companies in the legal industry, with public administration close behind. Other industries posting COVID-19 content were healthcare, nonprofits, and public safety.
 Around the same time, employees who were able to work from home had to make adjustments — and that was reflected in companies’ messaging. Many employer branding teams grappled with the new normal by posting content about remote work, with almost 15% of company posts touching on it in mid-to-late March. 
 Perhaps because people are looking for resources and ways to cope with the pandemic, engagement with coronavirus posts from companies is significantly higher than the engagement for an average company post.
 That level of engagement has also increased over time as people are paying more and more attention to what companies are saying and doing about coronavirus.
 While coronavirus-related content received below-average numbers in January, they steadily picked up as the months went on — first in Asia, then in Europe and North America. By April, posts from companies received above-average engagement across all regions; in North America and Asia Pacific, those posts got 30% more engagement than the average post. 
 Though it can be difficult to know what to say without coming off as tone-deaf, the posts that resonated most can provide some direction. As it turns out, the coronavirus posts that got the most engagement were about how those companies were stepping up to help relief efforts, in ways big and small.
 Some took on major efforts, like mass-producing hand sanitizer or setting up a center for COVID patients. Others got similar levels of engagement from smaller human stories, like an engineer driving through a snowstorm to work on ventilators or an illustrator getting stuck in Wuhan and creating comics to teach kids in lockdown not to be afraid. 
 Posts about working from home (WFH) have seen an even more dramatic increase in engagement:
 In March, WFH posts in Asia-Pacific and North America saw roughly 50% more engagement than the average post. So far in April, engagement has skyrocketed in Asia-Pacific — with WFH posts getting 165% more engagements than usual. 
 Some of the most successful posts showed how companies were equipping employees to work remotely; other posts offered looks at employees’ improvised home offices. 
 We also looked closely at what words appeared more often in the posts that resonated most. Compared to other coronavirus-related posts, the ones with the highest engagement tended to use words about health, helping, and support. 
 Messages promoting public health also resonated — references to health authorities, social distancing, and healthcare workers were correlated with stronger engagement. 
 In short, it looks like messages that put people first perform best. Whether it’s honoring healthcare workers, supporting local communities, or caring for employees, posts that convey empathy and togetherness are getting the most attention — and rightly so. 
 This is an incredibly difficult time, and every person and organization will be affected differently. While no one can say exactly how long our current situation will last, demonstrating solidarity, compassion, and care can help all of us get through it together. 
 Co-authored by Greg Lewis. 
 Methodology
 Related story
 https://business.linkedin.com/talent-solutions/blog/employer-brand/2020/data-shows-how-coronavirus-has-influenced-employer-branding
