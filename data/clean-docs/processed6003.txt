Coronavirus: RBS says revamped loan scheme will make 'big difference' - BBC News
 A revamped loan fund for ailing firms hit by the coronavirus lockdown will have an immediate impact, RBS has said.
 RBS chairman Sir Howard Davies admitted there had been problems but expects to see a "sharp increase" in lending to small firms in the next few days.
 On Thursday, Chancellor Rishi Sunak overhauled the scheme amid claims banks were taking advantage of the crisis.
 The government has pledged to guarantee £330bn of loans but only £145m has been lent so far.
 Small firms say they have struggled with onerous eligibility criteria for the government-backed loans, which are being issued by High Street banks and other lenders.
 They have also complained of facing interest rates of up to 30% and being asked to make unreasonable personal guarantees.
 It comes as the UK is facing recession as large parts of the economy are shut down.
 Any figure below 50 marks contraction.
 Mr Sunak said that under changes to the Coronavirus Business Interuption Loan Scheme (CBILS):
 He also said RBS had struggled with the demand after inquiries about the loans jumped "by 45 times" in a week.  
 "I think we have to accept that the scale of this process and the speed with which it's been put in place has caused challenges for everybody," he said.
 "But we've had good discussions with the Treasury and small firms, and I think the changes announced overnight will make a quite a big difference."
 On Wednesday, Business Secretary Alok Sharma said it would "completely unacceptable" if any banks were unfairly refusing funds to good businesses in financial difficulty.
 He also referenced the financial crisis - when taxpayers bailed out a number of the UK's largest banks - suggesting lenders should now repay the favour.
 However, Sir Howard told the BBC that comparing the current crisis to 2008 was "rewriting history".
 "In the last crisis the problem was that the banks didn't have the money to lend, there was a credit crunch.
 "We're not in that position at all. The banks have got the money to lend, we have a large amount of capital, we are not constrained in the volumes we can lend."
 On Thursday, Mr Sunak said the government was making "great progress" on supporting businesses to help manage their cashflows but needed to take "further action" by extending the scheme.
 But some firms still feel they will struggle to access the loans fast enough or that they are too risky.
 There has been widespread concern, acknowledged by the government, that some of the emergency measures to provide financial assistance to businesses are not working. 
 Too few firms felt able or willing to take on loans that carried an 80% government guarantee to the lender but not the borrower. The Treasury has announced new rules, meaning business owners asking to borrow less than £250,000 will no longer have to offer up personal guarantees.
 Perhaps most importantly, the requirement for companies to have first tried to get a normal commercial loan elsewhere will be dropped. 
 However, they are still loans. Companies wishing to take them out will be 100% liable for the debt and the government has not capped the interest rate banks can charge even though banks are able to borrow at close to 0%. 
 The loans may now be available to more businesses but what's not clear is whether firms want them.
 Labour welcomed the measures but accused the government of being "behind the curve" when implementing support measures.
 "There remain huge gaps in support for employees and self-employed that must be addressed immediately if people are to avoid facing serious hardship in this crisis," said shadow chancellor John McDonnell.
 The head of the Confederation of British Industry, Carolyn Fairbairn, described the changes as a "big step forward" although she said more detail was needed.
 "Each week brings unprecedented levels of economic support and it's encouraging to see the government stepping in where urgent help is needed."
 Mike Cherry, national chairman of the Federation of Small Businesses, told the BBC's Today programme: "It's a very necessary and timely intervention by the chancellor, because clearly, businesses were being promised interest-free, fee-free, government support by the banks. 
 "Time and time again, the FSB has heard from our members and other small businesses who've approached banks seeking these emergency loans that they were being offered anything but."
 Stephen Jones, the chief executive of UK Finance which represents the banks, also welcomed the changes.
 Speaking to the Today programme, he said: "It was clear that those viable businesses, who were required to be offered under the terms of the scheme commercial lending under commercial terms, felt aggrieved that they were not given access to the scheme and therefore the change gives the scheme to all businesses who are capable of repaying debt after this crisis is over.
 "This change is extremely welcome and it means that banks will not be forced to make very unenviable assessments in terms of who cannot or can access the scheme in terms of viable businesses out there."
 Or use the form below
 If you are happy to be contacted by a BBC journalist please leave a telephone number that we can
                         you provide it and location, unless you state otherwise. Your contact details will never be published.
                         When sending us pictures, video or eyewitness accounts at no time should you endanger yourself or others,
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/uk-england-tyne-52143673
