UK will have the most coronavirus deaths in Europe, analysts predict
 Keep Me Logged In
 Britain will be hit harder by the coronavirus crisis than any other European country, researchers have predicted, with the U.K. expected to see more than 60,000 deaths from COVID-19.
 New research from the Institute for Health Metrics and Evaluation (IHME), published Monday with a correction issued on Tuesday, forecast a total of 151,000 deaths across Europe during the "first wave" of the pandemic, which scientists predict will end on August 4.
 It compares to a total of 81,766 deaths expected to occur in the U.S. by the same date, according to IHME analysts.
 The European analysis was based on data from various sources, including national and local governments, the WHO, and individual countries' social-distancing policies.
 For many countries, death tolls would be influenced by the demand for hospital resources being "well in excess of what it available," researchers said.
 "For example, peak demand in the U.K. is expected to total 102,794 hospital beds needed compared to 17,765 available, 24,544 ICU beds compared to 799 ICU beds available, and 20,862 ventilators needed," they explained, noting that data on the availability of ventilators in Britain was currently unavailable.  
 According to the analysis, Britain was expected to reach its peak daily death rate on April 17, when researchers predicted there would be a total of 2,932 deaths in one day. The country was on a trajectory that would result in a total of 66,314 deaths by August 4, scientists concluded.
 However, the IHME's prediction model has been disputed, with Neil Ferguson of Imperial College London telling The Guardian newspaper that its figures on hospital bed use and deaths were twice as high as they should be.
 Imperial College London, which has helped shape the U.K. government's response to the pandemic, estimates that the total number of deaths from COVID-19 in Britain will be between 7,000 and 20,000.
 To date, more than 7,000 people in the U.K. have died after contracting the virus. But at a press briefing on Tuesday, the government's Chief Scientific Advisor, Patrick Vallance, said it was possible the number of new cases in Britain "could be moving in the right direction."
 The British government implemented lockdown measures in late March, urging citizens to only leave their homes for exercise, medical purposes, to buy basic necessities or to go to work if necessary. However, the government faced criticism early on in the country's outbreak for being too cautious in its approach to controlling the spread of the virus.
 Italy — currently the country with the highest number of deaths from COVID-19 in the world — would have the second-highest overall death toll in Europe, the IHME predicted, with a total of 20,300 deaths expected by August 4. 
 There have been 17,669 deaths from the coronavirus and 139,422 confirmed cases in Italy so far, according to data compiled by Johns Hopkins University. However, Italian health officials said on Sunday that the number of deaths and new infections were beginning to drop.
 In Spain, currently the country with the second-highest number of deaths from COVID-19 globally, researchers predicted there would be a total of 19,209 deaths during the first wave of the pandemic.
 According to Johns Hopkins University, 14,792 people have died of the coronavirus in Spain so far, but new cases and deaths are beginning to slow down, data suggests.
 The European countries expected to see the region's lowest overall death tolls were Malta and Cyprus, which were predicted to have a total of 19 and 54 deaths respectively by August.
 The peak daily death rate for the whole of Europe was expected to occur during the third week of April, the IHME said.
 The IHME, part of the University of Washington, initially developed its coronavirus forecasts to help U.S. hospitals and state governments determine when COVID-19 would overwhelm their ability to care for patients.
 Got a confidential news tip? We want to hear from you.
 Data is a real-time snapshot *Data is delayed at least 15 minutes. Global Business and Financial News, Stock Quotes, and Market Data and Analysis.
 Data also provided by 
 https://www.cnbc.com/2020/04/09/uk-will-have-the-most-coronavirus-deaths-in-europe-analysts-predict.html
