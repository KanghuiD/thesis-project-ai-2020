WHO: Coronavirus May Affect All Countries
 Print
 No media source currently available
 The World Health Organization (WHO) says the new coronavirus could spread worldwide.
 “The outbreak is getting bigger,” WHO spokesman Christian Lindmeier told reporters on Friday in Switzerland.
 Coronavirus cases have been confirmed in almost 60 countries. At least seven countries reported their first cases in the past 24 hours. Those countries are Azerbaijan, Belarus, Lithuania, Mexico, the Netherlands, New Zealand and Nigeria.
 The new coronavirus has infected more than 83,000 people around the world. Mainland China, where the virus was discovered, has the largest number of infections. On Friday, China reported 327 new cases, its lowest number since January 23. The country now has more than 78,800 cases and almost 2,800 deaths from the disease, called COVID-19.
 About three-fourths of all new infections are being reported in countries other than China. Most of the new cases are in South Korea, Japan, Italy and Iran.
 Mike Ryan is head of the WHO’s emergency program. He said Iran’s outbreak may be worse than what is known. Iran has reported the most deaths outside of China - 34 from 388 reported cases.
 More than a health crisis
 The continuing health crisis also became an economic crisis. The Reuters news agency reports that the world’s financial markets lost $5 trillion this week. It is the worst week for stock shares since the 2008 financial crisis.
 On Friday, Switzerland joined other countries in banning major public events to try to limit the spread of the virus. Swiss officials cancelled the 2020 Geneva Motor Show, one of the automobile industry’s most important gatherings.
 On Thursday, Saudi Arabia announced a temporary ban on foreigners from entering the country to visit Islam’s holiest places. Saudi officials said the move is meant to slow the spread of the virus. The decision has affected thousands of Muslims already traveling to the country. It also could have an effect on millions more with plans during the holy month of Ramadan and the hajj.
 In Japan, Prime Minister Shinzo Abe called on schools to close until the end of March. The city of Tokyo is preparing to hold the 2020 Summer Olympics in July, but discussions were being held about whether to postpone the games. Japanese officials declared a state of emergency in the northern island of Hokkaido on Friday because of the spread of the virus there. Tokyo Disneyland and Universal Studios Japan closed their doors to visitors and cancelled events.
 South Korea reported 571 new cases of the virus on Friday. The country now has 2,337 cases of the virus, more than any other country outside of China.
 Speaking from his eatery at the Gwangjang Market in Seoul, Kim Yun-ok told one reporter, “there’s almost no one coming here.”
 In Italy, where the number of new cases is growing and the number of visitors shrinking, Prime Minister Giuseppe Conte feared his country will face a recession.
 Simone Venturini is an official in the Italian city of Venice, where tourism has already been hurt by last year's flooding. With more cases of the virus reported in Italy, Venturini said, “the damage that worries us even more is the damage to the economy.”
 Kanya Yontararak owns a women’s clothing store in Bangkok, Thailand. Sales at her store have dropped sharply. She is not making not enough to pay her rent. After years of dealing with flooding and the country’s political crises, Yontararak said, “Coronavirus is the worst situation [she has] ever seen.”
 I'm Ashley Thompson.
 Hai Do adapted this story for Learning English based on Associated Press and Reuters news reports. George Grow was the editor.
 ________________________________________________________________
 outbreak - n. a sudden increase of disease
 hajj - n. a journey to Mecca that is a religious duty for Muslims
 tourism - n. the activity of traveling to a place for pleasure
 rent - n. money that you pay in return for being able to use the property
 https://learningenglish.voanews.com/a/who-coronavirus-may-affect-all-countries/5308395.html