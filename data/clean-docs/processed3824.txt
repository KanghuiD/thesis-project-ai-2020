IMF Warns of Economic Calamity in Middle East from Coronavirus Pandemic  | Voice of America - Fidar News
 The International Monetary Fund is urging governments in the Middle East to take immediate action to blunt a likely economic setback that could arise from the novel coronavirus pandemic. 
 Jihad Azour, the IMF’s regional director for the Middle East and Central Asia, said in a report Tuesday that the pandemic “has become the largest near-term challenge to the region.” 
 Azour said the rapid spread of COVID-19, coupled with the lowering of crude oil prices due to the price war between Saudi Arabia and Russia, will deal a severe blow to economic activity in the region, especially in the tourism and hospitality fields, bringing with it rising joblessness and falling wages.  
 The IMF says war-torn countries such as Iraq, Sudan and Yemen will be especially vulnerable to the coronavirus pandemic. 
 Website 
 Daily Online News Publisher based in Kigali, Rwanda. We provide around the clock live News, Sports, Business, Entertainment and Lifestyle Updates.
 https://www.fidar.rw/imf-warns-of-economic-calamity-in-middle-east-from-coronavirus-pandemic-voice-of-america/
