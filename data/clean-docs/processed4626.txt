  Coronavirus (COVID-19) - NHS
  - NHS
 Menu
 Close menu
 Back to Health A to Z
 Use the quick NHS coronavirus status checker to tell us about your current experience of the virus.
 This will help the NHS plan its response to coronavirus by showing where the virus is spreading and how it affects people.
 Everyone must stay at home to help stop the spread of coronavirus.
 You should only leave your home for very limited purposes:
 These reasons are exceptions – even when doing these activities, you should be minimising time spent outside of the home and ensuring you are 2 metres apart from anyone outside of your household.
 There is separate advice:
 There are things you can do to help reduce the risk of you and anyone you live with getting ill with coronavirus.
 wash your hands with soap and water often – do this for at least 20 seconds
 use hand sanitiser gel if soap and water are not available
 wash your hands as soon as you get home
 cover your mouth and nose with a tissue or your sleeve (not your hands) when you cough or sneeze
 put used tissues in the bin immediately and wash your hands afterwards
 do not touch your eyes, nose or mouth if your hands are not clean
 To help yourself stay well while you're at home:
 If you need medical help for any reason, do not go to places like a GP surgery, pharmacy or hospital.
 If you have symptoms of coronavirus (a high temperature or a new, continuous cough), use the 111 coronavirus service.
 If you need help or advice not related to coronavirus:
 Read more advice about getting medical help at home.
 If you're pregnant and worried about coronavirus, you can get advice about coronavirus and pregnancy from the Royal College of Obstetricians and Gynaecologists.
 You can help people at high risk of getting seriously ill from coronavirus by joining the NHS Volunteer Responders.
 Read more about the NHS Volunteer Responders on the NHS England website.
 Get answers to common questions about staying at home on GOV.UK.
       Page last reviewed: 15 April 2020
       Next review due: 16 April 2020
     
 https://www.nhs.uk/conditions/coronavirus-covid-19/