European markets: coronavirus and oil prices are in focus
 Keep Me Logged In
 European stocks closed slightly higher on Monday as global coronavirus developments and a fall in U.S. oil prices were in focus.
 The pan-European Stoxx 600 pared earlier losses to close 0.4% higher provisionally, with sectors and major bourses mixed. Health care stocks saw the biggest gains, up 1.8%, while autos slipped 1%.
 Monday's choppy trading session came as U.S. crude futures for May plunged more than 44% to around $8 a barrel. Traders continue to fret over a slump in oil demand due to the coronavirus pandemic, with one analyst describing the situation stateside to CNBC as "quite dire."
 In the U.S., stock markets were mixed after falling sharply earlier in the session Monday, as oil and coronavirus developments weighed.
 Covid-19 remain in focus with more than 2.4 million infected worldwide and 166,235 deaths globally, according to data compiled by Johns Hopkins University.
 Meanwhile, economic data out of Europe on Monday showed that the euro zone's trade surplus with the rest of the world grew to 23 billion euros ($25.1 billion) in February, up from 18.5 billion euros a year before. 
 Separately, IHS Markit revealed Monday that a third of U.K. households had already suffered a drop in income because of the coronavirus crisis, with researchers recording the weakest income reading for Britain since the survey began in 2009. 
 It comes as the U.K.'s online furlough scheme went live, with the CEO of British tax authority HMRC telling the BBC 67,000 claims had been made in the first 30 minutes. 
 Looking at individual stocks, Italian hearing aid manufacturer Amplifon added almost 11% to lead the European benchmark, followed by Danish health care equipment manufacturer Ambu, which climbed almost 8%.
 Philips was up more than 6% after the Dutch health technology company said it expected to return to growth in the second half of the year.
 At the other end of the index, Norway's Mowi was among the worst performers after missing first-quarter earnings expectations and postponing its dividend. Shares slipped almost 7%. 
 Got a confidential news tip? We want to hear from you.
 Data is a real-time snapshot *Data is delayed at least 15 minutes. Global Business and Financial News, Stock Quotes, and Market Data and Analysis.
 Data also provided by 
 https://www.cnbc.com/2020/04/20/european-markets-coronavirus-and-oil-prices-are-in-focus.html
