Coronavirus
 Thank you for visiting nature.com. You are using a browser version with limited support for CSS. To obtain
             the best experience, we recommend you use a more up to date browser (or turn off compatibility mode in
             Internet Explorer). In the meantime, to ensure continued support, we are displaying the site without styles
             and JavaScript.
 Collection
  | 
 03 February 2020
 To support urgent research to combat the ongoing outbreak of COVID-19, caused by the novel coronavirus SARS-CoV-2, the editorial teams at Nature Research have curated a collection of relevant articles. Our collection includes research into the basic biology of coronavirus infection, its detection, treatment and evolution, research into the epidemiology of emerging viral diseases, and our coverage of current events. The articles will remain free to access for as long as the outbreak remains a public health emergency of international concern.
 SARS-CoV-2 uses ACE2 as the entry receptor. Here, the authors show that an ACE2-Ig fusion protein inhibits entry of virus pseudotyped with the SARS-CoV-2 spike protein, show differential binding kinetics of SARS-CoV and SARSCoV-2 spike proteins to ACE2, and determine pharmakocinetic parameters of ACE2-Ig in mice.
 Article
 Open Access
 24 Apr 2020
 Nature Communications
 9 Apr 2020
 Nature
 Here, it is shown that there was community transmission of SARS-CoV-2 early in January.
 Brief Communication
 7 Apr 2020
 Nature Microbiology
 A study of 246 individuals with seasonal respiratory virus infections randomized to wear or not wear a surgical face mask showed that masks can significantly reduce detection of coronavirus and influenza virus in exhaled breath and may help interrupt virus transmission.
 3 Apr 2020
 Nature Medicine
 1 Apr 2020
 High-resolution crystal structures of the receptor-binding domain of the spike protein of SARS-CoV-2 and SARS-CoV in complex with ACE2 provide insights into the binding mode of these coronaviruses and highlight essential ACE2-interacting residues.
 30 Mar 2020
 The crystal structure of the receptor-binding domain of the SARS-CoV-2 spike in complex with human ACE2 sheds light on the structural differences compared with the receptor-binding domain of SARS-CoV that increase its binding affinity to ACE2.
 SARS-CoV-2 in patient samples is detected in under an hour using a CRISPR-based lateral flow assay.
 Letter
 16 Apr 2020
 Nature Biotechnology
 SARS-CoV-2 has spread globally. Here, the authors characterize the entry pathway of SARS-CoV-2, show that the SARS-CoV-2 spike protein is less stable than that of SARS-CoV, and show limited cross-neutralization activities between SARS-CoV and SARS-CoV-2 sera.
 27 Mar 2020
 Data Descriptor
 24 Mar 2020
 Scientific Data
 An estimation of the clinical severity of COVID-19, based on the data available so far, can help to inform the public health response during the ongoing SARS-CoV-2 pandemic.
 19 Mar 2020
 Correspondence
 17 Mar 2020
 16 Mar 2020
 The outbreak of 2019-novel coronavirus disease (COVID-19) that is caused by SARS-CoV-2 has spread rapidly in China, and has developed to be a Public Health Emergency of International Concern. However, no specific antiviral treatments or vaccines are available yet. This work aims to share strategies and candidate antigens to develop safe and effective vaccines against SARS-CoV-2.
 6 Mar 2020
 npj Vaccines
 This study describes the development of an approach to rapidly screen lineage B betacoronaviruses, such as SARS-CoV and the recently emerged SARS-CoV-2, for receptor usage and their ability to infect cell types from different species. Using it, they confirm human ACE2 as the receptor for SARs-CoV-2 and show that host protease processing during viral entry is a significant barrier for viral entry.
 24 Feb 2020
 Phylogenetic and metagenomic analyses of the complete viral genome of a new coronavirus from the family Coronaviridae reveal that the virus is closely related to a group of SARS-like coronaviruses found in bats in China.
 3 Feb 2020
 Characterization of full-length genome sequences from patients infected with a new coronavirus (2019-nCoV) shows that the sequences are nearly identical and indicates that the virus is related to a bat coronavirus.
 Nature recorded major events as the pandemic spread across the globe.
 News
 22 Apr 2020
 Here, Hotez and colleagues highlight the two ‘faces’ of immune enhancement that could impact COVID-19 vaccine design.
 28 Apr 2020
 Nature Reviews Immunology
 In Brief
 The COVID-19 crisis could change the way we conduct our scientific lives, for better and for worse.
 Editorial
 Nature Ecology & Evolution
 Radiotherapy can be safely delivered during the coronavirus disease 2019 (COVID-19) pandemic, often through use of hypofractionated regimens, which minimize the number of visits to treatment centres while also avoiding potentially detrimental delays in the delivery of cancer care.
 27 Apr 2020
 Nature Reviews Clinical Oncology
 Nearly 100 years since it was first used in humans as a vaccine for tuberculosis, Bacillus Calmette–Guérin (BCG) has been suggested as a possible agent to prevent coronavirus disease 2019 (COVID-19). A number of studies are underway to investigate this possibility but — even if they prove effective — many questions will remain.
 Nature Reviews Urology
 Coronavirus disease 2019 (COVID-19) is caused by severe acute respiratory syndrome coronavirus 2, which invades a cell through binding to the ACE2 receptor and TMPRSS2 priming. Patients with severe disease predominantly present with pneumonia-related symptoms. However, evidence suggests that COVID-19 infection also has implications for the urogenital tract. Thus, urogenital organs should be considered when treating COVID-19.
 20 Apr 2020
 Preliminary data suggest that people with obesity are at increased risk of severe COVID-19. However, as data on metabolic parameters (such as BMI and levels of glucose and insulin) in patients with COVID-19 are scarce, increased reporting is needed to improve our understanding of COVID-19 and the care of affected patients.
 23 Apr 2020
 Nature Reviews Endocrinology
 Crowdsourcing efforts are currently underway to collect and analyze data from patients with cancer who are affected by the COVID-19 pandemic. These community-led initiatives will fill key knowledge gaps to tackle crucial clinical questions on the complexities of infection with the causative coronavirus SARS-Cov-2 in the large, heterogeneous group of vulnerable patients with cancer.
 21 Apr 2020
 Nature Cancer
 15 Apr 2020
 Over 180 clinical trials of proposed COVID-19 drugs are already recruiting patients, and another 150 are registered to start recruiting patients soon. But many of these trials are small and not designed to identify the best treatment strategies for the COVID-19 pandemic. For Chief Executive of the Research Council of Norway John-Arne Røttingen, a more collaborative approach is now needed. And as Chair of the Executive Group and the International Steering Committee of the WHO’s recently launched Solidarity trial, he hopes this mega-trial can provide a blueprint, he told Asher Mullard.
 An Audience With
 14 Apr 2020
 Nature Reviews Drug Discovery
 Research Highlight
 Clinicians and researchers are rapidly adapting to working in the midst of a pandemic. Herein, we share our initial thoughts of the consequences of COVID-19 for the oncology community.
 6 Apr 2020
 Previous crises have shown how an economic crash has dire consequences for public health. But in the COVID-19 pandemic, the world is entering uncharted territory. The world’s leaders must prepare to preserve health.
 As the COVID-19 pandemic sweeps through the world, we must reassess the principles that guide our individual and collective responses and the way we operate in society. In the face of crisis, we must lead with science and humanity.
 As the spread of SARS-CoV-2 has triggered worldwide closures of research labs and facilities, Kostas Kostarelos shares his views on what may be going wrong in the fight against COVID-19 and how the nanoscience community could and should contribute.
 Thesis
 Nature Nanotechnology
 In the short time since SARS-CoV2 emerged, much has been learned about the immunopathology of the infection. Here, Xuetao Cao discusses what these early insights imply for drug discovery and clinical management.
 The prevalence of direct kidney involvement in novel coronavirus disease (COVID-19) is low, but such involvement is a marker of multiple organ dysfunction and severe disease. Here, we explore potential pathways of kidney damage and discuss the rationale for extracorporeal support with various blood purification strategies in patients who are critically ill with COVID-19.
 Nature Reviews Nephrology
 Many physicists want to use their mathematical modelling skills to study the COVID-19 pandemic. Julia Gog, a mathematical epidemiologist, explains some ways to contribute.
 World View
 8 Apr 2020
 Nature Reviews Physics
 Patients on haemodialysis or peritoneal dialysis are likely to be at increased risk of novel coronavirus disease (COVID-19). Preventive strategies must be implemented to minimize the risk of disease transmission in dialysis facilities, including education of staff and patients, screening for COVID-19 and separation of infected or symptomatic and non-infected patients.
 COVID-19 has started to reach Africa, a continent that has in recent decades faced the ongoing HIV/AIDS pandemic and the Ebola epidemic of 2014–2016. Dr Matshidiso Moeti, WHO Regional Director for Africa, talks to Nature Human Behaviour about the African response to COVID-19.
 Q&A
 Nature Human Behaviour
 The stock market provides a view of what investors expect for the future. It is precisely in complex situations such as the COVID-19 outbreak that the prescience of the market is particularly valuable, argues Alexander F. Wagner.
 2 Apr 2020
 Growth-at-any-cost economics has health costs, a reality the COVID-19 pandemic brings into sharp relief. Governments must manage the tension between economics and health, but they should not stray from their original mandate to protect people. Too much dependence on the private sector weakened pandemic response, argues Susan Erikson.
 The debate over whether autocracies or democracies are better at fighting epidemics is misguided. Under President Xi Jinping’s centralized command, his administration has both succeeded and failed at handling the COVID-19 crisis. While it effectively curbed infections within China after the virus had spread, it failed to stem the outbreak before it went global.
 The human tendency to impose a single interpretation in ambiguous situations carries huge dangers in addressing COVID-19. We need to search actively for multiple interpretations, and governments need to choose policies that are robust if their preferred theory turns out to be wrong, argues Nick Chater.
 In the current absence of medical treatment and vaccination, the unfolding COVID-19 pandemic can only be brought under control by massive and rapid behaviour change. To achieve this we need to systematically monitor and understand how different individuals perceive risk and what prompts them to act upon it, argues Cornelia Betsch.
 The global practice of monetizing ecosystems to further national economic development has laid fertile ground for the COVID-19 pandemic and others like it, writes Cobus van Staden.
 23 Mar 2020
 The global COVID-19 pandemic has the potential to severely affect those with rheumatic diseases or who are taking immunosuppressive therapies. Information is lacking as to how these groups will fare if they become infected. A global alliance has rapidly formed to try to address this information deficit.
 Nature Reviews Rheumatology
 The past decade has allowed the development of a multitude of digital tools. Now they can be used to remediate the COVID-19 outbreak.
 COVID-19 has affected vulnerable populations disproportionately across China and the world. Solid social and scientific evidence to tackle health inequity in the current COVID-19 pandemic is in urgent need.
 26 Mar 2020
 The coronavirus disease (COVID-19) worldwide outbreak has led to a dramatic challenge for all healthcare systems, including inflammatory bowel disease (IBD) centres. Here, we describe the fast changes and clinical issues that IBD specialists could face during this SARS-CoV-2 infection pandemic, highlighting the potential rearrangements of care and resetting of clinical priorities.
 25 Mar 2020
 Nature Reviews Gastroenterology & Hepatology
 Chloroquine — an approved malaria drug — is known in nanomedicine research for the investigation of nanoparticle uptake in cells, and may have potential for the treatment of COVID-19.
 The world is currently in the grip of the COVID-19 pandemic. Rapid changes in medical priorities are being enforced across all health-care systems. Urologists have had to reduce or halt their clinical activity and assist on COVID-19 wards. The repercussions on urological patient outcomes for delayed treatments and diagnosis remain to be defined.
 A seasoned public-health institute puts Nigeria in a good position to respond to COVID-19, although there are area-specific challenges to be addressed. Nature Medicine reports from Nigeria.
 News Feature
 11 Mar 2020
 A compounding factor of the outbreak is the potential for the general public to misunderstand the facts and design conspiracy theories.
 9 Mar 2020
 Severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2) infects host cells through ACE2 receptors, leading to coronavirus disease (COVID-19)-related pneumonia, while also causing acute myocardial injury and chronic damage to the cardiovascular system. Therefore, particular attention should be given to cardiovascular protection during treatment for COVID-19.
 5 Mar 2020
 Nature Reviews Cardiology
 As the COVID-19 outbreak continues, the next pandemic could be prevented by ending the wildlife trade and reinvesting in the monitoring of potential zoonoses.
 20 Feb 2020
 Mutation. The word naturally conjures fears of unexpected and freakish changes. Ill-informed discussions of mutations thrive during virus outbreaks, including the ongoing spread of SARS-CoV-2. In reality, mutations are a natural part of the virus life cycle and rarely impact outbreaks dramatically.
 18 Feb 2020
 Therapeutic options in response to the 2019-nCoV outbreak are urgently needed. Here, we discuss the potential for repurposing existing antiviral agents to treat 2019-nCoV infection (now known as COVID-19), some of which are already moving into clinical trials.
 10 Feb 2020
 Experts weigh up the best- and worst-case scenarios as the World Health Organization declares a global health emergency.
 News Explainer
 31 Jan 2020
 The strengthening of the Chinese Center for Disease Control and Prevention has been a turning point in outbreak responses in the area. This represents very welcome progress and development for global health security and diplomacy.
 27 Jan 2020
 How science can help control the outbreak
 Nature Video
 24 Jan 2020
 As the international community responds to an outbreak of coronavirus-induced pneumonia in Wuhan, China, early and open data sharing — which are vital for its control — depend on the trust that the data will not be used without proper attribution to those who generated it.
 22 Jan 2020
 Zoonotic diseases present a substantial global health burden. In this Opinion article, Plowrightet al. present an integrative conceptual and quantitative model that reveals that all zoonotic pathogens must overcome a hierarchical series of barriers to cause spillover infections in humans.
 Opinion
 30 May 2017
 Nature Reviews Microbiology
 As the line dividing human and wild habitats becomes thinner, we might be brewing the world's next big pandemic. Zoonoses are diseases that are naturally transmitted between animals and humans, and a new project aims to predict their occurrence.
 Feature
 23 Mar 2017
 From the Analyst's Couch
 Here a group of leaders in the field define our current understanding of ‘trained immunity’, which refers to the memory-type responses that occur in the innate immune system. The authors discuss our current understanding of the key epigenetic and metabolic processes involved in trained immunity and consider its relevance in immune-mediated diseases and cancer.
 Review Article
 4 Mar 2020
 In the short time since SARS-CoV-2 infections emerged in humans, much has been learned about the immunological processes that underlie the clinical manifestation of COVID-19. Here, the authors provide an overview of the pathophysiology of SARS-CoV-2 infection and discuss potential therapeutic approaches.
 In this Perspective article, the authors recount the earliest stages of translational research into IL-6 biology and the subsequent development of therapeutic IL-6 pathway inhibitors for the treatment of autoimmune rheumatic diseases and potentially numerous other indications.
 Perspective
 Shivashankar and Uhler propose a link between cell mechano-genomics and coronavirus replication with implications for understanding the pathogenicity of SARS-CoV-2.
 Nature Reviews Molecular Cell Biology
 Hydroxychloroquine and chloroquine are antimalarial drugs commonly used for the treatment of rheumatic diseases. Multiple mechanisms might explain the efficacy and adverse effects of these drugs, but further investigation could lead to the development of more specific and potent drugs.
 7 Feb 2020
 We need to integrate the knowledge and skills from different disciplines and from communities all over the world to enable effective responses to future epidemics.
 6 Nov 2019
 20 Sep 2019
 Clinical metagenomic next-generation sequencing (mNGS) is rapidly moving from bench to bedside. This Review discusses the clinical applications of mNGS, including infectious disease diagnostics, microbiome analyses, host response analyses and oncology applications. Moreover, the authors review the challenges that need to be overcome for mNGS to be successfully implemented in the clinical laboratory and propose solutions to maximize the benefits of clinical mNGS for patients.
 27 Mar 2019
 Nature Reviews Genetics
 Acute respiratory distress syndrome (ARDS) is the rapid onset of noncardiogenic pulmonary oedema, hypoxaemia and the need for mechanical ventilation in hospitalized patients. This Primer describes the risk factors for ARDS, the underlying pulmonary damage and repair in ARDS and the long-term consequences for survivors.
 Primer
 14 Mar 2019
 Nature Reviews Disease Primers
 The emerging field of precision epidemiology allows the personalized diagnosis, tracking and treatment of infectious diseases.
 6 Feb 2019
 A Perspective discussing the factors that have contributed to the success and failure of point-of-care tests for resource-limited settings and the challenges and opportunities that exist for developing new infectious disease diagnostics.
 13 Dec 2018
 This Review Article describes how recent advances in viral genome sequencing and phylogenetics have enabled key issues associated with outbreak epidemiology to be more accurately addressed, and highlights the requirements and challenges for generating, sharing and using such data when tackling a viral outbreak.
 Coronaviruses have a broad host range and distribution, and some highly pathogenic lineages have spilled over to humans and animals. Here, Cui, Li and Shi explore the viral factors that enabled the emergence of diseases such as severe acute respiratory syndrome and Middle East respiratory syndrome.
 10 Dec 2018
 The virulence of viruses is a major determinant of the health burden of viral infections in humans and other species. In this article, Geoghegan and Holmes discuss how largely disparate research fields — theoretical modelling of virulence evolution and experimental dissection of genetic virulence determinants in laboratory model systems — can be bridged by considering real genomic data of viral evolution in a phylogenetic context. They describe the underlying principles of virulence evolution and how they apply to real-world viral infections and outbreaks of global importance.
 10 Oct 2018
 Ubiquitin-like protein ISG15 is an interferon-induced protein that has been implicated as a central player in the host antiviral response. In this Review, Perng and Lenschow provide new insights into how ISG15 restricts and shapes the host response to viral infection and the viral immune-evasion strategies that counteract ISG15.
 16 May 2018
 So-called super-antibodies are highly potent, broadly reactive antiviral antibodies that offer promise for the treatment of various chronic and emerging viruses. This Review describes how recent technological advances led to their isolation from rare, infected individuals and their development for the prevention and treatment of various viral infections.
 30 Jan 2018
 Emerging viral diseases present a huge and increasingly important global threat to public health systems. Graham and Sullivan discuss the challenges presented by emerging viral diseases and discuss how innovations in technology and policy can address this threat.
 14 Dec 2017
 Nature Immunology
 Next-generation sequencing has the potential to support public health surveillance systems to improve the early detection of emerging infectious diseases. This Review delineates the role of genomics in rapid outbreak response and the challenges that need to be tackled for genomics-informed pathogen surveillance to become a global reality.
 13 Nov 2017
 Deubiquitylating enzymes (DUBs) have been implicated in several human diseases, including cancer, neurodegenerative diseases, inflammatory and autoimmune disorders, as well as infectious diseases. Here, Jackson and colleagues discuss the pathological roles of DUBs, consider the challenges in the development of selective DUB inhibitors and highlight first-generation agents approaching clinical trials.
 29 Sep 2017
 15 Jan 2020
 Scientific Reports
 Remdesivir (RDV) is a broad-spectrum antiviral drug with activity against MERS coronavirus, but in vivo efficacy has not been evaluated. Here, the authors show that RDV has superior anti-MERS activity in vitro and in vivo compared to combination therapy with lopinavir, ritonavir and interferon beta and reduces severe lung pathology.
 10 Jan 2020
 Viruses rely on host cell metabolism for replication, making these pathways potential therapeutic targets. Here, the authors show that AM580, a retinoid derivative and RAR-α agonist, affects replication of several RNA viruses by interfering with the activity of SREBP.
 10 Jan 2019
 Here, Gassen et al. show that S-phase kinase-associated protein 2 (SKP2) is responsible for lysine-48-linked poly-ubiquitination of beclin 1, resulting in its proteasomal degradation, and that inhibition of SKP2 enhances autophagy and reduces replication of MERS coronavirus.
 18 Dec 2019
 Cryo-EM structures of MERS-CoV S glycoprotein trimer in complex with different sialosides reveal how the virus engages with sialylated receptors, providing insight into receptor specificity and selectivity.
 2 Dec 2019
 Nature Structural & Molecular Biology
 Implantation of lung tissue into humanized mice enables in vivo study of the human immune response to pathogens.
 26 Aug 2019
 Antibodies that target the N-terminal domain (NTD) of the MERS-CoV spike remain poorly characterized. Here, Zhou et al. report the structural and functional analysis of the NTD-targeting mAb 7D10 and show that it synergizes with antibodies targeting the receptor-binding domain against different MERS-CoV strains.
 11 Jul 2019
 Structural and functional analyses reveal how 9-O-acetyl sialic acid is recognized by the human coronavirus OC43 S glycoprotein and how this interaction promotes viral entry.
 3 Jun 2019
 The pathogenic human coronaviruses SARS- and MERS-CoV can cause severe respiratory disease. Here the authors present the 3.1Å cryo-EM structure of the SARS-CoV RNA polymerase nsp12 bound to its essential co-factors nsp7 and nsp8, which is of interest for antiviral drug development.
 28 May 2019
 14 May 2019
 Dampened activation of the NLR family pyrin domain containing 3 (NLRP3) inflammasome in bat primary immune cells in response to infection with multiple zoonotic viruses is caused by decreased transcriptional priming, the presence of a unique splice variant and an altered leucine-rich repeat domain of bat NLRP3.
 25 Feb 2019
 4 Feb 2019
 9 Nov 2018
 Rachel Graham et al. show that coronaviruses with extensively rewired transcription regulatory networks are effective vaccine candidates against Severe Acute Respiratory Syndrome Coronavirus. The reversion of live-attenuated vaccine strains to virulence can be avoided with these vaccines.
 29 Oct 2018
 Communications Biology
 24 Oct 2018
 11 Oct 2018
 Corticosteroid therapy is frequently used for chronic obstructive pulmonary disease (COPD) but its use is associated with increased risk of pneumonia. Here the authors show that corticosteroid use impairs innate and adaptive immunity to rhinovirus infection, which is restored by exogenous IFNβ.
 8 Jun 2018
 8 May 2018
 Analysis of viral samples from deceased piglets shows that a bat coronavirus was responsible for an outbreak of fatal disease in China and highlights the importance of the identification of coronavirus diversity and distribution in bats in order to mitigate future outbreaks of disease.
 4 Apr 2018
 Understanding global epidemics spread is crucial for preparedness and response. Here the authors introduce an analytical framework to study epidemic spread on air transport networks, and demonstrate its power to estimate key epidemic parameters by application to the recent influenza pandemic and Ebola outbreak.
 15 Jan 2018
 16 Oct 2017
 Mice made susceptible to MERS-CoV, using CRISPR–Cas9 to alter the gene encoding the dipeptidyl peptidase 4 receptor, allow efficient viral replication in the lungs and display symptoms indicative of severe acute respiratory stress.
 28 Nov 2016
 Advanced search
 https://www.nature.com/collections/hajgidghjb
