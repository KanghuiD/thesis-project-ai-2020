TrackTogether: Coronavirus app & 'spread the survey' campaign | Goal.com
 Coronavirus has caused a global suspension of sports with no return date in mind for many football competitions due to the severity of the situation.
 Some countries have taken the steps of not only banning sporting events for the foreseeable future but also to enacting lockdown-style protocol, with people only permitted to leave the house for necessary reasons such as food shopping or work if their jobs are deemed essential.
 The spread of Covid-19 is unprecedented but a new app called TrackTogether says it is working with researchers at leading universities in the hope of combating the virus in a global initiative.
 TrackTogether is a not-for-profit organisation which aims to track and stop the global spread of coronavirus.
 It hopes to do this by bringing together software engineers, epidemiologists and public health research scientists to gather relevant data to combat the spread of coronavirus and aim to prevent future outbreaks.
 To get involved, people can visit the TrackTogether website and complete a short survey. 
 By collecting information, the organisation can give governments greater knowledge of the disease in countries, help keep symptomatic citizens at home unless they need medical help, and alert local officials about silent infections in communities.
 The stated goal is to collect anonymised data to build a clear picture of how many people worldwide have been experiencing symptoms of coronavirus, which allows scientists to track the spread of the virus and potentially halt it in countries less impacted.
 The TrackTogether app will be launched soon and it will be available to download via the iPhone App Store or Google Play for Android users.
 From there, you will be able to answer questions pertaining to your health and the app will send that information to researchers and public health experts who are working to better understand Covid-19.
 In the meantime, people are encouraged to go to tracktogether.org and complete a user survey on coronavirus symptoms and self-isolation.
 Once live, the website can also be visited to download the app, as well as to see a live tracker of how many other people have done so.
 https://www.goal.com/en-bh/news/tracktogether-coronavirus-tracking-app/12pkfbflik2hh1l82ru738gmi3
