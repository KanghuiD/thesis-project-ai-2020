Coronavirus Research and Innovation | Tulane University
 Since our founding, Tulane researchers have been at the forefront of combatting infectious disease.
 Researchers are conducting a study to determine whether hydroxychloroquine can prevent COVID-19. Patricia Kissinger, PhD, infectious disease epidemiologist at Tulane School of Public Health and Tropical Medicine, and Dr. Alfred Luk, professor at Tulane School of Medicine, are leading the study.
 Read More
 Bob Garry, professor of microbiology and immunology
 A leading expert in virology, Bob Garry is part of a team decoding the genome of the coronavirus to determine its origins and how it possibly evolved from bats and pangolins. Garry also has worked extensively to develop rapid diagnostics, vaccines and new therapies for infectious diseases like Lassa virus and Ebola.
 Tony Hu is a pioneer in developing advanced diagnostics for personalized medicine. Hu is working to develop highly sensitive blood or saliva tests that rely on nanotechnology to help doctors quickly diagnose COVID-19.
 Disease ecologist Lina Moses, PhD, is a leading expert in tropical virology. She spent time in Geneva coordinating research efforts for WHO and the Global Outbreak Alert and Response Network.
 Mac Hyman is developing mathematical models to better understand and predict the spread of COVID-19. His research provides a methodology to predict the number of new infections five, 10 and 15 days into the future.
 Under the direction of Jay Rappaport, Tulane National Primate Research Center is establishing a research program to develop a vaccine for the virus. He is leading a team to create one of the first nonhuman primate models of the disease.
 Maps & Directions
 https://tulane.edu/coronavirus-research-and-innovation
