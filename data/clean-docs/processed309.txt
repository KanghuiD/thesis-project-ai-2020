Herd immunity or lockdown: Which works better against coronavirus | Op-eds – Gulf News
 Cases of re-infections cast doubts on the prospects of a viable vaccine
 The Swedish government has gone out on a limb with its implementation of a strategy known as ‘herd immunity’ to beat back the most aggressive pandemic since the 1918 Spanish flu that killed over 50 million worldwide.
 At the outset Britain’s Prime Minister Boris Johnson chose a similar path only to beat a hasty retreat when Imperial College immunologists published a report underscoring a terrifying projection that leaving the nation opened up would result in 500,000 deaths.
 Whereas the vast majority of afflicted countries, including Sweden’s closest Scandinavian neighbours, are using social distancing to varying degrees to contain the spread of COVID-19 until either a cure or a vaccine becomes available, life for the Swedish people remains virtually unchanged.
 Cafes, restaurants, stores, resort hotels and many schools remain open on the premise that the more people infected, the more will gain immunity.
 In the meantime, as of Saturday, the number of cases in Sweden totalled 13,822 as opposed to Denmark and Norway where cases were in the low 7000s.
 Swedish officials argue that forcibly keeping people confined to their homes will not work in the long term.
 As I gaze out of my window at cloudless blue skies longing to stroll on the beach and soak up the sun I can sympathise. But what if the Swedish model turns out to be based on false projections? This particular virus doesn’t run to form.
 Why is it that so many of those infected are asymptomatic or experience the mildest of symptoms whereas others considered to be strong and healthy are fatally stricken?
 Why is it killing more men than women (twice as many in England and Wales and in Italy 71 per cent of deaths were males)?
 It is a devious virus known to reach its highest level of contagion before symptoms manifest and while it targets all who come into contact, children and teenagers rarely succumb to its serious effects.
 There is even a suggestion that people with a certain blood type are more susceptible than others.
 Sweden is counting on herd immunity that inevitably comes with a heavy price in terms of fatalities paying off down the road, but could that be a false hope given that the virus is mutating into different strains and, most troubling of all, the World Health Organisation (WHO) now says evidence fails to indicate that patients who have recovered have immunity.
 “There are a lot of countries that are suggesting using rapid diagnostic serological tests to be able to capture what they think will be a measure of immunity,” but “right now we have no evidence that the use of serological tests can show that an individual has immunity or is protected from re-infection,” warned WHO epidemiologist Dr. Maria van Kerkhove during a press conference held in Geneva.
 Her assessment has merit given that more than 141 recovered patients in South Korea have re-tested positive and similar cases have been reported in China. John Power writing in the South China Morning Post suggests this trend “raises doubts about developing a vaccine”.
 That said experts are putting forth a smorgasbord of alternative explanations ranging from faulty results, virus mutations to harmless remnants of the disease remaining in the body.
 The fact is that until this accursed plague takes its course for a year or more it cannot be fully understood. There are ‘experts’ who still believe that it will hibernate during the summer months only to rear its ugly head in the autumn.
 I would love to know why it has hit hardest some of the world’s wealthiest advanced nations — the US, Spain, Italy, France, Germany and the UK — which all have mandatory social distancing policies in place.
 In the meantime, the waiting game continues and although there is an information overload hitting us from all directions, in reality we are none the wiser.
 The question on everyone’s lips, particularly the newly unemployed and the owners of small businesses accruing debt, is ‘when can we get our livelihoods back on track?’
 All eyes on the US set to begin a phased return to normal in the coming weeks despite a surge in new cases to reinvigorate economic health where outcomes may provide us with a clue.
               Get Breaking News Alerts From Gulf News
             
 We’ll send you latest news updates through the day. You can manage them any time by clicking on the notification icon.
 By clicking below to sign up, you're agreeing to our
 and
 Forgot password
 or
 https://gulfnews.com/opinion/op-eds/herd-immunity-or-lockdown-which-works-better-against-coronavirus-1.71061610
