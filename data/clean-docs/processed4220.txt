Urban Milwaukee »  Op-Ed
 Pandemic shutdown has added challenges for system. The typical education bureaucrat won’t cut it. 
 City will eventually need to recover. Why not begin economic development planning now?
 “Freedom” and the “free market” won’t solve the pandemic. It will spread the disease. 
 Over last decade state leaders have backed 40 actions that have harmed environment. 
 Their push to cut WHO funding criticized by American Medical Association, U.S. Chamber of Commerce.
 They have a raw power that doesn’t need the approval of art critics, curators or anyone else.
 A plea from healthcare leaders: our workers' lives depend on you. 
 April in-person election amounted to Republican terrorism. The way forward is voting by mail. 
 Highway funding will be hard hit. Here’s how to replace the gas tax. 
 Voters angry about in-person election may have reacted by opposing Republican choice for high court.
 Whole series of executive actions and programs by Gov. Evers. Nothing by state Legislature. 
 Yes the pandemic is worrisome. But it's compounded by a political virus, a lack of leadership.
 Read more Press Releases
 Join Today
 More New Faces
 Take the next step, become a member.
 Ditch the ads, get free stuff
 Learn more
 This ad will close automatically in 15 seconds.
 Tired of seeing banner ads on Urban Milwaukee? Become a member for $9/month.
 https://urbanmilwaukee.com/category/series/op-ed/
