Independentie
 Search
 Sunday, 26 April 2020 | 8.1°C Dublin
 Menu
 Sections
 Close
 Gardai conduct a COVID-19 checkpoint near Newbridge, Co. Kildare
 Colin Keegan
     April 26 2020 01:30 AM
 Sir - We have rapidly gone from normal life to being told not to leave home unless absolutely necessary and then implementing social distancing. 
 It has become our new reality.
 But people living with cystic fibrosis know more than most the importance of isolating, of washing our hands, of social distancing and the importance of trying to keep well. People with cystic fibrosis are in a high-risk category and should be cocooning.
 CF is a genetically inherited disease that primarily affects the lungs. Exercise is extremely vital for us, as it's the most effective way of getting the mucus out of our lungs. For most patients with CF, going outside is the main exercise but now we're trying to do it all from home. I am lucky to have an exercise bike.
 My routine has changed during Covid-19. I am only allowed out into the garden and otherwise cannot step foot outside the house (it's hard to describe how it feels not being allowed out the front door). I try to structure my day, and try to focus on my health. By the way, did I mention that I'm 16?
 I guess I was lucky in a way, as before the lockdown I was in hospital for three weeks with an infection. I was on intense IV antibiotics, so leaving the hospital room was not an option. I effectively had a three-week head start in isolation. So I have now been in isolation for over two months - but this is good and I am extremely grateful for it, because if someone with CF like me got the virus, it could be really serious.
 It has been tough on my whole family, who are all cocooning with me - none of us can go to the shops because the risk is too high. But we have fantastic friends and family who have been getting our food, collecting my medication and generally giving us support.
 We have all taken many measures - to protect me. My mum works in the healthcare industry, but she removed herself from her job as it was too risky. But between my mum's friends and colleagues checking in on us, we couldn't have felt safer or as well looked after as we do. The support from everyone has been overwhelming. Thank you.
 My Covid-19 daily routine now consists of me getting up early, doing my nebulisers, taking my medication, then doing 20 minutes on my exercise bike. Weather depending, I might go out to the garden to enjoy the sun. After lunch I take another nebuliser, and more exercise - either on my bike or just do some laps of my garden. In the evening, more nebulisers and more tablets.
 I'd like to finish by saying well done everyone else at risk who is keeping themselves safe and well. We're doing great together.
 However I heard that there is a risk of complacency setting in, with healthy people stopping taking the precautions against the spread of the virus. Please don't grow complacent. There are times that high-risk people must rely on you to help save our lives. This is one of those times.
 Benat Broderick,
 Dublin
 Sean always kept up the hurling
 Sir - When I heard Sean O'Rourke announce his imminent retirement, from RTE, I somehow knew that the prospects for serious hurling for the rest of the year had seriously diminished.
 I always delighted in how Sean coloured and animated his interviews, arguments and interrogations with hurling analogies.
 In fact, he was fairly handy at 'senior hurling' himself, as more than a few nippy 'political' wing(ing) forwards found out, having been dispatched over the side line with a well-timed shunt or as many a patronised 'institutional' full forward who found themselves tangled in the net and the ball gone wide, may also testify.
 Whether a hurler on the ditch, a bit of a mullocher or even having a good eye for the ball, it was never advisable to get into a shemozzle with Sean O'Rourke because inevitably there would be only one winner.
 For us who pay full ticket price, without premium trimmings, ni bheidh a leitheid ann aris.
 Michael Gannon,
 Thomas Square, Kilkenny
 Sheila was faster than her postcard
 Sir - Niamh Horan's enjoyable but short piece on postcards (Sunday Independent, April 19) reminded me of years ago when Sheila, our next door neighbour, went on her first holiday to Spain.
 She sent a postcard to my Mam, "Hello, Mrs Abb, I'm on holidays in Spain, weather great, see you next week."
 Sheila was home the next week. But the postcard from Spain arrived two weeks later.
 Joan Hoban,
 Limerick
 Unfair to be so hard on Keelings
 Sir - Why are Keelings, one of our most amazing indigenous success stories, getting such a hard time from some sections of the media?
 During and after this crisis our home food production is vital. We need these industries to plan ahead, securing seed and fertiliser - which they seem to have in abundance on the Northside.
 We do not want Keelings to throw in the towel, because industries such as theirs are vital to our nation's long-term survival. Don't forget, we are very dependent on imported food and ingredients. I for one am looking forward to the "Bulgarian" strawberries.
 Michael Foley,
 Rathmines, Dublin 6
 Gene, you seem to have gone rogue
 Sir - In a time of crisis we probably all agree we need a strong government to get us through the next years.
 Well, in my opinion Gene Kerrigan's choice of cabinet (Sunday Independent, April 19)would not get us through four or five weeks. Half of them have their own individual goals they would wish to pursue. The Green candidates have barely any political experience at all - and you want them to make decisions in a time of major crisis? He recommends John McGuinness because "he doesn't play the party line" - so he is on a collision course before he even sits down at the Cabinet table.
 Gene, I think you have gone rogue - and you know there's not a hope in hell of most of them making Cabinet.
 Donough O Reilly,
 Kilmacud, Co Dublin
 Let's dip in to help stricken children
 Sir - When I came home after starting school over 60 years ago, I was presented with a small blackboard and a single piece of chalk. I remember being bored and lonely and wishing I had not started at all.
 But the school system here today is so caring and proactive in providing interesting lessons for children via technology. Most parents and guardians are able and willing to look after the children in their care.
 But not all children are so fortunate. Here in Ireland there are sick children and children living in stressful circumstances who all need additional support. But images of children in refuge camps, war-torn countries and poverty-stricken countries are the most heart-wrenching.
 It's possible they have no grandparents that can help them, but maybe we can?
 There are many organisations here in Ireland well placed to help these children. They need financial help. Can you give it? It's easy and safe to make a donation by phone or computer. The reputable charities have easy-to- use websites.
 So, grandparents of Ireland, let's consider spreading the love to those less fortunate than ourselves.
 A little help from a lot of people can make a big difference.
 Noeleen Dunne,
 Drogheda, Co Louth
 Staying in is the least we can do to help
 Sir — It seems like the whole world has gone to sleep or we are waiting to breathe out. On one hand there are no children’s voices, no noise from traffic, peaceful streets and empty pavements and parks. On the other, the hospitals must be bedlam, with doctors and nurses rushing around trying to save lives, and people fighting for their lives.
 Listening to the radio, watching TV or reading the papers, we are confronted with grim news every minute of every day. It is hard to stay positive. I’m sure like many, I keep getting the urge to go out but I curtail myself and do something else, such as gardening, decorating or just sitting in the sun. The sun has been a godsend. 
 However, for our front-line staff, they cannot avail of the good weather. It must be terrifying to have to go in every day not knowing what might happen, when all we are asked to do is stay at home. Surely, that’s the least we can do? Yes, it’s hard, but can you imagine what it’s like for our medics? Would you swap places? I have the highest admiration for these people, not forgetting porters and cleaning staff, so a big thank you to them. Hopefully medical staff will stay well and we will come through.
 Roisin Glynn-Steed,
 Rahoon, Co Galway
 Promised spending a political fairy tale
 Sir — The promises of Fianna Fail and Fine Gael concerning taxes, USC charge and social welfare payments – in light of the billions being spent on the coronavirus pandemic – is pie-in-the-sky fairy-tale economics. 
 It is essential to spend the money to save our people — but the debt will have to be repaid. Europe, as has been the practice, will not pay our debts.
 The promises were made to lure the smaller parties to join a coalition and to hoodwink the Irish people about the desperate economic situation facing us in the future. The promises will be like straws in the wind.
 No favours are ever given by majority parties in coalition and this will not change. 
 David J Fitzgerald, 
 Dun Laoghaire, Co Dublin
 Politicians need to join the suffering
 Sir — First, can I offer my condolences to all of those affected by a loved one’s death and unable to show their love and respect — but we need a working government urgently. 
 Fianna Fail and Fine Gael need a third leg for the stool. But we know any smaller party which puts itself forward is afraid of a backlash when we come out of this emergency. 
 The way to take this lead and emphasise the future of the country is to the fore is to suggest — since everybody is suffering — politicians take a pay cut of 75pc and give a firm “no” to unvouched and irregular expenses. Show we are all in this together. 
 Ken Maher, 
 Kilcoole, Co Wicklow
 We are all missing the human touch
 Sir — There have been many articles on the coronavirus in the Sunday Independent. With the greatest of respect to all your hard-working journalists, I pray this subject will end soon.
 One article I could relate to was by Emer O’Kelly entitled ‘I’m desperate for that touch of humanity’.
 I’m a hugger myself. As Emer writes: “We are warm-blooded animals, we humans. Touch acknowledges our humanity: we need to use it.” Let’s hope we can do so in the future, please God.
 Brian McDevitt,
 Glenties, Co Donegal
 Cancel this pointless exam
 Sir — Minister Joe McHugh should stop playing Russian roulette with the Junior Cycle exams before it has serious consequences on the mental and emotional health of about 63,000 15-year-old students. 
 He cancelled the official Junior Cert exam, replacing it with a watered-down version in September. A number of schools have wisely opted out. More are likely to follow.
 Emotionally drained students haven’t seen the inside of a classroom for six weeks. They are unlikely to do so before the school year ends on May 29. After weeks of home study, they are feeling isolated and detached, struggling to stay exam focused and motivated. They are suffering from cabin fever and an overload of virtual learning. They have little opportunity to retain physical and mental equilibrium through normal exercise, leisure and sports. They are in the dark about the timing, duration and content of a token exam with little credibility or validity. 
 Young students are already suffering enough anxiety without a meaningless exam hanging over their heads during the summer months. The minister should abort the substitute September exam. 
 The State Examinations Commission isn’t going to certify that exam, so it will be worthless but still cause great distress. Let the young people enjoy the summer free from exam stress so they can return to school refreshed and ready for senior cycle.
 Billy Ryle, 
 Tralee, Co Kerry
 Ireland needs more David Halls
 Sir — I feel compelled to write after reading about a busy man who retrained as a paramedic so he could help at the front line. I speak of David Hall, a man who we’ve seen in the past give of his time to help people who had run into trouble with their mortgages, etc.  
 He’s shown he has the public interest at heart and has done something about it. Well done, David Hall, we need more people like you in Ireland.
 Tony Hanrahan,
 Mullawnbrack, Co Tipperary
 Scientists shouldn’t delve into politics
 Sir — Esteemed biochemist Prof Luke O’Neill in last week’s Sunday Independent encourages us to “defer to the experts” like himself. But then — rather than deal with the science, which should have “primacy” — he sets out his political beliefs which include calling Donald Trump a gobsh**e. He also tells us how Chinese “have won acclaim for how they responded” — while glossing over the martyrdom of whistle-blower Dr Li Wenliang.  
 Prof O’Neill is entitled to his views — but I was hoping for scientific not political analysis.
 Betty Ruine,
 Ballinlough, Cork
 State of fear run by unelected doctors
 Sir — The restrictions imposed on the people of this country by the National Public Health Emergency Team 
 (Nphet) are akin to that which an authoritarian or totalitarian state would impose against its citizens (or subjects). 
 And then in last week’s Sunday Independent interview with Health Minister Simon Harris, we learnt that he doesn’t make decisions any more, but instead only does what his medical advisers tell him.
 So a body of unelected doctors is now running the country?
 People over 70 are now scared of walking outside their houses for fear of retribution. We can no longer go to the local library or hang out in the local pub.
 Let us all hope that in the next election, whenever that is, we give the current Government a message it won’t forget.
 Liam Doran,
 Clondalkin, Dublin
 Sunday Independent
 An INM Website
     
 https://www.independent.ie/opinion/letters/its-so-important-we-dont-get-complacent-39157493.html
