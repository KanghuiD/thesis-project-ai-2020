Rats could get more aggressive after coronavirus pandemic - Insider
 Rats across America have been left hungry amid mass coronavirus-related business shutdowns, and many are thought to have turned to cannibalism to survive — a move that could lead to a smarter, more aggressive rat breed in the future.
 According to multiple experts, rats that usually feed off garbage from restaurants and arenas have been left hungry as establishments have closed or limited operations to help prevent the spread of the novel coronavirus.
 Now the rats are looking for new food sources, but options are grim.
 Michael H. Parsons, a biological-sciences research scholar at Fordham University who's planning to launch a study about rats and COVID-19, told Insider that traditionally it's often shyer, less social rats that would migrate from colonies into the public eye, but because of food shortages, whole colonies are moving into new environments.
 "You have a smarter and even more resilient group of the population that have not been previously connected to humans, or at least not observed by humans directly," Parsons told Insider.
 But the migrations are a result of food shortages, Bobby Corrigan, an urban rodentologist, told NBC News.
 "A restaurant all of a sudden closes now, which has happened by the thousands in not just New York City but coast to coast and around the world, and those rats that were living by that restaurant, some place nearby, and perhaps for decades having generations of rats that depended on that restaurant food — well, life is no longer working for them, and they only have a couple of choices," he said.
 Corrigan said rats would most likely turn to cannibalism and infanticide to survive.
 "It's just like we've seen in the history of mankind, where people try to take over lands and they come in with militaries and armies and fight to the death, literally, for who's going to conquer that land," he told NBC News. "And that's what happens with rats."
 In the short term, the rats will be reducing their population and limiting breeding — which Parsons called a "best-case scenario" — but it could lead to a new, stronger, breed of rats as a long-term result.
 "It's a great scenario when rats are turning on one another," he said. "They're literally killing one another. But the problem here is that as soon as they find new food, which invariably they will … instead of having low-ranking rats looking for entries into our residencies, we have the smartest, most resilient."
 He added that rats bred quickly — with a 23-day pregnancy period — and those smarter, more aggressive rats could produce a more resilient group of rats searching for food in any way possible.
 "The survivors of the rat migration may convey more risk-taking, aggression, and lead to allele frequency changes and adaptation," he told Insider, citing his proposed rat study. "And we could have more resilient rats available for a possible second wave of the pandemic. Will they be more ready than we are?"
 Parsons said that while there were no reported cases of rats contracting COVID-19, the rodents could spread other diseases including bacterial and parasitic infections and hantavirus into new populations of humans and animals.
 He said a worst-case scenario would include rats becoming infected with diseases and carrying those onto people in new populations.
 Parsons urged people to ramp up rodent-control measures in their homes and businesses to keep more aggressive rodents out. 
 "The important message from my perspective is for people not to let their guards down and open the door to other illnesses," Parsons told Insider. "If a rat were to become infected with the virus, it could possibly mutate inside the rat and become a more virulent pathogen."
 Sign-Up
 https://www.insider.com/rats-could-get-more-aggressive-coronavirus-pandemic-2020-4
