How Has the Coronavirus Changed How You Use the Internet? - The New York Times
 student opinion
 Stuck at home during the pandemic, Americans have been spending more of their lives online. Is the debate over screen and online time over — at least for now?
 By Jeremy Engle
 Find all our Student Opinion questions here.
 How much of your life is online? Has that changed since the coronavirus pandemic?
 How has your internet behavior changed? Are there sites and apps you are visiting and using more? How has the coronavirus affected how you learn, play and connect online?
 In “The Virus Changed the Way We Internet,” Ella Koeze and Nathaniel Popper write:
 Stuck at home during the coronavirus pandemic, with movie theaters closed and no restaurants to dine in, Americans have been spending more of their lives online.
 But a New York Times analysis of internet usage in the United States from SimilarWeb and Apptopia, two online data providers, reveals that our behaviors shifted, sometimes starkly, as the virus spread and pushed us to our devices for work, play and connecting.
 Here are some of the big takeaways from The Times’s analysis of internet usage across the United States:
 We are looking to connect and entertain ourselves, but are turning away from our phones
 With the rise of social distancing, we are seeking out new ways to connect, mostly through video chat
 While traditional social media sites have been growing, it seems that we want to do more than just connect through messaging and text — we want to see one another. This has given a big boost to apps that used to linger in relative obscurity, like Google’s video chatting application, Duo, and Houseparty, which allows groups of friends to join a single video chat and play games together.
 We have also grown much more interested in our immediate environment, and how it is changing and responding to the virus and the quarantine measures. This has led to a renewed interest in Nextdoor, the social media site focused on connecting local neighborhoods.
 The search for updates on the virus has pushed up readership for local and established newspapers, but not partisan sites
 Amid the uncertainty about how bad the outbreak could get — there are now hundreds of thousands of cases in the United States, with the number of dead multiplying by the day — Americans appear to want few things more than the latest news on the coronavirus.
 Among the biggest beneficiaries are local news sites, with huge jumps in traffic as people try to learn how the pandemic is affecting their hometowns.
 Americans have also been seeking out more established media brands for information on the public health crisis and its economic consequences. CNBC, the business news site, has seen readership skyrocket. The websites for The New York Times and The Washington Post have both grown traffic more than 50 percent over the last month, according to SimilarWeb.
 Video games have been gaining while sports have lost out
 With all major-league games called off, there hasn’t been much sports to consume beyond marble racing and an occasional Belarusian soccer match. Use of ESPN’s website has fallen sharply since late January, according to SimilarWeb.
 At the same time, several video game sites have had surges in traffic, as have sites that let you watch other people play. Twitch, the leading site for streaming game play, has had traffic shoot up 20 percent.
 TikTok, the mobile app filled with short clips of pranks and lip-syncing, was taking off before the coronavirus outbreak and it has continued its steady ascent ever since. It can be nice to see that at least some things remain unchanged by the crisis.
 Students, read the entire article, then tell us:
 How has the coronavirus changed how you use the internet? How has it affected how you learn, play and connect online?
 How much time are you now spending each day and week online? Is that a big change from before the virus? Which online activities do you spend the most time engaging in? What sites and apps are you visiting and using most?
 Which takeaways and trends described in the article resonate with your own experiences? Do any of the graphs match your experience of the internet since the pandemic? What other takeaways have you observed in your online life and that of others?
 The article says, “Now that we are spending our days at home, with computers close at hand, Americans appear to be remembering how unpleasant it can be to squint at those little phone screens.” Does that match your own experience? Are you using your phone less? Are you messaging and texting less and using online video chats more?
 What has been the overall effect — socially, emotionally, intellectually, physically — of spending more time on the internet? For you or for others you know? On the whole, are you content with the amount of time you spend online?
 Screen time was hotly debated before the coronavirus. Nearly two-thirds of parents said they worried about their teenager spending too much time in front of screens and more than half of parents put limits on their children’s screen use. Is the debate over screen time over — at least for now? Does your family have any rules about screen and online time in your home?
 https://www.nytimes.com/2020/04/09/learning/how-has-the-coronavirus-changed-how-you-use-the-internet.html
