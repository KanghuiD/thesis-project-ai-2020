BLOG: Coronavirus updates from Saturday, April 11
 Would you like to receive local news notifications on your desktop?
 Menu
 FLORIDA – ABC Action News is committed to providing Tampa Bay area residents all of the updates on the coronavirus, COVID-19, and the impact it's having on our way of life. To help you stay on top of it all, we've compiled all the updates for Saturday, April 11. For today's updates, visit www.abcactionnews.com/virus. Don't forget we are in this together!
 
 8:40 PM
 7:36 PM
 7:15 PM
 6:37 PM
 4:38 PM
 4:19 PM
 2:43 PM
 2:22 PM
 12:57 PM
 11:44 AM
 11:10 AM
 11:04 AM
 10:52 AM
 10:00 AM
 9:00 AM
 Important updates from Friday, April 10
 Important updates from Thursday, April 9
 Important updates from Wednesday, April 8
 Important updates for Tuesday, April 7
 Important updates from Monday, April 6
 → COMPLETE COVERAGE OF CORONAVIRUS
 This blog is being updated frequently. Stay with ABC Action News for the latest. 
 Report a typo
 https://www.abcactionnews.com/news/coronavirus/live-blog-latest-coronavirus-updates-for-saturday-april-11