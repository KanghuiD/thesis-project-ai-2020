                Tennis: Top players donate, lower tier counts cost of coronavirus shutdown - Reuters
 4 Min Read
 MUMBAI (Reuters) - While top players donate to coronavirus relief efforts and post cooking and workout videos online, lower level professionals are counting the cost of the tennis shutdown with some fearing they will be unable to put food on the table if it lasts much longer. 
 Novak Djokovic last week pledged one million euros ($1.11 million) to help buy medical equipment in his native Serbia,  while Rafa Nadal has called on fellow Spanish athletes to help raise 11 million euros to fight the pandemic. 
 Roger Federer - the other member of the Big Three of men’s tennis - contributed one million Swiss Francs ($1.05 million) to help vulnerable families in his native Switzerland. 
 By contrast, Georgia’s Sofia Shapatava, the world’s 375th ranked women’s singles player, has started a petition seeking assistance from the International Tennis Federation (ITF) for lower-level professionals. 
 “Not many will be able to support their everyday life and then come back to playing after three months without competition,” the 31-year-old said in her online petition, which has more than 1,350 signatures.  
 The men’s ATP Tour and the WTA, which runs the women’s circuit, suspended all tournaments until early June after countries started locking down their borders to contain the spread of the flu-like virus. 
 In recent weeks, players who would usually be jetting around the world to tournaments have flooded social media with videos of their take on the toilet roll challenge, or their new home workout routines, cooking and dancing skills. 
 While tennis is a lucrative sport for those at the top — the 2020 Australian Open singles champions each took home A$4.12 million ($2.52 million) — those in the lower echelons often struggle to make ends meet. 
 A 2018 International Review Panel report commissioned to address betting and integrity issues said that players in the lowest tiers were susceptible to corruption because of the difficulty in making a living. 
 Only 250-350 players, the report said, earned enough to break even. 
 Tennis governing bodies have over the last few years attempted to improve the pay and conditions for a deeper pool of international players but it has not proved enough for those who depend solely on winnings. 
 Britain’s Tara Moore, who is ranked 447th in singles, earned about $2,800 this year before the shutdown. 
 Shapatava, who currently plays secondary ITF events, has collected $354,725 in career earnings since turning professional in 2004, but has only made about $3,300 this year. 
 “It is very weird to listen that professional tennis players struggle with food, but that is reality,” she wrote in a blog. 
 “Half of the people I know work on (the) side, coach or play club matches, and since the world is on shutdown that is impossible to do. 
 “I will not say that I am in the worst position on the planet, honestly I am healthy, I will have food on the table and my family is doing good, but I know people who feel far worse than me.” 
 India’s Prajnesh Gunneswaran said a number of players have been dipping into their savings or have had to depend on family to get by during the shutdown, which he hopes will not run as long as six months.  
 But the ATP Tour’s 132nd ranked singles player felt that even in the present climate tennis players were still in a better place than many others. 
 “Being laid off in a regular job means having to look for work. In our profession we don’t have that issue,” he told Reuters.  
 “So in that sense, we are better off than others. So it really comes down to how long this period is for ... usually outside 250 players barely manage to break even.” 
 Editing by Peter Rutherford
 All quotes delayed a minimum of 15 minutes. See here for a complete list of exchanges and delays.
 https://www.reuters.com/article/us-health-coronavirus-tennis/tennis-top-players-donate-lower-tier-counts-cost-of-coronavirus-shutdown-idUSKBN21J44Y
