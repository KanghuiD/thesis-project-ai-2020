THINK: Fresh Opinions, Sharp Analyses and Powerful Essays | NBC News
 Many owners faithfully paid for business interruption policies for emergencies like these. Now we're being told this situation just doesn't apply.
 Chef Thomas Keller
 The man who wrote "An ounce of prevention is worth a pound of cure" would have understood why the current state of the nation is necessary.
 Daryl Austin
 As China seeks to keep dissent quiet at home, it is also seeking to turn itself from an agent of obfuscation into a global goodwill ambassador.
 Gayle Tzemach Lemmon
 Though most viewers will watch for the look at the feminist movement, it's impossible to take your eyes off the "villain," Phyllis Schlafly.
 Jenni Miller 
 It's important for us to maintain our sense of humor during this frightening crisis — just as long as we don't overdo it.
 Bryan Reesman
 As a pandemic threatens the globe, pundits, medical professionals and policy experts share their analysis and advice.
 Let us know what you THINK by submitting a letter to the editor. 
 THINK is NBC News' home for fresh opinion, sharp analysis and powerful essays.
 Anne Case and Angus Deaton
 Ani Bundel
 Julio Ricardo Varela 
 James Carville
 The Election Day countdown is on. This analysis showcases the competing, contradictory narratives of the primary campaign.
 Nicolas Talbott
 Rev. Dr. Amy Butler 
 Jeff McCausland
 Ashley Pratte
 Steve Majors
 David Mark
 Andrew Paul
 Melissa Kimble 
 Why Is This Happening?
 Jessica Levinson 
 Michael Arceneaux
 Kurt Bardella
 Sara Luterman
 Dean Obeidallah
 DJ Spinderella
 Navin Nayak
 Megan Carpentier
 Danielle Campoamor 
 Maggie Mulqueen
 Jason Marczak and Diego Area
 Lauren Cox
 Joy Engel
 Susan Del Percio 
 Esther Choo and Vincent Rajkumar
 Michael Ellison
 Nancy Hightower
 Charlie Sykes 
 Tracy Law
 https://www.nbcnews.com/think