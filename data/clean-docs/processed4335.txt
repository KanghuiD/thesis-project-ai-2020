Transgender legal group releases coronavirus guide
 The Transgender Legal Defense and Education Fund has prepared a free online guide, available in both English and Spanish, for transgender and non-binary individuals attempting to navigate life during the coronavirus pandemic.
 “A Know Your Rights
 Guide for Transgender People Navigating COVID-19,” which contains information
 that was accurate as of March 22, explores both trans-specific concerns and
 equity concerns that disproportionately impact trans and non-binary
 individuals, such as issues involving employment, housing and health care. It
 also features a national appendix of health and advocacy resources.
 The guide stresses the existence of a pandemic does not void the
 fact that trans and non-binary individuals are entitled to receive care and
 providers must address individuals by the names and pronouns that they use and
 provide individuals access to facilities consistent with their identities.
 The guide encourages trans and non-binary individuals to
 self-advocate by knowing their rights and, if feeling ill, contacting a medical
 provider or a state health department hotline.
 According to the guide, the “extraordinary public health crisis”
 may lead trans and non-binary individuals to experience delays in receiving
 classified as “non-emergency surgeries” and business that courts and government
 agencies has deemed “non-essential,” which may or may not entail changing one’s
 legal name and gender marker.
 If a trans or non-binary individual feels that they are being
 subject to discrimination; the guide advises that the individual report each
 incident to the appropriate staff, take notes, obtain names, save copies of any
 documents and file a complaint.
 TLDEF describes itself on its website as “a
 nonprofit whose mission is to end discrimination and achieve equality for
 transgender people, particularly those in our most vulnerable communities.”
 As of Saturday, there were more than 120,000 confirmed cases of
 coronavirus in the U.S., with more than 2,000 deaths. Globally, there were more
 than 650,000 confirmed cases and more than 30,000 deaths.
 coronavirusTransgender Legal Defense and Education Fund
 https://www.washingtonblade.com/2020/03/29/transgender-legal-group-releases-coronavirus-guide/