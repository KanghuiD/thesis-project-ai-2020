Pay cuts a popular COVID-19 response among public companies | HR Dive
 A number of executives in industries particularly affected by public-health responses to COVID-19 have announced pay cuts in the past two months. Pay reductions for executives have happened in several of the industries most impacted by the coronavirus outbreak, according to Yahoo Finance, including in the airline, transportation and hospitality industries.
 Gallagher noted in its analysis that companies are taking alternative, non-layoff approaches in responding to the pandemic. These include pledges not to lay off staff, suspending planned layoffs, not counting paid time off or sick days, and offering funds to offset employees' costs. Walmart, for example, said it will waive its attendance policy through the end of April.
 Hiring freezes have emerged as another common cost-control measure: 4 in 10 companies in a Willis Towers Watson report last month said they had either implemented a freeze or reduced hiring, and an additional 28% said they would or might do the same.
 Separately from pay cuts, CEO turnover has trended downward during the pandemic, according to outplacement firm Challenger, Gray & Christmas. This is notable, the firm said earlier this month, because of a high turnover trend that persisted from the end of 2019 into the early months of 2020.
 Outside of the COVID-19 context, pay cuts have emerged in recent years as an approach for a number of issues. EasyJet, the U.K.-based airline, announced a voluntary reduction of its CEO's salary in early 2018 as a measure taken toward closing the gender pay gap at the company. And in a May 2019 opinion piece for The Guardian, CareCentrix CEO John Discroll described how CareCentrix froze the salaries of its senior leadership while raising its minimum base pay to the equivalent of $15 an hour.
 "We needed our executive team to buy into a vision of business success where every employee had a fair shot at success," Driscoll wrote. "It worked. Our business has tripled over the past five years. Our minimum wage is now approaching $16.50 per hour and last year we broadened profit sharing to all levels of the company."
 Follow
                                     
                                         Ryan Golden
                                     on
                                         
 Topics covered:  HR management, compensation & benefits, development, HR tech, recruiting and much more.
 The free newsletter covering the top industry headlines
 https://www.hrdive.com/news/pay-cuts-a-popular-covid-19-response-among-public-companies/576583/
