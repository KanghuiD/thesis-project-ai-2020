Maria Smith: Could coronavirus save us from extinction? | Opinion | Architects Journal
 For a better experience please update your browser to its latest version.
 28 April, 2020 8:29 am
 28 April, 2020 7:47 am
 28 April, 2020 6:57 am
 28 April, 2020 6:40 am
 More news
 24 April, 2020 7:57 am
 23 April, 2020 6:00 am
 22 April, 2020 8:31 am
 21 April, 2020 8:19 am
 28 April, 2020 8:47 am
 27 April, 2020 3:31 pm
 27 April, 2020 2:41 pm
 27 April, 2020 12:14 pm
 more competitions
 28 April, 2020 7:51 am
 24 April, 2020 7:23 am
 22 April, 2020 7:48 am
 21 April, 2020 8:03 am
 more opinion
 Bill’s passing makes the world of architecture seem a lesser place, says Paul Finch
 More opinion
 Architecture poet LionHeart delivers a highly-individual and humorous address at the AJ100 breakfast event in London
 more aj100
 Our heritage building studies in this issue are Walmer Castle visitor centre by Adam Richards Architects, and Lincoln’s Inn Great Hall and Library by MICA Architects. PLUS Brick By Brick ramps up Croydon’s infill housing programme; our second coronavirus survey paints a grim picture of an industry grinding to a standstill; and we reveal the latest supporters of our RetroFirst campaign.
 previous issues
 Choose your AJ subscription package
 Joint packages:
 1-9 subscriptions
  10+ Subscriptions 
 3 April, 2020
 By Maria Smith
 By showing us that major changes in behaviour are possible, the pandemic might help point the way to saving the planet says Maria Smith
 This pandemic is not nature fighting back; this is not a war against the virus or against nature. This is us – part of nature – feeling the terrible effects of ravaging our own life support systems.
 Demand for resources – from limestone for cement to lithium for smartphones – incentivises the invasion of tropical forests and wild landscapes for mining, logging, road-building, intensive farming, and other destructive land use shifts. This degrades landscapes and disrupts ecosystems. The species that survive are crowded together, enabling pathogens to jump from species to species, including humans. Our globalised society distributes infectious diseases very effectively and high pollution levels make it difficult to recover.
 Perhaps it’s a terrible thing to say, but it could be Covid-19 that saves us from extinction. We’ve already learnt that previously unconscionable behaviour change is possible and is effective. Nitrogen dioxide and particulate levels have dropped by almost half in London, Birmingham, and Bristol. Studies are even suggesting that the number of deaths caused by coronavirus could be outnumbered by the early deaths prevented through having cleaner air.
 Studies suggest the number of deaths caused by coronavirus could be outnumbered by early deaths prevented through having cleaner air 
 The reduction in driving, flying and industry has slashed carbon dioxide concentrations in the atmosphere as did the 2008 financial crash, the recession in the early 80s and even the black death. This correlation between economic activity and environmental indicators is well documented and while we may not like it, we can’t keep ignoring it.
 We must break the cycle of disaster capitalism, whose forces will prioritise short-term economic recovery and a return to ‘normal’ over tackling our systemic reluctance to live within the planet’s carrying capacity. To prepare for this, let’s frame the lockdown behaviours we’ve managed to quickly adopt, not as temporary inconveniences but as proofs of concept for more sustainable ways of living.
 Can the long-anticipated move to remote working allow us to address the housing affordability crisis? More than 70 per cent of the annual housebuilding target could be met with the 216,000 empty (empty, not ‘second’) homes in England. Being mostly concentrated in the Midlands, the North, and far west as opposed to affluent London and the South East, they’re considered in the ‘wrong’ place. What if we could respect the hard-won embodied energy of existing buildings in all their beautiful locations instead of throwing money at the richest regions in the hopes that they’ll make us the biggest returns? What if we could follow the resources, instead of the money?
 If we recognise that this crisis is part of the wider ecological emergency, then we will emerge from it less afraid of inconvenience
 The surge of mutual aid groups reveals that we have the resourcefulness and co-operation to prioritise collective action over every-man-for-himself consumerism. What if neighbourhood planning was based around a web of local networks proven to distribute skills and scarce resources? Couldn’t this also make us happier as we’re rewarded for helping our neighbours with a lovely shot of endorphins!
 We were expecting floods and fires, not pestilence. The error of the ‘climate change’ branding rears its head again. It isn’t just ‘change’, it’s a looming catastrophe. It isn’t just ‘climate’, it’s our entire planetary health. But if we recognise that this crisis is part of the wider ecological emergency, and recognise the long-term value in the changes we’re making, then when we emerge, blinking into the pubs and public spaces, we will emerge less afraid of inconvenience and change, better able to empathise with those with fewer safety nets, and better able help each other through difficult transitions.
 The future was never going to be graphene and fibreglass and AI chips in our retinas. The new normal will be an endorphin-fuelled vision of the good life. The new normal will be a careful reconfiguring of activity within fabric of the old. Who will help us realise this new normal? Does anyone know any architects?
 Maria Smith is an architect and engineer
 24 March 2020Will Hurst
 Architects are failing to appreciate the enormity of Covid-19 and should use their problem-solving abilities to help fight the global pandemic, a prominent public health expert has told the AJ
 15 April 2020Kate Youde
 As architects adopt new ways of working through the lockdown, Kate Youde looks at how small practices are managing to keep designing and delivering their projects
 22 April 2020Richard Waite
 The AJ’s second coronavirus survey shows the abrupt fall-off in work is biting hard. One in 10 of the anonymous respondents thinks their practice might not survive. Richard Waite reports 
 24 March 2020Richard Waite
 While the results of the AJ’s questionnaire show a widespread shift to home-working, there are longer-term worries about a significant business slowdown. Richard Waite reports
 A N Archist31 March, 2020 8:00 pm
 Let us hope that we are able to hymn a paean if we are able to get through this, and that the Virus PM is not just mouthing more empty words when he admits that there is such a thing as society.
 In truth, it was Churchill who had a great deal to be modest about, as it was Clement Attlee who ably managed the domestic front throughout the conflict, and went on to found the welfare state and NHS. The latter being the only reason for our survival so far.
 After a decade of Tory misrule and unnecessary, ideological austerity, the miracle of the postwar Labour government must be viewed with fresh eyes and used as a touchstone by the new leader. There is no such thing as the individual, powerless and alone, but only the concerted and collective effort of the huddled masses towards a better world. Rise up, you have nothing to lose but your chains!
                     Unsuitable or offensive?
 Chris Medland1 April, 2020 9:37 am
 agree 100% with Maria Smith. I hope that this crisis will lead to many positive changes socially, environmentally and economically. The reset button has been pressed, all change is possible.
 £35,000 - £45,000 p.a.
 40000-45000 per annum
 £32000 - £42000 per annum + Generous benefits + bonus
 £38,000 - £42,000 p.a.
 Discover architecture career opportunities. Search and apply online for your dream job.  
 Find out more
   
 The AJ supports the architecture industry on a  daily basis with in-depth news analysis, insight into issues  that are affecting the industry, comprehensive building studies with technical  details and drawings, client profiles, competition updates as well as letting  you know who’s won what and why.
 Site powered by Webvision
 https://www.architectsjournal.co.uk/opinion/could-coronavirus-save-us-from-extinction/10046750.article
