John Cassidy | The New Yorker
 John Cassidy has been a staff writer at The New Yorker since 1995. In
 2012, he began writing a daily column about politics and economics on
 newyorker.com. He has covered two Presidential elections, and has
 written extensively about the Trump Administration. He is also a regular
 contributor to The New Yorker’s political podcast, “Politics and
 More.” He has written many articles for the magazine, on topics ranging
 from Alan Greenspan and Ben Bernanke to the intelligence failures before
 the Iraqi War and the economics of John Maynard Keynes. He is the author
 of two books: “How Markets Fail: The Logic of Economic
 Calamities” and
 “Dot.Con: How America Lost Its Mind and Money in the Internet Era.” Before joining
 The New Yorker, he worked for the Sunday Times of London and the
 Post. He graduated from Oxford University in 1984 and from the
 Columbia School of Journalism in 1986. He grew up in Leeds, West
 Yorkshire.
 By John Cassidy
 https://www.newyorker.com/contributors/john-cassidy
