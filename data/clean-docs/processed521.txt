Coronavirus updates: Third Sector live blog | Third Sector
         23 April 2020
     
 Keep up to date with all the latest coronavirus-related news affecting the charity sector. Please refresh for the latest updates
 The charity says the coronavirus crisis has left people with pancreatic cancer isolated from both family and treatment.
 It adds that people with the disease need its specialist nurses more than ever for support and to help ease the burden on its colleagues in the NHS.
 City Lit, the London-based charity, has launched a range of online courses designed for those who want to try something new or revisit a past passion while in lockdown.
 Courses available online range from how to build a website in one day, Italian for beginners, dance lessons and an introduction to British Sign Language.
 In the spirit of the 2.6 challenge, London's Air Ambulance Charity is challenging supporters to do 26 moves of the "helicopter" exercise. The charity says it's tough, "but we know the public are up for it!"
 And staff at the RSPCA Felledge Equine Centre in Durham are challenging animal lovers in north-east England to take part in the 2.6 challenge around something animal-themed. 
 All supporters need to do is pick a challenge and make sure it's something you can do safely while conforming to social distancing rules. Set up a fundraising page on the RSPCA 2.6 Challenge JustGiving page or make a donation. Then spread the news and get as many people involved as you can.
 Its Business Response Forum webinars cover a range of topics, including health and wellbeing during the lockdown, ethnicity and Covid-19, and risk.
 The international relief and development organisation Human Appeal is calling on Muslims to share their Ramadan experiences in lockdown.
 Further afield, the charity says, it is focusing its efforts where people are particularly at risk because of coronavirus, from providing water disinfectant to nearly 300 wells in Gaza to helping to provide for 3,000 people in Pakistan who can no longer work to support their families.
 The online auction will run from 15 to 29 April and can be found here.
 A report from the non-profit Anti-Tribalism Movement says the community, numbering between 350,000 and 500,000, suffers from "significant inequalities in accessing education, employment, health and housing with resultingpoor outcomes", including health outcomes.
 There is also a high prevalence of hypertension, respiratory disease,diabetes and obesity, all risk factors for susceptibility to Covid-19, the report says.
 It offers a number of recommendations to the health authorities, the Greater London Authority, the police, the Department for Education and local authorities.
 The website, run by Royal Voluntary Service, says: "If you are currently not supported and need some help with shopping, a prescription collection or a friendly chat then our NHS Volunteer Responders are ready to help."
 It says only people in the following groups should get in touch:
 People aged 70 years and older with underlying health conditions
 If you are in the "extremely vulnerable" to Covid-19 group and have been sent a letter asking you to shield from the virus
 People who are pregnant 
 If you are newly socially vulnerable as a result of Covid-19
 People who are registered disabled
 Others with high-risk conditions could include: people with chronic lung disease or moderate to severe asthma; people with serious heart conditions; people who are immunocompromised including because of cancer treatment; people of any age with severe obesity, for example a body mass index over 40; certain underlying medical conditions, particularly if not well controlled, such as those with diabetes, dementia, renal failure, or liver disease might also be at risk.
 The step-by-step guide on how to make claim through the scheme, including essential information needed before you start, how to work out your claim and what happens after submitting your claim, will happen three times on 27 April: 10:45, 12:45 and 15:45.
 On its website, the organisation gives six tips for looking after your mental health during the pandemic. The are:
 Plan your day
 Move more every day
 Try a relaxation technique
 Connect with others
 Take time to reflect and practise self-compassion
 Improve your sleep
 Two wellbeing rooms have been funded by £75,000 from the Nottingham NHS Heroes appeal for City Hospital and Queen's Medical Centre NHS hospital staff. 
 The two rooms have been used by more than 2,000 staff members, reports Nottinghamshire Live, allowing them to have a safe space to take a break from the ward environment.
 The evening's events include Stay Home Lounge with BBC Radio 1, the #keepdancingchallenge, the banana bread challenge and the 2.6 challenge (in which our very own Rebecca Cooney will be taking part: donate donate here). On Friday 24 April The Big Quiz will take place in the evening.
 The Young People's Resources section gives advice and tips on how young people can look after their mental health and physical wellbeing.
 A paper published by Become, the charity for children in care and young care-leavers, is based on a nationwide survey carried out during the first week of the lockdown.
 It found that students were most worried about loss of income: respondents reported being unable to find new paid employment or access to universal credit. Many were also worried about loneliness and mental health, and said they wanted clarification about their accommodation arrangements.
 In a blog on its website, Bates Wells looks at different types of charity entity and how they can work out if they have the power to borrow.
 These include charitable companies limited by guarantee, charitable incorporated organisations, unincorporated trusts, unincorporated associations, community benefit societies and royal charter bodies.
 Hosted by the creative agency Raw London, the webinar, which begins at 4pm, will include comms specialists from the British Red Cross, the Alzheimer's Society, Children with Cancer UK and Marie Curie, as well as the digital consultant Kirsty Marrins.
 The Charity Finance Group is holding a free webinar today at 1pm to advise charities on how to navigate the financial situation caused by Covid-19.
 Join our free webinar at 1pm where we will discuss how you can best mitigate the impact of coronavirus on your charity's finances & answer your Qs, with experts @naziarhashemi and @peshframjee Register at https://t.co/6Os0mLfpfZ
 According to the Hereford Times, Western Power Distribution has launched its In This Together – Community Matters Fund to help local organisations that might need financial help.
 The Julia and Hans Rausing Trust, an independent grant-making charitable fund, is allocating £15.3m to charities over the next six months.
 Donations are going to a variety of groups, including:
 A donation of £5m has been given to the NHS Charities Together Covid-19 appeal.
 The insurance company's Covid-19 Community Grant Scheme gives local charities within a 20-mile radius of Conquest House (Hastings Direct’s head office) the opportunity to apply for a grant, reports the Hastings & St Leonards Observer.
 It writes that the scheme aims to help the most vulnerable in the community by means of food hamper provisions and through a community grant fund for local charities that colleagues would like the company to support.
 The culture secretary yesterday passed this verdict by webcam before the House of Commons Digital, Culture, Media & Sport Committee.
 Vicky Browning, chief executive of Acevo, has said that the £750m package is not enough. She's not alone: industry figures are expressing disappointment at the lack of support. Follow the conversation on the hashtag #NeverMoreNeeded.
 The £750m funding package from Treasury to support our sector is not enough to enable charities to respond to this crisis now and in the future. Charities are #NeverMoreNeeded https://t.co/r4cbFflMJW
 Of course, the government can't literally intervene to prevent any charities at all going bust during the immediate crisis or its aftermath. What it can do is make sure that the support it provides for charities and #socent is of a similar level of relevance to the support ...
 2 yrs ago I was in the room when, then DCMS SoS @MattHancock said civil society is 'the glue that holds society together' Yesterday current SoS @OliverDowden said 'we can't save every charity' when asked if he accepted that hundreds of charities could go under #NeverMoreNeeded
 The Courier reports that a number of charities, led by Anchor House and including Lighthouse for Perth, Andy's Man Club and Women's Wellbeing, are making the move because of concerns about the long-term impact of the lockdown on vulnerable people.
 It reports that the charities will share a unit in the city centre, which will be managed 24 hours a day, seven days a week. The new location, it writes, will have purpose-built rooms for those who need to stay, as well as showering facilities, sensory rooms and staff and volunteers providing support and running telephone helplines.
 The Lighthouse for Perth was granted £10,000 by the National Lottery on Monday to help it deal with issues relating to specifically coronavirus.
 The business support organisation says the GiveBradford Resilience Fund has been launched to help groups suffering difficulty during the lockdown by offering urgent financial support to cover core operating costs, volunteer expenses, staff costs and transport.
 GiveBradford was set up by Leeds Community Foundation to enable local communities across the district to get the support they need. As part of the national network oUK Community Foundations, it invests in these groups by distributing grants and sharing advice.
  The charity, which uses kindness to change the life of someone every week of the year, has seen a significant increase in donations during the lockdown, enabling it to help more people across the UK.
 It is sending "Happy Bags" filled with games and toys to 100 children in Wales. It says these children live in difficult circumstances, which have worsened since the lockdown was introduced. Many are living with parents who have serious physical or mental health issues and they often have to take on many extra responsibilities.
 It is also helping to send wellbeing packs to almost 400 older people in Berkshire who are in isolation, living on their own with no family support network or internet access. The wellbeing packs include essential items, as well as a regular newsletter to help them stay connected and keep their spirits up.
 The charity will also be helping to supply emergency food parcels to 100 vulnerable families in Leeds.
 The NET says a total of £35m has been pledged to the Coronavirus Appeal with a further £20m in match funding promised from the government. Further distributions will be made in the coming days, to continue to support the most vulnerable in society, it says.
 The money will be distributed through UK Community Foundations to 47 local community foundations across the country.
 The Children's Book Project and AT The Bus have joined forces to put together a lockdown bundle of books and creative resources to be gifted to 10,000 young people aged between three and 11 in London and Oxford at the beginning of what would have been the summer term.
 Their Story & Draw pack has been designed to inspire enjoyment, creativity, escapism and emotional wellbeing among children and will include three age-relevant books as well as paper, pencils and other resources.
 The Story & Draw packs are being distributed through schools in 12 London boroughs (Barking & Dagenham, Brent, Ealing, Enfield, Hackney, Hammersmith & Fulham, Hillingdon, Newham, Kensington & Chelsea, Southwark, Tower Hamlets and Westminster) and to a number of schools in Oxfordshire. Books have been donated by Penguin, Little Tiger and by book-buying families across the capital. Art supplies have been donated by Premier Paper, GF Smith and Fedrigoni. Each bundle will include a unique bookmark designed by the artist and illustrator Jane Ray.
 The international development charity says it is anticipating a fall in unrestricted income of £6m in the current financial year and is taking the step to focus on supporting vulnerable communities. The charity aims to save £1m in payroll over the next nine weeks and will be fully drawing down on its available reserves.
 It will implement the government's job-retention scheme from 27 April until 30 June. Christian Aid says it expects that 20 per cent of UK-based staff will be furloughed. All non-furloughed UK staff have been asked to move to a four-day week with 80 per cent of salary for 12 weeks.
 Directors and most senior managers will not be furloughed, but members of the leadership group have been asked to move to 80 per cent pay for the three months of May through to July. They will continue to work a full working week.
 Three local authorities say they will not penalise charities for flytipping if donations are left outside their shop doors while they're closed during lockdown.
 The Southend-on-Sea, Eastbourne and Swindon borough councils, which have all been quoted in local media warning people not to leave items outside charity shops while they are closed during the pandemic, have sought to reassure charities that they will not be fined.
 Read the full story here
 Oliver Dowden tells culture select committee: “The Chancellor has been honest, and I’ve echoed that, in that we can’t save every business, and that would include not being able to save every single charity."
 Dowden pointedd out that, in addition to the £750m package, charities could make use of other government initiatives such as the Coronavirus Job Retention Scheme.
 The charity for older people says: "Social distancing, self-isolation and shielding are aimed at reducing close contact with others. However, there are some important differences. Here's what they might mean for you."
 The guidance comes in light of what the regulator called in a statement “the extraordinary fundraising efforts and generosity displayed by the British public” during the coronavirus crisis.
 It had been designed to support the public to ensure their appeals are legal, ethical and as effective as possible, the statement said.
 The webinar, Coronavirus and Your Charity's Finances – Update, will provide an overview of the latest government announcements, including the support package announced by the Chancellor. Attendees will be able to ask questions of an expert panel of charity finance experts. These should be sent in in advance to policy@cfg.org.uk.
 The guest speakers are Pesh Framjee, partner, global head of non-profits team at Crowe, and Naziar Hashemi, partner in the same team at Crowe. The webinar starts at 1pm.
 The Museums Association is calling for museums across the UK to participate in the BBC Arts' Culture in Quarantine event, a whole day of content on social media, TV and radio focusing on the UK’s museums.
 The BBC, in partnership with the MA, Art Fund and the National Museum Directors' Council, will host and highlight social media content from museums, galleries and archives on the BBC Arts website and across other BBC programmes and accounts.
 Others are doing their running in places such as Teesside, Cardiff, Devon, Surrey and Hertfordshire, some using treadmills.
 The charity that feeds hungry children around the world has had to cancel most of its fundraising activities, just like other charities, but has devoted a page of its website to some alternative ways to raise money.
 These include sending gift cards, organising virtual quizzes, creating and selling crafts, hosting virtual birthday parties and donating unwanted items.
 The report, Coronavirus and Students Survey, covers wellbeing and welfare, self-isolation, support, education, finance, consumption, accomodation, government actions and the future.
 Among the findings, the NUS reports that 62 per cent of students are somewhat or very scared of contracting the virus and almost 93 per cent are practising social distancing, but 40 per cent are not at all or only somewhat aware of guidance on living in shared accommodation.
 The umbrella body says: "We are working with officials to design support measures to help the sector, but we need your help and we will be taking the key points raised in the survey to government. We need to know how social enterprises may be impacted by Covid-19 to ensure that we give the government the right information to support our sector."
 It says that examples include:
 Social enterprises suffering from a slowdown in trading conditions;
 Social enterprises that employ vulnerable or high-risk groups and could therefore be unduly affected by the spread of the virus;
 Social enterprises with low or no financial reserves because of the sector they trade within;
 Social enterprises that are at risk of their trading being suspended should the pandemic worsen.
 The Essex Community Foundation, a charitable trust, has secured more than £1.1m for its Essex Coronavirus Response and Recovery Programme to support local charities and voluntary organisations.
 And it says that since the programme was launched on 25 March nearly £530,000 has been awarded in grants.
 Pancreatic Cancer UK says the survey is for people with pancreatic cancer or their family members or friends who are concerned about the impacts of coronavirus on their treatment or care.
 The charity says that anyone with questions or worries about coronavirus and how this might affect them and their treatment should speak to its specialist nurses on its free support line, and look for more information on its website.
 Take the survey here.
 Overgate Hospice in Calderdale, working with the local Clinical Commissioning Group, launched a new telephone support and advice service on Monday.
 It says the line, called Hear For You, provides support to relatives of patients being cared for in any health or social care facility in relation to Covid-19, all relatives of end-of-life patients, bereaved relatives who can only attend funerals at the crematorium or burial in restricted numbers, and any health professional providing a front-line service in response to the pandemic.
 It says its phone lines, on 01422 378172, are open seven days a week from 10am until 6pm.
 According to BBC News, Esther Windass from York climbed 2,277ft (694m), the equivalent of ascending the Yorkshire fell Pen-y-ghent, by going up and down a stepladder in her garden.
 Confined to home by the coronavirus lockdown, she climbed up 456 times on Monday and raised more than £400 for the charity. BBC News says Pen-y-ghent is the lowest of the Three Peaks in the Yorkshire Dales.
 Esther's dad Steve Windass told the BBC that Esther chose to raise money for Martin House because one of her school friends goes there.
 The East Anglian Daily Times reports that Serena Grant, from Sudbury, has released a song called Stay Home, working with musicians who all contributed from home. Grant says every penny from sales of the song will go to the charity.
 It reports that the idea for the single was formed after Grant lost her back catalogue of music and songs. She said: “I became very very down for a while but got my keyboard out one evening and started playing about with some chords and lyrics."
 Watch the video below.
 According to the Stroud News & Journal, the charity has had to close all 10 of its shops in Gloucestershire and cancel all spring and summer events, which it says is losing it £30,000 each month.
 It reports that the charity is now unable to rehome animals and intakes are limited to urgent RSPCA welfare cases and high-priority animals.
 The Swindon Advertiser reports that people in the area who are spring cleaning during the lockdown have been advised to hang on to items they want to get rid of and not leave them outside closed charity shops.
 A spokesman for the council told the newspaper: “If charity bags are left outside shops we will have no choice but to treat it as fly-tipping, which is a far from ideal situation when our resources are already stretched trying to keep residents’ waste collections going in the current coronavirus crisis.”
 The taskforce brings key leaders from the housing and employment sectors together to work with government and policymakers to ensure that the sectors and the people and communities they serve are best supported during the pandemic.
 A letter to Rishi Sunak, the Chancellor of the Exchequer, says the government’s Coronavirus Job Retention Scheme brings welcome relief for many employers but does not meet the needs of the voluntary sector.
 Furloughed employees are able to volunteer, but not for their own organisations.
 The umbrella body said it had asked staff to volunteer to be furloughed and it planned to introduce a schedule by which people from various teams were off work on a rotating basis. 
 It said the largest proportion of furloughed employees would be in its conference and facilities teams.
 The organisation has been supporting people with sight loss for more than 145 years and has a team of 28 carers, plus other key workers, who are supporting some of the most vulnerable people in the region.
 Today's Guardian reports that the charity the charity is proposing a “a day of loss or a day of grieving”, possibly on 23 March, the anniversary of the day Britain went into lockdown.
 The newspaper says Marie Curie wants the day to become an annual event to remember the tens of thousands of people who are likely to have died from coronavirus, but also those who died during the epidemic and whose loved ones were prevented from gathering together to express their grief.
 It quotes chief executive Matthew Reed: “In normal times we know that the ability to attend a funeral, a rite of passage for thousands of years in human civilisation, is an important part of the grieving process. It closes the book on a person’s life and is the chance to open a new chapter for the people that are left. Because people aren’t able to properly express their grief it is creating a tsunami of loss. The consequences of not handling it well can last for decades.”
 The BHF has put 80 per cent of its workforce on furlough, while Scope has done the same with another 23 members of staff, so that 67 per cent of its employees are now on furlough.
 According to an open letter on the Unicef website, international donors have pledged about a quarter of the $2bn the UN requested in the Global Humanitarian Response Plan for COVID-19 in March.
 It says: "The Central Emergency Response Fund has also released $95m to kick-start the Covid-19 response, help contain the spread of the virus, maintain supply chains and provide assistance and protection to the most vulnerable people, including women and girls, refugees and internally displaced persons. But more needs to be done.
 Read the open letter here.
 The Bucks Free Press reports that Mike Peters, 63, will ride 113 miles, the distance from his home to the village of River in Kent, by completing 200 laps round the block where he lives.
 The newspaper says he is raising money for Baby Basics Dover & Deal, which was started four years ago by his daugther Hannah. Two of his grandsons live with Hannah, but he cannot see them because of the lockdown. It adds that he hasn't been on a bike for more than 15 years and will be using the one owned by his eldest grandson, Toby, who is five.
 The steering group of the Global Alliance for the Rights of Older People has said today that states around the world must look after "the rights of older persons on an equal basis with others without discrimination and without exception and in line with international standards".
 Garop says that older people already face human rights challenges around the world because of age discrimination and ageism, and they are at higher risk than others of serious repercussions from contracting Covid-19.
 It says: "Efforts to address Covid-19 may exacerbate ongoing threats to the rights of older persons, for example isolation and social exclusion, and the challenges of accessing information, health and social care, and social protection. Stay-at-home and no-visitor policies to contain the spread of the virus are putting some older persons at greater risk of violence, abuse and neglect. Other older persons may be affected disproportionately due to their care and support needs, or underlying health conditions. We are concerned that this pandemic has the potential to exacerbate the entrenched inequalities and disadvantages that older persons experience daily."
 Garop warns that it is essential for countries to put measures in place that ensure the human rights of older people are protected.
 Policy fellow Helen Gilburt writes that the volunteering response to the pandemic has been evolving, with the scale of the situation setting a challenge in terms of matching capacity to demand while ensuring the health and wellbeing of the volunteers.
 The event is happening both to raise funds and to attempt to get into Guinness World Records for the largest virtual tea party.
 The Spark says its free helpline will now be open from 9am to 9pm from Monday to Thursday, and from 9am to 4pm on Fridays, on 0808 802 2088.
 It says the confidential telephone service supports people living in Scotland who are struggling with relationship issues; people living outside Scotland are welcome to use the service but are charged a small fee.
 Ellwood Atfield's hub connects expert communicators, free of charge, with organisations that cannot afford their help or are struggling with their communications during the pandemic.
 It says it is offering the service to charities and other organisations involved in health, social care, education and social housing, having heard of communicators in critical organisations who have become exhausted working around the clock and would appreciate extra help.
 Volunteers and those needing them can apply here.
 The Lancashire Telegraph writes that One Voice Blackburn will be loaning the equipment so that young people can keep in touch with peers and join in with online group activities during the lockdown.
 It says the charity, which works with nearly 400 people in Blackburn and Darwen, has transferred many of its programmes and activities onto digital platforms such as Zoom, Bluejeans and Microsoft Teams.
 According to the Oldham Times, Emmaus Mossley, which provides homes, work and individual support to people who have experienced homelessness, has had to close its Secondhand Superstore because of the pandemic.
 The charity, which supports 26 formerly homeless people, operates a large social enterprise, says the Oldham Times, selling donated and upcycled furniture, household goods, electrical items, clothes and vintage wares.
 It says the data provided will be stored securely on electronic servers and accessed by the UnLtd research team, and it might share the data with third parties with which it is collaborating on this work, such as Social Enterprise UK. It asks for consent at the end of the survey and can anonymise the data.
 According to the Daily Record, the Brothers-Thrive app from Brothers in Arms allows men and women to get anonymous help from professionals without leaving their homes.
 A sister app called Sisters in Arms is also available. The newspaper says the apps were downloaded 1,100 times in just a few weeks, an increase of 140 per cent on the same period in 2019 and the biggest spike since the charity was launched.
 Haringey Council in north London and Edible London, a community interest company, have been working together since the pandemic began to deliver food to those who most need it.
 The borough’s residents have also been donating to Haringey Giving’s Covid-19 Appeal, and more than £28,000 of the £50,000 target has now been raised. Thirteen local projects have now been awarded grants, with a total value of £58,064, with priority given to not-for-profit projects that provide emergency food and supplies to Haringey’s most vulnerable residents.
 On 8 April, Rishi Sunak, the Chancellor of the Exchequer, announced a £750m package to help charities hit financially by the pandemic.
 The move came after pressure from a number of charities and umbrella organisations, which joined forces to call for funding to be provided and named their campaign #EveryDayCounts to reflect the fact that hundreds of charities were facing closure within days if no support was provided. 
 But Third Sector understands that, almost two weeks after the funding was announced, it could still be weeks before the process for applying for the money is operational.
 Derry Now reports that Children in Crossfire, which supports a range of projects in Africa, is holding the 24-hour fast on Friday to raise funds for vulnerable people and communities in Ethiopia and Tanzania likely to be deeply affected by the Covid-19 crisis.
 Richard Moore, founder and executive director of the charity, said: “The challenges we have been facing here are beginning to take hold across Africa, including Ethiopia and Tanzania.
 “Added to the extreme poverty that already exists in those countries and a plague of locusts currently sweeping across east Africa destroying crops, it is clear that the coronavirus pandemic will have a devastating impact."
 BBC News reports that Mike Manders, 34, decided on his venture because a race he'd been entered for, the Hull 10k, had been postponed. He set the treadmill up in his Hull garden and completed the distance in a time of one hour 38 minutes wearing the five-stone suit of armour.
 Manders, who works for Hull Trains, wants to raise £1,000 for Action for Children. BBC News reports that he is a keen mediaeval re-enactor in his spare time and took up running last year when his weight reached more than 18 stone.
 The think tank says the guidance covers how specific areas will be affected and what philanthropists can do. It covers the situation, funding, how the sector works, 16 different sectors, learning and solutions.
 The Smallpeice Trust and the RAF are donating engineering-based "Think Kits" to those schools that have remained open to support vulnerable children and students of key workers during the pandemic.
 The Think Kits include all the materials and tools necessary for groups of students to work in teams and build an electrically powered glider, supporting the development of their creativity and engineering skills while having fun in the classroom.
 The RAF has purchased kits at £250 each, and the trust will match this with an additional 50. They will be donated to non-selective state schools across nine regions in England, Scotland, Wales and Northern Ireland.
 The goal is to see which group’s glider can travel the furthest over two flights made within 10 minutes of each other. Students are involved with the whole design and construction process.
 The project will last up to nine weeks. Twenty students, split into five teams of four, will work on their own powered gliders. All students that finish their gliders will be eligible for a Crest Discovery Award and will receive a personalised certificate.
 The bank has purchased a three-month subscription for each of its 110-strong workforce to help support the publication during the pandemic.
 The Big Issue, which is sold by homeless people to help them make some money, has had to abandon all street sales to comply with social distancing measures.
 Margaret Willis, chief executive of Unity Trust Bank, said: “These are unprecedented times and now more than ever we must support each other. Through these subscriptions we not only support those most in need but also help colleagues to recognise and respond to some of the more difficult social challenges we face today.”
 In a statement released to the press, chief executive Angela Salt said the organisation has had to cancel its weekly unit meetings and events at its activity centres, suffered a decline in trading and other income and had had to postpone its fundraising activity.
 This meant, she said, that it would suffer estimated losses of between £3m and £5m for 2020 in the UK. In furloughing just over half its staff it has also paused major work projects and plans.
 Full story to come
 The What Works Centre for Wellbeing and the Spirit of 2012 are looking across published and unpublished evidence to find out how volunteering affects the wellbeing of volunteers, and have commissioned the Institute for Volunteering Research from the University of East Anglia to lead the project.
 If you are involved with an organisation, group or club that works with volunteers or funds volunteering projects, they are keen to see any research studies or evaluation reports that look at the wellbeing impacts of volunteering on volunteers. They are especially keen to work out how the huge increase in volunteering because of the pandemic can be sustained.
 The Scottish Council for Voluntary Organisations says attendees will hear from four charity professionals on the changes their organisations have made to service delivery during the pandemic.
 They are: Jenny Paterson of Breakthrough Dundee; Reid Aiton of Young Scot (to be confirmed); Laura Frood of Articulate cultural hub; and Jo-Ann Walsh of the Oasis Project.
 Having had to move many of its 5k Race for Life running events until later in the year, the charity wants supporters to do any sort of exercise-related challenge at home to raise money.
 The two new elements concern the charity funding from the government and other funding opportunities for charities. The former examines how the £750m emergency package is being allocated and to which areas and says the aim is for the sector to receive the money in the coming weeks.
 The second element, which the NCVO says is not exhaustive, also recommends charities to check out Funding Central, the Directory of Social Change, Locality and Charity Excellence.
 The guidance can be found here.
 The Association of Chief Officers of Scottish Voluntary Organisations is organising the Zoom event in partnership with Animate Consulting. It is open to chairs and vice chairs of voluntary sector organisations only.
 In response to the impact of the Covid-19 pandemic on merchant seafarers, fishers and their families, the charity says grants will be awarded immediately to delivery partners providing advice and support for individual seafarers.
 The online fundraising portal says it is working with the organisers of the UK’s biggest mass-participation events on the challenge and wants charities to understand what it entails.
 Aside from explaining how the challenge works, the guide gives advice on promoting campaigns, provides free marketing materials and gives examples of how some charities have already embraced the campaign.
 You can read the guidance here.
 The Ilkley Gazette quotes Gareth Batty, chief executive of the regional arm of FareShare: “In March, we had our busiest month in 21 years. And since the beginning of March we have already distributed just over 200 tonnes of food.
 “Drop-in centre cafés, school breakfast clubs and other places would come to get support, but what we have found in the past four weeks is that those processes had to change. With social distancing rules, there is more of a demand for food parcels and hampers."
 The Gazette reports that the charity has been working with Leeds City Council to help distribute food parcels to those in the district struggling to make ends meet.
 According to the online news source Legal Cheek, the charity will soon be accepting applications from anyone who is or has been on the roll of solicitors and has accessible savings within their household of less than £2,500.
 The charity also has a Covid-19 support hub on its website.
 Biggleswade Today reports that KidsOut, which has rebranded temporarily to KidsIn and provides days out for disadvantaged children across the UK, is appealing urgently for funds.
 During the lockdown, the newspaper says, it aims to provide positive experiences for children who have to stay indoors, and is planning for days out when the social distancing measures end.
 Gavin Bedford, marketing and communications executive for KidsIn, told the newspaper: “The charity has had to cancel all its upcoming corporate networking events until September, which provided about 50 per cent of our cash income."
 According to Metro, the version of You'll Never Walk Alone, recorded with the singer Michael Ball and the NHS Voices for Care Choir, went to the top of the chart within hours of being released.
 At the time of writing, Captain Moore's much-reported fundraising challenge, which involved the 99-year-old Second World War veteran walking lengths of his garden, had raised more than £26.6m through JustGiving. He had initially hoped to raise £1,000 for NHS Charities Together.
 Last Friday Metro reported that a choir of doctors and nurses, called Choir Breathe Harmony, had released a version of Mariah Carey's Anytime You Need a Friend to raise money for charity. The choir, made up of NHS staff from Guy’s and St Thomas’ hospitals, linked up with more than 100 singers and musicians from around the world.
 The newspaper reported that all proceeds from the single would be divided between two small charities – Breathe Arts Health Research and the MyCool Foundation – which deliver arts and health projects to patients and NHS staff.
 Members of the public are still leaving donations outside closed charity shops or around already full donation banks.
 These items then become damaged by the elements and pose a risk to environmental health and fire safety and could limit emergency service access to nearby buildings.
 The magazine is sold by some of the UK’s most vulnerable people as a means of lifting themselves out of poverty, but Covid-19 has brought an end to street sales.
 The charity said the agreement with the Co-op, which has 2,600 stores, would help to fund its work throughout the pandemic and beyond.
 Join a growing community of Third Sector professionals today	
 Stay signed in
 Forgotten password?
                                     MI5, MI6 and GCHQ
                                     London, Cheltenham and Manchester
                                 
                                     Charity Commission
                                     SW1H 9AJ City of Westminster, London
                                     L20 7HS Bootle, Liverpool, North West or SW1H 9AJ City of Westminster, London
                                     Power to Change
                                     London (Greater), Bristol, Sheffield
                                     Open Society Foundations
                                     London or New York
 Search more jobs
 Sponsored webcasts, surveys and expert reports from Third Sector partners
 For charities today the challenge is that...
 The third sector has never been under such...
 This report provides an overview of the current...
 More Third Sector Insight
 In the first of a series, we investigate the risks to charities from having flawed cyber security - and why we need to up our game...
 	  Register now 
 https://www.thirdsector.co.uk/coronavirus-updates-third-sector-live-blog/article/1680734
