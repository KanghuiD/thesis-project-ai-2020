Self-isolation suppers: easy meals from the pantry, fridge and freezer | The Canberra Times | Canberra, ACT  
  Dahl with mango chutney yogurt. Picture: Ashley St George. 
 Shopping at my local supermarket on the weekend and I'm a bit shocked by what I find. While, everyone is relatively calm, I think I missed the great toilet paper rush, I'm faced with empty aisles. Not just the toilet paper aisle, but there's no pasta, no just a couple of packets of gnocchi left, some young man picks them up and gives them the once over, wondering that they are (they're delicious, with a little sage burnt butter, plenty of salt). There's no meat either and complete sections of the frozen food section are empty, glass doors frosting over because there's nothing to chill.
 While there's plenty no one can explain at the moment, one thing I can't understand is why, all of a sudden, have our shopping patterns changed and why are we all so freaked out about having to feed ourselves? Don't most of us have to feed ourselves, our families, every night of the week anyway? Sure, it's good to know Italian and Sons is doing takeaway, for example, but who's going to be outsourcing meals seven days a week?
 Not me. I'm quite happy to be cooking up some self-isolation suppers.
 Mind you, I'm not in self-isolation. I have the next couple of weeks on my own, so I kind of am isolated. I have decided to make one weekly dash to the supermarket, or my local IGA (#buylocal), the butcher. I'm not meal planning, just buying things I think I'll like that week, useful things that will last longer than one meal, a plump free-range chicken, pork, sage and apple sausages from the Lyneham butcher, bunches of bright green broccolini.
 But for the most part I'm cooking from the fridge, the pantry and the freezer and I'm loving it.
  Dietitian Georgia Houston says canned and frozen foods can still be nutritious. Picture: Ashley St George
 Georgia Houston is a dietitian running her own business GH Nutrition says this is a great way to start thinking about different approaches to cooking.
 "For specific pantry items, canned and packaged food are all great options to create a base for meals," she says.
 "These items won't go off if you don't use them over the next few weeks or months, and are often easy to prepare and very versatile. 
 "Things like cereal, crackers, pasta, rice and canned legumes or lentils can all come in handy. Other things to include could be things like long-life milk, nut butters, frozen berries, canned vegetables, tinned tuna and of course chocolate to get you through."
 Houston says it's important to make a list of food that you and your family actually use day-to-day. 
 "This means buying foods that you know you or your family will eat regardless if you will be isolated at home. Please don't buy just for the sake of buying. Buying up anything and everything you see in the store can be tempting. However, in the end it likely will become a waste and can really put others in a bind."
 She said while fresh food, particularly when eaten in season, is the best option, it's not always available or affordable.
 "In these circumstances, frozen or canned foods can be great alternatives," she says. "Frozen or canned fruit, veggies, legumes and lentils can often be equally nutritious, really convenient and a great way to reduce food wastage, if stored correctly."
  "Chinese chicken", some random noodles, soy sauce, Chinese five spice and chicken thighs. Picture: Karen Hardy 
 Most frozen fruit and vegetables are often snap frozen soon after they are picked. This means that the quality of the produce is at its optimum when being picked and frozen, and they can quite easily have a higher quality of vitamins and minerals than their fresh counterparts. 
 "However, in saying all this. How you cook your fruit and veg is way more important than if it is fresh or frozen. For example, if you boil veggies in a large amount of water for too long, this can cause a lot of the nutrients to leach out. 
 "Regardless of whether you are using fresh or frozen vegetables, aim to use as little water as possible when cooking. Steaming, microwaving or baking can be a much better option than boiling."
 She said it was also important to look at the labels when choosing canned fruit or vegetables, as often they can be added with salt or sugar. Choose canned vegetables without added salt and canned fruits in natural juice, rather than a syrup.
 She suggests maybe finding a day when you can cook some meals which will freeze well. Those days where you just can't face cooking.
 "Stews, lasagnas, pasta, stir-fry's, soups, mince or legume based dishes are all just as great re-heated," she says. 
  Ross Dobson's 3 Ways With ... Stale Bread is the cookbook for the times. Picture: Supplied
 "For these dishes, add lots of veggies or canned beans to make them more nutritious and go further. For example, I love to add grated carrot and zucchini to spaghetti bolognese, plus tinned tomatoes, chopped mushrooms and capsicum."
 She says it's important to consider diet as a contributor to overall health.
 "It can be tempting to fork out expensive money for fancy supplements right now, but you don't need to. If you are eating a healthy, varied diet you will be doing the best thing for your immune system. 
 "Eating a healthy diet will look different for everyone, but it essentially involves making sure that you are consuming a wide variety of fruits, vegetables, wholegrains, lean proteins and healthy fats every day. When in doubt, aim to eat the rainbow, as a variety of colours means a variety of nutrients. 
 "Also in these times, be kind to yourself. Too much exercise or constantly feeling stressed from work or school can lead to burn out and suppress our immune system."
 I'm being kind to myself by getting in touch with cooking again. One book I've dragged off the shelves is Ross Dobson's 3 Ways With .... Stale Bread (Murdoch Books, 2007). It's an oldie but a goodie and I track Dobson down and we joke it's time perhaps for a new edition 3 Ways With ... during COVID-19.
 "I'd have to redo it with pretty photographs because apparently that's all people look at when they buy cookbooks isn't it," he jokes. "Do people actually cook anymore?"
 Yes, they do Ross. Just look at the pasta aisle.
 "I went to my supermarket the other day and I couldn't believe there was still so much canned fish on the shelves. A can of tuna is one of the most versatile things out there.
 "I like tuna and tomato passata and canned anchovies and olives, whip up a puttanesca. And lentils and chickpeas, I always seem to have those around."
 3 Ways With ... Stale Bread contains more than 300 recipes using things you'll find in the back of the pantry, freezer and fridge. A quick look at my stocks and I have these things in mind this week: creamy potato and leek soup to use up some cream cheese, puff pastry with tomato, haloumi and mint salad, to use up the pastry, haloumi with some mint from the garden; chargrilled chili prawns, with sweet chilli sauce and frozen prawns already on hand.
 Dobson is still authoring cookbooks - his new one Chinese Food Made Easy is out in a few weeks. 
 "I just hope people will find a love of cooking again," he says. "If there's something good to come out of this, maybe that's it."
  Healthy chili con carne. Picture: Ashley St George
 Ingredients
 1 tbsp extra-virgin olive oil
 1 large brown onion, finely chopped
 1 1/2 tsp ginger, finely grated
 1/3 cup korma curry paste
 1 tsp ground cumin
 1 tsp ground turmeric
 1 tsp sweet paprika
 400g can diced tomatoes
 250g cherry tomatoes
 1 cup water
 1 cup light coconut milk
 400g canned brown lentils, drained and rinsed
 400g canned chickpeas, drained and rinsed
 2 tbsp fresh coriander leaves, finely chopped
 brown rice, aim for 1/2 - 1 cup cooked rice per person, to serve
 Mango chutney ingredients:
 3/4 cup Greek yoghurt
 1 tbsp mango chutney
 Method
 Fill a medium saucepan with cold water. Add rice and cook for 25 minutes.
 Heat oil in a large saucepan over medium heat. Add onion and ginger, stirring until soft. Add paste and spices and cook, stirring until fragrant.
 Add canned tomatoes, cherry tomatoes, water, coconut milk, lentils and chickpeas. Mix to combine and bring to the boil. Reduce heat, simmer, uncovered for 10 minutes.
 Meanwhile, prepare mango chutney.
 Once rice is cooked, drain and divide between four bowls. Serve dahl with brown rice, coriander and chutney.
 Serves 4.
 READ MORE
 1 tbsp extra- virgin olive oil
 1 red capsicum, deseeded, finely chopped
 1 red onion, finely chopped, plus extra, to serve
 1 long fresh green chilli, chopped, plus extra, to serve
 3 garlic cloves, crushed
 500g turkey mince
 2 tbsp tomato paste
 400g canned chopped tomatoes
 1 1/2 cups salt reduced chicken stock or water
 1 tbsp dried oregano
 3 tsp ground cumin
 2 tsp smoked paprika
 1 cinnamon stick
 400g can black beans, rinsed, drained
 brown rice, aim for 1/2-1 cup cooked rice per person, to serve
 Greek yoghurt, to serve
 avocado, sliced, to serve
 fresh coriander sprigs, to serve
 wholegrain corn chips, to serve
 Heat the oil in a large, deep frying pan over high heat. Add the capsicum, onion, chilli and garlic. Reduce heat to medium-low. Cook, stirring often, for 10 minutes or until soft.
 Increase heat to high. Add the mince and cook, breaking up any lumps with a wooden spoon, for 5 minutes, or until the mince browns. Add the tomato paste and stir to combine. Add the tomato, stock or water, oregano, cumin, paprika and cinnamon. Season with sea salt and cracked black pepper. Bring to the boil and then reduce heat to medium-low and simmer for 25 minutes, or until liquid has reduced.
 Meanwhile, fill a medium saucepan with cold water. Add rice and cook for 25 minutes. With five minutes remaining on the mince, stir in the beans.
 Serve the chilli con carne with brown rice and top with extra red onion, chilli, yoghurt, avocado, corn chips and coriander.
 From Georgia Houston
 Heat a few splashes of light olive oil in a large heavy-based casserole dish. Cook four chicken forequarters in the sizzling oil for five to six minutes, turning the pieces often, so they are well browned, then remove. Cook one chopped onion, two chopped garlic cloves, one chopped carrot and one sliced celery stalk in the dish for two to three minutes to soften the vegetables. Add 250ml white wine to the pan and let it sizzle for a few minutes until almost evaporated. Return the chicken to the dish with 400g tinned chopped tomatoes, 90g black olives and 250ml water. Season well, cover and simmer for 50 minutes. Stir through a handful of chopped flat leaf parsley and serve with potato mash or soft polenta.
 Serves 4. 
 Have your oven hot and ready at 200C. Heat a splash of olive oil in a casserole dish and stir-fry one chopped onion and two grates zucchini for two to three minutes so the onion softens and sizzles in the hot oil. Add 220g short-grain rice to the pan and stir for a minute then add 225g tinned tuna, drained, 400g tinned chopped tomatoes, one teaspoon rosemary leaves, 375ml chicken stock and season well with salt and black pepper. Bring to the boil, cover the dish with a tight-fitting lid and cook in the oven for 30 minutes. Scatter a handful of chopped flat leaf parsley on top. 
 Serves 2.
 From 3 Ways With ... Stale Bread
 https://www.canberratimes.com.au/story/6692850/self-isolation-suppers-easy-meals-from-the-pantry-fridge-and-freezer/
