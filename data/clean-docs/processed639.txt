Coronavirus: Advice issued for spending Ramadan in lockdown - BBC News
 The Muslim Council of Britain, the largest umbrella organisation for Muslims in Britain, has published online guidance to help millions cope with the restrictions of lockdown during the coming fasting month of Ramadan.
 It says this year's Ramadan, which begins at the end of this week, will be "a very different experience for Muslims as we adapt to changing circumstances during the Covid-19 pandemic".  
 With lockdown continuing, there will be no congregational acts of worship outside the home, no Taraweeh prayers at the mosque and no iftars (usually a huge ritual meal marking the breaking of the fast after sundown) with friends and family to attend.
 Instead, the MCB is offering guidance on how to arrange virtual iftars online with loved ones and community members by using video chat. 
 Plan your iftar menus in advance, it says, so as to avoid multiple shopping trips. 
 It also suggests eating high-energy, slow-burning foods during the second meal of the night, the suhoor, which takes place just before dawn, to help maintain energy levels throughout the daylight fasting hours.
 Daytime fasting can sometimes lead to frayed tempers, especially when someone has been up for much of the night and is then expected to start work in the morning.
 The MCB advises Muslims to "honour your workplace duties with patience and good grace to those around you". 
 But it also warns that a refusal by employers to allow flexibility in work timings for fasting employees without a legitimate business reason could amount to unlawful indirect discrimination. 
 The Muslim holy month of Ramadan is a special time for nearly two billion Muslims all over the world. 
 In any normal year, it is a time of communal prayer, of daytime fasting, night-time feasting, extensive socialising and acts of profound generosity and charity as Muslims reaffirm their faith in God. 
 For those living in the West, forsaking food and drink during daylight hours while the rest of the population is able to indulge publicly in cafes and restaurants has always been a testing time. 
 But this year it will be very different. 
 With lockdown continuing, most of those visible temptations on the streets will be absent as people stay at home.
 Yet individual isolation is completely counter-intuitive to most Muslims during the month of Ramadan. Usually, whole communities tend to pour onto the streets after dark to share and enjoy the communal experience with their relatives and neighbours. 
  But Dr Emman El-Badawy, an expert on Islamic jurisprudence, believes the spirit of Ramadan will survive. 
 "So much of the essence of Ramadan can be maintained during isolation. 
 "The spiritual aspects may even be heightened for some of us, with less distractions than usual. 
 "The communal practices will be missed under the restrictions, for sure, but there are already great initiatives being built to help with this."
 The US leader will sign an executive order to suspend migration, but it's not clear how he will do it.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/uk-52363397
