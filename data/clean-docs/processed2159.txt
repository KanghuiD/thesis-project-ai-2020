Above the Law
 Biglaw
 By
 Kathryn Rubino 
 The firm is a lot bigger, but COVID-19 is still leaving its mark.
 Finance
 Ray Dalio: Coronavirus Is Just The Thing Humanity Needed To Build A Bright And Exciting Future
 Jon Shazar - Dealbreaker 
 Isolation may be getting to the Wizard of Westport.
 Courts
 The Youngest Supreme Court Justice Ever
 Not the biggest thing they’re known for.
 Paul Singer Should Probably Just Factor In A Fine Whenever Investing In French Transport
 It’s apparently a continuing cost of doing business.
 Maybe this will inspire a Biglaw trend!
 Morning Docket
 Jordan Rothman 
 * The prosecutor who resigned over a sentencing memo in the Roger Stone case has joined the DC Attorney General’s Office. Seems like he landed on his feet. [Hill]
 * A New Jersey lawyer cannot be readmitted to practice unless he shows that his wife has no access to his accounts, checkbooks, and other financial records. There must be a good story behind this… [ABA Journal]
  (more…)
 * The prosecutor who resigned over a sentencing memo in the Roger Stone case has joined the DC Attorney General's Office. Seems like he landed on his feet. [Hill]
 * A New Jersey lawyer cannot be readmitted to practice unless he shows that his wife has no access to his accounts, checkbooks, and other financial records. There must be a good story behind this... [ABA Journal]
 * A federal judge has dismissed a malicious prosecution claim filed by Jussie Smollett stemming from the attack he allegedly staged to increase his profile. [USA Today]
 * A Kentucky lawyer has been charged with making terroristic threats after allegedly threatening Kentucky's governor. This attorney should brush up on his constitutional law. [Hill]
 * Dozens of in-house lawyers are agreeing to pay cuts in order to help their companies deal with issues created by COVID-19. [Bloomberg Law]
 * A knife-wielding lawyer allegedly forced a journalist to delete footage of this attorney at a shelter-at-home protest. Guess this lawyer took the law into his own hands... [New York Post]
 Promoted
 Sponsored Content
 Free Course: Force Majeure and Other Defenses to Contract Performance During the Covid-19 Pandemic
 Free Course: Force Majeure and Other Defenses to Contract Performance During the Covid-19 Pandemic 
 Above the Law readers are offered 1 free CLE course each quarter, thanks to Lawline.
 Lawline 
 See Also
 Oof, Those Bar Exam Scores: An historic low.
 In A World Without The Bar Exam: Diploma privilege faces obstacles. 
 Arizona Law School Making Cuts: The result of COVID-19 austerity.
 Cuts At McDermott Will & Emery: Tipsters say there’re layoffs at the firm.
 Biglaw Gets Relatable: When a quarantined employee’s kid takes over the firm’s IG feed.
 Cuts At McDermott Will & Emery: Tipsters say there're layoffs at the firm.
 Biglaw Gets Relatable: When a quarantined employee's kid takes over the firm's IG feed.
 Advertising
 Thanks To Our Amazing Advertisers
 Above the Law 
 We appreciate your support.
 Small Law Firms
 How Lawyers Can Make Zoom Bloom And Boom Rather Than Doom Online Marketing
 Carolyn Elefant 
 Scooter Is Now The Perjury Dog And You May Tweet Her Your Stories Of Lying Under Oath, General Dishonesty
 Jonathan Wolf 
 Perjury Dog isn’t necessarily going to be limited to stories about lying under oath in court.
 Starting Remotely During COVID-19
 Starting Remotely During COVID-19 
 What if you are one of the lucky candidates to actually interview, receive, and accept a job offer with a major law firm during this weighty time of COVID-19?
 Daniel Roark and Katherine Loanzon - Kinney Recruiting 
 Law Schools  
 Family Law
 Insemination Fraud Case Scores Win
 Ellen Trachman 
 Third-degree burns are not the norm in insemination cases.
 Sports
 World Of Esports Betting And How It’s Presented An Opportunity In The Coronavirus Climate
 Darren Heitner 
 The business of betting on esports, an industry built on top of competing in video games, appears to be breaking out during the coronavirus crisis.
 Health Care / Medicine
 Moody’s Report: Health Insurers Will Remain Profitable After Pandemic
 ELISE REUTER - Medcity News 
 A report released Tuesday by Moody’s Investors Service predicted that most insurers will remain profitable after the COVID-19 pandemic.
 Law Schools
 Top 50 Law School To Slash Faculty And Staff Salaries, Conduct Furloughs
 Staci Zaretsky 
 This is the first law school to implement such measures due to the coronavirus outbreak.
 Law Firms Need to Get Creative in Hiring Amid the COVID-19 Outbreak
 Law Firms Need to Get Creative in Hiring Amid the COVID-19 Outbreak 
 So what is a law firm to do?
 KATHERINE LOANZON - Kinney Recruiting 
 Biglaw  
 Courts  
 After Complaint Likening Pride Flag To Swastika, Texas Judicial Commission Makes Courtroom Rainbowfrei Zone
 Elizabeth Dye 
 They did it to root out bias, WINK WINK.
 Law Firm Essentials For Data Privacy And Compliance In 2020
 Law Firm Essentials For Data Privacy And Compliance In 2020 
 What legal professionals need to know about the changing data privacy landscape.
 Why Attorney Supervision Could Undermine The ‘Diploma Privilege Plus’ Movement
 Joe Patrice 
 But just because there are obstacles isn't a reason not to try.
 The Plight Of Current Law Students Is Worse Than During The Great Recession
 No law student during the Great Recession had to worry about whether the bar exam would occur within a few months after graduation. But wait, there's more...
 Bad News For Law Schools: Multistate Bar Exam Scores Hit An All-Time Low
 Just when legal academia was seeing an increase in bar scores, the numbers come crashing down to earth.
 Did You Major In Logistics?
 Jill Switzer 
 Biglaw is adjusting to a new normal, but it’s not just Biglaw, it’s every firm of every shape and size.
 Am Law 50 Firm Lays Off Associates, Staff During Pandemic
 Bad news from this top Biglaw firm.
 Your Professional Relationships Could Determine Whether You Will Get A Paycheck Protection Program Loan
 Steven Chung 
 If this sounds unfair, you are not wrong.
 Hedge Funder Gives Lucky Person Some Social Distance On The Cheap
 He wanted $70 million, but can afford to take $37 million.
 * A lawyer who got ejected from the Second Circuit last year is asking the Supreme Court to hear his case. Since the high court is conducting arguments by phone currently, maybe he’ll just get hung up on. [New York Law Journal]
 * A lawyer who stole $128,000 from a mentally ill client has been suspended from practice. [Bloomberg Law]
 * A lawyer who got ejected from the Second Circuit last year is asking the Supreme Court to hear his case. Since the high court is conducting arguments by phone currently, maybe he'll just get hung up on. [New York Law Journal]
 * Missouri has become the first U.S. state to sue China over the COVID-19 pandemic. Not sure this is a distinction to be proud of. [U.S. News and World Report]
 * A Texas judge has been forced to take down a rainbow flag after an attorney filed a complaint and compared the symbol to a swastika and Confederate flag. [Hill]
 * Attorney General Barr has called stay-at-home orders "disturbingly close to house arrest" and the Justice Department might take actions against states that go too far. [NBC News]
 * Lawyers are having a difficult time determining if COVID-19 is an act of God. Maybe they should subpoena the Almighty to get more clarity... [Bloomberg Law]
 Register for The Changing Lawyer LIVE!, A One-Day Virtual Conference on April 23
 Register for The Changing Lawyer LIVE!, A One-Day Virtual Conference on April 23 
 All conference proceeds will support The CDC Foundation's Emergency Response fund. Sign up here.
 Presented by Litera 
 Our Sites
 https://abovethelaw.com/
