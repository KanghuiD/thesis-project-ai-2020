Air Canada to Pilot Application of TraceSCAN Technology for COVID-19 Contact Tracing in the Workplace
 Searching for your content...
 Phone
  877-269-7890 from 8 AM - 10 PM ET
 Contact Cision
  877-269-7890
 from 8 AM - 10 PM ET
 News provided by
 Oct 07, 2020, 08:00 ET
 Share this article
 MONTREAL, Oct. 7, 2020 /CNW Telbec/ - Air Canada announced today it plans to explore the application of COVID-19 contact tracing technology in its workplace using the Bluetooth enabled TraceSCAN app and wearable technology developed by Canadian-based Facedrive Inc.
 "The health and safety of our employees is of paramount concern to Air Canada and is key to restoring our operations safely for our customers. Air Canada has embraced a science-based approach to managing COVID-19 and as part of this has committed to evaluate the use of new technologies like TraceSCAN's wearables. We are enthusiastic about exploring the use of TraceSCAN because this Canadian technology has the potential to provide another layer of safety for our employees, so they can focus on taking care of our customers," said Samuel Elfassy, Vice President, Safety at Air Canada.
 "Our project with Air Canada is ground-breaking and provides another critical example of how TraceSCAN supplements the capability of the Canadian government-sponsored COVID Alert App. TraceSCAN will allow Air Canada to provide further protection to their employees, especially as many of these employees work in environments where the use of cell phone technology is not possible," said Sayan Navaratnam, Chairman and Chief Executive Officer at Facedrive Inc. "We are very pleased to help Air Canada explore ways to provide additional protection to their employees. We hope this pilot will be a model for other airlines to follow and look forward to continuing to work with industry to protect Canadians as our country returns to its normal working and travel schedules."
 The technology behind TraceSCAN was developed in partnership with leading minds at the University of Waterloo.
 "TraceSCAN is an AI-powered solution for contact tracing in the workplace which can track staff exposure to COVID-19 without GPS information. The technology provides a risk-based assessment of an individual's exposure within dynamic and dense work environments. The technology enables the creation and management of safer work environments needed during this critical phase of the pandemic," said William Melek, University of Waterloo mechanical and mechatronics engineering professor.
 TraceSCAN Wearables is an innovative technology, combining AI-enabled mobile application along with wearables devices built on the nRF52 Bluetooth chipset, which is used by the world's leading wearable manufacturers. Facedrive Health developed industry-specific TraceSCAN wearable technology to slow the spread of COVID-19 at the workplace through wristbands, wearable tags worn around the neck or pods that can be kept in the pocket.
 CAUTION REGARDING FORWARD-LOOKING INFORMATION
 About Air Canada 
 About Facedrive Inc.
 Internet:         aircanada.com/media
 Media Resources:Photos Videos B-RollArticles
 SOURCE Air Canada
  www.aircanada.com
 Also from this source
 Air Canada Makes Using eUpgrades Easier, More Rewarding for... 
 Air Canada Finalizing Initial Order for Newly Approved COVID-19... 
 Air Canada Announces Longer-Term Refinancings to Replace... 
 877-269-7890
 https://www.newswire.ca/news-releases/air-canada-to-pilot-application-of-tracescan-technology-for-covid-19-contact-tracing-in-the-workplace-897345300.html
