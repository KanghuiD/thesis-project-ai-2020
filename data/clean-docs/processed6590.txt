Spain and other countries count coronavirus death tolls differently and the numbers may be misleading, experts warn - Olive Press News Spain
 Covid-19 death figures are incomplete
 EPIDEMIOLOGY experts have warned that not all deaths from coronavirus are being recorded correctly.
 Experts claim that misinformation about the coronavirus death toll isn’t just confusing, it’s dangerous.
 If there is something that all countries have in common, it is that each record their coronavirus death toll in their own way, and none of them show a correct figure.
 According to El Pais, France only counts those who die in hospitals, Spain does not record deaths in nursing homes who haven’t been previously tested and the UK do not consider COVID-19 as a cause of death until it occurs after the fifth day of illness. Additionally in the UK, the relatives of a person who has died of coronavirus are asked for permission to include the deceased in the official count or not.
 Italy includes in the registry of coronavirus victims all patients who had tested positive and who have died, regardless of other aspects of their clinical history. As opposed to Spain, who when communicating the official figures, distinguish deaths with coronavirus but not by coronavirus.
 Furthermore, only a percentage of the population are officially diagnosed, as occurs in Spain due to the lack of testing, thus the percentage of deaths out of the total number of infected people is higher.
 Experts caution that misleading numbers are leaving the public confused about their risks and what they can do to help curb the spread of the virus.
  
   Read More Olive Press Issues Online
 
   Last Name   
    Make this an anonymous donation.  
   Donation Total:   5,00€ 
 https://www.theolivepress.es/spain-news/2020/03/30/spain-and-other-countries-count-coronavirus-death-tolls-differently-and-the-numbers-may-be-misleading-experts-warn/