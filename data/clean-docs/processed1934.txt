Coronavirus in Oklahoma: Oklahoma anticipates $416 million revenue failure, Stitt says
 log in to manage your profile and account
  TULSA, Okla. —  The state government setup a call center that will be staffed 24/7 to answer your questions about the coronavirus. Call 877-215-8336 to speak with an operator.
  Still happening? Which Oklahoma events are canceled, suspended or postponed?
  Content Continues Below
  The Oklahoma State Department of Health is tracking cases in the state. Go here to check its updated list of affected counties.
  The Centers for Disease Control and Prevention is tracking overall cases in the U.S. here.
  Looking for our previous coverage? Here’s a round-up of what happened in Oklahoma March 21-27.
  UPDATES THROUGH APRIL 5 CONTINUE BELOW
  Update 11:00 a.m. April 5 
  Update 4:30 p.m. April 4
  Governor Kevin Stitt announced the launch of a new volunteer initiative that will connect courageous Oklahomans to volunteer opportunities in their communities, once health professionals determine it is safe and appropriate to do so.
  The program, called Ready. Help. Go., provides a place for Oklahomans to offer their hand in future volunteer opportunities. Ready. Help. Go.’s first phase will focus on gathering information about willing volunteers and their skill sets and is intended to support communities throughout the state through the COVID-19 response and beyond.
  Update 11 a.m. April 4
  Update 2:09 p.m. April 3: Gov. Stitt said Friday the state could experience a $416 million revenue failure of approximately $416 million for the rest of fiscal year 2020, which ends June 30.
  Update 12:00 p.m. April 3: The City of Tulsa and the Health Department held a news conference Friday after the state announced three new deaths from coronavirus in Tulsa County.
  One of the dispatchers at Tulsa County Sheriff’s Office tested positive for the virus. A volunteer at the sheriff’s office died from the virus.
  According to the Tulsa Health Department, 20 of the 175 cases in Tulsa County have recovered.
  MORE >>> ‘Beloved volunteer’ at Tulsa County Sheriff’s Office dies from coronavirus
  Update 10:40 a.m. April 3: There were 988 cases of coronavirus in Oklahoma -- including 38 deaths.
  The four new deaths include two people in Muskogee County.
  There are an additional four deaths:
  Update 4:30 p.m. April 2: Gov. Kevin Stitt held a briefing from Oklahoma City on Thursday. Watch it here:
 Live update with Health Commissioner Gary Cox on Oklahoma's response to #COVID19.
  Update 11:10 a.m. April 2: Oklahoma cases of coronavirus jumped to 879 including four more people who died from it.
  Update 10:10 a.m. April 2: Gov. Kevin Stitt declared a health emergency for all 77 Oklahoma counties for the next 30 days.
  The declaration gives Stitt broader powers to take action against the spread of coronavirus in the state.
  He designated the Oklahoma State Commissioner of Health as the primary public health authority responding to the emergency.
  “This declaration also gives the governor the authority to allow health care professionals who have left the workforce to quickly rejoin the front lines against COVID-19 and protect first responders by helping them manage their personal protective equipment (PPE)," Stitt’s office says.
  “By loosening some restrictions, first responders will be able to know if the home they are dispatched to has a resident who has tested positive for COVID-19. Patient names and other identifying information will still be restricted.”
  Update 6:57 a.m. April 2: Gov. Kevin Stitt joined FOX23 News This Morning to talk about the work the state is doing to increase COVID-19 testing for everyone and more about the virus in the area.
  Update 5:01 a.m. April 2: The Tulsa Police Department tweeted that one of their officers tested positive for coronavirus.
  The department is asking the public to continue filing police reports online if possible.
  Update 3:31 p.m. April 1: Gov. Kevin Stitt announced Wednesday he has extended his Safer at Home executive orders to all 77 Oklahoma counties.
  He also said the state is lifting restrictions on who can be tested for COVID-19 and encouraged anyone with symptoms, or who had contact with a positive case, to go get test as soon as possible.
  Watch the news conference here:
 We are live providing an update on the State’s response to COVID-19.
  Update 7:09 a.m. April 1: The City of Tulsa and local health officials held a news conference to share the latest information on the coronavirus situation.
  The city’s “stay at home” order is extended to April 30.
  Mayor Bynum also announced the city plans to partner with Tulsa County, the Tulsa Day Center, and Salvation Army to help shelters accommodate more homeless people while maintaining social distance.
  The city is also establishing a Resilience and Recovery fund with $1.1 million from the city to help small businesses.
  Watch every live news conference as it happens on the free FOX23 News app. DOWNLOAD HERE.
  Update 7:09 a.m. April 1: Oklahoma coronavirus cases hit 719 including 30 deaths from the virus in the state.
  Greer County is the latest county to get added to Gov. Stitt’s “Safer at Home” order after getting its first case.
  Tulsa County is up to 115 cases.
  The seven new deaths came from these areas:
  Update 7:09 a.m. April 1: Bixby Mayor Brian Guthrie signed an Executive Order Tuesday for all residents that will go into effect at 11:59 p.m. Wednesday.
  Update 8:09 p.m. March 31: Broken Arrow Mayor Craig Thurmond issued a shelter-in-place order for all residents effective noon on Wednesday.
  Under this proclamation:
 To reduce the community spread of COVID-19, Mayor Craig Thurmond has issued a shelter in place order for all residents. Please note that this emergency order takes effect on 12:00 noon, April 1, 2020 and will continue through April 30. The order states that all Broken Arrow residents are required to stay at home except for those that are working in essential jobs or to take care of essential needs such as buying groceries, gas, medical care, or caring for a loved one. We ask everyone to please maintain social distancing at all times when you’re out. For more information please visit￼ brokenarrowok.gov/coronavirus.
  Update 4:20 p.m. March 31: The Saint Francis Health System in northeastern Oklahoma is now performing COVID-19 testing in-house.
  Previously, collected specimens were sent out-of-state. Now they will be processed in the Saint Francis Laboratory with results typically reported within 24 hours.
  There is no up-front, out-of-pocket cost for patients being tested through Warren Clinic – it is handled like all other lab requests throughout the system. And, like all other lab testing, a physician order is still required. Warren Clinic’s process for screening and referral to testing has not changed and patients should still call their primary care physician if they are symptomatic or have had exposure to someone who has tested positive.
  Update: 1:02 p.m. March 31: The Tulsa County Board of Elections is moving the upcoming election scheduled for April 7.
  All school board election on that ballot will be moved to June 30.
  The bond issues for Berryhill Public Schools, Keystone Public Schools, and the City of Jenks do not have a new date set.
  Update: 11:30 a.m. March 31: There are now 565 positive cases of coronavirus in Oklahoma and six more people have died from the virus.
  Update: 3:18 p.m. March 30: Now that a positive case of COVID-19 has been found in Rogers County, the Claremore police and fire departments are making changes to slow the spread.
  Both departments will limit non-essential public activities including station tours and other events with 10 or more people.
  Visitors will no longer be allowed at either agency and non-emergency needs will be directed to the police department at 918-341-1212 and the fire department at (918) 341-1477.
  Update 8:48 a.m. March 30: State officials released new numbers showing 481 cases of coronavirus in Oklahoma.
  A Cleveland County man died from the virus -- bringing the state’s death toll to 17.
  New counties with cases include Beckham, Cotton and Love counties. These counties will now be required to come into compliance with Governor Kevin Stitt’s “Safer at Home” executive order.
  Update 8:48 a.m. March 30: Jenks City Council passed Ordinance 1511 which orders Jenks citizens to shelter in their homes other than conducting essential activities or operating essential businesses.
  The ordinance goes into effect at 11:59 p.m on March 30 and ends at 11:59 p.m. April 16.
  Jenks citizens are legally required to stay at least 6 feet away from others in public.
  Jenks police will enforce the new ordinance and may issue citations to repeat offenders.
  Update 7:19 p.m. March 29:
  Update 5:55 p.m. March 29:
  Governor Stitt announced a new executive order regarding traveling:
  “As we continue to respond to #COVID19, I have issued an EO requiring travelers from six states to self-quarantine for 14 days, requiring delivery personnel to submit to screenings upon request at hospitals, clinics, long term care facilities and day cares and protecting health care workers and their families from discrimination in housing or childcare. Our health care workers are the true heroes in this fight against COVID-19. These protections will help us #FlattenTheCurve and continue to keep our health care workers and their families safe as they take care of their fellow Oklahomans.”
  An emergency executive order was issued for the city of Catoosa:
  Emergency Executive Order 2020-02, Order Issued Pursuant to Civil Emergency Authority Requiring Shelter In Place and Closure of All Non-Essential Businesses. This order shall take effect at 11:59 a.m. on March 30, 2020 and shall continue in effect indefinitely.
  Update 11:10 a.m. March 29:
  Update 12:15 p.m. March 28: Tulsa Mayor G.T. Bynum issued an executive order putting the city under a shelter-in-place directive beginning at 11:59 p.m. Saturday night and ending April 16. The order will be evaluated then.
 Today, I have signed an order requiring shelter-in-place protocols for Tulsa. The Tulsa Safer At Home executive order...
  Please note as part of the executive order issued today, all City-owned sport courts, golf courses, sports fields and dog parks are closed, and the use of any fitness equipment, playground equipment, or shelters within City Parks is prohibited even if not locked, gated, or fenced.
  View the full executive order here.
 3/28/2020 COVID-19 - Tulsa Situation Update
  Update 10:34 p.m. March 27: The Oklahoma Health Department reports 322 positive cases of coronavirus across 38 counties.
  That’s 248 more cases than we knew about on Monday. Much of that is a reflection of increased testing. Gov. Stitt. announced Thursday that Oklahoma State University now has the reagents to perform 10,000 tests.
  The state reports 105 COVID-19 patients are hospitalized. The most recent death, a Creek County man in his 70s, brings the state’s death toll to 8.
  Oklahoma County has the most cases with 94, followed by Tulsa County with 49.
 Trending Stories
 Trending Video
 Coronavirus: Aldi gives employees 10% raise 
 https://www.fox23.com/news/local/oklahoma-coronavirus-updates-see-whats-happening-this-week/LMZBNRCQXFBOFD4LFVF73IDV5A/
