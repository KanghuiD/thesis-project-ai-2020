How coronavirus is turbocharging office holiday politics | Financial Times
 										Coronavirus business&nbspupdate
 										Get 30 days complimentary access to our Coronavirus Business Update newsletter
 									
 										30 days complimentary
 Pilita Clark
 Be the first to know about every new Coronavirus story
 At the start of this year I made a plan to spend this Easter break flying down on easyJet to a house near a beach at the bottom of Spain. Along with the plans of numberless others, it failed.
 EasyJet is grounded. I am locked down in London. The beach in Spain is closed and in a town just up the coast, some locals greeted a convoy of ambulances trying to bring in pensioners evacuated from a virus-ravaged care home by pelting the vehicles with stones.
 All things considered, staying home instead has been fine, especially since I still have a job and more to the point, managed to take a chunk of annual leave before the world shut down. 
 A lot did not, which is one reason why the politics of the office leave roster have gone into corona overdrive, while the idea of the holiday itself is being upended.
 Having once had the job of drawing up team holiday rotas, I know it has always required the skills of a Nobel-winning diplomat and a butcher. The needs and wants of all staff must be carefully juggled and judged before a final, decisive chop. It is difficult to imagine how much harder that task is now.
 Last week I came across people in the City of London who had been working exhausting hours for weeks, in swamped teams. Now they needed volunteers to cancel the Easter breaks they had booked months earlier. 
 No one was risking their lives, as health workers around the world are. All were able to work at home, unlike the bus drivers, supermarket staff and couriers who do not have the choice.
 Yet they still faced a dilemma. If they failed to stay at their desk, how safe would their job be if their company started the cutbacks ripping through their industry? If they failed to take a holiday, how safe would their marriage — or mental equilibrium — be after yet another draining week of work?
 At the other end of the spectrum, I spoke to people whose bosses had told them they should take leave now if they could, because otherwise they were likely to get too tired and if they all tried to take time off at once later there would be chaos.
 “They said we can’t have everyone taking holidays in September or whenever this is all over,” said one analyst at a bank. “We haven’t been given any formal rules about it from the top but this is what is happening in the division where I work.”
 Fair enough, I thought, until I joined a video hang-out where one of the speakers announced he was actually about to go on holiday for a week.
 The Financial Times is making key coronavirus coverage free to read to help everyone stay informed. 
 Find the latest here.
 To do precisely what, I wondered. It has been impressive to read about “Costa del Yorkshire” Brits who have dragged beach towels and pebbles into their living rooms to recreate the holiday spots they are missing. Top marks also go to the confined French couple who went to the bother of posting photos of themselves in sunglasses and swimwear to look as if they were on their cancelled break in Mexico.
 In real life though, how much fun can really be had on a holiday cooped up at home? 
 A frazzled colleague who decided to do it reports it was better than expected. Completing a 1,000-piece jigsaw puzzle for the first time in years proved surprisingly therapeutic. It was pleasing to see his shocked teenagers discover on a family movie night that Alien and other films made decades before their birth were actually quite good. Serious progress had been made on one or two outstanding chores around the house.
 Still, he would have preferred the week in the countryside that he was originally supposed to have taken. And there was the not insignificant matter of using valuable annual leave in semi-captivity. Or so he thought.
 It is a measure of the mass corona confusion at large that neither he nor I knew at the time that the government had made a notable announcement on statutory annual holidays, which are mostly lost if not taken. 
 To help key industries, rules would be relaxed so that workers could carry unused leave into the next two years.
 This move was widely reported but in the tsunami of coronavirus news, it has taken time to sink in. It was, in other words, very much like almost everything else in this eerie, unsettling time. 
 					Get alerts on Work & Careers when a new story is published
 				
 International Edition
 https://www.ft.com/content/73058d36-7a3f-11ea-af44-daa3def9ae03
