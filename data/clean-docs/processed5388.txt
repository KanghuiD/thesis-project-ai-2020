U.S. now leads the world with most deaths, Boris Johnson out of hospital
 
 The U.S. has surpassed Italy as the country with the highest number of coronavirus deaths with nearly 22,000 recorded by early Monday, according to NBC News figures.
 Worldwide, the death toll is more than 113,000, and the number of confirmed cases has surpassed 1.8 million, according to Johns Hopkins University. 
 In the U.K., Prime Minister Boris Johnson was released from St. Thomas' Hospital in London and returned home Sunday, a promising sign for the Conservative leader's recovery.
 To mark Easter, Pope Francis gave his annual address on Sunday to an empty basilica, calling for solidarity and prayer during these difficult times as holiday traditions have been upended in the pandemic.
 In pop culture, coronavirus survivor Tom Hanks made a surprise appearance on "Saturday Night Live," giving the opening monologue for the show's remote episode from his kitchen. SNL's current and former cast members also memorialized SNL music producer Hal Willner, who died of complications from the virus.
 As unemployment continued to soar in the U.S., Former Vice President Joe Biden released a plan to reopen the American economy in a New York Times op-ed.
 Mainland China reported 99 new coronavirus infections, more than doubling from the previous day to reach a one-month high, as the number of single-day imported cases hit a record, official data released Sunday showed. Almost all the new infections — the biggest daily count since March 6 — involve travelers from overseas. Just two out of the 99 cases were locally transmitted.
 In addition, highlighting another major source of risk, newly reported asymptomatic coronavirus cases nearly doubled to 63, up from 34 the previous day, according to China's National Health Commission.
 Download the NBC News app for latest updates on the coronavirus outbreak.
 Tom Winter
 An emergency medical technician for the New York City Fire Department who worked on the World Trade Center rescue and recovery effort after 9/11 has died, the department said early Monday
 Gregory Hodge, 59, was a 24-year veteran of FDNY most recently working as a watch commander at the emergency management office.
 “EMT Hodge was a skilled first responder who provided outstanding emergency medical care to thousands of New Yorkers throughout his long and distinguished career of service,” said Commissioner Daniel A. Nigro. “This pandemic has impacted our Department at every level, especially our EMS members who are responding to more medical calls than ever before."
 The Manhattan resident began his career assigned to a station in Harlem and later worked in the Bronx. He is survived by an adult son.
 Reuters
 IDLIB, Syria — Thousands of displaced Syrians have begun moving back to their homes in war-torn Idlib province despite the risk of renewed conflict, some driven by fear that the coronavirus could wreak havoc on crowded camps near the Turkish border.
 About 1 million Syrians fled Idlib and its surrounding countryside in northwest Syria this past year after Russian-backed government forces stepped up a campaign to retake the last rebel stronghold after nine years of war.
 Syria's northwest does not yet have a confirmed case of coronavirus, but doctors fear the area's ravaged medical infrastructure and overflowing camps would quickly turn any outbreak into a humanitarian disaster.
 BREAKING:  A federal court issued a new ruling to block Alabama from using the guise of the COVID-19 crisis to prevent people from accessing abortion care.Abortion is essential, time-sensitive health care — and it will remain available in Alabama.
 As deadly tornadoes and fierce windstorms battered the South on Sunday, people who found safety in a Mississippi shelter still tried to practice social distancing.
 The Associated Press
 SIOUX FALLS, S.D. — Virginia-based Smithfield Foods announced Sunday it is closing its pork processing plant in Sioux Falls until further notice after hundreds of employees tested positive for the coronavirus — a step the head of the company warned could hurt the nation's meat supply.
 The announcement came a day after South Dakota Gov. Kristi Noem and Sioux Falls Mayor Paul TenHaken wrote to Smithfield and urged the company to suspend operations for 14 days so that its workers could self-isolate and the plant could be disinfected.
 The plant, which employs about 3,700 people in the state's largest city, has become a hot spot for infections. Health officials said Sunday that 293 of the 730 people who have been diagnosed with COVID-19 in South Dakota work at the plant.
 Erin McLaughlin and Andrew Blankstein
 LOS ANGELES — As Los Angles County reaches over 9,000 reported COVID-19 cases and nearly 300 deaths, a huge unknown remains here and across the nation: How bad is it?
 With a population of 10 million, larger than those of more than 40 states, the county is a prime location to launch a large-scale study that aims to answer that question and learn more about antibodies that could potentially provide immunity from COVID-19, the disease associated with the coronavirus, according to public health and policy experts.
 Read the full story.
 David K. Li
 More than 6,000 residents of New York City have lost their lives from complications brought on by COVID-19, health officials said Sunday night. 
 There have been at least 6,182 confirmed coronavirus fatalities as of 5 p.m., according to the city's Department of Health and Mental Hygiene's daily tally.
 The increase of 440, from the same time on Saturday, follows spikes of more than 500 in four of the previous five reporting periods. 
  
 https://www.nbcnews.com/health/health-news/live-blog/coronavirus-live-updates-u-s-surpasses-20-000-deaths-lead-n1182086