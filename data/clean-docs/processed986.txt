Opinion: Evidence of grim long-term impacts of Coronavirus | Morning Bulletin
 	THERE'S no crystal ball for any of us to know how long we will be subjected to social distancing rules during this coronavirus pandemic.
 	I follow many experts on social media that have been discussing the medical pros and cons of different approaches to the pandemic and the economic impacts.
 	They all have the same message - this pandemic is going to have long lasting effects on our society, both medically, mentally and financially.
 	There is no way to balance the economic welfare of the world with the physical health and wellbeing. Both are going to be damaged.
 	Right now, the psychological well being of extroverts is being affected.
 	I follow a blogger who is based in Manhattan, New York.
  beautybeyondbones wrote a heartbreaking blog overnight that is an eye-opener for what could happen here in Australia if we didn't get the pandemic under control or remove the social distancing restrictions which leads to increased cases.
 	"I'm going to be really honest. Yesterday was the first really hard day. Sure, I've faced the fear of contracting the virus, been stunned by the sobering sight of death, and grieved the separation from my family. But yesterday was hard for a different reason. It was the first day where I really felt the acute sting of loneliness."
 	 
 	It was day 32 of quarantine for the young woman.
 	She explains further down she is an extrovert - someone who is energised by physical social interaction with other people.
 	Her blog came as photographs published on news websites showed mass graves being dug up to deal with the high number of deaths in the razzle, dazzle American city.
  Related: 
  New York 'as terrible as it gets' with COVID-19 
  New York officially world epicentre of pandemic 
  COVID-19 NYC: Video shows inmates burying coffins 
 	It's a black picture being painted of what could be if we don't stick to social isolation and it's an easy picture to forget when we are safe in our own homes in regional Australia where there are not many cases of this deadly virus. Please stay safe and talk to loved ones a lot.
  News
                     
                         The family of a child rape victim say they are “gutted” after an appeal of rapist’s sentence was thrown out.
                 
  Health
                         Melissa Ligonis’s life has been turned upside by corona­virus. She has cystic ­fibrosis and says it is unlikely she would survive if she got it.
  Business
                         Camping chain Aussie Disposals will close 11 stores after the business’ sales fell dramatically during the bushfire crisis and due to the coronavirus. 
  Pets & Animals
                         In a sign the gender reveal trend is getting out of hand, a baby bunny was dyed pink and gifted to a expectant couple leaving the rabbit traumatised.
                         The much-loved teen has been remembered as ‘a rock’ to her friends as tributes pour in the community. 
  Politics
                         The Queensland Parliament has been asked to consider whether a Labor MP is eligible to amid questions over a business transaction through his legal practice.
                         It is believed two vehicles are blocking the road. 
                         The island has approval for a eco-resort with more than 100 units, pool, gym...
                         A 15 year old has been charged for the break-in at 10 business premises in Yeppoon...
                         Pranksters wanting a laugh are going to get more than they bargained for as Police...
 Refer to our helpful FAQ section for any problems you might be experiencing.
 https://www.themorningbulletin.com.au/news/blog-experts-discussions-show-grim-long-term-impac/3995149/
