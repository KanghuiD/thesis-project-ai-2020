New coronavirus could hurt Chinese GDP at a bad time — and Japan's if it spreads | The Japan Times
 16
 M/CLOUDY
 Kyodo, AFP-JIJI, Reuters
 BEIJING – Fears are mounting that a new coronavirus identified in China may spread, not only infecting humans but also hurting the world’s second-biggest economy, which already is beset by a trade war with the United States.
 A potential pandemic of the novel virus, reminiscent of the 2003 epidemic of severe acute respiratory syndrome (SARS), would deal a blow to China’s tourism and retail industries, further hampering the nation’s economic growth, analysts said.
 googletag.cmd.push(function() { googletag.display('div-gpt-ad-1499653692894-0'); });
 Because Japan will receive a significant number of foreign visitors for the Tokyo Summer Olympics and Paralympics, the world’s third-largest economy would also be exposed to the threat of the virus, they added.
 Late last year, health authorities in Wuhan said unexplained pneumonia cases had cropped up in the city, 1,000 kilometers south of Beijing. On Jan. 9, state-run media reported a new type of coronavirus had been confirmed.
 After announcing that a 61-year-old man had died from a mysterious form of pneumonia and that an outbreak within a family had been spotted, Wuhan authorities said Wednesday that the possibility cannot be ruled out that the virus is transmissible among humans.
 RELATED STORIES
 				China virus outbreak linked to seafood market not currently spreading: WHO			
 				17 new cases of mystery virus reported in China			
 				A glance at the new coronavirus outbreak in China			
 On Thursday, a 69-year-old Wuhan man became the second reported fatality.
 Thailand has detected two infections, and Japan has confirmed its first case of the disease.
 Researchers said Friday that the number of infected people is likely hundreds more than officially reported. A paper published by scientists with the MRC Centre for Global Infectious Disease Analysis at Imperial College in London, which advises bodies including the World Health Organization, estimated a “total of 1,723 cases” in Wuhan as of Jan. 12.
 The researchers took the number of cases reported outside China to infer how many were likely infected in the city, based on international flight traffic data from Wuhan’s airport.
 “For Wuhan to have exported three cases to other countries would imply there would have to be many more cases than have been reported,” professor Neil Ferguson, one of the authors of the report, told the BBC.
 “People should be considering the possibility of substantial human-to-human transmission more seriously than they have so far,” he added, saying it is “unlikely” that animal exposure is the main source of infection.
 Authorities in Hong Kong have stepped up detection measures, including rigorous temperature checkpoints for travelers from the mainland.
 The U.S. said from Friday it would begin screening flights arriving from Wuhan at San Francisco Airport and New York’s JFK — which both receive direct flights — as well as Los Angeles, where many flights connect.
 Health authorities said on Saturday they had discovered four more cases in Wuhan. The four were diagnosed on Thursday and were in stable condition, the Wuhan Municipal Health Commission said.
 As many Chinese are expected to travel abroad during the Lunar New Year holiday this month, infections could become rampant mainly in Asian nations, experts warn.
 Japan’s top government spokesman, Chief Cabinet Secretary Yoshihide Suga, told a news conference on Thursday, “There is no clear evidence that the virus would persistently transmit from people to people.”
 The European Center for Disease Prevention and Control, however, has said the new virus “has similarity with the SARS coronavirus,” although it added, “Further genetic analyses are ongoing.”
 In 2003, SARS, believed to have originated in bats, emerged and raged in China and then spread worldwide, killing 774 people.
 That year, the Chinese economy was hit hard by the prevalence of the coronavirus, which dissuaded foreigners from visiting the country and people living there from going out for shopping, dining and entertainment.
 A group of researchers at the China Center for Economic Research at Peking University said in a 2004 paper that the SARS pandemic had led to a total loss of $25.3 billion to China’s economy in 2003.
 With the tourism sector taking a knock, the growth rate of the country’s gross domestic product then was calculated to have been 1 to 2 percentage points lower than it would have been if the SARS epidemic had not erupted, the group said.
 Yusuke Miura, chief researcher at the Mizuho Research Institute in Tokyo, said, “It remains to be seen how much the infection of the new coronavirus will expand, but the reemerging of the SARS-like illness is certain to pose a downside risk to the Chinese economy.”
 “A decrease in foreign visitors to China would of course hurt the tourism industry. In addition, locals would try to avoid crowds in such places as shopping malls, which would undermine private consumption,” he said.
 Retail sales of consumer goods account for roughly 40 percent of China’s GDP.
 “If foreigners get more worried about the new virus, they would start to shy away from buying Chinese goods and investing in China,” Miura said, adding, “The new virus could be a big headache to the Chinese economy that has been struggling to regain momentum.”
 In 2019, the nation’s economy grew 6.1 percent from a year earlier, its worst pace in 29 years, as trade spats with the United States have been stifling exports — a key driver of growth. Chinese GDP last year totaled 99.09 trillion yuan ($14.42 trillion).
 Some economists say Japan could fall victim to the coronavirus, given that a particularly high number of people, including Chinese, are expected to visit Tokyo from overseas for the Olympic and Paralympic Games this summer.
 According to China’s largest online travel operator, Ctrip.com, Japan has become one of the most popular Chinese tourist destinations, along with Singapore, Thailand and Malaysia.
 This means the risk of a viral pandemic could increase in Japan unless the government makes every effort to strengthen quarantine controls at ports of entry, observers say.
 In 2018, Upper House lawmaker Ryuhei Kawada asked the administration of Prime Minister Shinzo Abe to bolster measures to prevent the spread of infections like SARS and Middle East respiratory syndrome (MERS).
 If a new type of contagion similar to SARS or MERS gains prevalence at home in 2020, Japan could face an economic loss of ¥2.7 trillion ($24.50 billion) with 580,000 people losing jobs, Kawada said, citing government estimates.
 Ahead of the major sports events scheduled to be held for more than a month from late July, Chinese, Japanese and South Korean health ministers have confirmed that the three neighbors will swiftly exchange information about infectious diseases.
 						Click to enlarge
 					
 China, health, SARS, global economy, Japanese economy, MERS, covid-19 
 Strength in numbers: A more open approach to tracking the virus in Japan			
 Designing for good: Creators in Japan respond to the coronavirus			
 Resetting the political calendar after COVID-19			
 In era of COVID-19, a shift to digital forms of teaching in Japan			
 Where we want to go in Japan once this is all over			
 DEEP DIVE			
 Episode 47: The current state of Japan’s emergency			
 LAST UPDATED: Apr 15, 2020			
 Just for kicks: Japan’s sneaker obsession rebounds
 LAST UPDATED: Feb 29, 2020
 				Directory of who’s who in the world of business in Japan
 			
 LAST UPDATED: Apr 27, 2020			
 Sponsored contents planned and edited by JT Media Enterprise Division.
 広告出稿に関するおといあわせはこちらまで
 Read more
 https://www.japantimes.co.jp/news/2020/01/18/asia-pacific/science-health-asia-pacific/new-coronavirus-china-economy/
