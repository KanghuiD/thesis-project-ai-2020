California should suspend Assembly Bill 5 in the wake of the coronavirus pandemic – Orange County Register
 					
 			Trending:		
 The impact of the coronavirus’ global outbreak changes daily and Californians need help.
 So far it has upended the economy, thrown the stock market in disarray, and severely altered people’s lives across the nation. Unemployment claims have surpassed 10 million, and Congress passed a $2 trillion stimulus package to keep our economy afloat to monetarily support vulnerable Americans during these tumultuous times.
 One impactful change the state of California can make is suspending Assembly Bill 5.
 Our state attracts freelancers and creatives. Unfortunately, the passage of AB5 last fall is jeopardizing these workers’ livelihoods by making it more difficult to legally define workers as independent contractors.
 This restrictive law has spread across a wide spectrum of industries, harming truckers, musicians, rideshare drivers, and even those at cleaning companies and software companies.
 AB5 codified a California Supreme Court case, Dynamex Operations West, Inc. v. Superior Court, which held that most workers are employees and ought to be classified as such. AB5 therefore mandates that those employees fall under labor law protections, such as minimum wage laws, sick leave, unemployment and workers compensation benefits. This, however, introduces a whole slew of complications, such as freelancers who work for multiple employers.
 Freelance journalists, in particular, are facing major issues as publications are either dropping writers from the roster or limiting individuals’ submissions to 35 per year, which renders the journalists’ paychecks to untenable. Under AB5, journalists, magazine contributors, opinion writers and the like are now limited to 35 articles per year, per publication, before the mandate of being brought onto payroll kicks in. This would prove too costly and unsustainable for many publications, seeing they would have to provide benefits to their freelance writers, many of whom write for multiple outlets, further confusing the situation.
 Another group heavily affected by this are musicians. Bands that get paid to play at various venues while touring, or studio musicians who record instruments for various singers, TV shows and movies don’t have a single employer that would be able to provide them with benefits as required in this unclear law. This puts all musicians in a state of limbo while trying to figure out where they fit and whether they can continue working like they used to.
 This shift has certainly contributed to the unprecedented unemployment numbers as companies lay off workers due to the coronavirus’s effect on the economy and as businesses have to shut down. Now those workers are in desperate need of an income.
 Related Articles
 			For victims of relationship violence, staying home isn’t always safer		
 			A cry for justice for detained immigrants		
 			Changing Prop. 13 will harm small businesses, especially those owned by minorities		
 			Over-regulation and coronavirus testing delays		
 			Toilet paper short, food abundant		
 Now is the time, more than ever, to give our workers as many opportunities as possible. That means not burdening them with unnecessary legal restraints like AB5. 21st century technology offers so many opportunities for people to work remotely and make careers for themselves without ever having to leave their home. This crisis is restricting many people into their homes, so why not give them more opportunities to do freelance work and make money while they are at home?
 At a time when our communities should rally together as we get through this pandemic, job restrictions should not add to our list of worries.
 I urge Gov. Newsom to suspend AB5 during this period with so many jobs on the line.
 Repealing AB5 will not be the only solution to helping Californians find work as we face this crisis, but it will absolutely help.
 We need to allow Californians to find available work with the least amount of hindrance to their search.
 Michelle Steel is a member of the Orange County Board of Supervisors and a candidate to represent the 48th District in the House of Representatives.
 Get the latest news delivered daily!
 OK
 https://www.ocregister.com/california-should-suspend-assembly-bill-5-in-the-wake-of-the-coronavirus-pandemic