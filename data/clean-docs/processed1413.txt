WaPo issues scathing video evidence of Trump’s White House ‘intimidation tactics’ against journalists – Raw Story
 Published
 on
 By
 On Friday, a White House staffer was caught trying to kick CNN out of their assigned front-row seat and move them to the back row.
 The White House press corps refused to go along with the move, even with the White House falsely claiming the Secret Service would enforce their decree.
 On Monday, The Washington Post published a video opinion piece after obtaining footage of part of the incident.
 “The White House is invoking the Secret Service and using cheap intimidation tactics to scare reporters into other seats,” Erik Wemple, the newspaper’s media critic, said in the video.
 “They’re using these seats to accomplish a change in tone in the briefing room. The president doesn’t want to answer — or even face — the sort of questions that CNN and other outlets are asking in that briefing room,” he explained. “It doesn’t matter where you put the reporters, those questions have to be answered.”
 Watch:
 Enjoy good journalism?
 … then let us make a small request. The COVID crisis has cut advertising rates in half, and we need your help. Like you, we here at Raw Story believe in the power of progressive journalism. Raw Story readers power David Cay Johnston’s DCReport, which we've expanded to keep watch in Washington. We’ve exposed billionaire tax evasion and uncovered White House efforts to poison our water. We’ve revealed financial scams that prey on veterans, and legal efforts to harm workers exploited by abusive bosses. And unlike other news outlets, we’ve decided to make our original content free. But we need your support to do what we do.
 Raw Story is independent. Unhinged from corporate overlords, we fight to ensure no one is forgotten.
 … then let us make a small request. The COVID crisis has cut advertising rates in half, and we need your help. Like you, we believe in the power of progressive journalism — and we’re investing in investigative reporting as other publications give it the ax. Raw Story readers power David Cay Johnston’s DCReport, which we've expanded to keep watch in Washington. We’ve exposed billionaire tax evasion and uncovered White House efforts to poison our water. We’ve revealed financial scams that prey on veterans, and efforts to harm workers exploited by abusive bosses. We need your support to do what we do.
 Donald Trump resumed his highly-controversial COVID-19 briefings on Monday, after it was widely reported that White House staff and outside advisors feared the TV spectacles had been damaging the president's political standing.
 "The president said this weekend the briefings were not worth the 'time and effort,' that is a quote from him," CNN's Don Lemon noted. That didn't last long, because he was back rewriting history and shifting the blame tonight. 
 For analysis, Harwood interviewed CNN White House correspondent John Harwood.
 "He just can't stay away, can he?" Lemon asked.
 The debate continues as to how to estimate the increase in excess deaths that occurred before there was adequate COVID-19 testing.
 Many people who died at home or because of heart attack or stroke were not tested for coronavirus, which was initially thought to be more pulmonary than cardiovascular.
 But researchers have noticed steep increases in deaths that were not attributed to coronavirus, suggesting a large number of unreported coronavirus deaths.
 On Monday, the chairman of the New York City Council's Committee on Health, suggested that all excess deaths should be attributed coronavirus.
 The federal government is failing to provide Personal Protective Equipment for federal workers being called back to work, Axios reported Monday.
 "The Internal Revenue Service called roughly 10,000 employees back to work this week, but it is requiring that they provide their own facial masks to protect themselves from the coronavirus pandemic," 
 "Democrats on the House Ways and Means Committee have blasted the plan, stating that it's 'completely irresponsible and unethical for the IRS to demand those workers obtain their own protective equipment.' A memo notes that employees who do not provide their own equipment may be forced to return home," Axios reported.
 Masthead |
 https://www.rawstory.com/2020/04/wapo-issues-scathing-video-evidence-of-trumps-white-house-intimidation-tactics-against-journalists/
