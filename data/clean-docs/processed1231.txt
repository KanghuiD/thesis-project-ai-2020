At home in Aspen: Pantry-ready recipes from ‘Tastes of Aspen’ author Jill Sheeley | AspenTimes.com
 Entertainment Entertainment | 
 atravers@aspentimes.com
 Jill Sheeley has been cooking in Aspen for decades, back to her ski bum days in the kitchens of local restaurants in the 1970s. She’s also collected and written local recipes for her cookbooks “Tastes of Aspen” and “Lighter Tastes of Aspen,” and included additional favorites in her 2019 memoir “Those Were the Days.”
 As the local stay-at-home order went into effect to stem the spread of the new coronavirus, the Aspen Times turned to Sheeley for recommendations for the best recipes that anybody can make at home right now. Sheeley has been following public health directives and began an informal quarantine on March 13.
 “I’m on day nine of self-isolation,” she said Monday, “which has flown by due to the fact that my dog, Ranger and I are lucky to still be able to go outdoors and cross-county ski and pretend it’s just another perfect day in paradise.”
 During her homestay, Sheeley said, she baked banana and blueberry muffins, the smell of which transported her fondly back to her college days when she babysat for a ranching family outside Denver and made meals from scratch with ingredients from their greenhouse.
 “We baked six loaves of bread each week and the sniffs wafted outside and brought family members and ranch workers in to devour a loaf of cinnamon, raisin bread slathered with warm butter,” she recalled. Cooking, Sheeley noted, is a comfort in stressful times.
 “My friend calls this, ‘cooking therapy,’” Sheeley said. “I share that feeling of satisfaction cooking or baking something beyond delicious and filling my home with comforting aromas. Smells are carried more acutely in our memories than any other of our senses. We seem to carry around a scent storage file that reveals a time and place we’d long tucked away.”
 Sheeley combed through her own recipe files, and called on some friends to find “pantry ready” recipes for everyone quarantining at home who’d like to try make something delicious.
 JILL’S VEGGIE SPAGHETTI
 INGREDIENTS: Spaghetti noodles, cooked till tender Spaghetti sauce grilled onions & zucchini squashes (grilled yesterday) Baked butternut squash, cut into cubes Red cabbage, chopped up Broccoli and Cauliflower, chopped up Garlic, minced Spices, your favorites Parmesan cheese
 DIRECTIONS: Simmer the spaghetti sauce with the veggies, garlic and spices. Add the noodles to the sauce, sprinkle with Parmesan cheese and enjoy.
 SIMPLE WHOLE-WHEAT IRISH SODA BREAD From Sylvia Wendrow, of Carbondale Prep Time: 10 minutes Serves: 2-pound loaf (12 slices)
 INGREDIENTS: 2 cups whole-wheat flour 2 cups all-purpose flour, plus more for dusting 1 teaspoon baking soda 1 teaspoon salt 2 1/4 cups buttermilk or 2 1/4 cups regular milk with 2 1/4 tablespoons of vinegar stirred in
 DIRECTIONS: Preheat oven to 450 degrees F. (or 425 F convection) Coat a baking sheet with cooking spray and sprinkle with a little flour. Whisk whole-wheat flour, all-purpose flour, baking soda and salt in a large bowl. Make a well in the center and pour in buttermilk. Using one hand, (or large spoon) stir in full circles starting in the center of the bowl working toward the outside of the bowl) until all the flour is incorporated. The dough should be soft but not too wet and sticky. When it all comes together, in a matter of seconds, turn it out onto a well-floured surface. Clean dough off your hand. Pat and roll the dough gently with floury hands, just enough to tidy it up and give it a round shape. Flip over and flatten slightly to about 2 inches. Transfer the loaf to the prepared baking sheet. Mark with a deep cross using a serrated knife and prick each of the four quadrants. Bake the bread for 20 minutes.(15 minutes convection) Reduce oven temperature to 400° and continue to bake until the loaf is brown on top and sounds hollow when tapped, 30 to 35 minutes more. (20-25 minutes convection) Transfer the loaf to a wire rack and let cool for about 30 minutes
 LAYERED ENCHILADAS (AKA ‘MEXICAN MESS’) From Carol Jenkins of Woody Creek
 INGREDIENTS: 1 lb ground beef or turkey 16 0z jar of salsa 1 lge onion chopped but split into 2 portions 7 oz can diced green chili 2 lge cloves of garlic minced 15 oz can of Hatch enchilada sauce (med.) Spices: 1 Tbsp cumin, 1 tsp each paprika, chili powder, ½ tsp cayenne S& P to taste 1 ½ cups of grated Colby or cheddar cheese & ¾ cup of grated pepper jack cheese 4-5 flour tortillas (8” size) Pitcher of Margarita for chef (optional)
 DIRECTIONS: In a cold skillet, add 2Tbsp olive oil and garlic. Turn to medium heat. Once starts to sizzle, add ½ the chopped onions. Cook till onions are soft then add the meat. Cook and add the seasonings (if need more heat increase cayenne or add chipotle Tabasco). Stir and cook till meat has lost its pinkness- do not overcook as it will bake in oven. Set aside. In a medium sized Dutch oven or casserole dish with lid, smear a little oil on bottom and top with tortilla. Add some meat, then salsa, green chilis, fresh onions, enchilada sauce and cheese. Repeat layers. For top layer, end with the meat, salsa, onions, and chilies and pour remaining sauce around. Top with cheeses. Bake covered at 350 for 30 minutes, then uncovered for 10. Let sit for 10 minutes before serving
 PUMPKIN GINGER MUFFINS
 From Shelly Gross of Snowmass Village
 INGREDIENTS: 1 cup white flour 2 cups whole wheat flour 2 ½ cup wheat bran 1 jar crystallized ginger 2 tsp baking soda 4 tsp pumpkin pie spice 2 tsp ground ginger 1/2 tsp salt 2 cup packed brown sugar 1 15 oz canned pumpkin 2/3 cup buttermilk 2/3 cup canola oil 1/2 cup molasses 2 tsp vanilla extract 4 lg eggs raw sugar to sprinkle on top Muffin cups
 DIRECTIONS: Preheat oven to 400 Combine flour, wwflour and wheat bran, pumpkin pie spice, baking soda, ginger & salt in a med. Bowl. Stir well. Make a well in center. Combine brown sugar, canned pumpkin, buttermilk, canola oil, molasses, vanilla, and eggs, stir well with a whisk. Add to flour mixture, Stir until moist. Mix in crystallized ginger Coat muffin tins with baking spray. Spoon mixture into cups and sprinkle with raw sugar. Bake for about 15 min. until center comes out clean.k Cool on a wire rack.
 BANANA BREAD
 From Cindi Davis of Aspen
 Makes 2 loaves
 INGREDIENTS: 1 stick soft butter 1 cup sugar 2 eggs beaten 4-5 bananas on the ripe side cut up into small pieces 1 teaspoon baking soda ½ tea salt 2 cups of flour
 DIRECTIONS: Cream sugar and butter together Add eggs blend Cut up bananas add to butter sugar eggs Add baking soda, salt to flour mix Combine all ingredients Flour two pans with Pam and lightly flour Pour in batter 45 minutes at 350
 BAKED FRENCH TOAST From Jill’s Aunt Ellie in Larchmont, N.Y.
 INGREDIENTS: 1 (13-14 inch long) loaf of Italian bread or a challah ½ stick of unsalted butter, softened 3 to 4 eggs 1 and 2/3 cup milk ½ to 1 tsp. vanilla ¼ tsp. salt
 DIRECTIONS: Butter a 13 by 9 by 2 inch baking dish Cut diagonal slices from bread or any way you like and place in buttered pan. They should fit tightly. Generously butter top of each slice. Whisk together eggs, milk, vanilla and ¼ tsp. salt until well combined and pour evenly over bread. Chill, covered, overnight or at least one hour until all is absorbed. Preheat oven to 425 degrees Bring pan to room temperature and sprinkle bread with sugar and cinnamon or just cinnamon. Bake, uncovered, in middle of oven until bread is puffed and top is golden, 20 to 25 minutes. Serve immediately.
 Readers around Aspen and Snowmass Village make the Aspen Times’ work possible. Your financial contribution supports our efforts to deliver quality, locally relevant journalism.
 Now more than ever, your support is critical to help us keep our community informed about the evolving coronavirus pandemic and the impact it is having locally. Every contribution, however large or small, will make a difference.
 Each donation will be used exclusively for the development and creation of increased news coverage.
 User Legend: Moderator Trusted User
  Director of Operations /Property Manager Full-time & year round employment Prior experience required, excellent communication…
  JOIN OUR TEAM PROJECT MANAGER ASSISTANT PROJECT MANAGER SUPERINTENDENT ASSISTANT SUPERINTENDENT Premier construction company offering…
  CALCO Concrete Pumping Hiring pump operators for Front Range and Mountain locations. Mechanically inclinded, work…
    El Jebel  CREW *$15.00 HR.** SHIFT MANAGERS *$16.00 HR.** • Free Meals…
  Hiring Class A CDL Drivers. Will pay up to $30 an hour based on experience.…
  Roaring Fork Club 100 Arbaney Ranch Rd. Basalt, CO. 81621. Bussers Golf Course Maintenance Landscape…
  Pitkin County is currently hiring for the following:  - Pitkin County Sheriff's Office - Sergeant…
 Tue H: 53° L: 36°
 Wed H: 68° L: 31°
 Thu H: 70° L: 40°
 Fri H: 69° L: 41°
 Sat H: 66° L: 41°
  Jobs
  Classifieds
  Autos
  Real Estate
  Rentals
  Service Directory
  Pets
  Merchandise
  Legals
  Farm & Ranch
  Recreation
  Announcements
  Buy Photos
  Contribute
  Advertise
  Submit an Obituary
  Archives
  Commercial Print Sales
  RSS
  Aspen E-edition
  Snowmass E-edition
  Cookie List
  Breckenridge - Summit County
  Craig - Moffat County
  Glenwood Springs - Rifle
  Park City - Utah
  Steamboat Springs - Routt County
  Vail - Eagle Valley
  Winter Park - Granby - Grand County
  Aspen High School - Skier Scribbler
 https://www.aspentimes.com/entertainment/at-home-in-aspen-pantry-ready-recipes-from-tastes-of-aspen-author-jill-sheeley/
