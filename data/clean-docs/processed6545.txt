Coronavirus crisis may deny 9.5 million women access to family planning | Global development | The Guardian
 Liz Ford
 Fri 3 Apr 2020 06.00 BST
 Marie Stopes UK said it was open for business as usual.
 Simon Cooke, MSI’s chief executive, said: “Women and girls will pay the price if governments do not act now to safeguard access to essential healthcare, including safe abortion and contraception.”
 “The crisis led to a spike in the number of teenage pregnancies. People were too afraid to go to hospital or government facilities. If we had stopped providing contraception and post-abortion care, it would have been much worse.”
 A briefing paper, published by the UN population fund last month, warned that the availability and distribution of contraceptives and other maternal health medicines were already being hampered by the pandemic. “This could have life-threatening consequences and reverse recent gains to ensure universal access to sexual and reproductive health,” it said.
 https://www.theguardian.com/global-development/2020/apr/03/coronavirus-crisis-may-deny-95-million-women-access-to-family-planning
