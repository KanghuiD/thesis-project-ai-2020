Democrats scramble to refocus their health platform on the coronavirus
 By Lev Facher @levfacher 
 April 10, 2020
 WASHINGTON — Democratic groups are scrambling to recalibrate the party’s health care messaging for the 2020 campaign to highlight the Trump administration’s response to the coronavirus pandemic.
 Already, advocacy groups have aired commercials in Florida, Michigan, Pennsylvania, and Wisconsin attacking Trump for downplaying the crisis in its early stages and waiting too long to ramp up the procurement of critical supplies like test kits, masks, and ventilators. They have commissioned new opinion polls, retooled campaign events, and hired new staffers with expertise in public health. Joe Biden, the de facto Democratic nominee for president, has attacked Trump in a nationwide advertising campaign centered on the argument that the president is “incapable of leading during a crisis.”
 It’s a sweeping overhaul of the policy foundations that have been central to the Democratic Party’s electoral hopes in 2020. The shift comes seven months before the November election, and follows a primary season almost myopically focused on issues like Medicare and prescription drug prices.
 The Democrats’ hairpin turn stands in contrast to the Republican Party’s efforts to prevent the 2020 campaign from becoming a referendum on Trump’s response.
 The Democrats’ messaging seeks to make the dramatic case that Trump’s re-election would cost American lives.
 “If you want to actually protect America, and protect American lives, the president needs to see this in a political sphere,” said Leslie Dach, who heads the Democratic Party-aligned health advocacy group Protect Our Care. “It’s what he responds to.”
 “At a time of extreme crisis, Michigan families need a steady and trusted leader, but Donald Trump has failed that test,” one ad argues.
 The progressive advocacy group Priorities USA targeted those three states, as well as Florida, with its own $6 million ad blitz. Those ads quote Trump’s refusal to accept blame for major shortages in testing kits as the crisis escalated. They highlight the now-familiar quote when the president was asked about the shortage and told reporters: “I don’t take responsibility at all.”
 An obvious challenge for Democrats is they are overshadowed — at least on a national level — by the daily televised briefings from Trump and his team.
 Trump has repeatedly boasted in recent days that the briefings yield sky-high television ratings. A survey from Navigating Coronavirus, a progressive polling group formed to conduct opinion surveys on Trump’s response, showed that 93% of Americans, in some form, have watched parts of Trump’s daily coronavirus press briefings. With congressional and presidential campaigns grounded, the ensuing airtime gap has spurred the Democratic Party infrastructure into action.
 A roundup of STAT's top stories of the day.
 Navigating Coronavirus last month also hired Ian Sams, formerly the press secretary for the presidential campaign of Sen. Kamala Harris (D-Calif.), as a senior adviser. For the past month, the group has conducted regular polling, which in the past week has shown approval for the administration’s response declining as the country’s death toll has surged.
 “This is tapping into real public anxieties about what’s going on right now and trying to put fact-based information — Trump’s own words on his handling of the crisis — in front of voters,” Sams said.
 Across the country, individual Democratic candidates have also sought to use the coronavirus to highlight long-touted policy ideas that they argue are more important than ever in light of the pandemic.
 Democratic candidates have lambasted other GOP efforts, including an ongoing lawsuit that would repeal the Affordable Care Act entirely, and touted the bill House Democrats passed in December that would slash prescription drug prices.
 “People who have been laid off and couldn’t previously afford their insulin definitely cannot afford their insulin now,” said Rosemary Enobakhare, the campaign director for Health Care Voter, a left-leaning group working to spotlight health care as a 2020 campaign issue.
 In light of the crisis, Health Care Voter scrapped an event focused on drug pricing in favor of a virtual event focused on “the failure of the Trump administration” to respond to the crisis.
 Some Democratic operatives said in interviews that campaigns would not hesitate to recall controversial advice from some elected Republicans. Many cited the words of Texas Lt. Gov. Dan Patrick, who in a television interview last month suggested the elderly should assume some risk of contracting coronavirus for the sake of re-invigorating the economy.
 Lucinda Guinn, executive director of the Democratic Congressional Campaign Committee, pointed to Republican lawmakers who have dodged questions about whether to reopen Affordable Care Act exchanges to allow recently unemployed Americans to enroll in new health insurance plans — and the ongoing Republican lawsuit to repeal the Affordable Care Act, which attorneys general have pledged to continue despite the pandemic.
 She cited two races: That of Rep. Rodney Davis (R-Ill.), who has dodged questions about whether Trump should reopen the Affordable Care Act exchanges, and Kathaleen Wall, a Texas Republican who recently apologized for a television ad last week that referred to the “Chinese virus” and argued China’s government “poisoned our people.”
 “Whether you are the Rodney Davis version of removing health care from millions of Americans, or you’re the Kathaleen Wall version of removing health care from millions of Americans and being racist while you’re doing it, the juxtaposition between our candidates looking out for their communities and this continued desire to undo the progress we’ve made on health care is very stark,” she said, referring to GOP candidates’ desire to repeal the Affordable Care Act.
 The Democratic attacks have drawn pushback from Republican operatives who see the commercials as politicizing a pandemic as tens of thousands of Americans fight for their lives. GOP aides have also criticized Democrats’ advocacy for health policies like Medicaid expansion and others not directly related to the pandemic response.
 “Democrats are again trying to use a pandemic to push their socialist ideas,” said Chris Pack, the communications director for the National Republican Congressional Committee. “Unlike the Democrats, we aren’t politicizing this.”
 The NRCC, however, has attacked Democratic lawmakers who held up roughly $250 billion in aid for small businesses, citing the need to provide additional funding for hospitals even as Congress continues to distribute $100 billion in previously approved hospital aid.
 Already, the escalating death toll — and, to a degree, Democrats’ advocacy — has sparked a shift in public opinion.
 A CNN poll this released this week showed Trump with a 52% disapproval rating for his handling of the crisis, up from 48% a month prior. The federal government overall fared worse than Trump, with 55% of respondents expressing disapproval for its response.
 Unlike many Democratic groups, Biden has been measured in his criticism of Trump. Asked in a recent interview whether the Trump administration’s pandemic-response missteps meant “there was blood on the president’s hands,” Biden replied: “I think that’s a little too harsh.”
 And Rep. Jim Clyburn (D-S.C.), the third-ranking Democrat in the House of Representatives, said last week that a new special committee empaneled to oversee Covid-19 relief would not investigate the Trump administration’s early response. The committee, he said, would be “forward-looking.”
 Only Sen. Bernie Sanders (I-Vt.), who announced Wednesday he would suspend his campaign for the Democratic nomination, has said what Biden refused to, telling ABC’s “The View”: “I think his inaction has cost the lives of many, many Americans.”
 Washington Correspondent
 Lev Facher covers the politics of health and life sciences.
 campaigns
 Coronavirus
 Donald Trump
 White House
 Republish this article
 this entire pandemic is now a political football game. we voters are merely spectators from home watching the politicians throwing anything against the wall to see if it sticks! stay home and watch the economy crash! no replay there. keep everyone on extended unemployment benefits! first down democrats. this a game trump can’t win for losing!
 It is 100% correct to criticize the bully-style ignorant huge delay in any action against Covid-19 as displayed by the current President. If Americans fail to see that this man is the direct cause of a unnecessary high death toll, then the nation is doomed. There have to be functioning brains somewhere, to put a stop to the dictatorial reign of this destructive self-centered prick.
 Please, let’s spend less time and energy reminding Americans that Trump is a poor leader….
 And find one who is!!!!
 Unfortunately, this election will come on the heels of the worst national situation we’ve yet faced
 Any candidate who does not have Covid 19 on their resume will seem under qualified
 Trump, despite his poor leadership skills and dangerously ignorant calls during this crisis, will still be able to claim war time leadership as one of his “talents”
 Crazy as it may seem, once we are on the flat side of the curve, there are people who will be grateful to Trump for being safe and see him as the savior
 It’s human nature 
 Please Governor Cuomo for President
 Our country needs you!!!!!
 I found out how this president is woefully incapable of understanding health issues. While Obama started a campaign to end pain medication in 2011, Trump has taking this idiocy to a whole level, allowing dea, cdc, fbi to scare doctors into not prescribing the pain meds they desparaetly need. Trumps efforts has succeded in most of the lower 48 in eliminating now he’s started in Alaska. This is how I learned how he.has no empathy for folks, like me, who suffer chronic pain issues. When the Covid 19 hit, he denied it’s deadly effect. I watched him on OAN news say how many are going to be effected? He says idiot things then takes it back. He touts that he closed entry to travelers from China….TOO late! He thinks about Money not us. weeks ago he said we need to go back to work and get the economy going. Not thinking of how it will kill many more of us. Elections are comming. There is not great choices for us to elect. Listen to our governors and mayors who aren’t fueled by the almighty dollar. (Some). Listen to yourself. We are fighting for our lives from this vile disease.
 So should Trump. For tax fraud and for inciting hate.
 By Nicholas Florko 
 By Ed Silverman 
 By Ike Swetlitz 
 A roundup of STAT’s top stories of the day in science and medicine
 https://www.statnews.com/2020/04/10/democrats-2020-health-platform-trump-coronavirus/