CVS plans free coronavirus testing program; 1,000 U.S. locations
 					
 			Trending:		
 CVS announced a new plan Monday to significantly boost its coronavirus testing capacity nationwide.
 The pharmacy is aiming to open 1,000 coronavirus testing locations across the United States by the end of May, CVS said in a Monday press release. The company hopes to process about 1.5 million tests per month, provided supplies and lab capacity are available.
 Customers will use self-swab tests after setting up appointments online via the CVS Minute Clinic portal. After dropping their test off, they should expect to receive a call from CVS with their results within a few days.
 A spokesperson said the company will roll out the testing to MinuteClinic locations first. There are about 60 such stores in California, including locations in Burlingame, Campbell, Cupertino, Danville, Mountain View, San Francisco, San Jose, San Mateo, Santa Clara, Sunnyvale and Walnut Creek. You can find a location on the CVS website, though testing is not available yet in California.
 Related Articles
 			Watch live: Sunset at Land’s End in San Francisco		
 			Health workers rush to prevent outbreak after Oakland homeless camp resident tests positive for COVID-19		
 			Reversing course, Trump delivers coronavirus briefing		
 			Trump says he takes no responsibility for cleaner misuse		
 			Freed from lockdown, New Zealanders hit fast food, coffee joints		
 The pharmacy is currently conducting testing in Connecticut, Georgia, Massachusetts, Michigan and Rhode Island. Its first location opened in Massachusetts on March 19.
 The sites will be drive-up only, meaning those on foot cannot be tested. Anyone who wants to be tested must set up a time and location, then bring proof of identity and state residence when they arrive. They don’t need a doctor’s note but must meet the CDC’s guidelines for seeking testing, which you can read here.
 CVS did not specify which of its Minute Clinic locations would be up and operating first — the company is working with federal and local governments to determine that timeline.
 OK
 https://www.mercurynews.com/2020/04/27/cvs-plans-to-unveil-free-drive-up-coronavirus-testing-program-at-1000-u-s-locations/
