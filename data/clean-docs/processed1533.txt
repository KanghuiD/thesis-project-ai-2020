Op-Ed: My family is separated by coronavirus. But with far-flung loved ones, the world doesn't seem so vast - Los Angeles Times
 My family is certainly abiding by social distancing rules. At the moment, its farthest-flung members are about 7,000 miles apart.
 Mom, grandma, two brothers and I are scattered around Los Angeles — Virgil Village, Northeast L.A., Monterey Park. Dad is in Guangzhou, China, two hours by air from Wuhan, the initial epicenter of the coronavirus pandemic. Given the current state of travel between China and the U.S., we don’t know when my father will be able to come to Los Angeles — or when my mother might be able to join him there.
 COVID-19 has brought with it fears of sickness and health; it has also brought fear of China and the Chinese.
 Those fears have fueled assaults on Asians and Asian Americans: A woman was spat on in San Francisco; a father and his children were stabbed in Texas; friends of a friend were cursed at outside a Washington state hospital.
 “We should have closed the borders on you people,” the attacker said to the Seattle couple, who were there for their infant’s checkup.
 I am a product of open borders, and I hope xenophobia doesn’t keep them closed after the coronavirus threat ebbs.
 In the 1990s, my father was one of the first U.S.-based attorneys granted a license to open a firm in China. Guangzhou and L.A. are sister cities, economic and social partners. He leapt at the chance to help bring them closer together in business.
 When my parents told us we were moving from Los Angeles to China, I was so young, I thought “China” was a friend’s house.
 I was born in L.A., but from the ages of 5 to 17, I lived in Guangzhou. Since then, my family has split its time between Southern California and southern China. My childhood friends, international students like me, are also far-flung, living, working and starting families on different continents.
 When loved ones are in so many places, the planet doesn’t feel quite so vast. But even before this pandemic, the oceans and mountain ranges between us seemed to be expanding. Xenophobia has grown and there are calls for higher walls between us.
 Right now, we’re all isolating in our homes in our respective countries. When the outbreak is over, will we remain isolationists?
 Maybe it’s helpful to think of China as a friend’s house. Seven thousand miles notwithstanding, we’re neighbors in this fight.
 Allison Hong is a page designer at the Los Angeles Times.
 Get thought-provoking perspectives with our weekly newsletter.
 https://www.latimes.com/opinion/story/2020-04-12/op-ed-family-separated-by-coronavirus
