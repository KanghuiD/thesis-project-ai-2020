Coronavirus Pandemic Exposes British Snitches & Police Tyrants | National Review
 National Review
 								Easter Still Frames the Irish Response to Times of National Crisis							
 								Corona Conspiracies							
 								Sketching Out a Worst-Case Scenario for the Coronavirus Pandemic							
 								New Zealand Had Help Flattening the Curve							
 								Powerful Americans Were Catastrophically Wrong about China							
 Before our lives switched to lockdown and immobility, I was enjoying a few pints with a friend in a pub on Fulham’s bustling North End Road. We were discussing one of our favorite topics — Germany — and how we’ve both noticed that the country is infected with a rancid culture of snitching.
 By contrast, Britain, I said, can take great pride from our national attitude against tattletales, which starts in the school playground before fermenting into full-blown opposition to informing on neighbors when the police come knocking.
 He told me I was wrong, and that we’re just as ripe for the Germanic grassing (informing) that we both lamented.
 He has been proven right. The coronavirus epidemic has revealed that vast swaths of our society need little incentive to transform into curtain-twitching, proto-Stasi informants.
 A little over two weeks ago, Prime Minister Boris Johnson announced tougher restrictions on our liberty to prevent the spread of the virus. “Stay home and save lives” was his rallying cry. Unfortunately, many took this to mean “Stay home and make sure your neighbor does the same.”
 				jwplayer('jwplayer_XtkmEUkk_8HR1M1dH_div').setup(
 				{"displaydescription":false,"playlist":"https:\/\/cdn.jwplayer.com\/v2\/playlists\/XtkmEUkk?contextual=true&search=__CONTEXTUAL__&recency=30D","ph":2}
 			);
 		
 	(function() {
 		if("function" === typeof jwplayer) {
 			var jwp = jwplayer(document.querySelectorAll(".jw-player-container div.jwplayer")[0]) || null;
 			if(jwp) {
 				var episode = jwp.getConfig().playlist[0];
 				var pubdate = new Date(episode.pubdate * 1000);
 				var prelimMonth = pubdate.getUTCMonth() + 1;
 				var month = prelimMonth.length > 1 ? prelimMonth : "0" + prelimMonth;
 				var prelimDate = pubdate.getUTCDate();
 				var date = prelimDate.length > 1 ? prelimDate : "0" + prelimDate;
 				var finalDate = pubdate.getUTCFullYear() + "-" + month + "-" + date;
 				var title = episode.title;
 				var labelMapping = "c3=\"nationalreview\", c4=\"*null\", c6=\"*null\", ns_st_st=\"National Review\", ns_st_pu=\"National Review\", ns_st_pr=\"" + title + "\", ns_st_ep=\"" + title + "\", ns_st_sn=\"*null\", ns_st_en=\"*null\", ns_st_ge=\"News\", ns_st_ia=\"0\", ns_st_ce=\"0\", ns_st_ddt=\"" + finalDate + "\", ns_st_tdt=\"*null\"";
 				jwp.on("ready", function () {
 					ns_.StreamingAnalytics.JWPlayer(jwp, {
 						publisherId: "23390304",
 						labelmapping: labelMapping,
 					});
 				});
 			}
 		}
 	})();
 Northamptonshire — which I now rank as one of England’s bottom three counties — has reported a thriving snitching community, with its chief constable, Nick Adderly, recently revealing that the force has faced a surge in calls from people reporting their neighbors for “going out for a second run.”
 Not wanting to miss out on an opportunity to embarrass themselves in public, many of our overzealous police forces have launched online contact forms so people can grass on their neighbors. Some of these portals were launched because police control rooms were “inundated” with calls from nosy neighbors. It seems many are only too keen to fill them out when they spot someone sneaking out for a quick stroll.
 Cambridgeshire police tweeted: “We have now developed an online form allowing you to report individuals breaching Covid-19 restrictions via our website.” The account shared a webpage where people can give details about a person or organization ignoring the rules.
 And they’re not alone. A portal from Humberside police asks you to give details on people who are “not following social-distancing rules.” Police forces in the West Midlands, Greater Manchester, and Avon and Somerset have launched online forms and phone lines to clamp down on people gathering.
 So, is it just a few reporting portals, set up so the police can satisfy the all-seeing informants who want to “do their bit” in the fight against the virus? It is with profound regret that I can report that it gets much, much worse.
 How quickly we have lapsed into statism.
 Police have also taken to cycling around London parks and telling off citizens for enjoying “non-essential exercise,” such as a brief stroll and rest in the sun in a park away from their cramped and miserable flats. Just last week, a woman was fined £660 for being at Newcastle Central train station and “failing to provide identity or reasons for travel to police.” She soon found out that this crime doesn’t exist.
 This is intolerable. British police are governed by the beautiful and freedom-preserving philosophy of policing by consent, which refers to the Peelian principles. Named after Prime Minister Robert Peel — the “father of modern policing” — the nine principles were issued to every officer from 1829 onward.
 For reasons of succinctness, I won’t list them all. But my favorite is the fifth, which commands policemen to “seek and preserve public favour, not by pandering to public opinion; but by constantly demonstrating absolutely impartial service to law.”
 They have failed this principle. A host of popular media figures has been demanding tighter restrictions, with shock jock extraordinaire Piers Morgan — who once polluted American screens on his ghastly CNN show — being particularly vociferous. He has regularly called people relaxing in London parks “traitors” and urged severe punishment.
 For reasons far beyond the capacity of my imagination, Morgan is a popular figure. His voice is one of many in a particularly forceful chorus calling for the police to be sharper. The police have pandered to the authoritarian urges of public opinion, failing to abide by impartial service to the law.
 One gentleman named Keith phoned a BBC radio call-in show last week and swiftly demanded that the police shoot people going to sit in the sun for a few moments. The presenter quickly moved on from his contribution in typically embarrassed English style, but I am sure there were thousands of listeners nodding along, possibly remarking upon what a smart and insightful proposition Keith had offered. Let’s hope that the police at least fail to sway to this particular strand of public opinion.
 Is this attitude here to stay? Quite possibly, which means I am desperately longing for when I can get back to the pub to drown my sorrows.
 Get our conservative analysis delivered right to you. No charge.
 https://www.nationalreview.com/2020/04/coronavirus-pandemic-exposes-british-snitches-police-tyrants/
