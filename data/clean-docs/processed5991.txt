Opinion
 Wednesday
 22nd Apr 2020
 By March, the emergency had forced every government in Europe into an impossible choice - letting many people die and health systems collapse, or ground much of public life and inflict massive harm on their economic lives.
 Four months into the corona crisis and one month into the social and economic shutdown, it seems the big geopolitical loser of the pandemic is likely going to be Europe.
 We are experiencing the first global pandemic unfolding in the 24/7 news cycle and taking its toll, in real time, on our daily lives, our financial security and the global economy.
 The new EU naval mission to prevent arms-smuggling to Libya has clearly ignored Libya's reality and all expert advice.
 Right now, we are about halfway between utopia and dystopia. This is our watershed moment.
 After the failure of the European Council, a dynamic compromise between Germany and the nine states advocating 'coronabonds' is urgently needed.
 Covid-19 is no excuse to allow authoritarian minds more leeway. While Polish government uses the pandemic to conduct unfair elections, EU countries must see it as an existential political threat alongside the health and economic crisis.
 'As a single mother, I am now caring for my severely disabled son alone, 24/7, without any assistance, without rest, with more demand for shopping, cooking, feeding and intensive care.'
 Welcome to the Hobbesian jungle of international anarchy.
 As president of the Italian region of Emilia-Romagna and of the Council of European Municipalities and Regions, I have witnessed firsthand the efforts and sacrifices of our doctors, nurses, police officers, waste collectors, civil servants, volunteers and countless others.
 An average global temperature rise above 1.5 or 2°C creates risks society cannot handle. This dwarfs the fallout of Covid-19.
 From Iran, to Egypt, to Saudi Arabia, to Algeria, to Turkey, to Thailand, and even within the EU bloc with Hungary, the coronavirus pandemic is providing cover for authoritarian leaders to dispense with democracy - and even eliminate opponents.
 It is still very likely that Europe will face a new deadly spread of the virus next autumn or winter. Until a reliable vaccine and cure are in place, we all have to live in this new reality.
 The rise in support for mainstream parties has been paired with stagnation or decline for far-right populist parties and figures - the AfD has dropped to 10 percent in Germany and Marine Le Pen and Matteo Salvini are treading water.
 In repointing Europe's approach to industrial policy, some policymakers have prized China as an example to follow. Luckily, the European Commission is moving towards a European approach.
 The 1948-51Marshall Plan provided about €118bn in today's figures in American assistance to European countries. These numbers are dwarfed by prospective needs, and the needs are not just European or American - but global.
 World food supplies - an urgent message from the director-general of the UN's Food & Agriculture Organization.
 Millions of gig economy workers have been left abandoned by companies during the coronavirus outbreak. Workers have complained that gloves, masks, or gel have not been made available.
 Clear-headed thinking becomes nearly impossible under this relentless barrage of bad news and apocalyptic analysis, Ferraris writes - a state of mind he describes as "cogito interruptus".
 As doctors in Europe, we call on the leaders of our governments and of the European Union to take immediate action to ensure the safety of refugees.
 Declaring a state of emergency is not even an option on the table for Poland's Law and Justice (PiS) chairman Jarosław Kaczyński - as it would render the 10 May election impossible.
 When one day the coronavirus nightmare is over, we should not wake up in an authoritarian state where individual freedoms have dwindled.
 As Li Wenliang, the deceased Chinese doctor who was reprimanded for reporting on the virus, said: "There should be more openness and transparency".
 The coronavirus crisis resembles a medieval morality play. Either save as many people as possible - or keep the economy going at the cost of thousands of lives.
 Italy is not the only European country turning to China for help in fighting the crisis, and the Chinese authorities consistently use European requests for self-aggrandisement. Europe should be wary of being used to advance an authoritarian state's propaganda.
 At the moment, Europe clearly lacks a true moral leadership and coordination to deal with this emergency. We, members of Volt Belgium, call on you to stand up and show more solidarity and unity across the EU.
 There is no blueprint for this crisis and in view of the rise of populism, Germany may be lucky to have an experienced leader who can actually read and understand scientific briefings.
 The numbers that the World Health Organisation publishes, the numbers that journalists and governments around the world refer to, are contaminated with politics. They are not useless, they tell us something, but they paint a skewed picture. How so?
 As citizens are urged to self-quarantine and wash their hands with soap and warm water, what if there's nowhere to hide, if you live in an overcrowded site or shanty-town, and don't have access to clean water and sanitation?
 The Aether theory made a lot of sense, it was beautiful, even poetic. Its only problem was that it was wrong. There is no such thing as aether out there in the cosmos.
 Falling demand and prices for oil and raw materials will revive the risk of deflation. The collapse in international trade and long-term rethinking of China's role as the major hub for the production of consumer goods and electronics is inevitable.
 Covid-19 showed how little it means to be European in times of crisis. But it makes one thing clear: the eurosceptic mantra of the 'European Superstate' becoming more ridiculous by the day.
 By 18 March, trade commissioner Phil Hogan wants to sign a deal with the Trump administration which, to add a bit of spice, includes fast-tracking GMO imports in an attempt to please the US farming industry.
 The EU's only response so far is to work with Greece to strengthen the border into south-east Europe - a short-term measure which fails to deal with Turkey's intention to pressure the EU into supporting its wider agenda in Syria.
 The EU's new Africa strategy promises relentless support for "a comprehensive continent-to-continent free trade area" - and ignores the risks posed by trade liberalisation where labour, fiscal and social regulation is immensely diverse and sometimes weak.
 An open message from the Chinese ambassador to the EU.
 https://euobserver.com/opinion