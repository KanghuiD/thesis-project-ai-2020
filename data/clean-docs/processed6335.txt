Opinion columns and debate | Dezeen
 While converting existing buildings into fully functioning hospitals is unfeasible, high schools and large hotels can be used as care space for coronavirus patients, says Jason Schroer, who is director of health at HKS's firm in Dallas. Here he explains how. More
 The coronavirus has exposed fundamental problems with our healthcare systems that we should not forget once the pandemic passes, warns Reinier de Graaf in a letter to the present from the future. More
 The Pratt Institute has launched an archival project that pays tribute to women in architecture. Following a celebratory dinner party held in March, the school's dean of architecture Harriet Harriss explains why projects like the Mistresses Archive are vital to achieve gender parity in the profession. More
 Two new graphic novels, which a Le Corbusier figure makes an appearance in, depict the uncomfortable side of modern architecture, writes Owen Hatherley. More
 Coronavirus has changed the relationship between cities and their rural hinterlands, writes Mimi Zeiger, casting new light on the essays accompanying Rem Koolhaas' now-shuttered Countryside exhibition. More
 As people around the world face the realities of self isolation, Ukrainian architect Sergey Makhno predicts how our homes will change once the coronavirus pandemic is over. More
 The mass work-from-home experiment imposed on us by coronavirus could finally force companies to embrace remote working, says Tom Ravenscroft. More
 Aaron Betsky, president of Frank Lloyd Wright's School of Architecture at Taliesin, is leaving the post after a bitter fight to save the experimental institution. Here he details the behind-the-scenes battle to save the school. More
 The Countryside, The Future exhibition at the Guggenheim says more about the shortcomings of its curator Rem Koolhaas than it does about the countryside, argues architecture critic Inga Saffron. More
 Jane Hall's Breaking Ground aims to rectify gender inequality by singling out female architects from their male counterparts. But, ahead of International Women's Day this year, Mimi Zeiger argues this corrective project could do more harm than good. More
 The upset caused by the resignation of Cooper Hewitt director Caroline Baumann is the least of the design museum's problems, says Aaron Betsky. More
 A draft order by Donald Trump to make all new federal buildings classical is the latest example of how traditional architecture is used to disguise racist agendas, says Phineas Harper. More
 Following the news that Frank Lloyd Wright's School of Architecture at Taliesin will close this year, first-year student Alex Martinec reflects on how it marks not just the end of a physical institution, but the architect's way of thinking. More
 The emotional and economic impacts of cities are closely connected, but this is lost in a proliferation of meaningless phrases like "healthy placemaking" and "human-centric design", says Reinier de Graaf. More
 Interior design will evolve, innovate, adapt and improve in 2020, says Michelle Ogundehin. Her trend report includes provocative collaborations, Ukrainian design and a rejection of digital technology. More
 As the 2020s begins, Aaron Betsky predicts that architects in the new decade will focus on reuse, flexible spaces and earthy materials. More
 High-tech architecture aligned with the revival of Victorian values in the 1980s, but did not end up producing the factories of a new period of British creativity, says Owen Hatherley. More
 Holly Brockwell is unimpressed by the sexist advertising for the gold folding phone launched by Pablo Escobar's brother, but is not surprised. More
 Pantone played it safe and still missed the mark by picking Classic Blue as its colour of the year for 2020, says Michelle Ogundehin. More
 Continuing our high-tech architecture series, Catherine Slessor looks at how the women in Team 4 helped shape the narrative of the movement. More
 Dezeen Daily is sent every day and contains all the latest stories from Dezeen.
 https://www.dezeen.com/opinion/
