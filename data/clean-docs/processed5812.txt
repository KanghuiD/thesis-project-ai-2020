Website Launches to Document Anti-Asian Hate Crimes in Wake of COVID-19 – NBC Bay Area
 On a busy San Francisco street in the broad light of day,
 Yuanyuan Zhu was harassed and spit on by a stranger simply for being Asian in
 the midst of a global pandemic caused by what the president of the United Stats
 calls the "Chinese virus."
 Zhu had just been dropped off near the corner of Van Ness
 Avenue and Geary Boulevard when a man walking past her on the sidewalk shouted
 "f*** China" while staring directly at her. 
 She immediately thought to get away from him by crossing the
 street, but there were too many cars and the light was against her. 
 Instead she tried to keep her distance, hoping that was the
 end of his outburst.
 But as a bus rolled past, the man again yelled out, this
 time screaming at the driver to "run them all over," Zhu said. 
 He turned to her and she knew it was coming. 
 "Please don't," she said. 
 "Right after I said that, he spit on me," Zhu
 said. "I didn't really know what to do."  
 Zhu made it to the safety of her nearby gym before the man
 was able "to do something else," she said, but the experience left
 her shaken, terrified. 
 And while she's experienced overt racism in the past, Zhu
 says the environment that's emerged in the wake of the novel coronavirus is particularly
 hateful. 
 "Lots of people around me have encountered this same
 thing recently," she said, noting that many of her friends now fear
 wearing the now-ubiquitous protective face masks for fear of being singled out
 as Asian and potentially sick. 
 "I'm angry and sad and don't know what to do," she
 said. "It's getting really, really crazy."
 Zhu is grateful, however, for a new Internet-based tool
 built to collect reports of attacks, harassment and discrimination.
 In an effort to quantify and combat hate crimes targeting
 Asian Americans and Pacific Islanders stemming from people's reaction to the
 novel coronavirus pandemic, a coalition of civil rights groups has set up a web
 page where victims can report such incidents.
 Since the December outbreak of the virus in Wuhan, China,
 the infection has grown to more than 207,000 confirmed cases and more than
 8,500 deaths worldwide, according to numbers reported Thursday by the World
 Health Organization. 
 As the COVID-19 disease continues to spread with alarming rapidity,
 countries all over the globe are taking drastic measures to slow its growth and
 in many places, including the Bay Area, people are responding by panic-buying
 food and hoarding household supplies.
 A more sinister trend, however, also appears to be emerging:
 violence and harassment directed toward people of Asian descent living in the United
 States. 
 In a particularly harrowing case, a Los Angeles boy was
 recently attacked at his middle school and accused of having the coronavirus, according
 to the "Stop AAPI Hate" webpage set up by three organizations -- the
 Asian Pacific Policy and Planning Council, Chinese for Affirmative Action and
 the Department of Asian American Studies at San Francisco State University.
 "We are currently providing support to a child who had
 to go to the emergency room after he was assaulted and accused by bullies of
 having the coronavirus, and so that tells us we may need to work with schools
 to address shunning and school bullying but we need to know how widespread it is,"
 said Manjusha Kulkarni, executive director of A3PICON. 
 The website hosts a form that people who have experienced
 similar incidents can fill out and includes information such as the victim's ethnicity,
 the location and date of the incident, the type of discrimination and the
 suspected reason for it. 
 The information will help advocates push for stronger
 protections for Asian American and Pacific Islander populations, develop public
 education campaigns and provide resources to victims of attacks and
 discrimination, according to a Thursday news release announcing the new
 website.
 Kulkarni's concerns were echoed by a bevy of elected
 officials from around the Bay Area and throughout the state, several of whom
 labeling COVID-19 a "Chinese virus." 
 "COVID-19 is a public health issue, not a racial one.
 Calling it a 'Chinese virus' only encourages hate crimes and incidents against
 Asian Americans at a time when communities should be working together to get through
 the crisis," said California Assemblyman Phil Ting, D-San Francisco.
 "I encourage victims to confidentially input their
 encounters on the new reporting site, so we can monitor the situation and
 provide support to those impacted," Ting said.
 "In times of crisis, we need leaders who work to
 support all of us. A virus does not discriminate based on race and
 ethnicity," Becerra said. "Xenophobia has no place in our
 society."
 Zhu, said that even though she still fears walking the
 streets after the attack and that she sometimes wears a hat and sunglasses so
 people can't easily identify her as Asian, she's thankful for the chance to
 report her story in a way that can have real impacts. 
 "I'm really happy that there is this platform for me to
 speak up and to raise awareness," Zhu said. 
 "I want people to know about this and I don't want it
 to happen to other people," she said.  
 The Stop AAPI Hate webpage can be found here: http://www.asianpacificpolicyandplanningcouncil.org/stop-aapi-hate/
 https://www.nbcbayarea.com/news/coronavirus/website-launches-to-document-anti-asian-hate-crimes-in-wake-of-covid-19/2258297/
