India coronavirus: Should people pay for their own Covid-19 tests? - BBC News
 Thyrocare, a private diagnostic laboratory in India, had just started testing for Covid-19, when the Supreme Court ordered all tests to be carried out free. 
 “We thought the order would say the rich would pay, and the government would pay for the poor,” says Arokiaswamy Velumani, Thyrocare’s founder. 
 At 4,500 rupees ($59; £47), it’s not a cheap test. But the court did not clarify if and how private labs would be reimbursed. Panic stricken, some, including Thyrocare, put testing on hold. 
 An anxious federal government petitioned the court to reconsider - which it did.  
 According to the new order, issued on 13 April, the government will reimburse private labs for testing the 500 million people covered by a flagship public health insurance scheme. The rest would have to pay.
 But the volte-face sparked a bigger question: can India scale up testing for Covid-19 if it’s not free?
 A steep price tag
 India's numbers - 15,712  active cases and 507 deaths - are relatively low for a country of 1.3bn. Many believe this is because it's still testing too little - as of Sunday there had been 386,791. 
 But scaling up is a challenge. The Indian Council of Medical Research (ICMR) has approved only one homegrown testing kit so far, imports are delayed because of a global surge in demand, and the protective gear and medical staff required to conduct tests are in short supply. Also the sheer size of India’s population, and the resources needed to reach every corner of the country, is daunting. 
 All of this has made testing expensive. It’s free at government hospitals and labs - and for months they were the only ones permitted to even test for coronavirus. But soon private players were roped in to support India’s underfunded and struggling public health system.
 The government capped the price of a test at 4,500 rupees at home, or 3,500 rupees in a hospital, based on the recommendations of an expert committee including heads of private health firms. 
 But the figure, says Malini Aisola, from the All India Drug Action Network, a health sector watchdog, is "arbitrary". One virologist said when he calculated the cost, it worked out to around 700 rupees. 
 "If the private sector was part of the process of deciding the cost, the government should release the breakdown,” argues Ms Aisola.
 But private lab owners say it’s a fair price. “The supply chains are clogged - everyone is working on advance payments,” says Zoya Brar, founder and CEO of Core Diagnostics. 
 She says that the basic RT-PCR test kit - widely used to diagnose HIV and influenza - costs around 1,200 rupees. And this is supplemented with an extraction kit, used to pull DNA and RNA, another kind of genetic code, from the sample.  
 “This is in short supply and when it’s available, we’re getting it for around 1,000 rupees, which is a blessing.” And then, she adds, there are the overheads - personal protective equipment (PPE) for staff; employees’ salaries; and the cost of running the lab overall. 
 Thyrocare's Dr Velumani says he is also paying higher salaries than usual because staff are being pressured to stop working by their families who are scared they may contract the virus.
 The case for free testing
 Right now, Indians are getting tested only if a doctor advises them to do so. But the long wait at government hospitals, and the prohibitive cost at private ones, could deter even those with symptoms from showing up. 
 “If you want to contain a pandemic, you can’t have testing determined by cost,” says Jayati Ghosh, an economics professor at Delhi’s Jawaharlal Nehru University.  
 And making it free only for the poorest Indians doesn’t help either, according to some economists. 
 “There’s a big chunk of people just above the poverty line that are also struggling and there are middle-class workers who have been laid off and can’t afford to pay for their families to get tested,” says Vivek Dehejia, an economist. 
 More importantly, the asymptomatic nature of the virus in many people means that India may soon have no choice but to begin mass testing. 
 “If you really want to push up testing rates across the country, you can’t expect everyone to pay out of their pockets - for themselves and their families - especially if they’re not showing any symptoms,” Mr Dehejia says.
 Singapore and South Korea have both been lauded for their aggressive testing, which is funded by the government. Vietnam, perhaps more comparable to India, has focused more on isolating infected people, but the government still foots the bill for testing. 
 “You cannot contain the virus unless you know who has it,” says Prof Ghosh. “So it’s in your interest to make sure there is universal access [to testing].”
 Who should pay?
 Economists who spoke to the BBC made a range of suggestions - including employers chipping in, and insurance covering it - but all seemed to agree the government should do more. 
 Already, it is paying for the lion’s share of testing, however, Mr Dehejia says it should "encourage and subsidise free testing".
 “You cannot rely on private charity to get you out of an international public health emergency.”
 But India's health sector is poorly funded - it receives just about 1.3% of GDP - and is largely unregulated. Health insurance is not mandatory, and the market is fragmented - most policies cover hospitalisation but not diagnosis or medicines. 
 And now with private hospitals in the mix, it’s going to be harder for the government to retain control of its testing strategy. A prominent hospital chain has just made testing mandatory at the time of admission, which runs counter to current guidelines, recommending testing only for those with symptoms or who have come in contact with a positive case. 
 Of course, testing could become cheaper as more homegrown kits are approved, and supply outstrips demand. Some states are also experimenting with standardised collection points - such as mobile centres or kiosks -  which means fewer PPE suits and lower transport costs.   
 India is also considering pool testing, which involves collecting a large number of samples and testing them in one go. If the test is negative, nobody has the virus but if it’s positive, everyone who gave a sample has to be tested individually. 
 “It’s definitely a good way to reduce costs - as long as it’s done efficiently and smartly,” Ms Brar says. 
 But the more immediate solution, she says, is to perhaps regulate prices.  
 “If you can fix the price of the raw materials, you can fix the overall price.” 
 Germany is among several European countries easing the lockdown as rates of infection slow.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-asia-india-52322559
