	Stocks ignore economic calamity to push higher
 Han Tan
 ForexTime (FXTM)
 Follow
 Following
 Equities heartened by incoming support measures.
 Rebound may turn out to be a false dawn.
 Investors should remain vigilant.
 Asian stocks are set to enter the weekend on the back of four consecutive days of gains, after their US counterparts extended their advances overnight. It’s taken just two weeks for the Dow Jones index to go from a bear market to a bull market, at least technically, while the SP 500 recorded its steepest 3-day climb since 1933!
 Stock markets appear content to dismiss signs of the economic carnage left in the coronavirus’ wake for now,even ignoring the record high US jobless claims data which rose by over three million during the span of a single week.This print is the highest on record by a factor of nearly five, and up over 1000% on the week before. The delivery of economic rescue packages and unlimited quantitative easing has apparentlybeen enough to send equities higher for the time being.
 Gains in equities this week do not truly reflect market confidence that the coronavirus outbreak has peaked and that the economic turmoil is over. While the stimulus measures being rolled out around the world can mitigate the initial negative fallout from the coronavirus outbreak and help support the eventual recovery, Covid-19’s economic toll could be more severe. It is this that would then test policymakers’ ability to push against the virus’ drag on their respective economies.
 It remains doubtful whether the recent surge in global equities can be sustained. The wild price swings put an artificial gloss over valuations, with prices disconnected from the economic realities of the coronavirus impact.This rebound in stock markets may eventually prove to be a false dawn, one fueled by volatility rather than fundamentals and we feel thiswarrants cautionand vigilance on the part of investors.
 Risk Warning: CFDs are complex instruments and come with a high risk of losing money rapidly due to leverage. 90% of retail investor accounts lose money when trading CFDs with this provider. You should consider whether you understand how CFDs work and whether you can afford to take the high risk of losing your money.
 Access to real-time signals, community and guidance now!
 EUR/USD is trading closer to 1.0850, down from nearly reaching 1.09 as the US dollar is staging a comeback and markets marginally cool down. US Conference Board Consumer Confidence tumbled to 86.6, within expectations.
 GBP/USD is trading around 1.2450, holding onto only some of its gains. PM Johnson is contemplating the next steps in the battle against coronavirus amid improving statistics. US Consumer Confidence is due out later on.
 The crypto market is at a crossroads that will determine the path to new historic highs. Ambiguity at a technical level is absolute and the market does not have much time to choose the way forward. Sentiment indicators remain at high-fear levels.
 Gold struggled to capitalize on its goodish intraday bounce from four-day lows and was last seen trading in the neutral territory, around the $1710 region.
 WTI (June futures on Nymex) has come under fresh selling pressure in the European session, as the bears now look to test the contract low of $6.55 on a break below the $10 psychological level.
 Trading foreign exchange on margin carries a high level of risk and may not be suitable for all investors. The high degree of leverage can work against you as well as for you. Before deciding to trade foreign exchange you should carefully consider your investment objectives, level of experience and risk appetite. The possibility exists that you could sustain a loss of some or all of your initial investment and therefore you should not invest money that you cannot afford to lose. You should be aware of all the risks associated with foreign exchange trading and seek advice from an independent financial advisor if you have any doubts.
 https://www.fxstreet.com/analysis/stocks-ignore-economic-calamity-to-push-higher-202003270753
