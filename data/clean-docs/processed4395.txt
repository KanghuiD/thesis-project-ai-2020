NHS nurse who still worked night shifts aged 84 dies of coronavirus | Metro News
 135.6m shares
 An ‘inspirational’ nurse who ‘devoted her life’ to working for the NHS has died from coronavirus aged 84.
 Margaret Tapley was still working night shifts in the weeks before her death. Her granddaughter Hannah Tapley, an athlete who has competed in the British Championships in the high jump, described her as the ‘strongest woman I’ve ever met’.
 Paying tribute, she continued: ‘I considered her as an additional parent and I’d never be able to do anything I have done without her. I am so proud to call her my grandma. 
 ‘She was the most hard working, caring and perfect woman out there. Devoting her life to others and working for the NHS doing night shifts as a nurse at her age! So many people will have such amazing memories of her as she affected so many people’s lives in such a positive way.
 ‘One of my many favourites will be how excited she always got when we saw each other and the messages she would send me before every competition to motivate or calm me down.
 ‘Everyday she would text and phone me sometimes talking about the most random things and I will miss this extremely. She was one of my biggest fans and would support me through everything.
 ‘I’m not quite sure how to adapt to this but I know she will be watching everything and supporting myself and all my family just in a different place.’
 Hannah went on to add that her grandma would ‘never ever ever be forgotten’, as she would be ‘extremely missed’.
 ‘She took huge pride in her work but was so humble. She embodied the nursing spirit. For anyone who worked with her or knew her, that spirit that we all saw and felt lives on in us.
 ‘Grandma may have been called home in what feels all too early for us left behind, but the values, spirit and giving nature that she brought to the world is carried on in us that we’re touched by her life.’
 As of today, at least 16,000 people have died from Covid-19 in the UK, while 120,067 have tested positive for the virus. 
 Lockdown means I've never met my boyfriend
 Donald Trump 'can't imagine why' more people are drinking disinfectant
 Captain Tom Moore has been sent 125,000 cards for his 100th birthday
 It is thought more than 60 nurses have died after contracting coronavirus. Earlier this week Health Secretary Matt Hancock said he hoped the government would soon be able to produce a daily death toll for NHS workers from the virus. 
 The government is facing mounting criticism over shortages of PPE and a lack of coronavirus tests for frontline health workers. 
 On Friday, Hancock said one billion items of PPE will have been shipped to the UK as of this weekend – but today 84 tonnes of vital kit, including 400,000 gowns, was delayed en route from Turkey. 
 For more stories like this, check our news page.
 Not convinced? Find out more »
 https://metro.co.uk/2020/04/19/grandma-embodied-nursing-spirit-dies-coronavirus-12578588/
