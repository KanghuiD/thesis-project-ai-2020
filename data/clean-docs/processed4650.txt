Coronavirus (COVID-19) - NHS
 Menu
 Close menu
 Back to Health A to Z
     Get the latest NHS information and advice about coronavirus (COVID-19), a new illness that affects your lungs and airways.
   
 Find out about the main symptoms of coronavirus and where to get medical advice if you think you have them.
 Advice about not leaving your home (self-isolation) and looking after yourself if you or someone you live with has symptoms.
 Advice for people at higher risk from coronavirus, including older people, people with health conditions and pregnant women.
 Advice about symptoms of coronavirus in children, including when to get medical help if your child seems unwell.
 Advice about staying at home to avoid getting coronavirus, including when you can leave your home and how to get medical help.
 Find out what you can do to help the NHS respond to the spread of coronavirus.
 Links to government advice, information for health professionals and advice for other parts of the UK.
 https://www.nhs.uk/conditions/coronavirus-covid-19/
