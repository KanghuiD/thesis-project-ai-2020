Opinion - Chicago Tribune
 Remote learning helped Chicago, Illinois and the rest of America tackle a critical mission — teaching our children in the midst of a pandemic. This fall, however, students need to be back in their schools, face to face with their teachers, in an environment that maximizes learning.
 Trump thinks testing makes our numbers worse, but it's the key to overcoming the coronavirus.
 Meat's nice. It's yummy. But it's not a "scarce and critical material essential to the national defense," as President Trump's recent order suggests.
 Victory gardens sprang up by the thousands in Chicago and its suburbs during World War II in response to a rallying call to prevent food shortages.
 How does Gov. J.B. Pritzker — and how do local governments — collect taxes from businesses he’s shut down?
 With Mother’s Day fast approaching, I’ve been reflecting on my role as a mom during this global crisis.
 Sweden's approach to the coronavirus pandemic is creating a lot of buzz. The country's ambassador to the U.S. explains what's happening there.
 Check out the latest cartoons by Tribune editorial cartoonist Scott Stantis.
 A look at the current state of politics through the eyes of editorial cartoonist Joe Fournier.
 Netflix's "Tiger King" popularity also draws public attention to the problematic big cat zoos in Illinois.
 Ultimately, this pandemic will require strong global cooperation and coordination. It's not a "war."
 A hat tip to workers at meat processing plants, doing a dirty job during the COVID-19 pandemic.
 The killing is eerily similar to Trayvon Martin's with one important difference: a leaked video. That’s the only way black victims get to tell their side of the story.
 A vaccine may be available within a year, but there is little proof that use or effectiveness will be high enough to achieve herd immunity.
 We'd like to remind Gov. J.B. Pritzker that when he issued the stay-at-home edict, he didn’t promise it would eliminate COVID-19. He said it would bend the curve.
 At a time when staying at home has been equated with staying alive, perhaps few Chicagoans face greater peril from the coronavirus crisis than those experiencing homelessness.
 Editorial: Chicago Forward — Young lives in the balance: A call to action to reach Chicagoland’s disconnected youth
 We urgently call on you to join us again in our new “Chicago Forward” campaign: “Young lives in the balance: How to reach Chicagoland’s disconnected youth.”
 Whenever restaurants do get to resume dine-in service, though, things will be different. And with help from everyone on both sides of the bill, we can push through it.
 Little-understood "certificate-of-need" regulations in many states make it difficult to expand health care capacity when a crisis hits.
 The pandemic will experience a surge in infections if Americans have to vote in person in November. Voting by mail is the way to go.
 As the number of American deaths from COVID-19 rose above 70,000, President Donald Trump issued a rallying cry for our long-quarantined country to bravely reopen so he can get reelected in the fall. "Our country wants to open,” Trump lied Tuesday. "And the people of our country should think of themselves as warriors.”
 Especially during this pandemic, it is crucial to reduce the jail population.
 John Kass: If there were ever a time that the American people needed credible national journalism, it is now, during this awful coronavirus pandemic. But the double standard over Joe Biden and Brett Kavanaugh and the lack of curiosity over Michael Flynn don't inspire much confidence.
 Column by Dahleen Glanton: Ida B. Wells, awarded a Pulitzer Prize 89 years after her death, is as relevant as ever
 https://www.chicagotribune.com/opinion/
