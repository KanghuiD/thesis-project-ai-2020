Can I drive somewhere else to exercise or walk my dog? The lockdown rules in the UK explained | Edinburgh News
 256 deaths in Edinburgh and Lothians linked to Covid-19 according to new figures
 Revealed: How closely Edinburgh residents are sticking to lockdown restrictions
 Under new government guidance, people are allowed to go out once daily for exercise - but does this include driving to a beauty spot or park to run or walk your dog?
 Here’s what you should know.
 Can I drive to take my dog on a walk?
 As lockdown rules continue to be enforced, The National Police Chiefs’ Council (NPCC) has issued guidance for people about what they can and cannot do when leaving their homes.
 The current rules being enforced by the lockdown states that there are only four reasons you should be leaving your home:
 - One form of daily exercise (which includes walking your dog)
 - Shopping for essentials
 - Travelling to and from work
 - To fulfil medical needs
 In terms of exercising, you can:
 - Go for a run or cycle, practice yoga, go for a walk in the countryside or in cities, and attend to an allotment
 - Drive to the countryside and go for a walk (where far more time is spent walking than driving)
 - Stopping to rest or eat lunch while undertaking a long walk
 What you’re not allowed to do is drive for a prolonged period of time for only a brief amount of exercise, or take a short walk to something like a park bench where you sit for a long period.
 The Environment Department has also stated that the public should “stay local and use open spaces near to your home where possible – do not travel unnecessarily”. 
 Given the current lockdown situation, you probably shouldn’t be driving to a separate location to exercise or walk your dog, but in some cases where it is necessary, it is permitted.
 Are parks closed?
 In the list released by the government, it stated: “Parks will remain open only for individuals and households to exercise once a day.
 “Communal spaces within parks such as playgrounds and football pitches will be closed.”
 The National Trust also announced that they are closing all of their gardens and parks as well due to the spread of coronavirus.
 Their statement said: “We have now closed all of our gated gardens and parks as well as our houses, cafes and shops to help restrict the spread of the coronavirus, and by the end of Tuesday 24 March, we will close all our car parks.
 “We believe it is important that people do not travel, and instead stay at home and observe social distancing measures.”
 What is essential travel?
 You should be aware of what the government defines as “essential travel”.
 After releasing the guidelines on social distancing, the government also said that people should avoid travelling unless it’s essential.
 The statement reads: “Essential travel does not include visits to second homes, camp sites, caravan parks or similar, whether for isolation purposes or holidays.
 Coronavirus: the facts
 What is coronavirus?
 COVID-19 is a respiratory illness that can affect lungs and airways. It is caused by a virus called coronavirus.
 What caused coronavirus?
 The outbreak started in Wuhan in China in December 2019 and it is thought that the virus, like others of its kind, has come from animals.
 How is it spread?
 As this is such a new illness, experts still aren’t sure how it is spread. But, similar viruses are spread in cough droplets. Therefore, covering your nose and mouth when sneezing and coughing, and disposing of used tissues straight away is advised. Viruses like coronavirus cannot live outside the body for very long.
 What are the symptoms? 
 The NHS states that the symptoms are: a dry cough, high temperature and shortness of breath - but these symptoms do not necessarily mean you have the illness. Look out for flu-like symptoms, such as aches and pains, nasal congestion, runny nose and a sore throat. It’s important to remember that some people may become infected but won’t develop any symptoms or feel unwell. 
 What precautions can be taken?
 Washing your hands with soap and water thoroughly. The NHS also advises to cover your mouth and nose with a tissue or your sleeve (not your hands) when you cough or sneeze; put used tissues in the bin immediately and try to avoid close contact with people who are unwell. Also avoiding touching eyes, nose and mouth unless your hands are clean.
 Government advice
 As of Monday 23 March the prime minister has put the UK into lockdown and instructed all citizens to stay at home. People can only leave their homes to exercise once a day, go shopping for food and medication, travel for medical needs or to care for a vulnerable person, and travel to work only if essential. Police will be able to enforce these restrictions.
 All non-essential shops will close with immediate effect, as will playgrounds, places of worship and libraries. Large events or gatherings of more than two people cannot go ahead, including weddings and celebrations. Funerals can only be attended by immediate family.
 Children of separated parents can go between both parents' homes.
 Anyone with a cough or cold symptoms needs to self-isolate with their entire household for 14 days.
 The government has now instructed bars, restaurants,  theatres and non-essential businesses to close and will review on a ‘month to month’ basis. Schools closed from Friday 20 March for the foreseeable future, and exams have been cancelled.
 The over 70s or anyone who is vulnerable or living with an underlying illness are being asked to be extra careful and stay at home to self-isolate. People with serious underlying health conditions will be contacted and strongly advised to undertake "shielding" for 12 weeks.For more information on government advice, please check their website. 
 Should I avoid public places?
 You should now avoid public places and any non-essential travel. Travel abroad is also being advised against for the next 30 days at least, and many European countries have closed their borders.
 What should I do if I feel unwell?
 Don’t go to your GP but instead look online at the coronavirus service that can tell you if you need medical help and what to do next.
 Only call 111 if you cannot get help online.
 When to call NHS 111
 Only call NHS 111 if you can’t get help online and feel very unwell. This should be used if you feel extremely ill with coronavirus symptoms. If you have been in a country with a high risk of coronavirus in the last 14 days or if you have been in close contact with someone with the virus please use the online service.
 https://www.edinburghnews.scotsman.com/health/coronavirus/can-i-drive-somewhere-else-exercise-or-walk-my-dog-lockdown-rules-uk-explained-2519615
