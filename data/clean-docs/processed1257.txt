Coronavirus (COVID-19) | Worcestershire County Council
 On the 16 April the Government announced that the current lockdown measures will stay in place for another three weeks at least to help protect lives.
 That means that we must all continue to follow the current social distancing guidelines.
 These include:
 Anyone can spread the virus. By staying at home this will help to prevent the spread of coronavirus and preserve life.
 This will make the process of getting an appointment quicker and easier, while reducing the burden on business.
 Full details on how to access testing and who is eligible can be found on the GOV.UK website: Coronavirus (COVID-19): getting tested (opens in a new window)
 You might be worried about yourself or a friend/relative’s wellbeing whilst we are being asked to stay at home as part of the COVID-19 emergency measures. If so, help is available:
 SafeLives ending domestic abuse # you are not alone
 Safe Lives have all the domestic abuse resources in a one stop link:
 Website: SafeLives ending domestic abuse - Domestic abuse and COVID-19 (opens in a new window)
 Free phone 24 hour National Domestic Abuse Helpline 0808 2000 247
 West Mercia Women’s Aid Helpline 24 hours a day on 0800 980 3331
 Website: West Mercia Women's Aid (opens in a new window)
 Men’s Advice Line on 0808 801 0327 (Monday to Friday 9.00am to 5.00pm)
 In an emergency always dial 999
 If you have a query or issue that cannot be addressed within the guidance, please do not hesitate to contact the Health Protection Team at Public Health England:
 Website: Public Health England (opens in a new window)
 Telephone: 0344 225 3560
 Telephone out of hours: 01384 679031
                     Powered by Jadu Content Management.
                 
 http://www.worcestershire.gov.uk/coronavirus
