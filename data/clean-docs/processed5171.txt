Coronavirus protest in Brazil sees millions bang pots from balconies - BBC News
 People in Brazil have expressed anger at President Jair Bolsonaro's handling of the coronavirus pandemic by banging pots and pans together on balconies.
 Millions of protesters in the cities of São Paulo and Rio de Janeiro appeared at windows on Wednesday evening calling for the president to step down.
 It was the biggest protest against Mr Bolsonaro's government to date.
 There are over 500 cases of the virus in Brazil, including two government ministers. Four people have died. 
 Mr Bolsonaro, who has previously dismissed precautions taken against the novel coronavirus as "hysteria" and "fantasy", has been criticised for his response to the deadly outbreak. 
 The president has been tested for the virus twice, but said on both occasions the results were negative.
 However 14 people who travelled with him to Florida to meet President Trump have tested positive for the virus.
 Wednesday's loud protests began during a televised statement by the president in the afternoon, then resumed with more intensity at an agreed time later in the evening.
 People in tall buildings and houses across the country banged empty pots, switched their lights on and off and shouted "Bolsonaro out!". 
 Mr Bolsonaro used his televised address to announce emergency measures to limit the spread of the virus, and a financial aid package to support the economy and low-income families. 
 Appearing alongside his top ministers, all wearing face masks, the president acknowledged the "gravity" of the pandemic, but added: "I want to calm the Brazilian people... we can't let ourselves go into hysteria."
 However, he admitted that there were likely many additional cases not included in the official statistics. 
 The Brazilian government also said it would ask Congress to declare a "public state of calamity" due to the coronavirus pandemic.
 It was not enough to appease the protesters. 
 On Sunday, Mr Bolsonaro was criticised for coming out of reported self-isolation to mingle with thousands of his supporters who were marching in protest at Brazil's Congress and judiciary. 
 His health ministry had earlier warned against attending large gatherings.
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-latin-america-51955679
