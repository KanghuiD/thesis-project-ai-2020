Coronavirus: Here's how you can stop bad information from going viral - BBC News
 Coronavirus misinformation is flooding the internet and experts are calling on the public to practise "information hygiene". What can you do to stop the spread of bad information? 
 But experts say the number one thing you can do to halt misinformation is to simply stop and think.
 If you have any doubts, pause, and check it out further. 
 Before you forward it on, ask some basic questions about where the information comes from.
 It's a big red flag if the source is "a friend of a friend" or "my aunt's colleague's neighbour". 
 We recently tracked how a misleading post from someone's "uncle with a master's degree" went viral. 
 Some of the details in the post were accurate - some versions, for example, encouraged hand washing to slow the spread of the virus. But other details were potentially harmful, making unproven claims about how to diagnose the illness.
 "The most reliable sources of information remain public health bodies like the NHS, the World Health Organisation, or the Centers for Disease Control and Prevention in the USA." says Claire Milne, deputy editor of UK-based fact-checking organisation Full Fact.
 Appearances can be deceptive. 
 It is possible to impersonate official accounts and authorities, including BBC News and the government. Screenshots can also be changed to make it look like information has come from a trusted public body. 
 Check known and verified accounts and websites. If you can't easily find the information, it might be a hoax. And if a post, video or a link looks fishy - it probably is. 
 Capital letters and mismatched fonts are something fact-checkers use as an indicator a post might be misleading, according to Claire Milne from Full Fact.
 Don't forward things on "just in case" they might be true. You might be doing more harm than good.
 Often we post things into places where we know there are experts - like doctors or medical professionals. That might be OK, but make sure you're very clear about your doubts. And beware - that photo or text you share might later be stripped of its context. 
 But it's a mix of accurate and inaccurate advice.
 When you get sent long lists of advice, it's easy to believe everything in them just because you know for certain that one of the tips (say, about hand washing) is true.
 But that's not always the case.
 It's the stuff that gets us fearful, angry, anxious, or joyful that tends to really go viral. 
 "Fear is one of the biggest drivers that allows misinformation to thrive," says Claire Wardle of First Draft, an organisation that helps journalists tackle online misinformation.
 Urgent calls for action are designed to ramp up anxiety - so be careful.
 "People want to help their loved ones stay safe, so when they see 'Tips for preventing the virus!' or 'Take this health supplement!' people want to do whatever they can to help," she says.
 Are you sharing something because you know it's true - or just because you agree with it?
 Carl Miller, research director of the Centre for the Analysis of Social Media at think tank Demos, says we're more likely to share posts that reinforce our existing beliefs.
 "It's when we're angrily nodding our head that we're most vulnerable," he says. "That's when, above everything else, we just need to slow down everything that we do online."
 Learn more about media literacy:
 With additional reporting from BBC Monitoring 
 Our radio show airs every Saturday on BBC World Service
 Answering life’s questions through daily features, quizzes and opinions.
 https://www.bbc.com/news/blogs-trending-51967889
