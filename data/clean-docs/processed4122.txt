	Coronavirus advice  | Falkirk Council
 This page was last updated: 23/04/2020 14:33:41
 Welcome to our COVID-19 employee information section, created to keep you up to date with the latest guidance and advice relating to work and coronavirus. 
 The page is updated regularly. Please pop back frequently to ensure you're aware of all that is going on.
 We cannot answer queries from non-employees.
 Following the First Minister's announcement on Monday, 23 March, if you work for an essential council service and are fit, have no underlying health conditions, are not pregnant or self-isolating or sick, you have to come to work.
 If you work in social work, you are helping to protect our vulnerable children, adults and older people by ensuring they get the food, medicine and essential care they need. 
 If you work in education, you now play a vital role in helping NHS and care workers continue to do their jobs; by attending work you are helping our nurses and doctors focus their time on fighting coronavirus. 
 Across the Council, there are a great many more employees carrying out other essential roles. Some examples include our tradespeople in Building & Maintenance, refuse collectors in Waste, Catering & Cleaning staff in schools and employees keeping our crematorium open.
 We want to thank you all for your continued commitment and dedication. You are all doing an amazing job in extremely difficult circumstances.
 https://www.falkirk.gov.uk/employees/coronavirus/
