Coronavirus deaths climb to 172, with 13,491 confirmed cases | The Times of Israel
 The death toll from the deadly novel coronavirus in Israel rose to 172 Sunday, with 13,491 confirmed cases of the virus in the country, according to the Health Ministry’s evening round up of the outbreak.
 There were 109 people on ventilators, which require sedation of the patients and the insertion of a breathing tube. Of those, two are in a chronic condition, the ministry said. Another six are connected to ECMO machines, providing cardiac and respiratory support.
     
             if(typeof rgb_remove_toi_dfp_banner != "function" || !rgb_remove_toi_dfp_banner("#div-gpt-ad-336x280_Middle_1")){
                 googletag.cmd.push(function() { googletag.display("div-gpt-ad-336x280_Middle_1"); });
             };
         
 A total of 146 patients are in serious condition, 142 are considered in a moderate condition, and 9,277 have light symptoms. Meanwhile, 3,754 people have recovered from the disease.
 							
 								
 									Free Sign Up
 Of those who have COVID-19, the disease caused by the coronavirus, 571 are being treated in the hospital and 6,745 are at home. Another 2,055 are in special quarantine centers set up in hotels operated by the IDF’s Homefront Command.
 The ministry also said 10,010 virus tests had been conducted over the past 24 hours, similar to the numbers ahead of the weekend. Test numbers have been hovering at around 7,000-10,000 a day over the past week. Prime Minister Benjamin Netanyahu has said that he hopes to get to 30,000 tests a day, though that goal stills seems far off.
 One of those who died Sunday was a 29-year-old woman with a terminal illness, who was Israel’s youngest virus victim.
 Another of the fatalities was an 85-year-old woman, who died at the Baruch Padeh Medical Center in Tiberias. She was a resident of the Yokra assisted living facility in Yavneâel, making her the 18th person from there to die of the virus.
 A 71-year-old man also died at Baruch Padeh. According to Hebrew reports, he was a resident of the “Nof Tiberius” assisted living facility in the city, the second fatality from that nursing home.
 Nearly 40 percent of all deaths in Israel as a result of the pathogen have been residents of nursing homes.
 In addition, a 96-year-old man died at Netanya’s Laniado Hospital and a 76-year-old woman also died at Jerusalem’s Hadassah Ein Kerem Medical Center.
 Israel’s first 168 fatalities broke down as follows by age group, the Health Ministry said Sunday: 85+ age group: 79 deaths; 70-85-year-olds: 70 deaths; 60-70-year-olds: 13 deaths; under 60-year-olds: 6 deaths
 A Magen David Adom ambulance service medic, wearing protective gear, seen handling coronavirus tests from patients outside a hotel being used as a coronavirus quarantine facility, in Jerusalem on April 19, 2020. (Nati Shohat/Flash90)
 Bnei Brak continued to be the community with the highest rate of infection, followed by the Arab Israeli community of Deir al-Asad, in second place. The government on Friday declared Deir al-Asad and neighboring Biâina as ârestricted areas,â amid fears of a coronavirus outbreak there.
 The two adjacent towns in northern Israel, which are a single municipality, were locked down Saturday morning for seven days and testing was increased for residents of the area. Bnei Brak has also seen ongoing restrictions on residents, though a closure on the town was lifted Thursday.
 Police at a temporary checkpoint in the northern Arab Israeli town of Deir al-Asad, April 15, 2020. (Basel Awidat/ Flash90)
 Increased testing is seen as vital to being able to slowly reopen the economy and ease social distancing restrictions on the population.
 Officials have blamed shortages in test components for their difficulty in raising test numbers.
 Magen David Adom workers wearing protective clothing disinfect their ambulance after taking care of a patient with a possible coronavirus infection on April 14, 2020. (Nati Shohat/Flash90)
 Efforts to hit the target received a boost with the Foreign Ministry saying it had signed a deal with Chinese company BGI that will see the firm send lab equipment to Israel by the end of this week, allowing a significant increase to the number of daily tests.
 Netanyahu on Saturday evening announced the removal of some restrictions on industries, commerce and personal freedoms starting Sunday, as part of what he said was a careful and gradual process.
 In the early hours of Sunday morning, the cabinet approved the rollback, after reports of hours of bickering between ministers during long overnight telephone debates.
 Outdoor prayers of up to 19 people will now be allowed (just short of two âminyansâ or quorums), with two meters between worshipers, who must wear masks.
 Ministers also agreed on a NIS 200 fine for anyone caught more than once without a face mask in public.
         Coronavirus latest
 https://www.timesofisrael.com/coronavirus-deaths-climb-to-172-with-13491-confirmed-cases/
