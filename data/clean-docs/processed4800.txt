Virus Outbreak: Chen, Azar talk by phone about prevention efforts - Taipei Times
 The Ministry of Health and Welfare Office of International Cooperation said the 30-minute call began at 8pm, and Azar praised Taiwan’s achievements in preventing the spread of COVID-19 and thanked Taiwan for donating masks to the US.
 Azar and Chen, who heads the Central Epidemic Command Center (CECC), had an in-depth talk about disease prevention strategies, global health and safety, and touched upon Taiwan’s participation in the WHO, the office said.
 Photo: Lu Yi-hsuan, Taipei Times
 At the center’s daily news conference yesterday, Chen said that Azer was curious about the government’s prevention measures, especially precision testing and results.
 “He expressed that there is a lot of room for us [Taiwan and the US] to cooperate in using technology for disease prevention and to benefit the health of all human beings, and that the US would continue to support Taiwan’s participation in the World Health Assembly [WHA],” Chen said.
 Also taking part in the call were American Institute in Taiwan (AIT) Director Brent Christensen, Centers for Disease Control Director-General Chou Jih-haw (周志浩), Department of North American Affairs Director-General Vincent Yao (姚金祥) and Deputy Minister of Foreign Affairs Hsu Szu-chien (徐斯儉) from the Ministry of Foreign Affairs, and Department of International Organizations Director-General Bob Chen (陳龍錦) and technical superintendent Liu Li-ling (劉麗玲) from the health ministry.
 Their call was part of ongoing efforts to cooperate in disease prevention, Yao said yesterday during a news briefing at the foreign ministry in Taipei.
 The call was a natural arrangement following a Taiwan-US joint statement on a partnership against coronavirus on March 18 and a March 31 virtual forum involving high-ranking officials from both nations, Yao said.
 Chen had met Azar on the sidelines of the WHA annual session in Geneva, Switzerland, in 2018 and last year, even though Taiwan was not invited to the assembly, he said.
 The US government has been consistent in supporting Taiwan’s bid to join the WHA, Yao said.
 How Taiwan might join this year’s WHA, which is scheduled for next month, is still under discussion, given the uncertainty over how the assembly would be held, he added.
 Yao also addressed a New York Post story on Sunday that described the measures Taiwan has taken to contain COVID-19 as “authoritarian.”
 The story by Paula Froelich, headlined “Life after lockdown: Electronic monitoring, fines and compulsory face masks,” described Taiwan as “the Chinese-run state” and said “almost everyone is tracked,” citing as examples the government’s use of GPS signals to track the phones of individuals who have arrived from overseas and “borough chiefs are tasked with making calls twice a day to people in their jurisdiction [under home quarantine] to make sure they’re staying home.”
 Taiwan’s disease prevention measures are among the most transparent in the world, while the nation’s achievements in limiting COVID-19 by using certain technological tools have been praised and accepted by the public, Yao said as he condemned the newspaper’s report.
 He also criticized the reporter for not checking the facts before the paper published the story.
 The ministry would continue to ask the newspaper for a correction through the Taipei Economic and Cultural Representative Office in the US and other channels, Yao said.
 Froelich’s story, which cited a NBC News report datelined Taipei on Thursday about “life without lockdown” in Taiwan for several of its examples, ran the same day that the New York Post published an opinion piece by James B. Meigs, headlined “Why Taiwan was the only nation that responded correctly to coronavirus.”
 Meigs’ piece, which analyzed leadership failures ranging from the Titanic hitting an iceberg to the NASA’s Challenger disaster, praised the actions taken by Taiwan’s public health authorities and government leaders in comparison with those taken by Republican and Democratic lawmakers, and other elected officials in the US.
 Officials in the US had made mistakes due to a failure to anticipate the worst, while “Taiwan makes sure that its health institutions are hyper-vigilant about epidemic risks” as a result of the SARS epidemic of 2003, Meigs wrote. 
 Additional reporting by Lin Chia-nan and Diane Baker
 SUSPECTS OUT ON BAIL:
                                         The Taipei District Prosecutors’ Office said it has applied with the High Court to have the three returned to detention as it searches for others involved                                        Hong Kong bookseller Lam Wing-kee (林榮基) said that China was behind an attack against him, after his three alleged assailants were released on bail yesterday.
 “The Chinese Communist Party’s proxies have a history  of attacks carried out against Hong Kongers who fled to Taiwan,” Lam told reporters. “Now they are only causing trouble for Hong Kongers, but later they will make trouble for Taiwanese.”
 Lam said that he would be more vigilant.
 “I was quite surprised to hear that the suspects got released on bail so quickly,” he added. “I am, of course, fearful and will avoid walking down dark alleys.”
 Lam was                                    
                                                                                 China Airlines Ltd’s (CAL) new aircraft would highlight images and elements of Taiwan that would make them distinct from Chinese airlines’ planes, Deputy Minister of Transportation and Communications Wang Kwo-tsai (王國材) said yesterday, amid calls to rename the airline to avoid people confusing it with Chinese carriers.
 The issue of whether to rename the nation’s largest carrier came to the fore again after Taiwan announced that it would donate 10 million masks to countries affected by the COVID-19 pandemic, with CAL carrying out the deliveries, but several foreign media outlets reported that the masks came from China after seeing the words                                    
 CAUSE AND EFFECT?
                                         Ministry spokeswoman Joanne Ou said Taiwan’s ambassador had not been recalled, but only transferred as part of the normal rotation                                        Paraguay has accepted Taiwan’s proposal of a new ambassador, while ties between the two allies remain stable, Minister of Foreign Affairs Joseph Wu (吳釗燮) told lawmakers yesterday, adding that Taiwan would also be appointing a new ambassador to Haiti. 
 Wu made the remarks after Chinese Nationalist Party (KMT) Legislator Wu Sz-huai (吳斯懷) on Wednesday asked whether Paraguay has grown closer to China.
 Paraguay and Haiti are two of Taiwan’s 15 remaining diplomatic allies. 
 The ministry on Monday “recalled” Ambassador to Paraguay Diego Chou (周麟) after Paraguay received more than 40,000kg of disease prevention goods from China last week, the lawmaker said,                                    
                                                                                 Peter Tsai (蔡秉燚), the Taiwan-born inventor of the key technology used in N95 respirators and masks, is studying ways to sterilize masks for reuse amid a global shortage due to the COVID-19 pandemic.
 Tsai, 68, said that he was motivated not by money, but by a desire to help others.
 “Given the choice, I would prefer to help 100 million people rather than earn US$100 million,” Tsai said on Wednesday.
 Tsai, who has retired from his work as a researcher at the University of Tennessee, wrote an article for the University of Tennessee Research Foundation in which he explored ways to sterilize and                                    
 https://www.taipeitimes.com/News/taiwan/archives/2020/04/29/2003735482
