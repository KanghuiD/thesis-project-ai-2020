Governing: State and local government news and analysis
 THE FUTURE OF Finance
 Everybody wants to rebuild it. Nobody wants to pay for it. But there are plenty of options for planning and financing infrastructure projects that don't require deficit financing.
 THE FUTURE OF What’s Happening Now
 During the pandemic, Democratic governors have alternately knocked the president and sought his help. For the opposition party, power in states now provides a base that Congress does not.
 In the Arena
 Whether it is a devastating hurricane or global pandemic, Serena DiMaso will be there to lend a helping hand. From the front lines and Assembly floor, DiMaso is constantly working to strengthen and uplift her community.
 Special Project: Sponsor Content
 Equipping state and local leaders with tactics and resources to respond in a crisis.
 The Trump administration fears that more federal fiscal aid would be a disincentive for state political leaders to reopen their economies. But they're as eager as anyone to get people back to work.
 Already, thousands of state and local government workers have been furloughed or laid off. Falling revenues and soaring demand could lead to budget shortfalls of up to 40 percent, making help from Congress crucial.
 A number of U.S. police departments are using drones as part of their efforts to slow the spread of COVID-19, though public safety agencies differ in the way they employ the emerging technology.
 $5 billion
 THE FUTURE OF What’s Next
 COVID-19 has a long incubation time, and testing can take days to get results. Don't let continually rising case numbers make you give up on staying at home.
 Governing is building a 50-state map to visualize the changes underway to declare states “Open for Business” even as the coronavirus remains at large across the country.
 Despite widespread disruption to session calendars, state legislators still find time to address matters other than the pandemic, an illustration of how the wheels of democracy keep turning.
 Future in Context
 Two centuries ago, John Adams and Thomas Jefferson had to wait months, sometimes years, for a new book to arrive from Europe. Today, technology has removed boundaries to knowledge that would amaze our founding fathers.
 More Articles
 Getting Her Hands Dirty: In The Arena with Assembly Member Serena DiMaso
 The Future of States & Localities
 Where Government is Going – One Headline at a Time Delivered to your inbox everyday.
 Sign Up Now
 GOV Daily: News and analysis at the collision of tech and society and fallout consequences for policy, legislation and strategies to govern.
 Future of Security: From disinformation to deep fakes: protecting critical infrastructure and personal data in a rapidly changing threat environment.
 Future of Finance: From government funds to cryptocurrencies, muni bonds to opportunity zones, pay-as-you-go to long-term financing, direct taxation to P3s.
 https://www.governing.com/
