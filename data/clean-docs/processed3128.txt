The Japan Times - News on Japan, Business News, Opinion, Sports, Entertainment and More
 15
 M/CLOUDY
 On Japan's stretched front line, doctors and nurses DIY a coronavirus response
 			Hospitals like St. Luke's in Tokyo are saving their limited ICU capacity for an increasing number of critically ill patients and improvising makeshift gear to protect front-line staff.
 				5 hours ago			
 Dead, incapacitated or healthy, Kim Jong Un speculation serves as useful thought exercise
 			Whatever Kim Jong Un's fate, speculation over his condition brings into focus important considerations for Asia, including how it might grapple with a scenario involving his death.
 				6 hours ago			
 Abe government may stamp out hanko seals to promote working from home
 			Based on Abe’s instructions, a government committee began discussions Tuesday to revise the regulations and call for more online procedures.
 ANA reports biggest quarterly group net loss in January-March period
 			The company said its group net profit in the business year ended March plunged 75.0 percent from a year earlier to ¥27.66 billion.
 				2 hours ago			
 Pentagon officially releases military videos of UFOs
 In Tokyo, Defense Minister Taro Kono said that Self-Defense Forces pilots have never encountered a UFO, but added that he would consider protocols for SDF pilots in case of encounters.
 Not all pachinko parlors in Tokyo shut; 600 still being investigated
 The Tokyo Metropolitan Government has clarified Gov. Yuriko Koike's statement that all pachinko parlors in Tokyo have complied with requests to suspend business as of Tuesday morning, after at least four were found to ...
 Taiwan doctor's easy-to-make Aerosol Box embraced in Japan
 A creative solution to protect health care providers at high risk of contracting the coronavirus amid the global shortage of personal protective equipment is attracting interest throughout the world, including in Japan.
 The Aerosol Box, ...
 Odd couple: LDP-Komeito political marriage faces testing times
 Power struggles in Abe's party could further strain the two-decades-old coalition in the weeks to come
 In cash-loving Japan, banks still busy despite coronavirus emergency
 Elderly customers, a general lack of enthusiasm for electronic money and increased free time see high customer traffic at banks despite stay-at-home requests.
 Treading fine line, France, Spain to unveil virus lockdown exit
 Virus could double food insecurity in nine African countries: U.N.
 Yoshihide Suga says no lawmaker served as go-between in selection of mask suppliers
 Stimulus steps cannot easily buoy coronavirus-hit economy: Abe
 Nissan expects fiscal 2019 net loss due to coronavirus impact
 COVID-19 strategy: The Japan model
 The nation has a comparatively low mortality from COVID-19 despite the absence of a strict lockdown.
 Strength in numbers: A more open approach to tracking the virus in Japan			
 Designing for good: Creators in Japan respond to the coronavirus			
 Resetting the political calendar after COVID-19			
 In era of COVID-19, a shift to digital forms of teaching in Japan			
 Where we want to go in Japan once this is all over			
 DEEP DIVE			
 Episode 47: The current state of Japan’s emergency			
 LAST UPDATED: Apr 15, 2020			
 Just for kicks: Japan’s sneaker obsession rebounds
 LAST UPDATED: Feb 29, 2020
 				Directory of who’s who in the world of business in Japan
 			
 LAST UPDATED: Apr 27, 2020			
 Tokyo confirms 112 new cases of COVID-19 on Tuesday
 		The update brought the single day figure back into triple digits, after totals below 100 were logged over the past couple of days.	
 Remdesivir drug to be approved for coronavirus patients in May
 Opposition camp submits rent relief bill to Diet
 Frequent shallow-focus quakes signal risk for central Japan
 Hosting Tokyo Olympics next year difficult without vaccine, says chief of Japan's medical association
 More prefectures in Japan extend school closures amid virus uncertainty
 Democratic Party for the People to propose September school enrollment
 March jobless rate rises to one-year high as coronavirus hits economy
 		The politically sensitive figure could spell trouble for the prime minister.	
 Japan businesses that refuse to shut down may yet face legal consequences
 McDonald's to halt in-store dining in more stores across Japan
 Government to help restaurants install ventilators amid pandemic
 Nissan to slash May car production by 78% vs. last year
 77 Japan Post workers rebuked for improper insurance sales
 Embraer takes Boeing to arbitration over failed deal as Brazil eyes China tie-up
 WHO chief says pandemic 'far from over,' expressing worry about children
 The U.N. agency is concerned about rising numbers of cases and deaths ...	
 Biden accuser spoke to neighbor of alleged assault: report
 Putin has a Syria ‘headache’ and the Kremlin’s blaming Assad
 Virus lockdowns an extra ordeal for special-needs children
 Despite risks, auto workers step up to make medical gear amid COVID-19 outbreak
 Blow for China's virus diplomacy as 'One Sea' friendship song leaves Filipinos furious
 Trump reverses course on briefings, gives update on testing
 View more Multimedia articles
 NPB shutdown robs fans of limited chances to see aging legends
 		With the delayed season expected to start behind closed doors, it's unclear when Japanese baseball's elder statesmen will receive cheers from packed stands again.	
 Formula One still faces challenge with revised season plan
 USA Swimming hopes to return to pool with tentative schedule
 England's Ellis Genge wants new players' union after COVID-19 pay cuts
 South Korean soccer snapped up by sports-deprived foreign broadcasters
 Injured Kei Nishikori returns to court at virtual Madrid Open
 UK government's delay in stopping sport events scrutinized
 On the run: Taking up the world's new hobby
 		If you don't have a dog to walk, running gets you outside for some fresh air and helps keep your health up at a time when we all need to be healthy.	
 Compensation may be a matter for the 'Red Book'
 Coronavirus crisis offers chance to update Japanese schools
 Tips for teaching and learning online
 'The swab goes really deep into your head': What it's like to take the test for coronavirus in Japan
 Getting a party rolling in Japan
 Let's discuss the state of emergency
 'Nashi ni' and 'nuki de' will help you do without
 Stay-home request hits girls with no place to go
 ‘Zest for life’ shapes over 30 years of work, play in Japan
 'The Dutch East India Company in Early Modern Japan': Water buffaloes, cassowaries and Arabian horses
 		From exotic animals to spectacles and chandeliers, Michael Laver’s “The Dutch East India Company in Early Modern Japan” shows how well-placed gifts were essential in opening up Japan to early European traders.	
 ‘Automatic Eve’ review: Familiar tropes reimagined with brilliant sci-fi originality
 Traveling Gekidan Miyama troupe grinds to a halt
 Racking up the views on YouTube with The First Take
 'Zero': Practicing kindness in life and in love
 Government assistance is vital in pandemic
 The COVID-19 crisis is testing how well Japan protects the people and businesses in an emergency. The government need to sustain its assistance as steadily as possible while making sure the most needy are not left behind.
 Government’s support for schools is essential
 Students in public schools closed by the pandemic are at a strict disadvantage to their peers in private schools. The nation must do everything it can so these children are not left behind.
 How lockdown could lead to stronger parliaments
 Parliamentary democracies need parliaments to function. This is as true of the Westminster institution in London, sometimes described as the mother of parliaments, as it is of the Diet in Tokyo or of any other parliamentary legislatures in states round the world that struggle ...
 Sponsored contents planned and edited by JT Media Enterprise Division.
 広告出稿に関するおといあわせはこちらまで
 Read more
 THE JAPAN TIMES DIGITAL ARCHIVES
 RELATED SITES
 DISASTER INFORMATION
 ESG Consortium
 Satoyama Consortium
 Japan Spelling Bee
 JT GROUP SITES
 The Japan Times Alpha
 Book Club
 To our readers: Internal review of 'comfort women' and 'wartime labor' descriptions
 https://www.japantimes.co.jp/
