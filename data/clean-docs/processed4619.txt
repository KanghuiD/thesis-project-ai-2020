CNN, MSNBC’s Decision to Not Air Trump’s Briefing Is Overdue – Opinion | IndieWire
 Tyler Hersko
 							Apr 3, 2020 12:53 am						
 US President Donald Trump speaks to members of the news media in February 2020.
 KEVIN DIETSCH/POOL/EPA-EFE/Shutterstock
 After days of internal newsroom pressure and external criticisms, CNN and MSNBC cut away from President Donald Trump’s coronavirus briefing on Wednesday when he used it as an opportunity to begin discussing his southern border wall.
 The networks’ move came after several days of meandering presidential presentations that sounded more like Trump rallies than important medical briefings. As Deadline noted, CNN and MSNBC have faced inside turmoil from their own anchors, such as John King and Chuck Todd, who criticized them while on air.
 The decision to not air Trump’s coronavirus briefings is laudable. In an unprecedented time of global uncertainty about the world’s health, welfare, and the ability to walk outside our homes without contracting a deadly virus that may hang in the air, the president often has used his nationally broadcast time on the dais to spread lies and misleading information.
 Related
 					The Show Must Go On: Here's What's Still Open for Business in Hollywood				
 					Coronavirus Cancellations: Every Film, TV Show, and Event Affected by the Outbreak				
 					Emmy Predictions 2020: Best Actor in a Limited Series or TV Movie				
 					Emmy Predictions 2020: Best Supporting Actor in a Comedy Series				
 			Popular on IndieWire		
 		function buildJW() {
 			if ('function' === typeof jwplayer) {
 				contextual_player = jwplayer('jwplayer_contextual_player_div').setup({
 					playlist: "https:\/\/cdn.jwplayer.com\/v2\/playlists\/zj3v2AtA?semantic=true&backfill=true&search=__CONTEXTUAL__",
 					width: "100%",
 					aspectratio: "16:9",
 					autostart: "viewable",
 					mute: true,
 					floating: false,
 									});
 			}
 		}
 Still, while the networks’ decision is commendable, it’s also fantastically late. Misinformation is nothing new for him, which is why cable news outlets have drawn considerable criticism for dedicating significant air time to Trump. However, like Trump’s 2016 campaign rallies — which network heads have admitted were ratings hits — the president’s coronavirus briefings have attracted large viewership numbers.
 Trump kicked off his 2016 presidential campaign by denigrating Mexicans as drug dealers. Now, rather than racist rhetoric or demonization of non-Republican political groups, his presentation of the coronavirus epidemic presents a real-time threat to public health.
 News organizations have taken some steps over the last few years to filter Trump’s statements  — mostly through rigorous fact checking or after-the-fact analysis — but it’s a half-measure when so many of the president’s lies are presented on-air.
 The Daily Beast recently reported that CNN chief Jeff Zucker, who greenlit “The Apprentice” in 2004, defended the network’s decision to carry the press briefings because, Trump’s lies aside, it was important for viewers to hear important health information from experts such as Anthony Fauci, the country’s top coronavirus expert and a member of Trump’s coronavirus task force. Fair enough, but there’s no reason that the networks couldn’t air critical information from Fauci and other experts after their journalists compiled the relevant segments.
 News organizations should not ignore Trump’s myriad briefings and events — watchdog journalism is as essential as ever — but, as Washington Post columnist Margaret Sullivan noted, if the press values journalistic integrity over profits, it must recognize the importance of not letting the president spread lies on their platforms. That’s true for the industry’s coronavirus coverage — but there’s no reason it should be limited to the current pandemic.
 This Article is related to: Television and tagged CNN, Coronavirus, Donald Trump, MSNBC, The Agitator
 Listen to these IndieWire podcasts.
 IndieWire: Millions of Screens
 IndieWire: Screen Talk
 Interviews with leading film and TV creators about their process and craft.
 https://www.indiewire.com/2020/04/cnn-msnbc-not-airing-trump-coronavirus-briefing-1202222521/