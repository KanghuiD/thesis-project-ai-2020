Opinion: Coronavirus Is a Test of Asian Leadership - Caixin Global
 Global leadership is one of the most prominent victims of the virus. The US President seems to be exclusively focusing on the domestic front; the European Commission is doing its best but has its limits; Africa and Latin America are bracing for Covid-19, learning from global best practices and seeking to ensure health supplies.
 Despite being the epicenter of the pandemic, China has projected strong resilience and preliminary evidence suggests the worst phase there is now over. Other Asian countries such as Japan and South Korea are also showing success in controlling the disease.
 It is clear that the Covid-19 pandemic will change the world as we know it. What will the new world order look like? Has the West lost its footing? Will the pandemic cement the rise of China and an “Asian Century”?
 The leadership and international coordination vacuum must be filled somehow. The natural expectation is to see what China and the G-20 propose in terms of handling the looming global economic crisis.
 China has been the main player in terms of globally relevant initiatives. China has received a lot of praise for sending supplies to Italy and other European countries. The Serbian president delivered a very emotional speech after receiving help from China help and highlighted the Europe’s poor response.
 There are signs China can quickly bounce back, but we should not forget the multitude of crises Beijing has had to deal with during the past year, from Hong Kong unrest and trade war with the US to the Covid-19 pandemic.
 Asian countries should also consider their strategic interests and assume leadership they want to be acknowledged as leaders. It’s interesting that Saudi Arabia, an Asian country, leads the G-20, while the US leads the developed country club, the G-7. Are Asian globalization champions like China capable and credible enough to revamp and reimagine global cooperation when everybody is talking about deglobalization and the return of hardcore nationalism?
 Japan, South Korea, rich countries from the Gulf and ASEAN have a choice between working together or separately. Will they form a dream team of Asian leadership despite disagreements? Or will they move closer to Western solutions?
 One aspect to keep in mind is that global value chains. Decoupling may not be certain, but diversification and a new wave of industrial patriotism will become the norm.
 If it is to propose a new vision of globalization, China will have to work together with other Asian powers. Support from countries with different economic profiles and experiences can add credibility for those who skeptical of what Beijing has to offer in terms of international public goods. This coordination will not be easy, as it comes with its own problems and historical difficulties.
 ASEAN countries will have to put the entire discussion about economic transformation and a fourth industrial revolution on hold, and think fast about how to compensate for lost jobs and the decline of an entire economic model.
 Those interested in the "Asian Century" should look to the EU: with unity in diversity, Europe will be back stronger after this crisis. But the bottom line is that the world is looking also — if not primarily — to China and Asia for a fresh take on what to do next.
 Radu Magdin is a global analyst and consultant, and former prime ministerial advisor in Romania and Moldova. The article has been edited for length and clarity.
 Participation of Indian Prime Minister Narendra Modi in the 5th Eastern Economic Forum Discussed in Moscow
 Get exposure for your startup at RISE 2020
 CreditEase’s Tang Ning: China’s Wealth Management Market is Undergoing Five Major Shifts
 Meet 5 of the best startups selected to represent China at the largest technology event in Asia
 Media Partners
 OR
 https://www.caixinglobal.com/2020-04-08/opinion-coronavirus-is-a-test-of-asian-leadership-101540131.html