Coronavirus: US doctors warn 'don't take medical advice from Trump' - CBBC Newsround
 US President Donald Trump has been criticised by doctors and scientists for giving "irresponsible" and "dangerous" advice about treating coronavirus.
 It came after the president suggested research should be done to see if the virus could be treated by injecting disinfectant into the body.
 Most disinfectants contain bleach which would make you very seriously ill and could kill you if it gets inside your body.
 Dr Xand and Dr Chris answer your coronavirus questions
 Coronavirus: What is being done to tackle the virus?
 Advice if you're worried about coronavirus 
 Mr Trump's own public health agencies also warn against bleach as a medicine.
 The US Centers for Disease Control and Prevention warned Americans to be careful with cleaning products as sales of household disinfectants have gone up during the pandemic.
 The president also suggested shining patients' bodies with UV light, an idea dismissed by a doctor at the briefing.
 During Thursday's White House news briefing the US government announced the results of its research, that suggested coronavirus appeared to weaken more quickly when exposed to sunlight and heat.
 The research also said bleach could kill the virus in saliva or respiratory fluids within five minutes and a cleaning fluid called isopropyl alcohol could kill it even more quickly.
 Although he said the research should be treated with caution, Mr Trump suggested more research should be done into how these might help with treating coronavirus.
 He talked about the possibility of projecting "ultraviolet or very powerful light" onto patients, as well trying to shine the light inside of the body, "either through the skin or in some other way".
 Speaking about disinfectant Mr Trump also asked: "Is there a way we can do something like that, by injection inside or almost a cleaning?"
 Pointing to his head, he went on: "I'm not a doctor. But I'm, like, a person that has a good you-know-what."
 Dr Vin Gupta, who specialised in treating the lungs, told NBC News: "This notion of injecting or ingesting any type of cleansing product into the body is irresponsible and it's dangerous".
 Another lung specialist John Balmes, at Zuckerberg San Francisco General Hospital, warned that even breathing fumes from bleach could cause severe health problems.
 He said: "Not even a low dilution of bleach or isopropyl alcohol is safe. It's a totally ridiculous concept."
 Kashif Mahmood, a doctor in Charleston, West Virginia, tweeted: "As a physician, I can't recommend injecting disinfectant into the lungs or using UV radiation inside the body to treat Covid-19.
 "Don't take medical advice from Trump."
 Coronavirus: What's the latest advice about wearing masks?
 Coronavirus: When will schools go back?
 Happy birthday Hubble!
 Swift calls former bosses 'greedy' over new live album
 UN: 'Fight climate change like we fight coronavirus'
 https://www.bbc.co.uk/newsround/52408918
