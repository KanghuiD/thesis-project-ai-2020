Here’s the information and advice that’s changed about the coronavirus since the outbreak began 
 newsPublic Health
 By Catherine Marfin
 8:31 PM on Apr 9, 2020
 During a rapidly developing crisis like the coronavirus outbreak, what we know can change quickly.
 In the weeks since the virus reached the United States, new information about it has spurred shifts in prevention advice and predictions about how the outbreak may play out as scientists and health experts have learned more about the disease.
 Here’s how information and advice has changed since the beginning of the outbreak:
 In the early stages of the outbreak, Americans scrambled to stock up on supplies to help protect them from the virus, including surgical masks and N95 respirators. The surge in demand caused shortages across the country, making it harder for health care workers to find them.
 Top public health officials maintained for months that only two groups of people needed to wear masks: people who were sick, and people who were taking care of someone who was sick.
 Healthy people didn’t need to wear them, health experts said, because masks are primarily designed to keep what’s in a doctor’s mouth away from patients — not protect the doctor from germs in the air.
 But the federal Centers for Disease Control and Prevention reversed that recommendation on April 3, urging all Americans to wear cloth masks in places where social distancing guidelines are hard to follow, such as grocery stores and pharmacies.
 While some studies have shown that cloth masks aren’t as effective as medical-grade ones, health experts say a homemade mask is better than nothing, and will help free up supplies of surgical masks for health care workers.
 The information about a mask primarily protecting people around the wearer is still true. But the CDC says that because scientists have found increasing evidence that people who don’t show symptoms can transmit the virus, it’s important for everyone to wear a mask in public to prevent spreading the virus without knowing it.
 The idea is if everyone wears a mask to protect those around them, everyone in a community will be protecting one another.
 At first, public health experts didn’t know whether it was possible for people to transmit the virus without showing symptoms of the disease. But as the outbreak continues and scientists learn more, the CDC says there is increasing evidence that asymptomatic people can give the virus to others.
 On the CDC’s web page on cloth masks, seven studies are cited as evidence that asymptomatic transmission is possible.
 Robert Redfield, director of the CDC, told NPR that the organization believes up to 25% of people who have the coronavirus may be asymptomatic, and that people who eventually develop symptoms can spread the virus to others at least two days before feeling sick.
 The New York Times reported that one study found that 20% to 40% of infections in China may have happened before symptoms appeared. Other studies, like one conducted in February in China by the World Health Organization, have found that the majority of people who might appear to be asymptomatic eventually develop symptoms.
 Health experts say the most important thing to note is that there is evidence that people can give the virus to others without feeling sick, highlighting a need to follow stay-at-home orders and wear masks in public places.
 In the early days of the pandemic, people were quick to compare COVID-19 to a bad case of the flu.
 On the surface, both illnesses appear to be similar. They are both transmitted through respiratory droplets when people cough or sneeze, and they both have many of the same symptoms: fever, a dry cough, aches and pains and fatigue.
 But many COVID-19 patients experience shortness of breath, which isn’t as severe in cases of influenza.
 As more data becomes available about the new virus, health experts say there are a few other important differences between the two illnesses.
 An editorial by top U.S. health authorities in The New England Journal of Medicine said that each person with coronavirus infects about 2.2 other people.
 The number may be skewed by how the outbreak was controlled in the early stages, but it’s still a higher rate than the flu, which has a transmission rate of about 1.3 people for every positive case.
 Health experts say another important difference is the rate of hospitalization for each illness.
 As many as 80% of cases of COVID-19 are mild to moderate, according to a study published by WHO in February. Data from China found that as many as 20% of people become ill enough to be sent to the hospital, NPR reported.
 That’s much higher than the flu. According to data from the CDC, the hospitalization rate for the flu was 1% to  2% from 2010 to 2018.
 And the length of the hospital stay differs, too. A study of nearly 200 patients in China found that the average hospital stay was 11 days, nearly twice as long as the average stay for the seasonal flu, about five days.
 There’s some debate about how the death rate compares between the two illnesses. The flu has killed on average since 2010 about 0.13% of people who contracted it, according to CDC estimates.
 Some health experts say that COVID-19 is estimated to have a death rate of 1% to 2% of patients. But the New England Journal of Medicine editorial said the rate may end up being lower than that and closer to the mortality rate of a severe seasonal flu.
 More data is needed to assess the true death rate of the disease, but health experts say it’s important to note that while most people have built up immunity to the flu, no one has immunity to the new coronavirus, making everyone more susceptible to contracting it.
 Catherine Marfin, Breaking News Reporter. Catherine covers breaking news at The Dallas Morning News. She is a recent graduate of the University of Texas at Austin, where she studied journalism and public relations. While at UT, Catherine served as managing editor of The Daily Texan, UT's student paper, and interned at the Texas Tribune and Houston Chronicle. 
 Get the latest coronavirus updates delivered to your inbox by signing up for our exclusive newsletter.
 Stand with us in our mission to discover and uncover the story of North Texas
 https://www.dallasnews.com/news/public-health/2020/04/09/heres-the-information-and-advice-thats-changed-about-the-coronavirus-since-the-outbreak-began/