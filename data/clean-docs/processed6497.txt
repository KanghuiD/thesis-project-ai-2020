Children may be the silent victims of coronavirus | The Japan Times
 16
 M/CLOUDY
 Bloomberg
 Kuala Lumpur – Many parents felt a profound sense of relief when analyses of the coronavirus found that kids rarely show symptoms and usually aren’t at much risk. Unfortunately, that doesn't mean they’re safe from the global pandemic.
 Every parent is familiar with the day-to-day perils of the COVID-19 outbreak, with playgrounds closed, social interaction limited and screen time all too tempting. But the most consequential effects of this crisis are likely to come from two areas: health and education.
 googletag.cmd.push(function() { googletag.display('div-gpt-ad-1499653692894-0'); });
 For kids, that can have long-lasting consequences. At the height of the West African Ebola epidemic, in 2014, institutional deliveries of children — a crucial means of reducing maternal mortality — decreased by 37 percent compared to the previous year. Prenatal consultations, which have been shown to improve children’s long-term physical and mental health, decreased by 40 percent. Worse, vaccination rates declined sharply. In 2013, before the outbreak, a still-too-low 73 percent of Liberian children under age one were fully vaccinated. From July to September of 2014 that number dropped to 36 percent. Measles vaccination rates alone declined by 45 percent.
 This drop-off created a large "immunity gap" that allowed the disease to spread more easily. The consequences lingered: From 2016 to 2017, 49 percent of Liberia's 2,954 suspected measles cases were detected in children under the age of five. In all likelihood, the current outbreak will have similar effects. In late March, the World Health Organization called for preventative mass-vaccination programs to be suspended worldwide to allow for social-distancing measures. Although this was probably the right call, even public health experts who supported the decision acknowledged that it will contribute to a surge of diseases that might last for years.
 Another serious worry — for parents and for the world — is that some 1.4 billion children currently live in areas where schools have been fully or partially closed, from Southern California to rural India. Most of those kids have no idea when they’ll be able to return. When schools in Liberia, Guinea and Sierra Leone were closed to curtail the spread of Ebola, students lost as many as 1,848 hours of education.
 A significant body of research suggests that such a prolonged absence from school can be detrimental educationally and developmentally, and especially harmful for low-income students. As plenty of frustrated parents are now finding, distance-learning programs can be ineffective even under ideal circumstances. In poorer households, where parents are less likely to be able to work from home, the challenges can be severe. Especially in developing regions, girls are at particular risk of not resuming their education due to increased poverty, domestic responsibilities and pregnancy rates during the pandemic. Even when schools do reopen, the virus may pose a threat for months or years into the future.
 For their part, schools should strive to maintain as much continuity with students as possible. Step one should be a concerted effort to pay and retain teachers during the crisis. They'll be key to reopening schools quickly, and could provide some much-needed stability for students who have lost their sense of routine or even safety. Finally, planning needs to start now on assessments and accelerated curriculums designed to help students catch up and review what they were learning before the pandemic. Modest steps like these won't lead to cures or treatments for the coronavirus. But they might help ensure that its effect on future generations isn’t needlessly destructive.
  Adam Minter is a Bloomberg Opinion columnist. He is the author of "Junkyard Planet: Travels in the Billion-Dollar Trash Trade” and the forthcoming "Secondhand: Travels in the New Global Garage Sale." 
 						Click to enlarge
 					
 covid-19, school closures, remote learning 
 Strength in numbers: A more open approach to tracking the virus in Japan			
 Designing for good: Creators in Japan respond to the coronavirus			
 Resetting the political calendar after COVID-19			
 In era of COVID-19, a shift to digital forms of teaching in Japan			
 Where we want to go in Japan once this is all over			
 DEEP DIVE			
 Episode 47: The current state of Japan’s emergency			
 LAST UPDATED: Apr 15, 2020			
 Just for kicks: Japan’s sneaker obsession rebounds
 LAST UPDATED: Feb 29, 2020
 				Directory of who’s who in the world of business in Japan
 			
 LAST UPDATED: Apr 27, 2020			
 Sponsored contents planned and edited by JT Media Enterprise Division.
 広告出稿に関するおといあわせはこちらまで
 Read more
 https://www.japantimes.co.jp/opinion/2020/04/27/commentary/world-commentary/children-may-silent-victims-coronavirus/
