October 20 coronavirus news
 By Emma Reynolds, Joshua Berlinger, Adam Renton, Meg Wagner and Melissa Macaya, CNN
 Our live coverage of the coronavirus pandemic has moved here.
 From CNN's Sandi Sidhu in Hong Kong
 Cathay Pacific Group, the owner of Hong Kong flag carrier Cathay Pacific, is cutting 5,900 jobs across its global workforce in a bid to stay afloat, the company said Wednesday.
 Cathay is just one of many airlines across the world that have been decimated by the Covid-19 pandemic, as anti-epidemic measures have forced travelers to stay home.
 The company said it would shutter its regional carrier, Cathay Dragon, and lay off 5,300 employees based in Hong Kong. About 600 employees based outside of Hong Kong are likely to be affected "subject to local regulatory requirements," the company said in a statement.
 Tang said that in spite of these efforts, the company was burning through up to 2 billion Hong Kong dollars (about $260 million) in cash each month, which was "simply unsustainable." 
 He said the changes announced today would reduce those expenditures by about 500 million Hong Kong dollars ($65 million) per month.
 Tang said Cathay expects to operate at just 25% of its 2019 capacity in the first half of next year, and less than 50% for the entirety of 2021.
 From CNN Business' Hanna Ziady
 Bank tellers are out and robotics engineers are in, according to a new report that says the coronavirus recession is accelerating technological changes that could displace 85 million jobs within the next five years.
 "Automation, in tandem with the Covid-19 recession, is creating a 'double disruption' scenario for workers," said the report published Wednesday by the World Economic Forum, which warns that inequality is likely to increase unless displaced workers can be retrained to enter new professions.
 More than two-fifths of large companies surveyed by the WEF plan to reduce their workforces due to the integration of technology.
 The coronavirus pandemic has caused a sharp spike in unemployment around the world. Several major economies in Europe and elsewhere have extended support for wages in order to offset the alarming rise in joblessness. Unemployment in the United States, meanwhile, continues to march higher while lawmakers quarrel over new stimulus measures.
 The pandemic risks deepening existing inequalities because industries that have been hardest hit, including travel and tourism, hospitality and retail, tend to have younger, and lower-wage workers who are disproportionately female.
 The World Bank has warned that the pandemic could increase income inequality and push up to 115 million people into extreme poverty this year.
 Read the full story:
 From CNN’s Andy Rose
 Nevada Gov. Steve Sisolak is warning residents that more restrictions could be needed if people aren’t more careful about stopping the spread of Covid-19. 
 The state’s daily case count has doubled in the past month, according to state health officials. The test positivity rate in Nevada is now 9.1%, the highest rate since early September and 82% higher than the goal set by the World Health Organization.
 "We're beginning – just beginning – to see an alarming trend in the number of cases and hospitalizations in Nevada,” he said.
 From CNN's Josiah Ryan 
 Mindfulness expert Jon Kabat-Zinn led CNN's Anderson Cooper today on "Full Circle" through a short guided meditation, a discipline which he said can help bring calm in these turbulent times.
 After Anderson closed his eyes, Kabat-Zinn started the session by ringing bells and instructing viewers to follow the sounds into silence and the sensations of the breath leaving the body. 
 "You drop into the present moment when you do this," he said. "The breath can be a glide path ... back into this moment however it is, and remembering this is it."
 Kabat-Zinn said that a simple habit of meditating for as little as five minutes per day can have powerful psychological and even biological benefits that can be especially helpful to people in times of stress and change. 
 "You can learn by exercising this muscle on a regular basis to rest in this awareness, that's embracing your body breathing," he said. "It can transform your biology... it also rearranges your brain in various ways."
 "This looks a lot like its doing nothing," Kabat-Zinn said. "It's not doing nothing. I prefer to call it 'non-doing'... and actually you can get a lot more work done sometimes by practicing 'non-doing.'"
 Watch the moment:
 Washington state is requiring people to wear masks at all times in living areas on colleges campuses, including dormitories and Greek houses, following a series of coronavirus outbreaks.
 The state is dealing with 35 coronavirus outbreaks at colleges and universities, Gov. Jay Inslee said Tuesday at a news conference. More than 800 cases are "directly attributable to these congregants living and social gatherings associated with campuses," the governor said.
 Being outdoors or in “sleeping areas” would be the only exceptions to the order. Common areas will be limited to five people at a time. Colleges and universities must also provide quarantine facilities for all group housing, including fraternities and sororities.
 Inslee also encouraged residents to limit the number of people they invite to their homes.
 From CNN's Hilary McGann in London, and journalist Peter Taggart in Belfast
 Ireland reported 13 new coronavirus deaths on Tuesday, the highest amount it has reported in a day since May. 
 During a press conference, Ireland's Chief Medical Officer Tony Holohan said the deaths reported occurred in recent days. 
 Another 1,269 cases were also recorded. 
 On Monday — the same day the country's case count surpassed 50,000 — the Irish government announced it would impose the strictest level of restrictions in the country for the next six weeks, in an effort to tackle a surge in cases. 
 Watch:
 From CNN's Kevin Dotson
 Southern Miss interim head football coach Scotty Walden tested positive for Covid-19 Tuesday morning, the university has announced.
 Southern Miss is scheduled to play Liberty University in a game this Saturday. 
 Southern Miss’s game against UTEP scheduled for last Saturday was postponed, with Southern Miss citing “an increase in COVID-19 cases in the program.”
 From CNN’s Elizabeth Cohen and Wes Bruer
 Moderna and Pfizer, the two frontrunners in the US race for a Covid-19 vaccine, will be making presentations to an all-day meeting Thursday of a US Food and Drug Administration advisory committee, but both companies said they will not be presenting data from their Phase 3 clinical trials. 
 Both companies have said they could possibly apply to the FDA for authorization to market their vaccines in the coming weeks if their Phase 3 clinical trial data turns out to be positive. 
 Pfizer has said it could apply for emergency use authorization after the third week in November. Moderna has said the federal government could give an EUA to the company’s experimental Covid-19 vaccine in December if the company gets positive interim results in November.
 Once the companies apply for emergency use authorization from the FDA, the Vaccines and Related Biological Products Advisory Committee will advise the agency on whether it thinks authorization should be given. 
 The Thursday meeting is scheduled from 10 a.m. to 5 p.m., but Dr. Paul Offit, a member of the FDA’s Vaccines and Related Biological Products Advisory Committee, said members have been asked to keep their schedules clear until 7 p.m. because it might run long. 
 Offit, director of the Vaccine Education Center at Children’s Hospital of Philadelphia, said “general parameters” will be discussed for how to measure vaccine safety and efficacy. 
 From CNN’s Rebekah Riess
 Tennessee has seen a slight increase in its Covid-19 case rate and positivity rate, as well as a dramatic rise in hospitalizations, with numbers up over 50% since Oct. 1, Tennessee Health Commissioner Dr. Lisa Piercey announced Tuesday. 
 Dr. Piercey said the increase in hospitalizations is primarily because most of Tennessee’s cases are now coming from rural areas. 
 To help mitigate the rise in hospitalizations, the state is working with hospital partners on different targeted approaches, including additional funding for hospital staffing, and amping up Covid-19 specific nursing homes to reduce the load of nursing home patients on hospitals, Gov. Bill Lee said.  
 Dr. Piercey added that Tennessee’s alternative care sites continue to exercise and prepare in the catastrophic event the state would need to open one of the sites.
 Please enable JavaScript for a better experience.
 https://edition.cnn.com/world/live-news/coronavirus-pandemic-10-20-20-intl/index.html