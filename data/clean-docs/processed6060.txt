Latest & Breaking News Melbourne, Victoria | The Age
 The global death toll from coronavirus has now passed 203,000, with more than 2.9 million known cases of infection. In Australia, the death toll stands at 83, with some states moving to ease lockdown restrictions.
 Some employers say staff don't want to work at all, or want to work minimal hours, but still receive payments of $1500 a fortnight.
 The inevitable outcome of this debacle is a slew of court cases, according to Harvey Norman founder Gerry Harvey.
 A truck driver has been charged with culpable driving over the Eastern Freeway crash that killed four police officers last week.
 "I have no adequate words to describe the grief and loss we are feeling right now," one Victoria Police officer said as the force comes to terms with the tragedy.
 Health authorities are expecting more than half of all Australians to download the voluntary COVIDSafe contact-tracing app.
 Some signs of life have emerged from the North Korean presidency after weeks of speculation about Kim Jong-un's health.
 Being unable to hold your loved one’s hand or kiss them goodbye as they die alone in a hospital bed is perhaps the most brutal and devastating hallmark of coronavirus.
 A humanitarian flight with 181 passengers on board has landed in Melbourne from Buenos Aires.
 Many Australians are relishing the opportunity to reconnect with the long-lost art of letter-writing.
 Perpetrators are 'weaponising' the health crisis to control their victims amid a dramatic jump in calls for help.
 The government says it has done enough to protect Australians' privacy - and it looks like it might be about right.
 Four small funerals for the fallen police officers killed in the Eastern Freeway tragedy will take place in coming weeks before a larger state memorial is held 'as soon as possible'.
 Lou Bougias isn't sure if it's good luck or bad luck but tragedy just seems to follow him around. 
 Federal Education Minister Dan Tehan has challenged Victoria to open schools next month rather than persist with remote learning until the end of term.
 The state government has ruled out following in the footsteps of other states that have started easing restrictions.
 Cancer referrals at one of Australia's leading oncology centres have plummeted by 30 per cent, fuelling concerns seriously ill people could be going undiagnosed.
 Drivers are being licensed with only a few hours of experience behind the wheel of a heavy vehicle,.
 Farmers ploughing millions of dollars worth of fresh produce into the soil as coronavirus ban on dining continues.
 The nation's leaders are being urged to prioritise large-scale home-building investments in the months ahead to help aid economic recovery
 Rupert Murdoch’s News Corp Australia has hired consultants to cut costs by centralising some functions, cutting print production of some smaller newspapers and reviewing its regional newspapers business.
 North Korea's "cult-driven system" makes it essential to have a family member in charge, and Kim Jo-yong "has shown that she knows how to exercise authority."
 Stuff political editor
 Daily Life editor
 Share information with our journalists securely and confidentially. Learn more
 The house price record in this waterfront neighbourhood has been hit out of the park in an off-market deal. 
 Fresh air, balconies and two storeys: these will be the biggest post-coronavirus priorities for apartment design, experts say.
 In the competition for survival between men and women, women will win out. And there's a straightforward reason why.
 The AFLPA remains open to the idea of using quarantine hubs to restart the season, but are determined to ensure that any demands made on players and their families are reasonable.
 Cricket Australia has $300 million resting on the series between Australia and India going ahead but will need the approval of the federal government.
 A spectacular running effort by the Demons forward deserves the accolade for season 2020, but another contender is on constant replay in her household.
 A missed text message and a wavering telco will continue to cause headaches for Rugby Australia, which is reeling from Raelene Castle's sudden exit.
 Ed Langdon has walked pilgrim trails, completed torturous hill sessions and withstood friendly digs from his brother between leaving Freo and joining Dees.
 Australian coach of two-times Grand Slam winner Simona Halep has backed the idea of unifying men's and women's tours, saying it would make tennis simpler for both players and fans.
 Damien Hardwick believes the defending premiers could be up and running for competitive matches with as little as two weeks' serious training.
 AFL boss Gillon McLachlan will be remembered largely for the game he bequeaths to his successor, whenever that happens.
 https://www.theage.com.au/
