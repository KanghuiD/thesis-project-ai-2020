Coronavirus: German zoo may have to feed animals to each other - BBC News
 Zoos that should have been crowded in the sunny Easter holidays are now hard-up and asking for donations, as the coronavirus lockdown bites.
 A zoo director in northern Germany has even admitted that some animals might soon have to be fed to others, if the zoo is to survive.
 "We've listed the animals we'll have to slaughter first," Neumünster Zoo's Verena Kaspari told Die Welt.
 Berlin Zoo has infant panda twins, but their fans can only watch them online.
 The zoo's spokeswoman Philine Hachmeister told DPA news agency "the panda twins are adorably sweet". 
 "Constantly we're thinking 'the visitors should be watching them live'. We don't want the little pandas to be grown up by the time we finally reopen."
 Ms Kaspari at Neumünster Zoo said killing some animals so that others could live would be a last resort, and "unpleasant", but even that would not solve the financial problem. 
 The seals and penguins needed big quantities of fresh fish daily, she pointed out. 
 "If it comes to it, I'll have to euthanise animals, rather than let them starve," she said. 
 "At the worst, we would have to feed some of the animals to others."
 Ms Kaspari's zoo belongs to an association, which is not covered by the state emergency fund for small businesses. 
 She estimates the zoo's loss of income this spring will be about €175,000 (£152,400).
 Besides direct appeals for public donations, Germany's zoos are jointly requesting government aid worth €100m, DPA reports.
 Germany's national zoo association (VdZ) argues that zoos, unlike many other businesses, cannot go into hibernation and run down costs. Animals still have to be fed daily and looked after, while a tropical enclosure has to be heated above 20C. 
 VdZ chief Jörg Junhold said the lockdown was costing a typical German zoo about €500,000 weekly in lost turnover.
 Schönbrunn Zoo, one of Vienna's top attractions, says it can manage for the time-being by drawing on existing savings. 
 But on 1 April it sent 70% of its 230 staff on three months' furlough - sent home with their jobs safeguarded. Austria has a "Kurzarbeit" (subsidised short-time work) system like Germany's, so that most workers do not lose their jobs when their employer hits hard times.
 Some zookeepers have also warned that the crisis has an emotional cost for certain animals, because they miss the attention they would usually get from the public.
 Ms Hachmeister at Berlin Zoo said "the apes especially love to watch people".
 She said seals and parrots were also fascinated by their visitors, and "for them now it's really pretty boring".
 Last week Moscow Zoo also said its two giant pandas were "missing something now". 
 "They've started to much more actively approach every single person who walks past their enclosure," it said.
 The Commission president says Italy was let down by "too many" at the start of the outbreak.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/world-europe-52283658
