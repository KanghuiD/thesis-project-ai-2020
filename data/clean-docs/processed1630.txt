South Sudan reports first case of coronavirus | South Sudan News | Al Jazeera
 Country's vice-president says 29-year old patient showed symptoms on April 2 and is being treated in isolation.
 South Sudan has confirmed its first case of COVID-19, its vice president said on Sunday.
 It became the 51st of Africa's 54 countries to have the disease caused by the new coronavirus.
 Riek Machar said a 29-year old patient arrived in the country from Ethiopia on February 28, and that she was being treated in isolation.
 The patient is under quarantine at United Nations premises and health workers are tracing the people who had been in contact with her, said David Shearer, head of UN operations in South Sudan.
 The patient first showed signs of the disease on April 2 and is recovering, said officials.
 South Sudan, with 11 million people, currently has four ventilators and wants to increase that number, said Machar, who emphasised that people should stay three to six feet (one to two metres) apart from others.
 "The only vaccine is social distancing," said Machar.
 To prevent the spread of the virus in South Sudan, President Salva Kiir last week imposed a curfew from 8pm to 6am for six weeks and closed borders, airports, schools, churches and mosques.
 On Sunday, Ethiopia reported its first death from the virus and announced five more cases, bringing its total to 43, most of them imported by travellers.
             News agencies
 	 
 https://www.aljazeera.com/news/2020/04/south-sudan-reports-case-coronavirus-200405140524402.html
