By freeing its people, China would react better to crises like coronavirus | The Japan Times
 15
 M/CLOUDY
 It is a pity that the Olympic Games in Tokyo had to be postponed to 2021. For the last several weeks, the word “coronavirus” has dominated global news and people have made songs and jokes in varied languages. Whether it is branded “Made in China coronavirus” or “Chinese virus,” the truth is Japan and the world would not have suffered its adverse consequences had China acted swiftly to contain it. Unfortunately, even the doctor who the Chinese authorities reprimanded for raising the alarm about the coronavirus threat died of this disease.
 My heart goes to all the Chinese and others in many parts of the world who have died from COVID-19 and are suffering from this invisible virus which can affect anybody.
 googletag.cmd.push(function() { googletag.display('div-gpt-ad-1499653692894-0'); });
 The totalitarian Chinese regime lead by Xi Jinping controls all information in China and the lands they occupy. Their insecurity is such that even in the face of the dreaded coronavirus, which does not make a distinction of whether you are a Chinese, Tibetan or from any other country, they reportedly still held a military drill in Tibet’s capital Lhasa a few days before March 10, allegedly to show their readiness to crush any demonstrations to mark the 61st anniversary of the Tibetan national uprising against China’s occupation. Regardless, the anniversary was marked by Tibetans and supporters in several cities around the world with public rallies and prayers.
 The coronavirus abundantly makes clear to the Chinese regime and the world that we are all the same human beings who together own the same Mother Earth. China owes an apology for bringing about the current coronavirus-related global suffering. The least it can do is to give to the Tibetans, Chinese and other oppressed people the basic human rights and freedoms that people in the free world take for granted. Only when people can freely express themselves can China in the future have the confidence to contain deadly and invisible killer diseases like the coronavirus and the leadership can then rest in peace.
 Tsering Tashi
 London
 The opinions expressed in this letter to the editor are the writer’s own and do not necessarily reflect the policies of The Japan Times.
 Strength in numbers: A more open approach to tracking the virus in Japan			
 Designing for good: Creators in Japan respond to the coronavirus			
 Resetting the political calendar after COVID-19			
 In era of COVID-19, a shift to digital forms of teaching in Japan			
 Where we want to go in Japan once this is all over			
 DEEP DIVE			
 Episode 47: The current state of Japan’s emergency			
 LAST UPDATED: Apr 15, 2020			
 Just for kicks: Japan’s sneaker obsession rebounds
 LAST UPDATED: Feb 29, 2020
 				Directory of who’s who in the world of business in Japan
 			
 LAST UPDATED: Apr 27, 2020			
 Sponsored contents planned and edited by JT Media Enterprise Division.
 広告出稿に関するおといあわせはこちらまで
 Read more
 https://www.japantimes.co.jp/opinion/2020/04/03/reader-mail/freeing-people-china-react-better-crises-like-coronavirus/
