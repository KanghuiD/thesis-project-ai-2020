      Pharmacists can play a bigger role in fighting coronavirus — we just need to let them - MarketWatch
     
 As Italy’s death toll from the novel coronavirus climbed to one of the highest in the world, its doctors made a plea to other countries: Manage the pandemic in the community, not in hospitals and emergency rooms.
 When people with COVID-19 show up at hospitals, they can spread the virus to other patients and health care workers. Italian authorities believe that the instinct to go to the emergency room first, even when the symptoms aren’t severe, contributed to the country’s current disaster.
 Like Italy’s, the United States’ health system is hospital-centered. As a result, pandemics can quickly overwhelm the hospital system, limiting its capacity to deal with other urgent medical conditions.
 But an alternative exists that could help manage the pandemic among the majority of COVID-19 patients with mild or moderate symptoms. It is the 67,000 community pharmacies in the U.S., whose numbers dwarf the 5,500 hospitals and 1,400 community health centers. They are staffed by 175,000 pharmacists frequently referred to as the most overtrained and underutilized health-care professionals in America.
 As a clinical pharmacist and a professor of pharmacy at the University of Southern California, I’ve seen firsthand how much health-care pharmacists can deliver and what they could bring to the fight against the coronavirus.
 Many pharmacies already offered testing for streptococcal and influenza infections, but it wasn’t until April 8 that U.S. pharmacies were authorized to order and administer COVID-19 tests.
 Pharmacists are now positioned to broadly perform testing that will provide vital data as public health professionals strategize how best to slow the virus’s spread. Testing is an established tactic for significantly reducing the size of an infectious disease outbreak. Widespread testing for the coronavirus in countries including China, South Korea, Singapore and Germany and the subsequent isolation of infected patients appear to be common factors in containing the spread and limiting deaths from the disease.
 But offering testing is just one step. Pharmacists have the knowledge to do more on the front lines of a pandemic. They are trained side by side with physicians and nurses, and many complete postgraduate residencies and fellowships.
 In a number of programs across the county, pharmacists have shown that they can dramatically improve control of medical conditions, avoid the need for hospital admissions and reduce health-care costs.
 When pharmacists in Los Angeles teamed up with barbershops to manage high blood pressure for black patients, nearly 90% of the patients reached their blood pressure target, compared to only one-third of patients who received traditional medical care.
 In Kern County, Calif., another program had pharmacists contact 1,100 fragile Medicaid patients shortly after they were released from hospitals to ensure they were taking their medications correctly. The patients’ hospital readmissions rate dropped by 32%, saving an average of $2,139 per patient.
 A University of Southern California study demonstrated that patients whose diabetes medications were managed by a pharmacist were three times more likely to reach their blood sugar targets.
 In the case of COVID-19, pharmacists could be a first point of contact for people who suspect they have been infected but aren’t seriously ill.
 Why wasn’t all this already happening on a wide scale? Why did it require the Public Readiness and Emergency Preparedness Act, brought on by the current pandemic?
 COVID-19 has created new and unprecedented challenges for our health-care system. With more than 90% of the U.S. population within 5 miles of a pharmacy, pharmacists are well-situated to step in to help beat this national crisis.
 Steven W. Chen is the associate dean for clinical affairs at the University of Southern California in Los Angeles. This was first published by The Conversation — “US pharmacists can now test for coronavirus. They could do more if government allowed it.”
 The coronavirus pandemic could cost a cumulative $9 trillion in global economic activity over 2020 and 2021—more than the size of the Japanese and German economies combined.
           Historical and current end-of-day data provided by FACTSET.
           All quotes are in local exchange time.
           Real-time last sale data for U.S. stock quotes reflect trades reported through Nasdaq only.
           Intraday data delayed at least 15 minutes or per exchange requirements.
         
 https://www.marketwatch.com/story/pharmacists-can-play-a-bigger-role-in-fighting-coronavirus-we-just-need-to-let-them-2020-04-13?mod=opinion
