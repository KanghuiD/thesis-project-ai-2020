Coronavirus (COVID-19) in Scotland | NHS inform
 What coronavirus is, how to avoid catching it and what you must do if you or other people you live with have symptoms	
 Use our self-help guide to find out what to do next if you or someone you live with has symptoms of COVID-19	
 What you should do if you have to stay at home because of coronavirus 	
 Why physical distancing is important to stop the spread of coronavirus and who is most affected	
 Why shielding is important to stop the spread of coronavirus and who is most affected	
 How coronavirus will affect your maternity care and how to reduce the risk to your baby	
 What you need to know about immunisation and screening during the coronavirus (COVID-19) outbreak	
 Guidance for people who work in a health or care setting	
 Information about anticipatory care planning during the COVID-19 pandemic	
 If you have a non-urgent question about COVID-19 ask the NHS inform website for an automated answer	
 Caring for a fever or cough at home and hospital discharge information	
 How to look after a cough at home, and when to seek help	
 How to look after yourself if you have a fever, and when to seek help	
 How to look after your child if they have a fever, and when to seek help	
 Use our self-help guide to find out whether you can care your symptoms at home or need to seek help	
 Use our self-help guide to find out whether you can care for them at home or need to seek help	
 Advice on looking after your mental wellbeing during the coronavirus (COVID-19) pandemic	
 Work through a self-help guide for depression that uses cognitive behavioural therapy (CBT).	
 Work through a self-help guide for anxiety that uses cognitive behavioural therapy (CBT).	
 It's normal to feel worried or anxious when times are hard, but there are things you can do to help.	
 Learn what to do if you are struggling to deal with stress.	
 Getting help and support to come to terms with tragic events experienced either in person or witnessed in the media.	
 Latest COVID-19 advice for employers and employees from Find Business Support	
 Latest COVID-19 travel advice from Fit for Travel	
 Latest COVID-19 stay at home advice for parents from Parent Club	
 How to become an NHS or local community volunteer in Scotland	
 Latest COVID-19 advice for people who are pregnant from the Royal College of Obstetricians and Gynaecologists 	
 Including: cancer, diabetes, heart disease, IBD, chronic kidney and liver disease, dermatological, neurological, respiratory and rheumatic conditions, and rare diseases	
 Latest guidance about coronavirus in Arabic, including shielding, social distancing and stay at home advice	
 Latest advice about coronavirus in Slovak, including shielding, social distancing and stay at home advice	
 Latest advice about coronavirus in Hindi, including shielding, social distancing and stay at home advice	
 Latest advice about coronavirus in Chinese (simplified), including shielding, social distancing and stay at home advice	
 Latest advice about coronavirus in Polish, including shielding, social distancing and stay at home advice	
 Latest advice about coronavirus in Romanian, including shielding, social distancing and stay at home advice	
 Latest advice about coronavirus in Urdu, including shielding, social distancing and stay at home advice	
 Latest guidance about coronavirus in BSL, including shielding, social distancing and stay at home advice	
 Latest guidance about coronavirus in Easy Read, including shielding, social distancing and stay at home advice	
 Latest guidance on coronavirus (COVID-19) in audio including advice on shielding, social distancing and stay at home advice	
 Posters, leaflets and other communication resources to raise awareness of the latest coronavirus guidance	
 https://www.nhsinform.scot/illnesses-and-conditions/infections-and-poisoning/coronavirus-covid-19
