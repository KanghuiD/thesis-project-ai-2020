Your Turn: April 26 - ExpressNews.com
 Don’t judge  vehicles
 Re: “Food Bank, Southside ISD find a way through storm,” by  Gloria Padilla, Other Views, Sunday:
 I’m reaching out because columnist Gloria Padilla gave us a very important lesson.
 I, too, noticed on TV and in pictures the big SUVs and trucks at Food Bank distributions and worked hard to recall that many of these people had jobs at one time. I tried not to voice my thoughts out loud. Why are these big vehicles in line for food?
 However, I remember a time when I drove cars that gave me gray hair from worry in my younger years. Cars that looked nice  but had issues such as overheating and other things. Situations like an accident or traffic pileup  had me holding my breath and sweating bullets from worry. Memories long, long past.
 Thank you for the  reminder that it’s not up to us to judge people, especially in this devastating time. Thank you for the reminder that we should all be grateful for what we have. Thank you for the reminder that we should share our time, love and resources to the extent we can with others less fortunate. Thank you for your column, which I’m going to cut out and keep!
 Sylvia Cruz
 All about them
 Re: “Open economy, allow ‘pursuit of happiness,’” Other Views, Wednesday:
 Rep. Chip Roy’s opinion piece is misleading.
 He states that in a nine-year period (1946-1955) there were 300,000 polio infections and 30,000 deaths. Coronavirus has been with us four months and we have more than 800,000 infections and as of Friday more than 50,000 deaths. Those numbers are not falling. Can’t he see this virus is much more contagious and deadly than even  polio?
 The sad part of this story is his grandfather saying: “I am giving these government folks until May 1 and then I’m doing whatever the hell I want to do.”
 That’s the problem with our “pursuit of liberty” — too many folks believe it’s all about them. What about those of us who believe the science and feel that a virus this deadly must be contained before it’s safe for us to get back to enjoying our lives with our families and friends?
 Opening the country too soon may see the loss of thousands more grandparents and grandchildren to this deadly scourge.
 Be smart, people. It ain’t nothing but a thing, and it, too, shall pass.
 Daniel Torres
 Who will be left?
 A number of people  want to leave the sanctuary of their homes to get the economy restarted sooner than later. Go right ahead, if you think it’s your right to endanger the rest of society.
 As Lt. Gov. Dan Patrick has said, it’s OK to sacrifice the elderly to get the economy going again. Just so we can return to the self-indulgent, unsustainable lifestyle so many of us have been living for so long.
 I understand people have bills to pay and many are truly suffering financially, but how are you going to pay for two to three weeks in the ICU on top of all your other bills?
 If enough people get sick or die of this disease, who’s going to be left to get the economy working again? The same way America has always replenished its hardworking population ever since the Mayflower landed on our shores — immigrants.
 Shirley Moehring
 Homeless cared for
 The city of San Antonio cooperates with a variety of nongovernment organizations such as Corazon Ministries, South Alamo Regional Alliance for the Homeless, SAMMinistries and many others to provide food, hygiene kits and masks to our homeless population.
 City workers such as those with CENTRO and the juvenile court, and social workers are allowed to volunteer at these hubs and other places.
 It is wonderful to live in a city that not only talks about compassion but organizes and reorganizes to meet the needs of our residents.
 Dixie Yarbrough
 On ExpressNews.com: San Antonio homeless at risk for virus could be housed in empty hotel rooms
 Distribute aid better
 It’s understandable that the need for the Paycheck Protection Program was urgent both from a financial and psychological point of view to provide the support so necessary to small businesses. However,  a little more time could have been spent to ensure the most effective delivery of the aid.
 I  call the Paycheck Protection Program the “Poorly Planned Program.” Not enough was written into the bill to provide for a good distribution of the funds. The distribution was based on a first-come, first-served basis, which favored  bigger businesses that had established banking relationships and probably experts to assist in the application.
 There seemed to be no attempt  to ensure a fair distribution of funds based on the size of the business, geographical location by state, urban versus rural setting,  ethnic background of the owner or  the type of business.
 The second attempt seems to address some of these concerns, but I still feel we are using a scattershot approach when a highly skilled target weapon is needed to maximize the results and the fairness.
 Ted Sincoff, Converse
 On ExpressNews.com: Some San Antonio-area small-business owners wonder, wait on aid
 https://www.expressnews.com/opinion/letters_to_the_editor/article/Your-Turn-April-26-15224088.php
