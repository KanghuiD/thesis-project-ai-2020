OPINION: We should ask questions about the coronavirus strategy - The DePaulia
  Blaine Wilhour, State Representative|April 9, 2020
 Antonio Perez/Chicago Tribune via AP
 Illinois Gov. J.B. Pritzker, foreground, along with Chicago Mayor Lori Lightfoot, third from left, and public health officials provide updated public guidance around the coronavirus, during a press conference at the Thompson Center, Friday, Feb. 28, 2020, in Chicago.
 Covid-19 is a serious disease. People have died and more people will die. We all acknowledge the importance of taking preventative measures to protect our most vulnerable. But when taking draconian repressive measures that have far-reaching negative effects on our lives and livelihoods, the threshold on reliable data needs to be high. Our leaders need to be over-the-top transparent about this data. We also need to be nimble enough and humble enough to adjust strategy, either direction, if further evidence indicates it’s prudent.
 It has been widely reported that the original shutdowns and the ’15 days to slow the spread’ policies were based on a model released by the Imperial College of London. This model was projecting 2.2 million people would die if we did nothing and over 1 million would die if we put in place some mitigation practices. 
 We can see why politicians who are risk averse by nature would take extreme measures based off these details from a highly respected institution. The problem is that this institution has completely abandoned these projections and replaced them with projections that look nothing like the original.  
 Now, the “gold standard” model that is cited by politicians and the media is the IHME-University of Washington model. In most of the states that are transparent about up-to-date hospital data, this model has also produced wild inaccuracies in its projections. For instance, in New York the model was over projecting hospital needs by four times the actual number.
 To say that the models our leaders cite as evidence to uniformly shut down our lives have been unreliable is being very generous.
 Shouldn’t we be asking questions about this? Shouldn’t we be concerned about placing so much trust in people who just haven’t been very accurate in almost everything they have said? 
 In an article published on Feb. 17, Dr. Anthony Fauci, director of the National Institute of Allergy and Infectious Diseases, said that the danger of coronavirus to the United States was “just miniscule.”
 On Feb. 26, Chicago Mayor Lori Lightfoot lit into the CDC for stoking fear about the coronavirus. 
 “I don’t want to get ahead of ourselves and suggest to the public that there’s a reason for them to be fearful,” Lightfoot said.
  As late as March 3, Gov. Andrew Cuomo was telling New Yorkers that it was safe to use public transportation. They might smell bleach on the buses, but other than that, riding the bus or subway was safe. 
 All of these individuals appear on TV daily to make even more predictions. There even is a movement in the Democrat party to find a way for Andrew Cuomo to replace Joe Biden as the Democratic nominee. Our own governor, JB Pritzker desperately wants the Cuomo status. 
 Cuomo has received praise primarily because he has been mostly measured and extremely transparent with his data. Conversely, Governor Pritzker has been either behind on collecting the proper data or just chooses not to share it in a manner that it can be scrutinized to find trends that could allow us to ensure good decisions are being made. His press conferences have been low information, highlighted by political outbursts deflecting all blame to the federal government. That’s not really useful information and it is counterproductive on so many levels.
 In the midst of this crisis, few if any have stopped to question the validity of the things they are being told. Many of the same people who dismissed the threat of coronavirus are forcing a shutdown of our lives with no plan for when and how to get our country back online, and the expectation is that we better just shut up and comply.
 Our media and political culture have strongly discouraged all dissent. A couple of weeks ago, I was asking for legitimate data from Pritzker’s administration to ensure good data-based decisions were being made, and I was roundly ridiculed by Springfield insiders calling me an “unserious idiot” who was sowing seeds of doubt. 
 The idea that we shouldn’t question any of these moves is dangerous, irresponsible and downright scary. Maybe we are doing everything right – maybe we are not – but asking questions and making sure we are making good decisions based on real data is absolutely essential.
  An Ivy League professor recently published an article in the Federalist stating that the longer we quarantine the entire population, the more we delay herd immunity which could lead to more people succumbing to the virus in the long run. The author suggested that a more targeted approach to quarantining might be the better solution.
 Is this a better approach? Maybe. Maybe not. But can we at least have the discussion? The author published the piece anonymously because of potential negative repercussions. We are intentionally destroying people’s lives, wrecking our economy and limiting personal liberty all on the assumptions and models being made by unelected scientists and bureaucrats without an open discussion of the deadly impacts of mass unemployment.
 As a recent Wall Street Journal article noted, “The prob­lem here is not an in­abil­ity to think clearly. It’s an un­will­ing­ness to be seen think­ing clearly.”
 I have found myself struggling with the concern that political viability could be taking a front seat to all other considerations.  Are the media and political cultural making it politically unviable to change course even if the greater good may demand it?  
 We should be asking questions and we should expect answers. None of the governors’ moves have had legislative input. As an elected member of the Illinois General Assembly, I have trouble getting answers to basic questions. Consequential decisions are being made that affect our districts without so much of a heads up.   
 On April 8, the governor’s 30 day emergency powers come to an end. It is time for the governor to bring the Legislature to the table. Our constituents deserve input on potentially opening parts of the state less affected by the virus. We want answers – real answers – on hospitalizations and other pertinent questions. We want a plan for how and when to open the economy and put Illinoisans back to work. If the data shows that we are not ready to reopen on a regional basis, fine — but it needs to actually show that. We need to set parameters and establish benchmarks to reopen and we need to start today.
 Of course we need to take this threat seriously, but we need to ask tough questions, demand answers and demand accountability. The future of our country depends on how we answer these questions.  
 
 Name (required)
 Sign me up for the weekly DePaulia newsletter and breaking news alerts.
 Opinions
 OPINION: For Generation Z, COVID-19 is the latest in a series of major traumas
 OPINION: Maintaining community is vital for mental health during coronavirus pandemic
 COLUMN: Graduating when the future is a true unknown
 OPINION: Unemployment benefits lag, leaving some residents questioning if they will ever come
 OPINION: How China’s oppression of Uyghurs is linked to economic convenience
 COLUMN: Stop saying ‘only’ the elderly and immunocompromised
 COLUMN: Coronavirus robs sports fans of their most cherished outlet
 Letter to the editor: Former DePaulia editor dies after battle with Alzheimer’s
 OPINION: Stan culture is a new way to obsess
 The DePaulia
 The Student News Site of DePaul University
 https://depauliaonline.com/47725/opinions/opinion-we-should-ask-questions-about-the-coronavirus-strategy/