New Yorkers Recommended to Cover Nose, Mouth When Outside
 All New Yorkers are now advised to cover their mouth and nose when in public to prevent potential transmission of the new coronavirus, Mayor Bill de Blasio said Thursday, regardless of whether or not they have symptoms of the virus.
  
 Face coverings are now being recommended by @NYCMayor and @NYCHealthCommr they say for two reasons: to prevent a potentially asymptomatic or pre-symptomatic person from spreading #COVID19 and as a visual reminder to keep a distance of 6 feet when out and about #socialdistancing
 COVER YOUR MOUTH AND NOSE
 Recent studies have shown asymptomatic people may be significantly contributing to the spread of COVID-19 and some health officials believe wearing masks could in fact decrease the spread.
 Citing such studies, the city is recommending New Yorkers use homemade material â such as a scarf, bandanna, or piece of clothing â to cover their mouth and face.
 âWhen you put on that face covering, youâre protecting everyone else,â de Blasio said.
 De Blasio stressed that New Yorkers should not use medical-grade masks â such as surgical masks and N95 masks, which need to be saved for health care professionals and other first responders on the front line of the pandemic. Citing the finite number of masks, the mayor said Wednesday that the city needs more than 5 million masks.
 âYou donât need something a first responder or a health care worker needs,â de Blasio said at his daily coronavirus briefing.
 Face coverings should not replace social distancing, de Blasio said, advising New Yorkers to keep at least six feet apart when in public, do not gather in crowds, and stay at home with some exceptions, such as to get groceries, food, or medicine. Face coverings are also not substitutes for people frequently washing their hands and using hand sanitizer.
 New Yorker are also advised to not share their face coverings. An individual should use them for a day, and then hand wash them in soap and water and let them dry. New York City Health Commissioner Dr. Oxiris Barbot said New Yorkers should have more than one face covering.
 As long as they do not get wet, paper masks are acceptable to use, Barbot said.
 In a 2015 study out of Australia "the authors speculate that the cloth masksâ moisture retention, their reuse and poor filtration may explain" an increased risk of infection.https://t.co/kBKqydOx3U
 Barbot said face coverings will help remind New Yorkers about the need to socially distance and to take other precautions as the city continues to grapple with the coronavirus crisis.
 âFace coverings are intended to do two things: for individuals who may be at the beginning of an illness and donât yet know it, so theyâre pre-symptomatic, ensures that they donât transmit infection to other people when they have to go out for essential activities,â Barbot said. âThe other thing these face coverings do, again if someone has to go outside, I want them to be a reminder for anyone that they might come into contact with to keep the distance of 6 feet.â
 The new directive comes as federal health officials consider changing facial covering guidance,  which so far had stressed masks should only be worn by those who are sick or working in health care. Up until Thursday, the city had told New Yorkers to follow that guidance.
 The mayor said he did not envision the city later fining New Yorkers for going outside without covering their face.
 The city says children should wear face coverings when outside as well.
 De Blasioâs order comes a day after Los Angeles Mayor Eric Garcetti issued a similar order in the nationâs second largest city.â
 The MTA and the union union representing subway and bus workers on Thursday told commuters to cover their mouths while riding mass transit.
 CASE COUNT
 Citywide, as of Thursday, there were 51,809 confirmed cases of COVID-19, the state said. 1,562 people have died in New York City, as of 5 p.m. Thursday, according to the city.
 Thursday morning, Gov. Andrew Cuomo reported 92,381 confirmed coronavirus cases and 2,373 deaths in New York state. That number was reported before the cityâs death total increased to 1,562.
 A week ago, New York City's confirmed case total was 23,112. A week before that: 3,615.
 A week ago, New York City's confirmed coronavirus death count was 365. A week before that: 26.
 The borough-by-borough breakdown, with some fluctuation in the numbers:
 The numbers of cases are expected to only increase exponentially over the coming weeks and months as more tests are conducted.
 13,383 people in the state are hospitalized with the virus, Cuomo said Thursday morning, and 7,434 patients have been discharged.
 ------
 FURTHER CORONAVIRUS COVERAGE
 What to Do If You Test Positive for COVID-19
 How Hospitals Protect Against the Spread of Coronavirus
 Coronavirus Likely Spreads Without Symptoms
 Coronavirus: The Fight to Breathe
 Cuomo Orders Non-Essential Workers to Stay Home
 NYC Businesses to Be Fined If Caught Price Gouging Face Masks
 MTA Crews Cleaning to Prevent Coronavirus Spread
 Cuomo Granted Broad New Powers as New York Tackles Coronavirus
 https://www.ny1.com/nyc/all-boroughs/news/2020/04/02/do-masks-prevent-coronavirus-new-yorkers-now-recommended-to-cover-nose-mouth-when-outside