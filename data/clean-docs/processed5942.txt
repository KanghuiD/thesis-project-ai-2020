Coronavirus in the UK live blog as it happened: call for Covid-19 care home deaths to be published daily as 521 more deaths announced outside of hospitals | Northampton Chronicle and Echo
 Concern over Northampton doctors' surgery not closing after GP tested positive for coronavirus
 From PE sick notes to inspiring others: How running changed Northampton woman’s life
 We will be providing live updates until 6pm this evening.
 Last updated: Tuesday, 14 April, 2020, 17:54
 The International Monetary Fund (IMF) has approved the cancellation of a total of £400 million worth of debt repayments from some of the world's poorest countries, in order to allow them to focus on the coronavirus crisis.
 The 25 countries helped by this include Afghanistan, Madagascar and Rwanda. They will each have six months' worth of debt repayments cancelled.
 The funds will come from the IMF's Catastrophe Containment and Relief Trust, which will use recent pledges of £150 million from the United Kingdom and £80 million from Japan
 Executive director, Kristalina Georgieva, said: "This provides grants to our poorest and most vulnerable members to cover their IMF debt obligations for an initial phase over the next six months and will help them channel more of their scarce financial resources towards vital emergency medical and other relief efforts."
 The director general of the Confederation of British Industry (CBI), Carolyn Fairbairn, has revealed she has been told firms will be able to submit furlough claims from next Monday (20 April).
 Speaking on BBC Radio 4's Today Programme, Ms Fairbairn said, "I think one of the things that we recognise and that we've been going back to Government on is that there are instances where it is worth trying to redraw the rules.
 "All I would say is that we're in a race against time on this and the more complexity is introduced into the system, the more the risk is that it won't deliver on time.
 "Now we've got firms, hundreds and thousands of them over the country at the moment waiting for the furlough scheme to pay out - that we hope will start next week, the first date should be next Monday when it begins to deliver.
 "And so if we add complexity into this we run the risk of it not working and hundreds and thousands of firms are waiting for it to deliver."
 The worldwide number of confirmed Covid-19 cases is closing in on two million, as New York's death toll topped 10,000 yesterday (13 April).
 According to an online dashboard that tracks the global number of confirmed coronavirus cases, maintained by Johns Hopkins University, the figure currently sits at 1.9 million cases across the globe.
 A new study of data from five European countries has found that around half of coronavirus deaths are happening in care homes.
 The research (collected from official sources by a London School of Economics-based team) found that 42 to 57 per cent of those who died as a result of the Covid-19 strain of the virus were care home residents.
 The countries studied by the International Long Term Care Policy Network (LTCPN) included Italy, Spain, Ireland, Belgium and France.
 The result comes after industry bosses in the UK said daily death tolls are "airbrushing out" hundreds of older people who have died in the care system. Care England has estimated there have been nearly 1,000 deaths from coronavirus in care homes.
 A microbiologist charged with the murder of his boyfriend asked to be released from custody so his research skills could be used in the battle against Covid-19, reports the Press Association.
 Wyndham Lathem asked a judge to be freed on bail, saying his academic knowledge could be useful and health issues put him at greater risk if he contracted the coronavirus.
 Lathem, a microbiologist, worked at Northwestern University and is accused in the 2017 stabbing of Trenton Cornell-Duranleau.
 The Chicago Sun-Times, citing court records, reported Judge Charles Burns denied granting Lathem bail in an emergency hearing conducted via teleconference.
 Michael Gove’s daughter was tested for coronavirus due to his critical role in the government’s response to the pandemic, Downing Street has said.
 Mr Gove was forced into a 14 day self-isolation last week with the rest of his family, after his child developed symptoms.
 Downing Street defended permitting Mr Gove’s daughter to be tested, despite thousands of NHS workers and care staff still waiting for tests, insisting that Mr Gove was playing a “central role” in the virus response, and so he and his family could be tested.
 A protocol drawn up by the UK’s four chief medical officers last month designated priority testing for core government members and their families, including the Prime Minister and Health Secretary Matt Hancock.
 Mr Gove’s daughter tested negative on Wednesday (13 Apr).
 The government has warned the nationwide lockdown will not be lifted this week and insisted that its plan to tackle the pandemic “is working”.
 Foreign Secretary Dominic Raab said the latest data suggested the UK was “starting to win” the battle against the virus, three weeks since restrictions were imposed.
 However, Mr Raab insisted the virus is not yet past its peak and that it is still too early to relax the measures, with reports suggesting the lockdown will be extended for at least another three weeks, until 7 May.
 Details of how lockdown restrictions will eventually be lifted still remains unclear, with the government refusing to outline its “exit strategy”.
 The government's scientific advisors are expected to meet this week to review the latest figures, but the Foreign Secretary stressed it is crucial the UK does “not take our eye of the ball” with regards to social distancing.
 The World Health Organisation said lockdown restrictions should be lifted slowly and not “all at once” to avoid a resurgence of the virus, and only if appropriate measures are in place.
 A total of 1.4 million people have submitted a claim for Universal Credit as a result of the coronavirus outbreak, Work and Pensions Secretary Therese Coffey said.
 The figure marks an increase of 200,000 on the total announced by Ms Coffey last week.
 She said the welfare system was capable of processing and managing the huge volume of claims, amid concerns many will face financial hardship if they are unable to access any of the government’s coronavirus bailout measures.
 She also noted that people have been claiming other support benefits, including Jobseeker’s Allowance and Employment Support Allowance.
 Governments across the globe should agree on a common standard for medical screening at airports, the boss of Heathrow has said.
 Chief executive John Holland-Kaye claimed that a single system to assess passengers’ health will help demand for air travel recover from the coronavirus pandemic.
 He suggested that now is the time to agree a “common international standard for healthcare screening in airports”, so that once the crisis recedes, travellers can move through airports with confidence.
 The call comes after tourists criticized the “shocking” lack of testing on arrival back in the UK during the outbreak.
 Two of the UK's largest care home providers have announced the death of 521 residents in recent weeks. 
 The figures come in the wake of calls for the government to announce the full scale of deaths outside of the NHS. 
 HC One which operates over 300 care homes across the UK announced 311 deaths up to 8pm on April 13. 
 MHA, based in Derby, announced 210 deaths across 131 of its homes. 
 More than 1.2 million mortgage payment holidays have been provided to home owners whose finances have been hit by coronavirus, according to a trade association.
 This equates to around one in nine (11.2%) mortgages across the UK now being subject to a payment holiday, UK Finance said.
 https://www.northamptonchron.co.uk/health/coronavirus/coronavirus-uk-live-blog-it-happened-call-covid-19-care-home-deaths-be-published-daily-521-more-deaths-announced-outside-hospitals-2537619
