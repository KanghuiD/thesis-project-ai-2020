Smoking Cannabis vs. Edibles in the Time of Coronavirus - The Weed Blog
 While it may seem obvious to most of us, inhaled smoke from any source is not doing our lungs any good as we face the potentially dangerous coronavirus pandemic. Experts are weighing in with specific advice.
 Smoking weed or tobacco, regardless of how often, increases the risk for more severe complications from COVID-19, the coronavirus disease.
 "What happens to your airways when you smoke cannabis is that it causes some degree of inflammation, very similar to bronchitis, very similar to the type of inflammation that cigarette smoking can cause," pulmonologist Dr. Albert Rizzo said, per CNN.
 "Now you have some airway inflammation and you get an infection on top of it. So, yes, your chance of getting more complications is there," said Dr. Rizzo, chief medical officer for the American Lung Association.
 Pulmonologists agree that even small amounts of smoke in the lungs can interfere with diagnosing COVID-19, if you’re showing symptoms. 
 "You don't want to do anything that's going to confound the ability of healthcare workers to make a rapid, accurate assessment of what's going on with you," said Dr. Mitchell Glass, a pulmonologist and spokesperson for the American Lung Association. 
 "COVID-19 is a pulmonary disease," said Dr. Glass, who noted that any type of inhalation, not just weed, can lead to complications.
  We all know about the bursting cough that often follows a deep inhale of a nicely rolled joint. The inevitable dry cough happens to be among the first symptoms of the coronavirus, which begs the question: Why make diagnosing it more difficult by mimicking one of the key symptoms? 
 Another physician, Dr. Jordan Tishler, CEO of Boston-based InhaleMD, weighed in on the question of cannabis as a way of dealing with stress and anxiety. 
 Dr. Tishler believes that it can be helpful, although he recommends small doses.
 “In my practice, studies have shown that small doses can be beneficial… something like two puffs on a cannabis flower vaporizer so that we’re not inhaling any smoke and we’re only getting the medicine we need,” Dr. Tishler told the Weed Blog.
 Jen Palmer, a naturopathic physician and Director of Education for Charlotte’s Web CBD, essentially agreed and also recommends hemp-based CBD gummies to help survive self-quarantine, self-isolation and to deal with nerve-wracking anxiety that often produces sleeplessness.
 “CBD can help strengthen one’s immune system by controlling stress, which can be one of the biggest factors that can impair your immune system,” Dr. Palmer told the Weed Blog.
 “CBD can really help produce a sense of calm,” Palmer said. “I recommend taking gummies in the morning then again in the evening because CBD doesn’t stay in your system for a whole 24 hours, so it is good to break up your servings. Twice a day is ideal.”
 Oils and tinctures are also easy-to-consume options to reap the beneficial effects of cannabis without the inhalation.
 https://theweedblog.com/culture/smoking-cannabis-edibles-coronavirus
