Sri Lanka in zero new Coronavirus case day amid anti-Covid-19 curfews |  EconomyNext
 ECONOMYNEXT – Sri Lanka recorded the first zero new Coronavirus case day on March 25, as the country was engaged in an intense battle to bottle the Covid-19 bug, and curfew was tightened to combat the possibility of small clusters in the Western Province of the island.
 On May 25, Sri Lanka recorded no new cases, Health Minister Pavithra Wanniarachchi said.
 It is the first zero-new-case day since Sri Lanka’s Wave II infection phase began with a tour guide with Covid-19 was confirmed on March 11.
 Sri Lanka has confirmed 102 cases since then and there are 255 cases in designated hospitals under observation.
 The second fully recovered case, a woman was released on March 25, taking the total recovered, along with a Wave I Chinese lady to three.
 Related
 Sri Lanka, Vietnam in Coronavirus battle of wits to bust the Covid-19 bug
 GMOA issues warning to public: follow advice on COVID19 or face a calamity
 Sri Lanka’s Government Medical Officers Association has said that up to 20,000 persons could have been potentially exposed to the disease, requiring quarantine.
 Authorities do not say that Sri Lanka has active community transmission yet.
 Why Sri Lanka does not advocate masks yet: touching it can get you Coronavirus
 Sri Lanka slapped indefinite curfews on the capital district of Colombo, Gampaha, and Kalutara, the most populated districts.
 Sri Lanka’s Derana TV, a private television station reported that authorities were focusing on three possible clusters.
 A cricket match between Royal College and St Thomas’s College took place on March 12 to 14, where the 14 day incubation period ends on March 28.
 An incident at a Re Church involving a father and son duo on March 12, where the 14-day quarantine ends on March 25 for any contacts to show symptom.
 In Ariyali, Jaffna a Swiss pastor had conducted a service on March 15, where 14 days complete on March 29.
 Curfew was lifted for six hours in many areas in the country for people to buy food and medicine but it continues in the Western province.
 Sri Lanka curfew times for Thursday and Friday
 Sri Lanka has allowed farmers to work and food trucks to move but others are not allowed to move between districts.
 Deputy Inspector General Ajith Rohana said 900 roadblocks had been set up around the country to stop inter-district travel.
 Sri Lanka’s banks have been asked to give a 6-month debt moratorium for three-wheelers, school vans and small trucks and a series of other reliefs.
 The International Monetary Fund and World Bank have asked creditor countries to give a moratorium for borrowers of 76 countries qualifying for International Development Association window credit which also includes Sri Lanka.
 World Bank, IMF asks creditors to give Coronavirus relief to Sri Lanka, 75 others
 Sri Lanka asks for international debt relief on Coronavirus hit
 President Gotabaya Rajapaksa had asked for a moratorium on debt repayments.
 Sri Lanka’s rupee is under pressure following large liquidity injections and the central bank have slapped import controls.
 Unlike India and many other countries Sri Lanka’s airport remains open for outbound tourists and international passengers providing and to bring vital medical and other supplies to the country.
 Sri Lanka Airlines, Colombo airport running lonely South Asia hub in Coronavirus battle
 Website 
 Kithmina Hewage- Institute of Policy Studies
 Darini Rajasingham-Senanayake 
 https://economynext.com/sri-lanka-in-zero-new-coronavirus-case-day-amid-anti-covid-19-curfews-62047/
