LETTER: Abortion, guns and the coronavirus crisis | Las Vegas Review-Journal
 Clinics, firearm shops still open
 Victor Joecks’ April 5 column noting that abortion clinics remain open during the shutdown because “killing children pays well” makes no mention of how the gun industry is also making millions by selling guns. Nor does it describe what someone’s skull looks like after being shattered by a bullet.
 Since the shutdown, calls to domestic violence hotlines have spiked. Add that problem to the fact that alcohol sales have increased 55 percent, combined with our access to guns also going unchecked, spells disaster for a wife, husband, child, neighbor or the police who must confront someone stuck at home, drinking alcohol and waiving a gun.
 It is time for Mr. Joecks to stop promoting his one-sided agenda during this COVID-19 pandemic.
 Ignoring reality on the environment.
 Do transporation officials know that some people still need to get around town?
 This walled-off, impregnable division of the state needs to be more transparent and offer people something besides deafening silence.
 Can you hear me now?
 So why are mining and construction carrying on?
 And it doesn’t work in wide-open Nevada either.
 He has no answers for what Nevadans want to know.
 Struggles with remote learning indicates it’s too big.
 Airlines, concerts, sporting events, third parties (i.e. StubHub), etc., continue to hold millions of dollars of the public’s money at a time when they need it most.
 Mining tax hike could ease the pain.
 Powered by WordPress.com VIP
 https://www.reviewjournal.com/opinion/letters/letter-abortion-guns-and-the-coronavirus-crisis-2005031/
