Page not found | Around the O
 The requested page could not be found.
 Around the O is the UO’s go-to place for information about the university, its people and the difference they make in Oregon and around the world. We bring stories of the university’s groundbreaking research and world-class faculty and students to the broadest possible audience, while also serving as the hub for news, announcements and information of interest to the campus community.
 Submit Your Story Idea
 around@uoregon.edu
 Visit us on YouTube
 UO prohibits discrimination on the basis of race, color, sex, national or ethnic origin, age, religion, marital status, disability, veteran status, citizenship status, parental status, sexual orientation, gender identity, and gender expression in all programs, activities and employment practices as required by Title IX, other applicable laws, and policies. Retaliation is prohibited by UO policy. Questions may be referred to the Title IX Coordinator, Office of Investigations and Civil Rights Compliance, or to the Office for Civil Rights. Contact information, related policies, and complaint procedures are listed on the statement of non-discrimination.
 https://around.uoregon.edu/content/uo-researchers-share-expertise-covid-19-april-20-24
