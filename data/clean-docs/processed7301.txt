The negative impact of lockdown on hospitals during the coronavirus pandemic - Washington Times
 Sign In
             Treatment of serious illness, chemotherapy and heart disease are deferred with consequences
 Joe Biden’s choice of a non-leftist running mate critical to winning the presidency
 In COVID-19 America, freedom is a privilege subject to government permission slips
 YouTube fuels COVID-19 protest fires by censoring doctors’ warnings
 What difference, at this point, does it make? 
 ANALYSIS/OPINION:
 It is finally official. For anyone who is still believing that the  political masters of the universe know everything and could be  completely trusted, I have news for you: They’re getting a lot wrong.
 All of us, including politicians and the medical establishment, have been behind the coronavirus eight-ball because of the nefarious actions and coverup by China. Now, the decision to go into a complete lockdown  is proving to have been injurious to the very institution that it was  supposed to save: The hospital and medical infrastructure.
 TOP STORIES
 Biden appears to fall asleep during town hall with Hillary Clinton
 Georgia kicks off chilling door-to-door COVID-19 blood collections
 Trump takes first steps to prepare for transition in case he loses
 Fox reported, “Mayo Clinic is furloughing or reducing the hours of  almost half its workforce as the nonprofit medical center tries to stop  the financial bleeding from the coronavirus pandemic. ‘Approximately  30,000 staff from across all Mayo locations will receive reduced hours  or some type of furlough, though the duration will vary depending on the  work unit,’ according to a statement on Wednesday from spokeswoman  Ginger Plumbo, as reported by Post Bulletin. Mayo Clinic will continue  to pay for the health care benefits for all of its employees while they  are off work, since these are furloughs and not layoffs.”
 This development is nationwide, impacting multiple other health care systems and hospitals.
 The Phoenix Business Journal headline said, “Arizona health systems succumb to Covid-19 with layoffs, pay cuts, furloughs.”
 The San Francisco Chronicle has an almost identical headline,  “Stanford hospital system to cut pay 20%, furlough workers during  coronavirus pandemic.”
 These are just a few examples of this troubling turn of events  nationwide. Let’s remember why we began the social distancing and the  eventual stay-at-home orders. It was to “flatten the curve,” which was  directly related to making sure that the health care system was not  overburdened by an influx of patients. We have accomplished that due to  the heroics of our medical frontline heroes as well as Americans  faithfully following the White House guidelines in an effort to keep  themselves, and their neighbors, safe.
 Now, as we begin to make an effort to emerge from this unnatural  state, Dr. Scott Atlas at the New York Post explains the deleterious effect  on individual Americans and the health care system itself in an opinion  piece titled, “Science says: It’s time to start easing the lockdowns.”
 “When states and hospitals abruptly stopped ‘nonessential’ procedures and surgery, that didn’t mean unimportant care. Treatments for the most serious illnesses, including emergency care, were missed. Some estimate about half of cancer patients deferred chemotherapy. Approximately 80  percent of brain surgery cases were skipped. Perhaps half or more of acute stroke and heart-attack patients missed their only chances for early treatment, some dying and many now facing permanent disability.  Transplants from living donors are down 85 percent from the same period  last year,” he wrote.
 “And that doesn’t include the skipped cancer screenings, avoided  childhood vaccinations, missed biopsies of now-undiscovered cancers  numbering thousands per week — and countless other serious disorders  left undiagnosed,” Dr. Atlas noted.
 At the beginning of this debacle it was impossible to know what the  end result would be. What we do know right now, is that the mortality  rate is much lower than the models told us; that the United States is  not northern Italy; that we were saved in part because we were not  burdened by a bloated, incompetent socialized health care scheme; and  that America’s frontline health care workers and their heroism have now  been made more public than ever.
 The success we have had is despite a bloated federal government and due to what is now dubbed in Washington as “Trump time,” reflecting the new ability to get things done quickly. The influence of the president  in slashing away regulations and demanding results has made the  difference in everything from testing, to the production of ventilators,  to the distribution of personal protective equipment, to say nothing of the lives saved because  of his quick action at the start in banning flights from China.
 It is also more obvious than ever that we will need a Sept.  11th-type commission composed of people other than politicians to assess what went wrong and what went right. As citizens, we have also learned that even politicians and medical experts are only human and will sometimes get things wrong. It reminds us why the Founders made us the  sovereign with the duty of overseeing those who would dare to think they  know better than us.
 • Tammy Bruce, president of Independent Women’s Voice, author and Fox News contributor, is a radio talk-show host.
                         
                           Click
                             here for reprint permission.
  Click to Read More
  Click to Hide 
 https://www.washingtontimes.com/news/2020/apr/29/the-negative-impact-of-lockdown-on-hospitals-durin/
