Coronavirus advice and guidance : University of Sussex
 Skip to main contentSussex homepage Staff
 Find everything you need to know about the Coronavirus (Covid-19) situation at the University and how to support yourself this term.
 Sussexsport continue to offer free online exercise classes that are open to all students. They’ve also partnered with Life Fitness to provide hundreds of online workouts that you can do any time you want.
 What’s available where you are and what to do.
 Advice for if you’ve left campus or are living off-campus.
 Guidance for keeping well and staying positive at this time.
 Teaching and learning updates for your course.
 Ways to get the most from studying while at home.
 Updates for doctoral researchers and supervisors.
 Visa information, travel arrangements and more.
 What to do if studying overseas or on a placement.
 The National Health Service (NHS) and Public Health England are the official sources of all UK health advice and guidance.
 The NHS has guidance on:
 Public Health England has assessed the Coronavirus risk in the UK as high.
 The Foreign & Commonwealth Office (FCO), part of the UK government, is the official source of travel advice and guidance.
 FCO information covers:
 We are following all the guidance from these organisations.
 University of SussexSussex HouseFalmerBrighton, BN1 9RH
 +44 (0)1273 606755
 information@sussex.ac.uk
 University of Sussex homepageA-ZAccessibilityStaff search
 YouTube
 Vimeo
 Download our app
 https://student.sussex.ac.uk/coronavirus/
