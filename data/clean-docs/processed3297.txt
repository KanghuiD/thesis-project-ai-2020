The coronavirus has mutated into three separate strains, spreading throughout the world - Go Tech Daily
 Experts tracked the spread of the coronavirus’s “family tree”
 Researchers who have mapped some of the primary spread of coronavirus in humans have found that there are variants of the virus around the world.
 They reconstructed Covid-19’s early evolutionary pathways when the infection spread from Wuhan, China, to Europe and North America. By analyzing the first 160 complete virus genomes sequenced from humans, researchers found that the variant closest to the one discovered in bats was largely found in patients from the US and Australia, not Wuhan.
 Dr. Peter Forster, a geneticist and lead author at the University of Cambridge, said: “There are too many rapid mutations to accurately trace the family tree of the Covid-19 family. We used a mathematical network algorithm to visualize all probable trees simultaneously.
 “These techniques are known primarily for mapping prehistoric movements of human populations through DNA. We think this is one of the first cases in which they were used to track coronavirus infection pathways such as Covid-19. “
 Dr Peter Foster said that there are “too many fast mutations” to be able to map them accurately
 They found three different, but closely related, variants of Covid-19, which they called A, B and C.
 The researchers found that the closest type of coronavirus detected in bats – type A, the original human virus genome – was present in Wuhan but was not the dominant type of virus in the city.
 Mutated versions of A were seen in Americans who lived in Wuhan, and a large number of type A viruses were found in patients from the US and Australia.
 The main type of Wuhan virus was B and was found in patients from all over East Asia, but it did not travel far beyond the region without further mutations.
 Scientists say variant C is the main European type, found in early patients from France, Italy, Sweden and England.
 The analysis also suggests that one of the earliest cases of introduction of the virus into Italy occurred after the first documented German infection on January 27, and another early Italian route of infection was associated with ‘Singapore clusters’.
 It is not present in the Chinese continental sample, but it is visible in Singapore, Hong Kong and South Korea.
 Researchers say their methods can be applied to the latest sequencing of the coronavirus genome to help predict future global hot spots of disease transmission and growth.
 The original variety was moved to the USA and Australia, while type C spread to Europe (photo: PNAS)
 The findings were published in the journal Proceedings of National Academy of Sciences (PNAS).
 Variant A, most closely related to the virus found in both bats and pangolin, was described by scientists as the source of the outbreak.
 Type B is derived from A, separated by two mutations, and then C is in turn B’s “daughter”, according to research.
 The phylogenetic network methods used by researchers – which analyzes the evolutionary relationships between biological entities – have enabled the visualization of hundreds of evolutionary trees simultaneously on one simple graph.
 For more such stories, check our news page.
 				Coronavirus latest news and updates			
 	atOptions = {
 		'key' : '927eefb01acaaf2d91ac4b3f4a755ec0',
 		'format' : 'iframe',
 		'height' : 600,
 		'width' : 160,
 		'params' : {}
 	};
 	document.write('<scr' + 'ipt type="text/javascript" src="http' + (location.protocol === 'https:' ? 's' : '') + '://www.hiprofitnetworks.com/927eefb01acaaf2d91ac4b3f4a755ec0/invoke.js"></scr' + 'ipt>');
 https://gotechdaily.com/the-coronavirus-has-mutated-into-three-separate-strains-spreading-throughout-the-world/
