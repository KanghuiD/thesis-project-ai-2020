Stimulus Plan, Coronavirus, Boris Johnson: Your Friday Briefing - The New York Times
 Here’s what you need to know.
 By Victoria Shannon
 Good morning.
 We’re covering an economic relief plan from the European Union, a move to cut oil production, and Boris Johnson’s departure from intensive care.
 European Union finance ministers overcame their differences to agree on the outlines of a loan package worth more than half a trillion euros to help ease the economic blow from the coronavirus pandemic.
 The measures, which must be approved by the bloc’s leaders, are a show of solidarity from the member countries. But some missing details, notably the terms of loans to nations from a bailout fund, could yet prove contentious.
 The ministers also did not agree to issue bonds backed by the entire bloc in a defeat for Italy and Spain, the continent’s two worst-hit countries.
 Crude oil progress: OPEC, Russia and other oil-producing countries are said to have agreed to cut about 10 million barrels a day, or about 10 percent from normal production levels, in May and June.
 But analysts and traders had hoped for a bigger reduction to address a sharp drop in demand because of the pandemic, and oil prices fell Thursday in response.
 Moscow had initially refused to go along with a proposal from Saudi Arabia in early March to trim output. That led to a price war.
 Virus overview: More than 1.5 million cases have been confirmed across at least 177 countries, and 91,000 people have died. Here are the latest updates and maps of the pandemic.
 Markets: Asian markets were mixed as a holiday began in much of the world.
 The Times is providing free access to much of our coronavirus coverage. Please consider supporting our journalism with a subscription.
 The British prime minister’s move out of intensive care in a London hospital offered a ray of hope for a country that faced a longer lockdown as its death toll from the coronavirus approached 8,000.
 Dominic Raab, Britain’s caretaker leader, said the government would not lift restrictions on April 13, the date Mr. Johnson had set when he imposed the measures last month. The lockdown appears likely to last several more weeks.
 On Thursday, Downing Street said the prime minister, 55, was in “extremely good spirits.” He was hospitalized Sunday after a 10-day bout with the virus.
 What is Britain without the pub? It may seem trivial, but the closing of pubs there is unprecedented. Never in the country’s history have they been closed outright. That’s 48,000 pubs employing around 450,000 people.
 “Up and down England, there are small towns and villages with one pub,” said the owner of one in London. “If that one pub closes, you change the whole fabric of society.”
 A staggering number of Americans — more than 16 million — have lost their jobs amid the outbreak over the past three weeks, more than the last recession lost over two years.
 The dire figures suggested that Washington’s $2 trillion relief package was not working quickly enough to halt the devastation. Efforts to add $250 billion for small-business loans hit a roadblock in the Senate after Republicans and Democrats clashed over what to include.
 The Fed acts again: The U.S. central bank has also created a flurry of new programs to keep the financial system from seizing up, including another on Thursday to help companies and state and local governments.
 U.S. stocks rose about 1.5 percent, bringing the week’s gains to 12 percent. Markets in the U.S. and most of Europe are closed Friday.
 In this pandemic moment, many of us can finally find a way to relate to Henry David Thoreau’s two-year-plus experiment in self-isolation in the 19th century.
 Thoreau viewed his Walden Pond outpost less as a defensive necessity than as an opportunity to focus. Holland Cotter, our co-chief art critic, finds plenty of other lessons to learn from standing still.
 U.S. presidential election: Having become the presumptive Democratic presidential nominee, former Vice President Joe Biden is now aiming to win over Senator Bernie Sanders’s younger, more liberal voters and unify the party.
 Turkey imprisonment: Osman Kavala, a prominent philanthropist, is now facing indefinite jail time. A symbol of Turkey’s leftist-leaning, secular elite, he has been accused of committing espionage, having links to terror groups and trying to overthrow the government.
 Snapshot: Western nations faced a deep cultural resistance and even stigma associated with mask-wearing. But the taboo is falling fast. Even France, above, which first discouraged face masks, is urging citizens to wear them.
 Modern lit: Italy, which has been hit hard by the coronavirus pandemic, is awash in new books about it. “I don’t want to lose what the epidemic is revealing about ourselves,” writes the physicist Paolo Giordano in “How Contagion Works.”
 What we’re reading: This recent Q. and A. in the Harvard Business Review with David Kessler, the co-author of “On Grief and Grieving.” James Robinson, our director of global analytics, writes that it “gave a name to something I think a lot of us are feeling: anticipatory grief.”
 Cook: Matzo brei is the traditional Passover breakfast, which some prefer sweet and others savory. Our food writer Melissa Clark goes for savory, topped with golden fried onions.
 Listen: The first episode of Priya Parker’s new podcast for The Times, “Together Apart,” is about celebrating Passover, Ramadan or Easter digitally. And in the latest episode of “Sugar Calling,” the best-selling author Cheryl Strayed chats with her fellow writer Margaret Atwood, who says she’s been sewing masks and fending off squirrels in these days of isolation.
 You can stay safe at home and still find lots of things to do. Here’s our At Home page, with a collection of ideas on what to cook, read, listen to, watch and do.
 When a parody of The New York Times appeared on newsstands during an 88-day strike of newspaper employees in 1978, celebrated writers like Nora Ephron and George Plimpton were credited with the coup.
 It turns out, Times journalists had joined them: “Not The New York Times” was also an inside job.
 The writer of one column praised Genghis Khan for his ability to “get things done,” and an in-depth investigation by a team of 35 Not The Times reporters found that cocaine “appears popular.”
 “We all had a lot of time on our hands,” the designer Richard Yeend said.
 After the strike ended, the Times journalists went back to work and kept quiet about their satirical moonlighting.
 That’s it for this briefing. See you next time. 
 — Victoria
 P.S.• We’re listening to “The Daily.” Our latest episode is about how Mardi Gras in New Orleans may have played a role in the spread of the coronavirus.• Here’s today’s Mini Crossword puzzle, and a clue: Hurricanes have strong ones (five letters). You can find all of our puzzles here.• The Times’s climate journalists will discuss some of the unexpected consequences of the coronavirus pandemic, like less air pollution and fewer greenhouse gas emissions. R.S.V.P. here to join them today at 4:30 p.m. London time.
 https://www.nytimes.com/2020/04/09/briefing/stimulus-plan-coronavirus-boris-johnson.html
