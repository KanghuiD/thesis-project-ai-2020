Australian government advice on how to avoid coronavirus-related scams and cyber threats | ZDNet
 The Australian Cyber Security Centre and the Australian Competition and Consumer Commission have both issued advice on how to avoid falling victim as Aussies deal with a new way of living and working.
 
                                    By
                
 Asha Barbaschow
             
                     
         
                                         |            April 6, 2020 -- 06:49 GMT (07:49 BST)
                                                                                     | Topic: Coronavirus: Business and technology in a pandemic
 As the nation adapts to new ways of working during the COVID-19 pandemic, the Australian Cyber Security Centre (ACSC) has published guidance for small and micro businesses along with advice on web conferencing. 
 In addition to asking small businesses to read the Threat Update: COVID-19 Malicious Cyber Activity -- published originally last month and detailing recent COVID-19 phishing attempts -- the ACSC has also reminded them to not provide personal information to unverified sources and never provide someone with remote access to a computer.
 The ACSC also suggested that small businesses keep on top of software updates, use strong passwords, back up data frequently, ask staff to avoid public Wi-Fi, and ensure biometric unlock options are used on any personal devices when they are used for work purposes.
 Between March 10 and 26, the ACSC received more than 45 cybercrime and cybersecurity incident reports from individuals and businesses. All 45 cases were linked to COVID-19 themed scams and phishing activity.
 With web conferencing tools being used more frequently, the ACSC also provided a checklist of what to consider before settling on a specific product, saying the use of offshore web conferencing solutions introduces additional business and security risks.
 "The number of Australians working from home continues to increase in response to the COVID-19 pandemic -- resulting in a sharp rise in the use of web conferencing by organisations and individuals to bridge gaps in communication," ACSC head Abigail Bradshaw said.
 "In deciding on a platform for teleconferencing, close attention should be paid to whether a service provider claims ownership of any recorded conversations and content, metadata, or files that are created or shared when using their web conferencing solution.
 "It is also critical that organisations correctly configure their selected service to maximise the security of conversations and data."
 The Australian Competition and Consumer Commission (ACCC) on Monday warned that scammers have been trying to exploit Australians financially impacted by COVID-19, with new superannuation scams being reported to Scamwatch in recent weeks.
 "Scammers are cold-calling people claiming to be from organisations that can help you get early access to your super," ACCC Deputy Chair Delia Rickard said.
 Meanwhile, the Australian Taxation Office (ATO) is coordinating the early release of super through myGov, and the ACCC said there is no need to involve a third party or pay a fee to get access under the scheme.
 Since the government's announcement in March, there have been 87 reports of these scams but no reported losses, the ACCC said.
 Similarly, security vendor Proofpoint last week revealed research that showed threat actors taking advantage of the Australian government's COVID-19 stimulus package.
 An example from the research revealed a new threat actor in Australia that was posing as a major national newspaper and offering information about the stimulus package. The information included a malicious attachment leading to a credential phishing page. 
 The researchers anticipate that threat actors will continue to modify their strategies as the news surrounding COVID-19 shifts.
 Please review our terms of service to complete your newsletter subscription.
                                         
 Security
 Kernel vulnerabilities in Android devices using Qualcomm chips explored
 ICEBUCKET group mimicked smart TVs to steal ad money
 SentinelOne researcher trolled in new MBRLocker ransomware campaign
 Coronavirus: Cisco wanted to delay patch for critical flaw in phone used by doctors
                     Cookies |
                     Ad Choice |
                     Advertise |
                     Terms of Use |
                     Mobile User Agreement
 https://www.zdnet.com/article/australian-government-advice-on-how-to-avoid-coronavirus-related-scams-and-cyber-threats/