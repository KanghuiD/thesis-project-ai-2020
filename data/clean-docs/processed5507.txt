	Coronavirus (COVID-19) and your rights - Consumer Affairs Victoria
 Renting, buying and selling property, building and renovating, owners corporations, retirement villages
 Buying and selling new and used cars, pricing, cooling-off period, warranties, leasing, trade-ins, auctions
 Apply for, renew, update and cancel a licence or registration, lodge an annual statement, legal responsibilities
 Register, update, manage, or search for an incorporated association, fundraiser, or patriotic fund
 Forms and publications, legislation, languages, scams, Koori, and disability resources, advice in a disaster
 Breadcrumbs
 A State of Emergency has been declared in Victoria due to the serious risk to public health posed by coronavirus (COVID-19). This page sets out rights and responsibilities in areas we are receiving increased enquiries about. Due to the regularly changing nature of circumstances in Victoria, we will update this information as often as possible. 
 New temporary laws exist to protect Victorian tenants, landlords and the rental market from the impact of the coronavirus (COVID-19) pandemic. 
 Tenants and landlords must comply with the Residential Tenancies Act 1997 and the COVID-19 Omnibus (Emergency Measures) Act 2020. The new laws apply for six months from 29 March 2020. 
 New rules cover a moratorium on evictions, rent relief for eligible tenants, suspension of rental increases, land tax reductions and deferrals for landlords, and a new dispute resolution process. Details are set out under ‘Information for tenants’ and ‘Information for landlords’ on this page. For details of the rent relief grants, which we do not administer, see DHHS' rent relief grants page.
 We encourage landlords, agents and tenants to try to reach an arrangement, put it in writing, and register it with us. If you can’t reach an agreement, you can use the new, free mediation service. See a:
 The laws described on this page apply to residential tenancies. For information about commercial leases, see Commercial and retail leases and visit the Victorian Small Business Commission.
 New laws help protect you as a tenant from eviction and allow you to negotiate a rent reduction with your landlord if you’re suffering financial hardship due to coronavirus (COVID-19).
 The laws apply for six months from 29 March 2020. This means you can’t be evicted if you are served a notice to vacate on or after this date because you can’t pay your rent due to coronavirus (COVID-19).
 These laws apply if you’re a tenant or sub-tenant in a residential property, or a resident living in a rooming house, social housing, under a site agreement at a caravan park, or in specialist disability accommodation.
 See the new process for negotiating a reduced rental agreement.
 We understand that changes to renting rules in response to the coronavirus (COVID-19) pandemic have created some uncertainty regarding the ability of tenants to terminate their tenancy agreement. New regulations clarifying breaking a lease are expected to be in effect by mid-May.
 We recommend that you wait until the new regulations are in effect before seeking to end your lease.
 The new laws apply for six months from 29 March 2020. This means if your landlord served a notice to vacate on or after this date, it does not have effect. 
 The landlord may seek an order through the Victorian Civil and Administrative Tribunal (VCAT) to terminate the tenancy under some circumstances (including if you damage the property, use it for criminal activity, if serious violence occurs, or if the landlord wants to sell or move back into their property).
 The landlord must not list you on a residential tenancy database (blacklist) if you are unable to pay rent because of coronavirus (COVID-19). For more information, view Tenancy databases (blacklists).
 Eligible tenants can apply for rent relief if they register an agreement they have reached with their landlord with us, or use our dispute resolution process to reach a reduced rent agreement. Learn more about the rent relief grant and check your eligibility under this scheme.
 The laws are in effect for six months from 29 March 2020. Your landlord cannot increase the rent during that time. They can increase the rent after that time, but only within the terms of your lease agreement and the Residential Tenancies Act. If your landlord issues you with a notice of rent increase after this time and you think that an increase is not reasonable, you can apply to us for a rent assessment.
 The laws bring forward the commencement of amendments contained in the Residential Tenancies Amendment Act 2018  to protect renters who are experiencing family violence or personal violence.
 Persons who have been subjected to family violence or personal violence may apply to VCAT for an order to terminate or vary the existing tenancy agreement. These applications are included in the list of urgent matters which VCAT will continue to hear whilst the laws are in effect.
 If you are experiencing family violence from someone who resides in your rental property, you can apply for an intervention order to exclude them from the property. To apply for an intervention order, visit the Magistrates’ Court of Victoria. You can also apply for an interim intervention order in person at a local Magistrates' Court.
 You can also ask questions by lodging an enquiry.
 New laws support landlords and help protect tenants in residential properties from eviction, other than in exceptional circumstances, and allow for the rent to be reduced if they are suffering financial hardship.
 The laws apply for six months from 29 March 2020. If you have served a notice to vacate on or after 29 March 2020 to a tenant who cannot pay their rent because of financial hardship resulting from the coronavirus, it is not effective.
 The laws apply to you if you are the landlord of a residential property, the operator of a rooming house, the owner of a caravan park, or a provider of specialist disability accommodation. 
 Contact the tenant to determine why they have stopped paying rent. You cannot evict the tenant for non-payment of rent unless you can establish that the non-payment is wilful and seek a Victorian Civil and Administrative Tribunal (VCAT) decision to have the tenant evicted. Wilful non-payment may include: 
 Please include appropriate evidence to support your reason for seeking a VCAT decision, such as correspondence from the tenant advising that they are not paying.
 We will determine whether one of the exceptions applies and refer it to VCAT for determination if appropriate.
 If the tenant is not paying rent due to the impacts of the coronavirus (COVID-19), you should try to come to an agreement with them about the rent they can afford to pay. 
 Under these temporary laws, tenants can break their lease early, or have their fixed term rental period reduced, in some circumstances. The process for claiming the bond remains the same.
 You still have the same responsibilities to your tenant that currently exist under your lease, including carrying out repairs.
 Contact your insurance and mortgage providers to determine the terms covering your situation.
 If you agree to reduce the rent, you may also be eligible for a 25 per cent land tax reduction. A deferment to 31 March 2021 may also be possible. For more information, visit State Revenue Office – Tax relief for eligible businesses.
 The laws are in effect for six months from 29 March 2020. You will not be able to increase the rent during that time. You can increase the rent after that, but only by a reasonable amount.
 You can also ask questions, by lodging an enquiry.
 Some not-for-profit agencies may help eligible tenants in financial crisis. For initial advice and referral to a financial counsellor, visit the National Debt Helpline website.
 The Tenancy Assistance and Advocacy Program (TAAP) can make a referral where appropriate. It supports tenants who are financially disadvantaged and vulnerable, and have certain specified tenancy issues.
 On 18 March 2020, the Victorian Government announced almost $6 million for Victorian homelessness organisations. The funding will help protect Victorians at risk of, or experiencing, homelessness because of coronavirus (COVID-19). 
 The Stay at Home Directions (No 4) dated 13 April 2020 (SAHD) restrict the number of people who may gather in indoor spaces to two people. Unless an exception applies, a person must not enter a residence unless:
 Clause 11(3)(c) of the Stay at Home Direction (SAHD) allows a person to permit another person to enter their place of residence if it is necessary for the second person to enter the premises for the purpose of their work. Therefore, a person may permit an estate agent to enter the person’s place of residence to allow the estate agent to undertake their work related to the place of residence.
 Clause 11(3)(d) of the SAHD allows a person to permit another person to enter their place of residence if the second person is entering for the purposes of attending a private inspection of the premises for the purposes of a prospective sale or rental of the property.
 Private inspections of an occupied/tenanted residential property are permitted. An inspection is only permitted where an estate agent and one other person (the person for whom the inspection is organised by private appointment) are present at the premises.
 Those in isolation or quarantine:
 This does not prohibit any inspection that is permitted by law without consent by the resident, such as:
 Private inspections of a vacant residential property are permitted. An inspection is only permitted where an estate agent and one other person (the person for whom the inspection is organised by private appointment) are present at the premises.
 Inspections by vendors or landlords
 A vendor or a landlord wanting to enter a property to inspect it is permitted if they have served a valid notice to enter the premises under the Residential Tenancies Act 1997.
 Inspections by estate agents
 An estate agent is permitted to enter residential premises to exercise lawful duties as part of the exercise of their occupation, including to inspect a property on behalf of a landlord or vendor.
 Restrictions on indoor gatherings do not apply to an estate agent entering an indoor space where it is necessary to enter a property in the exercise of their occupation. Accordingly, an estate agent may enter premises to conduct an inspection on behalf of a landlord or vendor irrespective of the number of residents of the property present at the time.
 More activities may be restricted as the coronavirus (COVID-19) progresses. Estate agents should monitor the DHHS website for the most up-to-date information and advice.
 Other areas of the economy have also been asked to show more goodwill during this emergency.
 If your association is scheduled to hold a meeting, you must follow the restrictions on gatherings.
 Under the restrictions you must not enter any single, undivided indoor space if more than one other person is in that space.
 If you need to hold a meeting you can:
 If this emergency makes it difficult to engage an independent accountant or auditor to review your financial statements, you may request an extension. Complete our Extension of time to hold an AGM or lodge financial statements form (Word, 97 KB) and send it to cav.registration@justice.vic.gov.au.
 From midnight on 30 March, there are restrictions on gatherings, to protect members and the public and combat the spread of coronavirus (COVID-19).
 Under the new restrictions a person must not enter any single, undivided indoor space if more than one other person is in that space.
 If your co-operative is scheduled to hold a meeting (subject to its rules), you should choose an alternative approach. You must adhere to the requirements of the rules of the co-operative and the Co-operatives National Law (Victoria) (the CNL) if you:
 If you are experiencing difficulties engaging an independent accountant or auditor to review your financial statements, you may also seek an extension. 
 To seek an extension please complete the Application for extension or shortening of time form (Word, 128 KB) and submit to cav.registration@justice.vic.gov.au.
 There are restrictions on gatherings that apply to annual general meetings (AGMs) and other general meetings. 
 Under the restrictions you must not enter any single, undivided indoor space if more than one other person is in that space.
 Holding a meeting
 You must hold the meeting by teleconference or other remote technology.
 Some matters do not need an AGM and can be resolved by ballot. You can conduct a ballot by:  
 Owners corporations should also consider what powers and functions they can delegate to the committee and/or manager. This can ensure effective administration between meetings.  
 Owners corporations should consider limiting the use of their power to decide that certain matters can only be dealt with at a general meeting. This will help provide flexibility for decision-making while social distancing is needed. 
 For general information, view Meetings and committees.
 A registered owners corporation manager must pay the prescribed annual registration fee to the Business Licensing Authority (BLA) on the anniversary of the date thpe manager was last registered.
 There are restrictions on gatherings that apply to annual meetings or any other meetings. 
 If you cannot postpone a meeting, you can meet remotely via a:
 Managers should contact residents as soon as possible to agree on a preferred approach.
 No, retirement villages in Victoria do not fall within the scope of the Care Facilities Directions.
 The directions were released by Victoria's Chief Health Officer on 7 April 2020.
 A person who owns, controls or operates a brothel, sex on premises venue, strip club, escort agency or other adult entertainment venue must not operate that facility from midnight on 30 March until midnight on 11 May 2020.
 A Restricted Activity Direction issued on 30 March 2020 requires escort agencies and brothels to cease operation until midnight on 11 May 2020.
 Most Victorian consumers and businesses do the right thing. We urge buyers and sellers to consider the impact of excessive pricing and panic buying. We support measures to ensure supply and demand of products is fair for everyone.
 We encourage all businesses to set fair prices during this emergency. 
 Businesses can legally set their own prices, but must not mislead consumers about the reason for increased pricing. Excessive pricing by a business may be found to be unconscionable if the product is critical to help save or protect vulnerable consumers. This would make the high prices illegal. See our information about Price rip-offs.
 Private sales are not covered by Australian Consumer Law. Private sellers can legally list face masks, hand sanitiser and toilet paper on classified sites for significantly more than they sell for in stores.
 We urge consumers not to pay exorbitant amounts to people reselling important sanitary products. Retailers are aiming to keep shelves stocked with important products for those who need them.
 We recommend buying tickets from ‘authorised sellers’. Tickets sold by unauthorised sellers are not always legitimate. This could make it difficult to get a refund in the event of a cancellation. If purchasing online, use a secure connection.
 Insurance may provide further protection for consumers who cannot attend an event due to coronavirus (COVID-19). Check any exclusions carefully, as many policies do not cover ‘known events’.
 There is a significant increase in malicious activity surrounding coronavirus (COVID-19), including misinformation made to appear to be from trusted sources such as government agencies and media outlets.
 Protect yourself against online scammers, by following these quick tips:
 You can report a potential scam via our Report a scam page. Learn more about current coronavirus (COVID-19) scams from the Scamwatch website. 
 For accurate information about coronavirus (COVID-19), use the:
 Last updated: 8 May 2020
 https://www.consumer.vic.gov.au/resources-and-tools/advice-in-a-disaster/coronavirus-covid19-and-your-rights
