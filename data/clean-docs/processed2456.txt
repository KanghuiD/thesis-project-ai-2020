Opinion: The amazing evil genius of Coronavirus - Africa Feeds
 While not technically alive, there’s an evil genius to viruses that never ceases to amaze me. It’s one reason I became a virologist.
      (adsbygoogle = window.adsbygoogle || []).push({});
 A recent Nature paper reveal a remarkable trick SARS-Cov-2 learned that makes it nastier than the first SARS.
 Both viruses attach via their external spike protein to a protein on our cells called ACE2. Think of it as a particular doorknob that the virus knows how to turn.
 Every virus has a particular type of doorknob that it attaches to and turns so it can enter (infect) a cell.
 RelatedItems
 Boris Johnson is out of hospital after surviving Covid-19 battle
  16 mins ago
 Covid-19 genome traced to bats used for ‘U.S. funded experiment’ in Chinese lab
  2 hours ago
 Ghanaian scientists make ‘genetic material’ analysis of Covid-19
  12 hours ago
 Sudan says it needs $120 million to fight coronavirus
  17 hours ago
 var jnews_module_41399_2_5e9311ca9748b = {"header_icon":"","first_title":"Related","second_title":"Items","url":"","header_type":"heading_6","header_background":"","header_secondary_background":"","header_text_color":"","header_line_color":"","header_accent_color":"","header_filter_category":"","header_filter_author":"","header_filter_tag":"","header_filter_text":"All","post_type":"post","content_type":"all","number_post":"4","post_offset":0,"unique_content":"disable","include_post":"","exclude_post":41399,"include_category":"","exclude_category":"","include_author":"","include_tag":"37895,40506","exclude_tag":"","sort_by":"latest","date_format":"ago","date_format_custom":"Y\/m\/d","pagination_mode":"nextprev","pagination_number_post":"4","pagination_scroll_limit":0,"scheme":"","column_width":"auto","title_color":"","accent_color":"","alt_color":"","excerpt_color":"","css":"","paged":1,"column_class":"jeg_col_2o3","class":"jnews_block_20"};
 For example, HIV has to turn two doorknobs: CD4 and typically CCR5. MERS attached to one called DPP4. All our cells are covered in all kinds of proteins that make them distinctive from one another. Those proteins aren’t there to let in viruses.
 They have all kinds of functions such as regulating blood pressure, our immune response, sugar levels in our blood, etc.
 But viruses have evolved to grab onto one or more proteins to gain entry into cells, where they hijack the cell’s machinery to make more viruses. (Viruses can’t replicate on their own & hence, are not quite alive).
 So what viruses see are a bunch of houses w/ varying doorknobs & they try to spread through the neighborhood (our bodies) looking for houses (cells) covered in the doorknobs that they know how to turn.
 HIV enters T cells b/c T cells are covered in CD4 & CCR5 doorknobs that HIV knows how to turn. That makes HIV the kind of virus that destroys our immune system.
 Well, the ACE2 doorknob that SARS-1 & SARS-2 use is present on a variety of cells, including those in our lungs & throat.
 SARS-1 would enter a person via a droplet in the air (from cough) & quickly start infecting lung cells, causing severe damage person could really feel (i.e. become symptomatic).
 Covid-19: 99% of Italian patients who died had existing illnesses
 In other words, SARS-1 quickly made its presence known. In some patients, SARS-1 would go into the upper airways to replicate from where it could spread to others with a cough (or just breathing).
 But b/c SARS-1 patients got very sick from all the virus replicating in their lungs, they were quarantined before others got close enough to get sneezed or coughed on SARS-2, on the other hand, takes up residence in the throat cells first, which doesn’t cause significant symptoms.
 The person can remain asymptomatic or might not think they have anything worse than a cold. And from that person’s throat it can readily spread to others.
 Over the course of a week, in some patients, it will move into the lung neighborhood and replicate just as SARS-1 would, causing severe symptoms, by which point the person is quarantined, but no matter since it had successfully spread So SARS-1 was a comparatively dumb virus.
 It went straight for the lungs, announced itself before it could spread to others, and so got social distanced into extinction. But SAR-2, the one plaguing us now, is stealthier, spreading first before revealing itself (and causing harm).
 What’s the take-away for all of us? It’s that beating this virus means social distancing & wearing masks even if we think we aren’t infected.
 Because we might be. The virus might be replicating in our throats without us knowing (that’s its evil plan!), so put up a roadblock.
 These insights come to us thanks to the hard work of researchers in Germany who very carefully studied the replication patterns of SARS-CoV-2 in a small number of patients, measuring everything they could daily over the course of their infections.
 Another reassuring insight is that, as these patients’ immune systems revved up and produced antibodies, they stopped producing viable viruses.
 The researchers could still detect bits of the virus, but they couldn’t find evidence that, among those bits, there was any virus that could actually infect cells.
 Peter Kolchinsky is author of this opinion piece and a Virologist
 This isn’t a surprising result. It confirms what we already know about how we have survived viruses like this since humans first evolved. Our immune systems fight them off with antibodies.
 So when we have vaccines that prompt our immune system to produce high levels of such “neutralizing” antibodies that can inactive SARS-2, we’ll have a head start on the virus.
 Should any virus enter our bodies, those antibodies will help shut it down before it causes harm & keep it from spreading to others.
 Some will say “what about antibody-directed enhancement”. That’s a known risk that can be detected in early animal & human studies.
 With all the vaccine programs out there, we’ll have a good one (or several) that protects us from this pandemic and from future waves of SARS-Cov-2.
 Some have asked, why would SARS1 and 2 infect throat & lung cells differently if they both use ACE2. Great question. There’s a lot more to viruses than which doorknobs they turn.
 Besides spike protein, coronaviruses have many other parts that engage with our machinery. Let’s say SARS2 climbs stairs better & throat cells are a duplex (has stairs). So SARS1 gets tripped up in throat cells & prefers single-story lung cells.
 Also, some have questions about whether virus mutates too fast for a vaccine. Short answer, no. We’ll have a vaccine. Here’s detailed explanation.
 And for those wondering how America’s insurance system is impacting this pandemic, I’ve written on that. Answer – not good.
 And finally, if you like complex stuff made simple, have time for reading, & wonder what America can do to fix its healthcare affordability crisis, check out my new book. It’s like the Freakonomics of Drug Pricing. Won’t think of EpiPen & DTC the same.
 And coincidentally, for this book, I researched and wrote about the most widely prescribed generic drug in America called lisinopril… it inhibits ACE1 (related but not same as ACE2) to control blood pressure. It’s just one story among many.
 Author: Peter Kolchinsky (Virologist and Author, The Great American Drug Deal)
 Africafeeds.com is a member of the Africa Feeds Media Limited, an independent digital news media brand. It's headquarters is in Ghana.
 https://africafeeds.com/2020/04/06/opinion-the-amazing-evil-genius-of-coronavirus/
