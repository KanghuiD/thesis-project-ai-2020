Opinion: Gov. Kemp: Don’t risk virus’ resurgence here
                                         WATCH: GBI holds news conference with updates on arrests made in jogger's shooting death
 We all look forward to the day we can put this pandemic behind us, but given Georgia’s performance so far, Gov. Brian Kemp is moving too soon and confusing citizens. He is risking a resurgence of the coronavirus in our state.
 Yes, the statistical models have started to bend in our favor. While just models, these different analyses of COVID-19 cases and deaths, suggest that the state may be past its peak and a dire situation is getting better.
 If that is so, then all Georgians should celebrate that the sacrifices they’re making are working. As citizens, we’ve come together and our sheltering in place has been worth it.
 We can’t forget that people are still contracting the disease and dying from it. That means that while the pace has slowed, we’ll have to continue to follow the policies that public health experts have urged, lest we risk a resurgence of the virus.
 » LATEST NEWS: Stay informed on coronavirus
 Dr. Marc Lipsitch, a professor of epidemiology at Harvard’s T.H. Chan School of Public Health, put it this way: “If you open up enough it’s almost for certain” the virus will hit Georgia again. “It’s just waiting for more susceptible people and more contacts. That’s how viruses work.”
 “It’s a very big risk,” Lipsitch said.
 All the information available indicates that even those without symptoms can spread the virus; that’s what social distancing is all about.
 And the vulnerable among our population remain at great risk.
 The governor’s passion about the economic damage to Georgia was obvious at his Monday press conference.
 “We’re talking about somebody who has put their whole life into building a business,” he said. “That has people that they love and work with every single day … that are at home, going broke, worried about whether they can feed their children, make the mortgage payment.”
 The governor’s order will allow gyms, bowling alleys, salons and some other indoor facilities closed under his shelter-in-place order to resume operations by Friday as long as they comply with social distancing requirements and meet other rules issued by the state.
 Under Kemp’s measure, businesses must meet 20 separate guidelines to reopen. They include screening employees for signs of illness, requiring more hand-washing and prohibiting gatherings of workers.
 And restaurants, which were banned from in-person dining, will be allowed to reopen on April 27 if they meet guidelines his office will release later this week. Theaters will also be covered by those new standards. Bars and nightclubs will stay closed.
 “I didn’t do this unilaterally,” Kemp said.
 The governor said he spoke to governors of five other Southern states, all fellow Republicans, about how to “safely reopen our states.”
 He’s also apparently consulted business leaders who are part of his economic impact committee.
 He noted that he’d talked to health experts, but only mentioned Kathleen Toomey, commissioner of the Georgia Department of Public Health.
 The governor is right to take charge of efforts to reopen the state, especially since the president has indicated he will leave it up to governors.
 And he should be less ambiguous in this effort than he was in his decision-making and guidance about closing down the state. But now his shelter-in-place order will stay in place, despite his easing of restrictions. What’s a citizen to think?
 Kemp said Georgians “have good common sense and we’re trusting them to use it.” We agree, but his plan is likely to cause confusion. More importantly, he’s overlooking some difficult facts about Georgia:
 The state ranks among the nation’s worst for its number of COVID-19 cases and the deaths.
 The state ranks in the bottom 10 in testing per capita. Experts say the state still lacks the testing capacity needed to detect the true scope of the disease, as well as the resources required to trace the contacts of infected persons, in order to isolate future outbreaks.
 Georgia has many more cases than we know about because only a small fraction of us have been tested. In fact, only those with the strongest symptoms qualified for testing until recently.
 » NEW SECTION: People Helping People
 Let’s also remember the state’s weak data gathering. For example, until last week, the state only attributed death to COVID-19 when diagnoses were confirmed by laboratory tests. By omitting symptomatic people who died before they could be tested, the state understated the problem and lacks a true picture of the pandemic.
 Some might excuse the governor for these problems; he inherited a state with a history of poor health outcomes, weak public health infrastructure and underfunded agencies. Others might say he’s done little to improve things during his tenure. That debate will have to wait.
 But when he referred to improvements in contact tracing and offered few details, he seemed to brush past showing how the state would develop an important anti-pandemic weapon that heretofore has hardly been mentioned. The contact tracing and ramp-up in tests promised by Kemp are still weeks away, even if everything goes smoothly, and nothing has gone smoothly so far.
 The governor and other leaders must acknowledge realities around the pandemic in Georgia. We lack systems to conduct widespread testing, which is the single most important matter as we consider reopening our state.
 The governor likes to talk about the importance of “chopping wood” to conquer big problems. That’s what’s required here.
 There are some crucial things that he should tend to:
 Testing. Tests of several types will be needed in great volume. Georgians will need to know if they’ve had the virus and now have immunity. They need to know if they risk exposure or exposing others.
 Goals, not dates. The coronavirus isn’t following the calendar. The state’s plan should be built around infection rates, testing data and ongoing plans to monitor new cases. Arbitrary dates aren’t helpful and could create false hope or a temptation to abandon practices that are working.
 Many Georgians find themselves in increasingly desperate economic condition; we are all anxious to get back to work and to reopen businesses.
 Gov. Kemp may have been too willing to accommodate us at the risk of our health and lives.
 The Editorial Board.
 		        See offers.
 Your subscription to the Atlanta Journal-Constitution funds in-depth reporting and investigations that keep you informed. Thank you for supporting real journalism.
 Download the new AJC app. More local news, more breaking news and in-depth journalism.
 The Atlanta Journal-Constitution.
 			Visitor Agreement and
 			Ad Choices. Learn about
 			careers at Cox Enterprises.
 		
 https://www.ajc.com/news/opinion/opinion-gov-kemp-don-risk-virus-resurgence-here/duHJgau1fDo1RC3ykkX96I/
