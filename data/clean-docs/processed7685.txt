Coronavirus school closures show need to end social promotion | Las Vegas Review-Journal
 Tens of thousands of elementary school students weren’t going to be reading on grade level — before losing two months of instruction.
 Each year, Nevada’s public schools shuffle tens of thousands of failing students onto the next grade. Imagine how destructive that practice is going to be after students have been out of school for five months.
 During the second week of distance learning, the Clark County School District contacted just 230,000 of its 320,000 students. Keep in mind that this figure doesn’t represent the number of students actively engaged in learning activities. It simply means a district employee attempted to reach out to a student or his family. Best-case scenario: One-third of district students are missing the last two months of academic instruction.
 It’s worse than that, obviously. Worksheets can’t replace textbooks. Some families don’t have computers. It’s harder to give struggling children individualized attention.
 This is especially concerning for elementary school students. Up until the third grade, you learn to read. After the third grade, you read to learn. It’s not optimal for high school students to miss out on learning about the periodic table. Assuming they can read, however, they can make up that work on their own. It’s a much bigger concern if the school shutdown means Little Susie never learns to read. You don’t solve that problem by handing 12th graders diplomas filled with words they can barely decipher.
 The CCSD wasn’t starting from a position of strength either. Just 49 percent of district fourth graders were proficient in reading last year, according to the Nevada Department of Education. That could be on the high side. The Nation’s Report Card found that 30 percent of district fourth graders were proficient in reading.
 This is why Gov. Steve Sisolak should use his emergency powers to order Nevada schools to end social promotion for students in early elementary grades. Social promotion allows schools to advance students to the next grade even when they are failing academically.
 Districts should test this year’s first graders through third graders when they return to school in August. Students who haven’t mastered the reading skills necessary to succeed in the next level should repeat their previous grade.
 Critics of retention often make this seem like a punitive action. Nothing could be further from the truth. The easy solution for adults in the system is to pass students along. Parents don’t get upset. Underachieving teachers aren’t questioned about their performance. District officials use low test scores as evidence they need more money.
 The losers in the status quo are the students, who don’t develop the foundational skills needed to succeed academically. You hold kids back because you want to give them the best chance to succeed.
 The other objection is that failing students will hurt their self-esteem. That’s at least true temporarily. It always stinks to fail at something. But the alternative isn’t success. It’s condemning students to walk into school every day knowing that they can’t keep up. That they’re going to fail before the school day has even started. That’s not great for self-esteem either.
 In this case, coronavirus provides a way to mitigate that concern. No one has to feel bad about repeating a grade because a once-in-a-lifetime pandemic upended the education system. Blame that factor if you’re so worried about self-esteem.
 Another benefit of this plan is that it doesn’t cost anything extra in the short-term.
 Coranavirus has caused enough damage. Don’t let it condemn tens of thousands of Nevada’s school children to a lifetime of reading struggles.
 Gov. Steve Sisolak doesn’t have a plan to open Nevada. But he’s rhetorically committed to extending the lockdown through June — and potentially much longer.
 The fiscal situation facing governments throughout Nevada is going to be ugly. It shouldn’t be an excuse to eliminate property tax caps.
 Gov. Steve Sisolak is being dishonest in a desperate attempt to hide that he successfully recommended cutting staff in Nevada’s Unemployment Insurance office.
 Nevada has flattened the curve and its economy. Of course, it’s time to talk about reopening.
 Nevada stockpiles potential coronavirus drug Sisolak restricted
 Coronavirus can’t stop Easter and hope it provides
 Some reasons to be optimistic about Nevada’s fight to halt the spread of coronavirus are beginning to emerge.
 In an effort to stop coronavirus, Gov. Steve Sisolak has ordered Nevadans to stay at home and non-essential businesses to close. He’s allowing abortion clinics, however, to remain open.
 If only the national mainstream media was as incredulous about China’s coronavirus claims as they are about President Donald Trump’s.
 Powered by WordPress.com VIP
 https://www.reviewjournal.com/opinion/opinion-columns/victor-joecks/victor-joecks-school-closures-highlight-need-to-end-social-promotion-2015169/
