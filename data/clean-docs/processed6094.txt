Coronavirus NJ: Opening parks will improve people's immune system
 Gov. Murphy’s order to close the county and state parks misses the point. His duty is to protect the citizens of New Jersey. Closing the parks does not protect people, it harms people.
 A person’s best defense against the coronavirus is the person's own immune system. There are things a person can do to improve their immune system, such as getting enough sleep and eating a balanced diet.
 Regular exercise, such as running and walking, improves an individual’s immune system as well. Running also strengthens one’s respiratory system, and COVID-19 is a respiratory disease. In addition, Vitamin D strengthens the immune system. Direct sunlight is a source of Vitamin D. Closing the parks denies the people of New Jersey a safe place to exercise, and puts them at a greater risk from the coronavirus.
 Stay home:  Murphy orders all state and county parks closed
 A number of neighborhoods in New Jersey do not have sidewalks, forcing people into the street with the traffic. The CDC reports thousands of pedestrian fatalities each year from traffic accidents, with tens of thousands other accidents resulting in ER visits.
 Social gathering can and should already be banned in the parks under the social distancing orders. Enforce the rules. Punishing those people trying to protect themselves by running, walking, biking, etc., is not fair, and does not make sense. 
 Editorial:  Was Murphy's decision to close all state and county parks overkill?
 The point of flattening the curve is to lessen the pressure on the medical resources in the state. Healthy people are less likely to require hospital care, thereby flattening the curve.
 Murphy needs to open the parks up to protect all the people of New Jersey.
 Brian Hass
 Hazlet
 https://www.app.com/story/opinion/readers/2020/04/11/coronavirus-nj-opening-parks-improve-peoples-immune-system/2975584001/