Contributors | TheHill
 President Trump and like-minded people in Congress recognize that space exploration must be made to pay in order to be sustainable.
 The kind of global grief we are experiencing — and our inability to physically gather together in its midst — is truly unprecedented.
 Public funds should go to people, the poor and unemployed, not to wealthy or inefficient corporations.
 As a psychologist, I'd like to encourage folks, now several weeks into quarantine, to be courageous — give yourself a mental health check-in and seek support if you need it
 There are many good reasons to move to mail balloting in this pandemic year. There don’t seem to be any good reasons not to. 
 Everyone agrees that growing trees is good for the climate; there is tremendous disagreement, however, on whether we should use some of our forest products to produce energy. If properly done, forest bioenergy can produce a win-win for forests and the climate.
 WWII home front workers were provided expanded opportunity and prosperity from their labors on the floors of defense factories. Frontline workers of today deserve the same.
 The Rules Committee resolution on proxy voting contained two sections authorizing committees to hold hearings and vote on any measure on a virtual basis during this hiatus. It should be embraced as a first step by the bipartisan task force to keep the House at work.
 His election chances rest on how he handles this crisis.
 To even hint at ending the Affordable Care Act during a cataclysmic crisis is political madness.
 Even if students return as scheduled, colleges and universities will have to address a daunting “new normal.”
 Beijing has given Washington the opportunity to unite the world against it, given its missteps during the coronavirus pandemic.
 Allocating resources where they are needed is better than bloated bureaucracies that prove their value neither in good times nor in bad.
 Trump’s social media fluency may impress older voters, but not a generation that honestly can’t remember a world without it.
 White House czars add a layer of bureaucracy onto policy matters that add to the confusion of overlapping lines of authority with existing departments and agencies.
 While the military is used to leading in times of conflict, this crisis poses a very different existential threat to the force and its culture.
 Dispelling disinformation gives each of us the power and freedom to make our communities stronger and better.
 COVID-19 represents the most extensive case of human containment, quarantine and isolation in world history.
 To understand weaknesses in WHO’s handling of COVID19, first, we need to understand the role the organization is supposed to play.
 The COVID-19 pandemic demands that we reassess our priorities.
 A common denominator of black swan events is that they come without warning, meaning average investors often don’t recognize their seriousness until the time to act has passed. 
 Simply put, strengthening the U.S.'s current patchwork of whistleblower protections would save lives. 
 We can all agree that prosecutors should not be able to cheat to obtain a conviction — especially in a case about cheating.
 The nation needs these critical workers in the labor force.
 If there ever was a time to re-evaluate our treatment of asylum seekers and how we establish and maintain public health standards in detention facilities, it is now. 
 As an analyst, separating fact from fiction and illuminating the truth is one of the most difficult challenges.
 Securities regulators could use existing authority to mandate transparency of data markets and prod firms to protect user data.
 The constitutional solution to this is incumbent on several factors.
 The Hill 1625 K Street, NW Suite 900 Washington DC 20006 | 202-628-8500 tel | 202-628-8503 fax
 https://thehill.com/opinion
