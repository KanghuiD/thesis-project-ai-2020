Coronavirus disease (COVID-19): Prevention and risks - Canada.ca
 Human coronaviruses cause infections of the nose, throat and lungs. They are most commonly spread from an infected person through:
 Current evidence suggests person-to-person spread is efficient when there is close contact.
 There is a difference between advice to quarantine (self-isolate) and advice to isolate. These measures are in place to protect the health and safety of Canadians.
 Additional restrictions apply to travellers returning to Canada.
 Quarantine for 14 days if you have no symptoms and any of the following apply:
 You must isolate if any of the following apply:
 Think you might have COVID-19?
 Take a self-assessment
 Canadians should continue to think ahead about the actions that they can take to stay healthy and prevent the spread of COVID-19 in Canada. This includes staying at home as much as possible and being prepared in case you or a family member becomes ill. Everyone should be practising physical (social) distancing. Even if you do not have symptoms of COVID-19, you could become infected by others.
 As we continue to see transmission of the virus within different communities, we know that everyone must take precautions, even those who have not travelled outside of Canada.
 In an effort to prevent the spread of COVID-19 within communities and across the country, all Canadians are advised to:
 You can go for a walk if you:
 If you go out for a walk, do not congregate and always practise physical (social) distancing by keeping at least 2 metres apart from others at all times.
 Together, we can slow the spread of COVID-19 by making a conscious effort to keep a physical distance between each other. Physical (social) distancing is proven to be one of the most effective ways to reduce the spread of illness during an outbreak.
 This means making changes in your everyday routines to minimize close contact with others, including:
 Proper hygiene can help reduce the risk of infection or spreading infection to others:
 Coronaviruses are one of the easiest types of viruses to kill with the appropriate disinfectant product when used according to the label directions. Health Canada has published a list of hard surface disinfectants that are likely to be effective for use against COVID-19.
 Although they do not claim to kill COVID-19, cleaners can play a role in limiting the transfer of microorganisms. Health Canada recommends cleaning high-touch surfaces often, using either regular household cleaners or diluted bleach. This bleach solution should be prepared according to the instructions on the label or in a ratio of 1 teaspoon (5 mL) per cup (250 mL). Directions are based on bleach that is 5% sodium hypochlorite, to give a 0.1% sodium hypochlorite solution.
 These surfaces include:
 Refer to the guidance on cleaning and disinfecting public spaces for more information.
 Medical masks, including surgical, medical procedure face masks and respirators (like N95 masks), must be kept for health care workers and others providing direct care to COVID-19 patients.
 Wearing a non-medical mask or  face covering while out in public is optional. If you do choose to wear one, refer  to our guidelines on wearing non-medical masks and how to make your own. 
 Masks alone will not prevent the spread of COVID-19. You must consistently and strictly adhere to good hygiene and public health measures, including frequent hand washing and physical (social) distancing.
 COVID-19 is a serious health threat, and the situation is evolving daily. The risk will vary between and within communities, but given the increasing number of cases in Canada, the risk to Canadians is considered high.
 This does not mean that all Canadians will get the disease. It means that there is already a significant impact on our health care system. If we do not flatten the epidemic curve now, the increase of COVID-19 cases could impact health care resources available to Canadians.
 The risk for COVID-19 may be increased for certain settings such as:
 Have you been on a recent flight, cruise, train, or at a public gathering? Check the listed exposure locations to see if you may have been exposed to COVID-19.
 There is an increased risk of more severe outcomes for Canadians:
 People that fall into these categories should reconsider attending gatherings. This includes large gatherings and even smaller events in crowded or enclosed settings.
 If you have symptoms (cough, fever or difficulty breathing), do not attend a mass gathering, event or places where people gather. You could put someone whose health is vulnerable at risk.
 The risk for getting COVID-19 may be increased for travellers. Canadians are advised to avoid all non-essential travel. If you must travel, check the latest travel advice before you leave.
 We will continue to adapt our risk assessment based on the latest data available.
 Because COVID-19 is a new disease, we are still learning how it affects pregnant women. At this time, there is no evidence to suggest that pregnant women are at a greater risk for more serious outcomes related to COVID-19 or that their developing child could be negatively affected by COVID-19.
 You can protect yourself from becoming ill by taking the following precautions:
 For more information, refer to our advice for mothers  on pregnancy, childbirth and caring for newborns.
 If you are pregnant and concerned about COVID-19, speak to your health care provider.
 Surfaces frequently touched with hands are most likely to be contaminated, including:
 Products shipped within or from outside of Canada could also be contaminated. However, because parcels generally take days or weeks to be delivered, and are shipped at room temperature, the risk of spread is low. There is no known risk of coronaviruses entering Canada on parcels or packages.
 To protect yourself from COVID-19, make sure to do the following when handling products shipped within or outside of Canada:
 There is currently no evidence to suggest that food is a likely source or route of transmission of the virus and there are currently no reported cases of COVID-19 transmission through food. People are unlikely to be infected with the virus through food.
 Scientists and food safety authorities across the world are closely monitoring the spread of COVID-19.
 If we become aware of a potential food safety risk, appropriate actions will be taken to ensure the safety of Canada's food supply.
 Coronaviruses are killed by common cleaning and disinfection methods and by cooking food to safe internal temperatures.
 Learn more about food safety.
 The current spread of COVID-19 is a result of human-to-human transmission. There is no evidence to suggest that pets or other animals play a role in transmitting the disease to humans. Scientists are still trying to understand if and how it affects animals.
 Pets can contribute to our overall happiness and well-being, especially in times of stress. If you are feeling well (no symptoms of COVID-19) and are not self-isolating because of COVID-19 illness, you can continue to take walks with your dog or spend time with your pet. This can contribute to keeping both you and your pet healthy.
 As a precautionary measure, if you have COVID-19 symptoms or are self-isolating due to contact with a COVID-19 case, you should follow similar recommendations around animals, as you would around people in these circumstances:
 These measures are recommended as a precaution, and are basic practices to prevent transmission of diseases between humans and animals. If you have concerns, seek professional advice from your veterinarian or a public health professional who can help to answer your questions.
 The Canadian Food Inspection Agency website has more information about animals and COVID-19.
 We have not approved any product to prevent, treat or cure COVID-19. Selling unauthorized health products or making false or misleading claims to prevent, treat or cure COVID-19 is illegal in Canada. We take this matter very seriously and we are taking action to stop this activity.
 Health Canada has published a list of hard surface disinfectants that are likely to be effective for use against COVID-19. This list is updated regularly. Although they do not claim to kill viruses such as COVID-19, cleaners play a role in helping limit the transfer of microorganisms.
 We encourages anyone who has information regarding potential non-compliant sale or advertising of any health product claiming to treat, prevent or cure COVID-19, to report it using our online complaint form.
 Have a question? Start here!
 Thank you! I have found a page that may help.
 https://www.canada.ca/en/public-health/services/diseases/2019-novel-coronavirus-infection/prevention-risks.html
