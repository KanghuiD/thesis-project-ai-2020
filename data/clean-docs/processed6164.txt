No sleeping: MTA changes Code of Conduct to address homeless - silive.com
 The MTA has changed its Code of Conduct to disallow sleeping on the city's transit system. (AP Photo/Mary Altaffer)AP
 STATEN ISLAND, N.Y. -- Dozing off on the bus or train? That’s against the rules.
 On Monday, MTA New York City Transit Interim President Sarah Feinberg announced that the agency has changed its Code of Conduct to disallow passengers from sleeping in train cars and buses in an effort to address the homeless population that has been plaguing the city’s transit system.
 “We are changing our Code of Conduct to make it abundantly clear that the transit system must be used by people for transport only -- not for sheltering, sleeping, storing belongings or panhandling. We will enforce these new regulations in close coordination with our NYPD partners and the MTAPD,” Feinberg wrote in an opinion piece published by the New York Post.
 The MTA Code of Conduct Section 1050.7 now reads:
 “No person on or in any facility or conveyance shall sleep or doze where such activity may be hazardous to such person or to others or may interfere with the operation of the Authority’s transit system or the comfort of its passengers.”
 The change comes following numerous reports detailing issues associated with homelessness in the New York City subway system -- issues that have been exacerbated and highlighted by the steep drop in ridership amid the ongoing coronavirus (COVID-19) pandemic.
 “New York City’s homeless have sought refuge in the subways for as long as there has been homelessness and subways. But the COVID-19 pandemic has intensified the problem for ­obvious reasons,” Feinberg wrote.
 “So, it isn’t surprising that homeless individuals are seeking refuge in some of our subway cars, stations and tunnels. But just because it’s unsurprising doesn’t mean it’s acceptable. And the Big Apple’s attempts to ignore the existence of this vulnerable, often emotionally disturbed, population has made its presence in the system a growing frustration for essential New Yorkers riding the subways, not to mention the Metropolitan Transportation Authority’s own hard-working employees,” she continued.
 Feinberg said that the agency has put “tremendous amounts of work” in to address homelessness in the subway system in recent years -- including partnerships with organizations through the Homelessness Task Force, increased cleanings and additional MTAPD officers -- but called on New York City to do its part.
 “The reality is, all of this was supposed to be an interim initiative to give the city’s government more time to come up with its own response and solutions. We are still waiting,” Feinberg wrote. “Our city must do better than this. We shouldn’t leave the most vulnerable to suffer quietly in a tunnel or on a train, and our workers shouldn’t be left to clean up the mess that is left behind. We need action -- now.”
 CITY ANNOUNCES TARGETED OUTREACH
 Meanwhile, just hours before Feinberg’s piece published, Mayor Bill de Blasio announced plans to expand homeless outreach and offer more beds to unsheltered people living in the subway system.
 “Our COVID-19 response must prioritize the most vulnerable New Yorkers,” said de Blasio. “We are doing everything we can to strengthen our subway and street outreach to ensure that every New Yorker who needs a place to sleep will get one.”
 The mayor is calling on the MTA to temporarily close the following 10 end-of-line stations during overnight hours, from midnight to 5 a.m., where outreach teams will engage homeless individuals leaving the stations, and cleaning teams will perform enhanced sterilization of the facilities.
 The city will also open 200 Safe Haven beds starting this week, providing prioritized shelter to the most vulnerable members of New York City’s homeless population.
 Note to readers: if you purchase something through one of our affiliate links we may earn a commission.
 Community Rules  apply to all content you upload or otherwise submit to this site.
 Ad Choices
 https://www.silive.com/coronavirus/2020/04/no-sleeping-mta-changes-code-of-conduct-to-address-homeless.html
