Opinion | news-gazette.com
 Partly to mostly cloudy. High 73F. Winds SSW at 15 to 25 mph..
 Cloudy with rain developing after midnight. Low 51F. Winds S at 10 to 15 mph. Chance of rain 70%. Rainfall near a quarter of an inch.
  Updated: April 22, 2020 @ 1:22 am
 It’s comforting to feel that government is listening, revealing when it won’t.
 His request for Congress to provide $10 billion to bail out the state's underfunded public pensions was dismissed as what it is — a request for citizens of the other 49 states to help make up for the damage inflicted by Illinois' powers-that-be over several decades.
 I would like to address the lies we are hearing in the news recently about the coronavirus. 
 In a recent letter to the editor, the writer referred to “Margaret Thatcher’s sociopathic dictum: ‘There is no such thing as society.’” 
 Congratulations, News-Gazette — you have now joined the ranks of the scare-mongering media, led by the every-present Associated Press. I’m referring to the article on page A-7 in the Sunday paper, which screamed that the U.S. had passed Italy in the number of fatalities. 
 Candidates had to file their quarterly fundraising reports with the Federal Election Commission last week, and they show that individuals and political action committees continue to give and candidates’ campaign committees continue to spend.
 Daniel Kovats, executive director of the Illinois Democratic County Chairs Association, said his advice to supporters of state Rep. Carol Ammons and Maryam Ar-Raheem is to work out their differences and then let his group and the Illinois State Board of Elections know the result.
 Read the governor’s numbers and weep.
 Most often, county residents hear about animal-control wardens when there is a report of animals being neglected or abused. In these situations, wardens are called to assess the animals and, if they are in danger, impound them pending court hearings. 
 The chaos of the coronavirus pandemic has undoubtedly affected each one of us in some way or another. For most, in the world and our country, it has had a salty effect on lives — one that you wish you could just spit out and be done with. 
 Last spring, the worst flooding in more than 25 years created havoc for farmers across much of Illinois, impacting more than 40 percent of the state’s population. Months before that, in a rare December outbreak, 27 tornadoes tore through parts of central and southern Illinois, destroying or …
 Our nation is suffering through an extraordinary shutdown because the president and governors are following expert medical advice. The experts assert that in order to stop the spread of the coronavirus, we must shelter in place and not engage in normal business activities. The question now i…
 I wonder if some N-G letter writers ever step back and read out loud what they have written. 
 Tennis courts in Champaign-Urbana should be reopened to the public for informal play contingent on social-distancing requirements being met. 
 During this unprecedented time of social isolation, your Champaign County forest preserves offer a respite from cabin fever. 
 You can’t buy masks, can buy guns. Can’t buy sani-wipes, you can guns. Can’t go to school, can go buy guns. 
 Since coronavirus has compelled all of us to stay at home, it may be a good time to read fiction to understand what’s real. 
 The Vermilion County state’s attorney is calling in a grand jury and putting people’s lives at risk. 
 I am protecting you, are you protecting me? 
 This letter was going to criticize Donald Trump and his administration for their delay in implementing COVID-19 containment measures. But, what’s done is done, or, in this case, wasn’t done. 
 So, people are dying all over the country during a pandemic, and according to an AP article in this newspaper (Friday, April 10) and other legit media, the Trump administration is “buying gear overseas and turning it over to private companies to sell to states ...”?! 
 Rest easy, Led Zeppelin fans. Because the band got a bad rap, it beat the rap. 
 At midday Wednesday, they had one chairwoman — Maryam Ar-Raheem. By evening, they had two — with state Rep. Carol Ammons claiming victory in an election for the position Ar-Raheem contends she still holds — and an intra-party fight about who’s in charge.
 Sold as a measure aimed strictly at the super-rich, it’s increasingly clear that Gov. J.B. Pritzker’s proposed progressive income tax plan will reach far beyond that limited group to the middle class.
 If you are at least 57 years old, you were an adult (18+) in 1981 when the AIDS crisis began in America. Before testing for HIV became available, I and many other gay and bisexual men lived in fear that we might be infected, which then meant a death sentence. 
 I am wondering if eating a heavy amount of garlic in my soup will remove fluid from lungs and body and assist those who are waiting on respirators? 
 This is about Alan Griffin leaving the Illini. He was an excellent basketball player and never got much playing time. The player that should be leaving is Giorgi Bezhanishvili. George Girl, as I call him, would be better suited for the cheerleading squad than the basketball team. His stats a…
 I want to play the coronavirus blame game, too. Here ya go. The U.S., Italy and Spain lead the world in reported coronavirus cases. What happened? 
 As we shelter in place to prevent the spread of COVID-19, I wonder how long we will need to protect ourselves and others. Even if the pandemic passes soon, will there be a resurgence in the fall? These are significant questions to ponder, as is the safety and efficiency of voting by mail. 
 {{summary}}
 https://www.news-gazette.com/opinion/