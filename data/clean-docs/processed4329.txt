Coronavirus: at a glance | World news | The Guardian
 A summary of the biggest developments in the global coronavirus outbreak
 Kevin Rawlinson
 Tue 14 Apr 2020 00.00 BST
 Key developments in the global coronavirus outbreak today include:
 At least 1,911,407 people worldwide have been infected, according to researchers at Johns Hopkins University in the US, who have been tracking official data and media reports, meaning the tally is likely to be an underestimate. The researchers say at least 118,854 people have died.
 Restrictions will not be lifted in France until 11 May, the country’s president told the nation in a televised address.
 Emmanuel Macron said creches and schools would begin to reopen in four weeks. He said he would work on a plan to help struggling sectors, such as tourism and leisure, and extend aid measures for companies and workers.
 The country is likely to have 12 times more cases than are being officially reported by the country’s government, with too little testing and long waits for confirmation of results, a study suggests.
 Use of facial protection is going to become common in the wake of the outbreak, the World Health Organization’s Covid-19 envoy, Dr David Nabarro, said. “Some form of facial protection, I’m sure, is going to become the norm, not least to give people reassurance. But I would say, don’t imagine that you can do what you like when you are wearing a mask.”
 The country missed three opportunities to be part of an EU scheme to bulk-buy masks, gowns and gloves and will not be taking part in key talks about future purchases as pressure grows on ministers to protect NHS medics and care workers on the coronavirus frontline.
 The number of fatalities from coronavirus in Italy rose by 566 on Monday, 135 more than on Sunday, bringing the total death toll to over 20,000. Almost half of the latest deaths were registered in Lombardy, the northern region worst affected by the virus.
 A total of 11,329 patients have died in UK hospitals after testing positive, the UK’s Department of Health has said, up by 717 from 10,612 in 24 hours.
 https://www.theguardian.com/world/2020/apr/14/coronavirus-latest-at-a-glance
