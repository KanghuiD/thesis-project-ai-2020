Coronavirus: ‘My business could go bust if insurers don’t pay’ - BBC News
 Businessman Simon Ager says his company, the Pinnacle Climbing Centre in Northampton, is at risk of being bankrupted because his insurer is refusing to cover losses as a result of the coronavirus lockdown.
 He is one of a number of business owners who have said they might take legal action against insurers Hiscox, which has said it will not pay business interruption claims resulting from the outbreak.
 Mr Ager's policy covers the climbing centre for losses of up to £100,000 if it is forced to close in certain circumstances, although he says the lockdown is likely to cost him more than that.
 Hiscox's policy documents says it will cover financial losses for businesses which are unable to use their premises following "an occurrence of any human infectious or human contagious disease, an outbreak of which must be notified to the local authority".
 Marie-Claire di Mambro, a solicitor at Keystone Law, believes the clause should "cover any business that purchased this policy for any financial losses incurred as a consequence of Covid-19".
 A separate clause says the insurer will cover losses if an incident within a mile of an insured business means the government refuses access to that firm's premises. That could be a shop, factory or climbing wall, for example.
 Ms Di Mambro says this second clause should cover a business that cannot use its buildings because the government has forced companies to close as a result of the pandemic.
 But Hiscox says the insurance industry does not have enough money to cover all the losses that will emerge as a result of the lockdown.
 "Business interruption policies across the industry were never intended to cover pandemic risks," a spokeswoman said, noting that the insurer's lawyers do not think the pandemic is covered by its business interruption policies.
 Instead, the insurer argues that the policy was intended to cover incidents that occur only within a mile of a business - not across the whole country - or outbreaks such as Legionnaires' disease on the premises.
 But Roger Topping - a claims assessor at TopMark Adjusters - who is also considering bringing legal action against the Hiscox, argues that is not what the policy says.
 "The policy actually covers notifiable disease that causes an interruption to the business," he believes.
 Insurance coverage is different from each provider. 
 Allianz and Aviva say they do not cover any diseases that are not named in their policies, and Zurich said its policies "generally" did not provide cover for global events like the Covid-19 pandemic. Meanwhile, Axa and RSA say some of their policies may cover businesses which suffer a loss because of a coronavirus outbreak on their premises.
 Hiscox, which is one of the UK's best-known insurance companies and is worth about £2.7bn, would not say how many policies it has issued including the wording in question.
 Emily Pringle owns a candle and fragrance company, Notes of Northumberland, which has been forced to furlough its three employees. She says she is having to use the government's financial help schemes, because Hiscox refused her claim.
 "They shouldn't have been offering this sort of cover, unless they were prepared to cover it in the instance that something like this happened," she says.
 Ms Pringle called Hiscox just days before she was forced to shut her business. She says the insurer assured her that it would pay her losses as a result of the decision to close, and even sold her extra cover to protect her business from the pandemic's impact.
 But when she approached the insurer to make a claim, she was told that she was not covered. Hiscox says: "We are investigating Emily's circumstances."
 While Ms Pringle says Hiscox may not have intended to protect against losses from an event like this when it wrote the policy, she thinks her contract's wording means the insurer is obliged to pay.
 She says that any accidental coverage under the policy is not the fault of small businesses. "It just doesn't seem fair that they're saving themselves and sacrificing all of us. It's their mistake, not ours," she says.
 Daniel Duckett opened Lazy Claire Patisserie in Belfast two years ago, and he now employs six part-time staff.
 The patisserie has been forced to close, but Mr Duckett wants to continue paying staff and also rent to his landlord, another small business owner.
 "In the spirit of supporting other small businesses around us, we want to try to pay the people who need the money the most," he says.
 But he says reopening the business after the lockdown will be more difficult because Hiscox has refused his claim.
 Mr Duckett says the company's argument that contagious disease cover only applies to local outbreaks is different to the wording of his policy. He thinks Hiscox is arguing what its policy "should say".
 "What it should say and what they think it says is a matter of opinion, it's not a matter of fact," he tells the BBC.
 His letter to Mr Thaker and the chancellor says: "It is my belief that Hiscox have wrongly denied my claim and that of other businesses forced to close due to the government's direction."
 He quotes Hiscox, which tells prospective customers "we will always go the extra mile and always start by assuming your insurance claim is valid", and asks why the wording of his policy does not apply. 
 The Commission president says Italy was let down by "too many" at the start of the outbreak.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/business-52280012
