Opinion | Latest News & Analysis | The Australian Financial Review | AFR
 Record leverage among profitable investment-grade corporates and the rise of zombies is a Molotov cocktail bound to ignite in any serious shock such as COVID-19.
 Columnist
 The virus could spark a surge in class action law suits as legal firms and litigation funders rake in easy profits.
 Editorial
 Other than being successful political theatre, it's not clear what the supposed unity of the national cabinet has achieved.
 Ever-rising household and government debt will not stabilise the world economy forever. We will have to adopt more radical alternatives.
 For 30 years politicians fought to divide the spoils of prosperity. Now the whole game of governing has to be invented again.
 Political editor
 After the shock of COVID-19, getting 1 million Australians back to work requires the political system to finally update Australia's inflexible and complex workplace system.
 Tech now available allows employers to know when people log on and off and take their breaks, and questions about how this will work in practice are certain to outlast the shutdown.
 The 60 companies that presented at the Macquarie Australia conference were determined to show their resilience. But huge economic dislocation is coming. 
 The machinations over Eden-Monaro not only shows the deputy PM doesn’t control his party room, but also why the National Party will be a big problem for the PM’s reform agenda.
 The Bank of England describes its outlook as "a plausible illustrative economic scenario". Others might reasonably think it cloud-cuckoo-land.
 Shemara Wikramanayake celebrated her first year in the job with the best profit performance by a major financial institution in the profit reporting season, and a 5.5 per cent lift in her remuneration package to $18 million.
 The three biggest powers are too internally fractured to steer the post-virus world away from conflict.
 The electronic document exchange platform is restructuring its capital as a precursor to a potential initial public offering at a price over $2 billion.
 To earn the trust of all voters, UK Prime Minister Boris Johnson must stop the verbal obfuscation and statistical trickery.
 If central banks shift to nominal-GDP targeting, investors’ relative bullishness could turn out to be justified.
 China deserves serious and sustained scrutiny for its missteps and lack of transparency. But the latest political pile-on is unproductive.
 Cars and celluloid are a perfect match. So strap yourself in and make the most of your free time with these motoring movie suggestions.
 When two academics set out to measure liquidity in equity markets they found a formula which can optimise share trading by institutions and retail investors.
 The appointment of two technology experts emphasises where the global mining industry is heading, and helps complete chairman Ken MacKenzie's board puzzle.
 Billionaire Sam Zell earned the nickname the Grave Dancer for buying distressed property. But even he's worried about the permanent scars left by COVID-19, which is worth considering in the context of updates from Australian property groups Goodman Group and Vicinity Centres.
 Is your law firm getting the most of out of your existing technology, or looking at shiny new toys?
 The alternative to financial and operational reforms is to have fewer facilities and for families to take responsibility for looking after their older friends and relatives.
 Here's why Fat Prophets expects the fall in demand to weaken competitors far more than Uber and allow the company to gain more market share.
 Warren Buffett went against his own advice and bought airline stocks, and now he's more fearful than greedy. The Oracle may have reached the peak of his powers.
 Many investors believe the bear market starting in late February. Jim Bianco isn't one of them.
 Chairman Lim Kim Hai says the new owners of Virgin must deal with dozens of problems, including three chronic issues that should be fixed by the administrator.
 Few property leaders expect a fast recovery, even as Australia starts to normalise. Many say the pandemic will be “far, far worse" than the global financial crisis, warns Robert Harley.
 Explore all of David Rowe's editorial cartoons from April 2020 onwards.
 The Daily Habit of Successful People
 https://www.afr.com/opinion
