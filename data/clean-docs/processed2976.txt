Home | Anthem Coronavirus Information
 The Sydney Care mobile app features a tool to help you quickly understand your potential risk for COVID-19. You can also use the app to set up a telehealth visit through text or video.
 Get the Sydney Care App
 Find out what your symptoms may mean by answering five quick questions. This tool uses guidelines issued by the Centers for Disease Control and Prevention (CDC).
 Begin the Assessment
 Use LiveHealth Online to visit with a doctor 24/7 through live video. You can use the LiveHealth Online app on your mobile device or use your computer’s camera to have your telehealth visit.
 Get the LiveHealth Online App
 We've partnered with Aunt Bertha, a social care network. Find free and reduced-cost programs for help with food, transportation, health, housing, job training and more.
 We have partnered with Castlight Health to help you find a COVID-19 testing facility. Enter your state and county to quickly locate testing facilities near you.
 Find a COVID-19 Test Facility
 Visit Psych Hub
 Centers for Disease Control and Prevention (CDC) Coronavirus Webpage
 U.S. Department of State China Travel Advisory
 World Health Organization, Coronavirus
 Y0114_20_121463_I_C
 https://www.anthem.com/blog/member-news/how-to-protect/