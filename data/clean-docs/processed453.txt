Coronavirus updates: UK's Boris Johnson discharged from hospital
 Keep Me Logged In
 The coverage on this live blog has ended — but for up-to-the-minute coverage on the coronavirus outbreak, visit the live blog from CNBC's U.S. team.
 The data above was compiled by Johns Hopkins University as of 8:00 p.m. Beijing time. 
 U.K. Prime Minister Boris Johnson has been discharged from the hospital after a week that saw three days spent in intensive care. He will not immediately be returning to work, but rather will continue his recovery at Chequers, the prime minister's country house, a statement from his office said.
 Johnson tested positive for Covid-19 on March 27. 
 Confirmed coronavirus cases in the Netherlands rose by 1,188 overnight to a new total of 25,587 on Sunday, national health authorities said, also reporting 94 new deaths from the virus.
 Iranian fatalities from the Covid-19 rose by 117 from Saturday to Sunday, bringing the total death count in the Middle East's worst-hit country to 4,474. Confirmed cases reported by Iran's health ministry are at 71,686. 
 Malaysia reported 153 new coronavirus cases on Sunday, bringing the Southeast Asian nation's total confirmed cases to 4,683. The country reported three new deaths, bringing the number of fatalities from the virus to 76, the Malaysian health ministry said.
 Russia reported its biggest daily jump in new coronaviruses cases so far on Sunday at 2,186, bringing the country's total confirmed cases to 15,770. The death count rose by 24 overnight, for a total of 130. 
 Some 112 passengers who were onboard the coronavirus-affected Antarctic cruise ship, Greg Mortimer, arrived in Melbourne from Uruguay after the vessel docked in Montevideo.
 Australia's foreign minister, Marise Payne, tweeted the arrival of the passengers Sunday morning. 
 The ship had been stranded in La Plata River near the Uruguayan capital since March 27, Reuters reported.
 Most of the passengers who were repatriated to Melbourne via a charter flight had tested positive, according to the news wire. It added that two Australian passengers could not be transported as they had to stay in intensive care at a hospital because of their poor condition.
 Of the 112 passengers, 96 were Australians and 16 were New Zealanders, Reuters said. — Saheli Roy Choudhury
 India's health ministry said as of 8 a.m. local time on April 12, there were at least 8,356 total cases in the country, including 273 deaths. Some 715 people are said to have recovered. The worst-affected states and union territories in India are Maharashtra, Delhi, and Tamil Nadu. 
 Sunday's number marked an increase of 909 cases from the 7,447 total confirmed cases reported as of yesterday evening by the Press Information Bureau. 
 India looks set to extend its 21-day lockdown at least till the end of the month. Delhi Chief Minister Arvind Kejriwal tweeted about the decision on Saturday. While Prime Minister Narendra Modi has yet to formally announce the decision, a summary of his latest meeting with all the state chief ministers indicated there was consensus for an extension.
 "The Chief Ministers suggested that Lockdown should be extended by two weeks," the summary said. — Saheli Roy Choudhury
 The technology arm of the U.K.'s National Health Service has been working with Google and Apple to develop an app that can tell users if they've been in close contact with someone infected with the coronavirus,  the Sunday Times reported.
 Ministers hope that the app could make it possible for the U.K. to start lifting its most stringent social distancing measures from late May, according to the Times.
 Using Bluetooth technology, the system can alert a person who downloads the app if they have been in close proximity with someone who tested positive for Covid-19, the Times said. That could alert them to get tested. 
 Singapore is using a similar concept for its contact tracing app.
 Based on JHU data, the U.K. has at least 79,883 reported cases of infection and 9,892 people have died so far. — Saheli Roy Choudhury
 Saudi Arabia's King Salman ordered the extension of curfew until further notice because of how the coronavirus is spreading, the Saudi Press Agency reported. 
 The Saudi king previously ordered a curfew that took effect on March 23, from 7 p.m. to 6 a.m. local time for 21 days, according to Reuters, which cited state media. Last week, Saudi Arabia placed capital city Riyadh and other big cities under a 24-hour curfew and locked down much of the population, Reuters said. 
 Saudi Arabia has at least 4,033 reported cases of infections and 52 deaths, according to JHU data. — Saheli Roy Choudhury
 China's National Health Commission said there were 99 new confirmed cases of coronavirus on April 11 and 97 of those were "imported" as people returned from abroad. That number jumped from the 46 reported cases from a day earlier. 
 No new deaths were reported by the NHC on Sunday while another 63 were said to be asymptomatic cases where the patient tested positive for the virus but did not show the usual symptoms associated with it. China started adding asymptomatic cases to its daily tally in April. 
 There are 82,052 cases of infections reported to-date in mainland China and 3,339 people have died. Recently, China ended its 11-week lockdown of Wuhan, the city in Hubei province where the virus outbreak was first reported. — Saheli Roy Choudhury
 Less than 24 hours after the Treasury Department formally extended cash grant offers to the six largest airlines in the U.S., the union representing 120,000 flight attendants is blasting the move with a dire warning. 
 "This will lead to airline bankruptcies," said Sara Nelson, president of the Association of Flight Attendants union. "The Treasury Department is destabilizing the industry, not helping save it."
 Nelson's anger is fueled by the Treasury Department's decision to make 30% of each cash grant offer a low interest loan payable to the federal government. That move, which caught many airline executives by surprise, means the $25 billion approved by Congress for immediate cash grants will actually be $17.5 billion. The other $7.5 billion will now be loans airlines will be required to re-pay. — Phil LeBeau
 Singapore has seen a spike in the number of newly reported cases in recent days as the virus spread, creating several growing clusters that led to the quarantine of almost 20,000 resident workers last week. 
 As of noon local time on April 11, there were 191 new cases of infections, of which 52 were linked to known clusters and 20 were related to other patients. There have been 2,299 total cases and among them, 528 people have made full recoveries and have been released from hospitals and community isolation centers. Another person died from Covid-19, bringing the total death toll in the city-state to eight. 
 Singapore stepped up measures recently to slow down the spread by temporarily closing schools as well as most workplaces. Stricter enforcement of safe distancing rules has also been put in place. — Saheli Roy Choudhury
 The United Kingdom announced a 200 million pound ($248.94 million) aid package to help local charities and international organizations in their efforts to slow the spread of coronavirus in vulnerable countries and, therefore, prevent a second wave of infections coming to the U.K. 
 UN agencies are set to receive £130 million; of that, the World Health Organization will receive £65 million as it coordinates international efforts to tackle the pandemic while the rest will be distributed among UNICEF, UNHCR, the World Food Programme and UN Population Fund. The Red Cross will receive another £50 million for its efforts in difficult to reach areas such as those suffering from armed conflicts while £20 million will go to international NGOs, including U.K. charities.
 The cash will help countries with poor health care systems to quickly identify and care for patients with symptoms to minimize human-to-human transmissions; that includes war-torn Yemen as well as Bangladesh, which is housing about 850,000 Rohingya refugees, the British government said. — Saheli Roy Choudhury
 All times below are in Eastern time.
 Americans are starting to receive their cash payments via direct deposits, part of the $2 trillion bill passed by Congress to stimulate the economy after the decline caused by the pandemic.
 ″#IRS deposited the first Economic Impact Payments into taxpayers' bank accounts today. We know many people are anxious to get their payments; we'll continue issuing them as fast as we can," the IRS tweeted on Saturday.
 The entire country is now under a major disaster declaration for the coronavirus pandemic after the U.S. death toll reached the highest in the world on Saturday.
 President Donald Trump approved a disaster declaration for Wyoming on Saturday, which comes 22 days after the first disaster declaration in New York, the epicenter of the virus.
 All 50 states as well as the U.S. Virgin islands, the Northern Mariana Islands, Washington, D.C., Guam and Puerto Rico have received a federal disaster declaration. American Samoa is the one U.S. territory that isn't under a major disaster declaration.
 The designation by the Federal Emergency Management Agency allows states and territories to access federal funds to help fight the coronavirus spread. State officials and doctors have been competing for essential supplies like ventilators and personal protective equipment as hospitals grapple with shortages. — Emma Newburger
 The U.S. death toll reached 20,071, according to a tally from Johns Hopkins University. The U.S. surpassed Italy on Saturday as the country with the most coronavirus deaths in the world, according to JHU data.
 At least 20,071 people have died from the virus in the U.S. Italy has suffered 19,468 fatalities; Spain has recorded at least 16,353 deaths; and France has confirmed 13,851 deaths. — Meg Graham, Riya Bhattacharjee
 Read CNBC's coverage from the U.S. overnight: US death toll tops 20,000, Governors call for $500 billion more in aid
 Got a confidential news tip? We want to hear from you.
 Data is a real-time snapshot *Data is delayed at least 15 minutes. Global Business and Financial News, Stock Quotes, and Market Data and Analysis.
 Data also provided by 
 https://www.cnbc.com/2020/04/12/coronavirus-latest-updates.html