	Coronavirus - COVID-19
 COVID-19 : 
 🇬🇧 Information & advice  | 🇫🇷 Informations et conseils | 🇳🇱 Informatie & advies
 Travel from the UK to France:
 NO HOLIDAYS OR SOCIAL VISITS
 Travel from France to the UK:
 Terminal Buildings and Shuttles:
 In line with regulations our terminal buildings have now had to close. There are toilet facilities available in the allocation lanes - please use them as the toilets on the shuttles are now closed. Customers are required not to leave their cars whilst onboard the shuttles to comply with "social distancing" advice from both Governments.
 Important: Please note the latest advice from the FCO on travel for UK Citizens can be found here: https://www.gov.uk/guidance/travel-advice-novel-coronavirus
 We continue to take guidance from the Department for Transport, and Public Health England. Further information can be found on: gov.uk/guidance/wuhan-novel-coronavirus-information-for-the-public
 We are also staying up to date with French government advice, which can be found here: ​gouvernement.​fr/info-coronavirus
 If you are based in another country, please look at the advice your government is currently providing.
 Our usual, flexible conditions apply. Most ticket types have a full year from date of original purchase to be used, as long as amended before the day of your booked travel and there are no admin fees for changing dates. Our Flexiplus tickets are fully refundable.Travel to be completed within 1 calendar year of the date of original purchase.
 Standard Tickets
 You can amend your booking for free up to 24 hours before your departure time, if you’re travelling in the same direction and we have space available (you will have to pay the difference if the new ticket is more expensive, but there will be no amendment fees). Most ticket types have a full year from date of original purchase to be used. You can amend your booking here: My Eurotunnel
 Flexiplus Tickets
 As well as being amendable our Flexiplus tickets are fully refundable too. Travel to be completed within 1 calendar year of the date of original purchase.
 Bookings made with Tesco or other discount vouchers
 Eurotunnel has implemented additional cleaning procedures. Automatic hand sanitisers have been installed in passenger areas in both the UK and France, toilet facilities are being cleaned and checked for supplies more frequently, and all cleaning regimes have been increased on the terminals, and onboard the shuttles.
 We recommend that customers continue to carefully follow the official advice given. Useful advice can be found on the World Health Organisation’s website.
 Please follow the official travel and health advice issued by your country’s travel and health authorities.
                                 At Eurotunnel Le Shuttle we have exciting offers, news and tailored travel inspiration that we hope you would like to hear about.
                             
                                 Thank you for submitting your details and signing up to our newsletter and latest offers.
                                 Oh no! There was an error submitting your details...
                                 Unfortunately we have not been able to receive your details.  Please try again - we'd not want you to miss out on the latest offers!
 https://www.eurotunnel.com/uk/travelling-with-us/latest/covid-19/
