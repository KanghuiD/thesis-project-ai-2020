Trump holds Covid-19 briefing as governors ask for more help - as it happened | US news | The Guardian
 This blog is now closed.
 Bryan Armen Graham in New York (now) with 
 Enjoli Liston and 
 Tom Lutz (earlier)
 Mon 20 Apr 2020 01.19 BST
 First published on Sun 19 Apr 2020 14.43 BST
 1.12am BST
 01:12
 Here’s a recap of Sunday’s major news lines:
 Updated
 at 1.19am BST
 12.55am BST
 00:55
 Trump is asked whether he is inciting violence by calling for people to liberate states.
 “They’ve got cabin fever,” he says. “They want to get back. They want their life back. Their life was taken away from them. And you know, they learned a lot during this period. They learned to do things differently than they have in the past and they’ll do it hopefully until the virus has passed. And when the virus passes, I hope we’re going to be sitting next to each other at baseball games, football games, basketball games, ice hockey games. I hope we’re going to be sitting next to each other. The Masters is going to have 100,000 people, not 25 people waching at the course.”
 He adds: “I’ve never seen so many American flags at a rally as I’ve seen at these rallies. These people love our country. They want to get back to work.
 12.52am BST
 00:52
 Trump is asked why he is reading and showing clips full of praise for himself at a time when more than 22 million Americans are unemployed and more the 40,000 have died as a result of the coronavirus.
 “What I’m doing is I’m standing up for the men and women that have done such an incredible job,” he says. “Nothing is about me.”
 “You’re never going to treat me fairly, many of you. And I understand that. I got here with the worst, most unfair press treatment they say in the history of the United States for a president. They did say Abraham Lincoln had very bad treatment, too.”
 That one is actually true.
 at 1.03am BST
 12.39am BST
 00:39
 Remember about a half hour ago when Trump played that excerpt from the New York governor Andrew Cuomo’s press briefing only to complain the clip wasn’t long enough and the good parts were left out? We’re in luck. Someone on his team has gone and found the complete footage. 
 The lights have gone down again and we’re watching Cuomo’s remark that no one in New York was denied a ventilator if they needed
 at 1.04am BST
 12.29am BST
 00:29
 Trump is asked what advice he would give to the many protesters who are against the government’s restrictions.
 “You’re allowed to protest,” he says, adding that “some governors have gone too far”.
 He says he watched footage of the protests and was happy they were all practicing social distancing, which is very much not true.
 12.11am BST
 00:11
 Trump is in full stream-of-consciousness mode, very much in the spirit of his campaign rallies. 
 He says the coronavirus has taught the nation an important lesson about the “supply chain” and the need to not have to rely on other countries in times of crisis. He then spends a few minutes criticizing trade policies with hard words for Nafta and the World Trade Organization.
 “I want to read something I saw on television,” he says, before reading his own tweet from 19 Feburary off a large printed page.
 at 12.25am BST
 11.57pm BST
 23:57
 And now the lights go down and Trump plays an excerpt from the New York governor Andrew Cuomo’s press briefing earlier today.
 After some initial technical difficulties, Cuomo’s words ring through the briefing room: “These were just extraordinary efforts and acts of mobilization and the federal government stepped up and was a great partner and I’m the first person to say it. We needed help and they were there.”
 After Trump returns to the dais, he complains the clip wasn’t long enough and the good parts were left out: “Do you want to put the rest of it up or do you not have it? I just think it’s good because it’s bipartisan.”
 11.48pm BST
 23:48
 After spending a few minutes reading from a Wall Street Journal op-ed praising his response to the pandemic, Trump points to the declining trajectory in new cases in the Seattle, Detroit, New Orleans, Indianapolis and Houston metro areas as “more evidence that our aggressive strategy is working”.
 “Who ever heard of a thing like this?” he asks, circling back to familiar talking points. “We would’ve had millions of people die if we didn’t do this. Millions of people.”
 at 12.11am BST
 11.39pm BST
 23:39
 Donald Trump opens today’s coronavirus task force press briefing by saying they are “close to a deal” with Democrats on the latest relief bill for small businesses and workers, which could be resolved as soon as tomorrow.
 He then says 4.18 million Americans have been tested for the coronavirus. “That’s a record anywhere in the world,” Trump says. “More total tests than all the following nations combined: France, the United Kingdom, South Korea, Japan, Singapore, India, Austria, Australia, Sweden and Canada.”
 Trump says there is a “tremendous capacity” for testing before showing off a medical swab, likening it to a Q-tip. “We have ordered a lot of them,” he said, but notes that some states “don’t know where they are”.
 Then a return to what’s become a recurring motif over the past few days: “We are now the king of ventilators,” he says. “We have so many ventilators.”
 at 11.44pm BST
 10.55pm BST
 22:55
 New York City mayor Bill de Blasio ramped up the rhetroic in his campaign for increased federal funding for US cities during the coronavirus crisis during his press briefing on Sunday, asking Donald Trump whether his administration was “going to save New York City or are you telling New York City to drop dead?”
 As the Guardian’s Edward Helmore reports:
 De Blasio has criticized the $2tn coronavirus relief package that Trump signed last month, saying New York only received $1.4bn from the stimulus, compared with around $58bn for the airline industry. He has called for the next package, which congressional and administration leaders say they are “close” to reaching a deal on, to include tens of billions for states, cities and municipalities.
 The mayor had harsh words for the president, claiming both he and the vice-president, Mike Pence, have ignored his overtures on the stimulus funds.
 On Sunday, De Blasio sent a message to the administration that echoed a New York newspaper headline during the city’s bankruptcy crisis of the 1970s.
 “There was that famous Daily News cover that said ‘Ford to City: Drop Dead,’” De Blasio said. “So my question is, Mr Trump, Mr President, are you going to save New York City or are you telling New York City to drop dead? Which one is it?”
 “President Trump, what’s going on? Cat got your tongue?” De Blasio continued. “You’re usually really talkative. You usually have an opinion on everything. How on earth do you not have an opinion on aid to American cities and states?”
 10.25pm BST
 22:25
 A sensational investigation co-published today by ProPublica and the Seattle Times shows how officials in the first US state to be hit by Covid-19 struggled to send the public a clear, consistent message in the early days of the pandemic, bowing to a professional soccer team’s desire to host a game with 33,000 spectators despite urgent warnings from the health department for a ban on large gatherings. 
 The county’s numbers — 10 known deaths and nearly 60 confirmed cases as of late morning — were bad and getting worse. Many local events had already been called off for fear of spreading the coronavirus. Oyster Fest. The Puget Sound Puppetry Festival. A Women’s Day speaker series at the Gates Foundation. King County had ordered a stop to in-person government meetings unless they were considered essential.
 The health officer, Dr. Jeffrey Duchin, was to the Seattle area what Anthony Fauci would become for the country, the doctor at the microphone, dispensing guidance. Under Washington law, Duchin also had authority to make his wish an order.
 The most recent count of Covid-19 cases in Washington totals 11,802 infections and 624 deaths, according to the state’s Department of Health.
 at 10.27pm BST
 9.44pm BST
 21:44
 In a letter published in the New England Journal of Medicine as part of its ongoing Covid-19 Notes series, the chief executive of a Massachusetts hospital accuses the federal government of running an effective PPE blockade that’s prevented the delivery of critical equipment to states in need. Emphasis ours:
 Our supply-chain group has worked around the clock to secure gowns, gloves, face masks, goggles, face shields, and N95 respirators. These employees have adapted to a new normal, exploring every lead, no matter how unusual. Deals, some bizarre and convoluted, and many involving large sums of money, have dissolved at the last minute when we were outbid or outmuscled, sometimes by the federal government. Then we got lucky, but getting the supplies was not easy.
 A lead came from an acquaintance of a friend of a team member. After several hours of vetting, we grew confident of the broker’s professional pedigree and the potential to secure a large shipment of three-ply face masks and N95 respirators. The latter were KN95 respirators, N95s that were made in China. We received samples to confirm that they could be successfully fit-tested. Despite having cleared this hurdle, we remained concerned that the samples might not be representative of the bulk of the products that we would be buying. Having acquired the requisite funds – more than five times the amount we would normally pay for a similar shipment, but still less than what was being requested by other brokers – we set the plan in motion. Three members of the supply-chain team and a fit tester were flown to a small airport near an industrial warehouse in the mid-Atlantic region. I arrived by car to make the final call on whether to execute the deal. Two semi-trailer trucks, cleverly marked as food-service vehicles, met us at the warehouse. When fully loaded, the trucks would take two distinct routes back to Massachusetts to minimize the chances that their contents would be detained or redirected.
 Hours before our planned departure, we were told to expect only a quarter of our original order. We went anyway, since we desperately needed any supplies we could get. Upon arrival, we were jubilant to see pallets of KN95 respirators and face masks being unloaded. We opened several boxes, examined their contents, and hoped that this random sample would be representative of the entire shipment. Before we could send the funds by wire transfer, two Federal Bureau of Investigation agents arrived, showed their badges, and started questioning me. No, this shipment was not headed for resale or the black market. The agents checked my credentials, and I tried to convince them that the shipment of PPE was bound for hospitals. After receiving my assurances and hearing about our health system’s urgent needs, the agents let the boxes of equipment be released and loaded into the trucks. But I was soon shocked to learn that the Department of Homeland Security was still considering redirecting our PPE. Only some quick calls leading to intervention by our congressional representative prevented its seizure. I remained nervous and worried on the long drive back, feelings that did not abate until midnight, when I received the call that the PPE shipment was secured at our warehouse.
 9.11pm BST
 21:11
 A number of US governors from both sides of the aisle, including Virginia Democrat Ralph Northam and Maryland Republican Larry Hogan, have accused Donald Trump of making “delusional” and “dangerous” statements amid mounting pressure to roll back stay-at-home measures.
 As the Guardian’s Richard Luscombe and Edward Helmore report:
 Many state leaders have said they cannot embark on Trump’s recommended three-phrase programme to ease stay-at-home restrictions without a robust and widespread system of testing in place.
 Researchers at Harvard University have suggested the US should conduct more than three times the number of coronavirus tests it is currently administering over the course of the next month, the New York Times reported.
 Democratic Governor Ralph Northam of Virginia told CNN on Sunday that claims by Trump and Vice-President Mike Pence that states have plenty of tests were “just delusional”.
 “We have been fighting for testing,” he said on CNN’s State of the Union. “We don’t even have enough swabs, believe it or not. For the national level to say that we have what we need, and really to have no guidance to the state levels, is just irresponsible, because we’re not there yet.”
 Maryland, Virginia and Washington DC are still seeing increasing cases even as the epicenter of the US outbreak, New York, has started to see some declines. Boston and Chicago are also emerging hot spots with recent surges in cases and deaths.
 “The administration I think is trying to ramp up testing, they are doing some things with respect to private labs,” said Republican governor Larry Hogan of Maryland during a CNN interview. “But to try to push this off, to say the governors have plenty of testing and they should just get to work on testing, somehow we aren’t doing our jobs, is just absolutely false.”
 8.22pm BST
 20:22
 Tyson Foods, one of the world’s largest meat processors, has faced criticism after cases were linked to one of its plants in Waterloo, Iowa. 
 “My personal opinion is that it should be closed,” the Black Hawk county sheriff, Tony Thompson, told the Des Moines Register on Friday. “I think we need a hard boot, reset on that plant. I think we need to be able to sort out and cull the herd between the haves and the have-nots there. I think we need to deep-clean that facility and I think we need to restart that plant on a clean slate.”
 That news was followed on Sunday by a CNN report that another plant in Tennessee had recorded 90 positive tests for Covid-19. The company says workers are required to wear face coverings during shifts and have their temperatures taken at the start of each shift. 
 A spokesperson for Tyson also said cleaning is a priority. “We have team members dedicated to constantly wiping down and sanitizing common areas. In some cases, this additional cleaning involves suspending a day of production,” the spokesperson said on Sunday.
 at 9.10pm BST
 7.58pm BST
 19:58
 Donald Trump has started his afternoon by declaring the US “the king of ventilators”. Several state governors have said they need more help from the federal government with Covid-19 testing in an effort to curb the pandemic. However, the president thinks he’s on the right course, while implying the onus was on governors rather than the federal government to ramp up testing. 
 Just like I was right on Ventilators (our Country is now the “King of Ventilators”, other countries are calling asking for help-we will!), I am right on testing. Governors must be able to step up and get the job done. We will be with you ALL THE WAY!
 “Just like I was right on Ventilators (our Country is now the “King of Ventilators”, other countries are calling asking for help-we will!), I am right on testing,” the president tweeted. “Governors must be able to step up and get the job done. We will be with you ALL THE WAY!”
 On NBC’s Meet the Press on Sunday, Ohio’s Republican governor, Mike DeWine, said the FDA had not paid enough attention to companies “putting a slightly different formula together” for tests. “I could probably double, maybe even triple testing in Ohio virtually overnight” if the FDA considered such options, DeWine said. 
 7.40pm BST
 19:40
 The Miami Herald has reported on a problem facing one public hospital: agencies trying to headhunt staff during the Covid-19 pandemic. From the Herald’s report:
 Staffing agencies have mounted attempts to poach the public hospital’s most crucial workers, even as the novel coronavirus upends operations and mashes its bottom line, with estimated losses of $25 million per month going forward under the status quo.
 Plantada, who helps manage ventilators for critically ill COVID-19 patients, said the offers — as high as $7,000 per week plus room and board — are both aggressive and enticing. But she has no plans for leaving.
 “We want to support our community,” Plantada said. “Most of the therapists down here, that is our priority: sticking by and being here for everybody else.”
 You can read the full report here.
 https://www.theguardian.com/us-news/live/2020/apr/19/coronavirus-us-live-trump-latest-news-updates-cuomo-pelosi-lawmakers-funding-deal
