4 things PennLive readers really find irritating about the coronavirus pandemic | Opinion - pennlive.com
 It’s stressful enough remembering to keep your hands out of your face and washing them every time they touch anything that might have the deadly coronavirus. Add to that having to disinfect the house every two hours and make home-made masks now essential for the weekly jaunt to Giant. It’s enough to make anyone a bit irritable.
 Here they are:
 Price Gouging
 Attorney General Josh Shapiro says his office has received more than 3,000 complaints about businesses raising prices on everything from hand sanitizer to Lysol disinfectant. Emma Horst-Martz, with PennPIRG, a consumer watchdog organization, says Pennsylvania has more such complaints than any state in the nation. But it isn’t just local stores upping their prices to cash in on a crisis, it’s rampant on websites like Amazon, eBay, Walmart and Craigslist.
 The Pennsylvania Attorney General’s office has received thousands of complaints about businesses raising prices and trying to take advantage of vulnerable people during the coronavirus crisis. This is illegal, and Attorney General Josh Shapiro has started an aggressive campaign to try to stop it.
 
 Shapiro will joins the Coronavirus Q & A today  to discuss the ways his office is trying to protect Pennsylvanians during this crisis.
 Get your questions ready!
 In a disaster such as we’re living through with COVID-19, it’s illegal to raise prices in Pennsylvania more than 20 percent of their normal level. But as Shapiro says, don’t worry about the math, just go with your gut and report anything you think is absurd, like hand sanitizer for $459. That’s exactly what PennPIRG found in its February investigation into online price gouging. They also found companies trying to sell Lysol wipes for $70. And our many of our readers are livid at the cost of toilet paper these days.
 We continue the topic of price gouging today with  PennPIRG’s Emma Horst-Martz.  PennPIRG, a consumer advocacy group,  says prices for many hand sanitizers and surgical masks on Amazon have spiked at least 50 percent during the coronavirus. A four-pack of Purell hand sanitizer was selling for $159. Two containers of Lysol wipes cost $70.
 Hoarding 
 This is a biggie. Some think it should be a crime to buy up half the supermarket and store it in your basement. The Giant Company President Nicholas Bertram says the reason his shelves are empty is because of human behavior, simple as that. That’s why many stores are limiting the number of items you can buy, especially of things like hand sanitizer, toilet paper and wine.
 Have a conscience, our readers say. Take what you need, even take a little more, but don’t take it all. The rest of us need to survive this crisis, too.
 People ignoring social distancing
 People were even photographed just this week crowding around a lake enjoying the sunshine and fishing – a beautiful spring sight. But they forgot even outside you’re supposed to stay six feet apart. Those virus really can travel that far.
 Anglers appear to be ignoring social distancing and the call for wearing face masks along one part of Children's Lake in Boiling Springs on the unexpected first day of the 2020 trout fishing season.
 The reason people are so irritated about social distancing scofflaws is this virus could be stopped in its tracks if we would just follow the rules. If we stayed apart, COVID-19 could not spread its little droplets from my hand to your hand and then to your nose. We could be over this whole stay-at-home thing quicker and not have to wonder if kids will go back to school in September.
 State police have been trying to be nice and just issuing warnings. But that could change and soon. The message from our readers, don’t risk arrest and their health. Just stay home, please.
 Rude Behavior
 Some people are not being nice at all when they do venture outside. It’s probably because they’re irritated at all of the above, but it’s no reason to take it out on a supermarket cashier. Remember, these folks are on the front lines, too, trying to make sure we have what we need when we need it. They’re being ordered around, yelled at and even subjected to the dreaded sneezing.
 Social distancing reminders have been placed in Karns stores. Karns grocery stores, including the one in Hampden Township, have installed plexiglass "sneeze guards" at registers due to coronavirus.
 The windows are the latest social distancing practice used in the stores along with floor decals and signs.
 March 31, 2020.
 Dan Gleiter | dgleiter@pennlive.com
 Kudos to those stores that have installed sneeze protectors to offer some defense against flying droplets. But as we all try to get through these harrowing weeks of sickness, death and uncertainty, let’s remember common courtesy, mutual respect and appreciation for those risking their own health to serve others.
 Note to readers: if you purchase something through one of our affiliate links we may earn a commission.
 Community Rules  apply to all content you upload or otherwise submit to this site.
 Ad Choices
 https://www.pennlive.com/opinion/2020/04/four-things-pennlive-readers-really-find-irritating-about-the-coronavirus-pandemic.html