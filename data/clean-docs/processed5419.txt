Opinion - Chicago Tribune
 The coronavirus will be the reason, we expect, Gov. J.B. Pritzker will make an even harder push for his graduated income tax. Voters have many, many reasons to reject the pressure of sending more money to Springfield.
 Illinois Attorney General Kwame Raoul says first responders heading out to an emergency call can legally be notified ahead of time that someone at that address has been confirmed as being infected with COVID-19. Mayor Lori Lightfoot says that’s wrong, and unfairly labels COVID victims.
 To prevent more COVID-19 cases, more detainees should be released from the federal Metropolitan Correctional Center.
 Our flag flies to show to us that through the dark of the night in which we now find ourselves, there is a dawn to come and we shall pass through to it.
 The Leons now live in a household of 10 — make that 11 in a few days when the baby comes home — and things are a bit crazy. That’s probably why Cheryl said she was telling me her story while sitting in a closet.
 Chicago might be able to take a page from Seattle's playbook to keep more green spaces open — yet safe.
 There are far more unknown unknowns waiting for us in the years ahead, because we’re about to enter an era when every single American can start a conversation by asking “What did you do?”
 This pandemic presents unique challenges for millions of Americans who are joining 1.5 billion Muslims around the world in welcoming the month of Ramadan.
 Eric Zorn takes on the top four arguments against the referendum proposal to switch from a flat-income tax rate to graduated rates in Illinois.
 Check out the latest cartoons by Tribune editorial cartoonist Scott Stantis.
 A look at the current state of politics through the eyes of editorial cartoonist Joe Fournier.
 Here's one way to fight COVID-19, Chicago. Have a favorite restaurant you can afford to patronize? Place an order, and please tip what you can.
 Many governors have used the coronavirus crisis to wage the culture war. Not J.B. Pritzker.
 While Republicans were blasting Nancy Pelosi for showing off her ice cream, the House speaker was negotiating much-needed funds for pandemic relief.
 How my ex-husband and I have gotten stronger during the coronavirus pandemic.
 No Man's Land, a sliver of unincorporated land on the North Shore, embraced its identity as an enclave of illicit drinking, gambling and partying amid bored suburbanites thirsting for a discreet night out during Prohibition.
 Editorial: Chicago Forward — Young lives in the balance: A call to action to reach Chicagoland’s disconnected youth
 We urgently call on you to join us again in our new “Chicago Forward” campaign: “Young lives in the balance: How to reach Chicagoland’s disconnected youth.”
 Is bankruptcy for Illinois — and for Chicago — the last opportunity for taxpayers to escape the death spiral that our political class got us into?
 Gov. J.B. Pritzker on Thursday extended the stay-home order for Illinois residents and businesses to May 30, a full month beyond his previous April 30 order.
 A virtual visit to a hospital in Danville in Central Illinois that's been all but untouched directly by COVID-19 but is nevertheless feeling the consequences.
 The World Health Organization, China and the U.S. all made crucial mistakes during this pandemic.
 Justice Brett Kavanaugh's concurring opinion in Ramos v. Louisiana strongly suggests that he's got Roe v. Wade on the brain.
 The two cases that could greenlight unfettered employment discrimination against gays and transgender individuals are currently awaiting decision in May or June.
 Our country is facing a serious problem: The environment seems to be getting cleaner and animals are prospering. With much of the world sheltering in place during the coronavirus pandemic, so-called “scientists” have noticed a pronounced drop in air pollution. Various forms of wildlife have been spotted venturing into areas previously occupied by people. This is, of course, an outrage.
 https://www.chicagotribune.com/opinion/
