Temporary Glasgow coronavirus hospital 'may not be needed' - BBC News
 A temporary hospital being built at the SEC in Glasgow will hopefully not need to be used, Nicola Sturgeon has said.
 The first minister announced earlier this week that the facility should be operational in a fortnight.
 But she told MSPs on Wednesday that she hoped the country's existing hospitals would have enough capacity to cope with the coronavirus crisis.
 Ms Sturgeon also confirmed that a further 16 people with Covid-19 have died, bringing the total to 76.
 She said there had now been 2,310 confirmed cases of the virus in Scotland, an increase of 317 from Tuesday.
 She again stressed that this was an underestimate, and that the actual number would be considerably higher.
 The emergency facility at the SEC will be run by the NHS and will initially have capacity for 300 patients, which could be expanded in the future to more than 1,000.
 It will be known as NHS Louisa Jordan, after a nurse who served with the Scottish Women's Hospital in Serbia during World War I. 
 Similar temporary hospitals are also being built in London, Manchester and Birmingham.
 But with an estimated 3,000 hospital beds expected to be available for coronavirus patients in traditional hospitals across Scotland, Ms Sturgeon said it was hoped that none of the beds at the temporary hospital would be needed.
 And she paid tribute to people for observing social distancing rules since the UK-wide lockdown was imposed at the start of last week, which she said would be crucial in limiting the demands on the NHS over the next few weeks. 
 She told MSPs: "I know that the last 10 days have not been easy for anyone but, overwhelmingly, people across the country have been doing the right thing.
 "That is not unexpected, but it is heartening and it will continue to be crucial in the weeks to come."
 The latest statistics showed that a total of 2,352 people with coronavirus had died across the UK by 17:00 by Tuesday - 563 more than in the previous 24 hours.
 Some experts believe that Scotland is behind other parts of the UK, particularly London, in terms of the spread of the virus, and that the lockdown was therefore introduced at an earlier stage of the country's "epidemic curve".
 They hope that this could result in Scotland continuing to have a lower death rate than elsewhere in the UK.
 Ms Sturgeon said: "Our current modelling of the spread of the virus - which I must stress assumes continued high compliance with the lockdown measures -together with the steps we are taking to increase ICU capacity, suggests our intensive care units are in a much stronger position to cope with the expected peak of the epidemic."
 There are currently 147 patients with coronavirus being treated in hospital intensive care units (ICU), which Ms Sturgeon said was likely to increase over the next two to three weeks.
 ICU capacity across Scotland has doubled to 360 beds, 250 of which will be for the exclusive use of coronavirus patients, with that figure expected to increase to more than 500 this week as work continues towards ultimately quadrupling the number to more than 700 ICU beds.
 About 1,900 tests for the virus are being carried out in Scotland every day, with the Scottish government expecting to increase that to 3,500 tests every day within a month.
 Ms Sturgeon said: "We are now at the stage of this epidemic, as we expected to be, when the number of cases is rising rapidly and unfortunately that means that the numbers becoming seriously unwell and dying are also, sadly, rising.
 "We hope that the lockdown measures we are asking people to comply with will have a marked effect on the spread of the virus, and that we will see a slowdown in the next few weeks. 
 "However, given that these measures take some time to have an impact, it is too early to draw any firm conclusions yet and we must continue to plan for what will be a considerable impact on the National Health Service and on wider society."
 Ms Sturgeon said ventilators have been ordered "from a range of manufacturers", with these due to be delivered in the coming weeks.
 NHS boards have been working to repurpose 200 operating theatre anaesthetic machines for use as ventilators to "bridge any gap" in provision ahead of these new ventilators arriving.
 The latest figures add another 737 people on to the nationwide toll of hospital deaths.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/uk-scotland-52121144
