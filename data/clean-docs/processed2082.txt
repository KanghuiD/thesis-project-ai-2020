•  Coronavirus (COVID-19) containment opinion Switzerland 2020 | Statista
                                                     Industry Overview
                                                 
 Dossiers
 Get a quick quantitative overview of a topic
 Outlook Reports
 Trend forecasts in the Statista Market Outlooks
 Surveys
 Market insights from an expert and consumer perspective
 Toplists
 Company rankings for sales and analysis
 Market Studies
 Complete markets in evaluation
 Industry Reports
 All key figures for an industry
 Global Business Cities Reports
 Global Business City rankings and key figures on cities
 Brand Reports
 Insights and benchmarks on brands worldwide
 Country Reports
 Countries and their potential
 Further Studies
 Get a deeper insight into your topic
 Everything you need to know about Consumer Goods
 Identify market potentials of the digital future
 Key topics in mobility
 Key economic and social indicators
 Insights on most important technology markets
 Figures and insights about the advertising and media world
 Find studies from all around the internet
 Sales and employment figures at a glance
 Data sets for your business plan
                     
                     Directly accessible data for 170 industries from 50 countries
                     and over 1 Mio. facts.
                 
                             
                     Customized Research & Analysis projects:
                         
                                 
                     Quick Analysis with our professional Research Service:
                     Content Marketing & Information Design for your projects:
 More Information
                         KPIs for more than 20,000 online stores
                                     Additional Information
                             Show source
  Show sources information
                              Show publisher information
 March 2020
 Switzerland
 March 18-24, 2020
 1,297*
 15-79 years
 Swiss residential population, weighted by gender, age group, region, household size and occupation
 Online survey
 * According to the source, the survey was conducted in the following Swiss regions: German-speaking Switzerland, western Switzerland and Ticino.
 The source provides no exact information on the survey question. The wording chosen here may therefore differ slightly from the original
 The percentages for the estimated time periods are partially cumulated, therefore the sum of the percentages exceeds 100.
 State of Health
 COVID-19 cases worldwide as of April 16, 2020, by country
 COVID-19 deaths worldwide as of April 16, 2020, by country
 Coronavirus (COVID-19) cases in Switzerland by canton in 2020
 Coronavirus (COVID-19) infection rate in Switzerland per 100,000 people 2020
                             Register in seconds and access exclusive features.
                         Free
                                                         $39 per month*
                                 (billed annually)
                         This feature is limited to our corporate solutions.
 You only have access to basic statistics.
 Corporate solution including all features.
 https://www.statista.com/statistics/1110249/coronavirus-covid-19-containment-opinion-switzerland/