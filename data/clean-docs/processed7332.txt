Coronavirus response | European Commission
 The European Commission is coordinating a common European response  to the coronavirus outbreak. We are taking resolute action to reinforce our public health sectors and mitigate the socio-economic impact in the European Union. We are mobilising all means at our disposal to help our Member States coordinate their national responses and are providing objective information about the spread of the virus and effective efforts to contain it. 
 President von der Leyen has established a Coronavirus response team at political level to coordinate our response to the pandemic
 During these times of crisis, across the European Union, countries, regions and cities are stretching out a helping hand to neighbours and assistance is given to those most in need: donations of protective equipment such as masks, cross-border treatments of ill patients and bringing stranded citizens home. This is European solidarity at its best.
 More video messages from President von der Leyen 
 An EU COVID-19 Data Platform has been established to rapidly collect and share comprehensive coronavirus research data, such as DNA sequences, protein structures, data from pre-clinical research and clinical trials, as well as epidemiological data, to advance research efforts. The European Commission supports research and innovation to develop vaccines, new treatments, diagnostic tests and medical systems to prevent the spread of the coronavirus.  
 Timeline of EU action
 Follow the latest progress and get involved.
 https://ec.europa.eu/info/live-work-travel-eu/health/coronavirus-response_en
