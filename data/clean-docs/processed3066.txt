West Seattle Blog… | Coronavirus
 West Seattle, Washington
 First construction, now recreation. The governor’s second “reopening” announcement tops tonight’s roundup:
 SOME STATE SITES REOPENING FOR RECREATION: Gov. Inslee still won’t say if he’s extending the stay-home order past May 4th, but today he did say one thing will be different starting May 5th: Some state parks will reopen, and some outdoor recreation. Two slides:
 Here’s our coverage, with video. P.S. We asked the city if the governor’s inclusion of golf would mean West Seattle and other city courses will reopen. No reply.
 NEWEST KING COUNTY NUMBERS: From the Seattle-King County Public Health data dashboard:
 *5,990 people have tested positive, up 78 from yesterday
 *416 people have died, up 9 from yesterday
 One week ago, the totals were 5,293 and 360.
 ANOTHER LOCAL DEATH: One of those newly counted deaths was in West Seattle. According to the data dashboard’s zip-code tracking, 98126 now has 6 deaths (we check all five zip codes daily, and it was 5 yesterday); 98146 remains at 3, 98106 at 2, 98136 at 1, 98116 at 0.
 STATEWIDE NUMBERS: Find them here.
 WORLDWIDE NUMBERS: Find them here.
 FREE FOOD: If you’re in need, here’s a chance to get help on Wednesday.
 TROUBLE PAYING YOUR INTERNET BILL? Comcast sent a news release saying it’s extending temporary policies into the summer such as:
 SIGNS OF SUPPORT: Did you see SFD and SPD out during Friday Night Lights last week? If your neighborhood got a visit from Engine 36, you might have seen the signs these kids made:
 They’re featured at the end of this SFD post, which explains their dad is firefighter Brian Friske of North Delridge’s Station 36, and they made the signs for Engine 36 to display. (No word yet if FNL will continue this Friday.)
 (Photo courtesy Jean Johnson Productions)
 They did it at their Issaquah campus, and next, it’s West Seattle. Eastridge Church is partnering with Convoy of Hope to offer free food and household supplies to ~300 families – first come, first served – this Wednesday (April 29th). Here’s the announcement:
 In response to the COVID-19 pandemic, Eastridge Church will provide free non-perishable groceries and household supplies to about 300 families in West Seattle on Wednesday, April 29, from 1-5 p.m. (while supplies last) at their West Seattle location, 4500 39th Ave. SW. The pre-packed bags include about 70 pounds of food and supplies and will be distributed one per car.
 Steve Jamison, lead pastor of Eastridge Church states, “We are thankful to partner with Convoy of Hope to provide assistance to families from our area who can use some help in this time of need.”
 Convoy of Hope is a faith-based disaster relief organization with a driving passion to feed the world through children’s feeding initiatives, community outreaches, and disaster response.
 Eastridge will practice safe distancing protocols through a drive-through distribution method. Drivers will be asked to open their trunk (no need to leave their car) and volunteers will load the supplies of pre-packed bags, while supplies last.
 Hal Donaldson, president of Convoy of Hope, shares that, “this is a united act of compassion. We’re seeing so many groups link arms to help people get through this crisis. In some respects, kindness is a medicine that many Americans need right now, and we’re seeing it being given out across the nation.”
 For more information, contact Eastridge Church at 425-270-6300.
 Eastridge is the church known for its turkey-and-groceries giveaways before Thanksgiving each year.
 !(function(src,cb){var s=document.createElement('script');s.src=src;s.async=true;if(s.readyState){s.onreadystatechange=function(){if(s.readyState=='loaded'||s.readyState=='complete'){s.onreadystatechange=null;cb();}};}else{s.onload=function(){cb();};}document.getElementsByTagName('head')[0].appendChild(s);})('//mediaplayer.invintusmedia.com/app.js',function(){Invintus.launch({"clientID":"9375922947","eventID":"2020041080","simple":true});});
 2:36 PM: Click into the video window for Governor Inslee‘s media briefing, with guests from state Parks, Public Lands, and Fish and Wildlife. We’ll update as it goes.
 He says he’s announcing a “partial reopening of outdoor recreation” starting on May 5th.
 -day use of state parks
 -day use of public lands
 -day use of state fish and wildlife areas
 “Any additional relaxing” would depend on “data and compliance,” he says, and warns “this is not a return to normal.” He says it’s about “data, not dates.” Team sports, events, camping, gatherings are NOT resuming, he stresses. He also urges people to continue to limit travel,”not make overnight trips” to recreation areas. He says golf will be OK if you’re playing with one other non-related person.
 Public Lands Commissioner Hilary Franz speaks next, saying she’s hopeful this is a “first step toward normalcy,” but urging visitors to bring their own sanitizer, masks, etc., and to continue practicing distancing.
 The next guest, Fish and Wildlife director Kelly Susewind, says most – but not all – hunting and fishing seasons will open (but not Areas 1-4 yet). He reiterates that there’ll be no camping – “stick with day trips.”
 It’s also noted that not all state parks will reopen – coastal parks, for example.
 2:56 PM: The governor didn’t say anything about extending the stay-home order (which expires in a week), so that’s the first question he’s asked. “We are a long ways from the end of this virus, and we are going to have to maintain plenty of restrictions after May 4th,” he said, without a “yes” or “no” answer, but saying he’ll have more to say within a few days.
 He’s asked what data told him it’s OK to partially reopen recreation, and he launched into a variety off stats, good and not-so-good. “We are SO far from being able to turn this off like a light switch,” he warns. He also says the state is still far short of the testing capability he would like to have, though he says he received a federal assuurance that more we would be on the way within a few weeks.
 What about elective surgery, which he had recently mentioned as something else that might reopen soon? “We are trying to come up with a protocol” to allow them without taking away from PPE that’s needed by those working on the most urgent health-care needs. “We’re still talking with stakeholders.”
 What about haircutters? The governor gives a fairly nonspecific answer – that all depends “when we drive this [infection] number down small enough” that contact tracing and isolation are possible, how much social distancing will be possible, what protocols can be developed.
 3:13 PM: The governor is asked why he can’t share specific benchmarks he’s looking at for decisions on reopening more of the state. “Going forward, we’re going to be looking at opening several more industries,” he says without naming them, saying “multiple metrics” are involved – not just the infection rate, but also the health-care system’s status, the testing/tracing capabilities – “you have to consider all those together.” The next questioner asks him for specific numbers. “There’s no one number,” Inslee reiterates, but mentions some such as the number of tests that come back positive, which he says is now close to 8 percent but needs to be lower. “It is not enough for our health and our safety to just eliminate social distancing” – contact tracing and isolation of people exposed, and their families, will be needed for a long time, he says.
 The briefing ends at 3:25 pm. As always, the video should be available for playback soon in the same window above, and we will add links to whatever the governor’s office posts to spell out today’s announcements. (Added: Here’s a link.)
 9:03 AM: With one week left in the stay-home order, Gov. Inslee has announced a media briefing/Q&A for 2:30 pm today. Looks like the topic will be outdoor recreation, part of what he has said would be likely to reopen soon – the announcement includes this:
 The governor will be joined by Hilary Franz, commissioner of public lands; Kelly Susewind, director of the Washington Department of Fish and Wildlife; and Don Hoch, director of the Washington State Parks and Recreation Commission.
 The live video stream will be here; we’ll carry it too.
 9:16 AM: Meantime, an announcement just in from the governor’s office – Colorado and Nevada are joining Washington, Oregon, and California in the Western States Pact, “a working group of Western state governors with a shared vision for modifying stay at home orders and fighting COVID-19,” originally announced two weeks ago.
 Another day without big headlines in the virus crisis – here’s our nightly roundup:
 NEWEST KING COUNTY NUMBERS: The daily update from the Seattle-King County Public Health data dashboard:
 *5,912 people have tested positive, up 101 from yesterday
 *407 people have died, up 8 from yesterday
 One week ago, those totals were 5,172 and 346.
 MORE COVID-19 SYMPTOMS: From the daily bulletin sent by the state Health Department:
 The Centers for Disease Control and Prevention has updated its official list of symptoms that people experience when they get sick from the coronavirus. The symptoms added are: chills, repeated shaking with chills, muscle pain, headache, sore throat, and new loss of taste or smell. This is in addition to the previous official symptoms: fever, cough, and shortness or breath. These symptoms may appear 2-14 days after exposure to the virus.
 WILL STAY-HOME ORDER BE EXTENDED? Governor Inslee‘s statewide stay-home order is currently set to expire May 4th, one week from tomorrow. No word yet if he’s planning any announcement or media briefing tomorrow, but if he does, we’ll carry the livestream.
 PAYCHECK PROTECTION PROGRAM REOPENS: The Small Business Administration will resume taking applications for these loans at 7:30 am our time tomorrow.
 THOUGHT THE ALKI CROWDING WAS BAD? Southern California is having a heat wave, and many of their beaches are supposed to be closed, but (updated) at one that wasn’t …
 — Chris Cristi (@abc7chriscristi) April 24, 2020
 (Here are the Orange County stats, by the way.)
 NEIGHBORHOOD NEWS: Worried about how kids are doing without being able to go to school? Meet a West Seattle second-grader who’s spending some of her time doing something we can appreciate … reporting neighborhood news.
 SIGN SIGHTING: For the second consecutive Sunday night, we wrap up the roundup with an Upper Morgan sighting sent by Tamsen Spengler (thank you!)
 That’s Gwendolyn, editor-in-chief of West Seattle’s newest neighborhood-news publication, with “this week’s issue, hot off the presses,” explains Laurel, who sent the photo and report:
 We wanted to let you know about a news publication that has been created in response to the coronavirus shutdown. Gwendolyn, a Gatewood second-grader, has started her own newspaper called The Daily Excitement. It’s a weekly! She has gotten the whole family involved and we also have guest reporters from around West Seattle and as far away as Arizona and Portland.  We’ve published 5 issues so far and we are going strong. You can find it at thedailyexcitement.com. and readers can write to us with feedback or submissions at thedailyexcitement@gmail.com. All are welcome. 
 Five issues are already online! We just browsed the first one; topics include gardening, birding, and cooking, with the last category featuring the headline “Salted Caramel Sauce: Dangerous, or Delicious?”
 The governor has said that “outdoor recreation” might get clearance to reopen soon. A unique West Seattle business – and longtime WSB sponsor – is ready to help people get ready for some of that. From Emerald Water Anglers:
 These are tough times, and we breathe the same breath of uncertainty as everyone else. As we move forward in this new normal, us at Emerald Water Anglers and our growing community want to reach out open arms of encouragement. We will get through this and be better because of it. Please stay safe, healthy, and positive. We are looking forward to being able to fish and continue business as usual soon.
 Social Media
 Vimeo Video Profile – EWA Vimeo
 EWA Coffee
 EWA’s shop is at 42nd/Oregon in The Junction.
 We’ve learned a lot about local nonprofits and their scope during the coronavirus crisis. If you are fortunate enough to be able to assist, rather than needing assistance, here’s another way to do that. From Vicki Quinn, president of St Vincent de Paul‘s Holy Rosary Conference, and Bob Bucci, president of SVdP’s Our Lady of Guadalupe Conference:
 During these weeks of enforced isolation, have you wanted to help the thousands of our neighbors here in West Seattle who have been laid off, face an uncertain future and are unable to pay the rent, utilities or even put enough food on the table for their kids? 
 The St. Vincent de Paul Society has been helping our neighbors here in West Seattle, from the Admiral District down to White Center, for the last 60 years. We reach out to our neighbors through our 65 dedicated local volunteers; pre-pandemic we would always visit with our neighbors in need in their homes to make sure we did not overlook any need that they might have forgotten in their anxiety and stress. These days we try to deliver the same message of compassion and care by phone. 
 Even before the Covid-19 crisis, the priority for St Vincent de Paul in West Seattle was to keep our neighbors in their homes, to stop evictions and do this by helping pay their rent.  
 In March 2020, the SVdP Helpline received a staggering 1.079 requests for rent help – 40% more than in February 2020, and a 47% increase over March of last year.  Many callers in West Seattle have never called any organization for help before. When we talk to them, our responsibility is to let them feel the compassion and love of our entire community – to let them know that they are not alone and that their community cares deeply about their situation. 
 Alternatively, please send a check payable to St. Vincent de Paul Society, write West Seattle in the Memo line and mail it to St. Vincent de Paul of Seattle/King County, 5950 Fourth Avenue South | Seattle WA 98108. 
 To learn more about SVdP after receiving this, we asked about referrals from 211, the phone number people can call for help and resources. The reply: “St. Vincent de Paul of Seattle | King County has been the largest 2-1-1 referral source for many years, receiving 12,000 to 25,000 referrals annually, more than any of the 1,700 social service agencies in the county. Of course, today those numbers are off the charts.”
 Two scenes shared by WSB readers, as joy resouds through our neighborhoods, even in these stay-home days and nights:
 The photo is from Laura Wood, who explains, “Trombone practice in the sunshine: Henry and Patrick Wood, 15 year old twins from West Seattle High School who really miss playing in their music groups!!”
 Below, fiddler Dawn Hepburn joined neighbors on the east side of The Junction in “making a joyful noise” on Friday night:
 She was in the Alki Masonic Lodge parking lot, with The Whittaker to the north and east, Broadstone Sky to the west, the 4801 Fauntleroy Apartments and Origins Cannabis (both WSB sponsors) to the south.
 Here’s a short nightly roundup following a quiet Saturday – :
 *5,811 people have tested positive, up 122 from yesterday
 *399 people have died, up 12 from yesterday
 One week ago, those totals were 5,063 and 340.
 STATEWIDE NUMBERS: See them here.
 WORLDWIDE NUMBERS: See them – nation by nation – here.
 NO WS FARMERS’ MARKET TOMORROW: Though the U-District and Ballard markets opened for a second weekend, the West Seattle market remains closed. No explanation so far.
 FREE FOOD TOMORROW: Reminder,the KBM Commissary weekly free meal will be available 4-6 pm Sunday – details here.
 FOOD DELIVERY – FOR STUDENTS: In Seattle Public Schools‘ latest message to families, the district says meal distribution will be done along 38 special school-bus routes, in addition to ongoing availability at certain campuses.
 IN CASE YOU DIDN”T SEE THE SUNSET … thanks to James Bratsanos for the photo:
 GOT SOMETHING TO REPORT? westseattleblog@gmail.com or 206-293-6302, text/voice – thank you!
 Your local, independent businesses need you more than ever, whether they’re open, partly open, or entirely closed. Here are two videos about supporting them:
 FCA SUPPORTING ENDOLYNE JOE’S: This time of year, Endolyne Joe’s (9261 45th SW) usually has a dine-out fundraiser to support the Fauntleroy Fall Festival. This year, Joe’s needs the support instead – since like other restaurants it’s only allowed to be partly open – so the Fauntleroy Community Association urges you to order lunch and/or dinner there on Tuesday (April 28th), and made this video to explain:
 ‘SMALL BUSINESS IS FAMILY’: The proprietor of Flourish Beauty, Juniper Nails, and Spruce Apothecary, Tiann Hadeed, made this video not just about her businesses, but “to help educate our clients and community on the importance of supporting small business.”:
 Time to start Saturday with grocery-shopping updates. The only major change this past week is that Westwood Village QFC has launched curbside pickup; Christine from the store’s e-commerce team told us it’s being rolled out at most QFCs, and that the usual service fee is being waived during the coronavirus crisis. … Hours at local standalone stores, including senior/at-risk shopping hours, stayed unchanged (here’s our standing list).  We’re only shopping once a week, so we can’t scout all the stores for you, but readers have reported in each week, and it sounds like the safety measures are getting to be almost universal: Plexiglas, masks, floor spacing, etc. The main variable: Your fellow shoppers. They’re not all masking up, or paying attention to the one-way-aisle markers. (So don’t be THAT shopper!) Looking to avoid crowds? Shop in the first or last hour that your chosen store’s open. And let us know if you discover something new/changed! (Example: West Seattle Thriftway [WSB sponsor] says it has a new supply of garden seeds!)
 As expected, the governor has given one industry a partial clearance to get back to work, and that tops tonight’s roundup, exactly eight weeks after the first King County COVID-19 case was announced:
 GOVERNOR SAYS SOME CONSTRUCTION PROJECTS CAN RESUME: Gov. Inslee was joined at midday by industry and union reps with whom he said a safety plan had been worked out, in a process that he called a potential template for restarting other industries (no timeframe, though). Our coverage includes the video; here’s the announcement on the governor’s website, including the full list of rules.
 *5,689 people have tested positive, up 120 from yesterday
 *387 people have died, up 3 from yesterday
 One week ago, those totals were 4,902 and 331.
 2 DEATHS AT THE MOUNT: West Seattle long-term care/assisted-living center Providence Mount St. Vincent announced late today that two residents/patients who had tested positive for COVID-19 have died. (It’s in the zip code with the most deaths of any West Seattle zip code, per the data dashboard – 98126, with 5.)
 We were going to go back in the morning to check if they all were being removed, but Parks has just saved us the trouble, tweeting as we wrote this: “Earlier this week the parks department placed barriers at benches to help encourage visitors to keep moving and avoid congregating. Understanding that our seniors and those who are differently abled use benches to temporarily rest, we’ve decided to replace barriers with signs.”
 ALSO SEEN ON ALKI: The SPD Mounted Patrol was back today:
 Thanks to GT for the photo.
 SPEAKING OF FOOD: If you can donate some, Sunday will bring a local donation drive.
 NEIGHBORHOOD SIGHTING: Thanks to the texter who sent this:
 GOT INFO? westseattleblog@gmail.com or text/voice 206-293-6302 – thank you!
 Another fundraising auction that had to go online because of the coronavirus crisis is happening right now – here’s the announcement from Holy Rosary School:
 Due to COVID-19, Holy Rosary School’s annual auction has moved ONLINE.  The great news is you don’t need a ticket to participate. EVERYONE IS INVITED! Over 125 items will be up for bid between today and Saturday evening.  Closings are staggered, starting at 6:30 pm on Saturday. 
 Please consider bidding on items or donating to one of the school funds. If bidding, please read all restrictions thoroughly.  Winning bidders will be notified to pick up their items when the Stay at Home has been lifted or shipping is available at the winning bidder’s cost.  Thank you in advance for your support.
 For additional information, visit the Holy Rosary School website.
 (WSB photo, last week)
 Two Providence Mount St. Vincent residents/patients who tested positive for COVID-19 have died. The Mount announced that in an update sent today, eight days after announcing that 15 residents/patients and 10 caregivers there had tested positive. At the time, The Mount said a second round of testing was planned, and this update includes news on that:
 Thank you to our West Seattle friends, neighbors, and family members of The Mount for continuing to express care and concern for the residents, patients, and caregivers at The Mount. We have been humbled by the outpouring of love we have received in the form of handmade masks, deliveries of flowers and goodies as well as messages of support. We hope you know how much this all means.
 Thanks to Providence ExpressCare, we did a second round of COVID-19 rapid testing this week where we tested 732 residents/patients and caregivers. Due to the tremendous work of our team, we are doing an excellent job at controlling the spread of COVID-19 by caring not only for our patients and residents, but also in taking care of each other. The results of our second round of testing show a stabilization of the spread of COVID-19.
 One new resident/patient tested positive and two caregivers tested positive. Additionally, several of those patients/residents who previously tested as positive, are now in the process of clearing with first step negative test results. These will be confirmed with a second test.  Today there are 8 residents/patients who are positive for COVID 19.  Caregivers will be cleared to return to work through caregiver health. 
 We are deeply saddened that two of our residents/patients who tested positive for COVID-19 have died. These two individuals were cherished members of The Mount family and our collective hearts are broken. We extend our deepest sympathies to their families and loved ones and we are keeping them in our prayers in their time of loss.
 The safety and well-being of our residents, patients, their families and our caregivers remains our top priority, especially at this time. Even with signs of improvement, we will not let our guard down as we know how quickly this virus can spread. Our heightened infection control protocols, including the use of appropriate personal protection equipment (PPE), remain in place and we continue daily monitoring of all residents, patients, and caregivers for any signs of illness. COVID-19 has our full attention and we are committed to protecting everyone in our care. We will continue to share updates with the community as needed.
 The Mount (4831 35th SW) is West Seattle’s largest long-term-care/assisted-living facility, with other programs on site including the Intergenerational Learning Center day care.
 Last night, we reported on the West Seattle Chamber of Commerce’s letter to the City Council asking for a cap on third-party restaurant delivery fees, noting that other cities have taken similar actions. This afternoon, Mayor Jenny Durkan, City Council President Lorena González, and City Councilmember Lisa Herbold just announced an emergency order to do just that – and more. Here’s the announcement:
 “Our beloved main street restaurants are reeling from this economic crisis and exorbitant delivery service charges further threaten their ability to weather this storm. Restaurant owners across Seattle have adapted their business models to delivery or takeout only service, resulting in the unemployment of thousands of service industry workers and even thinner margins for these important small businesses. With tight margins, every dollar paid to an app-based delivery service is a dollar taken from our local restaurants, economy and workforce. We know some of these corporations are imposing inflated fees and profiting from this crisis on the backs of our main street. We cannot allow that to happen. This Emergency Order will provide much needed relief and establish a system that is more fair and equitable to our restaurants,” said Council President M. Lorena González.
 To further protect delivery drivers who, as independent contractors, are often shut out of federal unemployment relief, the Emergency Order requires that 100 percent of tips go to the drivers, and it includes provisions to make clear that it is illegal for a third-party platform to reduce driver compensation rates as a result of this order going into effect for the duration of the order.
 “Marination and Super Six currently rely on takeout and delivery in order to have an opportunity to survive in this new economic environment. Because so many of these platforms charge such high fees, we have started to handle all takeout and delivery orders in-house. But this commission cap will allow us to transition to a third-party delivery service without facing further financial stressors and allows third-party platforms and restaurants to do what they do best,” said Kamala Saxton, co-owner of Marination and Super Six.
 The 15 percent commission cap will take effect immediately and will remain in place until restaurants are allowed to offer unrestricted dine-in service. Violating the 15 percent commission cap is a misdemeanor offense and would be prosecuted by the Seattle City Attorney’s Office. Restaurants who wish to report a violation of the commission cap should call the Seattle Police Department’s non-emergency line at 206-625-5011.
 After more than five weeks, we’re still updating our list of West Seattle restaurants (and other prepared-food providers) and beverage businesses, as hours change and some reopen (as of today, A La Mode Pies in The Junction joined the latter category). See the list of 140+ businesses by going here. Thanks to everyone who continues to send us updates – both restaurateurs/staffers and customers – westseattleblog@gmail.com or text/call 206-293-6302. 
 P.S. Some other lists have sprung up too; one that’s exclusively local, and locally created, is westseattle.delivery. The West Seattle Junction Association also has a list on its website.
 !(function(src,cb){var s=document.createElement('script');s.src=src;s.async=true;if(s.readyState){s.onreadystatechange=function(){if(s.readyState=='loaded'||s.readyState=='complete'){s.onreadystatechange=null;cb();}};}else{s.onload=function(){cb();};}document.getElementsByTagName('head')[0].appendChild(s);})('//mediaplayer.invintusmedia.com/app.js',function(){Invintus.launch({"clientID":"9375922947","eventID":"2020041078","simple":true});});
 11:32 AM: Gov. Inslee is having another news-media briefing right now, scheduled to be joined this time by building-industry representatives. Click into the live coverage above; we’ll be adding notes as it goes. 
 He opens by thanking people for continuing to follow “Stay Home, Stay Healthy” and cites various polls showing support for it, while declaring that it’s “working” by saving lives. He then goes on to say “we’ve  found a way (for) low-risk construction that’s under way to resume.” He says a working group came up with recommendations to shape that and expects it to be “a good template” for other industries. But he says he still can’t say “when other businesses could reopen. … The day of reopening our whole economy is not today … it would be way too dangerous.”
 11:40 AM: He outlines the safety plan for construction sites, including that “if it can’t be done with social distancing, it can’t be done.” Every jobsite will have to have a safety supervisor.
 For other industries, he says, his office and the Department of Commerce “will convene stakeholder groups” to come up with plans.
 11:47 AM: After a few statements from building-industry group reps, it’s on to Q&A. First Q: How many projects will be able to resume? Probably most, but the plan addresses “types of tasks” that can be done safely, rather than types of projects, one rep replies. Second Q is about test-kit availability, and the governor says he’s continuing to work on that. Third Q, when will this enable resumption of some construction work? As soon as the governor signs the order, later today.
 In response to another question, he repeats that data and science will drive when other industries can reopen, and says that “probably tomorrow” that data will be discussed in detail. On the next followup, he repeats that the aggressive measures were necessary to “bend the curve … We now have bent the curve,” but reopening the rest of the economy has to be “an incremental process.” But “if we push that green button too soon, a lot of people are going to die.” 
 What will reopen next? As he said earlier in the week, they’re looking at elective surgery and outdoor recreation.
 12:06 PM: The briefing/Q&A is over. The video should be available for playback above shortly; we’ll also add any links that become available with details of the governor’s construction-industry order.
 3:11 PM: Here’s the governor’s post, with more details.
 (Photo courtesy Alki UCC, April 12th)
 That’s part of what was donated outside Alki UCC on Easter Sunday two weeks ago, and they’ve sent a reminder that you have another chance this Sunday:
 Thanks to our community’s generosity, Alki United Church of Christ (Alki UCC) will once again be accepting donations outside our building for an In-Person, Socially-Distanced Food Drive this Sunday, April 26 from 10:00 am to 4:00 pm.
 Contributions of non-perishable food and other items will be distributed via the White Center Food Bank; top requests include Canned Meat/Soup/Fruit (pop‐top cans preferred), Rice, Noodles, Peanut Butter, Oats, Toilet Paper, Diapers, Similac Formula, Cleaning Supplies, Hand Sanitizer, and Baby Wipes.
 The drive will benefit our vulnerable neighbors in need, those who have been disproportionately impacted by COVID 19. The food drives will continue every other Sunday (May 10 and 24) until further notice. Check alkiucc.org for updates.
 The church is at 6115 SW Hinds.
 One more summer-event cancellation has been announce: Delridge Day. But the community group that makes it happen has found a way to ensure the festival’s gifts are given anyway. From Pete Spalding:
 After much discussion, thought and contemplation of options. Visualizing Increased Engagement in West Seattle (VIEWS) has made the difficult decision to cancel the 2020 edition of the Delridge Day Festival.
 One of the things that VIEWS prides itself on is our commitment to giving back to the community that supports us. As you might be aware. we make donations each year after Delridge Day to help support community organizations.  We have made the decision to dip into our reserves this year to continue to support vital community organizations that are on the front lines in helping our neighbors during this crisis. So we are making our own stimulus payments to:
 $1,200    West Seattle Food Bank
 $1,200    White Center Food Bank
 VIEWS plans at this point include a Gathering of Neighbors event during the first quarter of 2021. A part of this event will be a segment where we plan on recognizing the Heroes of Delridge who are making a difference during this crisis situation that we are going through right now.
 We look forward to welcoming everyone to the 2021 edition of the Delridge Day Festival on the second Saturday of August.
 The governor’s making an announcement tomorrow morning, and that tops tonight’s roundup:
 GOVERNOR’S NEXT ANNOUNCEMENT: It’s set for 11:30 am Friday, and you can guess the subject from the guest list:
 The governor will be joined by Greg Lane, executive vice president, Building Industry Association of Washington and Mark Riker, executive secretary of the Washington State Building and Construction Trades Council. 
 Gov. Inslee had said earlier this week that a resumption of residential construction was likely soon. You can watch his remarks and media Q&A via livestream here.
 BUT WHAT ELSE IS AHEAD? From the state Health Department‘s daily bulletin:
 Our epidemiological data suggest that COVID-19 activity peaked in Washington at the end of March. While activity declined during early April, this decline may have slowed during the past week. Data from the past week are always preliminary and difficult to interpret so we will not fully understand these data for another week. The public health system in Washington is currently responding to outbreaks of COVID-19 in long term care settings, homeless shelters, food processing plants and among agricultural workers.  
 The Department of Health has convened an expert group of modelers to analyze our epidemiologic data. This group predicts with a high degree of confidence that relaxation of social distancing conditions to pre-covid19 levels will result in a sharp increase in the numbers of cases after 2 weeks. The group also believes that current diagnosis counts are still too high to lessen social distancing measures within the next two weeks. 
 *5,569 people have tested positive, up 120 from yesterday
 *384 people have died, up 5 from yesterday
 One week ago, those totals were 4,809 and 320.
 STATEWIDE NUMBERS:  Find them, county by county, on the state Department of Health page,.
 WORLDWIDE NUMBERS: More than 2 million cases. See how that breaks out, nation by nation, here.
 ‘KEEP IT MOVING’ = NO SITTING: A new level of restrictions spotted at Alki Beach Park:
 The park may be open, but the benches are “closed,” with boards across them, as of late today. We have an inquiry out to the city to ask where else this is being done.
 ALSO NOT HAPPENING AT ALKI: This year’s Alki Art Fair is the latest big spring/summer West Seattle event to be canceled – but watch for online spotlights next month.
 FIRST SERVICE CUTS, NOW CAPACITY CUTS: More changes for those who need to ride Metro buses in these pandemic times.
 NEED A MASK? Local family’s making them to raise money for local nonprofits.
 NEED FOOD? Here’s the menu for the free meal available via KBM Commissary on Delridge – home to many food-truck and catering chefs – this weekend.
 WHY TAKEOUT IS BETTER THAN DELIVERY … for the restaurants fighting to stay afloat.
 DIFFERENT DELIVERY: Tomorrow night, SFD and SPD vehicles will be out for a second round of “Friday Night Lights.” If you see them, please send a pic!
 Another major West Seattle summer event will take this summer off amid COVID-19 uncertainty. From Alki Art Fair organizers:
 It is with great sadness that we must cancel the 23rd annual Alki Art Fair. This was an incredibly difficult, but, necessary decision to make to minimize the potential spread of the coronavirus and to honor our responsibility to keep our community and families safe. 
 Let #AlkiArtFairatHome brighten your day by bringing art and music into your homes during this challenging time. We would urge you to support our artists in whatever way you can – purchase their art and their music, and, or, follow them on social media. They need all of our help right now. 
 And, finally, mark your calendars for the 24th annual Alki Art Fair – July 24-25, 2021. We very much look forward to welcoming you in-person next summer for a celebration of local art and music. 
 Stay safe, friends!
 Attention: City of Seattle Council Members
 Many restaurants lose money on their deliveries during normal market conditions,  the West Seattle Chamber of Commerce Board and its members are asking that you follow suit with San Francisco and now New York in limiting the fees that are able to be collected in the interest of our local businesses. 
 We recommend a cap of 15% to allow these businesses to continue to operate. The Chamber is concerned for our members. We must preserve some profit for restaurants so they can serve the West Seattle residents who rely on restaurants to cook their meals and allow these businesses to retain their employees. 
 Reducing these fees would encourage more restaurants to get off the sidelines and reopen if they knew they could pay a fair commission rate.  Our goal is to Bring back much needed jobs and stability to our service industry workers.
 Sadly, these companies have stood firm around not negotiating fees since the start of this crisis, all these businesses are asking for is a fair charge.
 We hope that you will help make this right .
 Respectfully, 
 Julia Jordan
 CEO, West Seattle Chamber of Commerce
 San Francisco ordered a 15 percent cap two weeks ago.
 Username: 
 Password: 
 Lost Password?
  Enter the code in the image*: 
 West Seattle Blog 
  3 months ago 
 New owners for a West Seattle landmark! westseattleblog.com/2020/01/biznote-passing-of-the-torch-at-poggie-tavern-new-ownership-after-23-... ... See MoreSee Less
 BIZNOTE: 'Passing of the torch' at Poggie Tavern - new ownership after 23 years
 westseattleblog.com
 Avoid 18th/Cambridge. Big SFD response for shed burning behind a house. Updating on WSB at westseattleblog.com/2020/01/seattle-fire-full-response-in-south-delridge-2/ ... See MoreSee Less
 https://westseattleblog.com/category/coronavirus/
