Analysis & Opinion - ABC News (Australian Broadcasting Corporation)
 While other states and the Commonwealth plunge head-first into eye-watering deficits Western Australia defies that trend in an emphatic way, and the Government is making no apology for it, writes political reporter Jacob Kagi.
         By Jacob Kagi
 WA is set to join governments around the world today in handing down a budget aimed at stimulating an economy struggling in the face of COVID-related job losses. But some believe in delivering a surplus, it's not going far enough.
         By Andrea Mayes
 The LNP hasn't got time on its hands — the election campaign officially started on Tuesday, early polling opens in just 10 days' time, and many Queenslanders are only just now starting to pay attention to state politics.
         By state political correspondent Peter McCutcheon
 In leading Australia's women's cricketers to a record-equalling ODI winning streak, Meg Lanning is putting up numbers greater than Ricky Ponting's, but her brilliant form is not the only reason, writes Geoff Lemon.
         By Geoff Lemon, in Brisbane
 With week one of the AFL finals done, the Tigers and Cats have taken their second chances. Cody Atkinson and Sean Lawson explore whether it's worth it, or if the extra game is just a full stop to the season.
         By Cody Atkinson and Sean Lawson
 The Morrison Government treats coronavirus seriously but this week's Budget does share a little of Donald Trump's "don't let it dominate your life" vibe, writes David Speers.
         By Insiders host David Speers
 After heavily contrasting results in week one, Collingwood and Geelong enter this weekend's AFL semi-final at opposite ends of the momentum scale — but in the cut and thrust of finals, how much does that really matter?
         By Dean Bilton
 Amid simmering tensions with Beijing, four regional allies that make up the Quad come together. But the meeting reveals Australia, the US, India and Japan have very different ideas on how to tackle a rising China, writes Jake Sturmer. 
         By North Asia correspondent Jake Sturmer in Tokyo
 The Government is placing a huge focus on getting people back into work, but there's little quality control on what jobs will be offered and no assurance that those who miss out won't be left in poverty, writes Michael Janda.
         By business reporter Michael Janda
 Trying to make fiscal forecasts in the pandemic is like playing "pin the tail on the donkey" — in six months we'll know if the Government's budget gamble has paid off, writes Laura Tingle.
         By Laura Tingle
 Scrolling through the endless zeroes of our newly minted national fiscal document, you'd be forgiven for thinking that we'd finally found the Moby Dick of the genre: the loser-free budget. Think again, writes Annabel Crabb.
         By Annabel Crabb
 The Government ditched its free market ideology to prop up the economy during the peak of the COVID-19 crisis, but this Budget sets out a path back towards business as usual for the Coalition, writes Ian Verrender.
         By business editor Ian Verrender
 Time was, Australia could count on Meg Lanning and Ellyse Perry to churn out runs in partnership — now Lanning and her deputy Rachael Haynes are doing the same, seemingly inevitable job, writes Geoff Lemon.
         By Geoff Lemon in Brisbane
 Both the Premier and Opposition leader are going to absurd lengths to the deny the obvious — there's a good chance neither Labor nor the LNP will form a majority government after the October 31 election.
 This year's Spring Racing Carnival, with little or no crowds, might give an indication of how much the spectacle depends on the party as much as the races for its appeal, writes Richard Hinds.
         By Offsiders columnist Richard Hinds
 For many of us, becoming ill with a virus might put us on the couch for a week or two. But for some people, contracting a viral infection like coronavirus can be life-altering, writes Michael Musker.
         By Michael Musker
 In a year like no other, coronavirus has changed almost everything — including how we take part in the most Australian of summer rituals.
         By Bellinda Kontominas
 Earning a week off makes Penrith and Melbourne hot favourites to make the NRL grand final, but red-hot Souths pose a genuine threat, writes ABC Grandstand's Luke Lewis.
 The campaign launch may seem a pointless election ritual to voters, but that's because voters are not the target audiences of these events.
         By ACT political reporter Tom Lowrey
 Have you got a story you'd like to tell? Would you like to write an opinion piece? Please send your 100-word pitch to features@abc.net.au
 Mon - Fri 6pm local on ABC TV
 9pm AEST on ABC News Channel
 If you have inside knowledge of a topic in the news, contact the ABC.
 ABC teams share the story behind the story and insights into the making of digital, TV and radio content.
 Read about our editorial guiding principles and the standards ABC journalists and content makers follow.
 Learn more
 AEDT = Australian Eastern Daylight Savings Time which is 11 hours ahead of GMT (Greenwich Mean Time)
 https://www.abc.net.au/news/analysis-and-opinion/
