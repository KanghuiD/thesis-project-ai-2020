Coronavirus crisis 'like no other' as cases near 2m: Live updates |  News | Al Jazeera
 IMF issues stark economic warning as confirmed infections worldwide approach the 2m mark and death toll tops 120,000.
 IMF says the global economy is expected to shrink by 3 percent this year, forecasting the steepest downturn since the Great Depression of the 1930s
 Total number of coronavirus cases in Russia has surged past 20,000 as the country posted a record number of daily cases.
 Some 1.93 million people around the world have now been confirmed to have the new coronavirus, according to data compiled by Johns Hopkins University. More than 120,000 have died, while nearly 450,000 have recovered.
 Sweden says the number of coronavirus deaths in the country has exceeded 1,000.
 Here are the latest updates:
 US intelligence indicates that the coronavirus likely occurred naturally, as opposed to being created in a laboratory in China, but there is no certainty either way, the top US general said.
 The remarks by Army General Mark Milley, the chairman of the Joint Chiefs of Staff, could fan speculation about the coronavirus' origins - something China has dismissed as a conspiracy theory that is unhelpful to the fight against the pandemic.
 Asked whether he had any evidence that the virus perhaps began in a Chinese laboratory and was perhaps released accidentally, Milley was non-committal at a Pentagon news briefing.
 "There's a lot of rumor and speculation in a wide variety of media, the blog sites, etc. It should be no surprise to you that we've taken a keen interest in that and we've had a lot of intelligence take a hard look at that," Milley told a news briefing.
 "And I would just say, at this point, it's inconclusive although the weight of evidence seems to indicate natural. But we don't know for certain."
 The International Monetary Fund views its $1 trillion in lending capacity as "quite substantial" to help members deal with the coronavirus pandemic, but further resources may be needed as the full brunt of the crisis reaches developing countries, its chief economist told the Reuters news agency.
 Gita Gopinath said 100 of the IMF's 189 members, of whom half were low-income countries, had now contacted the global lender about receiving emergency funding to beef up their efforts to contain the spread of the novel coronavirus and mitigate its economic impact.
 She welcomed an agreement by international creditors to suspend debt payments for the poorest countries through the end of the year as a "very, very good step," but said debt relief measures might have to be extended into 2021 since the worst of the pandemic's effects had not yet been felt in many of the poorest countries.
 The governor of Rio de Janeiro state said that he had tested positive for the new coronavirus, becoming the latest Brazilian government leader to contract the disease as it spreads across the country.
 The European Commission plans to narrow controls on the export of coronavirus protective equipment to just a single product - masks - as well as exempt the countries of the western Balkans from the restrictions.
 The EU executive, which overseas trade policy for the 27 EU member states, set out a draft regulation on Tuesday to apply for 30 days from April 26. The adjustments were designed to meet the EU's global commitments in tackling the COVID-19 pandemic.
 The bloc's current restrictions apply to protective spectacles and visors, face shields, protective garments, gloves, as well as mouth and nose masks.
 These products can only be exported to a non-EU country with an authorisation granted by individual EU countries. The restrictions were due to run from March 15 to April 25.
 The restrictions came in after a number of countries, including France and Germany, introduced their own export controls, angering fellow members such as Italy that were most in need of such equipment. 
 The US State Department reported its first coronavirus fatality among the staff at its headquarters in Washington, bringing the total death toll in its global workforce due to the outbreak to five.
 Walters said that the deceased individual, a civil servant, had been out of the office for more than two weeks. He did not provide further details including in what role the individual served at the State Department, citing privacy issues.
 Among the other four fatalities in the State Department's global workforce, three others were locally employed staff in foreign missions while a fourth person, a contractor working in New York, died of the illness last week.
 US President Donald Trump will hold a video teleconference with G7 leaders on Thursday to coordinate national responses to the coronavirus pandemic, the White House said.
 Trump, who is head of the G7 this year, had to cancel the group's annual summit, which he had planned to hold at the presidential retreat of Camp David, Maryland, in June.
 The Group of Seven nations include the United States, France, Britain, Italy, Canada, Japan and Germany, and all seven of them have been hit hard by the virus.
 China reported 89 new coronavirus cases on April 13, of which 86 were imported, the health authority said.
 The high number of imported cases in the country awakens fear of a second wave of coronavirus cases because of Chinese citizens coming back to their homeland from abroad.
 Apple Inc said it would release data that could help inform public health authorities on whether people are driving less during lockdown orders to slow the spread of the new coronavirus.
 The data is gathered by counting the number of routing requests from Apple Maps, which is installed on all iPhones, and comparing it with past usage to detect changes in the volume of people driving, walking or taking public transit around the world, Apple said.
 The information is being updated daily and compared with a date in mid-January, before most US lockdown measures were in place, Apple said.
 More than 90 percent of Americans are under stay-at-home orders and various lockdowns are underway in other countries around the globe.
 France officially registered more than 15,000 deaths from coronavirus infections, becoming the fourth country to go beyond that threshold after Italy, Spain and the United States, while the rate of increase of fatalities is slightly up again after steadying the days before.
 But the number of people in intensive care units fell to 6,730 from 6,821 over 24 hours, with this total declining for a sixth consecutive day, suggesting the national lockdown, extended to May 11 on Monday, is having positive effects in containing the disease.
 During a news conference Jerome Salomon, head of the public health authority, said the number of people who died from the disease in French hospitals and nursing homes had risen by five percent in a day to a cumulative total of 15,729, versus four percent on Monday and Sunday.
 Austria allowed thousands of shops to reopen on Tuesday, becoming one of the first countries in Europe to loosen a lockdown imposed to slow the spread of the coronavirus.
 Austria acted early in its outbreak to close schools, bars, theatres, restaurants, non-essential shops and other gathering places roughly four weeks ago. It has told the public to stay home and work from there if possible.
 The Alpine republic has fared relatively well so far, having reported 384 deaths in total, fewer than some larger European countries have been suffering each day. The daily increase in confirmed cases has fallen to low single digits in percentage terms and hospitalisations have stabilised.
 French space scientists are using the COVID-19 lockdown as a dry run for what it will be like to be cooped up inside a spacecraft on a mission to Mars.
 The guinea pigs in the experiment are 60 students who are confined to their dormitory rooms in the southern city of Toulouse - not far removed from the kind of conditions they might experience on a long space mission.
 Read more here.
 President Yoweri Museveni extended Uganda's initial 14-day lockdown by an extra three weeks, until May 5, as part of efforts to curb the spread of the novel coronavirus.
 Uganda and Rwanda have imposed a total lockdown in their fight against COVID-19, in east Africa's most stringent response to the infection.
 The top US infectious disease expert Anthony Fauci said that a May 1 target date for reopening the economy was "a bit overly optimistic," citing a lack of critical testing and tracing procedures.
 "We have to have something in place that is efficient and that we can rely on, and we're not there yet," Fauci, head of the National Institute of Allergy and Infectious Diseases, told the AP news agency in an interview.
 Fauci also told the AP that the length of the daily White House coronavirus briefings was "really draining". Trump, who is running for re-election in November, has at times used the briefings to promote his role and attack political opponents. Monday's lasted two and a half hours.
 Deaths from the COVID-19 epidemic in Italy rose by 602, up from 566 the day before, posting a second consecutive daily increase, but new infections slowed to 2,972 from 3,153, seeing the smallest daily tally since March 13.
 The total death toll since the outbreak came to light on February 21 rose to 21,067, the Civil Protection Agency said, the second-highest in the world after that of the United States.
 The number of officially confirmed cases climbed to 162,488, the third highest global tally behind those of the US and Spain.
 There were 3,186 people in intensive care on Tuesday against 3,260 on Monday - an 11th consecutive daily decline.
 Of those originally infected, 37,130 were declared recovered against 35,435 a day earlier.
 Zimbabwe's President Emmerson Mnangagwa has threatened a jail term of up to 20 years to the author of a statement, purporting to bear his signature, that said the lockdown to contain the coronavirus outbreak had been extended.
 Mnangagwa, who was speaking at his farm after touring Gweru city in central Zimbabwe, told state broadcaster ZBC on Tuesday that the statement, which circulated on social media last week and was immediately denied by the government, was fake.
 Turkey's confirmed cases of the coronavirus increased by 4,093 in the past 24 hours, and 98 more people have died, taking the death toll to 1,296, Health Minister Fahrettin Koca said.
 The total number of cases in the country stood at 61,049, he said.
 A total of 3,957 people have recovered so far, and the number of tests carried out over the past 24 hours was 34,456, the minister said.
 The construction sector would begin to reopen on Wednesday, said Prime Minister Imran Khan at a news conference, following a meeting of the country's National Coordination Committee on the coronavirus.
 Provincial authorities will, however, be free to extend their restrictions on these industries if they believe it is necessary, Khan said. 
 Sweden reported that more than 1,000 people had died from the novel coronavirus, as a group of experts attacked the authorities' approach to the crisis.
 Sweden's Public Health Agency said it had recorded a total of 11,445 confirmed cases of COVID-19 and 1,033 deaths.
 And it warned that because not all deaths had yet been reported over the four-day Easter weekend, the true number might be higher.
 Denmark's government wants to reopen society more quickly than previously anticipated, as the number of coronavirus-related hospitalisations continues to fall, Prime Minister Mette Frederiksen said.
 "The number of hospitalisations have not risen, they have fallen. And the number of people in intensive care is also falling," Frederiksen told a news briefing, without specifying the details of the government's proposal.
 The proposal will be discussed with other parties in parliament later on Tuesday, Frederiksen said.
 The International Monetary Fund did not mince words about its dire outlook for a coronavirus-ravaged world economy.
 "It is very likely that this year the global economy will experience its worst recession since the Great Depression, surpassing that seen during the financial crisis a decade ago," IMF Chief Economist Gita Gopinath wrote in the foreward for the fund's World Economic Outlook. 
 A popular line of toy figures in the Czech Republic have begun wearing face masks along with the rest of the country to raise funds to buy equipment for medical staff fighting the novel coronavirus.
 Toymaker Efko began equipping its small plastic Igracek figurines - which are similar to Lego or Playmobil - with masks in March.
 It is donating part of its sales from the toys marked "help with Igracek" to purchase protective equipment for doctors and nurses in the central Vysocina region where the company is based.
 "We thought if everyone else has to wear a mask, then so should Igracek," said Efko CEO Miroslav Kotik, adding that 15 Czech crowns ($0.60) of the retail price of the figurines would be donated to the effort.
 Georgia will lock down four big cities, including its capital Tbilisi, for 10 days from April 15 in an effort to prevent the spread of the coronavirus, Prime Minister Giorgi Gakharia said.
 Gakharia said the emergency situation in the country would be extended until May 10.
 New York Governor Andrew Cuomo took to morning TV shows to push back against President Donald Trump's claim of "total" authority to reopen the nation's virus-stalled economy, noting that a president is not an absolute monarch.
 "We don’t have a king," Cuomo said on NBC's Today.
 "We have a president. That was a big decision. We ran away from having a king, and George Washington was president, not King Washington. So, the president doesn’t have total authority."
 The Democratic governor, whose state has become the epicenter of the coronavirus pandemic in the United States, was reacting to  Trump's assertion Monday that "when somebody is president of the United States, the authority is total."
 Confirmed coronavirus cases in the Netherlands rose by 868 to 27,419, health authorities said, with 122 new deaths.
 Total deaths in the country are at 2,945, the Netherlands Institute for Public Health said in its daily update.
 The leader of the left-wing Irish nationalist party Sinn Fein, Mary Lou McDonald, said that a test taken at the end of March confirmed she had the coronavirus, but she was no longer infected and would return to work next week.
 McDonald said she was given the result on Monday after "weeks of being very unwell". The test was taken on March 28.
 McDonald's party stunned Ireland's establishment in February by winning the most votes in an inconclusive national election.
 US chain McDonald's has apologised after a sign telling black people they were banned from entering a branch in southern China prompted outrage online, following reports of discriminatory treatment towards Africans in the city.
 Tensions have flared between police and Africans in the southern metropolis of Guangzhou after local officials announced a cluster of COVID-19 cases in a neighbourhood with a large migrant population.
 As the row escalated, posts widely shared online showed a sign at fast food chain McDonald's saying black people were not allowed to enter the restaurant.
 Strong alcoholic drinks can be used "when absolutely necessary" instead of hand sanitiser in Japanese hospitals, authorities said, as supplies run dry as a result of the coronavirus pandemic.
 Spirits with an alcohol proof of between 70 and 83 percent can be substituted to sterilise hands under new rules set out in a health ministry document obtained by AFP news agency.
 Some vodkas are that strong, but traditional Japanese tipples such as sake and shochu do not make the grade - at a maximum alcohol proof of roughly 22 and 45 percent respectively.
 Guinean President Alpha Conde has decided to make the wearing of masks compulsory in a bid to curb the spread of coronavirus.
 Speaking in a televised address on Monday evening, the president said offenders would be "prevented from moving around" and slapped with a civil disobedience tax of 30,000 Guinean francs ($3.16, 2.8 euros).
 The order takes effect from Saturday.
 Conde called on all companies, ministries and NGOs to provide masks to their employees by Saturday. He also urged that masks be manufactured locally and sold cheaply.
 Battered by grim headlines, horrifying statistics and deep uncertainty over the coronavirus pandemic, many people worldwide are trying to lift their spirits by seeking out positive news stories.
 Sites specialising in upbeat news have seen a surge in recent weeks, and Google searches for "good news" have jumped five-fold since the start of the year.
 The Good News Network, created in the late 1990s, has seen traffic treble in the past month with more than 10 million visitors, according to founder and editor Geri Weis-Corbley.
 Iran said the number of lives lost in the country to the novel coronavirus dropped to double figures for the first time in one month.
 Health ministry spokesman Kianoush Jahanpour said 98 deaths from the COVID-19 disease were recorded in the past 24 hours, bringing the overall toll to 4,683.
 "Unfortunately, we lost 98 of our compatriots infected with the disease ... but after a month of waiting, this is the first day that the death toll has been double figures," he told a televised news conference
 Spain reported 567 deaths from the new coronavirus, a slight increase after a one day decline, bringing the total number of fatalities to 18,056 - officially the third-highest in the world behind the United States and Italy.  
 The number of new infections rose by 1.8 percent to 172,541 cases, according to the health ministry, the smallest increase since the country imposed a nationwide lockdown on March 14 to curb the spread of the virus.
 The number of coronavirus cases in Russia has crossed 20,000, while the death toll is nearing 200, authorities said.
 A total of 2,774 more people tested positive for COVID-19 over the past 24 hours, bringing the overall case count to 21,102, Russia’s coronavirus task force said in a statement.
 The death toll has risen to 170 after 22 more fatalities, while recoveries reached 1,694 as 224 more patients were discharged from hospitals.
 US President Donald Trump has claimed he has "total" authority to decide how and when to reopen the economy after weeks of strict physical distancing guidelines aimed at fighting the novel coronavirus.
 But governors from both parties were quick to push back, noting they have primary responsibility for ensuring public safety in their states and would decide when it was safe to begin a return to normal operations.
 The boss of one of the UK's biggest nursing home operators says the number of reported coronavirus deaths among elderly residents is much higher than has been officially reported.
 The government says outbreaks of COVID-19 have been reported in one in eight UK care homes. But David Behan, chairman of home operator HC-One, said cases of the new coronavirus had been reported in 232 of the firm's homes - two-thirds of the total.
 He says 311 residents have died with confirmed or suspected COVID-19.
 The chief minister of India's Kerala state has urged Prime Minister Narendra Modi to repatriate Indian workers in the United Arab Emirates (UAE) amid the coronavirus pandemic, expressing alarm over the Gulf country's response to the health emergency.
 In a letter sent on April 9, Pinarayi Vijayan revealed that numerous complaints were being received over "inadequate isolation and quarantine facilities" in the UAE, which is currently hosting more than three million migrant workers from India, according to the United Nations.
 Taiwan reported no new cases of the coronavirus for the first time in more than a month, in the latest sign that the island's early and effective prevention methods have paid off.
 Taiwan has won plaudits from health experts for how it has fought the virus, including starting as early as December 31 with checks on passengers arriving from China's Wuhan city, where the first cases were reported late last year.
 Taiwan has reported 393 cases to date and six deaths. A total of 338 were so-called imported cases, where people were suspected of becoming infected overseas before entering Taiwan, and the rest were cases of local transmissions.
 Andrew Pierce, a journalist for the Daily Mail tabloid, tweeted on Sunday: "If families gather for holy month of Ramadan there will be a huge spike in Covid cases. Doctors are very worried."
 AstraZeneca Plc said that it would start a clinical trial to assess the potential of Calquence in the treatment of the exaggerated immune response associated with COVID-19 infection in severely ill patients.
 Calquence is a BTK inhibitor and currently used to treat certain types of blood cancers.
 The drug has been approved for the treatment of adult patients with chronic lymphocytic leukaemia in the United States and a number of other countries.
 Turkey's parliament has approved legislation that will free some 90,000 prisoners to ease overcrowding in prisons amid the coronavirus pandemic.
 However, that does not include journalists and activists, who will remain behind bars.
 The legislation, approved early on Tuesday, reduces some sentences and places 45,000 convicts, currently serving terms in open prisons, under temporary house arrest.
 Malaysian police have arrested 62 migrant workers from Myanmar for breaching lockdown restrictions by allegedly staging a weekend street party marking Thingyan, the Buddhist New Year.
 "Some admitted to attending the event, and more arrests are being carried out to track the other suspects involved. Two mobile phones used by the suspects have also been seized," said Wan Kamarul Azran, assistant police commissioner of Sepang, Kuala Lumpur.
 The Thingyan festival is usually a week of concerts, street parties and water fights across much of Myanmar - as well as among the large communities of migrants from Myanmar in neighbouring Thailand and Malaysia.
 Heathrow Airport, traditionally the busiest in Europe, forecast that passenger demand would plunge by over 90 percent in April, as coronavirus restrictions stop most travel.
 Heathrow said that its passenger numbers were down 52 percent in March compared with the same period last year, with many of those being Britons returning home from abroad.
 The airport, owned by a group of investors including Spain's Ferrovial, the Qatar Investment Authority and China Investment Corp, said it was now only using one of its two runways, as flights continue for cargo.
 Hello, this is Usaid Siddiqui in Doha taking over from my colleague Kate Mayberry.
 I will shortly be handing the blog over to my colleagues in Doha. Keep checking in to follow our continuing coverage of the pandemic.
 Authorities in China's northeastern province of Heilongjiang have established a hotline to reward citizens for reporting people crossing the border illegally, after a jump in the number of coronavirus cases imported from Russia.
 According to a notice, people supplying verified information about illegal cross-border crimes will get 3,000 yuan ($426). Those who apprehend any illegal migrants and hand them over to authorities will get 5,000 yuan ($710).
 Russia has become China's largest source of imported cases, with 409 cases found to have originated there. State-owned Global Times said in an editorial Chinese citizens should stay put in Russia and not try to return home.
 Heilongjiang had 79 new cases of imported coronavirus on Monday. All were Chinese citizens travelling back from Russia, state media said on Tuesday.
 Leaders of the 10 members of the Association of Southeast Asian Nations (ASEAN) met online on Tuesday to discuss their countries' response to the coronavirus, including discussions on a regional stockpile of medical equipment for emergencies, and the creation of a regional fund to fight the pandemic.
 "It is in these grim hours that the solidarity of the ASEAN community shines like a beacon in the dark," Vietnamese Prime Minister Nguyen Xuan Phuc said in an opening speech.
 Containment efforts had placed the pandemic "actually under control," he said, warning against complacency.
 Further talks - with China, Japan and South Korea - will take place later on Tuesday.
 Confirmed new cases continue to slow in Germany, where the Robert Koch Institute (RKI) has just announced 2,082 more cases, bringing the total to 125,098.
 This is the fourth day of decline after four days of increases in new cases.
 A further 170 people died from the virus, RKI said on Tuesday.
 India's Prime Minister Narendra Modi has just announced the country's nationwide lockdown will be extended until May 3 to battle the coronavirus.
 Australia's Prime Minister Scott Morrison has said the country remains "many weeks away" from lifting any restrictions despite the sharp slowdown in coronavirus cases.
 "Patience has got to be our virtue here," Morrison said.
 Australia confirmed 63 new infections on Sunday and Monday - the lowest two-day increase in a month - bringing its total cases to 6,366.
 Cases in New Zealand are also falling, with only eight new cases on Tuesday - the lowest in more than three weeks.
 "Our goal has to be go early and go hard, so that we get into a position where we can ease up restrictions with confidence," New Zealand Prime Minister Jacinda Ardern, adding that no action would be taken for at least another week.
 Both countries have closed their borders to foreigners and imposed 14-day quarantines on returning residents.
 New Zealand has enforced a strict stay-at-home lockdown while Australia imposed tight restrictions on movement, gatherings and public activities.
 The Philippines has locked down its main island of Luzon, including the sprawling capital, Manila to try and curb a coronavirus outbreak that is now the largest in Southeast Asia.
 But for the country's poor, the lockdown has left them without food or an income and at risk of detention if they venture out to find either. You can read more in this story by Ana Santos.
 A group of Southeast Asian members of parliament is calling on regional governments to do more to help migrant workers and refugees who have not only lost their jobs as a result of the coronavirus but are also at greater risk of exposure and illness.
 "Because of their living conditions, work situation or limited access to healthcare they are at high risk of both catching the virus and falling into extreme poverty," said Charles Santiago, a Malaysian MP and chair of ASEAN Parliamentarians for Human Rights. "The recent images of migrant workers rushing to get home have only highlighted how the measures taken by one country deeply affect another."
 We have reported on the situation for the thousands of people from Myanmar who live and work in Thailand, the strict quarantines of migrant worker dormitories in Singapore and the fears facing refugees and asylum seekers in Malaysia, which is not a signatory to the UN Convention.
 South Korea's Korea Centers for Disease Control and Prevention says it confirmed 27 new cases of the coronavirus, the 13th successive day with less than 100 new cases.
 South Korean Prime Minister Chung Sye-kyun during an anti-virus meeting on Tuesday called officials to provide stronger support for scientists' efforts to develop vaccines and treatments for the virus, which he said would be a boon for the country's biomedical industry.
 // <![CDATA[
 // ]]]] >
 Johns Hopkins University has revised downwards the number of confirmed cases of coronavirus globally. It is now 1.918 million. Earlier, it said more than two million people around the world had been diagnosed with the virus.
 China has approved early-stage human tests for two experimental vaccines to combat the new coronavirus, state media outlet Xinhua reported on Tuesday.
 The vaccines are being developed by a Beijing-based unit of Nasdaq-listed Sinovac Biotech, and by the Wuhan Institute of Biological Products, an affiliate of state-owned China National Pharmaceutical Group.
 In March, China gave the green light for another clinical trial for a coronavirus vaccine candidate developed by China's Academy of Military Medical Sciences and Hong Kong-listed biotech firm CanSino Bio.
 China has confirmed 89 new cases on April 13, compared with 108 the previous day.
 The National Health Commission says 86 cases were imported - many of those cases are coming through the northeast province of Heilongjiang, which borders Russia.
 The number of total confirmed cases in China since the beginning of the outbreak now stands at 82,249 and the death toll is 3,341, with no new reported deaths on April 13. All arrivals to China from overseas must go through a 14-day centralised quarantine.
 The World Health Organization (WHO) will release its latest strategic advice on tackling the pandemic later on Tuesday.
 It will include guidance for governments considering lifting restrictions imposed to curb the spread of the virus.
 In a media conference on Monday evening, Director-General Tedros Adhanom Ghebreyesus said six criteria needed to be considered:
 While #COVID19 accelerates very fast, it decelerates much more slowly. This is especially concerning for countries with large poor populations, where #StayAtHome orders & other restrictions used in some high-income countries may not be practical.
 In his daily coronavirus briefing, US President Donald Trump played a campaign-style video defending his response to the pandemic amid criticism of the White House's handling of the crisis.
 The video was put together by a White House team.
 Top US health expert, Dr Anthony Fauci, says Trump did listen to his advice when he recommended that mitigation efforts be taken to stop the spread of the coronavirus.
 Trump retweeted a call to fire Fauci after that interview, but the White House said on Monday Trump did not intend to sack him.
 I'm Kate Mayberry in Kuala Lumpur.
 You can read all the updates from yesterday (April 13) here.
 Provincial authorities will, however, be free to extend their restrictions on these industries if they believe it necessary, Khan said .  In recent days, there have been differences between  Khan's  federal  government and the provincial government in Sindh province, home to the country's largest city, over how strict the lockdown should be.
 Portuguese companies suffer massive blow from coronavirus outbreakYou can now download all free assets in one place. This includes Watermark, Keyframe, Screener, XML, and Shotlist.LISBON, April 14 (Reuters) - Around 80% of Portuguese companies still operating or temporarily shut due to the coronavirus outbreak reported a sharp drop in their revenues, at times exceeding 75%, as authorities tightened lockdowns, a survey showed on Tuesday.Around 78% of businesses blamed the revenue free-fall on restrictions implemented by Portugal's Socialist government as part of a state of emergency to contain the spread of the coronavirus. The emergency was initially declared on March 18 but is set to be extended until May 1.The accommodation and restaurant sector suffered the hardest blows, a survey released by the National Institute of Statistics (INE) and the Bank of Portugal revealed, with businesses in the industry frequently reporting revenue declines of more than 75% during the second week of April.
             Al Jazeera News
 	 
 https://www.aljazeera.com/news/2020/04/million-confirmed-coronavirus-live-updates-200413235036857.html
