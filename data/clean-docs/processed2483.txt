The coronavirus issue | MIT Technology Review
 Navigating a world reshaped by covid-19
 Purchase or login to download the PDF
 How to safely ease social distancing while we wait for a covid-19 drug or vaccine.
 This is a chance to reinvent the way we collect and share personal data while protecting individual privacy.
 Coronavirus was a test, and many of the world’s most advanced nations have all too visibly failed. What can we do better?
 What it is, where it comes from, how it hurts us, and how we fight it.
 Scientists are scrambling to find powerful antibodies that can turn back the disease. But can they manufacture enough for everyone?
 The inside story of how one Indian state is flattening the curve through epic levels of contact tracing and social assistance.
 When the pandemic wanes, a poorer, more divided world will still face the rapidly rising threat of global warming.
 The US isn't ready to hold a vote during a pandemic. It needs to get there, quickly.
 Can being ready for one kind of disaster prepare you for another?
 Contrary to what you’ve heard, shutting down the country is also the quickest way to get it started back up again
 My experiences hiding from pirates on the Indian Ocean helped when the loneliness of coronavirus self-isolation kicked in. But there are still things I miss.
 The race to develop tests that will tell us how widespread the virus is.
 Once enough people get Covid-19, it will stop spreading on its own. But the costs will be devastating.
 Waiting for enough people to catch the coronavirus could take a very long time.
 Grassroots groups of researchers are taking matters into their own hands. But volunteering doesn’t always go smoothly.
 And they could even help with the next one.
 An interview with Christopher Kirchhoff, who wrote a post-mortem of the US Ebola response for the National Security Council.
 Regulatory changes and anxiety heightened by isolation are leading to a boom in use of mental health apps and teletherapy—but are they good enough?
 Technology historian Mar Hicks reflects on how catastrophes often reveal long-running structural inequalities and force those in power to fix them.
 A viral disease has a way of reminding us that technology can’t help us if it’s not paired with human cooperation.
 The mission of MIT Technology Review is to bring about better-informed and more conscious decisions about technology through authoritative, influential, and trustworthy journalism.
 https://www.technologyreview.com/magazines/the-coronavirus-issue/
