Coronavirus in India LIVE updates: PM Modi announces 21-day nationwide lockdown
 Coronavirus update: Prime Minister Narendra Modi on Tuesday announced nationwide lockdown in the wake of increasing cases of novel corona till 21 days i.e., three weeks. The nationwide lockdown will be in effect from midnight to save every Indian, he said. The 21-day lockdown period is to prevent the further spread of COVID-19. Earlier today, the central government on Tuesday prohibited the export of ventilators, sanitisers, and other ICU equipment with immediate effect in the wake of increasing novel coronavirus cases in India. According to a statement released by the Ministry of Health and Family Welfare, total number of positive coronavirus cases has risen to 519 (including 39 discharged people and 9 deaths).
 Out of these six cases, five are from Mumbai and one is from Nagar. The states of Bengal, Himachal Pradesh, UP, Odisha, Sikkim have entered their lockdown phase as the country battles COVID-19 pandemic. The central government had annonced a complete lockdown in 75 districts across the country on Sunday. Meanwhile, COVID-19 cases in the country has increased to 482, as stated by ICMR at 10am on Tuesday. As cases increase, Prime Minister Narendra Modi has announced that he will address the nation at 8pm.This is the second address to the nation in less than a week by the Prime Minister. He will speak about coronavirus, the measures taken and things that are important to keep in mind amid the outbreak. To make matters easier for coronavirus patients, the government has announced that it will now include COVID-19 treatment under the Ayushman Bharat PMJAY scheme. The first case in Northeast has also been reported. The patient, a citizen of Manipur had returned from the UK. Back in Wuhan that is the epicentre of the entire coronavirus pandemic, the authorities have lifted outbound travel restrictions on its citizens. Wuhan will allow outbound travel from April 8, while the other cities of Hubei province will allow it from March 25.
 Also: PM Modi's address to nation on coronavirus; where and when to watch live stream
 Follow the latest developments on the pandemic on the coronavirus live updates here:
 10.45 pm: The right thing to do is for every citizen to support the lockdown whatever may be the hardships; I am certain the PM understands the urgency of announcing a financial package, tweets former Finance Minister P Chidambaram.
 Listened carefully to Prime Minister’s address. Left me with a mix of emotions — relief, vindication, frustration, disappointment, worry, fear etc.— P. Chidambaram (@PChidambaram_IN) March 24, 2020
 Listened carefully to Prime Minister’s address. Left me with a mix of emotions — relief, vindication, frustration, disappointment, worry, fear etc.
 The lockdown is late, but better late than never. Those who mocked the idea of a lockdown will do the country a great favour by remaining silent for 21 days.— P. Chidambaram (@PChidambaram_IN) March 24, 2020
 The lockdown is late, but better late than never. Those who mocked the idea of a lockdown will do the country a great favour by remaining silent for 21 days.
 PM’s announcement left a gaping hole. Who is going to provide cash to the poor that they need for the next 21 days?— P. Chidambaram (@PChidambaram_IN) March 24, 2020
 PM’s announcement left a gaping hole. Who is going to provide cash to the poor that they need for the next 21 days?
 Why does it take 4 days and more to work out the promised financial package? We have enough talent to finalise the package in 4 hours.— P. Chidambaram (@PChidambaram_IN) March 24, 2020
 Why does it take 4 days and more to work out the promised financial package? We have enough talent to finalise the package in 4 hours.
 What is the meaning of Rs 15,000 crore announced by PM? Let me repeat — government needs to find Rs 5 lakh crore over the next 4-6 months to manage the economic consequences.— P. Chidambaram (@PChidambaram_IN) March 24, 2020
 What is the meaning of Rs 15,000 crore announced by PM? Let me repeat — government needs to find Rs 5 lakh crore over the next 4-6 months to manage the economic consequences.
 The right thing to do is for every citizen to support the lockdown whatever may be the hardships.— P. Chidambaram (@PChidambaram_IN) March 24, 2020
 The right thing to do is for every citizen to support the lockdown whatever may be the hardships.
 I am certain the PM understands the urgency of announcing a financial package and putting cash in the pockets of the poor, daily workers, agricultural workers, self-employed etc.— P. Chidambaram (@PChidambaram_IN) March 24, 2020
 I am certain the PM understands the urgency of announcing a financial package and putting cash in the pockets of the poor, daily workers, agricultural workers, self-employed etc.
 Once the package is announced, there are other pressing sectoral problems that have to be addressed. For example, how are the farmers going to harvest their crop beginning April 1?— P. Chidambaram (@PChidambaram_IN) March 24, 2020
 Once the package is announced, there are other pressing sectoral problems that have to be addressed. For example, how are the farmers going to harvest their crop beginning April 1?
 9.19 pm: Ministry of Home Affairs guidelines for offences and penalties during the 21-day complete lockdown in India.
 9.13 pm: Ministry of Home Affairs releases guidelines, list of essentials ahead of 21-day lockdown
 9.12 pm: Ministry of Home Affairs issues order for 21-day lockdown.
 9.00 pm: Coronavirus latest news: Six more tested positive in Punjab
 Six more people tested positive for COVID-19 on Punjab. With this the total number of confirmed cases to 29 in the state, according to the officials.
 8.54 pm: Coronavirus India: Cabinet Secretary takes high-level meeting to impose nation-wide lockdown
 Cabinet secretary Rajiv Gauba on Tuesday convened a high-level meeting with top DG and Chief Secretaries of states and UTs to ensure nation-wide lockdown in the country. They will also discuss several issues such as essential supply, medical stores etc.
 8.53 pm: Here are few other highlights from PM Modi's COVID-19 address
 8.43 pm: Don't believe rumours and superstitions, says PM Modi
 Don't believe in rumours and superstitions, don't take any medicines without consulting doctors, says PM Modi.
 8.42 pm: PM Modi tells India to think of corona warriors
 PM Modi in his address to the nation on Tuesday asked every Indian to think of the welfare of several essential service staff at the frontlines of the fight against COVID-19. . "Think of those doctors, those nurses, the para-medical staff, pathologists who are working day and night in order to save lives. Pray for those who are working nonstop to keep your neighbourhoods and the society sanitized. Think of the mediapersons who are working in life-threatening situations to bring you accurate news 24/7," he said.
 8.35 pm: PM Modi announces Rs 15,000 crore stimulus to fight against COVID-19
 Prime Minister Narendra Modi announced on Tuesday that the central government has approved Rs 15,000 crore-stimulus to strengthen the health infrastructure of India. "To strengthen the health infrastructure of the country, the central government has made a provision of Rs 15,000 crores today," he said. "This includes testing facilities related to coronavirus, personal protective equipment, isolation beds, ICU beds and ventilators. The number of other necessary equipment will be increased rapidly," he added.
 8.31 pm: India at a decisive stage in fight against coronavirus, says PM Modi
 "India today is at the stage where our actions today will decide how much we can reduce the impact of this major disaster. This time is to strengthen our resolve again and again," said PM Modi.
 8.27 pm: Pm Modi explains the need for a national lockdown
 "It took 67 days for the first lakh people to be infected from coronavirus. It only took 11 days for the next 1 lakh to be infected. Even more scary is the fact that it took only four days for the figure to rise to 3 lakhs," said PM Modi.
 8.26 pm: An infected person may not always show symptoms: PM Modi
 "You have to remember that many times a person infected with coronavirus may seem to be healthy in the beginning. It is not known if he is infected or not. So take precaution and stay in your homes," said PM Modi.
 8.24 pm: Coronavirus news in India: Please stay at home, appeals PM Modi
 "Just do one thing, stay at home! Today's decision of countrywide lockdown has drawn a Lakshman Rekha around the door of your house," said PM Modi on national lockdown.
 8.22 pm: Coronavirus in India: Lockdown will have economic cost but will save lives, says PM Modi
 "Certainly, the country will have to bear the economic cost of this lockdown. But every government's priority is to save the life of every Indian at this time," PM Modi said.
 8.20: Every state, district, village in India under lockdown from midnight, says PM Modi.
 8.15 pm:  If next 21 days are not controlled, nation will go 21 years behind: PM Modi
 8.11 pm: PM Modi announces complete lockdown in the nation
 Prime Minister Narendra Modi on Tuesday announced nationwide lockdown in the wake of increasing cases of novel corona till 21 days i.e., three weeks. The nationwide lockdown will be in effect from midnight to save every Indian, he said. The 21-day lockdown period is to prevent the further spread of COVID-19.
 8.05 pm: PM Modi thanks citizens for Janta Curfew success
 Prime Minister Narendra Modi on Tuesday thanked every citizen who made the March 22 Janta Curfew successful. "People of India proved that we can unite when there's a calamity. you all deserve appreciation for the success of Janta Curfew," PM Modi said during his 8 pm address.
 8.00 pm: PM Modi starts his address to the nation on COVID-19
 7:55 pm: PM Modi to address the nation shortly on coronavirus 
 Prime Minister Narendra Modi will address the nation shortly on issues relating to coronavirus.
 7:40 pm: Eight new coronavirus cases confirmed in Karnataka, tally goes up to 41
 Karnataka health department on Tuesday said that eight new confirmed cases of coronavirus has been detected in Karnataka, which took tally to 41. Karnataka CM B S Yediyurappa has warned that stringent action will be taken against those who do not follow law and come out onto streets unnecessarily in violation of steps enforced by government to control COVID-19 spread.
 7:30 pm: 232 FIRs registered, 111 people arrested for violating curfew in Punjab 
 As many as 232 FIRs have been registered and 111 people have been arrested for violating the curfew imposed in Punjab to contain coronavirus outbreak, Panjab government reportedly said.
 7:25 pm: Total coronavirus positive cases spike to 519
 According to a statement released by the Ministry of Health and Family Welfare, total number of positive coronavirus cases has risen to 519 (including 39 discharged people and 9 deaths).
 7:20 pm: Indian Railways to pay all contractual, outsourced staff 
 7:10 pm: 'Don't travel unnecessarily', urges Maharashtra CM Thackeray
 Maharashtra Chief Minister Uddhav Thackeray on Tuesday sought peoples' cooperation to contain the spread of coronavirus in the State. "Do not travel unnecessarily. Don't strain our resources. If possible don't step out. We will come out of this successfully, we need your co-operation," says Thackeray.
 He also asked Police to be gentle with people, who are moving out for essentials. "If people are moving out for essentials, be gentle with them and ask them not to move out without any reason. I am asking police that we are not stopping people from living, just changing the lifestyle a bit," he added. 
 7:00 pm: 2020 Tokyo Olympics postponed till 2021 due to coronavirus outbreak
 Japan's Prime Minister Shinzo Abe and International Olympic Committee (IOC) president Thomas Bach on Tuesday agreed to postpone the 2020 Tokyo Olympics until 2021, amid the novel coronavirus outbreak. 
 The unprecedented and unpredictable spread of the outbreak has seen the situation in the rest of the world deteriorating. Yesterday, the Director General of the World Health Organization (WHO), Tedros Adhanom Ghebreyesus, said that the COVID-19 pandemic is "accelerating". There are more than 375,000 cases now recorded worldwide and in nearly every country, and their number is growing by the hour.
 In the present circumstances and based on the information provided by the WHO today, the IOC President and the Prime Minister of Japan have concluded that the Games of the XXXII Olympiad in Tokyo must be rescheduled to a date beyond 2020 but not later than summer 2021, to safeguard the health of the athletes, everybody involved in the Olympic Games and the international community.
 6.45 pm: Coronavirus news in India: AIIMS Doctors write to Amit Shah over being ostracised by neighbours
 The Resident Doctors Association (RDA) of All India Institute of Medical Sciences (AIIMS) has written to Home Minister Amit Shah requesting action against the eviction of healthcare professionals from their homes and provision of transport facility as well.
 6.34 pm: Coronavirus in India: Union Minister Prahlad Joshi donates 1 month salary to PM's National Relief Fund
 Union Minister Prahlad Joshi said on Tuesday that he has donated his one month salary to Prime Minister's National Relief Fund to "strengthen the fight against COVID-19". "I have decided to donate my 1 month salary to the Prime Minister's National Relief Fund to strengthen the fight against #COVID19. I appeal to people serving in coal and mining sectors to come forward and support our nation in combating this pandemic by donating generously," he tweeted.
 I have decided to donate my 1 month salary to the Prime Minister's National Relief Fund to strengthen the fight against #COVID19. I appeal to people serving in coal and mining sectors to come forward and support our nation in combating this pandemic by donating generously.
 6.20 pm: Coronavirus Noida news: No entry to all parks till April 15
 The authorities have banned the entry to all parks in Gautam Buddha Nagar beginning March 24 till April 15. Only the concerned workers will be allowed to enter.
 6.13 pm: Coronavirus update in India: States should impose full lockdown; partial lockdown won't work, says Health Ministry
 The Health Ministry said in a press conference on Tuesday, that partial shutdown won't work and that the states should implement full lockdown in preventing the further spread of novel coronavirus pandemic.
 6.02 pm: Coronavirus Delhi news: Construction workers to get Rs 5,000, announces CM Kejriwal
 Delhi Chief Minister Arvind Kejriwal announced on Tuesday that the construction workers will be given Rs 5,000 by the Delhi government as their livelihood has been impacted by the COVID-19 outbreak.
 5.59 pm: Coronavirus in India: West Delhi police gives details of lockdown violations 
 The West Delhi Police on Tuesday said that it has made a total of 77 arrests/FIRs, detained 674 and seized 66 vehicles for violating lockdown imposed by the central government in the wake of novel coronavirus pandemic.
 5.53 pm: Have the capacity to carry out nearly 12,000 lab tests, says ICMR chief
 "Today we have the capacity to conduct around 12,000 lab tests. For the last few days, on average, we are conducting 1,200-1,300 lab tests on a daily basis,"The Indian Council of Medical Research (ICMR) chief Balram Bhargava said on Tuesday.
 5.46 pm: Coronavirus Delhi news: CM Kejriwal urges landlords to give 2-3 months' concession to tenants who cant pay rent
 Delhi Chief Minister Arvind Kejriwal on Tuesday appealed to the landlords to give 2-3 months' concession to tenants who are not in condition to pay rent.
 5.35 pm: Govt bans export of ventilators, sanitisers, other ICU equipment
 The central government on Tuesday prohibited the export of ventilators, sanitisers, and other ICU equipment with immediate effect in the wake of increasing novel coronavirus cases in India.
 5.26 pm: Coronavirus update in India: Mumbai Police finds unique way to keep people indoors
 5.23 pm: Coronavirus update in India: Maharashtra, Telangana legislative council bypolls postponed
 The legislative council bypolls in Maharashtra and Telangana have been postponed in the wake of increasing cases of COVID-19 in the states.
 5.14 pm: Coronavirus news in India: Rajya Sabha secretariat to remain closed till March 27
 The Rajya Sabha secretariat will remain closed till March 27 in the wake of COVID-19 pandemic.
 5.10 pm: Coronavirus update in India: Rahul Gandhi urges people to united against COVID-19
 Congress leader Rahul Gandhi on Tuesday wrote a letter in which he appealed to people to stand united against the novel coronavirus pandemic.
 5.03 pm: Coronavirus J&K news: Govt seals borders
 The National Highway connecting J&K with Punjab has been sealed permanently at Lakhanpur. People entering the union territory from this highway will be quarantined in Kathua.
 4.55 pm: Coronavirus cases in India: External Affairs Minister S Jaishankar discusses COVID-19 situation with his Chinese counterpart
 External Affairs Minister, S Jaishankar on Tuesday held discussions with his Chinese counterpart Wang Yi as he reviewed the current situation. S Jaishankar said, "Global challenges require global cooperation".
 Discussed with State Councilor and FM Wang Yi of #China our working together in combating #COVID19. Agreed to build further on our bilateral efforts in this domain. Exchanged views on the forthcoming #G20 Summit. 
 Global challenges require global cooperation.
 4.46 pm: Coronavirus in India: Total COVID-19 positive cases reach 29 in Punjab
 The total number of novel coronavirus positive cases have reached 29 in the state of Punjab, informed Health Minister Balbir Singh Siddhu on Tuesday. The state has sought an additional Rs 150 million from the central government to combat the novel coronavirus pandemic.
 4.35 pm: Coronavirus Maharashtra news: Six new COVID-19 positive cases confirmed; total tally at 107
 The number of novel coronavirus positive cases in Maharashtra have climbed to 107 after six fresh cases were confirmed in the state. Out of these six cases, five are from Mumbai and one is from Nagar.
 4.30 pm: COVID-19 lockdown Day 2: Cops ensure people follow rules
 4.15 pm: Coronavirus updates: Centre asks state govts to deploy fiscal resources for medical facilities
 The Central government has asked all state government to deploy fiscal resources for creating additional medical facilities such as hospitals, clinical labs, isolation wards etc to combat novel coronavirus pandemic.
 Central government asks all State Governments to deploy fiscal resources for establishing additional medical facilities - hospitals, clinical labs, isolation wards, expanding & upgrading existing facilities - to combat #COVID19. @PMOIndia@HMOIndia
 4.00 pm: Lockdown in Karnataka: Indira canteens to be closed
 The Karnataka government has decided to close the Indira canteens as a precautionary step to prevent the further spread of novel coronavirus. The state government runs Indira Canteen which is a food subsidisation programme.
 3.45 pm: Coronavirus Himachal Pradesh news: State govt imposes state-wide curfew
 The Himachal Pradesh government on Tuesday imposed a state-wide curfew in the wake of COVID-19 pandemic. One patient had died in the state on Monday.
 3.30 pm: Waiver on minimum balance requirement fee will remain for three months, says the FM.
 3.14 pm: PM himself monitoring the situation; sub-groups of industrialists, MPs under economic task force are already giving their inputs; economic package will be announced soon, says the FM.
 3.11 pm: With respect to commerce activities, for procedural and eligibility point of view, extension of timeline will be given, but there shall be not an extension or modification for scheme guidelines, says the FM.
 3.10 pm: We will reduce bank charges for digital trade transactions for all trade finance consumers, says the FM.
 3.10 pm: There shall not be any minimum balance requirement fee, says the FM.
 3.10 pm: Debit card holders to withdraw cash from any bank ATM for free for next three months, says FM.
 3.07 pm: Verification of documents and grant of NOC for quarantine will be relaxed from 7 days to 3 days, says the FM.
 3.07 pm: Rebooking of quarantine cubicles for cancelled consignments in aquatic quarantine facilities with additional booking charges will be allowed, says the FM
 3.06 pm: Delay up to 1 month in arrival of consignments will be condoned, says the FM.
 3.05 pm: Sanitary import permits for shrimp brood stock and any other aquaculture which were to expire betweeen March 1 and April 15, are all now being extended by 3 months, says the FM.
 3.00 pm: Threshold for insolvency under IBC raised from Rs 1 lakh to RS 1 crore to protect MSMEs, says the FM. If the situation continues beyond April 30 sections 7, 9 and 10 of IBC for six months to prevent companies from being forced into insolvency, says the FM.
 2.58 pm: Deadline for investing 15 per cent of debentures maturing during a particular year into specificied instruments extended to June 30.
 2.56 pm: Directors who do not comply with the minimum residency requirement will not be hold in violation, says the FM.
 2.55 pm: Newly incorporated companies given additional 6 months for declaration of commencement of business.
 2.48 pm: For 2019-20, if independent directors have not been able to hold a single meeting, it will not be seen as a violation, says the Finance Minister.
 2.48 pm: Requirement of holding board meetings has been relaxed for 60 days for two quarters, says the FM.
 2.47 pm: Moratorium on MA-21 registry from April 1, 2020, says the FM.
 2.45 pm: Customs clearance will operate 24X7 up to June 30 as an essential service, said FM Sitharaman. 
 2.38 pm: Last date to apply for compensation scheme has been extended to June 30, 2020.
 2.38 pm: Companies with less than Rs 5 crore turnover, no interest, late fee or penalty will be charged, says the FM. Bigger companies will only have to pay interest at 9 per cent, she added.
 2.37 pm: Last date for filing March, April and May GST returns and compensation returns has been extended to June 30, 2020, says FM Sitharaman.
 2.31 pm: Due dates for issue of notice, intimation, notification, approval order, sanction order, filing of appeal, furnishing of returns, return statements, applications, reports, and any other documents and any compliance by the taxpayers, including investment and savings instruments for rollover benefit of capital gains under Income-tax Act, Wealth Tax Act, Prohibition of Benami Property Transaction Act, Black Money Act, STT law, CTT law, Vivad Se Vishwas law, and others, where the time limit was expiring on March 20, 2020, shall now be extended to June 30, 2020, said the FM.
 2:30 pm: Vivad se Vishwas has been extended to June 30, no 10 per cent addtional charge at all, says FM Sitharaman. 
 2.30pm: Aadhaar-PAN linking extended to June 30, asys FM Nirmala Sitharaman
 2.30 pm: No extension, but on delayed deposit of TDS only reduced interest of 9 per cent will be charged till June 30, said the FM.
 2.26 pm: For delayed payments made till June 30, interest rate reduced from 12 to 9 per cent, said the FM.
 2:26 pm: For financial year 2018-19, the last date for income tax returns will be June 30, 2020, announced Finance Minister Nirmala Sitharaman while addressing the media via video conferencing.
 2.26 pm: Very close to coming out with economic package to deal with coronavirus crisis, says FM Nirmala Sitharaman
 2:05 pm: 12 coronavirus patients in Mumbai get cured
 At Kasturba Hospital in Mumbai, 12 patients have recovered from COVID-19, as per media report. 
 2:00 pm: FM Nirmala Sitharaman to hold press conference shortly
 Finance Minister Nirmala Sitharaman will hold a press conference in Delhi on Tuesday. In this press conference, she may announce conomic measures to overcome Covid-19 impact.
 2:00 pm: Indian Railways to manufacture protective gear
 In a bid to control the spread of novel coronavirus infection, Indian Railways is planning to develop personal protective equipment (PPE) on a large scale after procuring raw materials. It is also mulling to manufacture ventilators and train its medical staff to add to overall capability in order to fight the COVID 19 outbreak, as per an IE report.
 1:50 pm: Govt asks states to transfer funds to construction workers via DBT mode
 Labour Minister Santosh Gangwar has reportedly asked States to transfer funds to construction workers via DBT mode. Over 3.5 crore construction workers are registered with construction welfare boards.
 1:45 pm: Tamil Nadu govt to give one month's extra salary to health department workers
 Health Department workers like doctors, nurses and all those who attend to coronavirus patients will be given one month's extra salary, said Tamil Nadu CM Edappadi K Palaniswami in state assembly on Tuesday.
 1:40 pm: Reliance Ind sets up India's first dedicated COVID 19 hospital in Mumbai
 Mukesh Ambani-led Reliance Industries has set up India's first dedicated COVID 19 hospital in Mumbai. 101 positive COVID 19 cases have been reported in Maharashtra, so far.
 1:38 pm: Prime Minister Narendra Modi interacts with print media heads through video conferencing.
 1:35 pm: Bhopal Memorial Hospital declared as dedicated COVID19 hospital
 Madhya Pradesh: Bhopal Memorial Hospital has been made dedicated COVID19 hospital.
 1:30 pm: Jammu Government Medical College declared as dedicated COVID19 hospital 
 Government Medical College, Jammu has been declared as dedicated COVID19 hospital for Jammu Province, to be readied by 25th March evening, says Principal Secretary Planning Jammu & Kashmir Rohit Kansal.
 1:25 pm: Goa announces complete lockdown from midnight March 24
 Goa Chief Minister Pramod Sawant on Tuesday said there will be complete lockdown in Goa from midnight today till March 31.
 1:15 pm: Two coronavirus patients in Ladakh get cured
 Two patients who tested positive for coronavirus in Ladakh have been completely cured. Following this, total number of positive cases in Ladakh now stands at 11.
 1:00 pm: Actor Rajinikanth donates Rs 50 lakhs to film employees federation of South India union workers
 Actor Rajinikanth has donated Rs 50 lakhs to Film Employees Federation of South India Union workers who are facing shutdown due to coronavirus outbreak. 
 12:55pm: Health Minister says 1.8 lakh people under surveillance
 Union Health Minister Dr Harsh Vardhan said that National Centre for Disease Control  is monitoring potential coronavirus cases. Over 1,87,000 people are under surveillance and around 8,000 are housed in various isolation facilities.
 12:49pm: Finance Minister Nirmala Sitharaman to hold pres conference at 2pm
 FM Nirmala Sitharaman will hold a press conference at 2pm on Tuesday. She will address statutory and regulatory compliance matters  at the press conference. PM Narendra Modi will also address the nation at 8pm tonight.
 Even as we are readying an economic package to help us through the Corona lockdown (on priority, to be announced soon) I will address the media at 2pm today, specifically on statutory and regulatory compliance matters. Via video conference. @FinMinIndia@PIB_India@ANI@PTI_News— Nirmala Sitharaman (@nsitharaman) March 24, 2020
 Even as we are readying an economic package to help us through the Corona lockdown (on priority, to be announced soon) I will address the media at 2pm today, specifically on statutory and regulatory compliance matters. Via video conference. @FinMinIndia@PIB_India@ANI@PTI_News
 12:42pm: Death toll increases to 11
 Another person has succumbed to coronavirus. The death toll has reached 11. The latest victim, a 65-year-old patient from Maharashtra had travel history to UAE.
 12:34pm: J&K to turn hospitals into sanatorium for patients
 Jammu and Kashmir government has decided to turn hospitals into sanatorium for coronavirus patients. SKIMS Medical College, BCD Hospital Srinagar, JLNM Hospital, and Police Hospital, Srinagar have been transformed for isolation purposes.
 12:34pm: Ranchi Police cracks down on lockdown violators
 Ranchi Police has cracked down on people who have violated the lockdown. It has also taken action against shops that were selling products other than essential items. 
 12:30pm: Five people cured in last 24 hours: CM Kejriwal
 Chief Minister Arvind Kejriwal has said that there have been no new cases in Delhi for the last 24 hours and that 5 people have been cured of coronavirus during that time.
 12:20pm: US applauds India for janta curfew
 Bureau of South and Central Asian Affairs (SCA) in the US has applauded India for the janta curfew observed on Sunday. It said it was heartwarming to see people unite despite isolation. 
 Inspiring to see people across #India coming together despite being physically apart to applaud workers on the frontlines combatting #COVID19#JantaCurfew AGW https://t.co/F6OmN4CZEj— State_SCA (@State_SCA) March 23, 2020
 Inspiring to see people across #India coming together despite being physically apart to applaud workers on the frontlines combatting #COVID19#JantaCurfew AGW https://t.co/F6OmN4CZEj
 12:10pm: Visakhapatnam under lockdown
 Andhra Pradesh government has annoucned complete lockdown at Visakhapatnam. The state has reported six COVID-19 cases.
 12:00pm: Sonia Gandhi writes to PM Modi to help migrant workers
 Sonia Gandhi has written to PM Modi seeking help for migrant workers who are bound to lose their jobs amid the stringent lockdown. She said that around 44 million workers face uncertain future and that their livelihoods are at stake.
 11:50am: Election Commission of India (ECI) has deferred the Rajya Sabha Elections.
 11:45am: Govt to include coronavirus in Ayushman Bharat
 Governement will include treatment of coronavirus under the Ayushman Bharat PMJAY scheme. All beneficiaries will be included as per the existing norms.  
 11:40am: Air India aks employees to work from home
 11:30am: Coronavirus cases increase to 482 in India: ICMR
 11:05am: Prime Minister Narendra Modi will address the nation at 8pm today.
 10:50am: Wuhan to lift travel restrictions
 Wuhan, the epicentre of the coronavirus pandemic has decided to lift the travel restrictions it had imposed on the citizens. While restrictions will be lifted by April 8, other cities in Hubei province would lift them by March 25.
 10:46am: Indian armed forces evacuate 1,186 people from foreign countries
 The Indian armed forces have cleared 1,186 people from foreign countries. This also includes medical staff. Around 796 people are under quarantine. There are two positive cases while two have symptoms.  
 10:40am: Two more cases have been reported in Gujarat. Total cases 33.
 10:30am: Locals gather at Shaheen Bagh
 Despite a complete lockdown, locals have gathered at the Shaheen Bagh protest site. The site was cleared by the Delhi Police due to the lockdown. Along with Shaheen Bagh other protest sites such as Hauz Rani, Jafrabad were also cleared. 
 10:15am: The Mumbai Police has ordered specific timings for essential items
 The Mumbai Police Commissioner has ordered the following timings for esential items:
 10:10am: Protest site at Hauz Rani cleared
 Along with Shaheen Bagh, the Delhi Police has also cleared the Hauz Rani protest site. Section 144 has been imposed in Delhi and there is complete lockdown due to coronavirus.
 10:00am: Delhi Metro closed till March 31
 9:50am: All immigration posts closed
 The government has shut down all immigration posts. All 107 immigration check posts including at airports, sea ports, land ports, rail ports and river ports have been shut down by the government. However, vehicles and carriers with essential goods have been exempted from the restriction.
 9:40am: Another 8 cases reported in Maharashtra
 The state government announced that 8 more cases were reported in Maharashtra, taking the toll to 97. One cases each in Kalyan Dombivli, Mumbai, Thane, Khandala and a family of four from Islampur Sangli have been tested positive. They all had travel history outside India.
 9:37am: 30 UTs, states, 548 districts under lockdown
 Adding to the initial list, Indian government has announced lockdown in 548 districts across 30 states and union territories.
 9:33am: Delhi Sikh Gurudwara Committee offers space for quarantine
 The committee lauded CM Kejriwal in his efforts to curb coronavirus. In a statement the committe said that it will offer DSGMC Sarai of Gurudwara Majnu ka Tilla Sahib as quarantine facility, with provision for langar for the poor and needy.
 9:27am: Delhi Police Commissioner appeals to people to stay at home
 9:25am: Kashmiri students protest in Bengaluru due to lockdown
 A group of Kashmiri students protested in Bengaluru airport over cancellation of domestic airlines. The students said that they were supposed to go to Kashmir but were now stranded due to the lockdown.
 9:15am: People come out despite lockdown
 Despite lockdown, people across the nation have been seen coming out to the streets. Joggers were spotted in Nagpur even as the city is under lockdown. On Monday, the Prime Minister also lamented that people were not following restrictions as they should be and urged people to remain indoors. Delhi Chief Minister Arvind Kejriwal also asked people to stay indoors and said that people should not think that they can't contract coronavirus. 
 9:11am: Religious places shut down
 As Delhi undergoes a complete lockdown, religious places have also been shut. Visuals from an empty Hanuman temple in Connaught Place.
 9:10am: Nepal under lockdown from 6am today.
 9:05am: First case in Manipur reported
 Manipur has reported its first coronavirus case. The patient is a 23-year-old from Imphal West. The patient had returned from the UK and is now under treatment in JNIMS.
 9:00am: Security has been tightened at Shaheen Bagh
 As protesters refused to clear the site, the Delhi Police had to use force. Section 144 has been imposed in Delhi that prohibits gatherings of more than four people. Some protesters have been detained as well.
 8:50am: People stock up in Punjab
 As the state announces lockdown, people in Amritsar rush to the grocery shops to stock up on essential goods. Meanwhile, they all feel such a step is required to flatten the coronavirus curve.
 8:45am: Uttarakhand reserves 25% of hospital beds for coronavirus patients
 Uttarakhand has said that it will reserve 25 per cent of the beds in its public and private hospitals for coronavirus patients. This will be applicable for hospitals thata have atleast a 100 beds.
 8:40am: Kolkata Police arrests 225 people for violating lockdown
 Amid strict searches, the Kolkata Police have caught 225 people for violating lockdown on Monday. The Kolkata police said that the violators will be prosecuted under sections dealing with disobedience to order duly promulgated by public servants.
 8:37am: Ladakh samples negative
 All the 16 samples that were sent from Ladakh have been testes negative. Out of the 16 samples sent, 12 were from Kargil and 4 were from Leh.
 8:35am: Shaheen Bagh protest site cleared
 8:30am: First Indian test kits approved
 The Indian FDA/Central Drugs Standard Control Organisation (CDSCO) has given its nod for the first Made in India test kit for coronavirus. The kit was developed by Mylab Discovery Solutions Pvt Ltd in record six weeks. The kit is much-needed as currently, India ranks lowest in terms of testing done per million population.
 https://www.businesstoday.in/latest/trends/coronavirus-in-india-live-updates-9-dead-471-infected-govt-mounts-pressure-to-self-isolate/story/399029.html
