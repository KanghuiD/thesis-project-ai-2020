Vero connections say how New York's affected by coronavirus | Opinion
 “This virus has, for me, been almost like a hurricane with no wind, or a forest fire with no flame.”
 That’s how Shake Shack founder and New York restaurateur Danny Meyer described COVID-19 on “60 Minutes” last week.
 “We've laid off over 2,000 people by now,” said Meyer, whose Union Square Hospitality Group operates more than a dozen restaurants, mostly in New York City, Ground Zero of the novel coronavirus.
 Whitney Sanchez, 33, a graduate of Vero Beach High School and Florida State University, is the company’s director of operational excellence. She headed its coronavirus task force, which began meeting in mid-February.
 Sanchez is among several folks with Indian River County roots braving the coronavirus crisis in the New York, New Jersey and Connecticut area.
 With restaurants that serve tourists, Meyer's company took extra sanitary measures early. They decided to shut down March 9, Sanchez said, more than a week ahead of a city and state order to close.
 It was a day after her family gathered from various parts of the nation at New Jersey's Seton Hall University, her late father's alma mater, to celebrate a special Mass for the former Dodgers broadcaster Joe Sanchez.
 “They got back before everything started to get crazy,” she said.
 Crazy, as in her final subway ride from lower Manhattan to Williamsburg in Brooklyn, a 20-minute ride with relatively few riders.
 “Everyone just kind of stared and looked at each other,” Sanchez said, noting no one touched anything like they normally do.
 A week later Nick Porpiglia, her high school classmate, made his last subway commute from Manhattan to Brooklyn. Ridership had dramatically tailed off.
 “It was getting a little creepy down there,” said Porpiglia, on furlough from his job as food and beverage manager of the $700 and up per night Equinox, a new hotel at Hudson Yards. “Rats were starting to reclaim the platform.”
 While others on the train wore masks, Porpiglia hadn't started wearing one yet. The good news, he said, was there were so few people on board social distancing was easy.
 In Manhattan, Scott Johnson, 59, an architect who owns a home in Vero Beach and lives in a West Village apartment south of Hudson Yards, doesn’t leave his apartment without a mask.
 The bulk of his trips since March 9, when he started staying home, have been walking the dog and shopping at a nearby Japanese grocery that remains open and well-stocked.
 “That’s the biggest stress — just going to the grocery store,” he said. “It’s a different way of life.”
 Marisa Espat, 24, lives about a mile away in SoHo, where there are plenty of clothing stores, but fewer residences. And even if she wanted to go out — as of Thursday she’d left her building only for one jog since hunkering down March 15 — there aren’t lots of groceries around.
 The new normal for Espat, a graduate of Sebastian River High School and New York University, is ordering groceries online in the hope they’ll be delivered within five days, she said.
 Porpiglia has a much better view of the grocery situation from his 43rd-floor apartment near Brooklyn’s Barclay’s Center. He can time the line standing outside a nearby Whole Foods, one of several supermarkets in his neighborhood.
 “It’s the best time I’ve ever had in a Whole Foods, where they only let 10 people in the store and nobody’s in (the checkout) line,” he said.
 Otherwise, though, “you look down on the city; it’s desolate.” he said. Like Johnson, Porpiglia noted that because there’s no construction or traffic, the streets are quiet. “Everything’s on pause.”
 “You can stand in the middle of the road and see things you’ve never seen before,” Johnson said, citing the lack of vehicles and some places where you can see the Hudson and East rivers at the same time. “It’s empty. It’s beautiful.”
 Espat sits on the fire escape outside her apartment for fresh air. She used to see about 100 people on Grand Street; now she might see 10.
 “It’s very strange,” she said, which along with “eerie,” “weird” and “creepy,” were words that came up repetitively in my interviews.
 Among the more unusual things:
 Don't want to miss another column like this? Here's our latest membership deal
 1) A mobile morgue (a large refrigerated truck) is positioned near the former St. Vincent’s Hospital near Johnson’s home, he said.
 The hospital, which operated 1849-2010, was where patients were treated in cholera, typhoid and AIDS epidemics. It also treated people injured on 9/11 and in the Hudson River landing of US Airways Flight 1549.
 2) People stand in line waiting to be tested for COVID-19 outside a tent in a hospital parking lot in Porpiglia’s dense neighborhood.   
 Only Sanchez said she knew someone who died (March 25) in the pandemic, which, as of Thursday, had taken the lives of more than 7,000 New Yorkers. That person, top chef Floyd Cardoz, had partnered with Meyer on multiple ventures. He returned from India via Frankfurt, Germany, on March 8 and felt ill.
 That week, Sanchez worked out of her home, but saw the run on groceries. She left the next weekend for Connecticut, joining a friend who hunkered down with family. Three folks in the home were laid off.
 “It’s just nerve-wracking,” Sanchez said, noting she feels fortunate to have a job and a landlord who waived this month’s rent. “You just don’t know what tomorrow brings.”
 Espat, an associate events producer, said she’s working part time from home. She fills her day with online classes and group get-togethers to feel productive.
 “There are moments when I’m going stir crazy,” she said. “There are moments of worry. How long will it be before people feel comfortable together? There are so many emotions that come and go.”
 Accepting fear and anxiety are normal, she said, but concerning herself only about what she can control is important.
 “We’ve been through things before and we’ve always found a way to find a new normal,” Espat said with the optimism of a New Yorker. “We will find a way to be OK.”
 Just like we have when faced with hurricanes and forest fires. It just might take a while.
 https://www.tcpalm.com/story/opinion/columnists/laurence-reisman/2020/04/13/vero-connections-say-how-new-yorks-affected-coronavirus-opinion/5120835002/