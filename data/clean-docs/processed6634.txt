The UAE is grateful to the doctors and nurses trying to keep everyone alive - The National
 Thursday 30 April 2020
 Abu Dhabi
 UAE
 Thursday Apr 30, 2020
 42°
 H:42 °
 L:31 °
 H:40 °
 L:28 °
 H:41 °
 L:29 °
 H:37 °
 H:33 °
 H:35 °
 L:26 °
 L:24 °
 EMU 
 Thursday Apr 30,2020
 Abu Dhabi, UAEThursday 30 April 2020
 April 29, 2020
 Our readers have their say on the tireless work of the medical fraternity, dolphin sightings, restaurants closing and Boris Johnson back at work
 With reference to Nick Webster's article Coronavirus: inside the Covid-19 critical care ward of Dubai's Prime Hospital and Willy Lowry's video report (April 28): thank you so much to the doctors, nurses, staff and cleaners for all your sacrifices. We will continue to pray for all of you. 
 Mary Ann H Lim, Abu Dhabi
  We are grateful that the level of treatment in the UAE is this thorough. Our appreciation towards the hospital staff who put themselves in a very difficult situation for the sake of the wellness of the patients. 
 Abdul Rahman Mohamad, Dubai 
 One million tests shows how seriously UAE takes coronavirus
 In reference to Juman Jarallah's report Coronavirus: UAE conducts more than one million Covid-19 tests (April 25): this shows how seriously the UAE's leaders are taking the matter of testing. It makes people feel safe. A vast country like India too has so far managed to handle the spread. All administrations must continue to take the pandemic seriously to save lives.
 K Ragavan, Bengaluru, India
 A favourite haunt closes its doors
 In reference to Janice Rodrigues's piece 'It was the perfect neighbourhood restaurant': Kizmet, next to Dubai Opera, closes permanently (April 27): devastated for them and us. It was one of my favourite restaurants.
 Tala Badri, Dubai
  This is absolutely terrible and very sad. One of our favourite restaurants. Staff, food and ambience was fabulous and was always a great evening after the Opera House.
 SharonAnne O'Neill, Dubai
 Small mercies: dolphins are visible in the water 
 Regarding Nick Webster and Ruba Haza's report Lack of noise brings rare dolphin pod closer to Dubai coast (April 27): maybe they should stop those awful jet skis for good.
 Saraih Jones, Dubai 
 Boris Johnson delivers a rousing speech on his return to work
 With reference to Nicky Harley's report UK Prime Minister Boris Johnson: 'We are beginning to turn the tide, but it is the moment of maximum risk' (April 27): that was a nice encouraging speech by the Prime Minister.
 Bukenya Sulaiman, Kampala, Uganda
  It's amazing that Boris Johnson came back in just one car. It is not unusual in Nigeria for one politician to move around in a cavalcade of a dozen cars.
 Cpo Ugochukwu, Dubai 
 Updated: April 29, 2020 11:51 AM
 Germany outlaws Hezbollah with dawn raids on 'terror group' 
 The Afghan fashion designer saving livelihoods by making luxurious silk masks
 The Evin Diaries: Fear and fights inside Iran's most notorious jail
 Daughter 'desperate' to board flight to Dubai to be closer to critically-ill father
 Money & Me: 'I’m waiting for when no rent comes in because tenants have lost their jobs'
 How to prevent pandemics from happening ever again
 Economic activity must resume. Here's why
 Are the US and UK paying a price for freedom?
 Cartoon for April 30, 2020
 Syria's chemical attacks may have failed to create moral outrage, yet there is hope
 https://www.thenational.ae/opinion/comment/the-uae-is-grateful-to-the-doctors-and-nurses-trying-to-keep-everyone-alive-1.1012213
