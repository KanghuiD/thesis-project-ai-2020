Politics - The Daily Beast
 The mayor may be more philo-Semite than anti-Semite, but he painted NY's Jews with a terribly broad brush in his latest failed bid to present himself as a strong leader.
 Stacey Abrams’ massive political machine would be an asset as a vice presidential pick, but it’s not without drawbacks. 
 The notion that this should be disqualifying to Biden in a race against Trump is patently ridiculous. Anybody who claims otherwise is using sexual assault as a political football.
 Gun-control advocates won a reprieve at the Supreme Court on Monday, but not a victory.
 Trump thought it was a good idea for Hannity to explore legal action against the news outlet for its critical coverage of how Fox News handled the coronavirus crisis.
 We are deep into Wonderland, and with the election coming, we’re just going to get deeper and deeper. 
 The party gambled and won by dropping its principles to stand behind Trump—but now that’s looking like a Pyrrhic victory. 
 https://www.thedailybeast.com/category/politics
