Coronavirus Mortality Rate (COVID-19) - Worldometer
 Last updated: March 5, 3:00 GMT 
  See also: Death Rate by Age and Sex of COVID-19 patients
 
 On this page:
 In his opening remarks at the March 3 media briefing on Covid-19, WHO Director-General Dr Tedros Adhanom Ghebreyesus stated: 
 “Globally, about 3.4% of reported COVID-19 cases have died. By comparison, seasonal flu generally kills far fewer than 1% of those infected.” [13] 
 Initially, the World Health Organization (WHO) had mentioned 2% as a mortality rate estimate in a press conference on Wednesday, January 29 [1][2] and again on February 10. However, on January 29 WHO specified that this was a very early and provisional estimate that might have changed. Surveillance was increasing, within China but also globally, but at the time it was said that:
 The Report of the WHO-China Joint Mission published on Feb. 28 by WHO [12] is based on 55,924 laboratory confirmed cases. The report notes that "The Joint Mission acknowledges the known challenges and biases of reporting crude CFR early in an epidemic" (see also our discussion on: How to calculate the mortality rate during an outbreak). Here are its findings on Case Fatality Ratio, or CFR (the mortality rate):
 "As of 20 February, 2,114 of the 55,924 laboratory confirmed cases have died (crude fatality ratio [CFR: 3.8%) (note: at least some of whom were identified using a case definition that included pulmonary disease).
 The overall CFR varies by location and intensity of transmission (i.e. 5.8% in Wuhan vs. 0.7% in other areas in China). 
 In China, the overall CFR was higher in the early stages of the outbreak (17.3% for cases with symptom onset from 1-10 January) and has reduced over time to 0.7% for patients with symptom onset after 1 February. " [12] 
 The Joint Mission noted that the standard of care has evolved over the course of the outbreak. 
 Asked at a press conference on February 4 what the current mortality rate (or case fatality rate, CFR) is, an official with China NHC said that [7]:
 A preliminary study published on The Lancet on January 24 [3] provided an early estimation of 3% for the overall case fatality rate. Below we show an extract (highlights added for the relevant data and observations):
 Of the 41 patients in this cohort, 22 (55%) developed severe dyspnoea and 13 (32%) required admission to an intensive care unit, and six died.
 Hence, the case-fatality proportion in this cohort is approximately 14.6%, and the overall case fatality proportion appears to be closer to 3%. 
 However, both of these estimates should be treated with great caution because not all patients have concluded their illness (ie, recovered or died) and the true number of infections and full disease spectrum are unknown. 
 Importantly, in emerging viral infection outbreaks the case-fatality ratio is often overestimated in the early stages because case detection is highly biased towards the more severe cases. 
 As further data on the spectrum of mild or asymptomatic infection becomes available, one case of which was documented by Chan and colleagues, the case-fatality ratio is likely to decrease. 
 Nevertheless, the 1918 influenza pandemic is estimated to have had a case-fatality ratio of less than 5% but had an enormous impact due to widespread transmission, so there is no room for complacency.
 Fatality rate can also change as a virus can mutate, according to epidemiologists.
 A study on 138 hospitalized patients with 2019-nCoV infection, published on February 7 on JAMA, found that 26% of patients required admission to the intensive care unit (ICU) and 4.3% died, but a number of patients were still hospitalized at the time. [9]  
 A previous study had found that, out of 41 admitted hospital patients, 13 (32%) patients were admitted to an ICU and six (15%) died.[5]
 The Wang et al. February 7 study published on JAMA found that the median time from first symptom to dyspnea was 5.0 days, to hospital admission was 7.0 days, and to ARDS was 8.0 days.[9]
 Previously. the China National Health Commission reported the details of the first 17 deaths up to 24 pm 22 Jan 2020. A study of these cases found that the median days from first symptom to death were 14 (range 6-41) days, and tended to be shorter among people of 70 year old or above (11.5 [range 6-19] days) than those with ages below 70 year old (20 [range 10-41] days.[6]
 The JANA study found that, among those discharged alive, the median hospital stay was 10 days.[9] 
 For comparison, the case fatality rate with seasonal flu in the United States is less than 0.1% (1 death per every 1,000 cases).
 Mortality rate for SARS was 10%, and for MERS 34%. 
 The case fatality rate (CFR) represents the proportion of cases who eventually die from a disease.
 Once an epidemic has ended, it is calculated with the formula: deaths / cases.
 But while an epidemic is still ongoing, as it is the case with the current novel coronavirus outbreak, this formula is, at the very least, "naïve"  and can be "misleading if, at the time of analysis, the outcome is unknown for a non negligible proportion of patients." [8]
 In other words, current deaths belong to a total case figure of the past, not to the current case figure in which the outcome (recovery or death) of a proportion (the most recent cases) hasn't yet been determined.
 The correct formula, therefore, would appear to be:
  CFR = deaths at day.x / cases at day.x-{T}
 (where T = average time period from case confirmation to death)
 This would constitute a fair attempt to use values for cases and deaths belonging to the same group of patients.
 One issue can be that of determining whether there is enough data to estimate T with any precision, but it is certainly not T = 0 (what is implicitly used when applying the formula current deaths / current cases to determine CFR during an ongoing outbreak). 
 Let's take, for example, the data at the end of February 8, 2020: 813 deaths (cumulative total) and 37,552 cases (cumulative total) worldwide. 
 If we use the formula (deaths / cases) we get:
  813 / 37,552 = 2.2% CFR (flawed formula).
 With a conservative estimate of T = 7 days as the average period from case confirmation to death, we would correct the above formula by using February 1 cumulative cases, which were 14,381, in the denominator:
 Feb. 8 deaths / Feb. 1 cases = 813 / 14,381 = 5.7% CFR (correct formula, and estimating T=7).
 T could be estimated by simply looking at the value of (current total deaths + current total recovered) and pair it with a case total in the past that has the same value. For the above formula, the matching dates would be January 26/27, providing an estimate for T of 12 to 13 days. This method of estimating T uses the same logic of the following method, and therefore will yield the same result. 
 An alternative method, which has the advantage of not having to estimate a variable, and that is mentioned in the American Journal of Epidemiology study cited previously as a simple method that nevertheless could work reasonably well if the hazards of death and recovery at any time t measured from admission to the hospital, conditional on an event occurring at time t, are proportional, would be to use the formula:
  CFR = deaths / (deaths + recovered)
 which, with the latest data available, would be equal to:
  141,195 / (141,195 + 527,471) = 21% CFR (worldwide)
 If we now exclude cases in mainland China, using current data on deaths and recovered cases, we get: 
 137,853 / (137,853 + 449,579) = 23.5% CFR (outside of mainland China)
 The sample size above is limited, and the data could be inaccurate (for example, the number of recoveries in countries outside of China could be lagging in our collection of data from numerous sources, whereas the number of cases and deaths is more readily available and therefore generally more up to par).
 There was a discrepancy in mortality rates (with a much higher mortality rate in China) which however is not being confirmed as the sample of cases outside of China is growing in size. On the contrary, it is now higher outside of China than within. 
 That initial discrepancy was generally explained with a higher case detection rate outside of China especially with respect to Wuhan, where priority had to be initially placed on severe and critical cases, given the ongoing emergency. 
 Unreported cases would have the effect of decreasing the denominator and inflating the CFR above its real value. For example, assuming 10,000 total unreported cases in Wuhan and adding them back to the formula, we would get a CFR of 20.8% (quite different from the CFR of 21% based strictly on confirmed cases).
 Neil Ferguson, a public health expert at Imperial College in the UK, said his “best guess” was that there were 100,000 affected by the virus even though there were only 2,000 confirmed cases at the time. [11]
 Without going that far, the possibility of a non negligible number of unreported cases in the initial stages of the crisis should be taken into account when trying to calculate the case fatally rate. 
 As the days go by and the city organized its efforts and built the infrastructure, the ability to detect and confirm cases improved. As of February 3, for example, the novel coronavirus nucleic acid testing capability of
 Wuhan had increased to 4,196 samples per day from an initial 200 samples.[10]
 A significant discrepancy in case mortality rate can also be observed when comparing mortality rates as calculated and reported by China NHC: a CFR of 3.1% in the Hubei province (where Wuhan, with the vast majority of deaths is situated), and a CFR of 0.16% in other provinces (19 times less).
 Finally, we shall remember that while the 2003 SARS epidemic was still ongoing, the World Health Organization (WHO) reported a fatality rate of 4% (or as low as 3%), whereas the final case fatality rate ended up being 9.6%.
 https://www.worldometers.info/coronavirus/coronavirus-death-rate/