Coronavirus detected on air pollution particles: report | TheHill
 Researchers in Italy have found that coronavirus was detected on particles of air pollution, the Guardian reported.
 The researchers collected outdoor air pollution samples at both urban and industrial sites in the Bergamo province in Italy. They identified a gene specific to COVID-19 in several samples, according to the outlet. 
 The study has not yet been peer reviewed, and it does not determine whether the virus on pollution particles is in quantities sufficient enough to cause the disease in humans.
 The samples were confirmed in an independent laboratory by blind testing, according to the Guardian.
 Leonardo Setti, a researcher from the University of Bologna who led the study, told outlet that it is the lack of information about how the disease could be carrier by pollutants.
 “I am a scientist and I am worried when I don’t know,” he said. “If we know, we can find a solution. But if we don’t know, we can only suffer the consequences.” 
 If the virus was able to grab on to pollution particles, it could suggest that highly polluted areas could be more susceptible to transmit the disease. According to The Guardian, two other research groups have suggested that pollution particles could help the virus travel further. 
 The northern part of the country is one of the most polluted areas of Europe.
 Professor Jonathan Reid of Bristol University in the United Kingdom, who is also researching airborne transmission of the coronavirus, told the Guardian that, “It is perhaps not surprising that while suspended in air, the small droplets could combine with background urban particles and be carried around.”
 A study by researchers from the National Institute of Health, the Centers for Disease Control and Prevention, Princeton University and UCLA has found that coronavirus can stay viable in the air for up to three hours when the virus becomes suspended in droplets after someone coughs or sneezes.
 In the same study, researchers found that on surfaces such as plastic and stainless steel, the virus survived for up to two or three days. However, the researchers noted that the virus’s ability to infect people diminishes over time. 
 View the discussion thread.
 The Hill 1625 K Street, NW Suite 900 Washington DC 20006 | 202-628-8500 tel | 202-628-8503 fax
 https://thehill.com/blogs/blog-briefing-room/news/494677-coronavirus-detected-on-air-pollution-particles-report
