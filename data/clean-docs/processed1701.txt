What did Queen Elizabeth say in last night’s coronavirus speech? – The Sun
 THE Queen addressed the nation and Commonwealth over the ongoing coronavirus crisis on Sunday, April 5.
 Her Majesty said the UK "will succeed" in its fight against Covid-19, in the rallying message to Britain.
 In a rare speech, the Queen thanked Brits for following government rules to stay at home and praised those "coming together to help others".
 She also thanked key workers, saying "every hour" of work" brings us closer to a return to more normal times".
 "While we have faced challenges before, this one is different," the Queen said.
 "This time we join with all nations across the globe in a common endeavour, using the great advances of science and our instinctive compassion to heal.
 "We will succeed - and that success will belong to every one of us.
 "We should take comfort that while we may have more still to endure, better days will return: we will be with our friends again; we will be with our families again; we will meet again."
 The Queen, now 93, also said the "painful sense of separation from their loved ones" that social distancing was causing for people reminded her of the experience child evacuees had during the Second World War.
 "Now, as then, we know, deep down, that it is the right thing to do," she said.
 Don't miss the latest news and figures - and essential advice for you and your family.
 To receive The Sun's Coronavirus newsletter in your inbox every tea time, sign up here.
 Get Britain's best-selling newspaper delivered to your smartphone or tablet each day - find out more.
 You can watch the Queen's speech at the top of this page.
 We also followed it live in our blog - catch up with it HERE.
 The Queen's speech was broadcast on all major television channels and radio stations including the BBC, ITV and Channel 4.
 Prince Charles was revealed to have tested positive for coronavirus on March 25.
 However, he has now recovered after a period of self-isolation.
 Prince Charles' wife Camilla, Duchess of Cornwall was tested and found not to have the virus.
 No other Royal Family members have tested positive for coronavirus.
 On April 3, Prince Charles opened the 4,000-bed NHS Nightingale hospital in London via video-link.
 The facility, which was built in just nine days, will be used to treat Covid-19 patients who have been transferred from other intensive care units across London and is now the largest critical care facility in the world.
 Prince Charles said: "It is without doubt a spectacular and almost unbelievable feat of work in every sense, from its speed of construction in just nine days to its size and the skills of those who have created it.
 "An example if ever one was needed of how the impossible can be made possible and how we can achieve the unthinkable through human will and ingenuity.
 "The creation of this hospital is the result of an extraordinary collaboration and partnership between NHS managers, the military and all those involved to create a centre on a scale that has never been seen before in the United Kingdom."
  UK lockdown to be extended at least 3 weeks with virus peak days away
  Peter Andre shows fans upstairs at mansion with Junior's room and discs on wall
  Ricky Gervais tells celebs to stop moaning in mansions while NHS staff die
  UK Covid deaths may be 15% higher as shock stats reveal toll outside hospitals
  Pregnant Rochelle Humes shocked as Eamonn Holmes jokes 'Who's the dad?'
 https://www.thesun.co.uk/news/11321408/what-time-queen-speech-coronavirus/
