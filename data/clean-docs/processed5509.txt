Coronavirus: Writers 'face weird boom time' after crisis - BBC News
 Screenwriters will face a "weird boom time" when the coronavirus crisis ends, the chair of the union representing the profession has predicted.
 Lisa Holdsworth, of the Writers' Guild of Great Britain (WGGB), said studios would want "more scripts than ever".
 But there would be a "bottleneck" in production, meaning less established writers having to wait longer to get projects started and receive payment.
 The government praised the "fantastic" work of UK writers.
 Foreign Secretary Dominic Raab - standing in for Prime Minister Boris Johnson - said on Sunday that no changes to coronavirus lockdown measures were expected this week. He added that the UK was "still not past the peak of this virus".
 Ms Holdsworth, who has written for TV shows including Midsomer Murders, Call the Midwife and New Tricks, said: "There was a running joke on social media among writers for the first week of this that we've been doing this for years, we regularly take meetings in our pyjamas and all that kind of thing.
 "But as the weeks have gone on, it's begun to bite a little and the reports that we're getting through are of a rise in anxiety across the board."
 The government estimates the value of the creative industries to the UK economy is more than £100bn a year. and there are thought to be 85,000 self-employed authors, writers and translators in the country.
 With film and TV studios and theatres closed, Ms Holdsworth said coronavirus had created an "extremely worrying time" for a significant proportion of the WGGB's members.
 "Most freelancers prepare for a certain amount of uncertainty in their profession, certainly in the creative industries," she added. 
 "But it was awful, the week that we went into lockdown, to see actors, writers, musicians, their diaries emptied in the blink of an eye.
 "Not only were they waiting for money to come in - because it takes ages for venues to pay people and money to come through - but all their future earnings disappeared with no sign of when they're going to start again."
 The government announced last month that self-employed workers could apply for a monthly grant of up to £2,500 to help them cope with loss of earnings caused by coronavirus. But this money will not begin to arrive until at least early June. 
 Ms Holdsworth instead recommended "some kind of universal income, that then could have been claimed back via tax if needs be", adding: "But if this [the grant] is where it's got to go, then it must be expedited because people are in real hardship."
 The BBC is repeating comedy shows including Gavin and Stacey and Outnumbered at peak-time during the coronavirus crisis, in the absence of new productions.
 Fewer episodes of EastEnders, Coronation Street and Emmerdale are being shown, as filming is disrupted or halted.
 But Ms Holdsworth said: "Once TV and film production gets going again, people are going to want scripts more than ever. There's going to be a bottleneck.
 "The problem we will have is the number of facilities and studios available to us. People are going to have to get really creative about how they film stuff.
 "And so there will be a weird boom time once once restrictions are lifted. If you're a new, unestablished writer, it's going to be enormously difficult, more so than it usually is, because that bottleneck will mean companies won't be looking at new development for a while."
 However, one group which could do well more immediately out of the crisis is authors, according to Ms Holdsworth.
 "It's going too be interesting, in terms of people wanting to download books and having a bit more time on the hands to read," she said. "There may be a positive outcome."
 And what advice is there for scriptwriters, playwrights, poets and authors, looking ahead?
 "Don't write about Covid-19," said Ms Holdsworth. "No one's going to want to read, listen to or watch anything about that. There's a definite pivot towards things with a slightly cheerier outlook. People are going to need that."
 Creative industries minister Caroline Dinenage said: "The UK is home to a huge number of incredibly talented writers, from screenwriters to novelists and playwrights to poets.
 "They make up part of the UK's fantastic creative industries and have a huge role in keeping us entertained while we are all staying at home to protect the NHS and save lives."
 The World Health Organization "failed in its basic duty" over coronavirus, Donald Trump says.
 Have you been getting these songs wrong?
 What happens to your body in extreme heat?
 https://www.bbc.com/news/entertainment-arts-52136064
